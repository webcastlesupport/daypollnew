<?php
class BusinessProfile extends CI_Controller{


    public $s3_url   = "https://daypoll.s3.amazonaws.com/";

    private $loader = array('pageview'=>'undefined');
    function __construct() {
        
        parent::__construct();
        $this->load->model('Model');
        
    }

    function isAuthorised(){

        if(empty($this->session->userdata('sess_admin_id'))){
            redirect('Login');
        }
    }

    function businessprofile(){

        $this->isAuthorised();
        $this->loader['pageview']  = "Businessprofile";
        $order='bf_created_date'.' '.'desc';
        $this->loader['data']      = $this->Model
                                               ->fetch_from_table('business_profile',[],$order)
                                               ->result();  
        foreach($this->loader['data'] as $value => $key){
            $this->loader['data'][$value]->created_user_name = $this->Model
                                                                        ->fetch_from_table('users',array('user_id' => $this->loader['data'][$value]->bf_created_user))
                                                                        ->row('users_name');  
            $this->loader['data'][$value]->category          = $this->Model
                                                                        ->fetch_from_table('business_profile_category',array('bfc_id' => $this->loader['data'][$value]->bf_category))
                                                                        ->row('bfc_name');                                                             
        }                                                
        $this->load->view("common/template",$this->loader);
    }


    function businessprofileposts(){

        $this->isAuthorised();
        $this->loader['pageview']  = "BusinessProfilePosts";
        $order='bp_posts_uploaded_date'.' '.'desc';
        $this->loader['data']      = $this->Model
                                               ->fetch_from_table('business_profile_posts',[],$order)
                                               ->result_array();  
                                           
        foreach($this->loader['data'] as $value => $key){
            
            $this->loader['data'][$value]['created_user_name']        = $this->Model
                                                                                ->fetch_from_table('users',array('user_id' => $this->loader['data'][$value]['bp_posts_user_id']))
                                                                                ->row('users_name');  
            $this->loader['data'][$value]['bf_name']                  = $this->Model
                                                                                ->fetch_from_table('business_profile',array('bf_created_user' => $this->loader['data'][$value]['bp_posts_user_id']))
                                                                                ->row('bf_name');
            $this->loader['data'][$value]['bp_posts_photo_or_video']  = $this->s3_url."assets/images/business_profile/".$this->loader['data'][$value]['bp_posts_photo_or_video'];                                                           
        }     
                                                      
        $this->load->view("common/template",$this->loader);
    }

  
    function Blockbusinessprofileposts(){

        $flag   = "";
        $this->output->set_content_type('application/json');
        $id     = $this->input->post('id');
        $active = $this->input->post('active');
        
        if($active == 1)$flag = 0;
        if($active == 0)$flag = 1;
        
        $res    = $this->Model->update_table('business_profile_posts',array('bp_posts_id' => $id),array('bp_posts_active' => $flag));
        if($res > 0)
        $this->output->set_output(json_encode(array('details'=>$res,'error' => 0)));
        else
        $this->output->set_output(json_encode(array( 'error' => 1)));
    }
    


    function businessprofilepostsdetails(){

        $this->isAuthorised();
        $this->output->set_content_type('application/json');
        $res      = $this->Model
                            ->fetch_from_table('business_profile_posts',array('bp_posts_id' => $this->input->post('id')))
                            ->result();  
        if($res){                                      
        foreach($res as $value => $key){

            //$res[$value]->bf_photo          = $this->s3_url."assets/images/businessprofile/".$res[$value]->bf_photo;

            $res[$value]->bp_posts_photo_or_video    = $this->s3_url."assets/images/business_profile/".$res[$value]->bp_posts_photo_or_video;

            $res[$value]->created_user_name          = $this->Model
                                                            ->fetch_from_table('users',array('user_id' => $res[$value]->bp_posts_user_id))
                                                            ->row('users_name');  
                                                           
         
        }                       
        
            $this->output->set_output(json_encode(array('details' => $res[0] ,'error' => 0)));  
        }else{
            $this->output->set_output(json_encode(array('error' => 1)));  
        }                       
     
    }


    function businessprofiledetails(){

        $this->isAuthorised();
        $this->output->set_content_type('application/json');
        $res      = $this->Model
                                               ->fetch_from_table('business_profile',array('bf_id' => $this->input->post('id')))
                                               ->result();  
        if($res){                                      
        foreach($res as $value => $key){

            $res[$value]->bf_photo          = $this->s3_url."assets/images/businessprofile/".$res[$value]->bf_photo;

            $res[$value]->bf_cover_photo    = $this->s3_url."assets/images/businessprofile/cover/".$res[$value]->bf_cover_photo;

            $res[$value]->created_user_name = $this->Model
                                                    ->fetch_from_table('users',array('user_id' => $res[$value]->bf_created_user))
                                                    ->row('users_name');  
            $res[$value]->category          = $this->Model
                                                    ->fetch_from_table('business_profile_category',array('bfc_id' => $res[$value]->bf_category))
                                                    ->row('bfc_name'); 
            $res[$value]->polls             = $this->Model
                                                    ->fetch_from_table('business_profile_polls',array('bpp_id' => $res[$value]->bf_id))
                                                    ->num_rows(); 
                                                    
            $res[$value]->subcategory1      = $this->Model
                                                    ->fetch_from_table('business_profile_sub_category',array('bpsc_id' => $res[$value]->bf_sub_category_1))
                                                    ->row('bpsc_name'); 

            $res[$value]->subcategory2      = $this->Model
                                                    ->fetch_from_table('business_profile_sub_category_2',array('bpssc_id' => $res[$value]->bf_sub_category_2))
                                                    ->row('bpssc_name'); 

            $res[$value]->subcategory3      = $this->Model
                                                    ->fetch_from_table('business_profile_sub_category_3',array('bpsssc_id' => $res[$value]->bf_sub_category_3))
                                                    ->row('bpsssc_name'); 
        }                       
        
            $this->output->set_output(json_encode(array('values' => $res[0] ,'error' => 0)));  
        }else{
            $this->output->set_output(json_encode(array('error' => 1)));  
        }                       
     
    }

    function blockProfile(){

        $this->output->set_content_type('application/json');
        $this->isAuthorised();
        $id     = $this->input->post('cid');
        $flag   = $this->input->post('active') == 1 ? 0 :1;
        if($this->Model->update_table('business_profile',array('bf_id' => $id) , array('bf_status' => $flag))){
                  
        $this->output->set_output(json_encode(array('data'=> 'successfully blocked','error'=>0)));
        }
        else{
        $this->output->set_output(json_encode(array('data'=> 'operation failed' , 'error'=>1)));
        }
    }

    
     // load posts report page
     function businessprofilepostsreport(){

        $this->isAuthorised();
        $this->loaders['pageview'] = 'BusinessprofilepostsReport';
        $this->load->view('common/template',$this->loaders);

    }

    // load posts report page
    function businessprofilechallengereport(){

        $this->isAuthorised();
        $this->loaders['pageview'] = 'BusinessProfileChallengeReport';
        $this->load->view('common/template',$this->loaders);

    }

      // block posts by post id
      function blockposts(){

        $flag   = "";
        $this->output->set_content_type('application/json');
        $id     = $this->input->post('id');
        $active = $this->input->post('active');
        
        if($active == 1)$flag = 0;
        if($active == 0)$flag = 1;
        
       
        if($this->Model->update_table('business_profile_posts',array('bp_posts_id' => $id) , array('bp_posts_active' => $flag))){
                  
            $this->output->set_output(json_encode(array('data'=> 'successfully blocked','error'=>0)));
            }
            else{
            $this->output->set_output(json_encode(array('data'=> 'operation failed' , 'error'=>1)));
            }
    }

     // get posts details by id
     function getPostsdetails(){

        $this->output->set_content_type('application/json');
        $id             = $this->input->post('id');
        $res            = $this->Model->fetch_from_table('business_profile_posts',array('bp_posts_id' => $id))->result_array();
        $data           = $res[0];
        $photo_or_video = $data['bp_posts_photo_or_video'];
        $ext            = substr($photo_or_video, strpos($photo_or_video, ".") + 1);   
        $ext_           = ($ext=='mp4' ? $ext_value = "video":$ext_value = "photo");
        
        $data['bp_posts_uploaded_date']  = date('d/m/Y  h:i A' ,strtotime($data['bp_posts_uploaded_date'])); 

        $data['bp_posts_photo_or_video'] = $this->s3_url."assets/images/business_profile/".$data['bp_posts_photo_or_video'];
       
        $post_title                 = rtrim($data['bp_posts_title'],"\\"); 
        $post_title_json            = '{"posts_title": "'.$post_title.'"}';
        $post_json_decoded          = json_decode($post_title_json);
        $data['posts_title']        = $post_json_decoded->{'posts_title'};


        $post_content               = rtrim($data['bp_posts_content'],"\\"); 
        $post_content_json          = '{"posts_content": "'.$post_content.'"}';
        $post_json_decoded          = json_decode($post_content_json);
        $data['posts_content']      = $post_json_decoded->{'posts_content'};


        $this->output->set_output(json_encode(array('details'=>$data,'photo_or_video'=>$ext_)));
        
        unset($ext);

    }

         // get posts report details by id
         function getpostreportdetails(){

            $this->output->set_content_type('application/json');
            $id             = $this->input->post('id');
            $res            = $this->Model->fetch_from_table('business_profile_posts_report',array('posts_report_post_id'=>$id))->result_array();
            $this->output->set_output(json_encode(array('details'=>$res)));
    
        }

          // load group page
    function challenge($type = null){
        
        $this->isAuthorised();
        $this->loader['pageview'] = "BusinessProfileChallenges";
        $this->loader['res']      = $this->Model->getallChallenge($type);
        $this->loader['type']     = $type;  
        $this->load->view('common/template',$this->loader);
    }

    function blockchallenge(){

        $flag   = "";
        $this->output->set_content_type('application/json');
        $id     = $this->input->post('cid');
        $active = $this->input->post('active');
        
        if($active == 1)$flag = 0;
        if($active == 0)$flag = 1;
        
        $res    = $this->Model->update_table('business_profile_challenge',array('challenge_id' => $id),array('challenge_active' => $flag));
        if($res > 0)
        $this->output->set_output(json_encode(array('details'=>$res,'id' => $id,'act' => $active,'error' => 0)));
        else
        $this->output->set_output(json_encode(array( 'error' => 1)));
    }

       // get challenge detials
       function getchallengedetails(){

        $this->output->set_content_type('application/json');
        
        $cid    = $this->input->post('id');
        $res    = $this->Model->getchallengedetails($cid);
        foreach($res as $val => $key){
            $res[$val]['pollcount'] = $this->Model->getChallengepollCount($cid);
        }
        $this->output->set_output(json_encode(array('values' => $res[0])));
    }

    // block challenge by id
    function verifychallenge(){
        $this->output->set_content_type('application/json');
        
        $cid     = $this->input->post('cid');
        $active  = $this->input->post('active');
        if($active==1)$flag=0;
        if($active==0)$flag=1;
        $res    = $this->Model->verifychallenge($cid , $flag);
        $this->output->set_output(json_encode(array('values'=>$res)));
    }

     // get posts report details by id
     function getchallengereportdetails(){

            $this->output->set_content_type('application/json');
            $id             = $this->input->post('id');
            $res            = $this->Model->fetch_from_table('business_profile_challenge_report',array('challenge_report_challenge_id'=>$id))->result_array();
            $this->output->set_output(json_encode(array('details'=>$res)));

    }

     //get group posts
     function viewMembers($id = null){
        
        $zone = getallheaders();
        $this->loader['winner']     = "";
        $this->loader['winner_msg'] = "";
        $winner = "";
        
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";
        if(!empty($id)){
        $res           = $this->Model->viewMembers($id); 
        $res_challenge = $this->Model->getchallengedetails($id); 

        $dt = new DateTime();
                
            $current_date = $dt->format('d-m-Y h:i A');
           
            $GMT  = new DateTimeZone("GMT");
            $date = new DateTime($current_date, $GMT );
            $date->setTimezone(new DateTimeZone($zone['timezone']));
            $date =   $date->format('Y-m-d h:i A');
        
            
        if($res_challenge != null){
                
            foreach($res_challenge as $value => $key){

        $now            = new DateTime($zone['timezone']);
                   
                 
    
        $expdate = $res_challenge[$value]['challenge_expired_date']." ".$res_challenge[$value]['challenge_expired_time'];
        
       
        $expired_date   = date("Y-m-d H:i:s ", strtotime($expdate));
        
        
       
        $res_challenge[$value]['cdate'] = $date;
       
        $current_date = strtotime($date);
        $exp_date     = strtotime($expdate);
      
      
        $diff   = $exp_date - $current_date;
        $res_challenge[$value]['diff'] = $diff;
        
        $res_challenge[$value]['challenge_status']    = $diff > 0 ? 1 : 0;
       
        $expired_date   = new DateTime($expired_date);    
        
         
        if($diff < 0){
            $winner = $this->Model->getWinner($id);
           
            foreach($winner as $row => $k){
                $user_details = $this->Model->getUsername($winner[$row]['bp_challenge_posts_id']);
                $winner[$row]['users_name']  = $user_details['username'];
                $winner[$row]['users_photo'] = $user_details['photo'];
            }
        }       
        
        
        
        if($winner != null){
            $this->loader['winner'] = $winner;
        }else{
           
            $this->loader['winner_msg'] = "no winner";
        }

        }
    }

      
        if($res != null){
           $this->loader['data']   = $res;   
        }
        else{
            $this->loader['data']   = "";
        }

        }
        else{
            echo "parameter missing";
        }
       
        $this->isAuthorised();
        $this->loader['pageview']= "BusinessProfileChallengeMembers";   
        $this->load->view('common/template',$this->loader);
    }


}
?>