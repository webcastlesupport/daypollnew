<?php


class Category extends CI_Controller{
    
    private $loader = array('pageview'=>'undefined');
    function __construct() {
        
        parent::__construct();
        $this->load->model('Model');
        
    }

    function isAuthorised(){

        if(empty($this->session->userdata('sess_admin_id'))){
            redirect('Login');
        }
    }

    function Subcategory($id = null){
        
        $this->isAuthorised();
        $this->loader['pageview']       = "Subcategory";
        $this->loader['subcategory_id'] = $id;
         $order='bpsc_created_date'.' '.'desc';
    
        if($id){
        $this->loader['cname'] = $this->Model
                        ->fetch_from_table('business_profile_sub_category',array('bpsc_id' => $id))
                        ->row('bpsc_name');
        $this->loader['catid'] = $this->Model
                        ->fetch_from_table('business_profile_sub_category',array('bpsc_id' => $id))
                        ->row('bpsc_category_id');                
        }
        $this->loader['data']          = $this->Model
                                            ->fetch_from_table('business_profile_sub_category',[],$order) 
                                            ->result_array();
        foreach($this->loader['data'] as $value => $key){
            $this->loader['data'][$value]['cat_name']  = $this->Model
                                                            ->fetch_from_table('business_profile_category',array('bfc_id' => $this->loader['data'][$value]['bpsc_category_id'])) 
                                                            ->row('bfc_name');
        }
        $this->loader['category']      = $this->Model
                                            ->fetch_from_table('business_profile_category') 
                                            ->result_array();                                    
        $this->load->view('common/template',$this->loader);                                    
    
    }



    function Subcategory2($id = null){
        
        $this->isAuthorised();
        $this->loader['pageview']       = "Sub_category_2";
        $this->loader['subcategory_id'] = $id;
        $order='bpssc_created_date'.' '.'desc';
        if($id){
        $this->loader['cname'] = $this->Model
                        ->fetch_from_table('business_profile_sub_category_2',array('bpssc_id' => $id))
                        ->row('bpsc_name');
        $this->loader['catid'] = $this->Model
                        ->fetch_from_table('business_profile_sub_category_2',array('bpssc_id' => $id))
                        ->row('bpsc_category_id');                
        }
        $this->loader['data']          = $this->Model
                                            ->fetch_from_table('business_profile_sub_category_2',[],$order) 
                                            ->result_array();

        foreach($this->loader['data'] as $value => $key){
            $this->loader['data'][$value]['cat_name']      = $this->Model
                                                            ->fetch_from_table('business_profile_category',array('bfc_id' => $this->loader['data'][$value]['bpssc_category_id'])) 
                                                            ->row('bfc_name');
            $this->loader['data'][$value]['sub_cat_name']  = $this->Model
                                                            ->fetch_from_table('business_profile_sub_category',array('bpsc_id' => $this->loader['data'][$value]['bpssc_sub_category_id'])) 
                                                            ->row('bpsc_name');                                                
        }
        $this->loader['category']      = $this->Model
                                            ->fetch_from_table('business_profile_category') 
                                            ->result_array();          
        


        $this->load->view('common/template',$this->loader);                                    
    
    }


    function Subcategory3($id = null){
        
        $this->isAuthorised();
        $this->loader['pageview']       = "Sub_category_3";
        $this->loader['subcategory_id'] = $id;
        $order='bpsssc_created_date'.' '.'desc';
        if($id){
        $this->loader['cname'] = $this->Model
                        ->fetch_from_table('business_profile_sub_category_2',array('bpssc_id' => $id))
                        ->row('bpsc_name');
        $this->loader['catid'] = $this->Model
                        ->fetch_from_table('business_profile_sub_category_2',array('bpssc_id' => $id))
                        ->row('bpsc_category_id');                
        }
        $this->loader['data']          = $this->Model
                                            ->fetch_from_table('business_profile_sub_category_3',[],$order) 
                                            ->result_array();

        foreach($this->loader['data'] as $value => $key){
            $this->loader['data'][$value]['cat_name']           = $this->Model
                                                                    ->fetch_from_table('business_profile_category',array('bfc_id' => $this->loader['data'][$value]['bpsssc_category_id'])) 
                                                                    ->row('bfc_name');
            $this->loader['data'][$value]['sub_cat_name']       = $this->Model
                                                                    ->fetch_from_table('business_profile_sub_category',array('bpsc_id' => $this->loader['data'][$value]['bpsssc_sub_category_id'])) 
                                                                    ->row('bpsc_name');     
            $this->loader['data'][$value]['sub_sub_cat_name']   = $this->Model
                                                                    ->fetch_from_table('business_profile_sub_category_2',array('bpssc_id' => $this->loader['data'][$value]['bpsssc_sub_sub_category_id'])) 
                                                                    ->row('bpssc_name');                                                                                            
        }
        $this->loader['category']      = $this->Model
                                            ->fetch_from_table('business_profile_category') 
                                            ->result_array();          
        


        $this->load->view('common/template',$this->loader);                                    
    
    }


    function getSubCategory(){

        $this->output->set_content_type('application/json');
        $this->isAuthorised();
        $id = $this->input->post('catid'); 
        $res = $this->Model
                        ->fetch_from_table('business_profile_sub_category',array('bpsc_category_id' => $id))
                        ->result_array();
        if($res)                
        $this->output->set_output(json_encode(array('value'=>$res,'error'=>0)));
        else
        $this->output->set_output(json_encode(array('error'=>1)));
    }


    function getSubCategory2(){

        $this->output->set_content_type('application/json');
        $this->isAuthorised();
        $id = $this->input->post('catid'); 
        $res = $this->Model
                        ->fetch_from_table('business_profile_sub_category_2',array('bpssc_sub_category_id' => $id))
                        ->result_array();
        if($res)                
        $this->output->set_output(json_encode(array('value'=>$res,'error'=>0)));
        else
        $this->output->set_output(json_encode(array('error'=>1)));
    }
    

    function Category($id = null){
        
        $this->isAuthorised();
        $this->loader['pageview']       = "Category";
        $this->loader['category_id']    = $id;
         $order='bfc_created_date'.' '.'desc';
        if($id){
        $this->loader['cname'] = $this->Model
                        ->fetch_from_table('business_profile_category',array('bfc_id' => $id))
                        ->row('bfc_name');
        }
        $this->loader['data']     = $this->Model
                                            ->fetch_from_table('business_profile_category',[],$order) 
                                            ->result_array();
        $this->load->view('common/template',$this->loader);                                    
    
    }


    function getCategorybyId($id){
        $this->output->set_content_type('application/json');
        $this->isAuthorised();
       
        $res = $this->Model
                        ->fetch_from_table('business_profile_category',array('bfc_id' => $id))
                        ->result_array();
        if($res)                
        $this->output->set_output(json_encode(array('data'=>$res[0],'error'=>0)));
        else
        $this->output->set_output(json_encode(array('error'=>1)));
                  
    }

    function blockCategory(){
        $this->output->set_content_type('application/json');
        $this->isAuthorised();
        $id     = $this->input->post('cid');
        $flag   = $this->input->post('active') == 1 ? 0 :1;
        if($this->Model->update_table('business_profile_category',array('bfc_id' => $id) , array('bfc_status' => $flag))){
                  
        $this->output->set_output(json_encode(array('data'=> 'successfully blocked','error'=>0)));
        }
        else{
        $this->output->set_output(json_encode(array('data'=> 'operation failed' , 'error'=>1)));
        }
   
    }

    function saveCategory(){

        $this->output->set_content_type('application/json');
        $this->isAuthorised();
        $name           = $this->input->post('cname');
        $id             = $this->input->post('cid');
        if(!empty($name)){
        $check_exists   = $res = $this->Model
                                            ->fetch_from_table('business_profile_category',array('bfc_name' => $name))
                                            ->num_rows();  
        
        if($check_exists <= 0){
        if(!empty($id)){

            if($this->Model->update_table('business_profile_category',array('bfc_id' => $id),array('bfc_name' => $name))){
                  
                $this->output->set_output(json_encode(array('message'=> 'successfully updated','error'=>0)));
                }
                else{
                $this->output->set_output(json_encode(array('message'=> 'operation failed' , 'error'=>1)));
             }
        }else{

            
            if($this->Model->insert_table('business_profile_category',array('bfc_name' => $name))){
                    
            $this->output->set_output(json_encode(array('message'=> 'successfully saved','error'=>0)));
            }
            else{
            $this->output->set_output(json_encode(array('message'=> 'operation failed' , 'error'=>1)));
            }
    }
        }else{
        $this->output->set_output(json_encode(array('message'=> 'already exists' , 'error'=>1)));
        }
    }else{
        $this->output->set_output(json_encode(array('message'=> 'parameter missing' , 'error'=>1)));
    }
    }



    function blockSubCategory(){
        $this->output->set_content_type('application/json');
        $this->isAuthorised();
        $id     = $this->input->post('cid');
        $flag   = $this->input->post('active') == 1 ? 0 :1;
        if($this->Model->update_table('business_profile_sub_category',array('bpsc_id' => $id) , array('bpsc_status' => $flag))){
                  
        $this->output->set_output(json_encode(array('data'=> 'successfully blocked','error'=>0)));
        }
        else{
        $this->output->set_output(json_encode(array('data'=> 'operation failed' , 'error'=>1)));
        }
   
    }

    function blockSubCategory2(){
        $this->output->set_content_type('application/json');
        $this->isAuthorised();
        $id     = $this->input->post('cid');
        $flag   = $this->input->post('active') == 1 ? 0 :1;
        if($this->Model->update_table('business_profile_sub_category_2',array('bpssc_id' => $id) , array('bpssc_status' => $flag))){
                  
        $this->output->set_output(json_encode(array('data'=> 'successfully blocked','error'=>0)));
        }
        else{
        $this->output->set_output(json_encode(array('data'=> 'operation failed' , 'error'=>1)));
        }
   
    }
    function blockSubCategory3(){
        $this->output->set_content_type('application/json');
        $this->isAuthorised();
        $id     = $this->input->post('cid');
        $flag   = $this->input->post('active') == 1 ? 0 :1;
        if($this->Model->update_table('business_profile_sub_category_3',array('bpsssc_id' => $id) , array('bpsssc_status' => $flag))){
                  
        $this->output->set_output(json_encode(array('data'=> 'successfully blocked','error'=>0)));
        }
        else{
        $this->output->set_output(json_encode(array('data'=> 'operation failed' , 'error'=>1)));
        }
   
    }

    function saveSubCategory(){

        $this->output->set_content_type('application/json');
        $this->isAuthorised();
        $name           = $this->input->post('cname');
        $id             = $this->input->post('cid');
        $subcategory    = $this->input->post('subcat');
        if(!empty($name && $subcategory)){
        $check_exists   = $res = $this->Model
                                            ->fetch_from_table('business_profile_sub_category',array('bpsc_name' => $name,'bpsc_category_id'=>$subcategory))
                                            ->num_rows();  
        
        if($check_exists <= 0){
        if(!empty($id)){

            if($this->Model->update_table('business_profile_sub_category',array('bpsc_id' => $id),array('bpsc_category_id'=>$subcategory,'bpsc_name' => $name))){
                  
                $this->output->set_output(json_encode(array('message'=> 'successfully updated','error'=>0)));
                }
                else{
                $this->output->set_output(json_encode(array('message'=> 'operation failed' , 'error'=>1)));
             }
        }else{

            
            if($this->Model->insert_table('business_profile_sub_category',array('bpsc_category_id'=>$subcategory,'bpsc_name' => $name))){
                    
            $this->output->set_output(json_encode(array('message'=> 'successfully saved','error'=>0)));
            }
            else{
            $this->output->set_output(json_encode(array('message'=> 'operation failed' , 'error'=>1)));
            }
        }
        }else{
        $this->output->set_output(json_encode(array('message'=> 'already exists' , 'error'=>1)));
        }
        }else{
        $this->output->set_output(json_encode(array('message'=> 'parameter missing' , 'error'=>1)));
        }
    }
    

    function saveSubCategory2(){

        $this->output->set_content_type('application/json');
        $this->isAuthorised();
        $name           = $this->input->post('cname');
        $id             = $this->input->post('cid');
        $subcategory    = $this->input->post('subcatid');
        $category       = $this->input->post('catid');

        if(!empty($name && $subcategory && $category)){
        $check_exists   = $this->Model
                             ->fetch_from_table('business_profile_sub_category_2',array('bpssc_name' => $name,'bpssc_sub_category_id'=>$subcategory,'bpssc_category_id' => $category))
                             ->num_rows();  
        
        if($check_exists <= 0){
        if(!empty($id)){

            if($this->Model->update_table('business_profile_sub_category_2',array('bpsc_id' => $id),array('bpsc_category_id'=>$subcategory,'bpsc_name' => $name))){
                  
                $this->output->set_output(json_encode(array('message'=> 'successfully updated','error'=>0)));
                }
                else{
                $this->output->set_output(json_encode(array('message'=> 'operation failed' , 'error'=>1)));
             }
        }else{

            
            if($this->Model->insert_table('business_profile_sub_category_2',array('bpssc_sub_category_id'=>$subcategory,'bpssc_category_id' => $category,'bpssc_name' => $name))){
                    
            $this->output->set_output(json_encode(array('message'=> 'successfully saved','error'=>0)));
            }
            else{
            $this->output->set_output(json_encode(array('message'=> 'operation failed' , 'error'=>1)));
            }
        }
        }else{
        $this->output->set_output(json_encode(array('message'=> 'already exists' , 'error'=>1)));
        }
        }else{
        $this->output->set_output(json_encode(array('message'=> 'parameter missing' , 'error'=>1)));
        }
    }




    function saveSubCategory3(){

        $this->output->set_content_type('application/json');
        $this->isAuthorised();
        $name           = $this->input->post('cname');
        $id             = $this->input->post('cid');
        $subcategory    = $this->input->post('subcatid');
        $subcategory2   = $this->input->post('subcatid2');
        $category       = $this->input->post('catid');

        if(!empty($name && $subcategory && $category)){
        $check_exists   = $this->Model
                             ->fetch_from_table('business_profile_sub_category_3',array('bpsssc_name' => $name,'bpsssc_sub_category_id'=>$subcategory,'bpsssc_category_id' => $category))
                             ->num_rows();  
        
        if($check_exists <= 0){
        if(!empty($id)){

            if($this->Model->update_table('business_profile_sub_category_3',array('bpsc_id' => $id),array('bpsc_category_id'=>$subcategory,'bpsc_name' => $name))){
                  
                $this->output->set_output(json_encode(array('message'=> 'successfully updated','error'=>0)));
                }
                else{
                $this->output->set_output(json_encode(array('message'=> 'operation failed' , 'error'=>1)));
             }
        }else{

            
            if($this->Model->insert_table('business_profile_sub_category_3',array('bpsssc_sub_category_id'=>$subcategory,'bpsssc_sub_sub_category_id'=>$subcategory2,'bpsssc_category_id' => $category,'bpsssc_name' => $name))){
                    
            $this->output->set_output(json_encode(array('message'=> 'successfully saved','error'=>0)));
            }
            else{
            $this->output->set_output(json_encode(array('message'=> 'operation failed' , 'error'=>1)));
            }
        }
        }else{
        $this->output->set_output(json_encode(array('message'=> 'already exists' , 'error'=>1)));
        }
        }else{
        $this->output->set_output(json_encode(array('message'=> 'parameter missing' , 'error'=>1)));
        }
    }
}