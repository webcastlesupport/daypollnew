<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Challenge extends CI_Controller{
    
    private $loader = array('pageview'=>'undefined');
    function __construct() {
        
        parent::__construct();
        $this->load->model('ChallengeModel');
        
    }
    

    
    function isAuthorised(){

        if(empty($this->session->userdata('sess_admin_id'))){
            redirect('Login');
        }
    }
    
    // load group page
    function challenge($type = null){
        
        $this->isAuthorised();
        $this->loader['pageview'] = "Challenge";
        $this->loader['res']      = $this->ChallengeModel->getallChallenge($type);
        $this->loader['type']     = $type;  
        $this->load->view('common/template',$this->loader);
    }
    
    

    // get challenge detials
    function getchallengedetails(){

        $this->output->set_content_type('application/json');
        
        $cid    = $this->input->post('id');
        $res    = $this->ChallengeModel->getchallengedetails($cid);
        foreach($res as $val => $key){
            $res[$val]['pollcount'] = $this->ChallengeModel->getChallengepollCount($cid);
        }
        $this->output->set_output(json_encode(array('values' => $res[0])));
    }

    // block challenge by id
    function blockchallenge(){
        $this->output->set_content_type('application/json');
        
        $cid     = $this->input->post('cid');
        $active  = $this->input->post('active');
        if($active==1)$flag=0;
        if($active==0)$flag=1;
        $res    = $this->ChallengeModel->blockchallenge($cid , $flag);
        $this->output->set_output(json_encode(array('values'=>$res)));
    }

     // block challenge by id
     function verifychallenge(){
        $this->output->set_content_type('application/json');
        
        $cid     = $this->input->post('cid');
        $active  = $this->input->post('active');
        if($active==1)$flag=0;
        if($active==0)$flag=1;
        $res    = $this->ChallengeModel->verifychallenge($cid , $flag);
        $this->output->set_output(json_encode(array('values'=>$res)));
    }

     //get challenge participants
     function viewjoinedMembers($id = null){

        if(!empty($id)){
        $res = $this->ChallengeModel->viewMembers($id); 

        if($res != null){
            $this->loader['data']   = $res;   
        }
        else{
            $this->loader['data']   = "";
            }
        }
        else{
            echo "parameter missing";
        }
        $this->isAuthorised();
        $this->loader['pageview']= "Challengejoinedmembers";   
        $this->load->view('common/template',$this->loader);
    }


      //get group posts
      function viewMembers($id = null){
        
        $zone = getallheaders();
        $this->loader['winner']     = "";
        $this->loader['winner_msg'] = "";
        $winner = "";
        
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";
        if(!empty($id)){
        $res           = $this->ChallengeModel->viewMembers($id); 
        $res_challenge = $this->ChallengeModel->getchallengedetails($id); 

        $dt = new DateTime();
                
            $current_date = $dt->format('d-m-Y h:i A');
           
            $GMT  = new DateTimeZone("GMT");
            $date = new DateTime($current_date, $GMT );
            $date->setTimezone(new DateTimeZone($zone['timezone']));
            $date =   $date->format('Y-m-d h:i A');
        
            
        if($res_challenge != null){
                
            foreach($res_challenge as $value => $key){

        $now            = new DateTime($zone['timezone']);
                   
                 
    
        $expdate = $res_challenge[$value]['challenge_expired_date']." ".$res_challenge[$value]['challenge_expired_time'];
        
       
        $expired_date   = date("Y-m-d H:i:s ", strtotime($expdate));
        
        
       
        $res_challenge[$value]['cdate'] = $date;
       
        $current_date = strtotime($date);
        $exp_date     = strtotime($expdate);
      
      
        $diff   = $exp_date - $current_date;
        $res_challenge[$value]['diff'] = $diff;
        
        $res_challenge[$value]['challenge_status']    = $diff > 0 ? 1 : 0;
       
        $expired_date   = new DateTime($expired_date);    
        
         
        if($diff < 0){
            $winner = $this->ChallengeModel->getWinner($id);
           
            foreach($winner as $row => $k){
                $user_details = $this->ChallengeModel->getUsername($winner[$row]['challenge_posts_id']);
                $winner[$row]['users_name']  = $user_details['username'];
                $winner[$row]['users_photo'] = $user_details['photo'];
            }
        }       
        
        
        
        if($winner != null){
            $this->loader['winner'] = $winner;
        }else{
           
            $this->loader['winner_msg'] = "no winner";
        }

        }
    }

      
        if($res != null){
           $this->loader['data']   = $res;   
        }
        else{
            $this->loader['data']   = "";
        }

        }
        else{
            echo "parameter missing";
        }
       
        $this->isAuthorised();
        $this->loader['pageview']= "ChallengeMembers";   
        $this->load->view('common/template',$this->loader);
    }
}