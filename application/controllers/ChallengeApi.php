<?php

class ChallengeApi extends CI_Controller{


    function __construct()
    {
        parent::__construct();
        $this->load->model('ChallengeApiModel');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('', ''); 
    }

    // create challenge
    function createChallenge(){                                                             

        $this->output->set_content_type('application/json');

        $userid                     = $this->input->post('userid');
        $challengetitle             = $this->input->post('challenge_title');
        $challengelinecount         = $this->input->post('linecount');
        $challenge_content          = $this->input->post('challenge_content');
        $challenge_type             = $this->input->post('challenge_type');            // type 1 or type 2
        $challenge_sub_type         = $this->input->post('challenge_posts_sub_type');  // type 1 ( 1 or 2 )    
        $challenge_created          = $this->input->post('challenge_created_date');
        $challenge_exp_date         = $this->input->post('challenge_expired_date');
        $challenge_exp_time         = $this->input->post('challenge_expired_time');
        $challenge_media_type       = $this->input->post('challenge_media_type');
        $challenge_exp_media_type   = $this->input->post('challenge_exp_media_type');
        $user_array                 = $this->input->post('userlist');
       
        
        $this->form_validation->set_rules("userid","userid","required");
        $this->form_validation->set_rules("challenge_title","challenge_title","required");
        //$this->form_validation->set_rules("challenge_content","challenge_content","required");
        $this->form_validation->set_rules("challenge_type","challenge_type","required");
        $this->form_validation->set_rules("challenge_created_date","challenge_created_date","required");
        $this->form_validation->set_rules("challenge_expired_date","challenge_expired_date","required");
        $this->form_validation->set_rules("challenge_expired_time","challenge_expired_time","required");
        $this->form_validation->set_rules("challenge_media_type","challenge_media_type","required");

       
         if($challengelinecount == NULL){
            $challengelinecount = 0;
        }



        if($this->form_validation->run() == TRUE){
    
            switch($challenge_type){
                 
                case 1:
                
              
                $insert_challenge =        array('challenge_user_id'             => $userid,
                                                'challenge_title'                => $challengetitle,
                                                'challenge_content'              => $challenge_content,
                                                'challenge_type'                 => $challenge_type,
                                                'challenge_created_date'         => $challenge_created,
                                                'challenge_expired_date'         => $challenge_exp_date,
                                                'challenge_expired_time'         => $challenge_exp_time,
                                                'challenge_media_type'           => $challenge_media_type,
                                                'challenge_posts_sub_type'       => $challenge_sub_type,
                                                'challenge_line_count'           => $challengelinecount
                                               
                                              );

                    $result_id = $this->ChallengeApiModel->createChallenge($insert_challenge);
                    
                    if($result_id){

                        if($user_array != null){

                         
                            for($i = 0 ; $i < sizeof($user_array); $i++){

                                $res        = $this->ChallengeApiModel->inviteMembers($user_array[$i] , $result_id);
                            
                            }

                        }

                    switch($challenge_media_type){
                       
                       
                        // IMAGE ONLY  (type 1)
                        
                        case 1:
                        $challenge_posts_title      = $this->input->post('challenge_post_title');
                        

                        

                        if(isset($_FILES['challenge_post'])){   
                                    
                            $errors_thumb    = array();
                            $file_name_thumb = $_FILES['challenge_post']['name'];
                            $file_size_thumb = $_FILES['challenge_post']['size'];
                            $file_tmp_thumb  = $_FILES['challenge_post']['tmp_name'];
                            $file_type_thumb = $_FILES['challenge_post']['type'];
                            $tmp_thumb       = explode('.', $file_name_thumb);
                            $file_ext_thumb  = end($tmp_thumb);

                    
                            $thumb_name = "challenge_post-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                            
                            $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                            
                            if(in_array($file_ext_thumb,$expensions_thumb)=== false){

                            $errors_thumb[] = "extension not allowed, please choose a JPEG or PNG file.";
                            
                        }
                            
                            
                            
                            if(empty($errors_thumb)==true){
                           
                            move_uploaded_file($file_tmp_thumb,"assets/images/challenge/type1/".$thumb_name);
                            $challenge_image = $thumb_name;

                            $insert_challenge_post =   
                            array( 'challenge_posts_challenge_id'       =>  $result_id,
                            'challenge_posts_title'                     =>  $challenge_posts_title,
                            'challenge_posts_sub_post'                  =>  $challenge_image,
                            'challenge_posts_user_id'                   =>  $userid
                           
                            
                           
                            );
                            $insert_challenge_posts = $this->ChallengeApiModel->uploadChallengeposts($insert_challenge_post);
            
                           
                            
                            }else{
                              
                                $challenge_image = "null";
                            }
                        }

                        break;

                        case 2:
                        $challenge_posts_title = $this->input->post('challenge_post_title');
                        if(isset($_FILES['challenge_video_thumbnail'])){

                            $errors_thumb    = array();
                            $file_name_thumb = $_FILES['challenge_video_thumbnail']['name'];
                            $file_size_thumb = $_FILES['challenge_video_thumbnail']['size'];
                            $file_tmp_thumb  = $_FILES['challenge_video_thumbnail']['tmp_name'];
                            $file_type_thumb = $_FILES['challenge_video_thumbnail']['type'];
                            $tmp_thumb       = explode('.', $file_name_thumb);
                            $file_ext_thumb  = end($tmp_thumb);

                    
                            $thumb_name = "challenge_thumb-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                            
                            $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                            
                            if(in_array($file_ext_thumb,$expensions_thumb)=== false){

                            $errors_thumb[]="extension not allowed, please choose a JPEG or PNG file.";
                            
                        }
                            
                            
                            
                            if(empty($errors_thumb)==true){
                           
                            move_uploaded_file($file_tmp_thumb,"assets/images/challenge/thumb/".$thumb_name);
                            $challenge_video_thumb = $thumb_name;
                            
                            
                            
                            }else{
                              
                                $challenge_video_thumb = "null";
                            }
                        }

                    // challenge video upload
                                                    
                    if(isset($_FILES['challenge_post'])){

                        $errors    = array();
                        $file_name = $_FILES['challenge_post']['name'];
                        $file_size = $_FILES['challenge_post']['size'];
                        $file_tmp  = $_FILES['challenge_post']['tmp_name'];
                        $file_type = $_FILES['challenge_post']['type'];
                        $tmp       = explode('.', $file_name);
                        $file_ext  = end($tmp);


                        $post_name = "challenge-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;
                        
                        $expensions = array("jpeg","jpg","png","mp4","3gp");
                        
                        if(in_array($file_ext,$expensions)=== false){

                        $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                            
                        }
                            
                            if(empty($errors)==true){

                                move_uploaded_file($file_tmp,"assets/images/challenge/type1/".$post_name);
                                $challenge_post = $post_name;

                                $insert_challenge_post = 
                                
                                array( 'challenge_posts_challenge_id'       =>  $result_id,
                                'challenge_posts_title'                     =>  $challenge_posts_title,
                                'challenge_posts_sub_post'                  =>  $challenge_post,
                                'challenge_posts_video_thumbnail'           =>  $challenge_video_thumb,
                                'challenge_posts_user_id'                   =>  $userid                      
                                );
                            
                                $insert_challenge_posts = $this->ChallengeApiModel->uploadChallengeposts($insert_challenge_post);


                            }
                            else{
                                $challenge_post = "null";
                            }
                        }



                    }

                    if($challenge_exp_media_type != null){
                        
                        switch($challenge_exp_media_type){

                            case 1:
                            if(isset($_FILES['challenge_exp_video_thumbnail'])){

                                $errors_thumb    = array();
                                $file_name_thumb = $_FILES['challenge_exp_video_thumbnail']['name'];
                                $file_size_thumb = $_FILES['challenge_exp_video_thumbnail']['size'];
                                $file_tmp_thumb  = $_FILES['challenge_exp_video_thumbnail']['tmp_name'];
                                $file_type_thumb = $_FILES['challenge_exp_video_thumbnail']['type'];
                                $tmp_thumb       = explode('.', $file_name_thumb);
                                $file_ext_thumb  = end($tmp_thumb);
    
                        
                                $thumb_name = "challenge_thumb-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                                
                                $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                                
                                if(in_array($file_ext_thumb,$expensions_thumb)=== false){
    
                                $errors_thumb[]="extension not allowed, please choose a JPEG or PNG file.";
                                
                            }
                                
                                
                                
                                if(empty($errors_thumb)==true){
                               
                                move_uploaded_file($file_tmp_thumb,"assets/images/challenge/thumb/".$thumb_name);
                                $challenge_video_thumb = $thumb_name;
                                
                                
                                
                                }else{
                                  
                                    $challenge_video_thumb = "null";
                                }
                            }





                            if(isset($_FILES['challenge_exp_post'])){

                                $errors    = array();
                                $file_name = $_FILES['challenge_exp_post']['name'];
                                $file_size = $_FILES['challenge_exp_post']['size'];
                                $file_tmp  = $_FILES['challenge_exp_post']['tmp_name'];
                                $file_type = $_FILES['challenge_exp_post']['type'];
                                $tmp       = explode('.', $file_name);
                                $file_ext  = end($tmp);
        
        
                                $post_name = "challenge-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;
                                
                                $expensions = array("jpeg","jpg","png","mp4","3gp");
                                
                                if(in_array($file_ext,$expensions)=== false){
        
                                $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                                    
                                }
                                    
                                    if(empty($errors)==true){
        
                                        move_uploaded_file($file_tmp,"assets/images/challenge/type1/".$post_name);
                                        $challenge_post = $post_name;
        
                                        $insert_challenge_post = 
                                        
                                        array( 'challenge_posts_challenge_id'       =>  $result_id,
                                        'challenge_posts_title'                     =>  $challenge_posts_title,
                                        'challenge_posts_sub_post'                  =>  $challenge_post,
                                        'challenge_posts_video_thumbnail'           =>  $challenge_video_thumb,
                                        'challenge_posts_exp_media_type'            =>  1,
                                        'challenge_posts_type'                      =>  1,
                                        'challenge_posts_user_id'                   =>  $userid              
                                        );
                                    
                                        $insert_challenge_posts = $this->ChallengeApiModel->uploadChallengeposts($insert_challenge_post);
        
        
                                    }
                                    else{
                                        $challenge_post = "null";
                                    }
                                }

                                break;


                                case 0:

                                if(isset($_FILES['challenge_exp_post'])){   
                                    
                                    $errors_thumb    = array();
                                    $file_name_thumb = $_FILES['challenge_exp_post']['name'];
                                    $file_size_thumb = $_FILES['challenge_exp_post']['size'];
                                    $file_tmp_thumb  = $_FILES['challenge_exp_post']['tmp_name'];
                                    $file_type_thumb = $_FILES['challenge_exp_post']['type'];
                                    $tmp_thumb       = explode('.', $file_name_thumb);
                                    $file_ext_thumb  = end($tmp_thumb);
        
                            
                                    $thumb_name = "challenge_post-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                                    
                                    $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                                    
                                    if(in_array($file_ext_thumb,$expensions_thumb)=== false){
        
                                    $errors_thumb[] = "extension not allowed, please choose a JPEG or PNG file.";
                                    
                                }
                                    
                                    
                                    
                                    if(empty($errors_thumb)==true){
                                   
                                    move_uploaded_file($file_tmp_thumb,"assets/images/challenge/type1/".$thumb_name);
                                    $challenge_image = $thumb_name;
        
                                    $insert_challenge_post =   
                                    array( 'challenge_posts_challenge_id'       =>  $result_id,
                                    'challenge_posts_title'                     =>  $challenge_posts_title,
                                    'challenge_posts_sub_post'                  =>  $challenge_image,
                                    'challenge_posts_type'                      =>  1,
                                    'challenge_posts_user_id'                   =>  $userid
                                   
                                   
                                    
                                   
                                    );
                                    $insert_challenge_posts = $this->ChallengeApiModel->uploadChallengeposts($insert_challenge_post);
                    
                                   
                                    
                                    }else{
                                      
                                        $challenge_image = "null";
                                    }
                                }

                                break;
                            
                        }
                }
                    $this->output->set_output(json_encode(['message' => "successfully saved ", 'status' => "true"]));
                    
                }
                else{
                    $this->output->set_output(json_encode(['message' => "failed to upload", 'status' => "false"]));
                    
                }

                                              

                    break;
                              

            
                case 2:

                $insert_challenge =        array('challenge_user_id'             => $userid,
                                                'challenge_title'                => $challengetitle,
                                                'challenge_content'              => $challenge_content,
                                                'challenge_type'                 => $challenge_type,
                                                'challenge_created_date'         => $challenge_created,
                                                'challenge_expired_date'         => $challenge_exp_date,
                                                'challenge_expired_time'         => $challenge_exp_time,
                                                'challenge_media_type'           => $challenge_media_type,
                                                'challenge_line_count'           => $challengelinecount
                                               
                                              );


                $result_id = $this->ChallengeApiModel->createChallenge($insert_challenge);


                if($result_id > 0) {

            /*******************************   IMAGE ONLY     ***********************************/
                   

                            switch($challenge_media_type){

                                case 1:
                                
                                    $challenge_posts_title = $this->input->post('challenge_post_title');
                                    for($i=0;$i<sizeof($challenge_posts_title);$i++){
    
                                   
                                   // image only
                                 
                                if(isset($_FILES['challenge_post'])){   
                                    
                                    $errors_thumb    = array();
                                    $file_name_thumb = $_FILES['challenge_post']['name'][$i];
                                    $file_size_thumb = $_FILES['challenge_post']['size'][$i];
                                    $file_tmp_thumb  = $_FILES['challenge_post']['tmp_name'][$i];
                                    $file_type_thumb = $_FILES['challenge_post']['type'][$i];
                                    $tmp_thumb       = explode('.', $file_name_thumb);
                                    $file_ext_thumb  = end($tmp_thumb);
    
                            
                                    $thumb_name = "challenge_post-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                                    
                                    $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                                    
                                    if(in_array($file_ext_thumb,$expensions_thumb)=== false){
    
                                    $errors_thumb[]="extension not allowed, please choose a JPEG or PNG file.";
                                    
                                }
                                    
                                    
                                    
                                    if(empty($errors_thumb)==true){
                                   
                                    move_uploaded_file($file_tmp_thumb,"assets/images/challenge/type2/".$thumb_name);
                                    $challenge_image = $thumb_name;

                                    $insert_challenge_post =   
                                    array( 'challenge_posts_challenge_id'       =>  $result_id,
                                    'challenge_posts_title'                     =>  $challenge_posts_title[$i],
                                    'challenge_posts_sub_post'                  =>  $challenge_image,
                                    'challenge_posts_user_id'                   =>  $userid
                                   
                                    );
                                    $insert_challenge_posts = $this->ChallengeApiModel->uploadChallengeposts($insert_challenge_post);
                    
                                   
                                    
                                    }else{
                                      
                                        $challenge_image = "null";
                                    }
                                }
                            }
                            
                            break;

         /*******************************   IMAGE ONLY ENDS HERE     ***********************************/

                               
          /*******************************   VIDEO  ONLY     ***********************************/

                           case 2:
                          
                            $challenge_posts_title = $this->input->post('challenge_post_title');
                            for($i=0;$i<sizeof($challenge_posts_title);$i++){

                                
                               
                               // video thumbnail
                             
                            if(isset($_FILES['challenge_video_thumbnail'])){   
                                $errors_thumb    = array();
                                $file_name_thumb = $_FILES['challenge_video_thumbnail']['name'][$i];
                                $file_size_thumb = $_FILES['challenge_video_thumbnail']['size'][$i];
                                $file_tmp_thumb  = $_FILES['challenge_video_thumbnail']['tmp_name'][$i];
                                $file_type_thumb = $_FILES['challenge_video_thumbnail']['type'][$i];
                                $tmp_thumb       = explode('.', $file_name_thumb);
                                $file_ext_thumb  = end($tmp_thumb);

                        
                                $thumb_name = "challenge_thumb-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                                
                                $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                                
                                if(in_array($file_ext_thumb,$expensions_thumb)=== false){

                                $errors_thumb[]="extension not allowed, please choose a JPEG or PNG file.";
                                
                            }
                                
                                
                                
                                if(empty($errors_thumb)==true){
                               
                                move_uploaded_file($file_tmp_thumb,"assets/images/challenge/thumb/".$thumb_name);
                                $challenge_video_thumb = $thumb_name;
                                
                                
                                
                                }else{
                                  
                                    $challenge_video_thumb = "null";
                                }
                            }

                            
                           
                            // challenge video upload
                                   
                            if(isset($_FILES['challenge_post'])){
                            $errors    = array();
                            $file_name = $_FILES['challenge_post']['name'][$i];
                            $file_size = $_FILES['challenge_post']['size'][$i];
                            $file_tmp  = $_FILES['challenge_post']['tmp_name'][$i];
                            $file_type = $_FILES['challenge_post']['type'][$i];
                            $tmp       = explode('.', $file_name);
                            $file_ext  = end($tmp);

                    
                            $post_name = "challenge-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;
                            
                            $expensions = array("jpeg","jpg","png","mp4","3gp");
                            
                            if(in_array($file_ext,$expensions)=== false){

                            $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                                
                            }
                                
                                if(empty($errors)==true){

                                    move_uploaded_file($file_tmp,"assets/images/challenge/type2/".$post_name);
                                    $challenge_post = $post_name;

                                    $insert_challenge_post = 
                                    
                                    array( 'challenge_posts_challenge_id'       =>  $result_id,
                                    'challenge_posts_title'                     =>  $challenge_posts_title[$i],
                                    'challenge_posts_sub_post'                  =>  $challenge_post,
                                    'challenge_posts_video_thumbnail'           =>  $challenge_video_thumb,
                                    'challenge_posts_user_id'                   =>  $userid                      
                                    );
                                
                                    $insert_challenge_posts = $this->ChallengeApiModel->uploadChallengeposts($insert_challenge_post);
                    

                                }
                                else{
                                    $challenge_post = "null";
                                }
                            }
                               
                            }

                            break;

                    /*******************************   VIDEO ONLY  ENDS HERE    ***********************************/
                            
                        }
                 
                       
                    $this->output->set_output(json_encode(['message' => "successfully saved ", 'status' => "true"]));
                        
              
        }
                else{
                    $this->output->set_output(json_encode(['message' => "failed to upload", 'status' => "false"]));
                }
                break;
            }
            
        }
        else{
            $this->output->set_output(json_encode(['message'=>validation_errors(),'status'=>'false']));
        }
        }

    //     // get challenge list
    //     function getChallengelist(){
           

           

           


    //         // $now        = new DateTime(date("Y-m-d H:i:s"));
    //         // echo $now;
    //         // exit();
    //         // $exp_date   = strtotime('25-11-2018 11:04:45 AM');new DateTime('28-11-2018 12:34:32 AM');
    //         // $interval   = $now->diff($exp_date);
    //         // echo $interval->format('%d')." Days ".$interval->format('%h')." Hours ".$interval->format('%i')." Minutes " .$interval->format('%s');
    //         // exit();
            
           

    //         // $now = time(); // or your date as well
            
    //         // $your_date = strtotime("04-12-2018");
    //         // $datediff  = $your_date - $now;
            
    //         // echo  round($datediff / (60 * 60 * 24));

    //         // //echo $hourdiff = round(($your_date - $now)/3600, 1);
    //         // exit();





    //     $this->output->set_content_type('application/json');

    //     $zone = getallheaders();
        
    //     if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";
    //     $current_date = date('d-m-Y h:m:s A');
    //     $GMT  = new DateTimeZone("Asia/Kolkata");
    //     $userid = !empty($this->input->post('userid')) ? $this->input->post('userid') : 1;

    //         //    $date = new DateTime($current_date, $GMT);
              
    //         //    $date->setTimezone(new DateTimeZone($zone['timezone']));                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
    //         //    $d = $date->format('d-m-Y h:m:s A');
               
    //         //    print_r($d);
    //         //    exit();
             




    //     $res    = $this->ChallengeApiModel->getChallengelist();

            

    //     if($res != null){
            
    //         foreach($res as $value => $key){
                
               
    //             // time validity section

    //             $now            = new DateTime($zone['timezone']);

    //             $expdate = $res[$value]['challenge_expired_date']." ".$res[$value]['challenge_expired_time'];
                

    //             $expired_date   = date("Y-m-d h:i A ", strtotime($expdate));
                
               
    //             $date = date('Y-m-d h:i A');
                
    //             $current_date = strtotime($date);
    //             $exp_date     = strtotime($expdate);
    //             $diff   = $exp_date - $current_date;
    //             $res[$value]['challenge_status']    = $diff > 0 ? 1 : 0;
               
    //             $expired_date   = new DateTime($expired_date , $GMT);    
                
    //             $interval       = $now->diff($expired_date); 
    //             $isPolled       = $this->ChallengeApiModel->isChallengepolled($res[$value]['challenge_id'] , $userid);
    //             $res[$value]['ispolled']    = $isPolled > 0 ? 1 : 0;
               
    //             // print $date;
    //             // print "<pre>";
    //             // print $expdate;
    //             // print "<pre>";
    //             // print $interval->format('%d')." Days ".$interval->format('%h')." Hours ".$interval->format('%i')." Minutes " .$interval->format('%s');
    //             // exit();
                
    //             if($diff > 0){
               
    //             $res[$value]['validity_day']    = $interval->format('%d');
    //             $res[$value]['validity_hour']   = $interval->format('%h');
    //             $res[$value]['validity_minute'] = $interval->format('%i');
    //             $res[$value]['validity_second'] = $interval->format('%s');
                
    //             }
    //             else{
    //             $res[$value]['validity_day']    = 0;
    //             $res[$value]['validity_hour']   = 0;
    //             $res[$value]['validity_minute'] = 0;
    //             $res[$value]['validity_second'] = 0;
    //             }
             

    //             // time validtity ends here


    //             if($res[$value]['challenge_posts_sub_type'] == 1) {

    //                 $explenatory_video                      = $this->ChallengeApiModel->getExplanetoryvideo($res[$value]['challenge_id']);
    //                 if($explenatory_video){
    //                 $res[$value]['explanetory_video']       = base_url()."assets/images/challenge/type1/".$explenatory_video[0]['challenge_posts_sub_post']; 
    //                 $res[$value]['explanetory_video_thumb'] = base_url()."assets/images/challenge/thumb/".$explenatory_video[0]['challenge_posts_video_thumbnail']; 
    //                 $res[$value]['challenge_posts_exp_media_type'] = $explenatory_video[0]['challenge_posts_exp_media_type']; 
                   
    //                 }
    //             }

    //             $res[$value]['pollCount']               = $this->ChallengeApiModel->challengePollcount($res[$value]['challenge_id']);
    //             $res[$value]['commentCount']            = $this->ChallengeApiModel->challengeCommentcount($res[$value]['challenge_id']);
    //             $res[$value]['challenge_posts_post']    = $this->ChallengeApiModel->getChallengeposts($res[$value]['challenge_id']);
    //             $res[$value]['comment']                 = $this->ChallengeApiModel->getChallengecomments($res[$value]['challenge_id']);
                
    //             $res[$value]['totalpollscount']         = $this->ChallengeApiModel->totalPostsCount($res[$value]['challenge_id']);

               
              
                
    //             foreach($res[$value]['comment'] as $pic => $key_pic){
    //                 $res[$value]['comment'][$pic]['users_photo'] = base_url()."assets/images/users/".$res[$value]['comment'][$pic]['users_photo'];
    //             }
               
 
    //             $res[$value]['users_photo']             = base_url()."assets/images/users/".$res[$value]['users_photo'];
                   

    //             foreach($res[$value]['challenge_posts_post'] as $values => $keys){
                    
    //                 $res[$value]['challenge_posts_post'][$values]['users_photo']                        = base_url()."assets/images/users/".$res[$value]['challenge_posts_post'][$values]['users_photo']; 
    //                 $res[$value]['challenge_posts_post'][$values]['pollCount']                          = $this->ChallengeApiModel->challengepostPollcount($res[$value]['challenge_id'],$res[$value]['challenge_posts_post'][$values]['challenge_posts_id']);
    //                 if($res[$value]['totalpollscount'] > 0) {
                   
    //                 $res[$value]['challenge_posts_post'][$values]['pollPercent']                        = intval(($res[$value]['challenge_posts_post'][$values]['pollCount'] * 100 ) / $res[$value]['totalpollscount']);
                    
    //                 }
    //                 else{
    //                     $res[$value]['challenge_posts_post'][$values]['pollPercent'] = 0;
    //                 }
    //                 $res[$value]['challenge_posts_post'][$values]['challenge_media_type']               = $res[$value]['challenge_media_type'];
                    
    //                 $isPolledpost       = $this->ChallengeApiModel->isChallengepostpolled($res[$value]['challenge_posts_post'][$values]['challenge_posts_id'] , $userid);
                   
    //                 $res[$value]['challenge_posts_post'][$values]['isPolled']                           = $isPolledpost > 0 ? 1 : 0;

    //                 if($res[$value]['challenge_type'] == 1){

    //                     $res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post']       = base_url()."assets/images/challenge/type1/".$res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                       
    //                 }
    //                 else{
    //                     $res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post']       = base_url()."assets/images/challenge/type2/".$res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                       
    //                 }
    //                     $res[$value]['challenge_posts_post'][$values]['challenge_posts_video_thumbnail']    = base_url()."assets/images/challenge/thumb/".$res[$value]['challenge_posts_post'][$values]['challenge_posts_video_thumbnail'];
                    
    //                 //$res[$value]['challenge_posts_post'][$values]['users_photo']                = base_url()."assets/images/users/".$res[$value]['challenge_posts_post'][$values]['users_photo'];
            
    //             }
    //         }

         
    //         $this->output->set_output(json_encode(['value' => $res ,'status'=> true])); 
    //     }
       

    //     else{
            
    //     $this->output->set_output(json_encode(['status'=> false , 'value' => 'no value found'])); 
    // }
   

    // }


  // get challenge list
  function getChallengelist(){
           

    
            $this->output->set_content_type('application/json');
    
            $zone = getallheaders();
            
            if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";
            
            $dt = new DateTime();
                
            $current_date = $dt->format('d-m-Y h:i A');
           
            $GMT  = new DateTimeZone("GMT");
            $date = new DateTime($current_date, $GMT );
            $date->setTimezone(new DateTimeZone($zone['timezone']));
            $date =   $date->format('Y-m-d h:i A');
            
            $userid = !empty($this->input->post('userid')) ? $this->input->post('userid') : 1;
            
            
            $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
            $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
            $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
    
    
            $res    = $this->ChallengeApiModel->getChallengelist($country , $state , $district);
    
                
            $j = 0;

            if($res != null){
                
                foreach($res as $value => $key){
                    
                   
                    // time validity section
    
                    $now            = new DateTime($zone['timezone']);
                   
                 
    
                    $expdate = $res[$value]['challenge_expired_date']." ".$res[$value]['challenge_expired_time'];
                    
                   
                    $expired_date   = date("Y-m-d H:i:s ", strtotime($expdate));
                    
                    
                   
                    $res[$value]['cdate'] = $date;
                   
                    $current_date = strtotime($date);
                    $exp_date     = strtotime($expdate);
                  
                  
                    $diff   = $exp_date - $current_date;
                    $res[$value]['diff'] = $diff;
                    
                    $res[$value]['challenge_status']    = $diff > 0 ? 1 : 0;
                   
                    $expired_date   = new DateTime($expired_date);    
                    
                     
               
                    
                    $now_new     = $now->format('Y-m-d H:i:s');
                    $expired_new = $expired_date->format('Y-m-d H:i:s');
                    
                    $now_new_date       = new DateTime($now_new);
                    $expired_new_date   = new DateTime($expired_new);
                  
                    
                  
                    $interval       = $now_new_date->diff($expired_new_date); 
                    
                   
                    $isPolled       = $this->ChallengeApiModel->isChallengepolled($res[$value]['challenge_id'] , $userid);
                    $res[$value]['ispolled']    = $isPolled > 0 ? 1 : 0;
                   
                    // print $date;
                    // print "<pre>";
                    // print $expdate;
                    // print "<pre>";
                    // print $interval->format('%d')." Days ".$interval->format('%h')." Hours ".$interval->format('%i')." Minutes " .$interval->format('%s');
                    // exit();
                    
                    if($diff > 0){
                   
                    $res[$value]['validity_day']     = $interval->format('%d');
                    $res[$value]['validity_hour']    = $interval->format('%h');
                    $res[$value]['validity_minute']  = $interval->format('%i');
                    $res[$value]['validity_second']  = $interval->format('%s');
                    $res[$value]['challenge_winner'] = [];

                  
                    }

                    else{

                    $res[$value]['validity_day']     = 0;
                    $res[$value]['validity_hour']    = 0;
                    $res[$value]['validity_minute']  = 0;
                    $res[$value]['validity_second']  = 0;

                    $res[$value]['challenge_winner']        = $this->ChallengeApiModel->getWinner($res[$value]['challenge_id']);
                  
                       
                    
                    if($res[$value]['challenge_winner']){
                   
                        foreach($res[$value]['challenge_winner'] as $key_1 => $value_1){

                           // $res[$value]['challenge_winner']['']
                           if($res[$value]['challenge_winner'][$key_1]['users_login_type'] == 0){

                           
                           $res[$value]['challenge_winner'][$key_1]['users_photo']             = base_url()."assets/images/users/".$res[$value]['challenge_winner'][$key_1]['users_photo'];
                        
                           }
                        if($res[$value]['challenge_type'] == 1){
    
                            $res[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post']       = base_url()."assets/images/challenge/type1/".$res[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post'];
                           
                        }
                        else{
                            $res[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post']       = base_url()."assets/images/challenge/type2/".$res[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post'];
                           
                        }

                        $res[$value]['challenge_winner'][$key_1]['challenge_media_type'] = $res[$value]['challenge_media_type'];
                        
                        $res[$value]['challenge_winner'][$key_1]['challenge_posts_video_thumbnail'] = base_url()."assets/images/challenge/thumb/".$res[$value]['challenge_winner'][$key_1]['challenge_posts_video_thumbnail'];
                    }
                }

                   
                    }
                 
    
                    // time validtity ends here
    
    
                    if($res[$value]['challenge_posts_sub_type'] == 1) {
    
                        $explenatory_video                      = $this->ChallengeApiModel->getExplanetoryvideo($res[$value]['challenge_id']);
                        if($explenatory_video){
                        $res[$value]['explanetory_video']       = base_url()."assets/images/challenge/type1/".$explenatory_video[0]['challenge_posts_sub_post']; 
                        $res[$value]['explanetory_video_thumb'] = base_url()."assets/images/challenge/thumb/".$explenatory_video[0]['challenge_posts_video_thumbnail']; 
                        $res[$value]['challenge_posts_exp_media_type'] = $explenatory_video[0]['challenge_posts_exp_media_type']; 
                       
                        }
                    }
    
                    $res[$value]['pollCount']               = $this->ChallengeApiModel->challengePollcount($res[$value]['challenge_id']);
                    $res[$value]['commentCount']            = $this->ChallengeApiModel->challengeCommentcount($res[$value]['challenge_id']);
                    $res[$value]['challenge_posts_post']    = $this->ChallengeApiModel->getChallengeposts($res[$value]['challenge_id']);
                    $res[$value]['comment']                 = $this->ChallengeApiModel->getChallengecomments($res[$value]['challenge_id']);
                    
                    $res[$value]['totalpollscount']         = $this->ChallengeApiModel->totalPostsCount($res[$value]['challenge_id']);
                   
                    
                    foreach($res[$value]['comment'] as $pic => $key_pic){
                        $res[$value]['comment'][$pic]['users_photo'] = base_url()."assets/images/users/".$res[$value]['comment'][$pic]['users_photo'];
                    }
                   
                    if($res[$value]['users_login_type'] == 0 ){
                    $res[$value]['users_photo']             = base_url()."assets/images/users/".$res[$value]['users_photo'];
                       
                    }
    
                    foreach($res[$value]['challenge_posts_post'] as $values => $keys){
                        $res[$value]['challenge_posts_post'][$values]['parent_position']    = $j;
                        $res[$value]['challenge_posts_post'][$values]['users_photo']                        = base_url()."assets/images/users/".$res[$value]['challenge_posts_post'][$values]['users_photo']; 
                        $res[$value]['challenge_posts_post'][$values]['pollCount']                          = $this->ChallengeApiModel->challengepostPollcount($res[$value]['challenge_id'],$res[$value]['challenge_posts_post'][$values]['challenge_posts_id']);
                        if($res[$value]['totalpollscount'] > 0) {
                       
                        $res[$value]['challenge_posts_post'][$values]['pollPercent']                        = intval(($res[$value]['challenge_posts_post'][$values]['pollCount'] * 100 ) / $res[$value]['totalpollscount']);
                       
                        }
                        else{
                            $res[$value]['challenge_posts_post'][$values]['pollPercent']                    = 0;
                        }
                        $res[$value]['challenge_posts_post'][$values]['challenge_media_type']               = $res[$value]['challenge_media_type'];
                        
                        $isPolledpost       = $this->ChallengeApiModel->isChallengepostpolled($res[$value]['challenge_posts_post'][$values]['challenge_posts_id'] , $userid);
                       
                        $res[$value]['challenge_posts_post'][$values]['isPolled']                           = $isPolledpost > 0 ? 1 : 0;
    
                        if($res[$value]['challenge_type'] == 1){
    
                            $res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post']       = base_url()."assets/images/challenge/type1/".$res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                           
                        }
                        else{
                            $res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post']       = base_url()."assets/images/challenge/type2/".$res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                           
                        }
                            $res[$value]['challenge_posts_post'][$values]['challenge_posts_video_thumbnail']    = base_url()."assets/images/challenge/thumb/".$res[$value]['challenge_posts_post'][$values]['challenge_posts_video_thumbnail'];
                        
                        //$res[$value]['challenge_posts_post'][$values]['users_photo']                = base_url()."assets/images/users/".$res[$value]['challenge_posts_post'][$values]['users_photo'];
                
                    }

                    $j++;
                }
    
             
                $this->output->set_output(json_encode(['value' => $res ,'status'=> true])); 
            }
           
    
            else{
                
            $this->output->set_output(json_encode(['status'=> false , 'value' => 'no value found'])); 
        }
       
    
        }


    // challenge comment
    function commentChallenge(){

        $this->output->set_content_type('application/json');
        
        $userid      = $this->input->post('userid');
        $cid         = $this->input->post('challengeid');
        $comment     = $this->input->post('comment');
        $cuserid     = $this->input->post('challenge_user_id');

        if(!empty($userid && $cid && $comment)){

        $res = $this->ChallengeApiModel->commentChallenge($userid , $cid , $comment , $cuserid);
        if($res > 0){

            $getcomment = $this->ChallengeApiModel->getLastcomment($res);
            foreach($getcomment as $val => $ke){
                $getcomment[$val]['users_photo'] = base_url()."assets/images/users/".$getcomment[$val]['users_photo'];
            }
            $this->output->set_output(json_encode(['message' => 'successfully commented','comment' => $getcomment ,'status'=> true])); 
        }
        else{

            $this->output->set_output(json_encode(['message' => "operation failed" ,'status'=> false])); 
        }
    }
    else{
        $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> true])); 
    }
        
    }



    // comment challenge posts
    function commentChallengePosts(){

        $this->output->set_content_type('application/json');
        
        $userid      = $this->input->post('userid');
        $cid         = $this->input->post('challengeid');
        $comment     = $this->input->post('comment');
        $cuserid     = $this->input->post('challenge_user_id');
        $cpid        = $this->input->post('postid');

        if(!empty($userid && $cid && $comment && $cpid)){

        $res = $this->ChallengeApiModel->commentChallengePosts($userid , $cid , $comment , $cuserid , $cpid);
        if($res > 0){

            $getcomment = $this->ChallengeApiModel->getLastcomment($res);
            foreach($getcomment as $val => $ke){
                $getcomment[$val]['users_photo'] = base_url()."assets/images/users/".$getcomment[$val]['users_photo'];
            }
            $this->output->set_output(json_encode(['message' => 'successfully commented','comment' => $getcomment ,'status'=> true])); 
        }
        else{

            $this->output->set_output(json_encode(['message' => "operation failed" ,'status'=> false])); 
        }
    }
    else{
        $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> true])); 
    }

    }

    // poll type 2 challenge
    function pollChallenge(){

        $this->output->set_content_type('application/json');
        
        $userid      = $this->input->post('userid');
        $cid         = $this->input->post('challengeid');
        $cuserid     = $this->input->post('challenge_user_id');
        

        if(!empty($userid && $cid)){

        $res = $this->ChallengeApiModel->pollChallenge($userid , $cid ,$cuserid);

        switch ($res){

            case 2:
            $this->output->set_output(json_encode(['message' => 'successfully polled' ,'status'=> true])); 
            break;

            case 3:
            $this->output->set_output(json_encode(['message' => 'successfully unpolled' ,'status'=> true])); 
            break;

            case 0:
            $this->output->set_output(json_encode(['message' => 'operation failed' ,'status'=> false])); 
            break;
        }
       
    }
    else{
        $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> true])); 
    }
    } 

    // poll type 2 challenge
    function pollChallengeposts(){

        $this->output->set_content_type('application/json');
        
        $userid  = $this->input->post('userid');
        $cid     = $this->input->post('challengeid');
        $cpid    = $this->input->post('challengepostid');

        $type    = $this->input->post('type');    // 0 =>  normal poll , 1  => change subpost poll
        

        if(!empty($userid && $cid && $cpid)){
        
        if($type == 1){
            $res = $this->ChallengeApiModel->updatepollpostChallenge($userid , $cid , $cpid);
        }   
        else{ 
        $res = $this->ChallengeApiModel->pollpostChallenge($userid , $cid , $cpid);
        }
        switch ($res){
            
            case 2:
            $this->output->set_output(json_encode(['message' => 'successfully polled' ,'status'=> true])); 
            break;

            case 3:
            $this->output->set_output(json_encode(['message' => 'successfully unpolled' ,'status'=> true])); 
            break;

            case 0:
            $this->output->set_output(json_encode(['message' => 'operation failed' ,'status'=> false])); 
            break;

            case 4:
            $this->output->set_output(json_encode(['message' => 'successfully polled' ,'status'=> true])); 
            break;

            case 5:
            $this->output->set_output(json_encode(['message' => 'failed to update' ,'status'=> false])); 
            break;
        }
       
    }
    else{
        $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> false])); 
    }
    } 


     // poll type 2 challenge
     function unpollChallengeposts(){

        $this->output->set_content_type('application/json');
        
        $userid  = $this->input->post('userid');
        $cid     = $this->input->post('challengeid');
        $cpid    = $this->input->post('challengepostid');
        

        if(!empty($userid && $cid && $cpid)){

        $res = $this->ChallengeApiModel->unpollChallengeposts($userid , $cid , $cpid);

        switch ($res){
            
            case 2:
            $this->output->set_output(json_encode(['message' => 'successfully polled' ,'status'=> true])); 
            break;

            case 3:
            $this->output->set_output(json_encode(['message' => 'successfully unpolled' ,'status'=> true])); 
            break;

            case 0:
            $this->output->set_output(json_encode(['message' => 'operation failed' ,'status'=> false])); 
            break;
        }
       
    }
    else{
        $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> false])); 
    }
    } 

    // get comment list
    function getCommentlist(){

        $this->output->set_content_type('application/json');
        
        $challengeid  = $this->input->post('challengeid');

        if(!empty($challengeid)){

            $res = $this->ChallengeApiModel->getCommentlist($challengeid);
            if($res != null){
                
                foreach($res as $val => $key){
                    $res[$val]['users_photo'] = base_url()."assets/images/users/".$res[$val]['users_photo'];
                }
            $this->output->set_output(json_encode(['message' => 'data found' ,'value' => $res,'status'=> true])); 
            }
            else{
                $this->output->set_output(json_encode(['message' => 'no data found' ,'value' => $res,'status'=> true])); 
           
            }
        }
        else{

            $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> false])); 
        }
    }

    // get single challenge by id 
    function getChallengebyid(){

        $this->output->set_content_type('application/json');
        $challengeid  = $this->input->post('challengeid');

        if(!empty($challengeid)){

            $res = $this->ChallengeApiModel->getChallengebyid($challengeid);

            if($res != null){
                foreach($res as $value => $key){

                $now            = new DateTime();

                $expdate = $res[$value]['challenge_expired_date']." ".$res[$value]['challenge_expired_time'];
                
                              
                $expired_date   = date("Y-m-d h:i A ", strtotime($expdate));
                
               
                $date = date('Y-m-d h:i A');
                
                $current_date = strtotime($date);
                $exp_date     = strtotime($expdate);
                $diff   = $exp_date - $current_date;
                $res[$value]['challenge_status']    = $diff > 0 ? 1 : 0;
               
                $expired_date   = new DateTime($expired_date);    
                
                $interval       = $now->diff($expired_date); 
                //$interval->format('%d')." Days ".$interval->format('%h')." Hours ".$interval->format('%i')." Minutes " .$interval->format('%s');
                
                if($diff > 0){
               
                $res[$value]['validity_day']    = $interval->format('%d');
                $res[$value]['validity_hour']   = $interval->format('%h');
                $res[$value]['validity_minute'] = $interval->format('%i');
                $res[$value]['validity_second'] = $interval->format('%s');
                
                }
                else{
                $res[$value]['validity_day']    = 0;
                $res[$value]['validity_hour']   = 0;
                $res[$value]['validity_minute'] = 0;
                $res[$value]['validity_second'] = 0;
                }
             

                // time validtity ends here


                if($res[$value]['challenge_posts_sub_type'] == 1) {

                    $explenatory_video                      = $this->ChallengeApiModel->getExplanetoryvideo($res[$value]['challenge_id']);
                    if($explenatory_video){
                    $res[$value]['explanetory_video']       = base_url()."assets/images/challenge/type1/".$explenatory_video[0]['challenge_posts_sub_post']; 
                    $res[$value]['explanetory_video_thumb'] = base_url()."assets/images/challenge/thumb/".$explenatory_video[0]['challenge_posts_video_thumbnail']; 
                    $res[$value]['challenge_posts_exp_media_type'] = $explenatory_video[0]['challenge_posts_exp_media_type']; 
                   
                    }
                }

                $res[$value]['pollCount']               = $this->ChallengeApiModel->challengePollcount($res[$value]['challenge_id']);
                $res[$value]['commentCount']            = $this->ChallengeApiModel->challengeCommentcount($res[$value]['challenge_id']);
                $res[$value]['challenge_posts_post']    = $this->ChallengeApiModel->getChallengeposts($res[$value]['challenge_id']);
                $res[$value]['comment']                 = $this->ChallengeApiModel->getChallengecomments($res[$value]['challenge_id']);
                
                $res[$value]['totalpollscount']         = $this->ChallengeApiModel->totalPostsCount($res[$value]['challenge_id']);

               
              
                
                foreach($res[$value]['comment'] as $pic => $key_pic){
                    $res[$value]['comment'][$pic]['users_photo'] = base_url()."assets/images/users/".$res[$value]['comment'][$pic]['users_photo'];
                }
               
 
                $res[$value]['users_photo']             = base_url()."assets/images/users/".$res[$value]['users_photo'];
                   

                foreach($res[$value]['challenge_posts_post'] as $values => $keys){
                    
                    $res[$value]['challenge_posts_post'][$values]['users_photo']                        = base_url()."assets/images/users/".$res[$value]['challenge_posts_post'][$values]['users_photo']; 
                    $res[$value]['challenge_posts_post'][$values]['pollCount']                          = $this->ChallengeApiModel->challengepostPollcount($res[$value]['challenge_id'],$res[$value]['challenge_posts_post'][$values]['challenge_posts_id']);
                    if($res[$value]['totalpollscount'] > 0) {
                   
                    $res[$value]['challenge_posts_post'][$values]['pollPercent']                        = intval(($res[$value]['challenge_posts_post'][$values]['pollCount'] * 100 ) / $res[$value]['totalpollscount']);
                    
                    }
                    else{
                        $res[$value]['challenge_posts_post'][$values]['pollPercent'] = 0;
                    }
                    $res[$value]['challenge_posts_post'][$values]['challenge_media_type']               = $res[$value]['challenge_media_type'];
                    
                    

                    if($res[$value]['challenge_type'] == 1){

                        $res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post']       = base_url()."assets/images/challenge/type1/".$res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                       
                    }
                    else{
                        $res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post']       = base_url()."assets/images/challenge/type2/".$res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                       
                    }
                        $res[$value]['challenge_posts_post'][$values]['challenge_posts_video_thumbnail']    = base_url()."assets/images/challenge/thumb/".$res[$value]['challenge_posts_post'][$values]['challenge_posts_video_thumbnail'];
                    
                    //$res[$value]['challenge_posts_post'][$values]['users_photo']                = base_url()."assets/images/users/".$res[$value]['challenge_posts_post'][$values]['users_photo'];
            
                }
            


            }


                $this->output->set_output(json_encode(['message' => $res ,'status'=> true])); 

            }
            else{
                $this->output->set_output(json_encode(['message' => 'no data found' ,'status'=> false])); 
            }
        }
        else{
            $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> false])); 
        }
    }
   
    // update challenge posts
    function updateChallengePosts(){

        $this->output->set_content_type('application/json');

        $userid                 = $this->input->post('userid'); 
        $challengeid            = $this->input->post('challengeid');
        $challenge_posts_title  = $this->input->post('challenge_post_title');
        $challenge_media_type   = $this->input->post('challenge_media_type');


        if(!empty($userid && $challengeid && $challenge_media_type)){
            
            $user_array     = $this->input->post('userlist');

            if($user_array != null){

                         
                for($i = 0 ; $i < sizeof($user_array); $i++){

                    $res        = $this->ChallengeApiModel->inviteMembers($user_array[$i] , $challengeid);
                    if($res == 1){
                        $msg    = "successfully saved";
                        $status = 'true';
                    }
                    else{

                    }
                }

            }

            switch($challenge_media_type){
                       
                // IMAGE ONLY  (type 1)
                
                case 1:
               
              
                if(isset($_FILES['challenge_post'])){   
                            
                    $errors_thumb    = array();
                    $file_name_thumb = $_FILES['challenge_post']['name'];
                    $file_size_thumb = $_FILES['challenge_post']['size'];
                    $file_tmp_thumb  = $_FILES['challenge_post']['tmp_name'];
                    $file_type_thumb = $_FILES['challenge_post']['type'];
                    $tmp_thumb       = explode('.', $file_name_thumb);
                    $file_ext_thumb  = end($tmp_thumb);

            
                    $thumb_name = "challenge_post-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                    
                    $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                    
                    if(in_array($file_ext_thumb,$expensions_thumb)=== false){

                    $errors_thumb[] = "extension not allowed, please choose a JPEG or PNG file.";
                    
                }
                    
                    
                    
                    if(empty($errors_thumb)==true){
                   
                    move_uploaded_file($file_tmp_thumb,"assets/images/challenge/type1/".$thumb_name);
                    $challenge_image = $thumb_name;

                    $insert_challenge_post =   
                    array( 'challenge_posts_challenge_id'       =>  $challengeid,
                    'challenge_posts_title'                     =>  $challenge_posts_title,
                    'challenge_posts_sub_post'                  =>  $challenge_image,
                    'challenge_posts_user_id'                   =>  $userid
                   
                    
                   
                    );
                    $insert_challenge_posts = $this->ChallengeApiModel->uploadChallengeposts($insert_challenge_post);
    
                    if($insert_challenge_posts > 0){
                        $last_post  = $this->ChallengeApiModel->getLastinsertedpost($insert_challenge_posts);
                          
                            
                                foreach($last_post as $val => $key){
                                    $last_post[$val]['challenge_posts_video_thumbnail'] = base_url()."assets/challenge/thumb/".$last_post[$val]['challenge_posts_video_thumbnail'];
                                    $last_post[$val]['challenge_posts_sub_post']        = base_url()."assets/challenge/type1/".$last_post[$val]['challenge_posts_sub_post'];
                                    $last_post[$val]['users_photo']                     = base_url()."assets/images/users/".$last_post[$val]['users_photo'];
                                    $last_post[$val]['pollCount']                       = $this->ChallengeApiModel->challengepostPollcount($challengeid , $insert_challenge_posts);
                                    $last_post[$val]['pollPercent']                     = 0; 
                                    $last_post[$val]['challenge_media_type']            = $challenge_media_type;
                                 }
                            
                            
                            
                        $msg    = "successfully saved";
                        $status = 'true';
                    }
                    else{

                        $msg    = "failed to save";
                        $status = 'false';
                    }
                    
                    }else{
                      
                        $challenge_image = "null";
                    }
                }

                if(empty($last_post)){
                    
                    $last_post = [];
                }
                $this->output->set_output(json_encode(['message' => $msg ,'last_post' => $last_post,'status'=> $status])); 
                break;

                case 2:
               
                if(isset($_FILES['challenge_video_thumbnail'])){

                    $errors_thumb    = array();
                    $file_name_thumb = $_FILES['challenge_video_thumbnail']['name'];
                    $file_size_thumb = $_FILES['challenge_video_thumbnail']['size'];
                    $file_tmp_thumb  = $_FILES['challenge_video_thumbnail']['tmp_name'];
                    $file_type_thumb = $_FILES['challenge_video_thumbnail']['type'];
                    $tmp_thumb       = explode('.', $file_name_thumb);
                    $file_ext_thumb  = end($tmp_thumb);

            
                    $thumb_name = "challenge_thumb-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                    
                    $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                    
                    if(in_array($file_ext_thumb,$expensions_thumb)=== false){

                    $errors_thumb[]="extension not allowed, please choose a JPEG or PNG file.";
                    
                }
                    
                    
                    
                    if(empty($errors_thumb)==true){
                   
                    move_uploaded_file($file_tmp_thumb,"assets/images/challenge/thumb/".$thumb_name);
                    $challenge_video_thumb = $thumb_name;
                    
                    
                    
                    }else{
                      
                        $challenge_video_thumb = "null";
                    }
                }

            // challenge video upload
                                            
            if(isset($_FILES['challenge_post'])){

                $errors    = array();
                $file_name = $_FILES['challenge_post']['name'];
                $file_size = $_FILES['challenge_post']['size'];
                $file_tmp  = $_FILES['challenge_post']['tmp_name'];
                $file_type = $_FILES['challenge_post']['type'];
                $tmp       = explode('.', $file_name);
                $file_ext  = end($tmp);


                $post_name = "challenge-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;
                
                $expensions = array("jpeg","jpg","png","mp4","3gp");
                
                if(in_array($file_ext,$expensions)=== false){

                $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                    
                }
                    
                    if(empty($errors)==true){

                        move_uploaded_file($file_tmp,"assets/images/challenge/type1/".$post_name);
                        $challenge_post = $post_name;

                        $insert_challenge_post = 
                        
                        array( 'challenge_posts_challenge_id'       =>  $challengeid,
                        'challenge_posts_title'                     =>  $challenge_posts_title,
                        'challenge_posts_sub_post'                  =>  $challenge_post,
                        'challenge_posts_video_thumbnail'           =>  $challenge_video_thumb,
                        'challenge_posts_user_id'                   =>  $userid                      
                        );
                    
                        $insert_challenge_posts = $this->ChallengeApiModel->uploadChallengeposts($insert_challenge_post);

                        if($insert_challenge_posts > 0){
                            
                            $last_post  = $this->ChallengeApiModel->getLastinsertedpost($insert_challenge_posts);
                            foreach($last_post as $val => $key){
                                $last_post[$val]['challenge_posts_video_thumbnail'] = base_url()."assets/challenge/thumb/".$last_post[$val]['challenge_posts_video_thumbnail'];
                                $last_post[$val]['challenge_posts_sub_post']        = base_url()."assets/challenge/type1/".$last_post[$val]['challenge_posts_sub_post'];
                                $last_post[$val]['users_photo']                     = base_url()."assets/images/users/".$last_post[$val]['users_photo'];
                                $last_post[$val]['pollCount']                       = $this->ChallengeApiModel->challengepostPollcount($challengeid , $insert_challenge_posts);
                                $last_post[$val]['pollPercent']                     = 0; 
                                $last_post[$val]['challenge_media_type']            = $challenge_media_type;
                             }
                            $msg        = "successfully saved";
                            $status     = 'true';
                        }
                        else{
    
                            $msg    = "successfully saved";
                            $status = 'true';
                        }

                    }
                    else{
                        $challenge_post = "null";
                    }
                }
                if(empty($last_post)){
                    
                    $last_post = [];
                }
                $this->output->set_output(json_encode(['message' => $msg ,'last_post' => $last_post,'status'=> $status])); 
                break;


            }


        }
        else{
            $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> false])); 
     
        }
     

    }
}