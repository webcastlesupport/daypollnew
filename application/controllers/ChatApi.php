<?php
class ChatApi extends CI_Controller{

   
   
    public $media_url= "https://d.daypoll.com/";
    public $s3_url   = "https://s3.amazonaws.com/daypoll/";
    
    function __construct() {
        parent::__construct();
        $this->load->model('ChatApiModel');
        $this->load->library('form_validation');
    }
     
    // send message chat api
    function sendmsg(){

        $this->output->set_content_type('application/json');
        $to_id      = $this->input->post('to_id');
        $from_id    = $this->input->post('from_id');
        $type       = $this->input->post('type');                   // 0 => text 1 => image
        
        $this->form_validation->set_rules('to_id',"to id","required");
        $this->form_validation->set_rules('from_id',"from id","required");
        $this->form_validation->set_rules('type',"type","required");

        if($this->form_validation->run()==true){

        if($type == 1){

            $this->load->helper('form');

            $config['upload_path']      = "./assets/images/chat/";
            $config['allowed_types']    = '*';
            $config['file_name']        = 'chat'.mt_rand(100000,999999).date('H-i-s');
            //$config['max_height']     = '250';
            //$config['max_width']      = '250';
            //$config['max_size']       = '100kb';
            $config["overwrite"]        = false;

            $this->load->library('upload',$config);
            if(!$this->upload->do_upload("message"))
            {
                $error = array('error' => $this->upload->display_errors());

                $this->output->set_output(json_encode(['message' => $error, 'status'=>'false']));
                return false;
                
            }
            else
            {
                
                $data       = $this->upload->data();
                $message    = $data['file_name'];
            }

        }
        else{

            $message    = $this->input->post('message');
            $this->form_validation->set_rules('message',"message","required");
            
        }
        
            $insert = array('chat_sender_id' => $from_id , 'chat_reciever_id' => $to_id , 'chat_message' => $message ,'chat_type' => $type);
            $res = $this->ChatApiModel->sendmsg($insert);
           

            if($res > 0){

                $getlastMsg = $this->ChatApiModel->getlastMsg($res);
                $lastmsg    = $getlastMsg[0];
                $lastmsg['is_sender'] = 0;
                if($lastmsg['chat_type'] == 1){
                    
                    $lastmsg['chat_message'] = $this->media_url."assets/images/chat/".$lastmsg['chat_message'];
                }
            
                $this->output->set_output(json_encode(array('message' => 'succesfully inserted','data'=>$lastmsg,'status' => true)));
            }
            else{
                $this->output->set_output(json_encode(array('message' => 'failed to insert' ,'status' => false )));
            }
        }
        else{
            $this->output->set_output(json_encode(array('message' => 'parameter missing' , 'status' => false))); 
        }
    }

  

    // delete chat by chat id
    function deleteChatbyid(){

        $this->output->set_content_type('application/json');

        $chatid      = $this->input->post('chatid');
        //$userid      = $this->input->post('userid');
        if(!empty($chatid)){
            
            $res  = $this->ChatApiModel->deleteChatbyid($chatid);

            if($res > 0){
                $this->output->set_output(json_encode(array('message' => 'succesfully deleted', 'status' => true)));
            }
            else{
                $this->output->set_output(json_encode(array('message' => 'failed to delete', 'status' => false)));
            }
        }
        else{
            $this->output->set_output(json_encode(array('message' => 'parameter missing', 'status' => false)));
        }

    }

    // delete chat by chat id
    function deleteChatbyuser(){

        $this->output->set_content_type('application/json');

        $senderid      = $this->input->post('loggedid');
        $userid        = $this->input->post('userid');
        
        if(!empty($userid && $senderid)){
            
            $res  = $this->ChatApiModel->deleteChatbyuser($senderid , $userid);

            if($res > 0){
                $this->output->set_output(json_encode(array('message' => 'succesfully deleted', 'status' => true)));
            }
            else{
                $this->output->set_output(json_encode(array('message' => 'failed to delete', 'status' => false)));
            }
            }
            else{
                    $this->output->set_output(json_encode(array('message' => 'parameter missing', 'status' => false)));
            }

    }


    //chat inbox
    function chatInbox(){
        
        $this->output->set_content_type('application/json');
        $userid      = $this->input->post('user_id');
        if(!empty($userid)){

            $res_sender     = $this->ChatApiModel->get_sender($userid);
            $res_reciever   = $this->ChatApiModel->get_reciever($userid);

            $response       = array_merge($res_sender,$res_reciever);

                  
            $res =[];
            foreach($response as $value){
                $res[$value['userid']] = $value;
            }
          
            if($res){
                foreach($res as $key => $value){

                    $res[$key]['unread']    = $this->ChatApiModel->getUnreadMessages($res[$key]['loggedid'],$res[$key]['userid']);
                    $res[$key]['last_msg']  = $this->ChatApiModel->getlastMsgbyid($res[$key]['loggedid'],$res[$key]['userid']);
                    if($res[$key]['loggedid'] == $userid)
                    {
                    $res[$key]['user_id']    = $res[$key]['userid'];
                    $get_user                = $this->ChatApiModel->getusernameforChat($res[$key]['userid']);
                    $res[$key]['user_name']  = $get_user['username'];
                    $res[$key]['user_photo'] = $this->media_url."assets/images/users/".$get_user['userphoto'];
                    }else{

                    $res[$key]['user_id']    = $res[$key]['loggedid'];
                    $get_user                = $this->ChatApiModel->getusernameforChat($res[$key]['loggedid']);
                    $res[$key]['user_name']  = $get_user['username'];
                    $res[$key]['user_photo'] = $this->media_url."assets/images/users/".$get_user['userphoto'];
                    }

                    foreach($res[$key]['last_msg'] as $v => $k){


                        if($res[$key]['last_msg'][$v]['chat_type'] == 1)
                        $res[$key]['last_msg'][$v]['chat_message'] = $this->media_url."assets/images/chat/".$res[$key]['last_msg'][$v]['chat_message'];
                        
                        $res[$key]['is_sender']         = $res[$key]['last_msg'][$v]['chat_sender_id'] == $userid ? 1 :0;
                        $res[$key]['chat_date']         = $res[$key]['last_msg'][$v]['chat_date'];
                        $res[$key]['message']           = $res[$key]['last_msg'][$v]['chat_message'];
                        $res[$key]['type']              = $res[$key]['last_msg'][$v]['chat_type'];
                        $res[$key]['seen_status']       = $res[$key]['last_msg'][$v]['chat_seen_status'];

                    }

                    unset($res[$key]['last_msg']);
                    
                }

                foreach($res as $row){
                    $final_res[] = $row;
                }

             
                $this->output->set_output(json_encode(array('message' => $final_res, 'status' => true)));
            }
            else{
                $this->output->set_output(json_encode(array('message' => 'no messages found', 'status' => false)));
            }
        }else{
                $this->output->set_output(json_encode(array('message' => 'parameter missing', 'status' => false)));
        }

    }

    //search chat user
    function searchUserinChat(){
        
        $this->output->set_content_type('application/json');
        
        $search = $this->input->post('search');
        $userid = $this->input->post('userid');

        $res    = $this->ChatApiModel->searchUserinChat($search , $userid);

        if($res){
            $this->output->set_output(json_encode(array('message' => $res, 'status' => true)));
          
        }else{
            $this->output->set_output(json_encode(array('message' => 'no user found', 'status' => false)));
          
        }

    }

    // make message read
    function udpateMsgread(){
        
        $this->output->set_content_type('application/json');
        
        $fromid = $this->input->post('loggedid');
        $toid   = $this->input->post('userid');

        $res    = $this->ChatApiModel->udpateMsgread($fromid,$toid);

        if($res > 0){
            $this->output->set_output(json_encode(array('message' => "successfully updated", 'status' => true)));
          
        }else{
            $this->output->set_output(json_encode(array('message' => 'failed to update', 'status' => false)));
          
        }
    }

    // get individual chat 
    function getChatMessages(){
        $this->output->set_content_type('application/json');
        
        $fromid = $this->input->post('loggedid');
        $toid   = $this->input->post('userid');
        $this->ChatApiModel->udpateMsgread($fromid,$toid);
        $res    = $this->ChatApiModel->getMessages($fromid,$toid);

        if($res > 0){
            $this->output->set_output(json_encode(array('data' => $res, 'status' => true)));
          
        }else{
            $this->output->set_output(json_encode(array('data' => 'no messages found', 'status' => false)));
          
        }
    }


     // get chat by id
     function getChatlist(){


        $zone = getallheaders();
        $GMT  = new DateTimeZone("GMT");
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";

        $this->output->set_content_type('application/json');
        $userid      = $this->input->post('user_id');
        $sendid      = $this->input->post('sent_to');
        $time        = $this->input->post('time_zone');

        $this->form_validation->set_rules('user_id','user_id','required');
        $this->form_validation->set_rules('sent_to','sent_to','required');

        if($this->form_validation->run()== true){
        $this->ChatApiModel->udpateMsgread($userid,$sendid);
        $get_chat = $this->ChatApiModel->getChatlist($userid,$sendid);
       
        foreach($get_chat as $key => $val){

            if($get_chat[$key]['chat_type'] == 1){
                $get_chat[$key]['chat_message'] = $this->media_url."assets/images/chat/".$get_chat[$key]['chat_message'];
            }

            if($get_chat[$key]['chat_sender_id'] == $userid)$get_chat[$key]['is_sender'] = 0;
            else $get_chat[$key]['is_sender'] = 1;


            $date = new DateTime($get_chat[$key]['chat_date'], $GMT );
            $date->setTimezone(new DateTimeZone($zone['timezone']));
            $get_chat[$key]['chat_date'] = $date->format('Y-m-d H:i:s');

        }

        if($get_chat != null){
            $this->output->set_output(json_encode(array('message' => 'value found', 'data' => $get_chat, 'status' => true)));
        }
        else{
            $this->output->set_output(json_encode(array('message' => 'no data found' ,'status' => false )));
        }
    }
    else{
        $this->output->set_output(json_encode(array('message' => validation_errors(),'status' => false )));
    }
        
    }




}
?>