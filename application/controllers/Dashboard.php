<?php
/**
 * Created by PhpStorm.
 * User: user56
 * Date: 03-08-2018
 * Time: 15:59
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller{

    private $loaders=array('pageview'=>'undefined','sess_admin_username'=>'undefined','sess_admin_logdate'=>'undefined');

    function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        //$this->load->library('encrypt');
        $this->load->model(array('UserModel','AdminModel'));
    }

    function isAuthorised(){

        if(empty($this->session->userdata('sess_admin_id'))){
            redirect('Login');
        }
    }


    // index page (dashboard)
    function index(){

        $this->isAuthorised();
        $this->loaders['pageview']  = 'Dashboard';
        $this->load->view('common/template',$this->loaders);
    }


    function dash2(){
        $this->load->view('dash2');
    }


    // profile page
    function profile(){

        $this->isAuthorised();
        $this->loaders['pageview']  = "Profile";
        $this->load->view('common/template',$this->loaders);
    }

    // update Admin details
    public function updateAdmin(){

        $this->output->set_content_type('application/json');

        $username   = $this->input->post('usernameedit');
        $firstname  = $this->input->post('firstnameedit');
        $lastname   = $this->input->post('lastnameedit');

        $update     = array('admin_username'=>$username,'admin_firstname'=>$firstname,'admin_lastname'=>$lastname);
        $res        = $this->AdminModel->updateAdmin($update,$this->session->userdata('sess_admin_id'));

        if($res > 0){

            $retreiveAdmin = $this->AdminModel->retrieveAdmin($this->session->userdata('sess_admin_id'));


            $user          = $retreiveAdmin['data'][0];

            $this->session->set_userdata('sess_admin_id', $user['admin_id']);
            $this->session->set_userdata('sess_admin_username', $user['admin_username']);
            $this->session->set_userdata('sess_admin_logdate',date('d-m-Y H:i A',strtotime($user['admin_logdate'])));
            $this->session->set_userdata('sess_admin_firstname', $user['admin_firstname']);
            $this->session->set_userdata('sess_admin_lastname', $user['admin_lastname']);
            $this->session->set_userdata('sess_admin_photo', $user['admin_photo']);

            $this->output->set_output(json_encode(array('message'=>'successfully updated','error'=>0)));
        }else
            {
            $this->output->set_output(json_encode(array('message'=>'failed to update','error'=>1)));
        }

    }

    // update admin password 
    function updateAdminpassword(){

        $this->output->set_content_type('application/json');

        $oldpass    = hash('sha256',$this->input->post('oldpass'));
        $newpass    = hash('sha256',$this->input->post('rnewpass'));
        $update     = array('admin_password'=>$newpass);

        $res        = $this->AdminModel->updateAdminpassword($update,$this->session->userdata('sess_admin_id'),$oldpass);

        if($res > 0){

            $this->output->set_output(json_encode(array('message'=>'successfully updated','error'=>0,'response'=>$res)));

        }elseif($res == 0)
        {
            $this->output->set_output(json_encode(array('message'=>'failed to update','error'=>1,'response'=>$res)));

        }
        elseif($res == -1){

            $this->output->set_output(json_encode(array('message'=>'Old password is not correct','error'=>1,'response'=>$res)));
        }

    }


    // upload admin profile picture
    function uploadprofilepic()
    {

        $this->output->set_content_type('application/json');
        $this->isAuthorised();
        $this->load->helper('form');

        $config['upload_path']      = "./assets/images/admin/";
        $config['allowed_types']    = 'jpg|gif|png';
        $config['file_name']        = 'admin'.mt_rand(100000,999999);
        $config['max_height']       = '250';
        $config['max_width']        = '250';
        $config['max_size']         = '100kb';
        $config["overwrite"]        = false;

        $this->load->library('upload',$config);
        if(!$this->upload->do_upload("propic"))
        {
            $error  = array('error' => $this->upload->display_errors());
            $this->output->set_output(json_encode(['message' => "file is not specified or not supported (size should be below 100kb)<br>Allowed type should be png,gif or jpeg type<br> Width X Height (250px X 250px) ", 'error'=>1]));
            return false;
        }
        else
        {
            $data   = $this->upload->data();
            $path   = "./assets/images/admin/".$this->session->userdata("sess_admin_photo");
            $option = false;
            if(file_exists($path))
            {
                $option = true;
                unlink($path);
            }

            $result     = $this->AdminModel->uploadprofilepic($data['file_name'],$this->session->userdata("sess_admin_id"));
            $getadmin   = $this->AdminModel->getAdminphoto($this->session->userdata("sess_admin_id"));
            $this->session->set_userdata('sess_admin_photo',$getadmin);

            $this->output->set_output(json_encode(['message' =>"Your request was successfully performed!..",'error'=>"0"]));
        }
    }


    // session logout code
    function Logout(){
        
        $this->session->sess_destroy();
        $this->session->unset_userdata('sess_admin_id');
        $this->session->unset_userdata('sess_admin_username');
        $this->session->unset_userdata('sess_admin_logdate');
        $this->session->unset_userdata('sess_admin_lastname');
        $this->session->unset_userdata('sess_admin_photo');
        $this->session->unset_userdata('sess_admin_firstname');
        redirect('Login');
    }
    
    
    //dashboard chart code
    function getChartcount(){
        
        $res=$this->AdminModel->getChartcount();
        echo $res;
    }


}