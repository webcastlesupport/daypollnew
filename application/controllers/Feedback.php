<?php

class Feedback extends CI_Controller{

    private $loader = array('pageview'=>'undefined');
    function __construct() {
        
        parent::__construct();
        $this->load->model('FeedbackModel');
        
    }
    
    function isAuthorised(){

        if(empty($this->session->userdata('sess_admin_id'))){
            redirect('Login');
        }
    }

     // load feedback page
     function feedback(){
        
        $this->isAuthorised();
        $this->loader['pageview']="Feedback";
        $this->load->view('common/template',$this->loader);
    }
}
?>