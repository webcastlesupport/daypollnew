<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Group extends CI_Controller{
    
    private $loader = array('pageview'=>'undefined');
    function __construct() {
        
        parent::__construct();
        $this->load->model('GroupModel');
        
    }
    
    function isAuthorised(){

        if(empty($this->session->userdata('sess_admin_id'))){
            redirect('Login');
        }
    }
    
    // load group page
    function group(){
        
        $this->isAuthorised();
        $this->loader['pageview']="Group";
        $this->load->view('common/template',$this->loader);
    }

     // load group page
     function blockedgroupposts(){
        
        $this->isAuthorised();
        $this->loader['pageview']="blockedgroupposts";
        $this->load->view('common/template',$this->loader);
    }
    

      // block posts by post id
      function blockgroupposts(){

        $flag   = "";
        $this->output->set_content_type('application/json');
        $id     = $this->input->post('id');
        $active = $this->input->post('active');
        
        if($active == 1)$flag = 0;
        if($active == 0)$flag = 1;
        
        $res    = $this->GroupModel->blockgroupposts($id,$flag);
        $this->output->set_output(json_encode(array('details'=>$res)));
    }

    // get all group by list
    function getallGroup(){
        
        $this->isAuthorised();
        $this->output->set_content_type('application/json');
        $res    = $this->GroupModel->getallParty();
        $this->output->set_output(json_encode(array('values'=>$res)));
    }

    // block group by id
    function blockgroup(){
        $this->output->set_content_type('application/json');
        
        $groupid = $this->input->post('id');
        $active  = $this->input->post('active');
        if($active==1)$flag=0;
        if($active==0)$flag=1;
        $res    = $this->GroupModel->blockgroup($groupid , $flag);
        $this->output->set_output(json_encode(array('values'=>$res)));
    }

    //get group posts
    function viewPosts($id = null){

        if(!empty($id)){
        $res = $this->GroupModel->viewPosts($id); 

        if($res != null){
            $this->loader['data']   = $res;   
        }
        else{
            $this->loader['data']   = "";
            //echo "no value found";

        }
        }
        else{
            echo "parameter missing";
        }
        $this->isAuthorised();
        $this->loader['pageview']= "GroupPosts";   
        $this->load->view('common/template',$this->loader);
    }


      //get group posts
      function viewMembers($id = null){

        if(!empty($id)){
        $res = $this->GroupModel->viewMembers($id); 

        if($res != null){
            $this->loader['data']   = $res;   
        }
        else{
            $this->loader['data']   = "";
            }
        }
        else{
            echo "parameter missing";
        }
        $this->isAuthorised();
        $this->loader['pageview']= "GroupMembers";   
        $this->load->view('common/template',$this->loader);
    }


      // block posts by post id
      function blockposts(){

        $flag   = "";
        $this->output->set_content_type('application/json');
        $id     = $this->input->post('id');
        $active = $this->input->post('active');
        
        if($active == 1)$flag = 0;
        if($active == 0)$flag = 1;
        
        $res    = $this->GroupModel->blockposts($id,$flag);
        $this->output->set_output(json_encode(array('details'=>$res)));
    }


     // get group posts details by id
     function getgroupPostsdetails(){

        $this->output->set_content_type('application/json');
        $id             = $this->input->post('id');
        $res            = $this->GroupModel->getgroupPostsdetails($id);
        $data           = $res[0];
        $photo_or_video = $data['group_posts_photo_video'];
        $ext            = substr($photo_or_video, strpos($photo_or_video, ".") + 1);   
        $ext_           = ($ext=='mp4' ? $ext_value = "video":$ext_value = "photo");
        
        $data['group_posts_uploaded_date']  = date('d/m/Y  h:i A' ,strtotime($data['group_posts_uploaded_date'])); 

        $data['group_posts_photo_video']    = base_url()."assets/images/groupposts/".$data['group_posts_photo_video'];
       
        $this->output->set_output(json_encode(array('details'=>$data,'photo_or_video'=>$ext_)));
        
        unset($ext);

    }

}