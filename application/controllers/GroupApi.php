<?php

class GroupApi extends CI_Controller{

    //public $media_url= "https://s3.amazonaws.com/daypoll/";
    public $media_url= "https://d.daypoll.com/";
    function __construct()
    {
        //error_reporting(0);
        parent::__construct();
        $this->load->model('GroupApiModel');
        $this->load->library('form_validation');
    }
    
    //create new group
    function createGroup(){

        $insert = array();
        $this->output->set_content_type('application/json');
        
        $name       = $this->input->post('name');
        $country    = $this->input->post('country');
        $state      = $this->input->post('state');
        $city       = $this->input->post('district');
        $area       = $this->input->post('area');
        $privacy    = $this->input->post('privacy');  // 0 => public 1 => private
        $userid     = $this->input->post('userid');
        $scope      = $this->input->post('scope');
        $desc       = $this->input->post('description');


        $this->form_validation->set_rules("name","name","required");
        $this->form_validation->set_rules("privacy","privacy","required");
        // $this->form_validation->set_rules("country","country","required");
        // $this->form_validation->set_rules("state","state","required");
        // $this->form_validation->set_rules("district","district","required");
        // $this->form_validation->set_rules("area","area","required");
        $this->form_validation->set_rules("userid","userid","required");
        $this->form_validation->set_rules("scope","scope","required");

        if($this->form_validation->run()==true){


            $insert = array(
                'group_name'        => $name, 
                'group_created_by'  => $userid,
                'group_privacy'     => $privacy,
                'group_country'     => $country,
                'group_state'       => $state,
                'group_district'    => $city,
                'group_area'        => $area,
                'group_scope'       => $scope,
                'group_desc'        => $desc

            );

            // if(isset($_FILES['group_profile_image'])!=null){

                  
            //     // group  profile pic upload
                    
            //             $errors= array();
            //             $file_name = $_FILES['group_profile_image']['name'];
            //             $file_size = $_FILES['group_profile_image']['size'];
            //             $file_tmp  = $_FILES['group_profile_image']['tmp_name'];
            //             $file_type = $_FILES['group_profile_image']['type'];
                        
            //             $tmp      = explode('.', $file_name);
            //             $file_ext = end($tmp);
       
            //             $expensions  = array("jpeg","jpg","png");
                        
            //             if(in_array($file_ext,$expensions) === false){
            //                $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
            //             }
                        
                        
            //             if(empty($errors)==true){

                            
            //                 $profile_pic = 'group-'.mt_rand(10000,99999).date('h-i-s').'.'.$file_ext;
            //                 move_uploaded_file($file_tmp,"assets/images/group/profile/".$profile_pic);
                          
                        
            //                 $insert['group_profile_photo'] = $profile_pic;
                            
                          
            //             }
              
            // }




            // if(isset($_FILES['group_cover_image'])!=null){

                  
            //     // group  cover pic upload
                    
            //             $errors= array();
            //             $file_name = $_FILES['group_cover_image']['name'];
            //             $file_size = $_FILES['group_cover_image']['size'];
            //             $file_tmp  = $_FILES['group_cover_image']['tmp_name'];
            //             $file_type = $_FILES['group_cover_image']['type'];
                        
            //             $tmp      = explode('.', $file_name);
            //             $file_ext = end($tmp);
       
                   
                          
                        
                       
            //             $expensions  = array("jpeg","jpg","png");
                        
            //             if(in_array($file_ext,$expensions) === false){
            //                $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
            //             }
                        
                        
            //             if(empty($errors)==true){

                            
            //                 $cover_pic = 'group-'.mt_rand(10000,99999).date('h-i-s').'.'.$file_ext;
            //                 move_uploaded_file($file_tmp,"assets/images/group/cover/".$cover_pic);
                          
                        
            //                 $insert['group_cover_photo'] = $cover_pic;
                            
                          
            //             }
              
            // }

          
            $res        = $this->GroupApiModel->createGroup($insert);
            $get_name   = $this->GroupApiModel->getcreatedGroupname($res);
            if($res > 0){

                 $this->GroupApiModel->addAdminasmember($res,$userid);
                 $this->output->set_output(json_encode(array('message' => 'successfully created' ,'id' => $res,'name' => $get_name, 'status' => true)));
            } 
            else{
                $this->output->set_output(json_encode(array('message' => 'failed to insert' , 'status' => false)));
            }       
        }   
        else{
            $this->output->set_output(json_encode(array('message' => 'parameter missing' , 'status' => false)));
        }


    }


    // get group profile
    function getgroupProfile(){

        $zone = getallheaders();
        $GMT  = new DateTimeZone("GMT");
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";


        $this->output->set_content_type('application/json');
        
        $groupid    = $this->input->post('groupid'); 
        $userid     = $this->input->post('userid');
     
        $page_start  = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;

       
        if(!empty($groupid && $userid)){
            
            $res            = $this->GroupApiModel->getgroupProfile($groupid);
            
            $rank           = $this->GroupApiModel->getgrouprank();

            
            print_r($rank);
            exit();
            
            $group_rank     = 0;
            $group_counter  = 1;
            foreach($rank as $rank_val => $rank_key){

                if($rank[$rank_val]['group_followers_group_id'] == $groupid){
                    $group_rank =  $group_counter;
                }
            $group_counter++;    
            }

            //$membersbypoll  = $this->GroupApiModel->getgroupMembersbypoll($groupid);
            
            $allmembers     = $this->GroupApiModel->getgroupMembers($groupid);
            
            $is_joined      = $this->GroupApiModel->checkrequestExists($userid , $groupid);
            $isJoined       = $is_joined > 0 ? -1 : 0;


            $total_array  = [];
            $result_Array = [];
            foreach($allmembers['data'] as $k => $v){
                
                $allmembers['data'][$k]['total']       = $this->GroupApiModel->getGroupmemberspollcount($allmembers['data'][$k]['user_id']);
    
                $allmembers['data'][$k]['users_photo'] = $this->media_url."assets/images/users/".$allmembers['data'][$k]['users_photo'];
            }
            
            $i=0;
            foreach($allmembers['data'] as $row){
                $total_array[$i] = $row['total'];
                $i++;
            }
            arsort($total_array);
            foreach($total_array as $key => $value){
                $result_Array[] = $allmembers['data'][$key];
            }


            $result_Array = array_slice($result_Array,0,5); 
            


            if($isJoined != -1){

            $is_joined      = $this->GroupApiModel->isGroupjoined($userid,$groupid);
            $isJoined       = ($is_joined > 0) ? 1 : 0;
            
            }
            
           
            $get_res_posts  = [];
            $get_report_post= [];
            $get_hide_post  = [];

           
            if($isJoined > 0 && $res[0]['group_privacy'] == 1){
              
         
            $get_res_posts       = $this->GroupApiModel->getgroupPosts($groupid);
            
            $get_report_post = $this->GroupApiModel->getReportedposts($userid);
            $get_hide_post   = $this->GroupApiModel->gethiddenposts($userid);
         
            

            $hide_array =   [];
            foreach($get_hide_post as $row){
                $hide_array[]   = $row['hide_posts_post_id'];
            }
    
            $report_array =   [];
            foreach($get_report_post as $row){
                $report_array[]   = $row['group_posts_report_post_id'];
            }
    
           
    
    
            $final_array =   [];
            foreach($get_res_posts as $in_row){
                if(!in_array($in_row['group_posts_id'],$hide_array)){
                    $final_array[]    =  $in_row ;
                }
            }
            
            $res_posts_sort = [];
            foreach($final_array as $in_row){
               if(!in_array($in_row['group_posts_id'],$report_array)){
                   $res_posts_sort[]    =  $in_row ;
               }
           }
    
           $total_posts     = count($res_posts_sort); 
            
            
            $res_posts            = array_slice($res_posts_sort,$page_start,10);

            $res_posts_next       = array_slice($res_posts_sort,$page_start+10,10);



            if($res_posts){
            foreach($res_posts as $keys => $vals){
                
                $res_posts[$keys]['group_posts_photo_video'] = $this->media_url."assets/images/groupposts/".$res_posts[$keys]['group_posts_photo_video']; 
                $res_posts[$keys]['group_posts_video_thumbnail'] = $this->media_url."assets/images/groupposts/thumbnail/".$res_posts[$keys]['group_posts_video_thumbnail']; 
              
                $res_posts[$keys]['posts_poll_count']        = $this->GroupApiModel->grouppollsCount($res_posts[$keys]['group_posts_id']);
                $res_posts[$keys]['posts_comment_count']     = $this->GroupApiModel->grouppostscommentsCount($res_posts[$keys]['group_posts_id']);
                $res_posts[$keys]['users_photo']             = $this->media_url."assets/images/users/".$res_posts[$keys]['users_photo'];
                $isPolled                                    = $this->GroupApiModel->isPolledgrouppost($userid , $res_posts[$keys]['group_posts_id']);   
                $res_posts[$keys]['isPolled']                = $isPolled > 0 ? 1 : 0 ;   
                $res_posts[$keys]['downloadlink']            = $res_posts[$keys]['group_posts_photo_video'].'?postid='.$res_posts[$keys]['group_posts_id'].'type=2';

                $date = new DateTime($res_posts[$keys]['group_posts_uploaded_date'], $GMT );
                $date->setTimezone(new DateTimeZone($zone['timezone']));
                $res_posts[$keys]['group_posts_uploaded_date'] = $date->format('Y-m-d H:i:s'); 
               
                }
            }
        }
        else{
            $get_report_post = $this->GroupApiModel->getReportedposts($userid);
            $get_hide_post   = $this->GroupApiModel->gethiddenposts($userid);
            $get_res_posts      = $this->GroupApiModel->getgroupPosts($groupid);


             $hide_array =   [];
             foreach($get_hide_post as $row){
                 $hide_array[]   = $row['hide_posts_post_id'];
             }
     
             $report_array =   [];
             foreach($get_report_post as $row){
                 $report_array[]   = $row['group_posts_report_post_id'];
             }
     
             $final_array =   [];
             foreach($get_res_posts as $in_row){
                 if(!in_array($in_row['group_posts_id'],$hide_array)){
                     $final_array[]    =  $in_row ;
                 }
             }
             
             $res_posts_sort = [];
             foreach($final_array as $in_row){
                if(!in_array($in_row['group_posts_id'],$report_array)){
                    $res_posts_sort[]    =  $in_row ;
                }
            }
     
            $total_posts     = count($res_posts_sort); 
             
             
            $res_posts       = array_slice($res_posts_sort,$page_start,10);

            $res_posts_next  = array_slice($res_posts_sort,$page_start+10,10);


            if($res_posts){
            foreach($res_posts as $keys => $vals){
                
                $res_posts[$keys]['group_posts_video_thumbnail']    = $this->media_url."assets/images/groupposts/thumbnail/".$res_posts[$keys]['group_posts_video_thumbnail']; 
                $res_posts[$keys]['group_posts_photo_video']        = $this->media_url."assets/images/groupposts/".$res_posts[$keys]['group_posts_photo_video']; 
                $res_posts[$keys]['posts_poll_count']               = $this->GroupApiModel->grouppollsCount($res_posts[$keys]['group_posts_id']);
                $res_posts[$keys]['posts_comment_count']            = $this->GroupApiModel->grouppostscommentsCount($res_posts[$keys]['group_posts_id']);
                $res_posts[$keys]['users_photo']                    = $this->media_url."assets/images/users/".$res_posts[$keys]['users_photo'];
                $isPolled                                           = $this->GroupApiModel->isPolledgrouppost($userid , $res_posts[$keys]['group_posts_id']);   
                $res_posts[$keys]['isPolled']                       = $isPolled > 0 ? 1 : 0 ;   
                $res_posts[$keys]['downloadlink']                   = $res_posts[$keys]['group_posts_photo_video'].'?postid='.$res_posts[$keys]['group_posts_id'].'type=2';
                $res_posts[$keys]['sharecode']                      = $this->media_url.'post-view?postid='.$res_posts[$keys]['group_posts_id'].'&&type=2';
                
                $date = new DateTime($res_posts[$keys]['group_posts_uploaded_date'], $GMT );
                $date->setTimezone(new DateTimeZone($zone['timezone']));
                $res_posts[$keys]['group_posts_uploaded_date'] = $date->format('Y-m-d H:i:s'); 
               
                }
            }
        }

           

            $is_admin       = $this->GroupApiModel->isGroupadmin($userid,$groupid);
            $isAdmin        = ($is_admin > 0) ? 1 :0;
           

            if($res > 0){

                $profile                        = $res[0];
                $profile['group_profile_photo'] = $this->media_url."assets/images/group/profile/".$profile['group_profile_photo'];
                $profile['group_cover_photo']   = $this->media_url."assets/images/group/cover/".$profile['group_cover_photo'];
                
                $this->output->set_output(json_encode(array('profile' => $profile,'group_rank' => $group_rank,'members' => $result_Array ,'hidden_post_id' => $get_hide_post,'report_post_id' => $get_report_post,'posts' => $res_posts,'total_posts' => $total_posts,'is_Joined' => $isJoined,'memberscount' => $allmembers['count'],'is_Admin' => $isAdmin,'is_completed' => count($res_posts_next) > 0 ? 0 :1,'status' => true)));
   
            }
            else{
                $this->output->set_output(json_encode(array('value' => 'no data found' , 'status' => false)));
   
            }
           
        }
        else{
                $this->output->set_output(json_encode(array('value' => 'parameter missing' , 'status' => false)));
   
        }
        
    }

     // get group profile
     function returngetgroupProfile($groupid,$userid,$page_start){

        $zone = getallheaders();
        $GMT  = new DateTimeZone("GMT");
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";

        $this->output->set_content_type('application/json');
        
        if(!empty($groupid && $userid)){
            
            $res            = $this->GroupApiModel->getgroupProfile($groupid);
            
            $rank           = $this->GroupApiModel->getgrouprank();

            
            $group_rank     = 0;
            $group_counter  = 1;
            foreach($rank as $rank_val => $rank_key){

                if($rank[$rank_val]['users_group_followers_group_id'] == $groupid){
                    $group_rank =  $group_counter;
                }
            $group_counter++;    
            }

            $membersbypoll  = $this->GroupApiModel->getgroupMembersbypoll($groupid);
            
            $allmembers     = $this->GroupApiModel->getgroupMembers($groupid);
            
            $is_joined      = $this->GroupApiModel->checkrequestExists($userid , $groupid);
            $isJoined       = $is_joined > 0 ? -1 : 0;

          
            foreach($allmembers['data'] as $k => $v){

                $allmembers['data'][$k]['users_photo'] = $this->media_url."assets/images/users/".$allmembers['data'][$k]['users_photo'];
            }
            
            if($isJoined != -1){

            $is_joined      = $this->GroupApiModel->isGroupjoined($userid,$groupid);
            $isJoined       = ($is_joined > 0) ? 1 : 0;
            
            }
            
           
            $get_res_posts  = [];
            $get_report_post= [];
            $get_hide_post  = [];

           
            if($isJoined > 0 && $res[0]['group_privacy'] == 1){
              
         
            $get_res_posts       = $this->GroupApiModel->getgroupPosts($groupid);
            
            $get_report_post = $this->GroupApiModel->getReportedposts($userid);
            $get_hide_post   = $this->GroupApiModel->gethiddenposts($userid);
         
            

            $hide_array =   [];
            foreach($get_hide_post as $row){
                $hide_array[]   = $row['hide_posts_post_id'];
            }
    
            $report_array =   [];
            foreach($get_report_post as $row){
                $report_array[]   = $row['group_posts_report_post_id'];
            }
    
           
    
    
            $final_array =   [];
            foreach($get_res_posts as $in_row){
                if(!in_array($in_row['group_posts_id'],$hide_array)){
                    $final_array[]    =  $in_row ;
                }
            }
            
            $res_posts = [];
            foreach($final_array as $in_row){
               if(!in_array($in_row['group_posts_id'],$report_array)){
                   $res_posts[]    =  $in_row ;
               }
           }
    
           $total_posts     = count($res_posts); 
            
            
            $res_posts       = array_slice($res_posts,$page_start,10);



            if($res_posts){
            foreach($res_posts as $keys => $vals){
                
                $res_posts[$keys]['group_posts_photo_video'] = $this->media_url."assets/images/groupposts/".$res_posts[$keys]['group_posts_photo_video']; 
                $res_posts[$keys]['group_posts_video_thumbnail'] = $this->media_url."assets/images/groupposts/thumbnail/".$res_posts[$keys]['group_posts_video_thumbnail']; 
              
                $res_posts[$keys]['posts_poll_count']        = $this->GroupApiModel->grouppollsCount($res_posts[$keys]['group_posts_id']);
                $res_posts[$keys]['posts_comment_count']     = $this->GroupApiModel->grouppostscommentsCount($res_posts[$keys]['group_posts_id']);
                $res_posts[$keys]['users_photo']             = $this->media_url."assets/images/users/".$res_posts[$keys]['users_photo'];
                $isPolled                                    = $this->GroupApiModel->isPolledgrouppost($userid , $res_posts[$keys]['group_posts_id']);   
                $res_posts[$keys]['isPolled']                = $isPolled > 0 ? 1 : 0 ;   
                $res_posts[$keys]['downloadlink']            = $res_posts[$keys]['group_posts_photo_video'].'?postid='.$res_posts[$keys]['group_posts_id'].'type=2';
                $res_posts[$keys]['sharecode']               = $this->media_url.'post-view?postid='.$res_posts[$keys]['group_posts_id'].'&&type=2';


                $date = new DateTime($res_posts[$keys]['group_posts_uploaded_date'], $GMT );
                $date->setTimezone(new DateTimeZone($zone['timezone']));
                $res_posts[$keys]['group_posts_uploaded_date'] = $date->format('Y-m-d H:i:s'); 
               
                }
            }
        }
        else{
            $get_report_post = $this->GroupApiModel->getReportedposts($userid);
            $get_hide_post   = $this->GroupApiModel->gethiddenposts($userid);
            $get_res_posts      = $this->GroupApiModel->getgroupPosts($groupid);


             $hide_array =   [];
             foreach($get_hide_post as $row){
                 $hide_array[]   = $row['hide_posts_post_id'];
             }
     
             $report_array =   [];
             foreach($get_report_post as $row){
                 $report_array[]   = $row['group_posts_report_post_id'];
             }
     
             $final_array =   [];
             foreach($get_res_posts as $in_row){
                 if(!in_array($in_row['group_posts_id'],$hide_array)){
                     $final_array[]    =  $in_row ;
                 }
             }
             
             $res_posts_sort = [];
             foreach($final_array as $in_row){
                if(!in_array($in_row['group_posts_id'],$report_array)){
                    $res_posts_sort[]    =  $in_row ;
                }
            }
     
            $total_posts     = count($res_posts_sort); 
             
             
            $res_posts       = array_slice($res_posts_sort,$page_start,10);
            $res_posts_next  = array_slice($res_posts_sort,$page_start+10,10);




            if($res_posts){
            foreach($res_posts as $keys => $vals){
                
                $res_posts[$keys]['is_completed']                   = count($res_posts_next); 
                $res_posts[$keys]['group_posts_video_thumbnail']    = $this->media_url."assets/images/groupposts/thumbnail/".$res_posts[$keys]['group_posts_video_thumbnail']; 
                $res_posts[$keys]['group_posts_photo_video']        = $this->media_url."assets/images/groupposts/".$res_posts[$keys]['group_posts_photo_video']; 
                $res_posts[$keys]['posts_poll_count']               = $this->GroupApiModel->grouppollsCount($res_posts[$keys]['group_posts_id']);
                $res_posts[$keys]['posts_comment_count']            = $this->GroupApiModel->grouppostscommentsCount($res_posts[$keys]['group_posts_id']);
                $res_posts[$keys]['users_photo']                    = $this->media_url."assets/images/users/".$res_posts[$keys]['users_photo'];
                $isPolled                                           = $this->GroupApiModel->isPolledgrouppost($userid , $res_posts[$keys]['group_posts_id']);   
                $res_posts[$keys]['isPolled']                       = $isPolled > 0 ? 1 : 0 ;   
                $res_posts[$keys]['downloadlink']                   = $res_posts[$keys]['group_posts_photo_video'].'?postid='.$res_posts[$keys]['group_posts_id'].'type=2';

                $date = new DateTime($res_posts[$keys]['group_posts_uploaded_date'], $GMT );
                $date->setTimezone(new DateTimeZone($zone['timezone']));
                $res_posts[$keys]['group_posts_uploaded_date'] = $date->format('Y-m-d H:i:s'); 
               
                }
            }
        }

           

            $is_admin       = $this->GroupApiModel->isGroupadmin($userid,$groupid);
            $isAdmin        = ($is_admin > 0) ? 1 :0;
           
            

            

            if($res){

                $profile                        = $res[0];
                $profile['group_profile_photo'] = $this->media_url."assets/images/group/profile/".$profile['group_profile_photo'];
                $profile['group_cover_photo']   = $this->media_url."assets/images/group/cover/".$profile['group_cover_photo'];
                
                return $res_posts;
   
            }
            else{
                return $res_posts;
   
            }
           
        }
        else{
                $this->output->set_output(json_encode(array('value' => 'parameter missing' , 'status' => false)));
   
        }
        
    }

    //update Group
    function updateGroup(){

        $update = array();

        $this->output->set_content_type('application/json');
        
        // $created_user_id = $this->input->post('created_user_id');
        // $loggedid        = $this->input->post('logged_user_id');
        $groupid         = $this->input->post('groupid');
        
        

        $name       = $this->input->post('name');
        $desc       = $this->input->post('description');
        // $country    = $this->input->post('country');
        // $state      = $this->input->post('state');
        // $city       = $this->input->post('district');
        // $area       = $this->input->post('area');
        $privacy    = $this->input->post('privacy');  // 0 => public 1 => private
        $userid     = $this->input->post('userid');
        //$scope      = $this->input->post('scope');


        // $this->form_validation->set_rules("created_user_id","created_user_id","required");
        // $this->form_validation->set_rules("logged_user_id","logged_user_id","required");
        $this->form_validation->set_rules("groupid","groupid","required");

        $this->form_validation->set_rules("name","name","required");
        $this->form_validation->set_rules("privacy","privacy","required");
        // $this->form_validation->set_rules("country","country","required");
        // $this->form_validation->set_rules("state","state","required");
        // $this->form_validation->set_rules("district","district","required");
        // $this->form_validation->set_rules("area","area","required");
        // $this->form_validation->set_rules("userid","userid","required");
        //$this->form_validation->set_rules("scope","scope","required");
        
        
        if($this->form_validation->run()==TRUE){


           
            $getPhoto = $this->GroupApiModel->geteditprofilepic($groupid);


            $update = array(
                'group_name'        => $name, 
                'group_created_by'  => $userid,
                'group_privacy'     => $privacy,
                'group_desc'        => $desc
                // 'group_country'     => $country,
                // 'group_state'       => $state,
                // 'group_district'    => $city,
                // 'group_area'        => $area,
                //'group_scope'        => $scope

            );

            if(isset($_FILES['group_profile_image'])!=null){

                  
                //   profile pic upload
                    
                        $errors = array();
                        $file_name = $_FILES['group_profile_image']['name'];
                        $file_size = $_FILES['group_profile_image']['size'];
                        $file_tmp  = $_FILES['group_profile_image']['tmp_name'];
                        $file_type = $_FILES['group_profile_image']['type'];
                      
                        $tmp      = explode('.', $file_name);
                        $file_ext = end($tmp);
        
                        
                        if($getPhoto['profile'] != $file_name){
                          
                        $expensions  = array("jpeg","jpg","png");
                        
                        if(in_array($file_ext,$expensions) === false){
                           $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
                        }
                        
                        
                        
                        $path = "./assets/images/group/profile/".$getPhoto['profile'];

                        
                        $option=false;
                        if($getPhoto['profile'] != "none.png")
                        {
                           
                         $option=true;
                         unlink($path);
                        }
                        
                        
                        if(empty($errors)==true){

                            
                            $profile_pic = 'group-'.mt_rand(10000,99999).date('h-i-s').'.'.$file_ext;
                            move_uploaded_file($file_tmp,"assets/images/group/profile/".$profile_pic);
                          
                        
                            $update['group_profile_photo'] = $profile_pic;
                            
                          
                        }
              
                    }
                  
                
            }
    
                // profile pic ends here     

                if(isset($_FILES['group_cover_image'])!=null){

                    
                       
                    //   cover pic uploaddd
                        
                            $errors= array();
                            $file_name = $_FILES['group_cover_image']['name'];
                            $file_size = $_FILES['group_cover_image']['size'];
                            $file_tmp  = $_FILES['group_cover_image']['tmp_name'];
                            $file_type = $_FILES['group_cover_image']['type'];
                            
                            $tmp      = explode('.', $file_name);
                            $file_ext = end($tmp);
        
                            if($getPhoto['cover'] != $file_name){
                              
                           
                            $expensions  = array("jpeg","jpg","png");
                             
                            if(in_array($file_ext,$expensions) === false){
                               $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
                            }
                            
                           
                            
                            $path="./assets/images/group/cover/".$getPhoto['cover'];
                          
                            $option=false;
                            if($getPhoto['cover'] != "cover-none.png")
                            {
                             $option=true;
                             unlink($path);
                            }
                            
                            
                            if(empty($errors)==true){
                               
                                $cover_pic = 'cover-'.mt_rand(10000,99999).date('h-i-s').'.'.$file_ext;
                               move_uploaded_file($file_tmp,"assets/images/group/cover/".$cover_pic);
                               $update['group_cover_photo'] = $cover_pic;
                            }
                  
                        }
                       
                }

                $res = $this->GroupApiModel->updateGroup($groupid , $update);
                if($res > 0){
                    $this->output->set_output(json_encode(array('message' => 'succesfully updated' , 'status' => true)));
                }   
                else{
                    $this->output->set_output(json_encode(array('message' => 'failed to update' , 'status' => false)));
                }
        
    

   
    }
    else{
        $this->output->set_output(json_encode(array('message' => 'parameter missing' , 'status' => false)));   
        }
}

    // join group
    function joinGroup(){

        $this->output->set_content_type('application/json');

        $userid  = $this->input->post('userid');
        $groupid = $this->input->post('groupid');

        if(!empty($userid && $groupid)){
        
        
        $res    = $this->GroupApiModel->joinGroup($userid , $groupid);
            
        switch ($res){
            
            case $res > 0:
            $this->output->set_output(json_encode(array('message' => 'successfully joined','is_Joined' => 1 ,'status' => true)));
            break;

            case 0:
            $this->output->set_output(json_encode(array('message' => 'failed to join' , 'is_Joined' => 0,'status' => false)));
            break;

            case -1:
            $this->output->set_output(json_encode(array('message' => 'already joined' ,'is_Joined' => 0,'status' => false)));
            break;

        }
    }
        else{
            $this->output->set_output(json_encode(array('message' => 'parameter missing' ,'status' => false)));   
        }

    }



     // join group
     function unjoinGroup(){

        $this->output->set_content_type('application/json');

        $userid  = $this->input->post('userid');
        $groupid = $this->input->post('groupid');

        if(!empty($userid && $groupid)){
        
        
        $res    = $this->GroupApiModel->unjoinGroup($userid , $groupid);
              
        switch ($res){
            
            case $res > 0:
            $this->GroupApiModel->removeFollowers($userid , $groupid);
            $this->output->set_output(json_encode(array('message' => 'successfully unjoined','is_Joined' => 0 ,'status' => true)));
            break;

            case 0:
            $this->output->set_output(json_encode(array('message' => 'operation failed' , 'is_Joined' => 0,'status' => false)));
            break;


        }
    }
        else{
            $this->output->set_output(json_encode(array('message' => 'parameter missing' ,'status' => false)));   
        }

    }

    function uploadGroupposts(){

        $this->output->set_content_type('application/json');

        $userid         = $this->input->post('userid');
        $posttitle      = $this->input->post('title');
        $photo_or_video = $this->input->post('photo_or_video');         // photo or video  [photo -> 0 , video -> 1 , -1 -> other]
        $posts_content  = $this->input->post('content');
        $groupid        = $this->input->post('groupid');  
        $video_thumb    = $this->input->post('video_thumbnail'); 
        $linecount      = $this->input->post('linecount'); 
        $posts_width    = $this->input->post('posts_width');    
        $posts_height   = $this->input->post('posts_height');
        

        $country        = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state          = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district       = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        $area           = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;

        
        $this->form_validation->set_rules("userid","userid","required");
       
        $this->form_validation->set_rules("photo_or_video","photo_or_video","required");
        
        $this->form_validation->set_rules("groupid","groupid","required");
        
        if($linecount == NULL){
            $linecount = 0;
        }

    
        if($this->form_validation->run() == TRUE){
    
    
            if($photo_or_video == -1){
    
                $insert = array('group_posts_posted_by'=>$userid,'photo_or_video'=>$photo_or_video,
                                'group_posts_title'=>$posttitle,'group_posts_line_count'=>$linecount,
                                'group_posts_country'=>$country,'group_posts_state'=>$state,
                                'group_posts_city'=>$district,'group_posts_area'=>$area,
                                'group_posts_content'=>$posts_content,'group_posts_group_id'=>$groupid);
    
                $result = $this->GroupApiModel->uploadGroupposts($insert);
    
    
                if($result > 0) {
    
                    $this->output->set_output(json_encode(['message' => "successfully uploaded ", 'status' => "true"]));
                }
                else{
                    $this->output->set_output(json_encode(['message' => "operation failed!..", 'status' => "false"]));
                }
            }
            else if($photo_or_video == 0){
    
            $this->load->helper('form');
    
            $config['upload_path']      = "./assets/images/groupposts/";
            $config['allowed_types']    = '*';
            $config['file_name']        = 'groupposts'.mt_rand(100000,999999).date('H-i-s');
            //$config['max_height']     = '250';
            //$config['max_width']      = '250';
            //$config['max_size']       = '100kb';
            $config["overwrite"]        = false;
    
            $this->load->library('upload',$config);
            if(!$this->upload->do_upload("photo_video"))
            {
                $error = array('error' => $this->upload->display_errors());
    
                $this->output->set_output(json_encode(['message' => "file is not specified ", 'status'=>'false']));
                return false;
            }
            else
            {
                $data   = $this->upload->data();
    
                $insert = array('group_posts_posted_by'=>$userid,'photo_or_video'=>$photo_or_video,
                'group_posts_title'=>$posttitle,'group_posts_line_count'=>$linecount,'group_posts_photo_video'=>$data['file_name'],
                'group_posts_content'=>$posts_content,'posts_width' => $posts_width,'posts_height' => $posts_height,
                'group_posts_group_id'=>$groupid);


    
                $result = $this->GroupApiModel->uploadGroupposts($insert);
    
    
                if($result > 0) {
    
                    $this->output->set_output(json_encode(['message' => "successfully uploaded ", 'status' => "true"]));
                }
                else{
                    $this->output->set_output(json_encode(['message' => "operation failed!..", 'status' => "false"]));
                }
            }
    
            }


            else if($photo_or_video == 1){
    
                $this->load->helper('form');
        
                $config['upload_path']      = "./assets/images/groupposts/";
                $config['allowed_types']    = '*';
                $config['file_name']        = 'groupposts'.mt_rand(100000,999999).date('H-i-s');
                //$config['max_height']     = '250';
                //$config['max_width']      = '250';
                //$config['max_size']       = '100kb';
                $config["overwrite"]        = false;
        
                $this->load->library('upload',$config);
                if(!$this->upload->do_upload("photo_video"))
                {
                    $error = array('error' => $this->upload->display_errors());
        
                    $this->output->set_output(json_encode(['message' => "file is not specified ", 'status'=>'false']));
                    return false;
                }
                else
                {
                    $data   = $this->upload->data();
                    $flag = 1;
                    if($flag == 1){

                        if(isset($_FILES['video_thumbnail'])){
                            $errors= array();
                            $file_name = $_FILES['video_thumbnail']['name'];
                            $file_size = $_FILES['video_thumbnail']['size'];
                            $file_tmp  = $_FILES['video_thumbnail']['tmp_name'];
                            $file_type = $_FILES['video_thumbnail']['type'];
                            $tmp       = explode('.', $file_name);
                            $file_ext  = end($tmp);
                            
                            
                            $posts_thumb = "groupposts".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;
                            
                            $expensions  = array("jpeg","jpg","png");
                            
                            if(in_array($file_ext,$expensions)=== false){
                               $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
                            }
                            
                            
                            
                            if(empty($errors)==true){
                               
                               move_uploaded_file($file_tmp,"assets/images/groupposts/thumbnail/".$posts_thumb);
                               $thumb_name = $posts_thumb;
                               
                            }
                            else{
                                $thumb_name= NULL;
                            }
                  
                        }




                    $insert = array('group_posts_posted_by'=>$userid,'photo_or_video'=>$photo_or_video,
                    'group_posts_title'=>$posttitle,'group_posts_line_count'=>$linecount,'group_posts_photo_video'=>$data['file_name'],
                    'group_posts_content'=>$posts_content,'posts_width' => $posts_width,'posts_height' => $posts_height,
                    'group_posts_group_id'=>$groupid,'group_posts_video_thumbnail' => $thumb_name);
    
    
                        
                    $result = $this->GroupApiModel->uploadGroupposts($insert);
        
                    }
                    if($result > 0) {
        
                        $this->output->set_output(json_encode(['message' => "successfully uploaded ", 'status' => "true"]));
                    }
                    else{
                        $this->output->set_output(json_encode(['message' => "operation failed!..", 'status' => "false"]));
                    }
                }
        
                }
        }
        else{
            $this->output->set_output(json_encode(['message'=>'parameter missing','status'=>'false']));
        }
   
    }


    //comment posts api
    function commentgroupPost(){

        $this->output->set_content_type('application/json');

        $post_id          = $this->input->post('postid');
        $user_id          = $this->input->post('userid');
        $post_user_id     = $this->input->post('postuserid');
        $comment          = $this->input->post('comment');
        
        if(!empty($post_id && $user_id && $comment)){

        $res              = $this->GroupApiModel->commentgroupPost($user_id , $post_id , $comment , $post_user_id);

        

        if($res > 0){

            $this->output->set_output(json_encode(array('message' => "successfully commented",'comment'=>$res['data'],'status'=>true)));
        }
        else{
            
            $this->output->set_output(json_encode(array('posts' => "failed to comment",'status'=>false)));
        }
    }
    else{
            $this->output->set_output(json_encode(array('posts' => "parameter missing",'status'=>false)));
    }
    }

  // get post comments
  function getgroupPostcomments(){ 

    $this->output->set_content_type('application/json');

    $post_id     = $this->input->post('postid');
    
    $res         = $this->GroupApiModel->getgroupPostcomments($post_id);
    if($res){
    foreach($res as $key => $value){
        
        $res[$key]['users_photo']   = $this->media_url."assets/images/users/".$res[$key]['users_photo'];
        $res_reply                  = $this->GroupApiModel->getgroupcommentsreply($res[$key]['group_posts_comments_id'],$res[$key]['group_posts_comments_post_id']);
        
        foreach($res_reply as $value_1 => $key_1){
            $res_reply[$value_1]['reply_to_user']     = $this->GroupApiModel->getusersnamebyid($res_reply[$value_1]['group_posts_comment_reply_comment_user_id']);
               
            $res_reply[$value_1]['users_photo'] = $this->media_url."assets/images/users/".$res_reply[$value_1]['users_photo'];
        }
        $res[$key]['reply']         = $res_reply;
    }
}

    if($res!=null){

        $this->output->set_output(json_encode(array('data' => $res,'status'=>true)));
    }
    else{
        $this->output->set_output(json_encode(array('data' => "no value found",'status'=>false)));
    }
}

// get post comments by id
function getgroupPostcommentsbyid(){ 

    $this->output->set_content_type('application/json');

    $post_id     = $this->input->post('commentid');
    
    $res         = $this->GroupApiModel->getgroupPostcommentsbyid($post_id);

    if($res){
    foreach($res as $key => $value){
        
        $res[$key]['users_photo'] = $this->media_url."assets/images/users/".$res[$key]['users_photo'];
    
        $res_reply          = $this->GroupApiModel->getgroupcommentsreply($res[$key]['group_posts_comments_id'],$res[$key]['group_posts_comments_post_id']);
        if($res_reply){}
        foreach($res_reply as $k_1 => $v_1){
            $res_reply[$k_1]['reply_to_user']     = $this->GroupApiModel->getusersnamebyid($res_reply[$k_1]['group_posts_comment_reply_comment_user_id']);
              
            $res_reply[$k_1]['users_photo']         = $this->media_url."assets/images/users/".$res_reply[$k_1]['users_photo'];
        }
        $res[$key]['reply'] = $res_reply;
    }
    }
    
    if($res!=null){

        $this->output->set_output(json_encode(array('data' => $res,'status'=>true)));
    }
    else{
        $this->output->set_output(json_encode(array('data' => "no value found",'status'=>false)));
    }
}

// reply to comments
function replygroupPostscomments(){

    $this->output->set_content_type('application/json');

    $post_id     = $this->input->post('postid');
    $user_id     = $this->input->post('userid');
    $comment_id  = $this->input->post('commentid');
    $comment_user= !empty($this->input->post('comment_user_id')) ? $this->input->post('comment_user_id') : 1;
    $reply       = $this->input->post('reply');
    
    $insert      = array('group_posts_comments_reply_posts_id'=>$post_id,
                    'group_posts_comments_reply_comment_id'=>$comment_id,
                    'group_posts_comments_reply'=>$reply,
                    'group_posts_comment_reply_comment_user_id' => $comment_user,
                    'group_posts_comments_reply_user_id' => $user_id);

    $res         = $this->GroupApiModel->replygroupPostscomments($insert , $comment_user);

    if($res > 0){

        $this->output->set_output(json_encode(array('message' => "succesfully inserted",'status'=>true)));
    }
    else{

        $this->output->set_output(json_encode(array('posts' => "no value found",'status'=>false)));
    }
}

 //poll group posts
 function pollgroupPosts(){

    $this->output->set_content_type('application/json');
    
    $post_user_id    = $this->input->post('postuserid');
    $posts_id        = $this->input->post('postid');
    $user_id         = $this->input->post('userid');
    
    if(!empty($posts_id && $user_id)){
       
    $res        = $this->GroupApiModel->pollgroupPosts($user_id,$posts_id,$post_user_id);
    
    if($res['res'] == 1){
   
        $this->output->set_output(json_encode(array('is_polled'=>true,'message'=>"successfully polled",'status'=>true)));
    }
    
    else if($res['res'] == -1){

        $this->output->set_output(json_encode(array('is_polled'=>false,'message'=>'successfully unpolled','status'=>true)));
    
    }
    }
    else{
        $this->output->set_output(json_encode(array('message'=>"parameter missing",'status'=>false)));
    }
    
    }

    // poll group members
    function pollgroupMembers(){

        $this->output->set_content_type('application/json');
        
        $followerid  = $this->input->post('followerid');
        $userid      = $this->input->post('userid');
        $groupid     = $this->input->post('groupid');

        if(!empty($followerid && $userid && $groupid)){

        $res = $this->GroupApiModel->pollgroupMembers($userid , $followerid , $groupid);
        
        switch($res){
            
            case 1:
            $this->output->set_output(json_encode(array('message' => 'Successfully polled' , 'status' => 'true')));
            break;

            case -1:
            $this->output->set_output(json_encode(array('message' => 'Already polled' , 'status' => 'false')));
            break;

            case 0:
            $this->output->set_output(json_encode(array('message' => 'Failed to poll' , 'status' => 'false')));
            break;
                
        }
            }
            else{

            $this->output->set_output(json_encode(array('message' => 'parameter missing' , 'status' => 'false')));
            
        }
     
    }

    // confirm party user poll
    function confirmGroupuserpoll(){

        $this->output->set_content_type('application/json');

        $followerid  = $this->input->post('followerid');
        $userid      = $this->input->post('userid');
        $groupid     = $this->input->post('groupid');

        if(!empty($followerid && $userid && $groupid)){

            $res = $this->GroupApiModel->confirmpollgroupusers($userid , $followerid , $groupid);
            
            switch($res){
                
                case 1:
                $this->output->set_output(json_encode(array('message' => 'Successfully polled' , 'status' => 'true')));
                break;
    
                    
                case 0:
                $this->output->set_output(json_encode(array('message' => 'Failed to poll' , 'status' => 'false')));
                break;
                    
            }
                }
                else{
    
                $this->output->set_output(json_encode(array('message' => 'parameter missing' , 'status' => 'false')));
                
            }

          }

          // remove poll group user
          function unpollGroupmember(){

            $this->output->set_content_type('application/json');

            $followerid  = $this->input->post('followerid');
          
    
            if(!empty($followerid)){
    
                $res = $this->GroupApiModel->removePollGroupmember($followerid);
                
                switch($res){
                    
                    case 1:
                    $this->output->set_output(json_encode(array('message' => 'Successfully unpolled' , 'status' => 'true')));
                    break;
        
                        
                    case 0:
                    $this->output->set_output(json_encode(array('message' => 'Failed to unpoll' , 'status' => 'false')));
                    break;
                        
                }
                    }
                    else{
        
                    $this->output->set_output(json_encode(array('message' => 'parameter missing' , 'status' => 'false')));
                    
                }
    
          }


        // block group posts
        function blockgroupPosts(){
            
            $this->output->set_content_type('application/json');
            $postid  = $this->input->post('postid');

            if(!empty($postid)){
    
                $res = $this->GroupApiModel->blockgroupPosts($postid);
                
                
                switch($res){

                    case 1:
                    $this->output->set_output(json_encode(array('message' => 'successfully blocked' , 'status' => 'true')));
                    break;

                    case 0:
                    $this->output->set_output(json_encode(array('message' => 'successfully unblocked' , 'status' => 'true')));
                    break;

                    case -1:
                    $this->output->set_output(json_encode(array('message' => 'operation failed' , 'status' => 'false')));
                    break;

                } 
                
            }
            else{
                $this->output->set_output(json_encode(array('message' => 'parameter missing' , 'status' => 'false')));
            
            }

        }

        // remove members by admin
        function removegroupMember(){

            $this->output->set_content_type('application/json');
            
            $groupid  = $this->input->post('groupid');
            $userid   = $this->input->post('userid');

            if(!empty($userid && $groupid)){
    
                $res = $this->GroupApiModel->removegroupMember($groupid , $userid);
                
                if($res > 0){

                    $remove_followers = $this->GroupApiModel->removegroupMemberfollowers($groupid , $userid);
                    $this->output->set_output(json_encode(array('message'=>'successfully removed','status'=>true)));
                
                }
                
                else{
                    $this->output->set_output(json_encode(array('message'=>"operation failed",'status'=>false)));
                }
               
                
            }
            else{
                $this->output->set_output(json_encode(array('message' => 'parameter missing' , 'status' => 'false')));
            
            }
        }

        // get group members
        function getGroupmembers(){
            
            $this->output->set_content_type('application/json');
            
            $groupid        = $this->input->post('groupid');
            $loggeduserid   = $this->input->post('userid');

            if(!empty($groupid)){
    
                $res = $this->GroupApiModel->getGroupmembers($groupid);
              
            foreach($res['data'] as $key => $val){

                $res['data'][$key]['poll_count']  = $this->GroupApiModel->memberPollcount($res['data'][$key]['user_id'] , $groupid);  
                $isPolled                         = $this->GroupApiModel->isMemberPolled($loggeduserid , $res['data'][$key]['user_id'] , $groupid);
                $res['data'][$key]['is_Polled']   = $isPolled > 0 ? 1 : 0;
                $res['data'][$key]['users_photo'] = $this->media_url."assets/images/users/".$res['data'][$key]['users_photo'];
            
                $is_admin                         = $this->GroupApiModel->isGroupadmin($res['data'][$key]['user_id'],$groupid);
                $res['data'][$key]['isAdmin']     = ($is_admin > 0) ? 1 :0;
            
            }
            
               
              if($res['data'] != null){

                $this->output->set_output(json_encode(array('data' => $res['data'] , 'status' => 'true')));
            
              }
              else{
                $this->output->set_output(json_encode(array('message' => 'no data found' , 'status' => 'false')));
            
              }
                
            }
            else{
                $this->output->set_output(json_encode(array('message' => 'parameter missing' , 'status' => 'false')));
            
            }

        }

        // invite members to group
        function inviteMembers(){

            $this->output->set_content_type('application/json');
            
            $userid             = $this->input->post('userid');
            $loggeduserid       = !empty($this->input->post('loggeduserid')) ? $this->input->post('loggeduserid') : 2;
            $groupid            = $this->input->post('groupid');
            $link               = mt_rand(10000,99999);
            
            
            if(!empty($userid && $groupid)){

            for($i = 0 ; $i < sizeof($userid); $i++){

                $res        = $this->GroupApiModel->inviteMembers($userid[$i] , $groupid ,$link , $loggeduserid);
            
            }

            if($res > 0){
                $this->output->set_output(json_encode(array('message' => "invite link send" ,'status' => true)));
            }
            else{
                $this->output->set_output(json_encode(array('message' => 'operation failed' , 'status' => false)));
            }
            
        }
        else{
            $this->output->set_output(json_encode(array('message' => "parameter missing" ,'status' => false)));
          
        }

        }
    
        // join private group
        function joinPrivategroup(){

            $this->output->set_content_type('application/json');
            
            $userid   = $this->input->post('userid');
            $groupid  = $this->input->post('groupid');
            $token    = $this->input->post('token');  
            $id       = $this->input->post('id');  

            if(!empty($userid && $groupid && $token && $id)){

            
            $res = $this->GroupApiModel->joinPrivategroup($userid , $groupid ,$token);

           
            switch($res){

                case $res > 0:
                $this->output->set_output(json_encode(array('value'=>"successfully joined",'status'=>true)));
                $this->GroupApiModel->deleteNotification($id);
                break;

                case -1;
                $this->output->set_output(json_encode(array('value'=>"no invitation found",'status'=>false)));
                break;

                case -2;
                $this->output->set_output(json_encode(array('value'=>"already joined",'status'=>false)));
                break;

                case 0:
                $this->output->set_output(json_encode(array('value'=>"operation failed",'status'=>false)));
                break;

            } 
        }
        else{
            $this->output->set_output(json_encode(array('message'=>"parameter missing",'status'=>false)));
               
        }
        }


         //  get group list
         function groupList(){
            
            $this->output->set_content_type('application/json');
            
            $userid = $this->input->post('userid');
            $scope  = $this->input->post('scope');
            $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
            $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
            $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
            $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;

            $res_recent = $this->GroupApiModel->groupList(null ,null,null, null , null);
            //$res_top    = $this->GroupApiModel->topgroupList($scope ,$country , $state , $district , $area);
            $res_joined = $this->GroupApiModel->joinedgroupList($userid,null ,null , null , null , null);
            $get_top_group = [];
            switch($scope){

                case 0:
                $get_top_group = $this->GroupApiModel->gettopCountGroupglobal();               
                break;

                case 1:
                $get_top_group = $this->GroupApiModel->gettopCountGroup('users_country' , $country); 
             
                break;

                case 2:
                $get_top_group = $this->GroupApiModel->gettopCountGroup('users_state' , $state); 
                
                break;

                case 3:
                $get_top_group = $this->GroupApiModel->gettopCountGroup('users_city' , $district); 
                
                break;

                
            }
            
            
          

           
            if($res_recent){
                      
            foreach($res_recent as $key => $value){

                $res_recent[$key]['group_profile_photo'] = $this->media_url."assets/images/group/profile/".$res_recent[$key]['group_profile_photo'];
            }
        }

        if($res_joined){
            foreach($res_joined as $key => $value){

                $res_joined[$key]['group_profile_photo'] = $this->media_url."assets/images/group/profile/".$res_joined[$key]['group_profile_photo'];
            }
        }
        if($get_top_group){
            foreach($get_top_group as $key => $value){

                $get_top_group[$key]['group_profile_photo'] = $this->media_url."assets/images/group/profile/".$get_top_group[$key]['group_profile_photo'];
            }
        }
        
       
            // if($res_recent != null){
                
                $this->output->set_output(json_encode(array('recent_groups' => $res_recent ,'joined_groups' =>$res_joined,'top_rank_groups' => $get_top_group,'status' => true)));
          
            // }
            // else{
            //     $this->output->set_output(json_encode(array('message' => "no group found" ,'status' => false)));
          
            // }
           

            
        }




        // //  get group list
        // function groupList(){
            
        //     $this->output->set_content_type('application/json');
            
        //     $userid = $this->input->post('userid');
        //     $scope  = $this->input->post('scope');
        //     $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        //     $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        //     $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        //     $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;

        //     $res_recent = $this->GroupApiModel->groupList(null ,$country , $state , $district , $area);
        //     //$res_top    = $this->GroupApiModel->topgroupList($scope ,$country , $state , $district , $area);
        //     $res_joined = $this->GroupApiModel->joinedgroupList(null,$scope ,$country , $state , $district , $area);
        //     $get_top_group = [];
        //     switch($scope){
        //         case 1:
        //         $get_top_group = $this->GroupApiModel->gettopCountGroup('users_country' , $country); 
             
        //         break;

        //         case 2:
        //         $get_top_group = $this->GroupApiModel->gettopCountGroup('users_state' , $state); 
                
        //         break;

        //         case 3:
        //         $get_top_group = $this->GroupApiModel->gettopCountGroup('users_city' , $district); 
                
        //         break;

                
        //     }
            
            
           

           
        //     if($res_recent){
                      
        //     foreach($res_recent as $key => $value){

        //         $res_recent[$key]['group_profile_photo'] = base_url()."assets/images/group/profile/".$res_recent[$key]['group_profile_photo'];
        //     }
        // }

        // if($res_joined){
        //     foreach($res_joined as $key => $value){

        //         $res_joined[$key]['group_profile_photo'] = base_url()."assets/images/group/profile/".$res_joined[$key]['group_profile_photo'];
        //     }
        // }
        // if($get_top_group){
        //     foreach($get_top_group as $key => $value){

        //         $get_top_group[$key]['group_profile_photo'] = base_url()."assets/images/group/profile/".$get_top_group[$key]['group_profile_photo'];
        //     }
        // }
        //     if($res_recent != null){
                
        //         $this->output->set_output(json_encode(array('recent_groups' => $res_recent ,'joined_groups' =>$res_joined,'top_rank_groups' => $get_top_group,'status' => true)));
          
        //     }
        //     else{
        //         $this->output->set_output(json_encode(array('message' => "no group found" ,'status' => false)));
          
        //     }
           

            
        // }

    //     // get group invite members
    //     function getGroupInvitemembers(){
            
    //         $this->output->set_content_type('application/json');
    //         $userid         = $this->input->post('userid');
    //         if(!empty($userid)){

    //         $res_users      = $this->GroupApiModel->getGroupInvitemembers($userid,null); 


    //         $res_followers  = $this->GroupApiModel->getGroupInvitemembers(null,$userid); 



            
    //         foreach($res_users as $row){
    //             $res_followers[] = $row;
    //         }
    //         $user_array = array();
    //         foreach($res_followers as $row){
                
    //             if(!in_array($row['user_id'],$user_array)) {  
    //                 $user_array[] = $row['user_id']; 
    //                 if($row['user_id'] != $userid){
    //                     $res['value'][] = $row;
    //                 }
    //             }
    //         }

            

          
    //         foreach($res as $value => $key){
                
    //             foreach($res[$value] as $k => $v){
    //                 $res[$value][$k]['pollCount']   = $this->GroupApiModel->getPollCount($res[$value][$k]['user_id']);
    //                 $res[$value][$k]['users_photo'] = base_url()."assets/images/users/".$res[$value][$k]['users_photo'];
        
    //             }
    //         }


            
    //         if($res_followers != null){

    //             $res['status'] = true;
    //             $this->output->set_output(json_encode($res));
    //         }
    //         else{
    //             $this->output->set_output(json_encode(array('value' =>'no data found','status' => false)));
    //         }
    //     }
    //     else{
    
    //         $this->output->set_output(json_encode(array('value' =>'parameter missing','status' => true)));

    //     }
    // }

  // get group invite members
  function getGroupInvitemembers(){
            
    $this->output->set_content_type('application/json');
    $userid         = $this->input->post('userid');
    if(!empty($userid)){

    $res_users      = $this->GroupApiModel->getGroupInvitemembers($userid,null); 


    //$res_followers  = $this->GroupApiModel->getGroupInvitemembers(null,$userid); 


   
  
    // foreach($res_users as $row){
    //     $res_followers[] = $row;
    // }
    
   
    
   if($res_users){
       
    $res = array();        
    $user_array = array();
    foreach($res_users as $row){
        
        if(!in_array($row['user_id'],$user_array)) {  
            $user_array[] = $row['user_id']; 
            if($row['user_id'] != $userid){
                $res[] = $row;
            }
        }
    }
    
  

   
    if($res){
    foreach($res as $value => $key){
        
        
            $res[$value]['pollCount']   = $this->GroupApiModel->getPollCount($res[$value]['user_id']);
            $res[$value]['users_photo'] = $this->media_url."assets/images/users/".$res[$value]['users_photo'];

        
    }
    
     $response = $res;    
     $status = "true";
    }
    else{
      
      $response = [];
      $status = "false";
    }

        $this->output->set_output(json_encode(array('value' => $response , 'status' => $status)));
    
   }
   
    else{
        $this->output->set_output(json_encode(array('value' =>[],'status' => false)));
    }
}
else{

    $this->output->set_output(json_encode(array('value' =>'parameter missing','status' => false)));

}
}

     // get created group list
     function getcreatedGroup(){

        $this->output->set_content_type('application/json');
        $id = $this->input->post('userid');
        
        if(!empty ($id)){
        $res_created = $this->GroupApiModel->getcreatedGroup($id);
        $res_joined  = $this->GroupApiModel->getjoinedGroup($id);
            
        foreach($res_created as $value => $key){

            $res_created[$value]['group_profile_photo'] = $this->media_url."assets/images/group/profile/".$res_created[$value]['group_profile_photo'];
        }

        foreach($res_joined as $value => $key){

            $res_joined[$value]['group_profile_photo'] = $this->media_url."assets/images/group/profile/".$res_joined[$value]['group_profile_photo'];
        }

            $this->output->set_output(json_encode(array('message' => "value found",'created_group' => $res_created,'joined_group' => $res_joined,'status'=>true)));
        

        }
        else{
            $this->output->set_output(json_encode(array('message' => "userid required",'status'=>false)));
       
        }


    }

     
    function requestjoinGroup(){

        $this->output->set_content_type('application/json');
        
        $userid = $this->input->post('userid');
        $groupid= $this->input->post('groupid');

        if(!empty ($userid && $groupid)){
        $res = $this->GroupApiModel->requestjoinGroup($userid , $groupid);
        switch ($res){

            case 1:
            $this->output->set_output(json_encode(array('message' => "Successfully requested","isJoined" => -1,'status'=>true)));
            break;

            case 0:
            $this->output->set_output(json_encode(array('message' => "Failed to request","isJoined" => 0,'status'=>false)));
            break;

            case -1:
            $this->output->set_output(json_encode(array('message' => "Already requested","isJoined" => 0,'status'=>false)));
        }
    }
    else{
        $this->output->set_output(json_encode(array('message' => "Parameter missing",'status'=>false)));
    
    }
        
    }

    // get join reqeusts group

    function getGrouprequests(){
       
        $this->output->set_content_type('application/json');
        $groupid = $this->input->post('groupid');
        
        if(!empty($groupid)){
            $res = $this->GroupApiModel->getGrouprequests($groupid);
            if($res != 0){
                foreach($res as $value => $key){

                    $res[$value]['users_photo'] = $this->media_url."assets/images/users/".$res[$value]['users_photo'];
                    $res[$value]['pollCount']   = $this->GroupApiModel->getPollCount($res[$value]['user_id']);

                }
                $this->output->set_output(json_encode(array('message' => "Value found",'data' => $res,'status'=>true)));
  
            }
            else{
                $this->output->set_output(json_encode(array('message' => "No requests found",'status'=>false)));
  
            }
        }
        
        else{
            $this->output->set_output(json_encode(array('message' => "parameter missing",'status'=>false)));
 
        }
    }

    function approveGrouprequests(){

        $this->output->set_content_type('application/json');
        
        $groupid   = $this->input->post('groupid');
        $userid    = $this->input->post('userid');
        $requestid = $this->input->post('request_id');
        $type      = $this->input->post('type');          // 1 => approve , -1 => reject
       
        if(!empty($requestid && $type && $groupid && $userid)){
           
        $insert = array('group_followers_group_id' => $groupid , 'group_followers_user_id' => $userid);
        
       
        switch($type){
            
            case -1:
            $res = $this->GroupApiModel->rejectGrouprequests($requestid);
            if($res > 0){
                $msg    = "successfully rejected";
                $status = "true";
            }
            else{
                $msg    = "operation failed";
                $status = "false";
            }
            break;

            case 1:
            
            $res = $this->GroupApiModel->approveGrouprequests($requestid , $insert);

            if($res == 1){
                $msg    = "successfully approved";
                $status = "true";
            }
            else if($res == 0){

                $msg    = "operation failed";
                $status = "false";
            }

            else if($res == -1){
                $msg    = "no request found";
                $status = "false";
            }
            break;
        }
        
            $this->output->set_output(json_encode(array('message' => $msg,'status'=>$status)));
 
        }
        else{
            $this->output->set_output(json_encode(array('message' => "parameter missing",'status'=>false)));
 
        }

    }

                   

     // get created group list
     function getrecentGrouplist(){

        $this->output->set_content_type('application/json');
       
        $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;
        
       
        $res_recent = $this->GroupApiModel->groupList(null ,null , null , null ,null);
    
        
        if($res_recent != null){
        foreach($res_recent as $value => $key){

            $res_recent[$value]['group_profile_photo'] = $this->media_url."assets/images/group/profile/".$res_recent[$value]['group_profile_photo'];
        }


            $this->output->set_output(json_encode(array('message' => "value found",'recent_group' => $res_recent,'status'=>true)));
        }
        else{
            $this->output->set_output(json_encode(array('message' => "no value found",'recent_group' => [],'status'=>false)));

        }

        }




        // get top group list
     function gettopGrouplist(){

        $this->output->set_content_type('application/json');
       
        $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;
        
        $res_top     = $this->GroupApiModel->gettopgroup($country , $state , $district , $area);
        
         
        
        if($res_top != null){
        foreach($res_top as $value => $key){

            $res_top[$value]['group_profile_photo'] = $this->media_url."assets/images/group/profile/".$res_top[$value]['group_profile_photo'];
        }


            $this->output->set_output(json_encode(array('message' => "value found",'top_group' => $res_top,'status'=>true)));
        }
        else{
            $this->output->set_output(json_encode(array('message' => "no value found",'top_group' => [],'status'=>false)));

        }

        }
      


        // get joined group list
     function getjoinedGrouplist(){

        $this->output->set_content_type('application/json');
       
        $userid      = $this->input->post('userid');
        $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;
        
       if(!empty($userid)){
        
        $res_joined = $this->GroupApiModel->joinedgroupList($userid,null ,null , null , null , null);

        if($res_joined != null){
        foreach($res_joined as $value => $key){

            $res_joined[$value]['group_profile_photo'] = $this->media_url."assets/images/group/profile/".$res_joined[$value]['group_profile_photo'];
        }


            $this->output->set_output(json_encode(array('message' => "value found",'joined_group' => $res_joined,'status'=>true)));
        }
        else{
            $this->output->set_output(json_encode(array('message' => "no value found",'joined_group' => [],'status'=>false)));

        }
    }
    else{
        $this->output->set_output(json_encode(array('message' => "parameter missing",'status'=>false)));

    }


}

   // delete party posts posts
   function deletegroupPosts(){

    $this->output->set_content_type('application/json');
    
    $postid     = $this->input->post('postid');
    $groupid    = $this->input->post('groupid');
    $userid     = $this->input->post('userid');
    $page_start = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;
  

    if(!empty($postid)){

    $res        = $this->GroupApiModel->deletegroupPosts($postid);
    if($res > 0){


        $posts = $this->returngetgroupProfile($groupid,$userid,$page_start);
              
       
        if(count($posts) >= 9 )
        $last_posts = $posts[9];
        else 
        $last_posts = null;
         
            
        $delete_notifications = $this->GroupApiModel->deletenotifications($postid);


        
        
        if($last_posts == null)
        $this->output->set_output(json_encode(array('value' => "operation success",'is_completed' => 1,'status'=>true)));
        
        else{
        if($last_posts['is_completed'] > 0)
        $this->output->set_output(json_encode(array('value' => "operation success",'data' => array($last_posts),'is_completed' => 0,'status'=>true)));
        
        else
        $this->output->set_output(json_encode(array('value' => "operation success",'data' => array($last_posts),'is_completed' => 1,'status'=>true)));
        }



        // if($last_posts['group_posts_id'] == $postid || $last_posts == null)
        // $this->output->set_output(json_encode(array('value' => "successfully deleted",'is_completed'=>1,'status'=>true)));
        // else
        // $this->output->set_output(json_encode(array('value' => "successfully deleted",'data' => array($last_posts),'is_completed' => $last_posts['is_completed'] > 0 ? 0 : 1,'status'=>true)));
                 

            
    } 
    else{
        $this->output->set_output(json_encode(array('value' => 'operation failed' , 'status' => false))); 
    }
    }
    else{
        $this->output->set_output(json_encode(array('value' => 'parameter missing' , 'status' => false))); 

    }

}


      // report posts
      function reportgroupPost(){

        $this->output->set_content_type('application/json');

        $user_id     = $this->input->post('user_id');
        $post_id     = $this->input->post('post_id');
        $groupid     = $this->input->post('groupid');
        $msg         = $this->input->post('message');
        $page_start  = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;

        
        if(!empty($user_id && $post_id)){
           
        $res         = $this->GroupApiModel->reportgroupPost($user_id , $post_id , $msg);

           
        switch($res){

            case $res > 0:

            $posts = $this->returngetgroupProfile($groupid,$user_id,$page_start);
              
            if(count($posts) >= 9 )
            $last_posts = $posts[9];
            else 
            $last_posts = null;
         
            // if($last_posts['group_posts_id'] == $post_id || $last_posts == null)
            // $this->output->set_output(json_encode(array('value' => "successfully reported",'is_completed'=>1,'status'=>true)));
            // else
            // $this->output->set_output(json_encode(array('value' => "successfully reported",'data' => array($last_posts),'is_completed' => $last_posts['is_completed'] > 0 ? 0 : 1,'status'=>true)));
               
           
            
            if($last_posts == null)
            $this->output->set_output(json_encode(array('value' => "operation success",'is_completed' => 1,'status'=>true)));
            
            else{
            if($last_posts['is_completed'] > 0)
            $this->output->set_output(json_encode(array('value' => "operation success",'data' => array($last_posts),'is_completed' => 0,'status'=>true)));
            
            else
            $this->output->set_output(json_encode(array('value' => "operation success",'data' => array($last_posts),'is_completed' => 1,'status'=>true)));
            }
        
           

            $this->GroupApiModel->updategroupReportcount($post_id);
            $removePosts = $this->GroupApiModel->removePostsbygroupreportcount($post_id);
            
          
            break;

            case -1:
            $this->output->set_output(json_encode(array('value' => "already reported",'status'=>false)));
            break;
            
            case 0:
            $this->output->set_output(json_encode(array('value' => "operation failed",'status'=>false)));
            break;

        }
    }
        else{
            $this->output->set_output(json_encode(array('value' => "parameter missing",'status'=>false)));
           
        }
     
    }

     // hide posts
     function hidegroupPosts(){

        $this->output->set_content_type('applcation/json');
        
        $postid     = $this->input->post('postid');
        $userid     = $this->input->post('userid');
        $groupid    = $this->input->post('groupid');
        $page_start = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;

          

        if(!empty($postid && $userid)){

        $res        = $this->GroupApiModel->hidePosts($postid , $userid);
        if($res > 0){


            $posts = $this->returngetgroupProfile($groupid,$userid,$page_start);
              
           
           if(count($posts) >= 9 )
            $last_posts = $posts[9];
           else 
            $last_posts = null;


            
            
            if($last_posts == null)
            $this->output->set_output(json_encode(array('value' => "operation success",'is_completed' => 1,'status'=>true)));
            
            else{
            if($last_posts['is_completed'] > 0)
            $this->output->set_output(json_encode(array('value' => "operation success",'data' => array($last_posts),'is_completed' => 0,'status'=>true)));
            
            else
            $this->output->set_output(json_encode(array('value' => "operation success",'data' => array($last_posts),'is_completed' => 1,'status'=>true)));
            }

         
            // if($last_posts['group_posts_id'] == $postid || $last_posts == null)
            // $this->output->set_output(json_encode(array('value' => "operation success",'is_completed'=>1,'status'=>true)));
            // else
            // $this->output->set_output(json_encode(array('value' => "operation success",'data' => array($last_posts),'is_completed' => $last_posts['is_completed'] > 0 ? 0 : 1,'status'=>true)));
               
        
                     
        } 
        else{
            $this->output->set_output(json_encode(array('value' => 'operation failed' , 'status' => false))); 
        }
        }
        else{
            $this->output->set_output(json_encode(array('value' => 'parameter missing' , 'status' => false))); 
   
        }
    
    }


    // edit posts
    function editgroupPosts(){

        $this->output->set_content_type('applcation/json');
        
       
        $postid       = $this->input->post('postid');
        $title        = $this->input->post('title');
        $content      = $this->input->post('content');  
        $linecount    = $this->input->post('group_posts_line_count'); 

        if(!empty($postid)){

        
        $update     = array('group_posts_title' => $title ,'group_posts_line_count' => $linecount,'group_posts_uploaded_date' => date('Y-m-d h:m:s'),'group_posts_content' => $content);
        $res        = $this->GroupApiModel->editPosts($postid , $update);
        if($res > 0){

            $this->output->set_output(json_encode(array('value' => 'successfully updated' , 'status' => true)));   
        } 
        else{
            $this->output->set_output(json_encode(array('value' => 'operation failed' , 'status' => false))); 
        }
     }
        else{
            $this->output->set_output(json_encode(array('value' => 'parameter missing' , 'status' => false))); 
   
        }
       
        
     
    }



    // get posts api by posts id
    function getnotificationgroupPostsbyid(){

            
        $this->output->set_content_type('application/json');
       
        $postsid = $this->input->post('postid');
        $userid  = $this->input->post('userid');
        
        if(!empty($postsid)){

        $res                  = $this->GroupApiModel->getnotificationgroupPostsbyid($postsid);
       
        $pollcount            = $this->GroupApiModel->grouppollsCount($postsid);
        $isPostpolled         = $this->GroupApiModel->isPostpolled($postsid,$userid);
        $commentcount         = $this->GroupApiModel->grouppostscommentsCountanddata($postsid);
        
        $commentsCountouter   = $this->GroupApiModel->commentsCountouter($postsid);

       
        foreach($commentcount['data'] as $co => $va){
            
            $commentcount['data'][$co]['users_photo'] = $this->media_url."assets/images/users/".$commentcount['data'][$co]['users_photo']; 
        }
        if($res > 0){
            
            
            foreach($res as $key => $val){

                $res[$key]['group_posts_photo_video']       = $this->media_url."assets/images/groupposts/".$res[$key]['group_posts_photo_video'];
                $res[$key]['group_posts_video_thumbnail']   = $this->media_url."assets/images/groupposts/thumbnail/".$res[$key]['group_posts_video_thumbnail'];
                $res[$key]['users_photo']                   = $this->media_url."assets/images/users/".$res[$key]['users_photo'];
                $res[$key]['posts_poll_count']              = $pollcount;
                $res[$key]['posts_comment_count']           = $commentsCountouter;
                $res[$key]['comment']                       = $commentcount['data'];
                $res[$key]['isPolled']                      = $isPostpolled > 0 ? 1 : 0;
            }
            $this->output->set_output(json_encode(array('value' => $res , 'status' => true))); 
        }
        else{
            $this->output->set_output(json_encode(array('value' => 'no data found' , 'status' => false))); 
        }
       
        }
        else{

            $this->output->set_output(json_encode(array('value' => 'parameter missing' ,'status' => false)));
        }
    }



}
?>
