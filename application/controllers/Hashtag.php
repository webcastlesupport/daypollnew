<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class HashTag extends CI_Controller{
    
    private $loader = array('pageview'=>'undefined');
    function __construct() {
        
        parent::__construct();
        $this->load->model('HashTagModel');
        
    }
    
    function isAuthorised(){

        if(empty($this->session->userdata('sess_admin_id'))){
            redirect('Login');
        }
    }
    
    // load group page
    function hashtag(){
        
        $this->isAuthorised();
        $this->loader['pageview']="Hashtag";
        $this->load->view('common/template',$this->loader);
    }

     // load group page
     function blockedgroupposts(){
        
        $this->isAuthorised();
        $this->loader['pageview']="blockedgroupposts";
        $this->load->view('common/template',$this->loader);
    }
    

      // block posts by post id
      function blockgroupposts(){

        $flag   = "";
        $this->output->set_content_type('application/json');
        $id     = $this->input->post('id');
        $active = $this->input->post('active');
        
        if($active == 1)$flag = 0;
        if($active == 0)$flag = 1;
        
        $res    = $this->GroupModel->blockgroupposts($id,$flag);
        $this->output->set_output(json_encode(array('details'=>$res)));
    }

    // get all group by list
    function getallGroup(){
        
        $this->isAuthorised();
        $this->output->set_content_type('application/json');
        $res    = $this->GroupModel->getallParty();
        $this->output->set_output(json_encode(array('values'=>$res)));
    }

    // block group by id
    function blockgroup(){
        $this->output->set_content_type('application/json');
        
        $groupid = $this->input->post('id');
        $active  = $this->input->post('active');
        if($active==1)$flag=0;
        if($active==0)$flag=1;
        $res    = $this->GroupModel->blockgroup($groupid , $flag);
        $this->output->set_output(json_encode(array('values'=>$res)));
    }

    //get group posts
    function viewPosts($id = null){

        if(!empty($id)){
        $res = $this->GroupModel->viewPosts($id); 

        if($res != null){
            $this->loader['data']   = $res;   
        }
        else{
            $this->loader['data']   = "";
            //echo "no value found";

        }
        }
        else{
            echo "parameter missing";
        }
        $this->isAuthorised();
        $this->loader['pageview']= "GroupPosts";   
        $this->load->view('common/template',$this->loader);
    }


      //get group posts
      function viewMembers($id = null){

        if(!empty($id)){
        $res = $this->GroupModel->viewMembers($id); 

        if($res != null){
            $this->loader['data']   = $res;   
        }
        else{
            $this->loader['data']   = "";
            }
        }
        else{
            echo "parameter missing";
        }
        $this->isAuthorised();
        $this->loader['pageview']= "GroupMembers";   
        $this->load->view('common/template',$this->loader);
    }


      // block posts by post id
      function blockposts(){

        $flag   = "";
        $this->output->set_content_type('application/json');
        $id     = $this->input->post('id');
        $active = $this->input->post('active');
        
        if($active == 1)$flag = 0;
        if($active == 0)$flag = 1;
        
        $res    = $this->GroupModel->blockposts($id,$flag);
        $this->output->set_output(json_encode(array('details'=>$res)));
    }


     // get group posts details by id
     function getgroupPostsdetails(){

        $this->output->set_content_type('application/json');
        $id             = $this->input->post('id');
        $res            = $this->GroupModel->getgroupPostsdetails($id);
        $data           = $res[0];
        $photo_or_video = $data['group_posts_photo_video'];
        $ext            = substr($photo_or_video, strpos($photo_or_video, ".") + 1);   
        $ext_           = ($ext=='mp4' ? $ext_value = "video":$ext_value = "photo");
        
        $data['group_posts_uploaded_date']  = date('d/m/Y  h:i A' ,strtotime($data['group_posts_uploaded_date'])); 

        $data['group_posts_photo_video']    = base_url()."assets/images/groupposts/".$data['group_posts_photo_video'];
       
        $this->output->set_output(json_encode(array('details'=>$data,'photo_or_video'=>$ext_)));
        
        unset($ext);

    }

    // block hash tag 
    function blockHashTag(){

        $this->output->set_content_type('application/json');
        $id             = $this->input->post('id');
        $active         = $this->input->post('active')==1 ? 0 : 1;

        $res            = $this->HashTagModel->blockHashTag($id,$active);
       
        if($res > 0)
        $this->output->set_output(json_encode(array('status' => true)));
        else
        $this->output->set_output(json_encode(array('status' => false)));

    }

    //get hashtag by id
    function getHashTagbyId(){

        $this->output->set_content_type('application/json');
        $id             = $this->input->post('id');
        $res            = $this->HashTagModel->getHashTagbyId($id);
        
        if($res)
        $this->output->set_output(json_encode(array('data' => $res[0],'status' => true)));
        else
        $this->output->set_output(json_encode(array('data' => 'no data found','status' => false)));
    }

    //update hashtag
    function updateHashTag(){

        $this->output->set_content_type('application/json');
        $id             = $this->input->post('id');
        $name           = $this->input->post('name');
        $check_name     = $this->HashTagModel->checkHashTag($name);
        if($check_name > 0){
            $this->output->set_output(json_encode(array('msg' => 'Hashtag already exists','status' => true)));
        
        }
        else{
            $res            = $this->HashTagModel->updateHashTag($id,$name);
       
            if($res)
            $this->output->set_output(json_encode(array('msg' => 'successfully updated','status' => true)));
            else
            $this->output->set_output(json_encode(array('msg' => 'failed to update','status' => false)));
        }
        
    }
}

// Most of the comments are specified with appropriate function , left of the comments can be changed
// Dead codes can be removed
// Function with 580 lines in the challenge section , reason is that in that function nearly 6 conditions are checking and file uploading
// code alighnment  in correct manner
// For codeigniter framework we doesnt use any migration plugins,All changes done here by manually
// For validation required validation is checking in all database insert functions