<?php
/**
 * Created by PhpStorm.
 * User: user56
 * Date: 06-08-2018
 * Time: 13:17
 */

class Login extends CI_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model(array('UserModel','AdminModel'));
        $this->load->library('form_validation');
    }


    // get admin logo on login screen
    function getAdminloginphoto(){

        $this->output->set_content_type('application/json');
        $username   = $this->input->post('user');
        $res        = $this->AdminModel->getAdminloginPhoto($username);
        if(isset($res)) {

            $this->output->set_output(json_encode(array('photo' => base_url()."assets/images/admin/".$res, 'error' => 0)));
        }
        else{
            $this->output->set_output(json_encode(array('photo' => base_url()."assets/images/admin/none.png", 'error' => 1)));
        }

    }

    // load dashboard
    public function index(){

        if(!empty($this->session->userdata('sess_admin_id'))){
            redirect('Dashboard');
        }
        else {
            $this->load->view('Login');
        }
    }

    
    // check username and password admin section
    public function check_login(){

        $this->output->set_content_type('application/json');
        $username   =  $this->input->post('username');
        $password   =  hash('sha256',$this->input->post('password'));



      if(!empty($username && $password)){

       
          $check_user   = $this->UserModel->check_user($username,$password);

          if(!isset($check_user['error'])) {


              $user     = $check_user['data'][0];
              $this->session->set_userdata('sess_admin_logdate',$user['admin_logdate']);
              $this->AdminModel->updateLogindate($user['admin_id'], date('d-m-Y H:i A'));
              $this->session->set_userdata('sess_admin_id', $user['admin_id']);
              $this->session->set_userdata('sess_admin_username', $user['admin_username']);

              $this->session->set_userdata('sess_admin_firstname', $user['admin_firstname']);
              $this->session->set_userdata('sess_admin_lastname', $user['admin_lastname']);
              $this->session->set_userdata('sess_admin_photo', $user['admin_photo']);

              $this->output->set_output(json_encode(array('url'=>base_url().'Dashboard','message' => ' succesfully validated', 'error' => 0)));
          }

          else{

              $this->output->set_output(json_encode(array('message' => ' invalid username or password', 'error' => 1)));
          }
      }
      else{

          $this->output->set_output(json_encode(array('message'=>' username and password required','error'=>1)));
      }


    }

    function sendEmailpwd(){

        $this->output->set_content_type('application/json');
        $email   =  $this->input->post('email');

        $newpass=mt_rand(10000,9999);
        //$res = $this->AdminModel->sendEmailpwd($email);
    }

}