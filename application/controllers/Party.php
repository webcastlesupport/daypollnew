<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


 //wexep@digital-message.coms  ASD123##
class Party extends CI_Controller{
    
    private $loader = array('pageview'=>'undefined');
    public $s3_url   = "https://s3.amazonaws.com/daypoll/";
    
    function __construct() {
        
        parent::__construct();
        $this->load->model('PartyModel');
        $this->load->library('form_validation');
        
    }
    
    function isAuthorised(){

        if(empty($this->session->userdata('sess_admin_id'))){
            redirect('Login');
        }
    }
    
    // load party page
    function party(){
        
        $this->isAuthorised();
        $this->loader['pageview']="Party";
        $this->load->view('common/template',$this->loader);
    }

     // load party page
     function pollpartylist($id = null){
      
        $this->isAuthorised();
        $this->loader['data']     = $this->PartyModel->getPollpartyMembers($id);
        $this->loader['pageview'] = "partypolllist";
        $this->load->view('common/template',$this->loader);
    }


    // load party posts page
    function partyposts(){

        $this->isAuthorised();
        $this->loaders['pageview'] = 'Partyposts';
        $this->load->view('common/template',$this->loaders);

    }

    // load blocked posts page
    function blockedpartyposts(){

        $this->isAuthorised();
        $this->loaders['pageview'] = 'Blockedpartyposts';
        $this->load->view('common/template',$this->loaders);

    }

     // load blocked posts page
     function partyrestrictednations(){

        $this->isAuthorised();
        $this->loaders['pageview']  = 'Partyrestrictednations';
        $this->loaders['data']      = $this->PartyModel->getrestrictednations();
        $this->loaders['countries'] = $this->PartyModel->getCountry();
        $this->load->view('common/template',$this->loaders);

    }

    //remove party restriction 
    function removerestrictedParty(){
        $this->output->set_content_type('application/json');
        $id  = $this->input->post('id');
        $res = $this->PartyModel->removerestrictedParty($id);
        if($res)
        $this->output->set_output(json_encode(['message' => "successfully removed" , 'error' => 0]));
        else
        $this->output->set_output(json_encode(['message' => "failed to remove",'d'=>$id , 'error' => 1]));
    }

    //save party restricted countries
    function saveRestrictedCountry(){

        $this->output->set_content_type('application/json');
        $data = $this->input->post('data');
        $flag = 0;
        for($i = 0; $i<sizeof($data);$i++){
        $res  = $this->PartyModel->saveRestrictedCountry($data[$i]); 
        if($res > 0)
            $flag++;
        }    
        if($flag > 0) 
        $this->output->set_output(json_encode(['message' => 'successfully saved']));
        else
        $this->output->set_output(json_encode(['message' => 'failed to save']));
    }


     // block posts by post id
     function blockpartyposts(){

        $flag   = "";
        $this->output->set_content_type('application/json');
        $id     = $this->input->post('id');
        $active = $this->input->post('active');
        
        if($active == 1)$flag = 0;
        if($active == 0)$flag = 1;
        
        $res    = $this->PartyModel->blockpartyposts($id,$flag);
        $this->output->set_output(json_encode(array('details'=>$res)));
    }


      // get posts details by id
      function getPartyPostsdetails(){

        $this->output->set_content_type('application/json');
        $id             = $this->input->post('id');
        $res            = $this->PartyModel->getPartyPostsdetails($id);
      
        
        $data           = $res[0];
        $photo_or_video = $data['party_posts_photo_video'];
        $ext            = substr($photo_or_video, strpos($photo_or_video, ".") + 1);   
        $ext_           = ($ext=='mp4' ? $ext_value = "video":$ext_value = "photo");


        $post_title                 = rtrim($data['party_posts_title'],"\\"); 
        $post_title_json            = '{"party_posts_title": "'.$post_title.'"}';
        $post_json_decoded          = json_decode($post_title_json);
        $data['party_posts_title']  = $post_json_decoded->{'party_posts_title'};


        $post_content               = rtrim($data['party_posts_content'],"\\"); 
        $post_content_json          = '{"party_posts_content": "'.$post_content.'"}';
        $post_json_decoded          = json_decode($post_content_json);
        $data['party_posts_content']= $post_json_decoded->{'party_posts_content'};

        $data['party_posts_uploaded_date']  = date('d/m/Y  h:i A' ,strtotime($data['party_posts_uploaded_date'])); 
        $data['party_posts_photo_video'] = $this->s3_url."assets/images/partyposts/".$data['party_posts_photo_video'];   
       
        $this->output->set_output(json_encode(array('details'=>$data,'photo_or_video'=>$ext_)));
        
        unset($ext);

    }




     // create party page
     function Createparty(){
        
        $this->isAuthorised();
        $this->loader['pageview']="Createparty";
        $this->load->view('common/template',$this->loader);
    }
    
    // get all party by list
    function getallParty(){
        
        $this->isAuthorised();
        $this->output->set_content_type('application/json');
        $res    = $this->PartyModel->getallParty();
        $this->output->set_output(json_encode(array('values'=>$res)));
    }
    
    // block party by id
    function blockparty(){

        $flag   = "";
        $this->output->set_content_type('application/json');
        $id     = $this->input->post('id');
        $active = $this->input->post('active');
        if($active==1)$flag=0;
        if($active==0)$flag=1;
        $res    = $this->PartyModel->blockparty($id,$flag);
        $this->output->set_output(json_encode(array('details'=>$res)));
    }
    
    // approve party by admin
    function approveParty(){

        $flag   = "";
        $this->output->set_content_type('application/json');
        $id     = $this->input->post('id');
        $active = $this->input->post('active');
        if($active==1)$flag=0;
        if($active==0)$flag=1;
        $res    = $this->PartyModel->approveParty($id,$flag);
        $this->output->set_output(json_encode(array('details'=>$res)));
    }
    
    // get party details
    function getpartydetails(){
        
        $this->output->set_content_type('application/json');
        $id                 = $this->input->post('id');
        $res                = $this->PartyModel->getpartydetails($id);
        $getfollowercount   = $this->PartyModel->getfollowercount($id);
        $getpublicpollcount = $this->PartyModel->getpublicpollcount($id);
        $getsecretpollcount = $this->PartyModel->getsecretpollcount($id);
        
        foreach ($res as $key => $value){
            $res[$key]['party_photo']           = base_url()."assets/images/party/".$res[$key]['party_photo'];
            $res[$key]['party_cover_photo']     = base_url()."assets/images/party/cover/".$res[$key]['party_cover_photo'];
           
            $res[$key]['country']               = $this->PartyModel->getcountryidbyname($res[$key]['party_country']);
            $res[$key]['state']                 = $this->PartyModel->getstateidbyname($res[$key]['party_state']);
            $res[$key]['party_created_date']    = date('d-m-Y', strtotime($res[$key]['party_created_date']));
        }
        
        $this->output->set_output(json_encode(array('values'=>$res[0],'publicpolls' => $getpublicpollcount,'secretpolls' => $getsecretpollcount,'followers' => $getfollowercount)));
        
    }
    
    // get state by id
    function getstatebyid(){
        
        $this->output->set_content_type('application/json');
        
        $cid       = $this->input->post('cid');
        $res       = $this->PartyModel->getstatebyid($cid);

        if($res != null){
            $this->output->set_output(json_encode(array('values' => $res , 'error' =>0)));
        
        }
        else{
            $this->output->set_output(json_encode(array('error' => 1)));
        
        }
    }

     // get state by id
     function getdistrictbyid(){
        
        $this->output->set_content_type('application/json');
        
        $cid       = $this->input->post('cid');
        $res       = $this->PartyModel->getdistrictbyid($cid);

        if($res != null){
            $this->output->set_output(json_encode(array('values' => $res , 'error' =>0)));
        
        }
        else{
            $this->output->set_output(json_encode(array('error' => 1)));
        
        }
    }

    // save party
    function saveParty(){


        $this->output->set_content_type('application/json');
        
        
        $party_short_name   = $this->input->post('partyshortname');
        $party_name         = $this->input->post('partyname');
        $president          = $this->input->post('pname');
        $secretary          = $this->input->post('sname');
        $email              = $this->input->post('email');
        $scope              = $this->input->post('scope');
        
        $country            = $this->input->post('country');
        $state              = $this->input->post('state');
        $city               = $this->input->post('district');
        //$area       = $this->input->post('area');
        
        $this->form_validation->set_rules("partyshortname","partyshortname","required");
        $this->form_validation->set_rules("partyname","partyname","required");
        $this->form_validation->set_rules("pname","pname","required");
        $this->form_validation->set_rules("sname","sname","required");
        $this->form_validation->set_rules("email","email","required");
        $this->form_validation->set_rules("scope","scope","required");
        // $this->form_validation->set_rules("mobile","mobile","required");
        // $this->form_validation->set_rules("password","password","required");
        //$this->form_validation->set_rules("privacy","privacy","required");
        $this->form_validation->set_rules("country","country","required");
        $this->form_validation->set_rules("state","state","required");
        $this->form_validation->set_rules("district","district","required");
        //$this->form_validation->set_rules("area","area","required");
        
        if($this->form_validation->run()==TRUE){
            
            $statename      = $this->PartyModel->getstatename($state);
            $countryname    = $this->PartyModel->getcountryname($country);

            $insert = array('party_name' => $party_name ,'party_email' => $email ,'party_president' => $president,
                            'party_secretary' => $secretary , 'party_created_by' => 1 ,'party_country' => $countryname,
                            'party_state' => $statename ,'party_city' => $city ,'party_scope' => $scope ,'party_code' => $party_short_name 
                            );
            $res = $this->PartyModel->createParty($insert);

            if($res > 0){

               
                $this->output->set_output(json_encode(array('message'=>'successfully inserted','status'=>true)));
            }
            else{

                $this->output->set_output(json_encode(array('message'=>'failed to insert','status'=>false)));
            }


        }
        else{

                $this->output->set_output(json_encode(array('message'=>validation_errors(),'status'=>'false')));
        } 
    }




    // save party
    function editParty(){


        $this->output->set_content_type('application/json');
        
        
        $party_id           = $this->input->post('partyeditid');
        $party_short_name   = $this->input->post('editpartyshortname');
        $party_name         = $this->input->post('editpartyname');
        $president          = $this->input->post('editpname');
        $secretary          = $this->input->post('editsname');
        $email              = $this->input->post('editemail');
        $scope              = $this->input->post('editscope');
        
        $country            = $this->input->post('editcountry');
        $state              = $this->input->post('editstate');
        $city               = $this->input->post('editdistrict');
            
        $this->form_validation->set_rules("editpartyshortname","editpartyshortname","required");
        $this->form_validation->set_rules("editpartyname","partyname","required");
        $this->form_validation->set_rules("editpname","editpname","required");
        $this->form_validation->set_rules("editsname","editsname","required");
        $this->form_validation->set_rules("editemail","editemail","required");
        $this->form_validation->set_rules("editscope","editscope","required");
        $this->form_validation->set_rules("partyeditid","partyeditid","required");
        // $this->form_validation->set_rules("password","password","required");
        //$this->form_validation->set_rules("privacy","privacy","required");
        $this->form_validation->set_rules("editcountry","editcountry","required");
        $this->form_validation->set_rules("editstate","editstate","required");
        $this->form_validation->set_rules("editdistrict","editdistrict","required");
        //$this->form_validation->set_rules("area","area","required");
        
        if($this->form_validation->run() == TRUE){
            
            $statename      = $this->PartyModel->getstatename($state);
            $countryname    = $this->PartyModel->getcountryname($country);

            $insert = array('party_name' => $party_name ,'party_email' => $email ,'party_president' => $president,
                            'party_secretary' => $secretary , 'party_created_by' => 1 ,'party_country' => $countryname,
                            'party_state' => $statename ,'party_city' => $city ,'party_scope' => $scope,
                            'party_code' => $party_short_name 
                            );
            $res = $this->PartyModel->editParty($party_id,$insert);

            if($res > 0){

               
                $this->output->set_output(json_encode(array('message'=>'successfully updated','status'=>true)));
            }
            else{

                $this->output->set_output(json_encode(array('message'=>'failed to insert','status'=>false)));
            }


        }
        else{

                $this->output->set_output(json_encode(array('message'=>validation_errors(),'status'=>'false')));
        } 
    }

 // upload admin profile picture
 function uploadcoverpic()
 {

     $this->output->set_content_type('application/json');
     $this->isAuthorised();
     $this->load->helper('form');

     $config['upload_path']      = "./assets/images/party/cover/";
     $config['allowed_types']    = 'jpeg|jpg|png';
     $config['file_name']        = 'party-cover'.mt_rand(100000,999999);
     $config['max_height']       = '800';
     $config['max_width']        = '1000';
     //$config['min_height']       = '300';
     //$config['min_width']        = '300';
     $config['max_size']         = '300kb';
     $config["overwrite"]        = false;

     $this->load->library('upload',$config);
     if(!$this->upload->do_upload("coverpic"))
     {
         $error  = array('error' => $this->upload->display_errors());
         $this->output->set_output(json_encode(['message' =>"file is not specified or not supported (size should be below 300kb)<br>Allowed type should be png,gif or jpeg type<br> Width X Height (1000px X 800px) ", 'error'=>1]));
         return false;
     }
     else
     {   
         $partyid= $this->input->post('coverpicid');  
         $data   = $this->upload->data();
         $path   = base_url()."assets/images/party/cover/".$this->PartyModel->getPartycoverphoto($partyid);
         $option = false;
         if(file_exists($path) && $path != "cover-none.png")
         {
             $option = true;
             unlink($path);
         }

         $result     = $this->PartyModel->uploadpartycoverpic($data['file_name'],$partyid);
         

         $this->output->set_output(json_encode(['message' =>"Your request was successfully performed!..",'error'=>"0"]));
     }
 }



    

      // upload admin profile picture
      function uploadpartypic()
      {
  
          $this->output->set_content_type('application/json');
          $this->isAuthorised();
          $this->load->helper('form');
  
          $config['upload_path']      = "./assets/images/party/";
          $config['allowed_types']    = 'jpeg|jpg|png';
          $config['file_name']        = 'party'.mt_rand(100000,999999);
          $config['max_height']       = '500';
          $config['max_width']        = '500';
          //$config['min_height']       = '300';
          //$config['min_width']        = '300';
          $config['max_size']         = '300kb';
          $config["overwrite"]        = false;
  
          $this->load->library('upload',$config);
          if(!$this->upload->do_upload("propic"))
          {
              $error  = array('error' => $this->upload->display_errors());
              $this->output->set_output(json_encode(['message' =>"file is not specified or not supported (size should be below 300kb)<br>Allowed type should be png,gif or jpeg type<br> Width X Height (440px X 440px) ", 'error'=>1]));
              return false;
          }
          else
          {   
              $partyid= $this->input->post('propicid');  
              $data   = $this->upload->data();
              $path   = base_url()."assets/images/party/".$this->PartyModel->getPartyphoto($partyid);
              $option = false;
              if(file_exists($path) && $path != "none.png")
              {
                  $option = true;
                  unlink($path);
              }
  
              $result     = $this->PartyModel->uploadpartypic($data['file_name'],$partyid);
              
  
              $this->output->set_output(json_encode(['message' =>"Your request was successfully performed!..",'error'=>"0"]));
          }
      }
   
}