<?php

class PartyClaim extends CI_Controller{

    private $loader = array('pageview'=>'undefined');

    function __construct() {
        
        parent::__construct();
        $this->load->model('PartyClaimModel');
        
    }

    function isAuthorised(){

        if(empty($this->session->userdata('sess_admin_id'))){
            redirect('Login');
        }
    }


    // load party claim page
    function partyclaim(){
            
        $this->isAuthorised();
        $this->loader['pageview']="PartyClaim";
        $this->load->view('common/template',$this->loader);
    }


    function getPartyclaim(){


    }
}

?>