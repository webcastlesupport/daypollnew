<?php

class PostShare extends CI_controller{

    private $data = array();

    function __construct(){

        parent::__construct();
        $this->load->model('PostviewModel');
        
       
    }

    // postview page details by id
    function postview(){

        $type                  = $_GET['type'];
        $id                    = $_GET['postid'];
        
        $data = [];
        if($type == 0) 
        {   
            
           
            $this->data['value']        = $this->PostviewModel->getPostsbyid($id);
            $this->data['pollcount']    = $this->PostviewModel->getPollcount($id);
            $this->data['commentcount'] = $this->PostviewModel->getcommentcount($id);


            if($this->data['value'] !=null){  
 

                foreach($this->data['value'] as $key => $val){
    
                   
                    $post_title                 = rtrim($this->data['value'][$key]['posts_title'],"\\"); 
                    $post_title_json            = '{"posts_title": "'.$post_title.'"}';
                    $post_json_decoded          = json_decode($post_title_json);
                    $post_title                 = $post_json_decoded->{'posts_title'};
                   
                    $post_content               = rtrim($this->data['value'][$key]['posts_content'],"\\"); 
                    $post_content_json          = '{"posts_content": "'.$post_content.'"}';
                    $post_content_json_decoded  = json_decode($post_content_json);

                   

                    $post_content               = $post_content_json_decoded->{'posts_content'};
    
                   
                    $this->data['value'][$key]['posts_title']   = $post_title;
                    $this->data['value'][$key]['posts_content'] = $post_content;

                   
            
                }
            
            }


        }
        if($type == 1) 
        {
            $this->data['value']        = $this->PostviewModel->getpartyPostsbyid($id);
            $this->data['pollcount']    = $this->PostviewModel->getpartyPollcount($id);
            $this->data['commentcount'] = $this->PostviewModel->getpartycommentcount($id);

            if($this->data['value'] !=null){

                
               
                foreach($this->data['value'] as $key => $val){
    
                    

                    $post_title                 = rtrim($this->data['value'][$key]['party_posts_title'],"\\"); 
                    $post_title_json            = '{"party_posts_title": "'.$post_title.'"}';
                   
                    $post_json_decoded          = json_decode($post_title_json);
                    $post_title                 = $post_json_decoded->{'party_posts_title'};
                    
                    $post_content               = rtrim($this->data['value'][$key]['party_posts_content'],"\\"); 
                    $post_content_json          = '{"party_posts_content": "'.$post_content.'"}';
                    $post_content_json_decoded  = json_decode($post_content_json);
                    $post_content               = $post_content_json_decoded->{'party_posts_content'};
    
                   
                    $this->data['value'][$key]['party_posts_title']   = $post_title;
                    $this->data['value'][$key]['party_posts_content'] = $post_content;
                    
            
                }
            
            }


        }
        if($type == 2) 
        {
            $this->data['value']        = $this->PostviewModel->getgroupPostsbyid($id);
            $this->data['pollcount']    = $this->PostviewModel->getgroupPollcount($id);
            $this->data['commentcount'] = $this->PostviewModel->getgroupcommentcount($id);
       
            if($this->data['value'] !=null){

                foreach($this->data['value'] as $key => $val){
    
                   
                    $post_title                 = rtrim($this->data['value'][$key]['group_posts_title'],"\\"); 
                    $post_title_json            = '{"group_posts_title": "'.$post_title.'"}';
                    $post_json_decoded          = json_decode($post_title_json);
                    $post_title                 = $post_json_decoded->{'group_posts_title'};
                    
                    $post_content               = rtrim($this->data['value'][$key]['group_posts_content'],"\\"); 
                    $post_content_json          = '{"group_posts_content": "'.$post_content.'"}';
                    $post_content_json_decoded  = json_decode($post_content_json);
                    $post_content               = $post_content_json_decoded->{'group_posts_content'};
    
                   
                    $this->data['value'][$key]['group_posts_title']   = $post_title;
                    $this->data['value'][$key]['group_posts_content'] = $post_content;
            
                }
            
            }
       
       
       
        }
        if($type == 3) 
        {
            $this->data['value']        = $this->PostviewModel->getchallengebyid($id);
            $this->data['pollcount']    = $this->PostviewModel->getchallengePostPollcount($id);
            $this->data['commentcount'] = $this->PostviewModel->getchallengePostcommentcount($id);
       
            if($this->data['value'] !=null){

                foreach($this->data['value'] as $key => $val){
    
                   
                    $post_title                 = rtrim($this->data['value'][$key]['challenge_title'],"\\"); 
                    $post_title_json            = '{"challenge_title": "'.$post_title.'"}';
                    $post_json_decoded          = json_decode($post_title_json);
                    $post_title                 = $post_json_decoded->{'challenge_title'};
                    
                    $post_content               = rtrim($this->data['value'][$key]['challenge_content'],"\\"); 
                    $post_content_json          = '{"challenge_content": "'.$post_content.'"}';
                    $post_content_json_decoded  = json_decode($post_content_json);
                    $post_content               = $post_content_json_decoded->{'challenge_content'};
    
                   
                    $this->data['value'][$key]['challenge_title']   = $post_title;
                    $this->data['value'][$key]['challenge_content'] = $post_content;
            
                }
            
            }    
       
       
        }

        if($type == 5) 
        {   
            
           
            $this->data['value']        = $this->PostviewModel->getBusinessProfilePostsbyid($id);
            $this->data['pollcount']    = $this->PostviewModel->getBusinessProfilePostsPollcount($id);
            $this->data['commentcount'] = $this->PostviewModel->getBusinessProfilePostscommentcount($id);


            if($this->data['value'] !=null){  
 

                foreach($this->data['value'] as $key => $val){
    
                   
                    $post_title                 = rtrim($this->data['value'][$key]['bp_posts_title'],"\\"); 
                    $post_title_json            = '{"posts_title": "'.$post_title.'"}';
                    $post_json_decoded          = json_decode($post_title_json);
                    $post_title                 = $post_json_decoded->{'posts_title'};
                   
                    $post_content               = rtrim($this->data['value'][$key]['bp_posts_content'],"\\"); 
                    $post_content_json          = '{"posts_content": "'.$post_content.'"}';
                    $post_content_json_decoded  = json_decode($post_content_json);

                    $post_content               = $post_content_json_decoded->{'posts_content'};
    
                   
                    $this->data['value'][$key]['bp_posts_title']   = $post_title;
                    $this->data['value'][$key]['bp_posts_content'] = $post_content;

                }
            
            }


        }


        if($type == 6) 
        {
            $this->data['value']        = $this->PostviewModel->getBusinessProfilechallengebyid($id);
            $this->data['pollcount']    = $this->PostviewModel->getBusinessProfilechallengePollcount($id);
            $this->data['commentcount'] = $this->PostviewModel->getBusinessProfilechallengecommentcount($id);
       
            

            if($this->data['value'] !=null){

                foreach($this->data['value'] as $key => $val){
    
                   
                    $post_title                 = rtrim($this->data['value'][$key]['challenge_title'],"\\"); 
                    $post_title_json            = '{"challenge_title": "'.$post_title.'"}';
                    $post_json_decoded          = json_decode($post_title_json);
                    $post_title                 = $post_json_decoded->{'challenge_title'};
                    
                    $post_content               = rtrim($this->data['value'][$key]['challenge_content'],"\\"); 
                    $post_content_json          = '{"challenge_content": "'.$post_content.'"}';
                    $post_content_json_decoded  = json_decode($post_content_json);
                    $post_content               = $post_content_json_decoded->{'challenge_content'};
    
                   
                    $this->data['value'][$key]['challenge_title']   = $post_title;
                    $this->data['value'][$key]['challenge_content'] = $post_content;
            
                }
            
            }    
       
       
        }
       
        if($this->data['value'] !=null){

        $this->data['value']   = $this->data['value'][0];
        
        $this->load->view('postview',$this->data);
        }
        else{
            $this->load->view('postviewerror');
        }
    }


}
?>