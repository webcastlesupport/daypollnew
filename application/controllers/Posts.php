<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 8/10/2018
 * Time: 2:31 PM
 */

class Posts extends CI_Controller{

    private $loaders=array('pageview'=>'undefined','sess_admin_username'=>'undefined','sess_admin_logdate'=>'undefined');
    public $s3_url   = "https://s3.amazonaws.com/daypoll/";

    function __construct()
    {
        parent::__construct();
        $this->load->model('PostModel');
    }

    // check session values
    function isAuthorised(){

        if(empty($this->session->userdata('sess_admin_id'))){
            redirect('Login');
        }
    }


    // load posts page
    function posts(){

        $this->isAuthorised();
        $this->loaders['pageview'] = 'Posts';
        $this->load->view('common/template',$this->loaders);

    }

    // load blocked posts page
    function blockedposts(){

        $this->isAuthorised();
        $this->loaders['pageview'] = 'Blockedposts';
        $this->load->view('common/template',$this->loaders);

    }

    // load posts report page
    function postsreports(){

        $this->isAuthorised();
        $this->loaders['pageview'] = 'Postreports';
        $this->load->view('common/template',$this->loaders);

    }

    // get posts details by id
    function getPostsdetails(){

        $this->output->set_content_type('application/json');
        $id             = $this->input->post('id');
        $res            = $this->PostModel->getPostsdetails($id);
        $data           = $res[0];
        $photo_or_video = $data['posts_photo_or_video'];
        $ext            = substr($photo_or_video, strpos($photo_or_video, ".") + 1);   
        $ext_           = ($ext=='mp4' ? $ext_value = "video":$ext_value = "photo");
        
        $data['posts_uploaded_date']  = date('d/m/Y  h:i A' ,strtotime($data['posts_uploaded_date'])); 

        $data['posts_photo_or_video'] = $this->s3_url."assets/images/posts/".$data['posts_photo_or_video'];
       
        $post_title                 = rtrim($data['posts_title'],"\\"); 
        $post_title_json            = '{"posts_title": "'.$post_title.'"}';
        $post_json_decoded          = json_decode($post_title_json);
        $data['posts_title']        = $post_json_decoded->{'posts_title'};


        $post_content               = rtrim($data['posts_content'],"\\"); 
        $post_content_json          = '{"posts_content": "'.$post_content.'"}';
        $post_json_decoded          = json_decode($post_content_json);
        $data['posts_content']      = $post_json_decoded->{'posts_content'};


        $this->output->set_output(json_encode(array('details'=>$data,'photo_or_video'=>$ext_)));
        
        unset($ext);

    }

     // get posts report details by id
     function getpostreportdetails(){

        $this->output->set_content_type('application/json');
        $id             = $this->input->post('id');
        $res            = $this->PostModel->getpostreportdetails($id);
        $this->output->set_output(json_encode(array('details'=>$res)));

    }

    

    
    // block posts by post id
    function blockposts(){

        $flag   = "";
        $this->output->set_content_type('application/json');
        $id     = $this->input->post('id');
        $active = $this->input->post('active');
        
        if($active == 1)$flag = 0;
        if($active == 0)$flag = 1;
        
        $res    = $this->PostModel->blockposts($id,$flag);
        $this->output->set_output(json_encode(array('details'=>$res)));
    }
}