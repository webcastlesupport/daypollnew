<?php
//Project Name  : daypoll
//File Path     : [base_path]->application->controllers
//Last Updated  : 29-Aug-19
class Reports extends CI_Controller {

    public $s3_url      = "https://daypoll.s3.amazonaws.com/";
    private $loader     = array('pageview' => 'undefined');

    public function __construct() {
        parent::__construct();
        $this->load->model('ReportModel');
    }
    public function isAuthorised() {

        if (empty($this->session->userdata('sess_admin_id'))) {
            redirect('Login');
        }
    }
    public function challenges() {
        $this->isAuthorised();
        $this->loader['pageview']   = "challenge_report";
        $this->loader['challenges'] = $this->ReportModel
                                                        ->fetch_from_table('challenge','','challenge_id','DESC')
                                                        ->result();
        foreach($this->loader['challenges'] as $row){
            $userRow            =   $this->ReportModel
                                                ->fetch_from_table('users',array('user_id' => $row->challenge_user_id))
                                                ->row();
            $row->username      =   ($userRow) ? $userRow->users_name : '';
            $reportCount        =   $this->ReportModel
                                                ->fetch_from_table('challenge_report',array('challenge_report_challenge_id' => $row->challenge_id))
                                                ->num_rows();
            $row->reportCount   =   $reportCount;
            $pollCount          =   $this->ReportModel
                                                ->fetch_from_table('challenge_polls',array('challenge_polls_challenge_id' => $row->challenge_id))
                                                ->num_rows();
            $row->pollCount     =   $pollCount;
            $postCount          =   $this->ReportModel
                                                ->fetch_from_table('challenge_posts',array('challenge_posts_challenge_id' => $row->challenge_id,'challenge_posts_active' => '1'))
                                                ->num_rows();
            $row->postCount     =   $postCount;
            
        }
        $this->load->view("common/template", $this->loader);
    }
    
    public function groupPosts(){
        $this->isAuthorised();
        $this->loader['pageview']   = "goup_post_report";
        $this->loader['groups']     = $this->ReportModel
                                                    ->fetch_from_table('group_table',array('group_active' => '1'),'group_id','DESC')
                                                    ->result();
        foreach($this->loader['groups'] as $row){
            $userRow            =   $this->ReportModel
                                                ->fetch_from_table('users',array('user_id' => $row->group_created_by))
                                                ->row();
            $row->username      =   ($userRow) ? $userRow->users_name : '';
            $followersCount      =   $this->ReportModel
                                                ->fetch_from_table('group_followers',array('group_followers_group_id' => $row->group_id,'group_followers_active' => '1'))
                                                ->num_rows();
            $row->followerCount =   $followersCount;
            $postCount          =   $this->ReportModel
                                                ->fetch_from_table('group_posts',array('group_posts_group_id' => $row->group_id,'group_posts_active' => '1'))
                                                ->num_rows();
            $row->postCount     =   $postCount;
            
        }
        $this->load->view("common/template", $this->loader);
    } 
    
    public function partyPosts(){
        $this->isAuthorised();
        $this->loader['pageview']   = "party_report";
        $this->loader['posts']      = $this->ReportModel
                                                    ->fetch_from_table('party_posts',array('party_posts_active' => '1'),'party_posts_id','DESC')
                                                    ->result();
        foreach($this->loader['posts'] as $row){
            $partyRow           =   $this->ReportModel
                                                ->fetch_from_table('party',array('party_id' => $row->party_posts_party_id))
                                                ->row();
            $row->partyName     =   ($partyRow) ? $partyRow->party_name : '';
            $reportCount        =   $this->ReportModel
                                                ->fetch_from_table('party_posts_report',array('party_posts_report_post_id' => $row->party_posts_id))
                                                ->num_rows();
            $row->reportCount   =   $reportCount;
            $pollCount          =   $this->ReportModel
                                                ->fetch_from_table('party_posts_polls',array('party_posts_polls_post_id' => $row->party_posts_id))
                                                ->num_rows();
            $row->pollCount     =   $pollCount;
            
        }
        $this->load->view("common/template", $this->loader);
    } 
}

?>