<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 8/9/2018
 * Time: 3:12 PM
 */

class Users extends CI_Controller{

    private $loaders=array('pageview'=>'undefined','sess_admin_username'=>'undefined','sess_admin_logdate'=>'undefined');

    public function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->model('PartyModel');
    }

    function isAuthorised(){

        if(empty($this->session->userdata('sess_admin_id'))){
            redirect('Login');
        }
    }

    // load users page
    function users(){

        $this->isAuthorised();
        $this->loaders['pageview'] = 'Users';
        $this->load->view('common/template',$this->loaders);
    }

    function slice(){
        $char = '01/01/2019 - 02/06/2019';
        $from = explode('-',$char);
        echo $from[0].' 00:00:00';
    }

    // get all users list
    function getallUsers()
    {

        $options = "";
        $this->output->set_content_type('application/json');

        $daterange      = $this->input->post('daterange');

        $page           = $this->input->post('page');    
        $country        = $this->input->post('country');
        $state          = $this->input->post('state');
        $district       = $this->input->post('district');
        $statename   = "";
        $countryname = "";
        if($state)  $statename      = $this->UserModel->getstatename($state);
        if($country)$countryname    = $this->UserModel->getcountryname($country);
 

        $from = "";
        $to   = "";
        if($daterange){
        $newdate    = explode('-',$daterange);
       
        
        $from       = trim(date("Y-m-d", strtotime($newdate[0]))). ' 00:00:00';
        $to         = trim(date("Y-m-d", strtotime($newdate[1]))). ' 00:00:00';

        }


        $search     = $this->input->post('search');
        $select     = $this->input->post('selected');

        $sel        = "";
        
        if($select  == "")
        {
            $sel = "";
            unset($sel);
        
        }
        $option  = $this->input->post('option');
        if($option == 0){$options = 1;}
        if($option == 1){$options = 0;}

        $res     = $this->UserModel->getallUsers($page,$search,$options,$select,$from ,$to ,$countryname , $statename , $district);
        

        $all_user     = $this->UserModel->getallUsersCount($page,$search,$options,$select,$from ,$to ,$countryname , $statename , $district);
        

        if (!isset($res['error'])) {
            $num        = $all_user;
            $tot_pages  = ceil($num/9);
            $datas=$res['data'];

            foreach ($datas as $key=>$value) {

                $datas[$key]['users_created_date']=date('d/m/Y',strtotime($datas[$key]['users_created_date']));
                $datas[$key]['users_photo']= base_url()."assets/images/users/".$datas[$key]['users_photo']; 
            }
                $this->output->set_output(json_encode(['num' => $num,'page_count' => $tot_pages,'content' => $datas,'s' =>$options, 'error' => "0"]));
            
        } 
        else {
            $this->output->set_output(json_encode(['content' =>"not found",'error'=>"1",'select'=>$select]));
        }
        
    }
    
    // get user details by id
    function getuserDetails(){
        
         $this->output->set_content_type('application/json');
         $userid        = $this->input->post('userid');
         $res           = $this->UserModel->getuserDetails($userid);
         $getFollowers  = $this->UserModel->getFollowers($userid);
         $getPostscount = $this->UserModel->getPostscount($userid);
         
         foreach ($res as $key=>$value) {

                $res[$key]['users_created_date'] = date('d/m/Y',strtotime($res[$key]['users_created_date']));
                $res[$key]['users_photo']        = base_url()."assets/images/users/".$res[$key]['users_photo'];
                $res[$key]['users_login_date'] = date('d/m/Y',strtotime($res[$key]['users_login_date']));

        
            }
         $this->output->set_output(json_encode(['content' =>$res[0],'posts'=>$getPostscount,'followers'=>$getFollowers]));
    }

    // block user by id
    function blockuser(){
        
        $this->output->set_content_type('application/json');
        $userid        = $this->input->post('userid');
        $res           = $this->UserModel->blockuser($userid);
        if($res > 0){
            
        $this->output->set_output(json_encode(['message' => 'succesfully blocked' , 'error' => 0]));
        }
        else{
            $this->output->set_output(json_encode(['message' => 'failed to block' , 'error' => 1]));
        }
    }

    // block user by id
    function unblockuser(){
        
        $this->output->set_content_type('application/json');
        $userid        = $this->input->post('userid');
        
        $res           = $this->UserModel->unblockuser($userid);
        
        if($res > 0){
            
        $this->output->set_output(json_encode(['message' => 'succesfully unblocked' , 'error' => 0]));
        }
        else{
            $this->output->set_output(json_encode(['message' => 'failed to unblock' , 'error' => 1]));
        }
    }

    // get state by id
    function getstatebyid(){
        
        $this->output->set_content_type('application/json');
        
        $cid       = $this->input->post('cid');
        $res       = $this->PartyModel->getstatebyid($cid);

        if($res != null){
            $this->output->set_output(json_encode(array('values' => $res , 'error' =>0)));
        
        }
        else{
            $this->output->set_output(json_encode(array('error' => 1)));
        
        }
    }

     // get state by id
     function getdistrictbyid(){
        
        $this->output->set_content_type('application/json');
        
        $cid       = $this->input->post('cid');
        $res       = $this->PartyModel->getdistrictbyid($cid);

        if($res != null){
            $this->output->set_output(json_encode(array('values' => $res , 'error' =>0)));
        
        }
        else{
            $this->output->set_output(json_encode(array('error' => 1)));
        
        }
    }

}