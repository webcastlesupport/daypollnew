<?php

class BusinessProfileApi extends CI_Controller{

    public $media_url= "https://s3.amazonaws.com/daypoll/";
    public $s3_url   = "https://s3.amazonaws.com/daypoll/";


    
    
    function __construct()
    {     
        error_reporting(0);
        parent::__construct();
        $this->load->model(array('BusinessProfileModel','CommonModel','PostsApiModel','ChallengeApiModel'));
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('', ''); 
        $this->load->library('aws3');
    }

    function createProfile(){
        $this->output->set_content_type('application/json');
        
        $profilename    = $this->input->post('profile_name');
        $email          = $this->input->post('email');
        $createduser    = $this->input->post('created_user');
        $website        = $this->input->post('website');
        $location       = $this->input->post('location');
        $phone          = $this->input->post('phone');
        $description    = $this->input->post('description');
        $category       = $this->input->post('category');
        $subcategory    = $this->input->post('subcategory');
        $subcategory2   = $this->input->post('subcategory2');
        $subcategory3   = $this->input->post('subcategory3');
        $lat            = $this->input->post('lat');
        $log            = $this->input->post('log');
        $country        = $this->input->post('country');
        
        
        $this->form_validation->set_rules("profile_name","profile_name","required");
        $this->form_validation->set_rules("created_user","created_user","required");
        $this->form_validation->set_rules("category","category","required");
       
        
        if($this->form_validation->run() == TRUE){

            $check_exists = $this->BusinessProfileModel
                                                ->fetch_from_table('business_profile',array('bf_created_user' => $createduser))
                                                ->num_rows();
            if($check_exists > 0)
            $this->output->set_output(json_encode(array('message' => "You already have a business profile" , 'status' => false)));                                        
            else{
                $insert = array('bf_name' => $profilename , 'bf_email' => $email,'bf_country_location' => $country,
                                'bf_created_user' => $createduser , 'bf_website' => $website,
                                'bf_location' => $location,'bf_phone' => $phone , 'bf_description' => $description,
                                'bf_category' => $category , 'bf_sub_category_1' => $subcategory , 'bf_sub_category_3' => $subcategory2 ,
                                'bf_sub_category_3' => $subcategory3 , 'bf_lat' => $lat , 'bf_log' => $log
                                );

                
                $res = $this->BusinessProfileModel->insert_table('business_profile' , $insert);
                
              



                if(isset($_FILES['profile_photo'])){

                    $errors_thumb    = array();
                    $file_name_thumb = $_FILES['profile_photo']['name'];
                    $file_size_thumb = $_FILES['profile_photo']['size'];
                    $file_tmp_thumb  = $_FILES['profile_photo']['tmp_name'];
                    $file_type_thumb = $_FILES['profile_photo']['type'];
                    $tmp_thumb       = explode('.', $file_name_thumb);
                    $file_ext_thumb  = end($tmp_thumb);

            
                    $thumb_name = "bf-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                    
                    $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                    
                    if(in_array($file_ext_thumb,$expensions_thumb)=== false){

                    $errors_thumb[]="extension not allowed, please choose a JPEG or PNG file.";
                    
                }
                                        
                    if(empty($errors_thumb)==true){
                   
                        $data['tmp_name']  = $_FILES['profile_photo']['tmp_name'];
                        $data['file_name'] = $thumb_name;
        
       
                        $val = $this->aws3->sendFile('daypoll/assets/images/businessprofile',$data);   
                  
                        $profile_image = $thumb_name;
                       
                        $save_image    = $this->BusinessProfileModel->update_table('business_profile' , array('bf_id' => $res) , array('bf_photo' => $profile_image));
                    
                    }else{
                      
                        $profile_image = "none.jpg";
                    }
                }



                if(isset($_FILES['profile_cover_photo'])){

                    $errors_thumb    = array();
                    $file_name_thumb = $_FILES['profile_cover_photo']['name'];
                    $file_size_thumb = $_FILES['profile_cover_photo']['size'];
                    $file_tmp_thumb  = $_FILES['profile_cover_photo']['tmp_name'];
                    $file_type_thumb = $_FILES['profile_cover_photo']['type'];
                    $tmp_thumb       = explode('.', $file_name_thumb);
                    $file_ext_thumb  = end($tmp_thumb);

            
                    $thumb_name = "bf_cover-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                    
                    $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                    
                    if(in_array($file_ext_thumb,$expensions_thumb)=== false){

                    $errors_thumb[]="extension not allowed, please choose a JPEG or PNG file.";
                    
                }
                                        
                    if(empty($errors_thumb)==true){
                   
                        $data['tmp_name']  = $_FILES['profile_cover_photo']['tmp_name'];
                        $data['file_name'] = $thumb_name;
        
       
                        $val = $this->aws3->sendFile('daypoll/assets/images/businessprofile/cover',$data);   
                  
                        $cover_image = $thumb_name;
                       
                        $save_image    = $this->BusinessProfileModel->update_table('business_profile' , array('bf_id' => $res) , array('bf_cover_photo' => $cover_image));
                    
                    }else{
                      
                        $cover_image = "none.jpg";
                    }
                }

                if($res > 0){
                    $this->output->set_output(json_encode(array('message' => "successfully created" , 'status' => true)));
                }else{
                    $this->output->set_output(json_encode(array('message' => "operation failed" , 'status' => false)));
                }
            }
        }else{
            $this->output->set_output(json_encode(array('message' => "parameter missing" , 'status' => false)));
        }
        
    }



    function updateProfile(){
        $this->output->set_content_type('application/json');
        
        $profileid      = $this->input->post('profileid');
        $profilename    = $this->input->post('profile_name');
        $email          = $this->input->post('email');
        
       
        $website        = $this->input->post('website');
        $location       = $this->input->post('location');
        $phone          = $this->input->post('phone');
        $description    = $this->input->post('description');
        $lat            = $this->input->post('lat');
        $log            = $this->input->post('log');
        $country        = $this->input->post('country');
      
        
        
        $this->form_validation->set_rules("profile_name","profile_name","required");
       
       
        
        if($this->form_validation->run() == TRUE){

                $insert = array('bf_name' => $profilename , 'bf_email' => $email,'bf_website' => $website,
                                'bf_location' => $location,'bf_phone' => $phone ,
                                'bf_lat' => $lat,'bf_log' => $log ,'bf_country_location' => $country,
                                'bf_description' => $description
                                );

                
                $res = $this->BusinessProfileModel->update_table('business_profile' ,array('bf_id' => $profileid),$insert);
                
                if(isset($_FILES['profile_photo'])){

                    $errors_thumb    = array();
                    $file_name_thumb = $_FILES['profile_photo']['name'];
                    $file_size_thumb = $_FILES['profile_photo']['size'];
                    $file_tmp_thumb  = $_FILES['profile_photo']['tmp_name'];
                    $file_type_thumb = $_FILES['profile_photo']['type'];
                    $tmp_thumb       = explode('.', $file_name_thumb);
                    $file_ext_thumb  = end($tmp_thumb);

            
                    $thumb_name = "bf-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                    
                    $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                    
                    if(in_array($file_ext_thumb,$expensions_thumb)=== false){

                    $errors_thumb[]="extension not allowed, please choose a JPEG or PNG file.";
                    
                }
                                        
                    if(empty($errors_thumb)==true){
                   
                        $data['tmp_name']  = $_FILES['profile_photo']['tmp_name'];
                        $data['file_name'] = $thumb_name;
        
       
                        $val = $this->aws3->sendFile('daypoll/assets/images/businessprofile',$data);   
                  
                        $profile_image = $thumb_name;
                       
                        $save_image    = $this->BusinessProfileModel->update_table('business_profile' , array('bf_id' => $profileid) , array('bf_photo' => $profile_image));
                    
                    }else{
                      
                        $profile_image = "none.jpg";
                    }
                }



                if(isset($_FILES['profile_cover_photo'])){

                    $errors_thumb    = array();
                    $file_name_thumb = $_FILES['profile_cover_photo']['name'];
                    $file_size_thumb = $_FILES['profile_cover_photo']['size'];
                    $file_tmp_thumb  = $_FILES['profile_cover_photo']['tmp_name'];
                    $file_type_thumb = $_FILES['profile_cover_photo']['type'];
                    $tmp_thumb       = explode('.', $file_name_thumb);
                    $file_ext_thumb  = end($tmp_thumb);

            
                    $thumb_name = "bf_cover-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                    
                    $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                    
                    if(in_array($file_ext_thumb,$expensions_thumb)=== false){

                    $errors_thumb[]="extension not allowed, please choose a JPEG or PNG file.";
                    
                }
                                        
                    if(empty($errors_thumb)==true){
                   
                        $data['tmp_name']  = $_FILES['profile_cover_photo']['tmp_name'];
                        $data['file_name'] = $thumb_name;
        
       
                        $val = $this->aws3->sendFile('daypoll/assets/images/businessprofile/cover',$data);   
                  
                        $cover_image = $thumb_name;
                       
                        $save_image    = $this->BusinessProfileModel->update_table('business_profile' , array('bf_id' => $profileid) , array('bf_cover_photo' => $cover_image));
                    
                    }else{
                      
                        $cover_image = "none.jpg";
                    }
                }

                if($res > 0){
                    $this->output->set_output(json_encode(array('message' => "successfully updated" , 'status' => true)));
                }else{
                    $this->output->set_output(json_encode(array('message' => "operation failed" , 'status' => false)));
                }
            
        }else{
            $this->output->set_output(json_encode(array('message' => "parameter missing" , 'status' => false)));
        }
        
    }


    function getBusinessProfilebyid(){

        $this->output->set_content_type('application/json');

        $zone = getallheaders();
        $GMT  = new DateTimeZone("GMT");
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";


        $dt = new DateTime();      
        $current_date = $dt->format('d-m-Y h:i A');
       
      
        $date = new DateTime($current_date, $GMT );
        $date->setTimezone(new DateTimeZone($zone['timezone']));
        $date =   $date->format('Y-m-d h:i A');


        $id     = $this->input->post('id');
        $userid = $this->input->post('userid');

        if(!empty($id)){

            $rank = $this->BusinessProfileModel->getBusinessProfileRank();
            
            $group_rank = 0;
            $counter = 1;
           
            foreach($rank as $value => $key){
                if($rank[$value]['bpp_bp_id'] == $id){
                    $group_rank = $counter;
                }
                $counter++;
            }
            
            

            $res = $this->BusinessProfileModel
                                        ->fetch_from_table('business_profile',array('bf_id' => $id))
                                        ->result_array();
            
            $is_Polled = $this->BusinessProfileModel
                                        ->fetch_from_table('business_profile_polls',array('bpp_user_id' => $userid , 'bpp_bp_id'=>$id , 'bpp_status' => 1))
                                        ->num_rows();


            if($res){

                
                foreach($res as $value => $key){
                    $res[$value]['bf_photo']        = $this->s3_url."assets/images/businessprofile/".$res[$value]['bf_photo'];
                    $res[$value]['bf_cover_photo']  = $this->s3_url."assets/images/businessprofile/cover/".$res[$value]['bf_cover_photo'];
                    $isAdmin                        = $res[$value]['bf_created_user'] == $userid ? 1 : 0 ;
                }

            $this->output->set_output(json_encode(array('data' => $res,'rank' => $group_rank,'isAdmin' => $isAdmin,'isPolled' => $is_Polled == 0 ? 0 : 1,'status' => true)));
            }

            else{
            $this->output->set_output(json_encode(array('data' => 'no data found' , 'status' => false)));
            }
        }else{
            $this->output->set_output(json_encode(array('data' => 'parameter missing' , 'status' => false)));
        }    
    }






    function getBusinessProfilePosts(){

        $this->output->set_content_type('application/json');

        $zone = getallheaders();
        $GMT  = new DateTimeZone("GMT");
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";


        $dt = new DateTime();      
        $current_date = $dt->format('d-m-Y h:i A');
       
      
        $date = new DateTime($current_date, $GMT );
        $date->setTimezone(new DateTimeZone($zone['timezone']));
        $cdate =   $date->format('Y-m-d h:i A');

        
        $page_start     = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;

        $id             = $this->input->post('id');

        $created_user   = $this->BusinessProfileModel
                                            ->fetch_from_table('business_profile',array('bf_id'=>$id))                        
                                            ->row('bf_created_user');

                                           
        $userid         = $this->input->post('userid');

        if(!empty($id)){


            $get_hide_post          = $this->BusinessProfileModel->gethiddenposts($userid);
        
            $get_report_post        = $this->BusinessProfileModel->getReportedposts($userid);

            $get_report_challenge   = $this->BusinessProfileModel->getReportedchallenge($userid);

            $res = $this->BusinessProfileModel
                                        ->fetch_from_table('business_profile',array('bf_id' => $id))
                                        ->result_array();


            $res_posts = $this->BusinessProfileModel
                                            ->fetch_from_table('business_profile_posts',array('bp_business_profile_id'=>$id ,'bp_posts_active' => 1))                        
                                            ->result_array();

            foreach($res as $value => $key){
                $profile_image = $res[$value]['bf_photo'];
            }
            
            if($res_posts){
 

                foreach($res_posts as $val => $key){

                        $date = new DateTime($res_posts[$val]['bp_posts_uploaded_date'], $GMT );
                        $date->setTimezone(new DateTimeZone($zone['timezone']));
                        $res_posts[$val]['bp_posts_uploaded_date'] = $date->format('Y-m-d H:i:s');


                        $pollcount                  = $this->BusinessProfileModel
                                                                            ->fetch_from_table('business_profile_posts_polls',array('bp_posts_polls_post_id' => $res_posts[$val]['bp_posts_id']))
                                                                            ->num_rows();     
                        
                        
                        $commentcount               = $this->BusinessProfileModel
                                                                            ->fetch_from_table('business_profile_posts_comments',array('bp_posts_comments_post_id' => $res_posts[$val]['bp_posts_id']))
                                                                            ->num_rows(); 

                        $commentdata               = $this->BusinessProfileModel
                                                                            ->fetch_from_table('business_profile_posts_comments',array('bp_posts_comments_post_id' => $res_posts[$val]['bp_posts_id']))
                                                                            ->result_array();               

                        //$commentsCountouter         = $this->BusinessProfileModel->commentsCountouter($res_posts[$val]['bp_posts_id']);
                        $is_user_polled             = $this->BusinessProfileModel
                                                                            ->fetch_from_table('business_profile_posts_polls',array('bp_posts_polls_user_id' => $userid, 'bp_posts_polls_post_id' => $res_posts[$val]['bp_posts_id']))
                                                                            ->num_rows(); 
                        
                        $getProfile                 = $this->BusinessProfileModel
                                                                            ->fetch_from_table('business_profile',array('bf_id' => $id))
                                                                            ->result_array(); 

                        $getProfile = $getProfile[0];                                                        
                        $res_posts[$val]['bp_posts_video_thumbnail'] = $this->s3_url."assets/images/business_profile/thumbnails/".$res_posts[$val]['bp_posts_video_thumbnail'];
                      
                        
                       if($commentdata){
                       foreach($commentdata as $co => $va){
                           $commentdata[$co]['users_photo'] = $this->s3_url."assets/images/businessprofile/".$this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $id))
                                                                                                                       ->row('bf_photo'); 
                       }
                    }

                       
                       $ispolled = ($is_user_polled > 0) ? true : false;
        
                       $res_posts[$val]['created_date']     = strtotime(date("d-m-Y h:i A",strtotime($res_posts[$val]['bp_posts_uploaded_date']))); 
                       $res_posts[$val]['comment_count']    = $commentcount; 
                       $res_posts[$val]['poll_count']       = $pollcount; 
                       $res_posts[$val]['users_username']   = $getProfile['bf_name'];
                       $res_posts[$val]['users_name']       = $getProfile['bf_name'];
                       //$res_posts[$val]['users_photo']      = $getProfile['users_photo'];
                       
                       $res_posts[$val]['is_polled']        = $ispolled;
                       $res_posts[$val]['comment']          = $commentdata;
                       
                       $res_posts[$val]['post_sub_type']    = 0;
                      
                       $res_posts[$val]['sharecode']        = $this->media_url.'post-view?postid='.$res_posts[$val]['bp_posts_id'].'&&type=5';
                       $res_posts[$val]['downloadlink']     = $res_posts[$val]['bp_posts_photo_or_video'].'?postid='.$res_posts[$val]['bp_posts_id'].'&&type=0';

                       $res_posts[$val]['bp_posts_photo_or_video'] = $this->s3_url."assets/images/business_profile/".$res_posts[$val]['bp_posts_photo_or_video'];
                       $res_posts[$val]['users_photo']             = $this->s3_url."assets/images/businessprofile/".$profile_image;
                          
                    }
               }


               $getChallenge       =  $this->BusinessProfileModel
                                                            ->fetch_from_table('business_profile_challenge',array('challenge_profile_id'=>$id ,'challenge_active' => 1))                        
                                                            ->result_array();

     

               if($getChallenge != null){
                   
                    

                   foreach($getChallenge as $value => $key){
                       
                       //$getChallenge[$value]['created_date']     =  strtotime(date("d-m-Y h:i A",strtotime($getPosts['data'][$key]['posts_uploaded_date']))); 
                       $getChallenge[$value]['users_username']   = $getProfile['bf_name'];
                       $getChallenge[$value]['users_name']       = $getProfile['bf_name'];

                       $getChallenge[$value]['created_date']  =  strtotime($getChallenge[$value]['challenge_created_date']);
                       
                       
                       // time validity section
       
                       $now            = new DateTime($zone['timezone']);
                      
                    
       
                       $expdate = $getChallenge[$value]['challenge_expired_date']." ".$getChallenge[$value]['challenge_expired_time'];
                       
                      
                       $expired_date   = date("Y-m-d H:i:s ", strtotime($expdate));
                       
                       $current_date = strtotime($cdate);
                       $exp_date     = strtotime($expdate);
                       
                       $diff   = $exp_date - $current_date;
                       $getChallenge[$value]['diff'] = $diff;
                       
                       $getChallenge[$value]['challenge_status']    = $diff > 0 ? 1 : 0;
                      
                       $expired_date   = new DateTime($expired_date);    
                       
                       $now_new     = $now->format('Y-m-d H:i:s');
                       $expired_new = $expired_date->format('Y-m-d H:i:s');
                       
                       $now_new_date       = new DateTime($now_new);
                       $expired_new_date   = new DateTime($expired_new);
                     
                       $interval       = $now_new_date->diff($expired_new_date); 
                       
                      
                       $isPolled       = $this->BusinessProfileModel->isChallengepolled($getChallenge[$value]['challenge_id'] , $userid);
                       $getChallenge[$value]['ispolled']    = $isPolled > 0 ? 1 : 0;
                      
                       
                       
                       if($diff > 0){
                      
                       $getChallenge[$value]['validity_day']     = $interval->format('%d');
                       $getChallenge[$value]['validity_hour']    = $interval->format('%h');
                       $getChallenge[$value]['validity_minute']  = $interval->format('%i');
                       $getChallenge[$value]['validity_second']  = $interval->format('%s');
                       $getChallenge[$value]['challenge_winner'] = [];
       
                     
                       }
       
                       else{
       
                       $getChallenge[$value]['validity_day']     = 0;
                       $getChallenge[$value]['validity_hour']    = 0;
                       $getChallenge[$value]['validity_minute']  = 0;
                       $getChallenge[$value]['validity_second']  = 0;
       
                       $getChallenge[$value]['challenge_winner']        = $this->BusinessProfileModel->getWinner($getChallenge[$value]['challenge_id']);
                     
                   
                       
                       if($getChallenge[$value]['challenge_winner']){
                      
                           foreach($getChallenge[$value]['challenge_winner'] as $key_1 => $value_1){
       
                            if($created_user == $getChallenge[$value]['challenge_winner'][$key_1]['bp_challenge_posts_user_id']){

                                $getChallenge[$value]['challenge_winner'][$key_1]['users_name']             = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $id))
                                                                                                              ->row('bf_name');
                                
                                $getChallenge[$value]['challenge_winner'][$key_1]['users_photo']            = $this->s3_url."assets/images/businessprofile/".$this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $id))
                                                                                                              ->row('bf_photo');  
                               }else{ 
        
                               $getChallenge[$value]['challenge_winner'][$key_1]['users_name']              = $this->BusinessProfileModel->fetch_from_table('users',array('user_id' => $getChallenge[$value]['challenge_winner'][$key_1]['bp_challenge_posts_user_id']))
                                                                                                              ->row('users_name');
                               $getChallenge[$value]['challenge_winner'][$key_1]['users_photo']             = $this->media_url."assets/images/users/".$this->BusinessProfileModel->fetch_from_table('users',array('user_id' => $getChallenge[$value]['challenge_winner'][$key_1]['bp_challenge_posts_user_id']))
                                                                                                              ->row('users_photo');                                                                     
                               }

                               

                            //   // $res[$value]['challenge_winner']['']
                            //   if($getChallenge[$value]['challenge_winner'][$key_1]['users_login_type'] == 0){
       
                              
                            //   $getChallenge[$value]['challenge_winner'][$key_1]['users_photo']             =$this->media_url."assets/images/users/".$getChallenge[$value]['challenge_winner'][$key_1]['users_photo'];
                           
                            //   }
                           if($getChallenge[$value]['challenge_type'] == 1){
       
                               $getChallenge[$value]['challenge_winner'][$key_1]['bp_challenge_posts_sub_post']       = $this->s3_url."assets/images/business_profile/challenge/type1/".$getChallenge[$value]['challenge_winner'][$key_1]['bp_challenge_posts_sub_post'];
                              
                           }
                           else{
                               $getChallenge[$value]['challenge_winner'][$key_1]['bp_challenge_posts_sub_post']       = $this->s3_url."assets/images/business_profile/challenge/type2/".$getChallenge[$value]['challenge_winner'][$key_1]['bp_challenge_posts_sub_post'];
                              
                           }
       
                           $getChallenge[$value]['challenge_winner'][$key_1]['challenge_media_type'] = $getChallenge[$value]['challenge_media_type'];
                           
                           $getChallenge[$value]['challenge_winner'][$key_1]['bp_challenge_posts_video_thumbnail'] = $this->s3_url."assets/images/business_profile/challenge/thumb/".$getChallenge[$value]['challenge_winner'][$key_1]['bp_challenge_posts_video_thumbnail'];
                           
                       }
                   }
       
                      
                }
                    
       
                       // time validtity ends here
       
       
                       if($getChallenge[$value]['challenge_posts_sub_type'] == 1) {
       
                           $explenatory_video                      = $this->BusinessProfileModel->getExplanetoryvideo($getChallenge[$value]['challenge_id']);
                           if($explenatory_video){
                           $getChallenge[$value]['explanetory_video']       = $this->s3_url."assets/images/business_profile/challenge/type1/".$explenatory_video[0]['bp_challenge_posts_sub_post']; 
                           $getChallenge[$value]['explanetory_video_thumb'] = $this->s3_url."assets/images/business_profile/challenge/thumb/".$explenatory_video[0]['bp_challenge_posts_video_thumbnail']; 
                           $getChallenge[$value]['challenge_posts_exp_media_type'] = $explenatory_video[0]['bp_challenge_posts_exp_media_type']; 
                          
                           }
                       }
       
                       $getChallenge[$value]['pollCount']               = $this->BusinessProfileModel->challengePollcount($getChallenge[$value]['challenge_id']);
                       $getChallenge[$value]['commentCount']            = $this->BusinessProfileModel->challengeCommentcount($getChallenge[$value]['challenge_id']);
                       $getChallenge[$value]['challenge_posts_post']    = $this->BusinessProfileModel->getChallengeposts($getChallenge[$value]['challenge_id']);
                       $getChallenge[$value]['comment']                 = $this->BusinessProfileModel->getChallengecomments($getChallenge[$value]['challenge_id']);
                     
                       $getChallenge[$value]['totalpollscount']         = $this->BusinessProfileModel->totalPostsCount($getChallenge[$value]['challenge_id']);
                       $getChallenge[$value]['post_sub_type']               = 1;   
                       
                       foreach($getChallenge[$value]['comment'] as $pic => $key_pic){
                           $getChallenge[$value]['comment'][$pic]['users_photo'] = $this->s3_url."assets/images/businessprofile/".$profile_image;;
                       }
                      
                   

                      
                           
                            $getChallenge[$value]['users_photo']             = $this->s3_url."assets/images/businessprofile/".$profile_image;
                          
                      
                            

       
                       foreach($getChallenge[$value]['challenge_posts_post'] as $values => $keys){
                          //$getChallenge[$value]['challenge_posts_post'][$values]['parent_position']    = $j;
                           //$getChallenge[$value]['challenge_posts_post'][$values]['users_photo']                        = $this->s3_url."assets/images/businessprofile/".$this->BusinessProfileModel
                             //                                                                                                                           ->fetch_from_table('business_profile',array('bf_id' => $getChallenge[$value]['challenge_profile_id']))
                                //                                                                                                                        ->row('bf_photo');//$this->s3_url."assets/images/users/".$getChallenge[$value]['challenge_posts_post'][$values]['users_photo']; 
                           
                                if($getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_user_id'] == $created_user ){

                                    $getChallenge[$value]['challenge_posts_post'][$values]['users_name']              = $getProfile['bf_name'];
                                    $getChallenge[$value]['challenge_posts_post'][$values]['users_photo']             = $this->s3_url."assets/images/businessprofile/".$profile_image;
                        
                                }else{
                                 
                                    $getChallenge[$value]['challenge_posts_post'][$values]['users_photo']             = $this->media_url."assets/images/users/".$getChallenge[$value]['challenge_posts_post'][$values]['users_photo'];
                                  
                               }
                           
                           
                           
                           $getChallenge[$value]['challenge_posts_post'][$values]['pollCount']                          = $this->BusinessProfileModel->challengepostPollcount($getChallenge[$value]['challenge_id'],$getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_id']);
                           if($getChallenge[$value]['totalpollscount'] > 0) {
                          
                           $getChallenge[$value]['challenge_posts_post'][$values]['pollPercent']                        = intval(($getChallenge[$value]['challenge_posts_post'][$values]['pollCount'] * 100 ) / $getChallenge[$value]['totalpollscount']);
                          
                           }
                           else{
                               $getChallenge[$value]['challenge_posts_post'][$values]['pollPercent']                    = 0;
                           }
                           $getChallenge[$value]['challenge_posts_post'][$values]['challenge_media_type']               = $getChallenge[$value]['challenge_media_type'];
                           
                           $isPolledpost       = $this->BusinessProfileModel->isChallengepostpolled($getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_id'] , $userid);
                          
                           $getChallenge[$value]['challenge_posts_post'][$values]['ispolled']                           = $isPolledpost > 0 ? 1 : 0;
       
                           if($getChallenge[$value]['challenge_type'] == 1){
       
                               $getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_sub_post']       = $this->s3_url."assets/images/business_profile/challenge/type1/".$getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_sub_post'];
                              
                           }
                           else{
                               $getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_sub_post']       = $this->s3_url."assets/images/business_profile/challenge/type2/".$getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_sub_post'];
                              
                           }
                               $getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_video_thumbnail']    = $this->s3_url."assets/images/business_profile/challenge/thumb/".$getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_video_thumbnail'];
                           
                           //$res[$value]['challenge_posts_post'][$values]['users_photo']                = base_url()."assets/images/users/".$res[$value]['challenge_posts_post'][$values]['users_photo'];
                   
                           $getChallenge[$value]['challenge_posts_post'][$values]['downloadlink']                    = $getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_sub_post'];
                      
                           $getChallenge[$value]['challenge_posts_post'][$values]['downloadlink']                    = $getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_sub_post'].'?postid='.$getChallenge[$value]['challenge_id'].'&&type=3';
                           $getChallenge[$value]['challenge_posts_post'][$values]['sharecode']                       = $this->media_url.'post-view?postid='.$getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_challenge_id'].'&&type=6';
                   
                       }
       
                   }
       
                 }   

             foreach($getChallenge as $row){
                $res_posts[] = $row;
             }
             
           
             $i = 0;
             foreach($res_posts as $row){
 
                 $totalArray[$i] = $row['created_date'];
                 $i++;
             }
             arsort($totalArray);
             
             foreach($totalArray as $tot_key => $tot_val){
                $resultArray[] =  $res_posts[$tot_key];
            
            }

            

            $k = 0;
            foreach($resultArray as $row_count => $key_count){
               
                if($resultArray[$row_count]['post_sub_type'] == 1) 
                {   
                    
                    $resultArray[$row_count]['parent_position'] = $k;
                    foreach($resultArray[$row_count]['challenge_posts_post'] as $sub_row_count => $key_row_count){
                        $resultArray[$row_count]['challenge_posts_post'][$sub_row_count]['parent_position'] = $k;
                    }
                }
                
                
             
                $k++;
               
            }

           
            $hide_array =   [];
            foreach($get_hide_post as $row){
                $hide_array[]   = $row['hide_posts_post_id'];
            }
    
            $report_array =   [];
            foreach($get_report_post as $row){
                $report_array[]   = $row['posts_report_post_id'];
            }

            $challenge_report_array =   [];
            foreach($get_report_challenge as $row){
                $challenge_report_array[]   = $row['challenge_report_challenge_id'];
            }


            
    
            $final_post_array =   [];
            foreach($resultArray as $in_row){
                if(!in_array($in_row['bp_posts_id'],$hide_array)){
                    $final_post_array[]    =  $in_row ;
                }
            }

            $final_challenge_array =   [];
            foreach($final_post_array as $in_row){
                if(!in_array($in_row['challenge_id'],$challenge_report_array)){
                    $final_challenge_array[]    =  $in_row ;
                }
            }
            
            $final_last_array = [];
            foreach($final_challenge_array as $in_row){
               if(!in_array($in_row['bp_posts_id'],$report_array)){
                   $final_last_array[]    =  $in_row ;
               }
           }

           $final_last_challenge_array = [];
           foreach($final_last_array as $in_row){
              if(!in_array($in_row['challenge_id'],$report_array)){
                  $final_last_challenge_array[]    =  $in_row ;
              }
          }

          $total_posts     = count($final_last_challenge_array); 


              
          $profile_posts_final          = array_slice($final_last_challenge_array,$page_start,10);

          $profile_posts_final_next     = array_slice($final_last_challenge_array,$page_start+10,10);




            if($profile_posts_final){

            $this->output->set_output(json_encode(array('report_challenge_id' => $get_report_challenge,'hide_posts_id' => $get_hide_post ,'report_posts_id' =>$get_report_post,'posts'=>$profile_posts_final,'total_posts' => $total_posts,'is_completed' => count($profile_posts_final_next) > 0 ? 0 : 1,'status'=>true)));
       

            //$this->output->set_output(json_encode(array('report_challenge_id' => $get_report_challenge,'hide_posts_id' => $get_hide_post ,'report_posts_id' =>$get_report_post,'posts'=>$res_posts,'total_posts'=>count($res_posts),'status' => true)));
            }

            else{
            $this->output->set_output(json_encode(array('data' => 'no data found' , 'status' => false)));
            }
        }else{
            $this->output->set_output(json_encode(array('data' => 'parameter missing' , 'status' => false)));
        }    
    }



     // delete party posts posts
     function deletePosts(){

        $this->output->set_content_type('applcaition/json');
        
        $postid         = $this->input->post('postid');
        $profileid        = $this->input->post('profileid');
        $userid         = $this->input->post('userid');
        $scope          = $this->input->post('scope'); 

        $page_start  = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;


        if(!empty($postid)){

            
        $res        = $this->BusinessProfileModel->deletePosts($postid);
        if($res > 0){

    

            $posts = $this->returnBusinessProfilePosts($userid,$userid,$page_start,$profileid);
              
            if(count($posts) >= 9 )
            $last_posts = $posts[9];
            else 
            $last_posts = null;
          
            $delete_notifications = $this->BusinessProfileModel->deletenotifications($postid);

            if($last_posts == null)
            $this->output->set_output(json_encode(array('value' => "successfully deleted",'is_completed' => 1,'status'=>true)));
            
            else{
            if($last_posts['is_completed'] > 0)
            $this->output->set_output(json_encode(array('value' => "successfully deleted",'data' => array($last_posts),'is_completed' => 0,'status'=>true)));
            
            else
            $this->output->set_output(json_encode(array('value' => "successfully deleted",'data' => array($last_posts),'is_completed' => 1,'status'=>true)));
            }


            
            // if($last_posts['party_posts_id'] == $postid || $last_posts == null)
            // $this->output->set_output(json_encode(array('value' => "successfully deleted",'is_completed'=>1,'status'=>true)));
            // else
            // $this->output->set_output(json_encode(array('value' => "successfully deleted",'data' => array($last_posts),'is_completed' => $last_posts['is_completed'] > 0 ? 0 : 1,'status'=>true)));
            
            } 
        else{
            $this->output->set_output(json_encode(array('value' => 'operation failed' , 'status' => false))); 
        }
        }
        else{
            $this->output->set_output(json_encode(array('value' => 'parameter missing' , 'status' => false))); 
   
        }
    
    }


     // hide posts
     function hidePosts(){

        $this->output->set_content_type('applcation/json');
        
        $postid     = $this->input->post('postid');
        $userid     = $this->input->post('userid');
        $profileid    = $this->input->post('profileid');
        $page_start = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;

          

        if(!empty($postid && $userid)){

        $res        = $this->BusinessProfileModel->hidePosts($postid , $userid);
        if($res > 0){


            $posts = $this->returnBusinessProfilePosts($userid,1,$page_start,$profileid);
              
           
           if(count($posts) >= 9 )
            $last_posts = $posts[9];
           else 
            $last_posts = null;


            
            
            if($last_posts == null)
            $this->output->set_output(json_encode(array('value' => "operation success",'is_completed' => 1,'status'=>true)));
            
            else{
            if($last_posts['is_completed'] > 0)
            $this->output->set_output(json_encode(array('value' => "operation success",'data' => array($last_posts),'is_completed' => 0,'status'=>true)));
            
            else
            $this->output->set_output(json_encode(array('value' => "operation success",'data' => array($last_posts),'is_completed' => 1,'status'=>true)));
            }

         
      
        
                     
        } 
        else{
            $this->output->set_output(json_encode(array('value' => 'operation failed' , 'status' => false))); 
        }
        }
        else{
            $this->output->set_output(json_encode(array('value' => 'parameter missing' , 'status' => false))); 
   
        }
    
    }


    function returnBusinessProfilePosts($userid,$logged_userid,$page_start,$id){

        $this->output->set_content_type('application/json');

        $zone = getallheaders();
        $GMT  = new DateTimeZone("GMT");
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";


        $dt = new DateTime();      
        $current_date = $dt->format('d-m-Y h:i A');
       
      
        $date = new DateTime($current_date, $GMT );
        $date->setTimezone(new DateTimeZone($zone['timezone']));
        $cdate =   $date->format('Y-m-d h:i A');

        
        $page_start  = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;

        //$id     = $this->input->post('id');
        //$userid = $this->input->post('userid');





        if(!empty($id)){


            $get_hide_post          = $this->BusinessProfileModel->gethiddenposts($userid);
        
            $get_report_post        = $this->BusinessProfileModel->getReportedposts($userid);

            $get_report_challenge   = $this->BusinessProfileModel->getReportedchallenge($userid);





            $res = $this->BusinessProfileModel
                                        ->fetch_from_table('business_profile',array('bf_id' => $id))
                                        ->result_array();


            $res_posts = $this->BusinessProfileModel
                                            ->fetch_from_table('business_profile_posts',array('bp_business_profile_id'=>$id , 'bp_posts_active' => 1))                        
                                            ->result_array();

            foreach($res as $value => $key){
                $profile_image = $res[$value]['bf_photo'];
            }
            
            if($res_posts){
 

                foreach($res_posts as $val => $key){

                        $date = new DateTime($res_posts[$val]['bp_posts_uploaded_date'], $GMT );
                        $date->setTimezone(new DateTimeZone($zone['timezone']));
                        $res_posts[$val]['bp_posts_uploaded_date'] = $date->format('Y-m-d H:i:s');


                        $pollcount                  = $this->BusinessProfileModel
                                                                            ->fetch_from_table('business_profile_posts_polls',array('bp_posts_polls_post_id' => $res_posts[$val]['bp_posts_id']))
                                                                            ->num_rows();     
                        
                        
                        $commentcount               = $this->BusinessProfileModel
                                                                            ->fetch_from_table('business_profile_posts_comments',array('bp_posts_comments_post_id' => $res_posts[$val]['bp_posts_id']))
                                                                            ->num_rows(); 

                        $commentdata               = $this->BusinessProfileModel
                                                                            ->fetch_from_table('business_profile_posts_comments',array('bp_posts_comments_post_id' => $res_posts[$val]['bp_posts_id']))
                                                                            ->result_array();               

                        //$commentsCountouter         = $this->BusinessProfileModel->commentsCountouter($res_posts[$val]['bp_posts_id']);
                        $is_user_polled             = $this->BusinessProfileModel
                                                                            ->fetch_from_table('business_profile_posts_polls',array('bp_posts_polls_user_id' => $userid, 'bp_posts_polls_post_id' => $res_posts[$val]['bp_posts_id']))
                                                                            ->num_rows(); 
                        
                        $getProfile                 = $this->BusinessProfileModel
                                                                            ->fetch_from_table('business_profile',array('bf_id' => $id))
                                                                            ->result_array(); 

                        $getProfile = $getProfile[0];                                                        
                        $res_posts[$val]['bp_posts_video_thumbnail'] = $this->s3_url."assets/images/business_profile/thumbnails/".$res_posts[$val]['bp_posts_video_thumbnail'];
                      
                        
                       if($commentdata){
                       foreach($commentdata as $co => $va){
                           $commentdata[$co]['users_photo'] = $this->s3_url."assets/images/businessprofile/".$this->BusinessProfileModel
                                                                                                                        ->fetch_from_table('business_profile',array('bf_id' => $id))
                                                                                                                        ->row('bf_photo'); 
                       }
                    }

                       
                       $ispolled = ($is_user_polled > 0) ? true : false;
        
                       $res_posts[$val]['created_date']     = strtotime(date("d-m-Y h:i A",strtotime($res_posts[$val]['bp_posts_uploaded_date']))); 
                       $res_posts[$val]['comment_count']    = $commentcount; 
                       $res_posts[$val]['poll_count']       = $pollcount; 
                       $res_posts[$val]['users_username']   = $getProfile['bf_name'];
                       $res_posts[$val]['users_name']       = $getProfile['bf_name'];
                       //$res_posts[$val]['users_photo']      = $getProfile['users_photo'];
                       
                       $res_posts[$val]['is_polled']        = $ispolled;
                       $res_posts[$val]['comment']          = $commentdata;
                       
                       $res_posts[$val]['post_sub_type']    = 0;
                      
                       $res_posts[$val]['sharecode']        = $this->media_url.'post-view?postid='.$res_posts[$val]['bp_posts_id'].'&&type=5';
                       $res_posts[$val]['downloadlink']     = $res_posts[$val]['bp_posts_photo_or_video'].'?postid='.$res_posts[$val]['bp_posts_id'].'&&type=0';



                        $res_posts[$val]['bp_posts_photo_or_video'] = $this->media_url."assets/images/business_profile/".$res_posts[$val]['bp_posts_photo_or_video'];
                        $res_posts[$val]['users_photo']             = $this->s3_url."assets/images/businessprofile/".$profile_image;
                          
                    }
               }


               $getChallenge       =  $this->BusinessProfileModel
                                                            ->fetch_from_table('business_profile_challenge',array('challenge_profile_id'=>$id ,'challenge_active' => 1))                        
                                                            ->result_array();

     

               if($getChallenge != null){
                   
                    

                   foreach($getChallenge as $value => $key){
                       
                       //$getChallenge[$value]['created_date']     =  strtotime(date("d-m-Y h:i A",strtotime($getPosts['data'][$key]['posts_uploaded_date']))); 
                       $getChallenge[$value]['users_username']   = $getProfile['bf_name'];
                       $getChallenge[$value]['users_name']       = $getProfile['bf_name'];

                       $getChallenge[$value]['created_date']  =  strtotime($getChallenge[$value]['challenge_created_date']);
                       
                       
                       // time validity section
       
                       $now            = new DateTime($zone['timezone']);
                      
                    
       
                       $expdate = $getChallenge[$value]['challenge_expired_date']." ".$getChallenge[$value]['challenge_expired_time'];
                       
                      
                       $expired_date   = date("Y-m-d H:i:s ", strtotime($expdate));
                       
                       $current_date = strtotime($cdate);
                       $exp_date     = strtotime($expdate);
                       
                       $diff   = $exp_date - $current_date;
                       $getChallenge[$value]['diff'] = $diff;
                       
                       $getChallenge[$value]['challenge_status']    = $diff > 0 ? 1 : 0;
                      
                       $expired_date   = new DateTime($expired_date);    
                       
                       $now_new     = $now->format('Y-m-d H:i:s');
                       $expired_new = $expired_date->format('Y-m-d H:i:s');
                       
                       $now_new_date       = new DateTime($now_new);
                       $expired_new_date   = new DateTime($expired_new);
                     
                       $interval       = $now_new_date->diff($expired_new_date); 
                       
                      
                       $isPolled       = $this->BusinessProfileModel->isChallengepolled($getChallenge[$value]['challenge_id'] , $id);
                       $getChallenge[$value]['ispolled']    = $isPolled > 0 ? 1 : 0;
                      
                       
                       
                       if($diff > 0){
                      
                       $getChallenge[$value]['validity_day']     = $interval->format('%d');
                       $getChallenge[$value]['validity_hour']    = $interval->format('%h');
                       $getChallenge[$value]['validity_minute']  = $interval->format('%i');
                       $getChallenge[$value]['validity_second']  = $interval->format('%s');
                       $getChallenge[$value]['challenge_winner'] = [];
       
                     
                       }
       
                       else{
       
                       $getChallenge[$value]['validity_day']     = 0;
                       $getChallenge[$value]['validity_hour']    = 0;
                       $getChallenge[$value]['validity_minute']  = 0;
                       $getChallenge[$value]['validity_second']  = 0;
       
                       $getChallenge[$value]['challenge_winner']        = $this->BusinessProfileModel->getWinner($getChallenge[$value]['challenge_id']);
                     
                   
                       
                       if($getChallenge[$value]['challenge_winner']){
                      
                           foreach($getChallenge[$value]['challenge_winner'] as $key_1 => $value_1){
       
                              // $res[$value]['challenge_winner']['']
                              if($getChallenge[$value]['challenge_winner'][$key_1]['users_login_type'] == 0){
       
                              
                              $getChallenge[$value]['challenge_winner'][$key_1]['users_photo']             =$this->media_url."assets/images/users/".$getChallenge[$value]['challenge_winner'][$key_1]['users_photo'];
                           
                              }
                           if($getChallenge[$value]['challenge_type'] == 1){
       
                               $getChallenge[$value]['challenge_winner'][$key_1]['bp_challenge_posts_sub_post']       = $this->media_url."assets/images/business_profile/challenge/type1/".$getChallenge[$value]['challenge_winner'][$key_1]['bp_challenge_posts_sub_post'];
                              
                           }
                           else{
                               $getChallenge[$value]['challenge_winner'][$key_1]['bp_challenge_posts_sub_post']       = $this->media_url."assets/images/business_profile/challenge/type2/".$getChallenge[$value]['challenge_winner'][$key_1]['bp_challenge_posts_sub_post'];
                              
                           }
       
                           $getChallenge[$value]['challenge_winner'][$key_1]['challenge_media_type'] = $getChallenge[$value]['challenge_media_type'];
                           
                           $getChallenge[$value]['challenge_winner'][$key_1]['bp_challenge_posts_video_thumbnail'] = $this->s3_url."assets/images/business_profile/challenge/thumb/".$getChallenge[$value]['challenge_winner'][$key_1]['bp_challenge_posts_video_thumbnail'];
                           
                       }
                   }
       
                      
                }
                    
       
                       // time validtity ends here
       
       
                       if($getChallenge[$value]['challenge_posts_sub_type'] == 1) {
       
                           $explenatory_video                      = $this->BusinessProfileModel->getExplanetoryvideo($getChallenge[$value]['challenge_id']);
                           if($explenatory_video){
                           $getChallenge[$value]['explanetory_video']       = $this->s3_url."assets/images/business_profile/challenge/type1/".$explenatory_video[0]['bp_challenge_posts_sub_post']; 
                           $getChallenge[$value]['explanetory_video_thumb'] = $this->s3_url."assets/images/business_profile/challenge/thumb/".$explenatory_video[0]['bp_challenge_posts_video_thumbnail']; 
                           $getChallenge[$value]['challenge_posts_exp_media_type'] = $explenatory_video[0]['bp_challenge_posts_exp_media_type']; 
                          
                           }
                       }
       
                       $getChallenge[$value]['pollCount']               = $this->BusinessProfileModel->challengePollcount($getChallenge[$value]['challenge_id']);
                       $getChallenge[$value]['commentCount']            = $this->BusinessProfileModel->challengeCommentcount($getChallenge[$value]['challenge_id']);
                       $getChallenge[$value]['challenge_posts_post']    = $this->BusinessProfileModel->getChallengeposts($getChallenge[$value]['challenge_id']);
                       $getChallenge[$value]['comment']                 = $this->BusinessProfileModel->getChallengecomments($getChallenge[$value]['challenge_id']);
                     
                       $getChallenge[$value]['totalpollscount']         = $this->BusinessProfileModel->totalPostsCount($getChallenge[$value]['challenge_id']);
                       $getChallenge[$value]['post_sub_type']               = 1;   
                       
                       foreach($getChallenge[$value]['comment'] as $pic => $key_pic){
                           $getChallenge[$value]['comment'][$pic]['users_photo'] = $this->s3_url."assets/images/businessprofile/".$profile_image;;
                       }
                      
                    //    if($getChallenge[$value]['users_login_type'] == 0 ){
                          $getChallenge[$value]['users_photo']             = $this->s3_url."assets/images/businessprofile/".$profile_image;
                          
                    //    }


       
                       foreach($getChallenge[$value]['challenge_posts_post'] as $values => $keys){
                          //$getChallenge[$value]['challenge_posts_post'][$values]['parent_position']    = $j;
                           $getChallenge[$value]['challenge_posts_post'][$values]['users_photo']                        = $this->s3_url."assets/images/users/".$getChallenge[$value]['challenge_posts_post'][$values]['users_photo']; 
                           $getChallenge[$value]['challenge_posts_post'][$values]['pollCount']                          = $this->BusinessProfileModel->challengepostPollcount($getChallenge[$value]['challenge_id'],$getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_id']);
                           if($getChallenge[$value]['totalpollscount'] > 0) {
                          
                           $getChallenge[$value]['challenge_posts_post'][$values]['pollPercent']                        = intval(($getChallenge[$value]['challenge_posts_post'][$values]['pollCount'] * 100 ) / $getChallenge[$value]['totalpollscount']);
                          
                           }
                           else{
                               $getChallenge[$value]['challenge_posts_post'][$values]['pollPercent']                    = 0;
                           }
                           $getChallenge[$value]['challenge_posts_post'][$values]['challenge_media_type']               = $getChallenge[$value]['challenge_media_type'];
                           
                           $isPolledpost       = $this->BusinessProfileModel->isChallengepostpolled($getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_id'] , $userid);
                          
                           $getChallenge[$value]['challenge_posts_post'][$values]['ispolled']                           = $isPolledpost > 0 ? 1 : 0;
       
                           if($getChallenge[$value]['challenge_type'] == 1){
       
                               $getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_sub_post']       = $this->media_url."assets/images/business_profile/challenge/type1/".$getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_sub_post'];
                              
                           }
                           else{
                               $getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_sub_post']       = $this->media_url."assets/images/business_profile/challenge/type2/".$getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_sub_post'];
                              
                           }
                               $getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_video_thumbnail']    = $this->media_url."assets/images/business_profile/challenge/thumb/".$getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_video_thumbnail'];
                           
                           //$res[$value]['challenge_posts_post'][$values]['users_photo']                = base_url()."assets/images/users/".$res[$value]['challenge_posts_post'][$values]['users_photo'];
                   
                           $getChallenge[$value]['challenge_posts_post'][$values]['downloadlink']                    = $getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_sub_post'];
                      
                           $getChallenge[$value]['challenge_posts_post'][$values]['downloadlink']                    = $getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_sub_post'].'?postid='.$getChallenge[$value]['challenge_id'].'&&type=3';
                           $getChallenge[$value]['challenge_posts_post'][$values]['sharecode']                       = $this->media_url.'post-view?postid='.$getChallenge[$value]['challenge_posts_post'][$values]['bp_challenge_posts_id'].'&&type=6';
                   
                       }
       
                   }
       
                 }   

             foreach($getChallenge as $row){
                $res_posts[] = $row;
             }
             
           
             $i = 0;
             foreach($res_posts as $row){
 
                 $totalArray[$i] = $row['created_date'];
                 $i++;
             }
             arsort($totalArray);
             
             foreach($totalArray as $tot_key => $tot_val){
                $resultArray[] =  $res_posts[$tot_key];
            
            }

           
            $hide_array =   [];
            foreach($get_hide_post as $row){
                $hide_array[]   = $row['hide_posts_post_id'];
            }
    
            $report_array =   [];
            foreach($get_report_post as $row){
                $report_array[]   = $row['posts_report_post_id'];
            }

            $challenge_report_array =   [];
            foreach($get_report_challenge as $row){
                $challenge_report_array[]   = $row['challenge_report_challenge_id'];
            }


            
    
            $final_post_array =   [];
            foreach($resultArray as $in_row){
                if(!in_array($in_row['bp_posts_id'],$hide_array)){
                    $final_post_array[]    =  $in_row ;
                }
            }

            $final_challenge_array =   [];
            foreach($final_post_array as $in_row){
                if(!in_array($in_row['challenge_id'],$challenge_report_array)){
                    $final_challenge_array[]    =  $in_row ;
                }
            }
            
            $final_last_array = [];
            foreach($final_challenge_array as $in_row){
               if(!in_array($in_row['bp_posts_id'],$report_array)){
                   $final_last_array[]    =  $in_row ;
               }
           }

           $final_last_challenge_array = [];
           foreach($final_last_array as $in_row){
              if(!in_array($in_row['challenge_id'],$report_array)){
                  $final_last_challenge_array[]    =  $in_row ;
              }
          }

          $total_posts     = count($final_last_challenge_array); 


              
          $profile_posts_final          = array_slice($final_last_challenge_array,$page_start,10);

          $profile_posts_final_next     = array_slice($final_last_challenge_array,$page_start+10,10);




            if($profile_posts_final){

                return $$profile_posts_final;
            //$this->output->set_output(json_encode(array('report_challenge_id' => $get_report_challenge,'hide_posts_id' => $get_hide_post ,'report_posts_id' =>$get_report_post,'posts'=>$profile_posts_final,'total_posts' => $total_posts,'is_completed' => count($profile_posts_final_next) > 0 ? 0 : 1,'status'=>true)));
       

            //$this->output->set_output(json_encode(array('report_challenge_id' => $get_report_challenge,'hide_posts_id' => $get_hide_post ,'report_posts_id' =>$get_report_post,'posts'=>$res_posts,'total_posts'=>count($res_posts),'status' => true)));
            }

            else{
                return "no data found";
                //$this->output->set_output(json_encode(array('data' => 'no data found' , 'status' => false)));
            }
        }else{
            $this->output->set_output(json_encode(array('data' => 'parameter missing' , 'status' => false)));
        }    
    }

    function getBusinessProfilebylist(){

        $this->output->set_content_type('application/json');
        $location       = $this->input->post('location');
        $category       = $this->input->post('cat_id');
        $scope          = $this->input->post('scope');
        $userid         = $this->input->post('userid');

        $subcategory1   = $this->input->post('sub_cat_id1');
        $subcategory2   = $this->input->post('sub_cat_id2');
        $subcategory3   = $this->input->post('sub_cat_id3');

      
        $res = $this->BusinessProfileModel->getBusinessProfilebylist($category , $subcategory1 , $subcategory2 , $subcategory3);
               
        $res_by_poll = $this->BusinessProfileModel->getProfilebyPolls($scope , $location , $category , $subcategory1 , $subcategory2 , $subcategory3);  
        
        $bf_id_array = [];
        foreach($res_by_poll as $row){
            $bf_id_array[] = $row['bf_id'];
        }

                

        $new_array = [];
        foreach($res as $row){
            if(!in_array($row['bf_id'], $bf_id_array)){
                $new_array[] = $row;
            }
        }
               
       

        foreach($new_array as $row){
            $row['total'] = 0;
            $res_by_poll[] = $row;
        }

       

        if($res_by_poll){
            foreach($res_by_poll as $value => $key){

                $getPolled   = $this->BusinessProfileModel
                                            ->fetch_from_table('business_profile_polls',array('bpp_bp_id' => $res_by_poll[$value]['bf_id'],'bpp_user_id' => $userid , 'bpp_status' => 1))
                                            ->num_rows();
                $res_by_poll[$value]['ispolled']        = $getPolled > 0 ? 1 : 0;    

                $post_count                             = $this->BusinessProfileModel
                                                                                ->fetch_from_table('business_profile_posts',array('bp_business_profile_id' => $res_by_poll[$value]['bf_id'],'bp_posts_active' => 1))
                                                                                ->num_rows();

                $challenge_count                        = $this->BusinessProfileModel
                                                                                ->fetch_from_table('business_profile_challenge',array('challenge_profile_id' => $res_by_poll[$value]['bf_id'],'challenge_active' => 1))
                                                                                ->num_rows();

                $res_by_poll[$value]['posts_count']     = $post_count + $challenge_count;                                                           
                $res_by_poll[$value]['bf_photo']        = $this->s3_url."assets/images/businessprofile/".$res_by_poll[$value]['bf_photo'];
                $res_by_poll[$value]['bf_cover_photo']  = $this->s3_url."assets/images/businessprofile/cover/".$res_by_poll[$value]['bf_cover_photo'];
            }
            $this->output->set_output(json_encode(array('data' => $res_by_poll , 'status' => true)));
            
        }else{
            $this->output->set_output(json_encode(array('data' => 'no data found' , 'status' => false)));    
        }
        
    }

    function pollBusinessProfile(){

        $this->output->set_content_type('application/json');
        
        $userid          = $this->input->post('userid');
        $bpid            = $this->input->post('profileid');
        $userprofileid   = $this->input->post('userprofileid');
        
        if(!empty($userid && $bpid && $userprofileid)){

        $res = $this->BusinessProfileModel->pollBusinessProfile($userid , $bpid , $userprofileid);

        if($res['res'] == 1){
       
            $this->output->set_output(json_encode(array('is_polled'=>true,'message'=>"successfully polled",'status'=>true)));
        }
        
        else if($res['res'] == -1){
            $this->output->set_output(json_encode(array('is_polled'=>false,'message'=>'successfully unpolled','status'=>true)));
     
        }
        }   
        else{
            $this->output->set_output(json_encode(array('message'=>"parameter missing",'status'=>false)));
        }
    
    }


     // upload user posts
    
     function uploadPosts(){                                                             

        $this->output->set_content_type('application/json');

        $thumb_name     = null;
        $userid         = $this->input->post('userid');
        $linecount      = $this->input->post('linecount');
        $posttitle      = $this->input->post('title');
        $photo_or_video = $this->input->post('photo_or_video'); // photo or video  [photo -> video -> 1 , other -> -1 ]
        $posts_content  = $this->input->post('content');
        $poststype      = $this->input->post('type');   
        $posts_width    = $this->input->post('posts_width');    
        $posts_height   = $this->input->post('posts_height');   
        $profileid      = $this->input->post('profile_id');   
        
        if(!empty($posts_content)){
        $hash = explode(" ",$posts_content);
        foreach($hash as $val){

            if($val != ""){
                if($val[0] == '#'){
                    $tags[] = $val;
                }
            }
            else{
                $tags[] = null;
            }
        }
        }

        $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;



        $this->form_validation->set_rules("userid","userid","required");
        $this->form_validation->set_rules("profile_id","profile_id","required");
        $this->form_validation->set_rules("photo_or_video","photo_or_video","required");
        //$this->form_validation->set_rules("content","content","required");

        if($linecount == NULL){
            $linecount = 0;
        }

        
        if($this->form_validation->run() == TRUE){

         
            if(!empty($tags)){
              
                for($i=0;$i<sizeof($tags);$i++){
                $insert_tags= $this->PostsApiModel->insert_tags($tags[$i]);
                } 
            }
           
           
            if($photo_or_video == -1){

                $insert = array('bp_business_profile_id' => $profileid,'bp_posts_user_id'=>$userid,'bp_photo_or_video'=>$photo_or_video,'bp_posts_title'=>$posttitle,
                'bp_posts_content'=>$posts_content,'bp_posts_linecount' => $linecount,'bp_posts_type'=>$poststype,
                'bp_posts_created_type'=>1,'bp_posts_country' => $country , 'bp_posts_state' => $state , 
                'bp_posts_city' => $district ,'bp_posts_area' => $area ,'bp_posts_tags' => implode(" ",$tags));

                $result = $this->BusinessProfileModel->insert_table('business_profile_posts',$insert);


                if($result > 0) {

                    $this->output->set_output(json_encode(['message' => "successfully uploaded ", 'status' => "true"]));
                }
                else{
                    $this->output->set_output(json_encode(['message' => "operation failed!..", 'status' => "false"]));
                }
            }
            else if($photo_or_video == 0){

                if(isset($_FILES['photo_video'])){

                    $errors    = array();
                    $file_name = $_FILES['photo_video']['name'];
                    $file_size = $_FILES['photo_video']['size'];
                    $file_tmp  = $_FILES['photo_video']['tmp_name'];
                    $file_type = $_FILES['photo_video']['type'];
                    $tmp       = explode('.', $file_name);
                    $file_ext  = end($tmp);
    
    
                    $post_name = "bp_posts-".mt_rand(100000,999999).date('H-i-s').'.'.$file_ext;
                    
                    $expensions = array("jpeg","jpg","png","mp4","3gp");
                    
                    if(in_array($file_ext,$expensions)=== false){
    
                    $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                        
                    }
                        
                        if(empty($errors)==true){
    
                            $data['tmp_name']  = $_FILES['photo_video']['tmp_name'];
                            $data['file_name'] = $post_name;
            
           
                            $val = $this->aws3->sendFile('daypoll/assets/images/business_profile',$data);   
                      
                            //move_uploaded_file($file_tmp,"assets/images/challenge/type1/".$post_name);
                            
                            $insert = array('bp_business_profile_id' => $profileid,'bp_posts_user_id'=>$userid,'bp_photo_or_video'=>$photo_or_video,'bp_posts_title'=>$posttitle,'bp_posts_photo_or_video'=>$post_name,
                            'bp_posts_content'=>$posts_content,'bp_posts_width' => $posts_width,'bp_posts_height' => $posts_height,'bp_posts_linecount' => $linecount,
                            'bp_posts_type'=>$poststype,'bp_posts_created_type'=>1 , 'bp_posts_country' => $country , 'bp_posts_state' => $state , 
                            'bp_posts_city' => $district ,'bp_posts_area' => $area );
        
        
                            $result = $this->BusinessProfileModel->insert_table('business_profile_posts',$insert);
    
                        }
                        
                  


                if($result > 0) {

                    $this->output->set_output(json_encode(['message' => "successfully uploaded ", 'status' => "true"]));
                }
                else{
                    $this->output->set_output(json_encode(['message' => "operation failed!..", 'status' => "false"]));
                }
            }
            else{
                $this->output->set_output(json_encode(['message' => "no image found!..", 'status' => "false"]));
            
            }

        }
       
     



            // video upload


            else if($photo_or_video == 1){

                
                if(isset($_FILES['photo_video'])){

                    $errors    = array();
                    $file_name = $_FILES['photo_video']['name'];
                    $file_size = $_FILES['photo_video']['size'];
                    $file_tmp  = $_FILES['photo_video']['tmp_name'];
                    $file_type = $_FILES['photo_video']['type'];
                    $tmp       = explode('.', $file_name);
                    $file_ext  = end($tmp);
    
    
                    
                    $post_name = "bp_posts-".mt_rand(100000,999999).date('H-i-s').'.'.$file_ext;
                    
                    $expensions = array("jpeg","jpg","png","mp4","3gp");
                    
                    if(in_array($file_ext,$expensions)=== false){
    
                    $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                        
                    }
                        $flag = 0;
                        if(empty($errors)==true){
    
                            $data['tmp_name']  = $_FILES['photo_video']['tmp_name'];
                            $data['file_name'] = $post_name;
                          
                            $val = $this->aws3->sendFile_video('daypoll/assets/images/business_profile',$data); 
                            $flag = 1; 

                           
                            if($flag == 1){

                            if(isset($_FILES['posts_video_thumbnail'])){
                                $errors= array();
                                $file_name = $_FILES['posts_video_thumbnail']['name'];
                                $file_size = $_FILES['posts_video_thumbnail']['size'];
                                $file_tmp  = $_FILES['posts_video_thumbnail']['tmp_name'];
                                $file_type = $_FILES['posts_video_thumbnail']['type'];
                                $tmp       = explode('.', $file_name);
                                $file_ext  = end($tmp);
                                
                                
                                $posts_thumb = "bp_posts_thumb".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;
                                
                                $expensions  = array("jpeg","jpg","png");
                                
                                if(in_array($file_ext,$expensions)=== false){
                                   $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
                                }
                                
                                
                                
                                if(empty($errors)==true){
                                   
                                    $data['tmp_name']  = $_FILES['posts_video_thumbnail']['tmp_name'];
                                    $data['file_name'] = $posts_thumb;
        
                                    $val = $this->aws3->sendFile('daypoll/assets/images/business_profile/thumbnails',$data); 

                                   //move_uploaded_file($file_tmp,"assets/images/posts/thumbnails/".$posts_thumb);
                                   $thumb_name = $posts_thumb;
                                   
                                }else{
                                    $thumb_name= NULL;
                                }
                      
                            }

                            $insert = array('bp_business_profile_id' => $profileid,'bp_posts_user_id'=>$userid,'bp_photo_or_video'=>$photo_or_video,'bp_posts_title'=>$posttitle,'bp_posts_photo_or_video'=>$post_name,
                        'bp_posts_content'=>$posts_content,'bp_posts_width' => $posts_width,'bp_posts_height' => $posts_height,'bp_posts_linecount' => $linecount,
                        'bp_posts_type'=>$poststype,'bp_posts_video_thumbnail'=>$thumb_name,'bp_posts_created_type'=>1,
                        'bp_posts_country' => $country , 'bp_posts_state' => $state , 
                        'bp_posts_city' => $district ,'bp_posts_area' => $area );
    
    
                    $result = $this->BusinessProfileModel->insert_table('business_profile_posts',$insert);
                        }
                        if($result > 0) {
    
                            $this->output->set_output(json_encode(['message' => "successfully uploaded ", 'status' => "true"]));
                        }
                        else{
                            $this->output->set_output(json_encode(['message' => "operation failed!..", 'status' => "false"]));
                        }
                    
               
                    }
            }
               
            else{
                $this->output->set_output(json_encode(['message'=>validation_errors(),'status'=>'false']));
            }
        }
    }
}

// create challenge
function createChallenge(){                                                             

    $this->output->set_content_type('application/json');

    $userid                     = $this->input->post('userid');
    $profileid                  = $this->input->post('profile_id');
    $challengetitle             = $this->input->post('challenge_title');
    $challengelinecount         = $this->input->post('linecount');
    $challenge_content          = $this->input->post('challenge_content');
    $challenge_type             = $this->input->post('challenge_type');            // type 1 or type 2
    $challenge_sub_type         = $this->input->post('challenge_posts_sub_type');  // type 1 ( 1 or 2 )    
    $challenge_created          = $this->input->post('challenge_created_date');
    $challenge_exp_date         = $this->input->post('challenge_expired_date');
    $challenge_exp_time         = $this->input->post('challenge_expired_time');
    $challenge_media_type       = $this->input->post('challenge_media_type');
    $challenge_exp_media_type   = $this->input->post('challenge_exp_media_type');
    $user_array                 = $this->input->post('userlist');
   
    $flag                       = $challenge_type == 1 ? 50 :51; 
    
    $country                    = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
    $state                      = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
    $district                   = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
    $area                       = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;



    $this->form_validation->set_rules("userid","userid","required");
    $this->form_validation->set_rules("challenge_title","challenge_title","required");
    $this->form_validation->set_rules("challenge_type","challenge_type","required");
    $this->form_validation->set_rules("challenge_created_date","challenge_created_date","required");
    $this->form_validation->set_rules("challenge_expired_date","challenge_expired_date","required");
    $this->form_validation->set_rules("challenge_expired_time","challenge_expired_time","required");
    $this->form_validation->set_rules("challenge_media_type","challenge_media_type","required");
    $this->form_validation->set_rules("profile_id","profile_id","required");
    

   
     if($challengelinecount == NULL){
        $challengelinecount = 0;
    }

    if($this->form_validation->run() == TRUE){

        switch($challenge_type){
             
            case 1:
            
            $c_email        = !empty($this->input->post('challenge_email')) ? $this->input->post('challenge_email') : NULL;
            $c_ref_num      = !empty($this->input->post('challenge_ref_number')) ? $this->input->post('challenge_ref_number') : NULL;
            $c_gst          = !empty($this->input->post('challenge_gst')) ? $this->input->post('challenge_ref_number') : NULL;
            $c_service_amt  = !empty($this->input->post('challenge_service_amt')) ? $this->input->post('challenge_service_amt') : NULL;
            $c_total        = !empty($this->input->post('challenge_total')) ? $this->input->post('challenge_total') : NULL;

            $insert_challenge =        array('challenge_user_id'             => $userid,
                                            'challenge_profile_id'           => $profileid,
                                            'challenge_title'                => $challengetitle,
                                            'challenge_content'              => $challenge_content,
                                            'challenge_type'                 => $challenge_type,
                                            'challenge_created_date'         => $challenge_created,
                                            'challenge_expired_date'         => $challenge_exp_date,
                                            'challenge_expired_time'         => $challenge_exp_time,
                                            'challenge_media_type'           => $challenge_media_type,
                                            'challenge_posts_sub_type'       => $challenge_sub_type,
                                            'challenge_line_count'           => $challengelinecount,
                                            'challenge_payment_email'        => $c_email,
                                            'challenge_payment_ref_number'   => $c_ref_num,
                                            'challenge_payment_gst'          => $c_gst,
                                            'challenge_payment_service_amt'  => $c_service_amt,
                                            'challenge_payment_total'        => $c_total,
                                            'challenge_country'              => $country,
                                            'challenge_state'                => $state,
                                            'challenge_city'                 => $district,
                                            'challenge_area'                 => $area
                                           
                                          );

                $result_id = $this->BusinessProfileModel->insert_table('business_profile_challenge',$insert_challenge);
              
                if($result_id){

                    if($user_array != null){

                     
                        for($i = 0 ; $i < sizeof($user_array); $i++){

                            $res        = $this->BusinessProfileModel->inviteMembers($user_array[$i] , $result_id , $userid ,$flag);
                        
                        }

                    }

                switch($challenge_media_type){
                   
                   
                    // IMAGE ONLY  (type 1)
                    
                    case 1:
                    $challenge_posts_title      = $this->input->post('challenge_post_title');
                    

                    if(isset($_FILES['challenge_post'])){   
                      
                       
                        $errors_thumb    = array();
                        $file_name_thumb = $_FILES['challenge_post']['name'];
                        $file_size_thumb = $_FILES['challenge_post']['size'];
                        $file_tmp_thumb  = $_FILES['challenge_post']['tmp_name'];
                        $file_type_thumb = $_FILES['challenge_post']['type'];
                        $tmp_thumb       = explode('.', $file_name_thumb);
                        $file_ext_thumb  = end($tmp_thumb);

                
                        $thumb_name = "challenge_post-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                        
                        $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                        
                        if(in_array($file_ext_thumb,$expensions_thumb)=== false){

                        $errors_thumb[] = "extension not allowed, please choose a JPEG or PNG file.";
                        
                    }                     
                        
                        if(empty($errors_thumb)==true){

                            $data['tmp_name']  = $_FILES['challenge_post']['tmp_name'];
                            $data['file_name'] = $thumb_name;
            
           
                           
                            $val = $this->aws3->sendFile('daypoll/assets/images/business_profile/challenge/type1',$data);   
                       
                       
                        $challenge_image = $thumb_name;
                        
                        $posts_width  = $this->input->post('posts_width');
                        $posts_height = $this->input->post('posts_height');

                                                   
                        $insert_challenge_post =   
                        array( 'bp_challenge_posts_challenge_id'       =>  $result_id,
                        'bp_challenge_posts_title'                     =>  $challenge_posts_title,
                        'bp_challenge_posts_sub_post'                  =>  $challenge_image,
                        'bp_challenge_posts_user_id'                   =>  $userid,
                        'bp_posts_width'                               =>  $posts_width,
                        'bp_posts_height'                              =>  $posts_height  
                       
                        );

                        
                        
                        $insert_challenge_posts = $this->BusinessProfileModel->insert_table('business_profile_challenge_posts',$insert_challenge_post);
        
                       
                        
                        }
                        else{
                          
                            $challenge_image = "null";
                        }
                    }
                

                    break;

                    // video only
                    case 2:
                   
                    $challenge_posts_title = $this->input->post('challenge_post_title');
                   

                // challenge video upload


                if(isset($_FILES['challenge_video_thumbnail'])){

                    
                      
                    $errors_thumb    = array();
                    $file_name_thumb = $_FILES['challenge_video_thumbnail']['name'];
                    $file_size_thumb = $_FILES['challenge_video_thumbnail']['size'];
                    $file_tmp_thumb  = $_FILES['challenge_video_thumbnail']['tmp_name'];
                    $file_type_thumb = $_FILES['challenge_video_thumbnail']['type'];
                    $tmp_thumb       = explode('.', $file_name_thumb);
                    $file_ext_thumb  = end($tmp_thumb);

            
                    $thumb_name = "challenge_thumb-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                    
                    $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                    
                    if(in_array($file_ext_thumb,$expensions_thumb)=== false){

                    $errors_thumb[]="extension not allowed, please choose a JPEG or PNG file.";
                    
                }
                    
                    
                    
                    if(empty($errors_thumb)==true){
                   
                        $data['tmp_name']  = $_FILES['challenge_video_thumbnail']['tmp_name'];
                        $data['file_name'] = $thumb_name;
        
                       
                        $val = $this->aws3->sendFile('daypoll/assets/images/business_profile/challenge/thumb',$data); 
                    //move_uploaded_file($file_tmp_thumb,"assets/images/challenge/thumb/".$thumb_name);
                        $challenge_video_thumb = $thumb_name;

                    }else{
                      
                        $challenge_video_thumb = "null";
                    }
                }


                                                
                if(isset($_FILES['challenge_post'])){

                   
                    $errors    = array();
                    $file_name = $_FILES['challenge_post']['name'];
                    $file_size = $_FILES['challenge_post']['size'];
                    $file_tmp  = $_FILES['challenge_post']['tmp_name'];
                    $file_type = $_FILES['challenge_post']['type'];
                    $tmp       = explode('.', $file_name);
                    $file_ext  = end($tmp);


                    $post_name = "challenge-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;
                    
                    $expensions = array("jpeg","jpg","png","mp4","3gp");
                    
                    if(in_array($file_ext,$expensions)=== false){

                    $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                        
                    }
                        
                        if(empty($errors)==true){
                            $data['tmp_name']  = $_FILES['challenge_post']['tmp_name'];
                            $data['file_name'] = $post_name;
                            
                                                       
                            $val = $this->aws3->sendFile_video('daypoll/assets/images/business_profile/challenge/type1',$data); 
                            
                          
                            $challenge_post = $post_name;
                            $posts_width  = $this->input->post('posts_width');
                            $posts_height = $this->input->post('posts_height');
                        
                            $insert_challenge_post = 
                            
                            array( 'bp_challenge_posts_challenge_id'       =>  $result_id,
                            'bp_challenge_posts_title'                     =>  $challenge_posts_title,
                            'bp_challenge_posts_sub_post'                  =>  $challenge_post,
                            'bp_challenge_posts_video_thumbnail'           =>  $challenge_video_thumb,
                            'bp_challenge_posts_user_id'                   =>  $userid,
                            'bp_posts_width'                               =>  $posts_width,
                            'bp_posts_height'                              =>  $posts_height                        
                            );
                        
                            $insert_challenge_posts = $this->BusinessProfileModel->insert_table('business_profile_challenge_posts',$insert_challenge_post);

                       
                        }
                        else{
                            $challenge_post = "null";
                        }
                    }


               

                }

                if($challenge_exp_media_type != null){
                    
                    switch($challenge_exp_media_type){

                        case 1:
                        if(isset($_FILES['challenge_exp_video_thumbnail'])){

                           
                            $errors_thumb    = array();
                            $file_name_thumb = $_FILES['challenge_exp_video_thumbnail']['name'];
                            $file_size_thumb = $_FILES['challenge_exp_video_thumbnail']['size'];
                            $file_tmp_thumb  = $_FILES['challenge_exp_video_thumbnail']['tmp_name'];
                            $file_type_thumb = $_FILES['challenge_exp_video_thumbnail']['type'];
                            $tmp_thumb       = explode('.', $file_name_thumb);
                            $file_ext_thumb  = end($tmp_thumb);

                    
                            $thumb_name = "challenge_thumb-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                            
                            $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                            
                            if(in_array($file_ext_thumb,$expensions_thumb)=== false){

                            $errors_thumb[]="extension not allowed, please choose a JPEG or PNG file.";
                            
                        }
                            
                            
                            
                            if(empty($errors_thumb)==true){
                           
                                $data['tmp_name']  = $_FILES['challenge_exp_video_thumbnail']['tmp_name'];
                                $data['file_name'] = $thumb_name;
                
               
                                $val = $this->aws3->sendFile('daypoll/assets/images/business_profile/challenge/thumb',$data);    
                            //move_uploaded_file($file_tmp_thumb,"assets/images/challenge/thumb/".$thumb_name);
                            $challenge_video_thumb = $thumb_name;
                            
                            
                            
                            }else{
                              
                                $challenge_video_thumb = "null";
                            }
                        }





                        if(isset($_FILES['challenge_exp_post'])){

                            

                            $errors    = array();
                            $file_name = $_FILES['challenge_exp_post']['name'];
                            $file_size = $_FILES['challenge_exp_post']['size'];
                            $file_tmp  = $_FILES['challenge_exp_post']['tmp_name'];
                            $file_type = $_FILES['challenge_exp_post']['type'];
                            $tmp       = explode('.', $file_name);
                            $file_ext  = end($tmp);
    
    
                            $post_name = "challenge-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;
                            
                            $expensions = array("jpeg","jpg","png","mp4","3gp");
                            
                            if(in_array($file_ext,$expensions)=== false){
    
                            $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                                
                            }
                                
                                if(empty($errors)==true){
                                    
                                    $data['tmp_name']  = $_FILES['challenge_exp_post']['tmp_name'];
                                    $data['file_name'] = $post_name;
            
                                    
           
                                    $val = $this->aws3->sendFile_video('daypoll/assets/images/business_profile/challenge/type1',$data); 
                            
                                    
                                    //move_uploaded_file($file_tmp,"assets/images/challenge/type1/".$post_name);
                                    $challenge_post = $post_name;
                                    $posts_width  = $this->input->post('posts_width');
                                    $posts_height = $this->input->post('posts_height');
                                    //for($i = 0 ; $i < sizeof($posts_width) ; $i++){
                                    $insert_challenge_post = 
                                    
                                    array( 'bp_challenge_posts_challenge_id'       =>  $result_id,
                                    'bp_challenge_posts_title'                     =>  $challenge_posts_title,
                                    'bp_challenge_posts_sub_post'                  =>  $challenge_post,
                                    'bp_challenge_posts_video_thumbnail'           =>  $challenge_video_thumb,
                                    'bp_challenge_posts_exp_media_type'            =>  1,
                                    'bp_challenge_posts_type'                      =>  1,
                                    'bp_challenge_posts_user_id'                   =>  $userid,
                                    'bp_posts_width'                               =>  $posts_width,
                                    'bp_posts_height'                              =>  $posts_height               
                                    );
                                
                                    $insert_challenge_posts = $this->BusinessProfileModel->insert_table('business_profile_challenge_posts',$insert_challenge_post);
    
                               // }
                                }
                                else{
                                    $challenge_post = "null";
                                }
                            }


                            break;


                            case 0:

                            if(isset($_FILES['challenge_exp_post'])){   
                                
                                $errors_thumb    = array();
                                $file_name_thumb = $_FILES['challenge_exp_post']['name'];
                                $file_size_thumb = $_FILES['challenge_exp_post']['size'];
                                $file_tmp_thumb  = $_FILES['challenge_exp_post']['tmp_name'];
                                $file_type_thumb = $_FILES['challenge_exp_post']['type'];
                                $tmp_thumb       = explode('.', $file_name_thumb);
                                $file_ext_thumb  = end($tmp_thumb);
    
                        
                                $thumb_name = "challenge_post-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                                
                                $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                                
                                if(in_array($file_ext_thumb,$expensions_thumb)=== false){
    
                                $errors_thumb[] = "extension not allowed, please choose a JPEG or PNG file.";
                                
                            }
                                
                                
                                
                                if(empty($errors_thumb)==true){
                                
                                    $data['tmp_name']  = $_FILES['challenge_exp_post']['tmp_name'];
                                    $data['file_name'] = $thumb_name;
            
           
                                    $val = $this->aws3->sendFile('daypoll/assets/images/business_profile/challenge/type1',$data); 
                            



                                //move_uploaded_file($file_tmp_thumb,"assets/images/challenge/type1/".$thumb_name);
                                $challenge_image = $thumb_name;
                                $posts_width  = $this->input->post('posts_width');
                                $posts_height = $this->input->post('posts_height');
                               
                                //for($i = 0 ; $i < sizeof($posts_width) ; $i++){
                                $insert_challenge_post =   
                               
                                array( 'bp_challenge_posts_challenge_id'       =>  $result_id,
                                'bp_challenge_posts_title'                     =>  $challenge_posts_title,
                                'bp_challenge_posts_sub_post'                  =>  $challenge_image,
                                'bp_challenge_posts_type'                      =>  1,
                                'bp_challenge_posts_user_id'                   =>  $userid,
                                'bp_posts_width'                               =>  $posts_width,
                                'bp_posts_height'                              =>  $posts_height
                               
                               
                                );

                                $insert_challenge_posts = $this->BusinessProfileModel->insert_table('business_profile_challenge_posts',$insert_challenge_post);
                
                            //}
                                
                                }else{
                                  
                                    $challenge_image = "null";
                                }
                            }

                            break;
                        
                    }
            }
                $this->output->set_output(json_encode(['message' => "successfully saved ", 'status' => "true"]));
                
            }
            else{
                $this->output->set_output(json_encode(['message' => "failed to uploads", 'status' => "false"]));
                
            }

                                          

                break;
                          

            // balot
            case 2:
              
            
            $insert_challenge =        array('challenge_user_id'             => $userid,
                                            'challenge_profile_id'           => $profileid,
                                            'challenge_title'                => $challengetitle,
                                            'challenge_content'              => $challenge_content,
                                            'challenge_type'                 => $challenge_type,
                                            'challenge_created_date'         => $challenge_created,
                                            'challenge_expired_date'         => $challenge_exp_date,
                                            'challenge_expired_time'         => $challenge_exp_time,
                                            'challenge_media_type'           => $challenge_media_type,
                                            'challenge_line_count'           => $challengelinecount,
                                            'challenge_country'              => $country,
                                            'challenge_state'                => $state,
                                            'challenge_city'                 => $district,
                                            'challenge_area'                 => $area
                                           
                                          );


            $result_id = $this->BusinessProfileModel->insert_table('business_profile_challenge',$insert_challenge);


            if($result_id > 0) {

        /*******************************   IMAGE ONLY     ***********************************/
               

                        switch($challenge_media_type){

                            case 1:
                            
                                $challenge_posts_title = $this->input->post('challenge_post_title');
                                
                               
                                
                                for($i=0;$i<sizeof($challenge_posts_title);$i++){

                               
                               // image only
                             
                            if(isset($_FILES['challenge_post'])){   
                                
                                $errors_thumb    = array();
                                $file_name_thumb = $_FILES['challenge_post']['name'][$i];
                                $file_size_thumb = $_FILES['challenge_post']['size'][$i];
                                $file_tmp_thumb  = $_FILES['challenge_post']['tmp_name'][$i];
                                $file_type_thumb = $_FILES['challenge_post']['type'][$i];
                                $tmp_thumb       = explode('.', $file_name_thumb);
                                $file_ext_thumb  = end($tmp_thumb);

                                
                                $thumb_name = "challenge_post-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                                
                                $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                                
                                if(in_array($file_ext_thumb,$expensions_thumb)=== false){

                                $errors_thumb[]="extension not allowed, please choose a JPEG or PNG file.";
                                
                                }
                                
                                
                                
                                if(empty($errors_thumb)==true){
                               
                                    $data['tmp_name']  = $_FILES['challenge_post']['tmp_name'][$i];
                                    $data['file_name'] = $thumb_name;
            
           
                                    $val = $this->aws3->sendFile('daypoll/assets/images/business_profile/challenge/type2',$data); 
                            
                                //move_uploaded_file($file_tmp_thumb,"assets/images/challenge/type2/".$thumb_name);
                                $challenge_image = $thumb_name;
                                $posts_width  = $this->input->post('posts_width');
                                $posts_height = $this->input->post('posts_height');
                                
                                $insert_challenge_post =   
                                array( 'bp_challenge_posts_challenge_id'       =>  $result_id,
                                'bp_challenge_posts_title'                     =>  $challenge_posts_title[$i],
                                'bp_challenge_posts_sub_post'                  =>  $challenge_image,
                                'bp_challenge_posts_user_id'                   =>  $userid,
                                'bp_posts_width'                               =>  $posts_width[$i],
                                'bp_posts_height'                              =>  $posts_height[$i]  
                                );
                                $insert_challenge_posts = $this->BusinessProfileModel->insert_table('business_profile_challenge_posts',$insert_challenge_post);
                
                            
                                
                                }else{
                                  
                                    $challenge_image = "null";
                                }
                            }
                        }
                        
                        break;

     /*******************************   IMAGE ONLY ENDS HERE     ***********************************/

                           
      /*******************************   VIDEO  ONLY     ***********************************/

                       case 2:
                      
                        $challenge_posts_title = $this->input->post('challenge_post_title');
                        for($i=0;$i<sizeof($challenge_posts_title);$i++){

                           
                        // video thumbnail
                        $challenge_video_thumb = "";
                        if(isset($_FILES['challenge_video_thumbnail'])){   
                            $errors_thumb    = array();
                            $file_name_thumb = $_FILES['challenge_video_thumbnail']['name'][$i];
                            $file_size_thumb = $_FILES['challenge_video_thumbnail']['size'][$i];
                            $file_tmp_thumb  = $_FILES['challenge_video_thumbnail']['tmp_name'][$i];
                            $file_type_thumb = $_FILES['challenge_video_thumbnail']['type'][$i];
                            $tmp_thumb       = explode('.', $file_name_thumb);
                            $file_ext_thumb  = end($tmp_thumb);

                    
                            $thumb_name = "challenge_thumb-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                            
                            $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                            
                            if(in_array($file_ext_thumb,$expensions_thumb)=== false){

                            $errors_thumb[]="extension not allowed, please choose a JPEG or PNG file.";
                            
                        }
                            
                            
                            
                            if(empty($errors_thumb)==true){
                           
                                $data['tmp_name']  = $_FILES['challenge_video_thumbnail']['tmp_name'][$i];
                                $data['file_name'] = $thumb_name;
        
       
                                $val = $this->aws3->sendFile('daypoll/assets/images/business_profile/challenge/thumb',$data); 
                        
                            //move_uploaded_file($file_tmp_thumb,"assets/images/challenge/thumb/".$thumb_name);
                            $challenge_video_thumb = $thumb_name;
                            
                            }else{
                              
                                $challenge_video_thumb = "null";
                            }
                        }

                        
                       
                        // challenge video upload
                               
                        if(isset($_FILES['challenge_post'])){
                        $errors    = array();
                        $file_name = $_FILES['challenge_post']['name'][$i];
                        $file_size = $_FILES['challenge_post']['size'][$i];
                        $file_tmp  = $_FILES['challenge_post']['tmp_name'][$i];
                        $file_type = $_FILES['challenge_post']['type'][$i];
                        $tmp       = explode('.', $file_name);
                        $file_ext  = end($tmp);

                
                        $post_name = "challenge-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;
                        
                        $expensions = array("jpeg","jpg","png","mp4","3gp");
                        
                        if(in_array($file_ext,$expensions)=== false){

                        $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                            
                        }
                            
                            if(empty($errors)==true){

                                $data['tmp_name']  = $_FILES['challenge_post']['tmp_name'][$i];
                                $data['file_name'] = $post_name;
        
       
                                $val = $this->aws3->sendFile_video('daypoll/assets/images/business_profile/challenge/thumb',$data); 
                                //move_uploaded_file($file_tmp,"assets/images/challenge/type2/".$post_name);
                                $challenge_post = $post_name;
                                $posts_width  = $this->input->post('posts_width');
                                $posts_height = $this->input->post('posts_height');
                                
                                $insert_challenge_post = 
                                
                                array( 'bp_challenge_posts_challenge_id'       =>  $result_id,
                                'bp_challenge_posts_title'                     =>  $challenge_posts_title[$i],
                                'bp_challenge_posts_sub_post'                  =>  $challenge_post,
                                'bp_challenge_posts_video_thumbnail'           =>  $challenge_video_thumb,
                                'bp_challenge_posts_user_id'                   =>  $userid,
                                'bp_posts_width'                               =>  $posts_width[$i],
                                'bp_posts_height'                              =>  $posts_height[$i]                        
                                );
                            
                                $insert_challenge_posts = $this->BusinessProfileModel->insert_table('business_profile_challenge_posts',$insert_challenge_post);
                         

                            }
                            else{
                                $challenge_post = "null";
                            }
                        }
                           
                        }

                        break;

                /*******************************   VIDEO ONLY  ENDS HERE    ***********************************/
                        
                    }
             
                   
                $this->output->set_output(json_encode(['message' => "successfully saved ", 'status' => "true"]));
                    
          
    }
            else{
                $this->output->set_output(json_encode(['message' => "failed to upload", 'status' => "false"]));
            }
            break;
        }
        
    }
    else{
        $this->output->set_output(json_encode(['message'=>validation_errors(),'status'=>'false']));
    }
   }


   function checkIsBusinessProfile(){
       
       $this->output->set_content_type('application/json');
       $userid = $this->input->post('userid');
       if(!empty($userid)){
           $res       = $this->BusinessProfileModel
                                        ->fetch_from_table('business_profile',array('bf_created_user' => $userid , 'bf_status' => 1))
                                        ->row('bf_id');
           $isBlocked = $this->BusinessProfileModel
                                        ->fetch_from_table('business_profile',array('bf_created_user' => $userid , 'bf_status' => 0))
                                        ->row('bf_id');                                 
        if($isBlocked > 0){
            $this->output->set_output(json_encode(['message' => "profile blocked by admin" ,'isTrue' => 1,'id' => $isBlocked,'is_blocked' => 1,'status' => true]));
        }
        else{
        if($res > 0){   
            $this->output->set_output(json_encode(['message' => "profile found" ,'isTrue' => 1,'id' => $res,'is_blocked' => 0,'status' => true]));
        }else{
            $this->output->set_output(json_encode(['message' => "no profile found" ,'isTrue' => 0,'id'=>0, 'status' => true]));
            }
        }
       }else{
        $this->output->set_output(json_encode(['message' => "parameter missing" , 'status' => false]));
       }
       
   }

   function getBusinessProfileCategory(){

        $this->output->set_content_type('application/json');
        
       
        $res = $this->BusinessProfileModel
                                        ->fetch_from_table('business_profile_category',array('bfc_status'=>1))
                                        ->result_array();    
        if($res){   
            $this->output->set_output(json_encode(['message' => $res , 'status' => true]));
        }else{
            $this->output->set_output(json_encode(['message' => "no value found" , 'status' => true]));
        }
     
   }


   function getBusinessProfileSubCategory(){

    $this->output->set_content_type('application/json');
    
    $catid = $this->input->post('category_id');
    if(!empty($catid)){
    $res = $this->BusinessProfileModel
                                    ->fetch_from_table('business_profile_sub_category',array('bpsc_category_id' => $catid , 'bpsc_status' => 1))
                                    ->result_array();    
    if($res){   
        $this->output->set_output(json_encode(['message' => $res , 'status' => true]));
    }else{
        $this->output->set_output(json_encode(['message' => "no value found" , 'status' => false]));
    }
}else{
    $this->output->set_output(json_encode(['message' => "parameter missing" , 'status' => false]));
   
}
 
}


function getBusinessProfileSubCategory2(){

    $this->output->set_content_type('application/json');
    
    $catid = $this->input->post('sub_category_id');
    if(!empty($catid)){
    $res = $this->BusinessProfileModel
                                    ->fetch_from_table('business_profile_sub_category_2',array('bpssc_sub_category_id' => $catid , 'bpssc_status' => 1))
                                    ->result_array();    
    if($res){   
        $this->output->set_output(json_encode(['message' => $res , 'status' => true]));
    }else{
        $this->output->set_output(json_encode(['message' => "no value found" , 'status' => false]));
    }
}else{
    $this->output->set_output(json_encode(['message' => "parameter missing" , 'status' => false]));
   
}
 
}





function getBusinessProfileSubCategory3(){

    $this->output->set_content_type('application/json');
    
    $catid = $this->input->post('sub_category_id');
    if(!empty($catid)){
    $res = $this->BusinessProfileModel
                                    ->fetch_from_table('business_profile_sub_category_3',array('bpsssc_sub_sub_category_id' => $catid,'bpsssc_status' => 1))
                                    ->result_array();    
    if($res){   
        $this->output->set_output(json_encode(['message' => $res , 'status' => true]));
    }else{
        $this->output->set_output(json_encode(['message' => "no value found" , 'status' => false]));
    }
}else{
    $this->output->set_output(json_encode(['message' => "parameter missing" , 'status' => false]));
   
}
 
}




    //poll posts
    function pollPosts(){

        $this->output->set_content_type('application/json');
        
        $post_user_id    = $this->input->post('postuserid');
        $posts_id        = $this->input->post('postid');
        $user_id         = $this->input->post('userid');
        
        if(!empty($posts_id && $user_id)){
           
        $res        = $this->BusinessProfileModel->pollPosts($user_id,$posts_id,$post_user_id);
        
        if($res['res'] == 1){
       
            $this->output->set_output(json_encode(array('is_polled'=>true,'message'=>"successfully polled",'status'=>true)));
        }
        
        else if($res['res'] == -1){

            $this->output->set_output(json_encode(array('is_polled'=>false,'message'=>'successfully unpolled','status'=>true)));
        
        }
        }
        else{
            $this->output->set_output(json_encode(array('message'=>"parameter missing",'status'=>false)));
        }
        
    }

        // report posts
        function reportPost(){

            $this->output->set_content_type('application/json');
    
            $user_id       = $this->input->post('user_id');
            $post_id       = $this->input->post('post_id');
            $msg           = $this->input->post('message'); 
            $type          = $this->input->post('type');
            $logged_userid = $this->input->post('logged_userid');
            $profileid     = $this->input->post('profileid');
            
            $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
            $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
            $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
            $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;  
            $page_start  = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;
    
            if(!empty($user_id && $post_id)){
                
            $res         = $this->BusinessProfileModel->reportPost($user_id , $post_id , $msg);
           
    
    
            switch($res){
    
    
                case $res > 0:
    
    
                $this->BusinessProfileModel->updateReportcount($post_id,$msg);
    
                //check report count
                $remove_post = $this->BusinessProfileModel->removePostbyCount($post_id);
               
                   
                    $posts = $this->returnBusinessProfilePosts($user_id,$logged_userid,$page_start,$profileid);
                    
                    if(count($posts) >= 9 )
                    $last_posts = $posts[9];
                    else 
                    $last_posts = null;
                 
                    if($last_posts == null)
                    $this->output->set_output(json_encode(array('value' => "successfully reported",'is_completed' => 1,'status'=>true)));
                    
                    else{
                    if($last_posts['is_completed_profile'] > 0)
                    $this->output->set_output(json_encode(array('value' => "successfully reported",'data' => array($last_posts),'is_completed' => 0,'status'=>true)));
                    
                    else
                    $this->output->set_output(json_encode(array('value' => "successfully reported",'data' => array($last_posts),'is_completed' => 1,'status'=>true)));
                    }
    
                
                break;
    
                case -1:
                $this->output->set_output(json_encode(array('value' => "already reported",'status'=>false)));
                break;
                
                case 0:
                $this->output->set_output(json_encode(array('value' => "operation failed",'status'=>false)));
                break;
    
                }
            }
            else{
                $this->output->set_output(json_encode(array('value' => "parameter missing",'status'=>false)));
               
            }
         
        }

    //poll posts
    function getPolledUsers(){

        $this->output->set_content_type('application/json');
        
        
        $profileid        = $this->input->post('profileid');
        $user_id         = $this->input->post('userid');
        
        if(!empty($profileid)){
           
        $res        = $this->BusinessProfileModel->getPolledUsers(array('bpp_bp_id' => $profileid));
        
        if($res){
            
            foreach($res as $val => $key){
                $res[$val]['users_photo'] = $this->media_url."assets/images/users/".$res[$val]['users_photo'];
            }

            $this->output->set_output(json_encode(array('message'=>$res,'status'=>true)));
        }
        
        else {

            $this->output->set_output(json_encode(array('message'=>'no data found','status'=>false)));
        
        }
        }
        else{
            $this->output->set_output(json_encode(array('message'=>"parameter missing",'status'=>false)));
        }
        
    }

    function getPostcomments(){ 

        $this->output->set_content_type('application/json');

        $post_id     = $this->input->post('post_id');
        $userid      = $this->input->post('userid');
        $res         = $this->BusinessProfileModel->getPostcomments($post_id);

        $profile_id  = $this->BusinessProfileModel->fetch_from_table('business_profile_posts',array('bp_posts_id' => $post_id))->row('bp_business_profile_id');
        $created_id  = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $profile_id))->row('bf_created_user');
        
        if($res){

        foreach($res as $key => $value){
          
            if($res[$key]['bp_posts_comments_user_id'] == $created_id){
                $res[$key]['users_photo'] = $this->s3_url."assets/images/businessprofile/".$this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $profile_id))->row('bf_photo');
                $res[$key]['users_name']  = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $profile_id))->row('bf_name');
            }
            else{
                $res[$key]['users_photo'] = $this->media_url."assets/images/users/".$res[$key]['users_photo'];
            }
            
            $res_reply          = $this->BusinessProfileModel->getcommentsreply($res[$key]['bp_posts_comments_id'] , $res[$key]['bp_posts_comments_post_id']);
            
            foreach($res_reply as $val  => $keys){
  
                if($res_reply[$val]['bp_posts_comments_reply_user_id'] == $created_id){
                    $res_reply[$val]['users_photo'] = $this->s3_url."assets/images/businessprofile/".$this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $profile_id))->row('bf_photo');
                    $res_reply[$val]['users_name']  = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $profile_id))->row('bf_name');
                }
                else{
                    $res_reply[$val]['users_photo'] = $this->media_url."assets/images/users/".$res_reply[$val]['users_photo'];
                }
                
                
                $res_reply[$val]['reply_to_user']   = $this->BusinessProfileModel->getusersnamebyid($res_reply[$val]['bp_posts_comment_reply_comment_user_id']);
        
            }   

                       
            $res[$key]['reply'] = $res_reply;
           
        }
    
    
            $this->output->set_output(json_encode(array('data' => $res,'status'=>true)));

            //
        }
        else{
            $this->output->set_output(json_encode(array('data' => "no value found",'status'=>false)));
        }
    }

    //    // get comment list
    //    function getChallengeComments(){

    //     $this->output->set_content_type('application/json');
        
    //     $challengeid  = $this->input->post('challengeid');
    //     $type         = $this->input->post('type');   // 0 => normal challenge comment , 1 => posts comment

        
                                                              
                                                           
    //     if(!empty($challengeid)){

    //         $res = $this->BusinessProfileModel->getCommentlist($challengeid , $type);
           
    //         if($type == 1){      // challenge posts comment section

                 
    //             $challenge_id         = $this->BusinessProfileModel->fetch_from_table('business_profile_challenge_posts',array('bp_challenge_posts_id' => $challengeid))
    //                                     ->row('bp_challenge_posts_challenge_id');  

    //             $bus_profile_id       = $this->BusinessProfileModel->fetch_from_table('business_profile_challenge',array('challenge_id' => $challenge_id))
    //                                     ->row('challenge_profile_id');  
    //             $created_user         = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $bus_profile_id))
    //                                     ->row('bf_created_user'); 
                     
    //         if($res != null){
                
    //             foreach($res as $val => $key){

                   
    //                 if($res[$val]['bp_challenge_posts_comments_user_id'] == $created_user){
                        
    //                     $res[$val]['users_photo'] = $this->s3_url."assets/images/businessprofile/".$this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $bus_profile_id))->row('bf_photo');
    //                     $res[$val]['users_name']  = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $bus_profile_id))->row('bf_name');
                   
    //                 } else{
                                           
    //                     $res[$val]['users_photo']                          = $this->media_url."assets/images/users/".$res[$val]['users_photo'];
                    
    //                 }   

    //                 $res[$val]['bp_challenge_comments_id']             = $res[$val]['bp_challenge_posts_comments_id'];
    //                 $res[$val]['bp_challenge_comments_user_id']        = $res[$val]['bp_challenge_posts_comments_user_id'];
    //                 $res[$val]['bp_challenge_comments_comment']        = $res[$val]['bp_challenge_posts_comments_comment'];
    //                 $res[$val]['bp_challenge_comments_challenge_id']   = $res[$val]['bp_challenge_posts_comments_challenge_id'];
    //                 $res[$val]['bp_challenge_comments_date']           = $res[$val]['bp_challenge_posts_comments_challenge_date'];
                  
    //             }
    //         }
    //     }
    //         else{
                
    //             $bus_profile_id       = $this->BusinessProfileModel->fetch_from_table('business_profile_challenge',array('challenge_id' => $challengeid))
    //                                                        ->row('challenge_profile_id');  
                                                                
               
    //             $created_user         = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $bus_profile_id))
    //                                                        ->row('bf_created_user');
                

    //             foreach($res as $val => $key){

    //             if($res[$val]['bp_challenge_comments_user_id'] == $created_user){
    //                 $res[$val]['users_photo'] = $this->s3_url."assets/images/businessprofile/".$this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $bus_profile_id))->row('bf_photo');
    //                 $res[$val]['users_name']  = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $bus_profile_id))->row('bf_name');
               
    //             } else{
    //                 $res[$val]['users_photo']                       = $this->media_url."assets/images/users/".$res[$val]['users_photo'];
               
    //             }       

               
    //         }
    //         }
    //         $this->output->set_output(json_encode(['message' => 'data found' ,'value' => $res,'status'=> true])); 
           
    //         else{
    //             $this->output->set_output(json_encode(['message' => 'no data found' ,'value' => $res,'status'=> true])); 
           
    //         }
    //     }
    
    //     else{

    //         $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> false])); 
    //     }
    // }




     // get comment list
     function getChallengeComments(){

        $this->output->set_content_type('application/json');
        
        $challengeid  = $this->input->post('challengeid');
        $type         = $this->input->post('type');   // 0 => normal challenge comment , 1 => posts comment
                                                   
        if(!empty($challengeid)){                                                 
        
            $res = $this->BusinessProfileModel->getCommentlist($challengeid , $type);
           
            if($res != null){
               
                if($type == 1){      // challenge posts comment section

                    $challenge_id         = $this->BusinessProfileModel->fetch_from_table('business_profile_challenge_posts',array('bp_challenge_posts_id' => $challengeid))
                                            ->row('bp_challenge_posts_challenge_id');  
    
                    $bus_profile_id       = $this->BusinessProfileModel->fetch_from_table('business_profile_challenge',array('challenge_id' => $challenge_id))
                                            ->row('challenge_profile_id');  
                    $created_user         = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $bus_profile_id))
                                            ->row('bf_created_user'); 

                                           
                foreach($res as $val => $key){

                    if($res[$val]['bp_challenge_posts_comments_user_id'] == $created_user){
                        
                        $res[$val]['users_photo'] = $this->s3_url."assets/images/businessprofile/".$this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $bus_profile_id))->row('bf_photo');
                        $res[$val]['users_name']  = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $bus_profile_id))->row('bf_name');
                   
                    } else{
                                           
                        $res[$val]['users_photo']                          = $this->media_url."assets/images/users/".$res[$val]['users_photo'];
                    
                    }   

                    $res[$val]['bp_challenge_comments_id']             = $res[$val]['bp_challenge_posts_comments_id'];
                    $res[$val]['bp_challenge_comments_user_id']        = $res[$val]['bp_challenge_posts_comments_user_id'];
                    $res[$val]['bp_challenge_comments_comment']        = $res[$val]['bp_challenge_posts_comments_comment'];
                    $res[$val]['bp_challenge_comments_challenge_id']   = $res[$val]['bp_challenge_posts_comments_challenge_id'];
                    $res[$val]['bp_challenge_comments_date']           = $res[$val]['bp_challenge_posts_comments_challenge_date'];
                  
                }
            }else{
                
                $bus_profile_id       = $this->BusinessProfileModel->fetch_from_table('business_profile_challenge',array('challenge_id' => $challengeid))
                                        ->row('challenge_profile_id');  
                     

                $created_user         = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $bus_profile_id))
                                        ->row('bf_created_user');

               
                foreach($res as $val => $key){

                    if($res[$val]['bp_challenge_comments_user_id'] == $created_user){
                    $res[$val]['users_photo'] = $this->s3_url."assets/images/businessprofile/".$this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $bus_profile_id))->row('bf_photo');
                    $res[$val]['users_name']  = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $bus_profile_id))->row('bf_name');

                } else{
                    $res[$val]['users_photo']                       = $this->media_url."assets/images/users/".$res[$val]['users_photo'];

                }      

                }
            }
            $this->output->set_output(json_encode(['message' => 'data found' ,'value' => $res,'status'=> true])); 
           
            }else{
                $this->output->set_output(json_encode(['message' => 'no data found' ,'value' => $res,'status'=> true])); 
                }
            }
            else{

            $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> false])); 
        }
    }





    //comment business profile posts api
    function commentPost(){

        $this->output->set_content_type('application/json');

        $post_id          = $this->input->post('post_id');
        $user_id          = $this->input->post('user_id');
        $post_user_id     = $this->input->post('postuserid');
        $comment          = $this->input->post('comment');
     
        if(!empty($post_id && $user_id && $comment)){

        $res              = $this->BusinessProfileModel->commentPost($user_id , $post_id , $comment , $post_user_id);


        
        if($res > 0){

            $this->output->set_output(json_encode(array('message' => "successfully commented",'comment'=>$res['data'],'status'=>true)));
        }
        else{
            
            $this->output->set_output(json_encode(array('posts' => "failed to comment",'status'=>false)));
        }
    }
    else{
            $this->output->set_output(json_encode(array('posts' => "parameter missing",'status'=>false)));
    }
    }


    // challenge comment
    function commentChallenge(){

        $this->output->set_content_type('application/json');
        
        $userid      = $this->input->post('userid');
        $cid         = $this->input->post('challengeid');
        $comment     = $this->input->post('comment');
        $cuserid     = $this->input->post('challenge_user_id');

         

        if(!empty($userid && $cid && $comment)){

        $profile_id       = $this->BusinessProfileModel->fetch_from_table('business_profile_challenge',array('challenge_id' => $cid))
                            ->row('challenge_profile_id');

        $res = $this->BusinessProfileModel->commentChallenge($userid , $cid , $comment , $cuserid);
           
        if($res > 0){

            $getcomment = $this->BusinessProfileModel->getLastcomment($res);
           
            foreach($getcomment as $val => $ke){
               
            if($getcomment[$val]['bp_challenge_comments_user_id'] == $cuserid){
                $getcomment[$val]['users_photo'] = $this->s3_url."assets/images/businessprofile/".$this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $profile_id))->row('bf_photo');
                $getcomment[$val]['users_name']  = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $profile_id))->row('bf_name');
            }
            else{
                $getcomment[$val]['users_photo'] = $this->media_url."assets/images/users/".$getcomment[$val]['users_photo'];
            }
        }


            $this->output->set_output(json_encode(['message' => 'successfully commented','comment' => $getcomment ,'status'=> true])); 
        }
        else{

            $this->output->set_output(json_encode(['message' => "operation failed" ,'status'=> false])); 
        }
    }
    else{
        $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> false])); 
    }
        
    }



    // reply to comments
    function replyPostscomments(){

        
        $this->output->set_content_type('application/json');

        $post_id            = $this->input->post('post_id');
        $user_id            = $this->input->post('user_id');
        $comment_id         = $this->input->post('comment_id');
        $comment_user       = !empty($this->input->post('comment_user_id')) ? $this->input->post('comment_user_id') : 1;
        $reply              = $this->input->post('reply');
        
       
        $insert      = array('bp_posts_comments_reply_posts_id'=>$post_id,
                        'bp_posts_comments_reply_comment_id'=>$comment_id,
                        'bp_posts_comments_reply'=>$reply,
                        'bp_posts_comment_reply_comment_user_id' => $comment_user,
                        'bp_posts_comments_reply_user_id' => $user_id);

                        
        $res         = $this->BusinessProfileModel->replyPostscomments($insert,$comment_user);
        
        if($res > 0){

            $this->output->set_output(json_encode(array('message' => "succesfully inserted",'status'=>true)));
        }
        else{

            $this->output->set_output(json_encode(array('posts' => "no value found",'status'=>false)));
        }
    }


    // reply to comments
    function replyChallengecomments(){

        $this->output->set_content_type('application/json');

        $challangeid        = $this->input->post('challengeid');
        $user_id            = $this->input->post('userid');
        $comment_id         = $this->input->post('comment_id');
        $comment_user       = !empty($this->input->post('comment_user_id')) ? $this->input->post('comment_user_id') : 1;
        $reply              = $this->input->post('reply');
        
       
        $insert      = array('bp_challenge_comments_reply_challenge_id'=>$challangeid,
                        'bp_challenge_comments_reply_comment_id'=>$comment_id,
                        'bp_challenge_comments_reply'=>$reply,
                        'bp_challenge_comment_reply_comment_user_id' => $comment_user,
                        'bp_challenge_comments_reply_user_id' => $user_id);

                       

        $res         = $this->BusinessProfileModel->replyChallengecomments($insert,$comment_user);

        if($res > 0){

            $this->output->set_output(json_encode(array('message' => "succesfully inserted",'status'=>true)));
        }
        else{

            $this->output->set_output(json_encode(array('posts' => "no value found",'status'=>false)));
        }
    }







    // comment challenge posts
    function commentChallengePosts(){

        $this->output->set_content_type('application/json');
        
        $userid      = $this->input->post('userid');
        $cid         = $this->input->post('challengeid');
        $comment     = $this->input->post('comment');
        $cuserid     = $this->input->post('challenge_user_id');
        $cpid        = $this->input->post('postid');

        if(!empty($userid && $cid && $comment && $cpid)){
        
        $profile_id       = $this->BusinessProfileModel->fetch_from_table('business_profile_challenge',array('challenge_id' => $cid))
                            ->row('challenge_profile_id');
        
        $user_id          = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $profile_id))
                            ->row('bf_created_user');                    
        $res = $this->BusinessProfileModel->commentChallengePosts($userid , $cid , $comment , $cuserid , $cpid);
        if($res > 0){

            $getcomment = $this->BusinessProfileModel->getpostLastcomment($res);
            foreach($getcomment as $val => $ke){
                
               
                foreach($getcomment as $val => $ke){
               
                    if($getcomment[$val]['bp_challenge_posts_comments_user_id'] == $user_id){
                        $getcomment[$val]['users_photo'] = $this->s3_url."assets/images/businessprofile/".$this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $profile_id))->row('bf_photo');
                        $getcomment[$val]['users_name']  = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $profile_id))->row('bf_name');
                    }
                    else{
                        $getcomment[$val]['users_photo'] = $this->media_url."assets/images/users/".$getcomment[$val]['users_photo'];
                    }
                }
                
                
                $getcomment[$val]['bp_challenge_comments_id']          = $getcomment[$val]['bp_challenge_posts_comments_id']; 
                $getcomment[$val]['bp_challenge_comments_user_id']     = $getcomment[$val]['bp_challenge_posts_comments_user_id'];
                $getcomment[$val]['bp_challenge_comments_comment']     = $getcomment[$val]['bp_challenge_posts_comments_comment'];
                $getcomment[$val]['bp_challenge_comments_challenge_id']= $getcomment[$val]['bp_challenge_posts_comments_challenge_id'];
                $getcomment[$val]['bp_challenge_comments_date']        = $getcomment[$val]['bp_challenge_posts_comments_challenge_date'];
                
             
            }
            $this->output->set_output(json_encode(['message' => 'successfully commented','comment' => $getcomment ,'status'=> true])); 
        }
        else{

            $this->output->set_output(json_encode(['message' => "operation failed" ,'status'=> false])); 
        }
    }
    else{
        $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> false])); 
    }

    }



     // reply to comments
     function replyChallengecommentsPosts(){

        $this->output->set_content_type('application/json');

        $challangeid        = $this->input->post('challengeid');
        $user_id            = $this->input->post('userid');
        $comment_id         = $this->input->post('comment_id');
        $comment_user       = !empty($this->input->post('comment_user_id')) ? $this->input->post('comment_user_id') : 1;
        $reply              = $this->input->post('reply');
        
       
        $insert      = array('bp_challenge_posts_comments_reply_challenge_id'=>$challangeid,
                        'bp_challenge_posts_comments_reply_comment_id'=>$comment_id,
                        'bp_challenge_posts_comments_reply'=>$reply,
                        'bp_challenge_posts_comment_reply_comment_user_id' => $comment_user,
                        'bp_challenge_posts_comments_reply_user_id' => $user_id);

        $res         = $this->BusinessProfileModel->replyChallengecommentsPosts($insert,$comment_user);

        if($res > 0){

            $this->output->set_output(json_encode(array('message' => "succesfully inserted",'status'=>true)));
        }
        else{

            $this->output->set_output(json_encode(array('posts' => "no value found",'status'=>false)));
        }
    }


    // poll type 2 challenge
    function pollChallenge(){

        $this->output->set_content_type('application/json');
        
        $userid      = $this->input->post('userid');
        $cid         = $this->input->post('challengeid');
        $cuserid     = $this->input->post('challenge_user_id');
        

        if(!empty($userid && $cid)){

        $res = $this->BusinessProfileModel->pollChallenge($userid , $cid ,$cuserid);

        switch ($res){

            case 2:
            $this->output->set_output(json_encode(['message' => 'successfully polled' ,'status'=> true])); 
            break;

            case 3:
            $this->output->set_output(json_encode(['message' => 'successfully unpolled' ,'status'=> true])); 
            break;

            case 0:
            $this->output->set_output(json_encode(['message' => 'operation failed' ,'status'=> false])); 
            break;
        }
       
    }
    else{
        $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> true])); 
    }
    } 

    // poll type 2 challenge
    function pollChallengeposts(){

        $this->output->set_content_type('application/json');
        
        $userid     = $this->input->post('userid');
        $cid        = $this->input->post('challengeid');
        $cpid       = $this->input->post('challengepostid');
        $cuserid    = $this->input->post('challenge_user_id');

        $type    = $this->input->post('type');    // 0 =>  normal poll , 1  => change subpost poll
        

        if(!empty($userid && $cid && $cpid)){
        
        if($type == 1){
            $res = $this->BusinessProfileModel->updatepollpostChallenge($userid , $cid , $cpid );
        }   
        else{ 
        $res = $this->BusinessProfileModel->pollpostChallenge($userid , $cid , $cpid ,$cuserid);
        }
        switch ($res){
            
            case 2:
            $this->output->set_output(json_encode(['message' => 'successfully polled' ,'status'=> true])); 
            break;

            case 3:
            $this->output->set_output(json_encode(['message' => 'successfully unpolled' ,'status'=> true])); 
            break;

            case 0:
            $this->output->set_output(json_encode(['message' => 'operation failed' ,'status'=> false])); 
            break;

            case 4:
            $this->output->set_output(json_encode(['message' => 'successfully polled' ,'status'=> true])); 
            break;

            case 5:
            $this->output->set_output(json_encode(['message' => 'failed to update' ,'status'=> false])); 
            break;
        }
       
    }
    else{
        $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> false])); 
    }
    } 


     // poll type 2 challenge
     function unpollChallengeposts(){

        $this->output->set_content_type('application/json');
        
        $userid  = $this->input->post('userid');
        $cid     = $this->input->post('challengeid');
        $cpid    = $this->input->post('challengepostid');
        

        if(!empty($userid && $cid && $cpid)){

        $res = $this->BusinessProfileModel->unpollChallengeposts($userid , $cid , $cpid);

        switch ($res){
            
            case 2:
            $this->output->set_output(json_encode(['message' => 'successfully polled' ,'status'=> true])); 
            break;

            case 3:
            $this->output->set_output(json_encode(['message' => 'successfully unpolled' ,'status'=> true])); 
            break;

            case 0:
            $this->output->set_output(json_encode(['message' => 'operation failed' ,'status'=> false])); 
            break;
        }
       
    }
    else{
        $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> false])); 
    }
    } 


        // get post comments by id
        function getPostcommentsbyid(){ 

            $this->output->set_content_type('application/json');
    
            $comment_id  = $this->input->post('comment_id');
            $userid      = $this->input->post('userid');  
            
            $res         = $this->BusinessProfileModel->getPostcommentsbyid($comment_id);
    
            $post_id     = $this->BusinessProfileModel->fetch_from_table('business_profile_posts_comments',array('bp_posts_comments_id' => $comment_id))->row('bp_posts_comments_post_id');
            
            $profile_id  = $this->BusinessProfileModel->fetch_from_table('business_profile_posts',array('bp_posts_id' => $post_id))->row('bp_business_profile_id');
            $created_id  = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $profile_id))->row('bf_created_user');
        
            

            foreach($res as $key => $value){

                if($res[$key]['bp_posts_comments_user_id'] == $created_id){
                    $res[$key]['users_photo'] = $this->s3_url."assets/images/businessprofile/".$this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $profile_id))->row('bf_photo');
                    $res[$key]['users_name']  = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $profile_id))->row('bf_name');
                }
                else{
                    $res[$key]['users_photo'] = $this->media_url."assets/images/users/".$res[$key]['users_photo'];
                }

                //$res[$key]['users_photo'] = $this->media_url."assets/images/users/".$res[$key]['users_photo'];
                $res_reply          = $this->BusinessProfileModel->getcommentsreply($res[$key]['bp_posts_comments_id'],$res[$key]['bp_posts_comments_post_id']);
                
                foreach($res_reply as $val  => $keys){
                   
                    
                       if($keys['bp_posts_comments_reply_user_id'] == $created_id)
                        {   
                           
                            $res_reply[$val]['users_photo'] = $this->s3_url."assets/images/businessprofile/".$this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $profile_id))->row('bf_photo');
                            $res_reply[$val]['users_name']  = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $profile_id))->row('bf_name');
                      
                        }
                        else{
                            $res_reply[$val]['users_photo']      = $this->media_url."assets/images/users/".$res_reply[$val]['users_photo'];

                        }
                    
                    
                    
                    $res_reply[$val]['reply_to_user']    = $this->BusinessProfileModel->getusersnamebyid($res_reply[$val]['bp_posts_comment_reply_comment_user_id']);
            
                }   
                $res[$key]['reply'] = $res_reply;
            }
    
            if($res!=null){
    
                $this->output->set_output(json_encode(array('data' => $res,'status'=>true)));
            }
            else{
                $this->output->set_output(json_encode(array('data' => "no value found",'status'=>false)));
            }
        }


    
    // report posts
    function reportChallenge(){

        $this->output->set_content_type('application/json');

        $logged_userid    = $this->input->post('logged_userid');
        $profileid        = $this->input->post('profileid');
        $user_id          = $this->input->post('user_id');
        $challenge_id     = $this->input->post('challenge_id');
        $msg              = $this->input->post('message');
        $page_start       = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;

        $country          = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state            = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district         = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  


        if(!empty($user_id && $challenge_id)){
        
            
        $res         = $this->BusinessProfileModel->reportChallenge($user_id , $challenge_id , $msg);
    
           
        switch($res){

            case $res > 0:

           

            $posts = $this->returnBusinessProfilePosts($user_id,$logged_userid,$page_start,$profileid);
            
            if(count($posts) >= 9 )
            $last_posts = $posts[9];
            else 
            $last_posts = null;
         
            $this->BusinessProfileModel->updateChallengeReportcount($challenge_id);
         
            if($last_posts['challenge_id'] == $challenge_id || $last_posts == null)
            $this->output->set_output(json_encode(array('value' => "successfully reported",'is_completed' =>1,'status'=>true)));
            else
            $this->output->set_output(json_encode(array('value' => "successfully reported",'data' => array($last_posts),'is_completed' => $last_posts['is_completed'] > 0 ? 0 : 1,'status'=>true)));
            
            break;

            case -1:
            $this->output->set_output(json_encode(array('value' => "already reported",'status'=>false)));
            break;
            
            case 0:
            $this->output->set_output(json_encode(array('value' => "operation failed",'status'=>false)));
            break;

        }
    }
        else{
            $this->output->set_output(json_encode(array('message' => "parameter missing",'status'=>false)));
           
        }

    }
            // edit posts
    function editPosts(){

        $this->output->set_content_type('applcaition/json');
        
       
        $postid       = $this->input->post('postid');
        $title        = $this->input->post('title');
        $content      = $this->input->post('content'); 
        $linecount    = $this->input->post('posts_linecount'); 

        if(!empty($postid)){

        
        $update     = array('bp_posts_title' => $title ,'bp_posts_linecount' => $linecount,'bp_posts_uploaded_date' => date('Y-m-d h:m:s'),'bp_posts_content' => $content);
        $res        = $this->BusinessProfileModel->editPosts($postid , $update);
        if($res > 0){

            $this->output->set_output(json_encode(array('value' => 'successfully updated' , 'status' => true)));   
        } 
        else{
            $this->output->set_output(json_encode(array('value' => 'operation failed' , 'status' => false))); 
        }
     }
        else{
            $this->output->set_output(json_encode(array('value' => 'parameter missing' , 'status' => false))); 
   
        }
       
        
     
    }




    // get single challenge by id 
    function getChallengebyid(){

        $this->output->set_content_type('application/json');

        $challengeid = $this->input->post('challengeid');
        $userid      = !empty ($this->input->post('userid')) ? $this->input->post('userid') : 1;
        
        $created_user   = $this->BusinessProfileModel
                                            ->fetch_from_table('business_profile_challenge',array('challenge_id'=>$challengeid))                        
                                            ->row('challenge_user_id');

        $zone = getallheaders();
    
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";


        $dt = new DateTime();
            
        $current_date = $dt->format('d-m-Y h:i A');
    
        $GMT  = new DateTimeZone("GMT");
        $date = new DateTime($current_date, $GMT );
        $date->setTimezone(new DateTimeZone($zone['timezone']));
        $date =   $date->format('Y-m-d h:i A');


       
        if(!empty($challengeid)){

            $res              = $this->BusinessProfileModel->getChallengebyid($challengeid);
            $profile_id       = $this->BusinessProfileModel->fetch_from_table('business_profile_challenge',array('challenge_id' => $challengeid))
                                ->row('challenge_profile_id');  
            if($res != null){
                foreach($res as $value => $key){

                $now            = new DateTime($zone['timezone']);

                $expdate = $res[$value]['challenge_expired_date']." ".$res[$value]['challenge_expired_time'];
                
                              
                $expired_date   = date("Y-m-d h:i A ", strtotime($expdate));
                
                $res[$value]['cdate'] = $date;
                //$date = date('Y-m-d h:i A');
                
                $current_date = strtotime($date);
                $exp_date     = strtotime($expdate);
                $diff   = $exp_date - $current_date;
                $res[$value]['challenge_status']    = $diff > 0 ? 1 : 0;
               
                $expired_date   = new DateTime($expired_date);    
                
                $interval       = $now->diff($expired_date); 
                //$interval->format('%d')." Days ".$interval->format('%h')." Hours ".$interval->format('%i')." Minutes " .$interval->format('%s');
                

                $now_new     = $now->format('Y-m-d H:i:s');
                $expired_new = $expired_date->format('Y-m-d H:i:s');
                
                $now_new_date       = new DateTime($now_new);
                $expired_new_date   = new DateTime($expired_new);

                $interval       = $now_new_date->diff($expired_new_date); 

                if($diff > 0){
               
                $res[$value]['validity_day']    = $interval->format('%d');
                $res[$value]['validity_hour']   = $interval->format('%h');
                $res[$value]['validity_minute'] = $interval->format('%i');
                $res[$value]['validity_second'] = $interval->format('%s');
                
                }
                else{
                $res[$value]['validity_day']    = 0;
                $res[$value]['validity_hour']   = 0;
                $res[$value]['validity_minute'] = 0;
                $res[$value]['validity_second'] = 0;
                }
             

                // time validtity ends here


                if($res[$value]['challenge_posts_sub_type'] == 1) {

                    $explenatory_video                      = $this->BusinessProfileModel->getExplanetoryvideo($res[$value]['challenge_id']);
                    
                    

                    if($explenatory_video){
                    $res[$value]['explanetory_video']       = $this->s3_url."assets/images/business_profile/challenge/type1/".$explenatory_video[0]['bp_challenge_posts_sub_post']; 
                    $res[$value]['explanetory_video_thumb'] = $this->s3_url."assets/images/business_profile/challenge/thumb/".$explenatory_video[0]['bp_challenge_posts_video_thumbnail']; 
                    $res[$value]['challenge_posts_exp_media_type'] = $explenatory_video[0]['bp_challenge_posts_exp_media_type']; 
                        
                    }
                }

                $res[$value]['pollCount']               = $this->BusinessProfileModel->challengePollcount($res[$value]['challenge_id']);
                $res[$value]['commentCount']            = $this->BusinessProfileModel->challengeCommentcount($res[$value]['challenge_id']);
                $res[$value]['challenge_posts_post']    = $this->BusinessProfileModel->getChallengeposts($res[$value]['challenge_id']);
                $res[$value]['comment']                 = $this->BusinessProfileModel->getChallengecomments($res[$value]['challenge_id']);
                
                $res[$value]['totalpollscount']         = $this->BusinessProfileModel->totalPostsCount($res[$value]['challenge_id']);

                $res[$value]['ispolled']                = $this->BusinessProfileModel->isChallengepolled($challengeid , $userid);
                
                foreach($res[$value]['comment'] as $pic => $key_pic){
                    $res[$value]['comment'][$pic]['users_photo'] = $this->media_url."assets/images/users/".$res[$value]['comment'][$pic]['users_photo'];
                }
               
 
                $res[$value]['users_photo']             = $this->s3_url."assets/images/businessprofile/".$this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $res[$value]['challenge_profile_id']))->row('bf_photo');
                $res[$value]['users_name']              = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $res[$value]['challenge_profile_id']))->row('bf_name');
                
            

                foreach($res[$value]['challenge_posts_post'] as $values => $keys){

                        $GMT = new DateTimeZone("GMT");
                        $date = new DateTime($res[$value]['challenge_posts_post'][$values]['bp_challenge_posts_created_date'] , $GMT);
                        $date->setTimezone(new DateTimeZone($zone['timezone']));
                        $res[$value]['challenge_posts_post'][$values]['bp_challenge_posts_created_date']   = $date->format('d-m-Y h:i A');
                    
                    if($res[$value]['challenge_posts_post'][$values]['bp_challenge_posts_user_id'] == $created_user){
                    $res[$value]['challenge_posts_post'][$values]['users_photo']             = $this->s3_url."assets/images/businessprofile/".$this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $res[$value]['challenge_profile_id']))->row('bf_photo');
                    $res[$value]['challenge_posts_post'][$values]['users_name']              = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $res[$value]['challenge_profile_id']))->row('bf_name');
                    }else{
                        $res[$value]['challenge_posts_post'][$values]['users_photo']         = $this->media_url."assets/images/users/".$res[$value]['challenge_posts_post'][$values]['users_photo'];
                        //$res[$value]['challenge_posts_post'][$values]['users_name']              = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $res[$value]['challenge_profile_id']))->row('bf_name');
                    }  
    
                    //$res[$value]['challenge_posts_post'][$values]['users_photo']                        = $this->media_url."assets/images/users/".$res[$value]['challenge_posts_post'][$values]['users_photo']; 
                    $res[$value]['challenge_posts_post'][$values]['pollCount']                          = $this->BusinessProfileModel->challengepostPollcount($res[$value]['challenge_id'],$res[$value]['challenge_posts_post'][$values]['bp_challenge_posts_id']);
                    
                    $res[$value]['challenge_posts_post'][$values]['commentCount']                       = $this->BusinessProfileModel->challengepostCommentcount($res[$value]['challenge_posts_post'][$values]['bp_challenge_posts_id']);
                    if($res[$value]['totalpollscount'] > 0) {
                    $res[$value]['challenge_posts_post'][$values]['pollPercent']                        = intval(($res[$value]['challenge_posts_post'][$values]['pollCount'] * 100 ) / $res[$value]['totalpollscount']);
                    }
                    else{
                        $res[$value]['challenge_posts_post'][$values]['pollPercent'] = 0;
                    }
                    $res[$value]['challenge_posts_post'][$values]['challenge_media_type']               = $res[$value]['challenge_media_type'];
                    $res[$value]['challenge_posts_post'][$values]['parent_position']                    = 0;
                    $res[$value]['challenge_posts_post'][$values]['parent_position']                    = 0;
                    $res[$value]['challenge_posts_post'][$values]['ispolled']                           = $this->BusinessProfileModel->isChallengepostpolled( $res[$value]['challenge_posts_post'][$values]['bp_challenge_posts_id'],$userid);

                    if($res[$value]['challenge_type'] == 1){

                        $res[$value]['challenge_posts_post'][$values]['bp_challenge_posts_sub_post']       = $this->s3_url."assets/images/business_profile/challenge/type1/".$res[$value]['challenge_posts_post'][$values]['bp_challenge_posts_sub_post'];
                       
                    }
                    else{
                        $res[$value]['challenge_posts_post'][$values]['bp_challenge_posts_sub_post']       = $this->s3_url."assets/images/business_profile/challenge/type2/".$res[$value]['challenge_posts_post'][$values]['bp_challenge_posts_sub_post'];
                       
                    }
                        $res[$value]['challenge_posts_post'][$values]['bp_challenge_posts_video_thumbnail']    = $this->s3_url."assets/images/business_profile/challenge/thumb/".$res[$value]['challenge_posts_post'][$values]['bp_challenge_posts_video_thumbnail'];
                    
                        $res[$value]['challenge_posts_post'][$values]['downloadlink']                    = $res[$value]['challenge_posts_post'][$values]['bp_challenge_posts_sub_post'].'?postid='.$res[$value]['challenge_posts_post'][$values]['bp_challenge_posts_challenge_id'].'&&type=3';
                        $res[$value]['challenge_posts_post'][$values]['sharecode']                       = $this->media_url.'post-view?postid='.$res[$value]['challenge_posts_post'][$values]['bp_challenge_posts_id'].'&challengeid='.$res[$value]['challenge_posts_post'][$values]['bp_challenge_posts_challenge_id'].'&type=6';
                 
                }



                $res[$value]['challenge_winner']        = $this->BusinessProfileModel->getWinner($challengeid);
                  
             
                    
                if($res[$value]['challenge_winner']){
                    
                    
                    foreach($res[$value]['challenge_winner'] as $key_1 => $value_1){

                       if($created_user == $res[$value]['challenge_winner'][$key_1]['bp_challenge_posts_user_id']){

                        $res[$value]['challenge_winner'][$key_1]['users_name']             = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $profile_id))
                                                                                             ->row('bf_name');
                        
                        $res[$value]['challenge_winner'][$key_1]['users_photo']            = $this->s3_url."assets/images/businessprofile/".$this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id' => $profile_id))
                                                                                             ->row('bf_photo');  
                       }else{ 

                       $res[$value]['challenge_winner'][$key_1]['users_name']             = $this->BusinessProfileModel->fetch_from_table('users',array('user_id' => $res[$value]['challenge_winner'][$key_1]['bp_challenge_posts_user_id']))
                                                                                            ->row('users_name');
                       $res[$value]['challenge_winner'][$key_1]['users_photo']            = $this->media_url."assets/images/users/".$this->BusinessProfileModel->fetch_from_table('users',array('user_id' => $res[$value]['challenge_winner'][$key_1]['bp_challenge_posts_user_id']))
                                                                                            ->row('users_photo');                                                                     
                       }

                      
                       
                    //    if($res[$value]['challenge_winner'][$key_1]['users_login_type'] == 0){

                       
                    //    $res[$value]['challenge_winner'][$key_1]['users_photo']             = $this->media_url."assets/images/users/".$res[$value]['challenge_winner'][$key_1]['users_photo'];
                    
                    //    }
                    if($res[$value]['challenge_type'] == 1){

                        $res[$value]['challenge_winner'][$key_1]['bp_challenge_posts_sub_post']       = $this->s3_url."assets/images/business_profile/challenge/type1/".$res[$value]['challenge_winner'][$key_1]['bp_challenge_posts_sub_post'];
                       
                    }
                    else{
                        $res[$value]['challenge_winner'][$key_1]['bp_challenge_posts_sub_post']       = $this->s3_url."assets/images/business_profile/challenge/type2/".$res[$value]['challenge_winner'][$key_1]['bp_challenge_posts_sub_post'];
                       
                    }

                    $res[$value]['challenge_winner'][$key_1]['challenge_media_type'] = $res[$value]['challenge_media_type'];
                    
                    $res[$value]['challenge_winner'][$key_1]['bp_challenge_posts_video_thumbnail'] = $this->s3_url."assets/images/business_profile/challenge/thumb/".$res[$value]['challenge_winner'][$key_1]['bp_challenge_posts_video_thumbnail'];
                    $res[$value]['challenge_winner'][$key_1]['pollCount']               = $this->BusinessProfileModel->challengepostPollcount(null,$res[$value]['challenge_winner'][$key_1]['bp_challenge_posts_id']);
                    
                }
            }
            
        }
           
                $this->output->set_output(json_encode(['message' => $res ,'status'=> true])); 

            }
            else{
                $this->output->set_output(json_encode(['message' => 'no data found' ,'status'=> false])); 
            }
        }
        else{
            $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> false])); 
        }
    }


    // get single challenge by id 
    function getChallengebyid_old(){

        $this->output->set_content_type('application/json');
        $challengeid  = $this->input->post('challengeid');

        if(!empty($challengeid)){

            
            $res = $this->BusinessProfileModel->getChallengebyid($challengeid);

            
            if($res != null){
                foreach($res as $value => $key){

                $now            = new DateTime();

                $expdate = $res[$value]['challenge_expired_date']." ".$res[$value]['challenge_expired_time'];
                
                              
                $expired_date   = date("Y-m-d h:i A ", strtotime($expdate));
                
               
                $date = date('Y-m-d h:i A');
                
                $current_date = strtotime($date);
                $exp_date     = strtotime($expdate);
                $diff   = $exp_date - $current_date;
                $res[$value]['challenge_status']    = $diff > 0 ? 1 : 0;
               
                $expired_date   = new DateTime($expired_date);    
                
                $interval       = $now->diff($expired_date); 
                //$interval->format('%d')." Days ".$interval->format('%h')." Hours ".$interval->format('%i')." Minutes " .$interval->format('%s');
                
                if($diff > 0){
               
                $res[$value]['validity_day']    = $interval->format('%d');
                $res[$value]['validity_hour']   = $interval->format('%h');
                $res[$value]['validity_minute'] = $interval->format('%i');
                $res[$value]['validity_second'] = $interval->format('%s');
                
                }
                else{
                $res[$value]['validity_day']    = 0;
                $res[$value]['validity_hour']   = 0;
                $res[$value]['validity_minute'] = 0;
                $res[$value]['validity_second'] = 0;
                }
             

                // time validtity ends here


                if($res[$value]['challenge_posts_sub_type'] == 1) {

                    $explenatory_video                      = $this->BusinessProfileModel->getExplanetoryvideo($res[$value]['challenge_id']);
                    if($explenatory_video){
                    $res[$value]['explanetory_video']       = $this->s3_url."assets/images/business_profile/challenge/type1/".$explenatory_video[0]['challenge_posts_sub_post']; 
                    $res[$value]['explanetory_video_thumb'] = $this->s3_url."assets/images/business_profile/challenge/thumb/".$explenatory_video[0]['challenge_posts_video_thumbnail']; 
                    $res[$value]['challenge_posts_exp_media_type'] = $explenatory_video[0]['bp_challenge_posts_exp_media_type']; 
                   
                    }
                }

                $res[$value]['pollCount']               = $this->BusinessProfileModel->challengePollcount($res[$value]['challenge_id']);
                $res[$value]['commentCount']            = $this->BusinessProfileModel->challengeCommentcount($res[$value]['challenge_id']);
                $res[$value]['challenge_posts_post']    = $this->BusinessProfileModel->getChallengeposts($res[$value]['challenge_id']);
                $res[$value]['comment']                 = $this->BusinessProfileModel->getChallengecomments($res[$value]['challenge_id']);
                
                $res[$value]['totalpollscount']         = $this->BusinessProfileModel->totalPostsCount($res[$value]['challenge_id']);

               
              
                
                foreach($res[$value]['comment'] as $pic => $key_pic){
                    $res[$value]['comment'][$pic]['users_photo'] = $this->media_url."assets/images/users/".$res[$value]['comment'][$pic]['users_photo'];
                }
               
 
                $res[$value]['users_photo']             = $this->media_url."assets/images/users/".$res[$value]['users_photo'];
                   

                foreach($res[$value]['challenge_posts_post'] as $values => $keys){
                    
                    $res[$value]['challenge_posts_post'][$values]['users_photo']                        = $this->media_url."assets/images/users/".$res[$value]['challenge_posts_post'][$values]['users_photo']; 
                    $res[$value]['challenge_posts_post'][$values]['pollCount']                          = $this->BusinessProfileModel->challengepostPollcount($res[$value]['challenge_id'],$res[$value]['challenge_posts_post'][$values]['challenge_posts_id']);
                    if($res[$value]['totalpollscount'] > 0) {
                   
                    $res[$value]['challenge_posts_post'][$values]['pollPercent']                        = intval(($res[$value]['challenge_posts_post'][$values]['pollCount'] * 100 ) / $res[$value]['totalpollscount']);
                    
                    }
                    else{
                        $res[$value]['challenge_posts_post'][$values]['pollPercent'] = 0;
                    }
                    $res[$value]['challenge_posts_post'][$values]['challenge_media_type']               = $res[$value]['challenge_media_type'];
                    
                    

                    if($res[$value]['challenge_type'] == 1){

                        $res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post']       = $this->s3_url."assets/images/business_profile/challenge/type1/".$res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                       
                    }
                    else{
                        $res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post']       = $this->s3_url."assets/images/business_profile/challenge/type2/".$res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                       
                    }
                        $res[$value]['challenge_posts_post'][$values]['challenge_posts_video_thumbnail']    = $this->s3_url."assets/images/business_profile/challenge/thumb/".$res[$value]['challenge_posts_post'][$values]['challenge_posts_video_thumbnail'];
                    
                    //$res[$value]['challenge_posts_post'][$values]['users_photo']                = base_url()."assets/images/users/".$res[$value]['challenge_posts_post'][$values]['users_photo'];
            
                }
            
            }

                $this->output->set_output(json_encode(['message' => $res ,'status'=> true])); 

            }
            else{
                $this->output->set_output(json_encode(['message' => 'no data found' ,'status'=> false])); 
            }
        }
        else{
            $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> false])); 
        }
    }

        //delete challenge
        function deleteChallenge(){

            $this->output->set_content_type('application/json');
    
            $challengeid = $this->input->post('challengeid');
            $userid = $this->input->post('userid');
            $profileid = $this->input->post('profileid');
            $country = $this->input->post('country');
            $state = $this->input->post('state');
            $district = $this->input->post('district');
    
    
            $page_start  = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;
    
            if(!empty($challengeid)){
           
            $res = $this->BusinessProfileModel->deleteChallenge($challengeid);
            if($res > 0){
    
                $posts = $this->returnBusinessProfilePosts($userid,$userid,$page_start,$profileid);
                 
    
                if(count($posts) >= 9 )
                $last_posts = $posts[9];
                else 
                $last_posts = null;
                
                if($last_posts == null)
                $this->output->set_output(json_encode(array('value' => "operation success",'is_completed' => 1,'status'=>true)));
                
                else{
                if($last_posts['is_completed'] > 0)
                $this->output->set_output(json_encode(array('value' => "operation success",'data' => array($last_posts),'is_completed' => 0,'status'=>true)));
                
                else
                $this->output->set_output(json_encode(array('value' => "operation success",'data' => array($last_posts),'is_completed' => 1,'status'=>true)));
                }
    
                   
            }
            else{
                $this->output->set_output(json_encode(['status'=> false , 'value' => 'operation failed'])); 
                }
            }
            else{
                $this->output->set_output(json_encode(['status'=> false , 'value' => 'parameter missing']));
            }
            
    
        }



    //delete challenge
    function deleteChallengepost(){

        $this->output->set_content_type('application/json');

        $challengeid = $this->input->post('postid');
        if(!empty($challengeid)){
       
        $res = $this->BusinessProfileModel->deleteChallengepost($challengeid);
        if($res > 0){

            $this->output->set_output(json_encode(['status'=> true , 'value' => 'successfully deleted'])); 
        }
        else{
            $this->output->set_output(json_encode(['status'=> false , 'value' => 'operation failed'])); 
            }
        }
        else{
            $this->output->set_output(json_encode(['status'=> false , 'value' => 'parameter missing']));
        }
        

    }



    // update challenge posts
    function updateChallengePosts(){

        $this->output->set_content_type('application/json');

        $userid                 = $this->input->post('userid'); 
        $challengeid            = $this->input->post('challengeid');
        $challenge_posts_title  = $this->input->post('challenge_post_title');
        $challenge_media_type   = $this->input->post('challenge_media_type');
        $challenge_type         = ($this->input->post('challenge_type') == 1 ? 50 : 51);

        $posts_width  = $this->input->post('posts_width');
        $posts_height = $this->input->post('posts_height');


        if(!empty($userid && $challengeid)){
            
            $user_array     = $this->input->post('userlist');

            if($user_array != null){

                         
                for($i = 0 ; $i < sizeof($user_array); $i++){

                    $res        = $this->BusinessProfileModel->inviteMembers($user_array[$i] , $challengeid , $userid , $challenge_type);
                    if($res == 1){
                        $msg    = "successfully saved";
                        $status = 'true';
                    }
                    else{

                    }
                }

            }

            switch($challenge_media_type){
                       
                // IMAGE ONLY  (type 1)
                
                case 1:
               
              
                if(isset($_FILES['challenge_post'])){   
                            
                    $errors_thumb    = array();
                    $file_name_thumb = $_FILES['challenge_post']['name'];
                    $file_size_thumb = $_FILES['challenge_post']['size'];
                    $file_tmp_thumb  = $_FILES['challenge_post']['tmp_name'];
                    $file_type_thumb = $_FILES['challenge_post']['type'];
                    $tmp_thumb       = explode('.', $file_name_thumb);
                    $file_ext_thumb  = end($tmp_thumb);

            
                    $thumb_name = "challenge_post-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                    
                    $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                    
                    if(in_array($file_ext_thumb,$expensions_thumb)=== false){

                    $errors_thumb[] = "extension not allowed, please choose a JPEG or PNG file.";
                    
                }
                    
                    
                    
                    if(empty($errors_thumb)==true){
                   
                        $data['tmp_name']  = $_FILES['challenge_post']['tmp_name'];
                        $data['file_name'] = $thumb_name;
        
       
                        $val = $this->aws3->sendFile('daypoll/assets/images/business_profile/challenge/type1',$data);   
                          

                    //move_uploaded_file($file_tmp_thumb,"assets/images/challenge/type1/".$thumb_name);
                    $challenge_image = $thumb_name;

                    $insert_challenge_post =   
                                array( 'bp_challenge_posts_challenge_id'       =>  $challengeid,
                                'bp_challenge_posts_title'                     =>  $challenge_posts_title,
                                'bp_challenge_posts_sub_post'                  =>  $challenge_image,
                                'bp_challenge_posts_user_id'                   =>  $userid,
                                'bp_posts_width'                               =>  $posts_width,
                                'bp_posts_height'                              =>  $posts_height
                            
                                );

                    $insert_challenge_posts = $this->BusinessProfileModel->uploadChallengeposts($insert_challenge_post);
    
                    if($insert_challenge_posts > 0){
                        $last_post  = $this->BusinessProfileModel->getLastinsertedpost($insert_challenge_posts);
                          
                            
                                foreach($last_post as $val => $key){
                                    $last_post[$val]['bp_challenge_posts_video_thumbnail'] = $this->s3_url."assets/images/business_profile/challenge/thumb/".$last_post[$val]['bp_challenge_posts_video_thumbnail'];
                                    $last_post[$val]['bp_challenge_posts_sub_post']        = $this->s3_url."assets/images/business_profile/challenge/type1/".$last_post[$val]['bp_challenge_posts_sub_post'];
                                    $last_post[$val]['users_photo']                     = $this->media_url."assets/images/users/".$last_post[$val]['users_photo'];
                                    $last_post[$val]['pollCount']                       = $this->BusinessProfileModel->challengepostPollcount($challengeid , $insert_challenge_posts);
                                    $last_post[$val]['pollPercent']                     = 0; 
                                    $last_post[$val]['challenge_media_type']            = $challenge_media_type;
                                    $last_post[$val]['ispolled']                        = 0;
                                    $last_post[$val]['parent_position']                 = 0;
                                    $last_post[$val]['commentCount']                    = $this->BusinessProfileModel->challengepostCommentcount($insert_challenge_posts);
                            
                                    $last_post[$val]['downloadlink']                    = $last_post[$val]['bp_challenge_posts_sub_post'].'?postid='.$challengeid.'&&type=3';
                                    $last_post[$val]['sharecode']                       = $this->media_url.'post-view?postid='.$last_post[$val]['bp_challenge_posts_id'].'&&type=6';

                                }
                            
                            
                            
                        $msg    = "successfully saved";
                        $status = 'true';
                    }
                    else{

                        $msg    = "failed to save";
                        $status = 'false';
                    }
                    
                    }else{
                      
                        $challenge_image = "null";
                    }
                }

                if(empty($last_post)){
                    
                    $last_post = [];
                }
                $this->output->set_output(json_encode(['message' => $msg ,'last_post' => $last_post,'status'=> $status])); 
                break;

                case 2:
               
                if(isset($_FILES['challenge_video_thumbnail'])){                                                                                                                                              

                    $errors_thumb    = array();
                    $file_name_thumb = $_FILES['challenge_video_thumbnail']['name'];
                    $file_size_thumb = $_FILES['challenge_video_thumbnail']['size'];
                    $file_tmp_thumb  = $_FILES['challenge_video_thumbnail']['tmp_name'];
                    $file_type_thumb = $_FILES['challenge_video_thumbnail']['type'];
                    $tmp_thumb       = explode('.', $file_name_thumb);
                    $file_ext_thumb  = end($tmp_thumb);

            
                    $thumb_name = "challenge_thumb-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                    
                    $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                    
                    if(in_array($file_ext_thumb,$expensions_thumb)=== false){

                    $errors_thumb[] = "extension not allowed, please choose a JPEG or PNG file.";
                    
                    }
                    
                    if(empty($errors_thumb)==true){
                   
                        $data['tmp_name']  = $_FILES['challenge_video_thumbnail']['tmp_name'];
                        $data['file_name'] = $thumb_name;
        
       
                        


                        $val = $this->aws3->sendFile('daypoll/assets/images/business_profile/challenge/thumb',$data);   
                  
                    //move_uploaded_file($file_tmp_thumb,"assets/images/challenge/thumb/".$thumb_name);
                    $challenge_video_thumb = $thumb_name;
                    
                    
                    
                    }else{
                      
                        $challenge_video_thumb = "null";
                    }
                }

            // challenge video upload
                                            
            if(isset($_FILES['challenge_post'])){

                $errors    = array();
                $file_name = $_FILES['challenge_post']['name'];
                $file_size = $_FILES['challenge_post']['size'];
                $file_tmp  = $_FILES['challenge_post']['tmp_name'];
                $file_type = $_FILES['challenge_post']['type'];
                $tmp       = explode('.', $file_name);
                $file_ext  = end($tmp);


                $post_name = "challenge-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;
                
                $expensions = array("jpeg","jpg","png","mp4","3gp");
                
                if(in_array($file_ext,$expensions)=== false){

                $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                    
                }
                    
                    if(empty($errors)==true){

                        $data['tmp_name']  = $_FILES['challenge_post']['tmp_name'];
                        $data['file_name'] = $post_name;
        
       
                        $val = $this->aws3->sendFile_video('daypoll/assets/images/business_profile/challenge/type1',$data);   
                  
                        //move_uploaded_file($file_tmp,"assets/images/challenge/type1/".$post_name);
                        $challenge_post = $post_name;

                        $insert_challenge_post = 
                        
                        array( 'bp_challenge_posts_challenge_id'       =>  $challengeid,
                        'bp_challenge_posts_title'                     =>  $challenge_posts_title,
                        'bp_challenge_posts_sub_post'                  =>  $challenge_post,
                        'bp_challenge_posts_video_thumbnail'           =>  $challenge_video_thumb,
                        'bp_challenge_posts_user_id'                   =>  $userid,
                        'bp_posts_width'                               =>  $posts_width,
                        'bp_posts_height'                              =>  $posts_height                      
                        );
                    
                        $insert_challenge_posts = $this->BusinessProfileModel->uploadChallengeposts($insert_challenge_post);

                        if($insert_challenge_posts > 0){
                            
                            $last_post  = $this->BusinessProfileModel->getLastinsertedpost($insert_challenge_posts);
                            foreach($last_post as $val => $key){
                                $last_post[$val]['challenge_posts_video_thumbnail'] = $this->s3_url."assets/images/business_profile/challenge/thumb/".$last_post[$val]['bp_challenge_posts_video_thumbnail'];
                                $last_post[$val]['challenge_posts_sub_post']        = $this->s3_url."assets/images/business_profile/challenge/type1/".$last_post[$val]['bp_challenge_posts_sub_post'];
                                
                                $last_post[$val]['bp_challenge_posts_video_thumbnail'] = $this->s3_url."assets/images/business_profile/challenge/thumb/".$last_post[$val]['bp_challenge_posts_video_thumbnail'];
                                $last_post[$val]['bp_challenge_posts_sub_post']        = $this->s3_url."assets/images/business_profile/challenge/type1/".$last_post[$val]['bp_challenge_posts_sub_post'];
                                
                                $last_post[$val]['users_photo']                     = $this->media_url."assets/images/users/".$last_post[$val]['users_photo'];
                                $last_post[$val]['pollCount']                       = $this->BusinessProfileModel->challengepostPollcount($challengeid , $insert_challenge_posts);
                                $last_post[$val]['pollPercent']                     = 0; 
                                $last_post[$val]['challenge_media_type']            = $challenge_media_type;
                                $last_post[$val]['ispolled']                        = 0;
                                $last_post[$val]['parent_position']                 = 0;
                                $last_post[$val]['commentCount']                    = $this->BusinessProfileModel->challengepostCommentcount($insert_challenge_posts);
                            
                                $last_post[$val]['downloadlink']                    = $last_post[$val]['bp_challenge_posts_sub_post'].'?postid='.$challengeid.'&&type=3';
                                $last_post[$val]['sharecode']                       = $this->media_url.'post-view?postid='.$last_post[$val]['bp_challenge_posts_id'].'&&type=6';

                            }
                            $msg        = "successfully saved";
                            $status     = 'true';
                        }
                        else{
    
                            $msg    = "successfully saved";
                            $status = 'true';
                        }

                    }
                    else{
                        $challenge_post = "null";
                    }
                }
                if(empty($last_post)){
                    
                    $last_post = [];
                }
                $this->output->set_output(json_encode(['message' => $msg ,'last_post' => $last_post,'status'=> $status])); 
                break;


            }


        }
        else{
            $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> false])); 
     
        }
     

    }


    // get posts api by posts id
    function getnotificationPostsbyid(){

        $this->output->set_content_type('application/json');
       
        $postsid = $this->input->post('postid');
        $userid  = $this->input->post('userid');

        if(!empty($postsid)){

        $res                  = $this->BusinessProfileModel->getnotificationPostsbyid($postsid);
        $pollcount            = $this->BusinessProfileModel->pollsCount($postsid);
        $isPostpolled         = $this->BusinessProfileModel->isPostpolled($postsid,$userid);
        $commentcount         = $this->BusinessProfileModel->commentsCount($postsid);
        $commentsCountouter   = $this->BusinessProfileModel->commentsCountouter($postsid);


       
        foreach($commentcount['data'] as $co => $va){
            
            $commentcount['data'][$co]['users_photo'] = $this->media_url."assets/images/users/".$commentcount['data'][$co]['users_photo']; 
        }
        if($res > 0){
            
            foreach($res as $key => $val){
                
                $res[$key]['bp_posts_photo_or_video']  = $this->s3_url."assets/images/business_profile/".$res[$key]['bp_posts_photo_or_video'];
               
                $res[$key]['posts_photo_or_video']  = $this->s3_url."assets/images/business_profile/".$res[$key]['bp_posts_photo_or_video'];
                $res[$key]['posts_video_thumbnail'] = $this->s3_url."assets/images/business_profile/thumbnails/".$res[$key]['bp_posts_video_thumbnail'];
                //$res[$key]['users_photo']           = $this->media_url."assets/images/users/".$res[$key]['users_photo'];
                $res[$key]['poll_count']            = $pollcount['count'];
                $res[$key]['comment_count']         = $commentsCountouter;
                $res[$key]['comment']               = $commentcount['data'];
                $res[$key]['is_polled']             = $isPostpolled > 0 ? true : false;
                $res[$key]['users_photo']           = $this->s3_url."assets/images/businessprofile/".$this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id'=>$res[$key]['bp_business_profile_id']))->row('bf_photo');
                $res[$key]['users_name']            = $this->BusinessProfileModel->fetch_from_table('business_profile',array('bf_id'=>$res[$key]['bp_business_profile_id']))->row('bf_name');
                $res[$key]['sharecode']            = $this->media_url.'post-view?postid='.$postsid.'&&type=5';
  
            }

            
            $this->output->set_output(json_encode(array('value' => $res , 'status' => true))); 
        }
        else{
            $this->output->set_output(json_encode(array('value' => 'no data found' , 'status' => false))); 
        }
       
        }
        else{

            $this->output->set_output(json_encode(array('value' => 'parameter missing' ,'status' => false)));
        }
    }


  // get group invite members
  function getBusinessProfileInvitemembers(){
            
    $this->output->set_content_type('application/json');
    $profileid         = $this->input->post('profile_id');
    $userid            = $this->input->post('userid');
    if(!empty($profileid && $userid)){
    
   
    $res_users      = $this->BusinessProfileModel->FollowedMembers($profileid);
    
    


    
   if($res_users){
       
    $res = array();        
   
    foreach($res_users as $val){
      
        if($val['user_id'] != $userid){
           
            $res[] = $val;
        }
    }
    
    if($res){
    foreach($res as $value => $key){
        
        
            $res[$value]['pollCount']   = $this->BusinessProfileModel->getPollCount($res[$value]['user_id']);
            $res[$value]['users_photo'] = $this->media_url."assets/images/users/".$res[$value]['users_photo'];

        
    }
    
     $response = $res;    
     $status = "true";
    }
    else{
      
      $response = [];
      $status = "false";
    }

        $this->output->set_output(json_encode(array('value' => $response , 'status' => $status)));
    
   }
   
    else{
        $this->output->set_output(json_encode(array('value' =>[],'status' => false)));
    }
}
else{

    $this->output->set_output(json_encode(array('value' =>'parameter missing','status' => false)));

}
}




    
    function test(){

        $usage = [
            'January'  => '100',
            'February' => '145',
            'March'    => '110',
            'April' => '163',
            'May' => '212',
            'June' => '99',
            'July' => '120',
            'Aug' => '150',
            'Sep' => '184',
            'Oct' => '159',
            'Nov' => '140',
            'Dec' => '194',
            ];
            $arr = [];
            foreach($usage as $key => $val){
                
                if($usage[$key] > 100){
                    
                    $min_cost      = 100 * 6; 
                    $exceeded_unit = $usage[$key] - 100;
                    $total_amount  = $exceeded_unit * 9;
                    $arr[$key]     = $total_amount + $min_cost;
                   
                  
                     
                }
                else if($usage[$key] < 100){
                    $total_amount  = $usage[$key] * 6;
                    $arr[$key]     = $total_amount;
                   
                }

                else if($usage[$key] == 100){
                    $total_amount  = $usage[$key] * 6;
                    $arr[$key]     = $total_amount;
                   
                }
                
                
            }

            print_r($arr);
    }

    function s3_upload(){

        if(isset($_FILES['photo_video'])){

            $errors    = array();
            $file_name = $_FILES['photo_video']['name'];
            $file_size = $_FILES['photo_video']['size'];
            $file_tmp  = $_FILES['photo_video']['tmp_name'];
            $file_type = $_FILES['photo_video']['type'];
            $tmp       = explode('.', $file_name);
            $file_ext  = end($tmp);


            
            $post_name = "posts-".mt_rand(100000,999999).date('H-i-s').'.'.$file_ext;
            
            $expensions = array("jpeg","jpg","png","mp4","3gp");
            
            if(in_array($file_ext,$expensions)=== false){

            $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                
            }
                $flag = 0;
                if(empty($errors)==true){

                    $data['tmp_name']  = $_FILES['photo_video']['tmp_name'];
                    $data['file_name'] = $post_name;
                  
                    $val = $this->aws3->sendFile('daypoll/assets/images/posts',$data); 
                   
                    print $post_name;
            }
    }

    //     if(isset($_FILES['challenge_exp_post'])){   
                   
    //         $errors_thumb    = array();
    //         $file_name_thumb = $_FILES['challenge_exp_post']['name'];
    //         $file_size_thumb = $_FILES['challenge_exp_post']['size'];
    //         $file_tmp_thumb  = $_FILES['challenge_exp_post']['tmp_name'];
    //         $file_type_thumb = $_FILES['challenge_exp_post']['type'];
    //         $tmp_thumb       = explode('.', $file_name_thumb);
    //         $file_ext_thumb  = end($tmp_thumb);

    
    //         $thumb_name = "challenge_post-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
            
    //         $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
            
    //         if(in_array($file_ext_thumb,$expensions_thumb)=== false){

    //         $errors_thumb[] = "extension not allowed, please choose a JPEG or PNG file.";
            
    //     }
            
            
            
    //         if(empty($errors_thumb) == true){
            
    //             $data['tmp_name']  = $_FILES['challenge_exp_post']['tmp_name'];
    //             $data['file_name'] = $thumb_name;

    //             $challenge_image = $thumb_name;

               
    //              $this->aws3->sendFile('daypoll/assets/images/business_profile/challenge/type1',$data['file_name']); 
                
    //              //move_uploaded_file($file_tmp_thumb,"assets/images/partyposts/thumbnail/".$thumb_name);
                                       

          
    //         }else{
              
    //             $challenge_image = "null";
    //         }

    //         echo $challenge_image;
    //     }else{
    //             echo "no data";
    //             echo $challenge_image;
    //     }


    // }
     
    }
   }    
    

