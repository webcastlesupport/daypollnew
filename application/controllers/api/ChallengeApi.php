<?php

class ChallengeApi extends CI_Controller{

    public $media_url= "https://s3.amazonaws.com/daypoll/";
    public $s3_url   = "https://s3.amazonaws.com/daypoll/";
    public $video_base_url = "http://share.daypoll.com/";


   
    
    function __construct()
    {
        //error_reporting(0);
        
        parent::__construct();
        $this->load->model(array('ChallengeApiModel','CommonModel','UsersApiModel'));
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('', ''); 
        $this->load->library('aws3');
    }

    // create challenge
    function createChallenge(){                                                             

        $this->output->set_content_type('application/json');

        $userid                     = $this->input->post('userid');
        $challengetitle             = $this->input->post('challenge_title');
        $challengelinecount         = $this->input->post('linecount');
        $challenge_content          = $this->input->post('challenge_content');
        $challenge_type             = $this->input->post('challenge_type');            // type 1 or type 2
        $challenge_sub_type         = $this->input->post('challenge_posts_sub_type');  // type 1 ( 1 or 2 )    
        $challenge_created          = $this->input->post('challenge_created_date');
        $challenge_exp_date         = $this->input->post('challenge_expired_date');
        $challenge_exp_time         = $this->input->post('challenge_expired_time');
        $challenge_media_type       = $this->input->post('challenge_media_type');
        $challenge_exp_media_type   = $this->input->post('challenge_exp_media_type');
        $user_array                 = $this->input->post('userlist');
       
        $flag                       = $challenge_type == 1 ? 4 :64; 
        $country                    = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state                      = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district                   = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        $area                       = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;


        $this->form_validation->set_rules("userid","userid","required");
        $this->form_validation->set_rules("challenge_title","challenge_title","required");
        $this->form_validation->set_rules("challenge_type","challenge_type","required");
        $this->form_validation->set_rules("challenge_created_date","challenge_created_date","required");
        $this->form_validation->set_rules("challenge_expired_date","challenge_expired_date","required");
        $this->form_validation->set_rules("challenge_expired_time","challenge_expired_time","required");
        $this->form_validation->set_rules("challenge_media_type","challenge_media_type","required");

       
         if($challengelinecount == NULL){
            $challengelinecount = 0;
        }

        if($this->form_validation->run() == TRUE){
    
            switch($challenge_type){
                 
                case 1:
                
                $c_email        = !empty($this->input->post('challenge_email')) ? $this->input->post('challenge_email') : NULL;
                $c_ref_num      = !empty($this->input->post('challenge_ref_number')) ? $this->input->post('challenge_ref_number') : NULL;
                $c_gst          = !empty($this->input->post('challenge_gst')) ? $this->input->post('challenge_ref_number') : NULL;
                $c_service_amt  = !empty($this->input->post('challenge_service_amt')) ? $this->input->post('challenge_service_amt') : NULL;
                $c_total        = !empty($this->input->post('challenge_total')) ? $this->input->post('challenge_total') : NULL;

                $insert_challenge =        array('challenge_user_id'             => $userid,
                                                'challenge_title'                => $challengetitle,
                                                'challenge_content'              => $challenge_content,
                                                'challenge_type'                 => $challenge_type,
                                                'challenge_created_date'         => $challenge_created,
                                                'challenge_expired_date'         => $challenge_exp_date,
                                                'challenge_expired_time'         => $challenge_exp_time,
                                                'challenge_media_type'           => $challenge_media_type,
                                                'challenge_posts_sub_type'       => $challenge_sub_type,
                                                'challenge_line_count'           => $challengelinecount,
                                                'challenge_payment_email'        => $c_email,
                                                'challenge_payment_ref_number'   => $c_ref_num,
                                                'challenge_payment_gst'          => $c_gst,
                                                'challenge_payment_service_amt'  => $c_service_amt,
                                                'challenge_payment_total'        => $c_total,
                                                'challenge_country'              => $country,
                                                'challenge_state'                => $state,
                                                'challenge_city'                 => $district,
                                                'challenge_area'                 => $area
                                               
                                              );

                    $result_id = $this->ChallengeApiModel->createChallenge($insert_challenge);
                    

                 

                    if($result_id){

                        if($user_array != null){

                         
                            for($i = 0 ; $i < sizeof($user_array); $i++){

                                $res        = $this->ChallengeApiModel->inviteMembers($user_array[$i] , $result_id , $userid , $flag);
                            
                            }

                        }

                    switch($challenge_media_type){
                       
                       
                        // IMAGE ONLY  (type 1)
                        
                        case 1:
                        $challenge_posts_title      = $this->input->post('challenge_post_title');
                        

                        if(isset($_FILES['challenge_post'])){   
                          
                            $errors_thumb    = array();
                            $file_name_thumb = $_FILES['challenge_post']['name'];
                            $file_size_thumb = $_FILES['challenge_post']['size'];
                            $file_tmp_thumb  = $_FILES['challenge_post']['tmp_name'];
                            $file_type_thumb = $_FILES['challenge_post']['type'];
                            $tmp_thumb       = explode('.', $file_name_thumb);
                            $file_ext_thumb  = end($tmp_thumb);

                    
                            $thumb_name = "challenge_post-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                            
                            $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                            
                            if(in_array($file_ext_thumb,$expensions_thumb)=== false){

                            $errors_thumb[] = "extension not allowed, please choose a JPEG or PNG file.";
                            
                        }                     
                            
                            if(empty($errors_thumb)==true){

                                $data['tmp_name']  = $_FILES['challenge_post']['tmp_name'];
                                $data['file_name'] = $thumb_name;

               
                                $val = $this->aws3->sendFile('daypoll/assets/images/challenge/type1',$data);   
                           
                            //move_uploaded_file($file_tmp_thumb,"assets/images/challenge/type1/".$thumb_name);
                            $challenge_image = $thumb_name;
                            
                            $posts_width  = $this->input->post('posts_width');
                            $posts_height = $this->input->post('posts_height');

                                                       
                            $insert_challenge_post =   
                            array( 'challenge_posts_challenge_id'       =>  $result_id,
                            'challenge_posts_title'                     =>  $challenge_posts_title,
                            'challenge_posts_sub_post'                  =>  $challenge_image,
                            'challenge_posts_user_id'                   =>  $userid,
                            'posts_width'                               =>  $posts_width,
                            'posts_height'                              =>  $posts_height
                           
                            );

                            
                            
                            $insert_challenge_posts = $this->ChallengeApiModel->uploadChallengeposts($insert_challenge_post);
            
                           
                            
                            }
                            else{
                              
                                $challenge_image = "null";
                            }
                        }
                    

                        break;

                        // video only
                        case 2:
                        $challenge_posts_title = $this->input->post('challenge_post_title');
                        if(isset($_FILES['challenge_video_thumbnail'])){

                            $errors_thumb    = array();
                            $file_name_thumb = $_FILES['challenge_video_thumbnail']['name'];
                            $file_size_thumb = $_FILES['challenge_video_thumbnail']['size'];
                            $file_tmp_thumb  = $_FILES['challenge_video_thumbnail']['tmp_name'];
                            $file_type_thumb = $_FILES['challenge_video_thumbnail']['type'];
                            $tmp_thumb       = explode('.', $file_name_thumb);
                            $file_ext_thumb  = end($tmp_thumb);

                    
                            $thumb_name = "challenge_thumb-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                            
                            $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                            
                            if(in_array($file_ext_thumb,$expensions_thumb)=== false){

                            $errors_thumb[]="extension not allowed, please choose a JPEG or PNG file.";
                            
                        }
                            
                            
                            
                            if(empty($errors_thumb)==true){
                           
                                $data['tmp_name']  = $_FILES['challenge_video_thumbnail']['tmp_name'];
                                $data['file_name'] = $thumb_name;
                
               
                                $val = $this->aws3->sendFile('daypoll/assets/images/challenge/thumb',$data); 
                            //move_uploaded_file($file_tmp_thumb,"assets/images/challenge/thumb/".$thumb_name);
                                $challenge_video_thumb = $thumb_name;

                            }else{
                              
                                $challenge_video_thumb = "null";
                            }
                        }

                    // challenge video upload
                                                    
                    if(isset($_FILES['challenge_post'])){

                        $errors    = array();
                        $file_name = $_FILES['challenge_post']['name'];
                        $file_size = $_FILES['challenge_post']['size'];
                        $file_tmp  = $_FILES['challenge_post']['tmp_name'];
                        $file_type = $_FILES['challenge_post']['type'];
                        $tmp       = explode('.', $file_name);
                        $file_ext  = end($tmp);

                        $challenge_post_width  = $this->input->post('posts_width');
                        $challenge_post_height = $this->input->post('posts_height');

                        if($challenge_post_width < $challenge_post_height) {

                        $post_name = "challenge-portrait-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;

                        }else{

                        $post_name = "challenge-landscape-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;

                        }


                        
                        $expensions = array("jpeg","jpg","png","mp4","3gp");
                        
                        if(in_array($file_ext,$expensions)=== false){

                        $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                            
                        }
                            
                            if(empty($errors)==true){
                                $data['tmp_name']  = $_FILES['challenge_post']['tmp_name'];
                                $data['file_name'] = $post_name;


                                $val = $this->aws3->sendFile_video('daypoll/assets/images/challenge/type1',$data); 
                                
                                //move_uploaded_file($file_tmp,"assets/images/challenge/type1/".$post_name);
                                $challenge_post = $post_name;
                                $posts_width  = $this->input->post('posts_width');
                                $posts_height = $this->input->post('posts_height');
                             //   for($i = 0 ; $i < sizeof($posts_width) ; $i++){
                                $insert_challenge_post = 
                                
                                array( 'challenge_posts_challenge_id'       =>  $result_id,
                                'challenge_posts_title'                     =>  $challenge_posts_title,
                                'challenge_posts_sub_post'                  =>  $challenge_post,
                                'challenge_posts_video_thumbnail'           =>  $challenge_video_thumb,
                                'challenge_posts_user_id'                   =>  $userid,
                                'posts_width'                               =>  $posts_width,
                                'posts_height'                              =>  $posts_height,
                                'status'                                    =>  0
                                );
                            
                                $insert_challenge_posts = $this->ChallengeApiModel->uploadChallengeposts($insert_challenge_post);

                           // }
                            }
                            else{
                                $challenge_post = "null";
                            }
                        }



                    }

                    if($challenge_exp_media_type != null){
                        
                        switch($challenge_exp_media_type){

                           

                            case 1:
                            if(isset($_FILES['challenge_exp_video_thumbnail'])){

                                $errors_thumb    = array();
                                $file_name_thumb = $_FILES['challenge_exp_video_thumbnail']['name'];
                                $file_size_thumb = $_FILES['challenge_exp_video_thumbnail']['size'];
                                $file_tmp_thumb  = $_FILES['challenge_exp_video_thumbnail']['tmp_name'];
                                $file_type_thumb = $_FILES['challenge_exp_video_thumbnail']['type'];
                                $tmp_thumb       = explode('.', $file_name_thumb);
                                $file_ext_thumb  = end($tmp_thumb);
    
                        
                                $thumb_name = "challenge_thumb-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                                
                                $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                                
                                if(in_array($file_ext_thumb,$expensions_thumb)=== false){
    
                                $errors_thumb[]="extension not allowed, please choose a JPEG or PNG file.";
                                
                            }
                                
                                
                                if(empty($errors_thumb)==true){
                               
                                    $data['tmp_name']  = $_FILES['challenge_exp_video_thumbnail']['tmp_name'];
                                    $data['file_name'] = $thumb_name;
                    
                   
                                    $val = $this->aws3->sendFile('daypoll/assets/images/challenge/thumb',$data);    
                                //move_uploaded_file($file_tmp_thumb,"assets/images/challenge/thumb/".$thumb_name);
                                $challenge_video_thumb = $thumb_name;
                                
                                
                                
                                }else{
                                  
                                    $challenge_video_thumb = "null";
                                }
                            }

                            if(isset($_FILES['challenge_exp_post'])){

                                $errors    = array();
                                $file_name = $_FILES['challenge_exp_post']['name'];
                                $file_size = $_FILES['challenge_exp_post']['size'];
                                $file_tmp  = $_FILES['challenge_exp_post']['tmp_name'];
                                $file_type = $_FILES['challenge_exp_post']['type'];
                                $tmp       = explode('.', $file_name);
                                $file_ext  = end($tmp);
        
        
                                $post_name = "challenge-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;
                                
                                $expensions = array("jpeg","jpg","png","mp4","3gp");
                                
                                if(in_array($file_ext,$expensions)=== false){
        
                                $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                                    
                                }
                                    
                                    if(empty($errors)==true){
                                        
                                        $data['tmp_name']  = $_FILES['challenge_exp_post']['tmp_name'];
                                        $data['file_name'] = $post_name;
                
               
                                        $val = $this->aws3->sendFile_video('daypoll/assets/images/challenge/type1',$data); 
                                
                                        
                                        //move_uploaded_file($file_tmp,"assets/images/challenge/type1/".$post_name);
                                        $challenge_post = $post_name;
                                        $posts_width  = $this->input->post('posts_width');
                                        $posts_height = $this->input->post('posts_height');
                                        //for($i = 0 ; $i < sizeof($posts_width) ; $i++){
                                        $insert_challenge_post = 
                                        
                                        array( 'challenge_posts_challenge_id'       =>  $result_id,
                                        'challenge_posts_title'                     =>  $challenge_posts_title,
                                        'challenge_posts_sub_post'                  =>  $challenge_post,
                                        'challenge_posts_video_thumbnail'           =>  $challenge_video_thumb,
                                        'challenge_posts_exp_media_type'            =>  1,
                                        'challenge_posts_type'                      =>  1,
                                        'challenge_posts_user_id'                   =>  $userid,
                                        'posts_width'                               =>  $posts_width,
                                        'posts_height'                              =>  $posts_height               
                                        );
                                    
                                        $insert_challenge_posts = $this->ChallengeApiModel->uploadChallengeposts($insert_challenge_post);
        
                                   // }
                                    }
                                    else{
                                        $challenge_post = "null";
                                    }
                                }

                                break;


                                case 0:

                                if(isset($_FILES['challenge_exp_post'])){   
                                    
                                    $errors_thumb    = array();
                                    $file_name_thumb = $_FILES['challenge_exp_post']['name'];
                                    $file_size_thumb = $_FILES['challenge_exp_post']['size'];
                                    $file_tmp_thumb  = $_FILES['challenge_exp_post']['tmp_name'];
                                    $file_type_thumb = $_FILES['challenge_exp_post']['type'];
                                    $tmp_thumb       = explode('.', $file_name_thumb);
                                    $file_ext_thumb  = end($tmp_thumb);
        
                            
                                    $thumb_name = "challenge_post-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                                    
                                    $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                                    
                                    if(in_array($file_ext_thumb,$expensions_thumb)=== false){
        
                                    $errors_thumb[] = "extension not allowed, please choose a JPEG or PNG file.";
                                    
                                }
                                    
                                    
                                    
                                    if(empty($errors_thumb)==true){
                                    
                                        $data['tmp_name']  = $_FILES['challenge_exp_post']['tmp_name'];
                                        $data['file_name'] = $thumb_name;
                
               
                                        $val = $this->aws3->sendFile('daypoll/assets/images/challenge/type1',$data); 
                                



                                    //move_uploaded_file($file_tmp_thumb,"assets/images/challenge/type1/".$thumb_name);
                                    $challenge_image = $thumb_name;
                                    $posts_width  = $this->input->post('posts_width');
                                    $posts_height = $this->input->post('posts_height');
                                   
                                    //for($i = 0 ; $i < sizeof($posts_width) ; $i++){
                                    $insert_challenge_post =   
                                   
                                    array( 'challenge_posts_challenge_id'       =>  $result_id,
                                    'challenge_posts_title'                     =>  $challenge_posts_title,
                                    'challenge_posts_sub_post'                  =>  $challenge_image,
                                    'challenge_posts_type'                      =>  1,
                                    'challenge_posts_user_id'                   =>  $userid,
                                    'posts_width'                               =>  $posts_width,
                                    'posts_height'                              =>  $posts_height
                                   
                                   
                                    );

                                    $insert_challenge_posts = $this->ChallengeApiModel->uploadChallengeposts($insert_challenge_post);
                    
                                //}
                                    
                                    }else{
                                      
                                        $challenge_image = "null";
                                    }
                                }

                                break;
                            
                        }
                }
                    $this->output->set_output(json_encode(['message' => "successfully saved ", 'status' => "true",'challenge_id' => null]));
                    
                }
                else{
                    $this->output->set_output(json_encode(['message' => "failed to upload", 'status' => "false"]));
                    
                }

                                              

                    break;
                              

            
                case 2:
                  
                
                $insert_challenge =        array('challenge_user_id'             => $userid,
                                                'challenge_title'                => $challengetitle,
                                                'challenge_content'              => $challenge_content,
                                                'challenge_type'                 => $challenge_type,
                                                'challenge_created_date'         => $challenge_created,
                                                'challenge_expired_date'         => $challenge_exp_date,
                                                'challenge_expired_time'         => $challenge_exp_time,
                                                'challenge_media_type'           => $challenge_media_type,
                                                'challenge_line_count'           => $challengelinecount,
                                                'challenge_country'              => $country,
                                                'challenge_state'                => $state,
                                                'challenge_city'                 => $district,
                                                'challenge_area'                 => $area
                                               
                                              );


                $result_id = $this->ChallengeApiModel->createChallenge($insert_challenge);


                if($result_id > 0) {

            /*******************************   IMAGE ONLY     ***********************************/
                   

                            switch($challenge_media_type){

                                case 1:
                                
                                    $challenge_posts_title = $this->input->post('challenge_post_title');
                                    
                                   
                                    
                                    for($i=0;$i<sizeof($challenge_posts_title);$i++){
    
                                   
                                   // image only
                                 
                                if(isset($_FILES['challenge_post'])){   
                                    
                                    $errors_thumb    = array();
                                    $file_name_thumb = $_FILES['challenge_post']['name'][$i];
                                    $file_size_thumb = $_FILES['challenge_post']['size'][$i];
                                    $file_tmp_thumb  = $_FILES['challenge_post']['tmp_name'][$i];
                                    $file_type_thumb = $_FILES['challenge_post']['type'][$i];
                                    $tmp_thumb       = explode('.', $file_name_thumb);
                                    $file_ext_thumb  = end($tmp_thumb);
    
                                    
                                    $thumb_name = "challenge_post-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                                    
                                    $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                                    
                                    if(in_array($file_ext_thumb,$expensions_thumb)=== false){
    
                                    $errors_thumb[]="extension not allowed, please choose a JPEG or PNG file.";
                                    
                                    }
                                    
                                    
                                    
                                    if(empty($errors_thumb)==true){
                                   
                                        $data['tmp_name']  = $_FILES['challenge_post']['tmp_name'][$i];
                                        $data['file_name'] = $thumb_name;
                

                                        $val = $this->aws3->sendFile('daypoll/assets/images/challenge/type2',$data); 
                                
                                    //move_uploaded_file($file_tmp_thumb,"assets/images/challenge/type2/".$thumb_name);
                                    $challenge_image = $thumb_name;
                                    $posts_width  = $this->input->post('posts_width');
                                    $posts_height = $this->input->post('posts_height');
                                    
                                    $insert_challenge_post =   
                                    array( 'challenge_posts_challenge_id'       =>  $result_id,
                                    'challenge_posts_title'                     =>  $challenge_posts_title[$i],
                                    'challenge_posts_sub_post'                  =>  $challenge_image,
                                    'challenge_posts_user_id'                   =>  $userid,
                                    'posts_width'                               =>  $posts_width[$i],
                                    'posts_height'                              =>  $posts_height[$i]
                                    );
                                    $insert_challenge_posts = $this->ChallengeApiModel->uploadChallengeposts($insert_challenge_post);
                    
                                
                                    
                                    }else{
                                      
                                        $challenge_image = "null";
                                    }
                                }
                            }
                            
                            break;

         /*******************************   IMAGE ONLY ENDS HERE     ***********************************/

                               
          /*******************************   VIDEO  ONLY     ***********************************/

                           case 2:
                          
                            $challenge_posts_title = $this->input->post('challenge_post_title');
                            for($i=0;$i<sizeof($challenge_posts_title);$i++){

                               
                            // video thumbnail
                            $challenge_video_thumb = "";
                            if(isset($_FILES['challenge_video_thumbnail'])){   
                                $errors_thumb    = array();
                                $file_name_thumb = $_FILES['challenge_video_thumbnail']['name'][$i];
                                $file_size_thumb = $_FILES['challenge_video_thumbnail']['size'][$i];
                                $file_tmp_thumb  = $_FILES['challenge_video_thumbnail']['tmp_name'][$i];
                                $file_type_thumb = $_FILES['challenge_video_thumbnail']['type'][$i];
                                $tmp_thumb       = explode('.', $file_name_thumb);
                                $file_ext_thumb  = end($tmp_thumb);

                        
                                $thumb_name = "challenge_thumb-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                                
                                $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                                
                                if(in_array($file_ext_thumb,$expensions_thumb)=== false){

                                $errors_thumb[]="extension not allowed, please choose a JPEG or PNG file.";
                                
                            }
                                
                                
                                
                                if(empty($errors_thumb)==true){
                               
                                    $data['tmp_name']  = $_FILES['challenge_video_thumbnail']['tmp_name'][$i];
                                    $data['file_name'] = $thumb_name;

                                   $val = $this->aws3->sendFile('daypoll/assets/images/challenge/thumb',$data);


                            
                                //move_uploaded_file($file_tmp_thumb,"assets/images/challenge/thumb/".$thumb_name);
                                $challenge_video_thumb = $thumb_name;
                                
                                }else{
                                  
                                    $challenge_video_thumb = "null";
                                }
                            }

                            
                           
                            // challenge video upload
                                   
                            if(isset($_FILES['challenge_post'])){
                            $errors    = array();
                            $file_name = $_FILES['challenge_post']['name'][$i];
                            $file_size = $_FILES['challenge_post']['size'][$i];
                            $file_tmp  = $_FILES['challenge_post']['tmp_name'][$i];
                            $file_type = $_FILES['challenge_post']['type'][$i];
                            $tmp       = explode('.', $file_name);
                            $file_ext  = end($tmp);

                            $challenge_post_width  = $this->input->post('posts_width');
                            $challenge_post_height = $this->input->post('posts_height');

                            if($challenge_post_width < $challenge_post_height) {

                            $post_name = "challenge-portrait-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;

                            }else{

                            $post_name = "challenge-landscape-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;

                            }

                            
                            $expensions = array("jpeg","jpg","png","mp4","3gp");
                            
                            if(in_array($file_ext,$expensions)=== false){

                            $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                                
                            }
                                
                                if(empty($errors)==true){

                                    $data['tmp_name']  = $_FILES['challenge_post']['tmp_name'][$i];
                                    $data['file_name'] = $post_name;
            
           
                                    $val = $this->aws3->sendFile_video('daypoll/assets/images/challenge/type2',$data); 
                                    //move_uploaded_file($file_tmp,"assets/images/challenge/type2/".$post_name);

                                    $challenge_post = $post_name;
                                    $posts_width  = $this->input->post('posts_width');
                                    $posts_height = $this->input->post('posts_height');
                                    
                                    $insert_challenge_post = 
                                    
                                    array( 'challenge_posts_challenge_id'       =>  $result_id,
                                    'challenge_posts_title'                     =>  $challenge_posts_title[$i],
                                    'challenge_posts_sub_post'                  =>  $challenge_post,
                                    'challenge_posts_video_thumbnail'           =>  $challenge_video_thumb,
                                    'challenge_posts_user_id'                   =>  $userid,
                                    'posts_width'                               =>  $posts_width[$i],
                                    'posts_height'                              =>  $posts_height[$i],
                                    'status'                                    =>  0
                                    );
                                
                                    $insert_challenge_posts = $this->ChallengeApiModel->uploadChallengeposts($insert_challenge_post);
                             

                                }
                                else{
                                    $challenge_post = "null";
                                }
                            }
                               
                            }

                            break;

                    /*******************************   VIDEO ONLY  ENDS HERE    ***********************************/
                            
                        }
                 
                       
                    $this->output->set_output(json_encode(['message' => "successfully saved ", 'status' => "true",'challenge_id' => $insert_challenge_posts]));
                        
              
        }
                else{
                    $this->output->set_output(json_encode(['message' => "failed to upload", 'status' => "false"]));
                }
                break;
            }
            
        }
        else{
            $this->output->set_output(json_encode(['message'=>validation_errors(),'status'=>'false']));
        }
       }

       function check_isBlocked($userid){

        return $this->ChallengeApiModel->check_isBlocked(array('user_id' => $userid , 'users_active' => 0));
         }


  // get challenge list
  function getChallengelist(){
         
            $this->output->set_content_type('application/json');
    
            $zone = getallheaders();
            
            if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";
            
            $userid = !empty($this->input->post('userid')) ? $this->input->post('userid') : 1;
            $page_start  = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;

            if(empty($this->check_isBlocked($userid))){

            //get challenge report array
            $report_array = [];
            $report_posts = $this->ChallengeApiModel->getChallengereportArray($userid);
            
            if($report_posts){
            foreach($report_posts as $row){
                $report_array[] = $row['challenge_report_challenge_id'];
             }
            }
            $dt = new DateTime();
                
            $current_date = $dt->format('d-m-Y h:i A');
           
            $GMT  = new DateTimeZone("GMT");
            $date = new DateTime($current_date, $GMT );
            $date->setTimezone(new DateTimeZone($zone['timezone']));
            $date =   $date->format('Y-m-d h:i A');
            
            $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
            $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
            $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;
            $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;  
            
            $result_challenge_list    = $this->ChallengeApiModel->getChallengelist($country , $state , $district ,$area);
            
            $res = [];
            foreach($result_challenge_list as $in_row){
                if(!in_array($in_row['challenge_id'],$report_array)){
                    $res[]    =   $in_row;
                }
            }
            $j = 0;
            
            if($res != null){
                
                foreach($res as $value => $key){
                    
                   
                    // time validity section
    
                    $now            = new DateTime($zone['timezone']);
                   
                 
    
                    $expdate = $res[$value]['challenge_expired_date']." ".$res[$value]['challenge_expired_time'];
                    
                   
                    $expired_date   = date("Y-m-d H:i:s ", strtotime($expdate));
                    
                    
                   
                    $res[$value]['cdate'] = $date;
                   
                    $current_date = strtotime($date);
                    $exp_date     = strtotime($expdate);
                  
                  
                    $diff   = $exp_date - $current_date;
                    $res[$value]['diff'] = $diff;
                    
                    $res[$value]['challenge_status']    = $diff > 0 ? 1 : 0;
                   
                    $expired_date   = new DateTime($expired_date);    
                    
                     
               
                    
                    $now_new     = $now->format('Y-m-d H:i:s');
                    $expired_new = $expired_date->format('Y-m-d H:i:s');
                    
                    $now_new_date       = new DateTime($now_new);
                    $expired_new_date   = new DateTime($expired_new);
                  
                    
                  
                    $interval       = $now_new_date->diff($expired_new_date); 
                    
                   
                    $isPolled       = $this->ChallengeApiModel->isChallengepolled($res[$value]['challenge_id'] , $userid);
                    $res[$value]['ispolled']    = $isPolled > 0 ? 1 : 0;
                   
                    // print $date;
                    // print "<pre>";
                    // print $expdate;
                    // print "<pre>";
                    // print $interval->format('%d')." Days ".$interval->format('%h')." Hours ".$interval->format('%i')." Minutes " .$interval->format('%s');
                    // exit();
                    
                    if($diff > 0){
                   
                    $res[$value]['validity_day']     = $interval->format('%d');
                    $res[$value]['validity_hour']    = $interval->format('%h');
                    $res[$value]['validity_minute']  = $interval->format('%i');
                    $res[$value]['validity_second']  = $interval->format('%s');
                    $res[$value]['challenge_winner'] = [];

                  
                    }

                    else{

                    $res[$value]['validity_day']     = 0;
                    $res[$value]['validity_hour']    = 0;
                    $res[$value]['validity_minute']  = 0;
                    $res[$value]['validity_second']  = 0;

                    $res[$value]['challenge_winner']        = $this->ChallengeApiModel->getWinner($res[$value]['challenge_id']);
                  
                  
                   
                    if($res[$value]['challenge_winner']){
                        
                        foreach($res[$value]['challenge_winner'] as $inner_key => $inner_value){
                        $res[$value]['challenge_winner'][$inner_key]['users_name'] = $this->ChallengeApiModel->getWinnername($res[$value]['challenge_winner'][$inner_key]['challenge_posts_user_id']);
                        }

                        foreach($res[$value]['challenge_winner'] as $key_1 => $value_1){

                           // $res[$value]['challenge_winner']['']
                           // if($res[$value]['challenge_winner'][$key_1]['users_login_type'] == 0){

                           
                           // $res[$value]['challenge_winner'][$key_1]['users_photo']                      = $this->media_url."assets/images/users/".$res[$value]['challenge_winner'][$key_1]['users_photo'];
                        
                           // }

                            $res[$value]['challenge_winner'][$key_1]['users_photo']                      = $this->media_url."assets/images/users/".$res[$value]['challenge_winner'][$key_1]['users_photo'];
                            
                        if($res[$value]['challenge_type'] == 1){
    
                            $res[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post']        = $this->s3_url."assets/images/challenge/type1/".$res[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post'];
                           
                        }
                        else{
                            $res[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post']       = $this->s3_url."assets/images/challenge/type2/".$res[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post'];
                           
                        }

                        $res[$value]['challenge_winner'][$key_1]['challenge_media_type'] = $res[$value]['challenge_media_type'];
                        
                        $res[$value]['challenge_winner'][$key_1]['challenge_posts_video_thumbnail'] = $this->s3_url."assets/images/challenge/thumb/".$res[$value]['challenge_winner'][$key_1]['challenge_posts_video_thumbnail'];
                        
                    }
                  
                }

              

                   
                    }
                 
    
                    // time validtity ends here
    
    
                    if($res[$value]['challenge_posts_sub_type'] == 1) {
    
                        $explenatory_video                      = $this->ChallengeApiModel->getExplanetoryvideo($res[$value]['challenge_id']);
                        if($explenatory_video){
                        $res[$value]['explanetory_video']       = $this->s3_url."assets/images/challenge/type1/".$explenatory_video[0]['challenge_posts_sub_post']; 
                        $res[$value]['explanetory_video_thumb'] = $this->s3_url."assets/images/challenge/thumb/".$explenatory_video[0]['challenge_posts_video_thumbnail']; 
                        $res[$value]['challenge_posts_exp_media_type'] = $explenatory_video[0]['challenge_posts_exp_media_type']; 
                       
                        }
                    }
    
                    $res[$value]['pollCount']               = $this->ChallengeApiModel->challengePollcount($res[$value]['challenge_id']);
                    $res[$value]['commentCount']            = $this->ChallengeApiModel->challengeCommentcount($res[$value]['challenge_id']);
                    $res[$value]['challenge_posts_post']    = $this->ChallengeApiModel->getChallengeposts($res[$value]['challenge_id']);
                    $res[$value]['comment']                 = $this->ChallengeApiModel->getChallengecomments($res[$value]['challenge_id']);
                    
                    $res[$value]['totalpollscount']         = $this->ChallengeApiModel->totalPostsCount($res[$value]['challenge_id']);
                   
                    
                    foreach($res[$value]['comment'] as $pic => $key_pic){
                        $res[$value]['comment'][$pic]['users_photo'] = $this->media_url."assets/images/users/".$res[$value]['comment'][$pic]['users_photo'];
                    }
                   
                    if($res[$value]['users_login_type'] == 0 ){
                    $res[$value]['users_photo']             = $this->media_url."assets/images/users/".$res[$value]['users_photo'];
                       
                    }else{
                        if($res[$value]['users_photo']){
                            $res[$value]['users_photo']=$this->media_url."assets/images/users/".$res[$value]['users_photo'];
                        }
                    }
                    $polls_count=[];
                    foreach($res[$value]['challenge_posts_post'] as $values => $keys){

                        $res[$value]['challenge_posts_post'][$values]['parent_position']                    = $j;
                        $res[$value]['challenge_posts_post'][$values]['users_photo']                        = $this->media_url."assets/images/users/".$res[$value]['challenge_posts_post'][$values]['users_photo']; 
                        $res[$value]['challenge_posts_post'][$values]['pollCount']                          = $this->ChallengeApiModel->challengepostPollcount($res[$value]['challenge_id'],$res[$value]['challenge_posts_post'][$values]['challenge_posts_id']);
                        $polls_count[]																		= $this->ChallengeApiModel->challengepostPollcount($res[$value]['challenge_id'],$res[$value]['challenge_posts_post'][$values]['challenge_posts_id']);
                        $res[$value]['challenge_posts_post'][$values]['commentCount']                       = $this->ChallengeApiModel->challengepostCommentcount($res[$value]['challenge_posts_post'][$values]['challenge_posts_id']);
                        if($res[$value]['totalpollscount'] > 0) {
                       
                        $res[$value]['challenge_posts_post'][$values]['pollPercent']                        = intval(($res[$value]['challenge_posts_post'][$values]['pollCount'] * 100 ) / $res[$value]['totalpollscount']);
                       
                        }
                        else{
                            $res[$value]['challenge_posts_post'][$values]['pollPercent']                    = 0;
                        }
                        $res[$value]['challenge_posts_post'][$values]['challenge_media_type']               = $res[$value]['challenge_media_type'];
                        
                        $isPolledpost       = $this->ChallengeApiModel->isChallengepostpolled($res[$value]['challenge_posts_post'][$values]['challenge_posts_id'] , $userid);
                       
                        $res[$value]['challenge_posts_post'][$values]['ispolled']                           = $isPolledpost > 0 ? 1 : 0;
    
                        if($res[$value]['challenge_type'] == 1){
    
                            $res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post']       = $this->s3_url."assets/images/challenge/type1/".$res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                           
                        }
                        else{
                            $res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post']       = $this->s3_url."assets/images/challenge/type2/".$res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                           
                        }
                            $res[$value]['challenge_posts_post'][$values]['challenge_posts_video_thumbnail'] = $this->s3_url."assets/images/challenge/thumb/".$res[$value]['challenge_posts_post'][$values]['challenge_posts_video_thumbnail'];
                        
                        //$res[$value]['challenge_posts_post'][$values]['users_photo']                = $this->media_url()."assets/images/users/".$res[$value]['challenge_posts_post'][$values]['users_photo'];
                
                        $res[$value]['challenge_posts_post'][$values]['downloadlink']                    = $res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'].'?postid='.$res[$value]['challenge_posts_post'][$values]['challenge_posts_id'].'&&type=3';
                        $res[$value]['challenge_posts_post'][$values]['sharecode']                       = $this->video_base_url.'post-view?postid='.$res[$value]['challenge_posts_post'][$values]['challenge_posts_challenge_id'].'&&type=3';
                    }
                     // $array = $polls_count;
                     // $vals = array_count_values($array);
                     // ksort($vals);
                     // if($vals[count($vals)-1] > 1){
                     // 	echo  "drow";
                     // }else{
                     // 	echo  "winner";
                     // }
                    
                    
                    
                    $j++;   
                }


                $post_count =  count($res);
       
                $final_last_array       = array_slice($res,$page_start,10);

                $final_last_array_next  = array_slice($res,$page_start+10,10);
                
                
                $this->output->set_output(json_encode(['reported_challenge' => $report_posts,'value' => $final_last_array ,'total_posts'=>$post_count,'is_completed' =>count($final_last_array_next) > 0 ? 0 :1,'status'=> true])); 
            }
           
    
            else{
                
            $this->output->set_output(json_encode(['status'=> false , 'value' => 'no value found'])); 
        }
    }
       else{
        $this->output->set_output(json_encode(['status'=> false , 'value' => 'you are blocked by admin'])); 
       }
    
        }





  // get challenge list
  function returngetChallengelist($logged_userid,$userid,$page_start,$country,$state,$district){
         
    $this->output->set_content_type('application/json');

    $zone = getallheaders();
    
    if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";
    
   

    //get challenge report array
    $report_array = [];
    $report_posts = $this->ChallengeApiModel->getChallengereportArray($userid);
    
   

    if($report_posts){
    foreach($report_posts as $row){
        $report_array[] = $row['challenge_report_challenge_id'];
    }
    }
    $dt = new DateTime();
        
    $current_date = $dt->format('d-m-Y h:i A');
   
    $GMT  = new DateTimeZone("GMT");
    $date = new DateTime($current_date, $GMT );
    $date->setTimezone(new DateTimeZone($zone['timezone']));
    $date =   $date->format('Y-m-d h:i A');
    
    
    


    $result_challenge_list    = $this->ChallengeApiModel->getChallengelistprofileposts($logged_userid,$country , $state , $district);
    
    $res = [];
    foreach($result_challenge_list as $in_row){
        if(!in_array($in_row['challenge_id'],$report_array)){
            $res[]    =   $in_row;
        }
    }
    $j = 0;

    if($res != null){
        
        foreach($res as $value => $key){
            
           
            // time validity section

            $now            = new DateTime($zone['timezone']);
           
         

            $expdate = $res[$value]['challenge_expired_date']." ".$res[$value]['challenge_expired_time'];
            
           
            $expired_date   = date("Y-m-d H:i:s ", strtotime($expdate));
            
            
           
            $res[$value]['cdate'] = $date;
           
            $current_date = strtotime($date);
            $exp_date     = strtotime($expdate);
          
          
            $diff   = $exp_date - $current_date;
            $res[$value]['diff'] = $diff;
            
            $res[$value]['challenge_status']    = $diff > 0 ? 1 : 0;
           
            $expired_date   = new DateTime($expired_date);    
            
             
       
            
            $now_new     = $now->format('Y-m-d H:i:s');
            $expired_new = $expired_date->format('Y-m-d H:i:s');
            
            $now_new_date       = new DateTime($now_new);
            $expired_new_date   = new DateTime($expired_new);
          
            
          
            $interval       = $now_new_date->diff($expired_new_date); 
            
           
            $isPolled       = $this->ChallengeApiModel->isChallengepolled($res[$value]['challenge_id'] , $userid);
            $res[$value]['ispolled']    = $isPolled > 0 ? 1 : 0;
           
            // print $date;
            // print "<pre>";
            // print $expdate;
            // print "<pre>";
            // print $interval->format('%d')." Days ".$interval->format('%h')." Hours ".$interval->format('%i')." Minutes " .$interval->format('%s');
            // exit();
            
            if($diff > 0){
           
            $res[$value]['validity_day']     = $interval->format('%d');
            $res[$value]['validity_hour']    = $interval->format('%h');
            $res[$value]['validity_minute']  = $interval->format('%i');
            $res[$value]['validity_second']  = $interval->format('%s');
            $res[$value]['challenge_winner'] = [];

          
            }

            else{

            $res[$value]['validity_day']     = 0;
            $res[$value]['validity_hour']    = 0;
            $res[$value]['validity_minute']  = 0;
            $res[$value]['validity_second']  = 0;

            $res[$value]['challenge_winner']        = $this->ChallengeApiModel->getWinner($res[$value]['challenge_id']);
          
          
            
            if($res[$value]['challenge_winner']){
           
                foreach($res[$value]['challenge_winner'] as $key_1 => $value_1){

                   // $res[$value]['challenge_winner']['']
                   if($res[$value]['challenge_winner'][$key_1]['users_login_type'] == 0){

                   
                   $res[$value]['challenge_winner'][$key_1]['users_photo']                      = $this->media_url."assets/images/users/".$res[$value]['challenge_winner'][$key_1]['users_photo'];
                
                   }
                if($res[$value]['challenge_type'] == 1){

                    $res[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post']        = $this->s3_url."assets/images/challenge/type1/".$res[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post'];
                   
                }
                else{
                    $res[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post']       = $this->s3_url."assets/images/challenge/type2/".$res[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post'];
                   
                }

                $res[$value]['challenge_winner'][$key_1]['challenge_media_type'] = $res[$value]['challenge_media_type'];
                
                $res[$value]['challenge_winner'][$key_1]['challenge_posts_video_thumbnail'] = $this->s3_url."assets/images/challenge/thumb/".$res[$value]['challenge_winner'][$key_1]['challenge_posts_video_thumbnail'];
                
            }
        }

           
            }
         

            // time validtity ends here


            if($res[$value]['challenge_posts_sub_type'] == 1) {

                $explenatory_video                      = $this->ChallengeApiModel->getExplanetoryvideo($res[$value]['challenge_id']);
                if($explenatory_video){
                $res[$value]['explanetory_video']       = $this->s3_url."assets/images/challenge/type1/".$explenatory_video[0]['challenge_posts_sub_post']; 
                $res[$value]['explanetory_video_thumb'] = $this->s3_url."assets/images/challenge/thumb/".$explenatory_video[0]['challenge_posts_video_thumbnail']; 
                $res[$value]['challenge_posts_exp_media_type'] = $explenatory_video[0]['challenge_posts_exp_media_type']; 
               
                }
            }

            $res[$value]['pollCount']               = $this->ChallengeApiModel->challengePollcount($res[$value]['challenge_id']);
            $res[$value]['commentCount']            = $this->ChallengeApiModel->challengeCommentcount($res[$value]['challenge_id']);
            $res[$value]['challenge_posts_post']    = $this->ChallengeApiModel->getChallengeposts($res[$value]['challenge_id']);
            $res[$value]['comment']                 = $this->ChallengeApiModel->getChallengecomments($res[$value]['challenge_id']);
            
            $res[$value]['totalpollscount']         = $this->ChallengeApiModel->totalPostsCount($res[$value]['challenge_id']);
           
            
            foreach($res[$value]['comment'] as $pic => $key_pic){
                $res[$value]['comment'][$pic]['users_photo'] = $this->media_url."assets/images/users/".$res[$value]['comment'][$pic]['users_photo'];
            }
           
            if($res[$value]['users_login_type'] == 0 ){
            $res[$value]['users_photo']             = $this->media_url."assets/images/users/".$res[$value]['users_photo'];
               
            }

            foreach($res[$value]['challenge_posts_post'] as $values => $keys){

                $res[$value]['challenge_posts_post'][$values]['parent_position']                    = $j;
                $res[$value]['challenge_posts_post'][$values]['users_photo']                        = $this->media_url."assets/images/users/".$res[$value]['challenge_posts_post'][$values]['users_photo']; 
                $res[$value]['challenge_posts_post'][$values]['pollCount']                          = $this->ChallengeApiModel->challengepostPollcount($res[$value]['challenge_id'],$res[$value]['challenge_posts_post'][$values]['challenge_posts_id']);
                $res[$value]['challenge_posts_post'][$values]['commentCount']                       = $this->ChallengeApiModel->challengepostCommentcount($res[$value]['challenge_posts_post'][$values]['challenge_posts_id']);
                if($res[$value]['totalpollscount'] > 0) {
               
                $res[$value]['challenge_posts_post'][$values]['pollPercent']                        = intval(($res[$value]['challenge_posts_post'][$values]['pollCount'] * 100 ) / $res[$value]['totalpollscount']);
               
                }
                else{
                    $res[$value]['challenge_posts_post'][$values]['pollPercent']                    = 0;
                }
                $res[$value]['challenge_posts_post'][$values]['challenge_media_type']               = $res[$value]['challenge_media_type'];
                
                $isPolledpost       = $this->ChallengeApiModel->isChallengepostpolled($res[$value]['challenge_posts_post'][$values]['challenge_posts_id'] , $userid);
               
                $res[$value]['challenge_posts_post'][$values]['ispolled']                           = $isPolledpost > 0 ? 1 : 0;

                if($res[$value]['challenge_type'] == 1){

                    $res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post']       = $this->s3_url."assets/images/challenge/type1/".$res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                   
                }
                else{
                    $res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post']       = $this->s3_url."assets/images/challenge/type2/".$res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                   
                }
                    $res[$value]['challenge_posts_post'][$values]['challenge_posts_video_thumbnail']    = $this->s3_url."assets/images/challenge/thumb/".$res[$value]['challenge_posts_post'][$values]['challenge_posts_video_thumbnail'];
                
                //$res[$value]['challenge_posts_post'][$values]['users_photo']                = $this->media_url()."assets/images/users/".$res[$value]['challenge_posts_post'][$values]['users_photo'];
        
                
                
                
                
                $res[$value]['challenge_posts_post'][$values]['downloadlink']                    = $res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'].'?postid='.$res[$value]['challenge_posts_post'][$values]['challenge_posts_id'].'&&type=3';
                $res[$value]['challenge_posts_post'][$values]['sharecode']                       = $this->video_base_url.'post-view?postid='.$res[$value]['challenge_posts_post'][$values]['challenge_posts_id'].'&&type=3';
            }

            $j++;   
        }


        $post_count =  count($res);

        $final_last_array       = array_slice($res,$page_start,10);
      
        $final_last_array_next  = array_slice($res,$page_start+10,10);
        

        foreach($final_last_array as $k => $v){
            $final_last_array[$k]['is_completed']   =  count($final_last_array_next);
        }

        return $final_last_array;
         
    }
   

    else{
        $nullarray = [];
        return $nullarray; 
}


}


    // challenge comment
    function commentChallenge(){

        $this->output->set_content_type('application/json');
        
        $userid      = $this->input->post('userid');
        $cid         = $this->input->post('challengeid');
        $comment     = $this->input->post('comment');
        $cuserid     = $this->input->post('challenge_user_id');

        if(!empty($userid && $cid && $comment)){

        $res = $this->ChallengeApiModel->commentChallenge($userid , $cid , $comment , $cuserid);
       
        if($res > 0){

            $getcomment = $this->ChallengeApiModel->getLastcomment($res);
           
            foreach($getcomment as $val => $ke){
                $getcomment[$val]['users_photo'] = $this->media_url."assets/images/users/".$getcomment[$val]['users_photo'];
            }
            $this->output->set_output(json_encode(['message' => 'successfully commented','comment' => $getcomment ,'status'=> true])); 
        }
        else{

            $this->output->set_output(json_encode(['message' => "operation failed" ,'status'=> false])); 
        }
    }
    else{
        $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> false])); 
    }
        
    }



    // reply to comments
    function replyChallengecomments(){

        $this->output->set_content_type('application/json');

        $challangeid        = $this->input->post('challengeid');
        $user_id            = $this->input->post('userid');
        $comment_id         = $this->input->post('comment_id');
        $comment_user       = !empty($this->input->post('comment_user_id')) ? $this->input->post('comment_user_id') : 1;
        $reply              = $this->input->post('reply');
        
       
        $insert      = array('challenge_comments_reply_challenge_id'=>$challangeid,
                        'challenge_comments_reply_comment_id'=>$comment_id,
                        'challenge_comments_reply'=>$reply,
                        'challenge_comment_reply_comment_user_id' => $comment_user,
                        'challenge_comments_reply_user_id' => $user_id);

                       

        $res         = $this->ChallengeApiModel->replyChallengecomments($insert,$comment_user);

        if($res > 0){

            $this->output->set_output(json_encode(array('message' => "succesfully inserted",'status'=>true)));
        }
        else{

            $this->output->set_output(json_encode(array('posts' => "no value found",'status'=>false)));
        }
    }







    // comment challenge posts
    function commentChallengePosts(){

        $this->output->set_content_type('application/json');
        
        $userid      = $this->input->post('userid');
        $cid         = $this->input->post('challengeid');
        $comment     = $this->input->post('comment');
        $cuserid     = $this->input->post('challenge_user_id');
        $cpid        = $this->input->post('postid');

        
        if(!empty($userid && $cid && $comment && $cpid)){

        $res = $this->ChallengeApiModel->commentChallengePosts($userid , $cid , $comment , $cuserid , $cpid);
        if($res > 0){

            $getcomment = $this->ChallengeApiModel->getpostLastcomment($res);
            foreach($getcomment as $val => $ke){
                
                $getcomment[$val]['users_photo']                    = $this->media_url."assets/images/users/".$getcomment[$val]['users_photo'];
                $getcomment[$val]['challenge_comments_id']          = $getcomment[$val]['challenge_posts_comments_id']; 
                $getcomment[$val]['challenge_comments_user_id']     = $getcomment[$val]['challenge_posts_comments_user_id'];
                $getcomment[$val]['challenge_comments_comment']     = $getcomment[$val]['challenge_posts_comments_comment'];
                $getcomment[$val]['challenge_comments_challenge_id']= $getcomment[$val]['challenge_posts_comments_challenge_id'];
                $getcomment[$val]['challenge_comments_date']        = $getcomment[$val]['challenge_posts_comments_challenge_date'];
            
            }

            
            //push notify on comment
            $notification         = array('notifications_user_poll_id' => $cuserid, 'notifications_challenge_id' => $cid,'notifications_user_id' => $cuserid,'notifications_type' => 24);  
            $insert_notifications = $this->db->insert('notifications',$notification);

            $android_devices = $this->UsersApiModel->getUserDevices($cuserid,1);
            //$ios_devices = $this->UsersApiModel->getUserDevices($cuserid,2);


            $user_details = $this->UsersApiModel->getUsername($userid);

            $message = array(
                    'type' => 'challenge post comment',
                    'title' => 'Challenge Post Comment',
                    'message' => $user_details.' commented on your post'
                );

            if (!empty($android_devices)) sendAndroidPush($android_devices, $message);

            //if (!empty($ios_devices)) sendIosPush($ios_devices, $message);



            $this->output->set_output(json_encode(['message' => 'successfully commented','comment' => $getcomment ,'status'=> true])); 
        }
        else{

            $this->output->set_output(json_encode(['message' => "operation failed" ,'status'=> false])); 
        }
    }
    else{
        $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> false])); 
    }

    }



     // reply to comments
     function replyChallengecommentsPosts(){

        $this->output->set_content_type('application/json');

        $challangeid        = $this->input->post('challengeid');
        $user_id            = $this->input->post('userid');
        $comment_id         = $this->input->post('comment_id');
        $comment_user       = !empty($this->input->post('comment_user_id')) ? $this->input->post('comment_user_id') : 1;
        $reply              = $this->input->post('reply');
        
       
        $insert      = array('challenge_posts_comments_reply_challenge_id'=>$challangeid,
                        'challenge_posts_comments_reply_comment_id'=>$comment_id,
                        'challenge_posts_comments_reply'=>$reply,
                        'challenge_posts_comment_reply_comment_user_id' => $comment_user,
                        'challenge_posts_comments_reply_user_id' => $user_id);

        $res         = $this->ChallengeApiModel->replyChallengecommentsPosts($insert,$comment_user);

        if($res > 0){

            $this->output->set_output(json_encode(array('message' => "succesfully inserted",'status'=>true)));
        }
        else{

            $this->output->set_output(json_encode(array('posts' => "no value found",'status'=>false)));
        }
    }





    // poll type 2 challenge
    function pollChallenge(){

        $this->output->set_content_type('application/json');
        
        $userid      = $this->input->post('userid');
        $cid         = $this->input->post('challengeid');
        $cuserid     = $this->input->post('challenge_user_id');
        

        if(!empty($userid && $cid)){

        $res = $this->ChallengeApiModel->pollChallenge($userid , $cid ,$cuserid);

        switch ($res){

            case 2:
            $this->output->set_output(json_encode(['message' => 'successfully polled' ,'status'=> true])); 
            break;

            case 3:
            $this->output->set_output(json_encode(['message' => 'successfully unpolled' ,'status'=> true])); 
            break;

            case 0:
            $this->output->set_output(json_encode(['message' => 'operation failed' ,'status'=> false])); 
            break;
        }
       
    }
    else{
        $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> true])); 
    }
    } 

    // poll type 2 challenge
    function pollChallengeposts(){

        $this->output->set_content_type('application/json');
        
        $userid     = $this->input->post('userid');
        $cid        = $this->input->post('challengeid');
        $cpid       = $this->input->post('challengepostid');
        $cuserid    = $this->input->post('challenge_user_id');

        $type    = $this->input->post('type');    // 0 =>  normal poll , 1  => change subpost poll
        

        if(!empty($userid && $cid && $cpid)){
        
        if($type == 1){
            $res = $this->ChallengeApiModel->updatepollpostChallenge($userid , $cid , $cpid );
        }   
        else{ 
        $res = $this->ChallengeApiModel->pollpostChallenge($userid , $cid , $cpid ,$cuserid);
        }


        //push on 10 polls
        $unNotifiedPolls = $this->ChallengeApiModel->getUnNotifiedPolls($cpid);
        if($unNotifiedPolls->count == 5)
        {
            $users = $this->ChallengeApiModel->getPollUsers($cpid);

            $android_devices = $this->UsersApiModel->getUserDevices($users->challenge_posts_user_id,1);
            //$ios_devices = $this->UsersApiModel->getUserDevices($challenge->challenge_user_id,2);

            $message = array(
                    'type' => 'challenge post polls',
                    'title' => 'Challenge Post Polls',
                    'message' => $users->poll_user.' and other 4 people polled on your post '
                );

            if (!empty($android_devices)) sendAndroidPush($android_devices, $message);

            $notification         = array('notifications_user_poll_id' => $users->challenge_posts_user_id, 'notifications_challenge_id' => $users->challenge_posts_challenge_id,'notifications_user_id' => $users->challenge_posts_user_id,'notifications_type' => 73,'notification_text'=>$message['message']);  
            $insert_notifications = $this->db->insert('notifications',$notification);

            //if (!empty($ios_devices)) sendIosPush($ios_devices, $message);


            $this->ChallengeApiModel->updateChallengePostPolls($cpid,array('is_notified' => 1));
        }


        switch ($res){
            
            case 2:
            $this->output->set_output(json_encode(['message' => 'successfully polled' ,'status'=> true])); 
            break;

            case 3:
            $this->output->set_output(json_encode(['message' => 'successfully unpolled' ,'status'=> true])); 
            break;

            case 0:
            $this->output->set_output(json_encode(['message' => 'operation failed' ,'status'=> false])); 
            break;

            case 4:
            $this->output->set_output(json_encode(['message' => 'successfully polled' ,'status'=> true])); 
            break;

            case 5:
            $this->output->set_output(json_encode(['message' => 'failed to update' ,'status'=> false])); 
            break;
        }
       
    }
    else{
        $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> false])); 
    }
    } 


     // poll type 2 challenge
     function unpollChallengeposts(){

        $this->output->set_content_type('application/json');
        
        $userid  = $this->input->post('userid');
        $cid     = $this->input->post('challengeid');
        $cpid    = $this->input->post('challengepostid');
        

        if(!empty($userid && $cid && $cpid)){

        $res = $this->ChallengeApiModel->unpollChallengeposts($userid , $cid , $cpid);

        switch ($res){
            
            case 2:
            $this->output->set_output(json_encode(['message' => 'successfully polled' ,'status'=> true])); 
            break;

            case 3:
            $this->output->set_output(json_encode(['message' => 'successfully unpolled' ,'status'=> true])); 
            break;

            case 0:
            $this->output->set_output(json_encode(['message' => 'operation failed' ,'status'=> false])); 
            break;
        }
       
    }
    else{
        $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> false])); 
    }
    } 

    // get comment list
    function getCommentlist(){

        $this->output->set_content_type('application/json');
        
        $challengeid  = $this->input->post('challengeid');
        $type         = $this->input->post('type');  


        if(!empty($challengeid)){

            $res = $this->ChallengeApiModel->getCommentlist($challengeid , $type);

            $comment_enabled =  $this->ChallengeApiModel->isChallengeCommentEnabled($challengeid,$type);
            if ($type == 1) {
                if ($comment_enabled[0]->challenge_posts_comments_enable == 1) {

                    $enabled = 1;
                }else{
                     $enabled = 0;
                }
            }else{
                if ($comment_enabled[0]->challenge_comments_enable == 1) {

                    $enabled = 1;
                }else{
                     $enabled = 0;
                }

            }
            
            
           
            if($res != null){
                
                if($type == 1){
                foreach($res as $val => $key){

                    $res[$val]['users_photo']                       = $this->media_url."assets/images/users/".$res[$val]['users_photo'];
                    $res[$val]['challenge_comments_id']             = $res[$val]['challenge_posts_comments_id'];
                    $res[$val]['challenge_comments_user_id']        = $res[$val]['challenge_posts_comments_user_id'];
                    $res[$val]['challenge_comments_comment']        = $res[$val]['challenge_posts_comments_comment'];
                    $res[$val]['challenge_comments_challenge_id']   = $res[$val]['challenge_posts_comments_challenge_id'];
                    $res[$val]['challenge_comments_date']           = $res[$val]['challenge_posts_comments_challenge_date'];
                  
                }
            }
            else{
                foreach($res as $val => $key){
                $res[$val]['users_photo']                       = $this->media_url."assets/images/users/".$res[$val]['users_photo'];
               
            }
        }
            if ($enabled ==1) {
                $this->output->set_output(json_encode(['message' => 'data found' ,'enabled'=> true,'value' => $res,'status'=> true])); 
            }else{
                $this->output->set_output(json_encode(['message' => 'data found' ,'enabled'=> false,'value' => $res,'status'=> true])); 
            }
                
            }
            else{
                $this->output->set_output(json_encode(['message' => 'no data found' , 'enabled'=> true, 'value' => $res,'status'=> true])); 
           
            }
        }
        else{

            $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> false])); 
        }
    }



    // get single challenge by id 
    function getChallengebyid(){

        $this->output->set_content_type('application/json');

        $challengeid = $this->input->post('challengeid');
        $userid      = !empty ($this->input->post('userid')) ? $this->input->post('userid') : 1;


        $zone = getallheaders();
    
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";


        $dt = new DateTime();
            
        $current_date = $dt->format('d-m-Y h:i A');
    
        $GMT  = new DateTimeZone("GMT");
        $date = new DateTime($current_date, $GMT );
        $date->setTimezone(new DateTimeZone($zone['timezone']));
        $date =   $date->format('Y-m-d h:i A');


       
        if(!empty($challengeid)){

            $res = $this->ChallengeApiModel->getChallengebyid($challengeid);

            if($res != null){
                foreach($res as $value => $key){

                $now            = new DateTime($zone['timezone']);

                $expdate = $res[$value]['challenge_expired_date']." ".$res[$value]['challenge_expired_time'];
                
                              
                $expired_date   = date("Y-m-d h:i A ", strtotime($expdate));
                
                $res[$value]['cdate'] = $date;
                //$date = date('Y-m-d h:i A');
                
                $current_date = strtotime($date);
                $exp_date     = strtotime($expdate);
                $diff   = $exp_date - $current_date;
                $res[$value]['challenge_status']    = $diff > 0 ? 1 : 0;
               
                $expired_date   = new DateTime($expired_date);    
                
                $interval       = $now->diff($expired_date); 
                //$interval->format('%d')." Days ".$interval->format('%h')." Hours ".$interval->format('%i')." Minutes " .$interval->format('%s');
                

                $now_new     = $now->format('Y-m-d H:i:s');
                $expired_new = $expired_date->format('Y-m-d H:i:s');
                
                $now_new_date       = new DateTime($now_new);
                $expired_new_date   = new DateTime($expired_new);

                $interval       = $now_new_date->diff($expired_new_date); 

                if($diff > 0){
               
                $res[$value]['validity_day']    = $interval->format('%d');
                $res[$value]['validity_hour']   = $interval->format('%h');
                $res[$value]['validity_minute'] = $interval->format('%i');
                $res[$value]['validity_second'] = $interval->format('%s');
                
                }
                else{
                $res[$value]['validity_day']    = 0;
                $res[$value]['validity_hour']   = 0;
                $res[$value]['validity_minute'] = 0;
                $res[$value]['validity_second'] = 0;
                }
             

                // time validtity ends here


                if($res[$value]['challenge_posts_sub_type'] == 1) {

                    $explenatory_video                      = $this->ChallengeApiModel->getExplanetoryvideo($res[$value]['challenge_id']);
                    if($explenatory_video){
                    $res[$value]['explanetory_video']       = $this->s3_url."assets/images/challenge/type1/".$explenatory_video[0]['challenge_posts_sub_post']; 
                    $res[$value]['explanetory_video_thumb'] = $this->s3_url."assets/images/challenge/thumb/".$explenatory_video[0]['challenge_posts_video_thumbnail']; 
                    $res[$value]['challenge_posts_exp_media_type'] = $explenatory_video[0]['challenge_posts_exp_media_type']; 
                        
                    }
                }

                $res[$value]['pollCount']               = $this->ChallengeApiModel->challengePollcount($res[$value]['challenge_id']);
                $res[$value]['commentCount']            = $this->ChallengeApiModel->challengeCommentcount($res[$value]['challenge_id']);
                $res[$value]['challenge_posts_post']    = $this->ChallengeApiModel->getChallengeposts($res[$value]['challenge_id']);
                $res[$value]['comment']                 = $this->ChallengeApiModel->getChallengecomments($res[$value]['challenge_id']);
                
                $res[$value]['totalpollscount']         = $this->ChallengeApiModel->totalPostsCount($res[$value]['challenge_id']);

                $res[$value]['ispolled']                = $this->ChallengeApiModel->isChallengepolled($challengeid , $userid);
                
              
                
                foreach($res[$value]['comment'] as $pic => $key_pic){
                    $res[$value]['comment'][$pic]['users_photo'] = $this->media_url."assets/images/users/".$res[$value]['comment'][$pic]['users_photo'];
                }
               
 
                $res[$value]['users_photo']             = $this->media_url."assets/images/users/".$res[$value]['users_photo'];
                   

                foreach($res[$value]['challenge_posts_post'] as $values => $keys){
                     	$GMT = new DateTimeZone("GMT");
                        $date = new DateTime($res[$value]['challenge_posts_post'][$values]['challenge_posts_created_date'] , $GMT);
                        $date->setTimezone(new DateTimeZone($zone['timezone']));
                        $res[$value]['challenge_posts_post'][$values]['challenge_posts_created_date']   = $date->format('d-m-Y h:i A');
                    
                    $res[$value]['challenge_posts_post'][$values]['users_photo']                        = $this->media_url."assets/images/users/".$res[$value]['challenge_posts_post'][$values]['users_photo']; 
                    $res[$value]['challenge_posts_post'][$values]['pollCount']                          = $this->ChallengeApiModel->challengepostPollcount($res[$value]['challenge_id'],$res[$value]['challenge_posts_post'][$values]['challenge_posts_id']);
                    
                    $res[$value]['challenge_posts_post'][$values]['commentCount']                       = $this->ChallengeApiModel->challengepostCommentcount($res[$value]['challenge_posts_post'][$values]['challenge_posts_id']);
                    if($res[$value]['totalpollscount'] > 0) {
                   
                    $res[$value]['challenge_posts_post'][$values]['pollPercent']                        = intval(($res[$value]['challenge_posts_post'][$values]['pollCount'] * 100 ) / $res[$value]['totalpollscount']);
                    
                    }
                    else{
                        $res[$value]['challenge_posts_post'][$values]['pollPercent'] = 0;
                    }
                    $res[$value]['challenge_posts_post'][$values]['challenge_media_type']               = $res[$value]['challenge_media_type'];
                    $res[$value]['challenge_posts_post'][$values]['parent_position']                    = 0;
                    $res[$value]['challenge_posts_post'][$values]['parent_position']                    = 0;
                    $res[$value]['challenge_posts_post'][$values]['ispolled']                           = $this->ChallengeApiModel->isChallengepostpolled( $res[$value]['challenge_posts_post'][$values]['challenge_posts_id'],$userid);

                    if($res[$value]['challenge_type'] == 1){

                        $res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post']       = $this->s3_url."assets/images/challenge/type1/".$res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                       
                    }
                    else{
                        $res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post']       = $this->s3_url."assets/images/challenge/type2/".$res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                       
                    }
                        $res[$value]['challenge_posts_post'][$values]['challenge_posts_video_thumbnail']    = $this->s3_url."assets/images/challenge/thumb/".$res[$value]['challenge_posts_post'][$values]['challenge_posts_video_thumbnail'];
                    
                        $res[$value]['challenge_posts_post'][$values]['downloadlink']                    = $res[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'].'?postid='.$res[$value]['challenge_posts_post'][$values]['challenge_posts_challenge_id'].'&&type=3';
                        $res[$value]['challenge_posts_post'][$values]['sharecode']                       = $this->video_base_url.'post-view?postid='.$res[$value]['challenge_posts_post'][$values]['challenge_posts_id'].'&challengeid='.$res[$value]['challenge_posts_post'][$values]['challenge_posts_challenge_id'].'&type=3';
                 
                }



                $res[$value]['challenge_winner']        = $this->ChallengeApiModel->getWinner($challengeid);
                  
              
                    
                if($res[$value]['challenge_winner']){
               
                    foreach($res[$value]['challenge_winner'] as $key_1 => $value_1){

                       // $res[$value]['challenge_winner']['']
                       if($res[$value]['challenge_winner'][$key_1]['users_login_type'] == 0){

                       
                       $res[$value]['challenge_winner'][$key_1]['users_photo']             = $this->media_url."assets/images/users/".$res[$value]['challenge_winner'][$key_1]['users_photo'];
                    
                       }
                    if($res[$value]['challenge_type'] == 1){

                        $res[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post']       = $this->s3_url."assets/images/challenge/type1/".$res[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post'];
                       
                    }
                    else{
                        $res[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post']       = $this->s3_url."assets/images/challenge/type2/".$res[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post'];
                       
                    }

                    $res[$value]['challenge_winner'][$key_1]['challenge_media_type'] = $res[$value]['challenge_media_type'];
                    
                    $res[$value]['challenge_winner'][$key_1]['challenge_posts_video_thumbnail'] = $this->s3_url."assets/images/challenge/thumb/".$res[$value]['challenge_winner'][$key_1]['challenge_posts_video_thumbnail'];
                    
                }
            }
            


            }


                $this->output->set_output(json_encode(['message' => $res ,'status'=> true])); 

            }
            else{
                $this->output->set_output(json_encode(['message' => 'no data found' ,'status'=> false])); 
            }
        }
        else{
            $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> false])); 
        }
    }
   
    // update challenge posts
    function updateChallengePosts(){

        $this->output->set_content_type('application/json');

        $userid                 = $this->input->post('userid'); 
        $challengeid            = $this->input->post('challengeid');
        $challenge_posts_title  = $this->input->post('challenge_post_title');
        $challenge_media_type   = $this->input->post('challenge_media_type');
        $challenge_type         = ($this->input->post('challenge_type') == 1 ? 4 : 64);

        
        $posts_width  = $this->input->post('posts_width');
        $posts_height = $this->input->post('posts_height');


        if(!empty($userid && $challengeid)){
            
            $user_array     = $this->input->post('userlist');

            if($user_array != null){
        
                for($i = 0 ; $i < sizeof($user_array); $i++){

                    $res        = $this->ChallengeApiModel->inviteMembers($user_array[$i] , $challengeid , $userid , $challenge_type);
                    if($res == 1){
                        $msg    = "successfully saved";
                        $status = 'true';
                    }
                    else{

                    }
                }

            }

            switch($challenge_media_type){
                       
                // IMAGE ONLY  (type 1)
                
                case 1:
               
              
                if(isset($_FILES['challenge_post'])){   
                            
                    $errors_thumb    = array();
                    $file_name_thumb = $_FILES['challenge_post']['name'];
                    $file_size_thumb = $_FILES['challenge_post']['size'];
                    $file_tmp_thumb  = $_FILES['challenge_post']['tmp_name'];
                    $file_type_thumb = $_FILES['challenge_post']['type'];
                    $tmp_thumb       = explode('.', $file_name_thumb);
                    $file_ext_thumb  = end($tmp_thumb);

            
                    $thumb_name = "challenge_post-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                    
                    $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                    
                    if(in_array($file_ext_thumb,$expensions_thumb)=== false){

                    $errors_thumb[] = "extension not allowed, please choose a JPEG or PNG file.";
                    
                }
                    
                    
                    
                    if(empty($errors_thumb)==true){
                   
                        $data['tmp_name']  = $_FILES['challenge_post']['tmp_name'];
                        $data['file_name'] = $thumb_name;
        
       
                        $val = $this->aws3->sendFile('daypoll/assets/images/challenge/type1',$data);   
                          

                    //move_uploaded_file($file_tmp_thumb,"assets/images/challenge/type1/".$thumb_name);
                    $challenge_image = $thumb_name;

                    $insert_challenge_post =   
                                array( 'challenge_posts_challenge_id'       =>  $challengeid,
                                'challenge_posts_title'                     =>  $challenge_posts_title,
                                'challenge_posts_sub_post'                  =>  $challenge_image,
                                'challenge_posts_user_id'                   =>  $userid,
                                'posts_width'                               =>  $posts_width,
                                'posts_height'                              =>  $posts_height
                            
                                );

                    $insert_challenge_posts = $this->ChallengeApiModel->uploadChallengeposts($insert_challenge_post);
    
                    if($insert_challenge_posts > 0){
                        $last_post  = $this->ChallengeApiModel->getLastinsertedpost($insert_challenge_posts);
                          
                            
                                foreach($last_post as $val => $key){
                                    $last_post[$val]['challenge_posts_video_thumbnail'] = $this->s3_url."assets/images/challenge/thumb/".$last_post[$val]['challenge_posts_video_thumbnail'];
                                    $last_post[$val]['challenge_posts_sub_post']        = $this->s3_url."assets/images/challenge/type1/".$last_post[$val]['challenge_posts_sub_post'];
                                    $last_post[$val]['users_photo']                     = $this->media_url."assets/images/users/".$last_post[$val]['users_photo'];
                                    $last_post[$val]['pollCount']                       = $this->ChallengeApiModel->challengepostPollcount($challengeid , $insert_challenge_posts);
                                    $last_post[$val]['pollPercent']                     = 0; 
                                    $last_post[$val]['challenge_media_type']            = $challenge_media_type;
                                    $last_post[$val]['ispolled']                        = 0;
                                    $last_post[$val]['parent_position']                 = 0;
                                    $last_post[$val]['commentCount']                    = $this->ChallengeApiModel->challengepostCommentcount($insert_challenge_posts);
                            
                                    $last_post[$val]['downloadlink']                    = $last_post[$val]['challenge_posts_sub_post'].'?postid='.$challengeid.'&&type=3';
                                    $last_post[$val]['sharecode']                       = $this->video_base_url.'post-view?postid='.$challengeid.'&&type=3';

                                }
                            
                            
                            
                        $msg    = "successfully saved";
                        $status = 'true';

                        //Push notification
                        $challenge_details = $this->ChallengeApiModel->getChallengebyid($challengeid);

                        $notification         = array('notifications_user_poll_id' => $challenge_details[0]['challenge_user_id'], 'notifications_challenge_id' => $challengeid,'notifications_user_id' => $userid,'notifications_type' => 71);  
                        $insert_notifications = $this->db->insert('notifications',$notification);

                        $android_devices = $this->UsersApiModel->getUserDevices($challenge_details[0]['challenge_user_id'],1);
                        $ios_devices = $this->UsersApiModel->getUserDevices($challenge_details[0]['challenge_user_id'],2);


                        $user_details = $this->UsersApiModel->getUsername($userid);

                        $message = array(
                                'type' => 'challenge',
                                'title' => 'Challenge',
                                'message' =>$user_details.' participated in your challenge'
                            );

                        if (!empty($android_devices)) sendAndroidPush($android_devices, $message);

                        //if (!empty($ios_devices)) sendIosPush($ios_devices, $message);
                    }
                    else{

                        $msg    = "failed to save";
                        $status = 'false';
                    }
                    
                    }else{
                      
                        $challenge_image = "null";
                    }
                }

                if(empty($last_post)){
                    
                    $last_post = [];
                }
                $this->output->set_output(json_encode(['message' => $msg ,'last_post' => $last_post,'status'=> $status])); 
                break;

                case 2:
               
                if(isset($_FILES['challenge_video_thumbnail'])){                                                                                                                                              

                    $errors_thumb    = array();
                    $file_name_thumb = $_FILES['challenge_video_thumbnail']['name'];
                    $file_size_thumb = $_FILES['challenge_video_thumbnail']['size'];
                    $file_tmp_thumb  = $_FILES['challenge_video_thumbnail']['tmp_name'];
                    $file_type_thumb = $_FILES['challenge_video_thumbnail']['type'];
                    $tmp_thumb       = explode('.', $file_name_thumb);
                    $file_ext_thumb  = end($tmp_thumb);

            
                    $thumb_name = "challenge_thumb-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                    
                    $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                    
                    if(in_array($file_ext_thumb,$expensions_thumb)=== false){

                    $errors_thumb[] = "extension not allowed, please choose a JPEG or PNG file.";
                    
                    }
                    
                    if(empty($errors_thumb)==true){
                   
                        $data['tmp_name']  = $_FILES['challenge_video_thumbnail']['tmp_name'];
                        $data['file_name'] = $thumb_name;
        
       
                        $val = $this->aws3->sendFile('daypoll/assets/images/challenge/thumb',$data);   
                  
                    //move_uploaded_file($file_tmp_thumb,"assets/images/challenge/thumb/".$thumb_name);
                    $challenge_video_thumb = $thumb_name;
                    
                    
                    
                    }else{
                      
                        $challenge_video_thumb = "null";
                    }
                }

            // challenge video upload
                                            
            if(isset($_FILES['challenge_post'])){

                $errors    = array();
                $file_name = $_FILES['challenge_post']['name'];
                $file_size = $_FILES['challenge_post']['size'];
                $file_tmp  = $_FILES['challenge_post']['tmp_name'];
                $file_type = $_FILES['challenge_post']['type'];
                $tmp       = explode('.', $file_name);
                $file_ext  = end($tmp);


                if($posts_width < $posts_height) {

                   $post_name = "challenge-portrait-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;

                    }else{

                    $post_name = "challenge-portrait-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;

                    }

                
                
                $expensions = array("jpeg","jpg","png","mp4","3gp");
                
                if(in_array($file_ext,$expensions)=== false){

                $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                    
                }
                    
                    if(empty($errors)==true){

                        $data['tmp_name']  = $_FILES['challenge_post']['tmp_name'];
                        $data['file_name'] = $post_name;
        
       
                        $val = $this->aws3->sendFile('daypoll/assets/images/challenge/type1',$data);   
                  
                        //move_uploaded_file($file_tmp,"assets/images/challenge/type1/".$post_name);
                        $challenge_post = $post_name;

                        $insert_challenge_post = 
                        
                        array( 'challenge_posts_challenge_id'       =>  $challengeid,
                        'challenge_posts_title'                     =>  $challenge_posts_title,
                        'challenge_posts_sub_post'                  =>  $challenge_post,
                        'challenge_posts_video_thumbnail'           =>  $challenge_video_thumb,
                        'challenge_posts_user_id'                   =>  $userid,
                        'posts_width'                               =>  $posts_width,
                        'posts_height'                              =>  $posts_height                      
                        );
                    
                        $insert_challenge_posts = $this->ChallengeApiModel->uploadChallengeposts($insert_challenge_post);

                        if($insert_challenge_posts > 0){
                            
                            $last_post  = $this->ChallengeApiModel->getLastinsertedpost($insert_challenge_posts);
                            foreach($last_post as $val => $key){
                                $last_post[$val]['challenge_posts_video_thumbnail'] = $this->s3_url."assets/images/challenge/thumb/".$last_post[$val]['challenge_posts_video_thumbnail'];
                                $last_post[$val]['challenge_posts_sub_post']        = $this->s3_url."assets/images/challenge/type1/".$last_post[$val]['challenge_posts_sub_post'];
                                $last_post[$val]['users_photo']                     = $this->media_url."assets/images/users/".$last_post[$val]['users_photo'];
                                $last_post[$val]['pollCount']                       = $this->ChallengeApiModel->challengepostPollcount($challengeid , $insert_challenge_posts);
                                $last_post[$val]['pollPercent']                     = 0; 
                                $last_post[$val]['challenge_media_type']            = $challenge_media_type;
                                $last_post[$val]['ispolled']                        = 0;
                                $last_post[$val]['parent_position']                 = 0;
                                $last_post[$val]['commentCount']                    = $this->ChallengeApiModel->challengepostCommentcount($insert_challenge_posts);
                            
                                $last_post[$val]['downloadlink']                    = $last_post[$val]['challenge_posts_sub_post'].'?postid='.$challengeid.'&&type=3';
                                $last_post[$val]['sharecode']                       = $this->video_base_url.'post-view?postid='.$challengeid.'&&type=3';

                            }
                            $msg        = "successfully saved";
                            $status     = 'true';

                            //Push notification
                            $challenge_details = $this->ChallengeApiModel->getChallengebyid($challengeid);

                            $notification         = array('notifications_user_poll_id' => $challenge_details[0]['challenge_user_id'], 'notifications_challenge_id' => $challengeid,'notifications_user_id' => $userid,'notifications_type' => 71);  
                            $insert_notifications = $this->db->insert('notifications',$notification);

                            $android_devices = $this->UsersApiModel->getUserDevices($challenge_details[0]['challenge_user_id'],1);
                            $ios_devices = $this->UsersApiModel->getUserDevices($challenge_details[0]['challenge_user_id'],2);


                            $user_details = $this->UsersApiModel->getUsername($userid);

                            $message = array(
                                    'type' => 'challenge',
                                    'title' => 'Challenge',
                                    'message' =>$user_details.' participated in your challenge '.$challenge_details[0]['challenge_title']
                                );

                            if (!empty($android_devices)) sendAndroidPush($android_devices, $message);

                            //if (!empty($ios_devices)) sendIosPush($ios_devices, $message);
                        }
                        else{
    
                            $msg    = "successfully saved";
                            $status = 'true';

                            //Push notification
                            $challenge_details = $this->ChallengeApiModel->getChallengebyid($challengeid);

                            $notification         = array('notifications_user_poll_id' => $challenge_details[0]['challenge_user_id'], 'notifications_challenge_id' => $challengeid,'notifications_user_id' => $userid,'notifications_type' => 71);  
                            $insert_notifications = $this->db->insert('notifications',$notification);

                            $android_devices = $this->UsersApiModel->getUserDevices($challenge_details[0]['challenge_user_id'],1);
                            $ios_devices = $this->UsersApiModel->getUserDevices($challenge_details[0]['challenge_user_id'],2);


                            $user_details = $this->UsersApiModel->getUsername($userid);

                            $message = array(
                                    'type' => 'challenge',
                                    'title' => 'Challenge',
                                    'message' =>$user_details.' participated in your challenge '.$challenge_details[0]['challenge_title']
                                );

                            if (!empty($android_devices)) sendAndroidPush($android_devices, $message);

                            //if (!empty($ios_devices)) sendIosPush($ios_devices, $message);
                        }

                    }
                    else{
                        $challenge_post = "null";
                    }
                }
                if(empty($last_post)){
                    
                    $last_post = [];
                }
                $this->output->set_output(json_encode(['message' => $msg ,'last_post' => $last_post,'status'=> $status])); 
                break;


            }


        }
        else{
            $this->output->set_output(json_encode(['message' => 'parameter missing' ,'status'=> false])); 
     
        }
     

    }


// get posts api by posts id
function getnotificationchallengebyid(){

     
    $this->output->set_content_type('application/json');

    $zone = getallheaders();
    
    if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";
    
    $dt = new DateTime();
        
    $current_date = $dt->format('d-m-Y h:i A');
   
    $GMT  = new DateTimeZone("GMT");
    $date = new DateTime($current_date, $GMT );
    $date->setTimezone(new DateTimeZone($zone['timezone']));
    $date =   $date->format('Y-m-d h:i A');
    
    $userid      = !empty($this->input->post('userid')) ? $this->input->post('userid') : 1;
    $challangeid = !empty($this->input->post('challengeid')) ? $this->input->post('challengeid') : 1;
    
    
    


    $res    = $this->ChallengeApiModel->getnotificationChallengebyid($challangeid);

    
    
    if($res != null){
        


            foreach($res as $values => $keys){
          
                $res[$values]['users_photo']                        = $this->media_url."assets/images/users/".$res[$values]['users_photo']; 
                $res[$values]['pollCount']                          = $this->ChallengeApiModel->challengepostPollcount(null,$res[$values]['challenge_posts_id']);
                $res[$values]['commentCount']                       = $this->ChallengeApiModel->challengepostCommentcount($res[$values]['challenge_posts_id']);
               
            
                $isPolledpost       = $this->ChallengeApiModel->isChallengepostpolled($res[$values]['challenge_posts_id'] , $userid);
               
                $res[$values]['isPolled']                           = $isPolledpost > 0 ? 1 : 0;

                if($res[$values]['challenge_posts_type'] == 1){

                    $res[$values]['challenge_posts_sub_post']       = $this->s3_url."assets/images/challenge/type1/".$res[$values]['challenge_posts_sub_post'];
                    
                }
                else{

                    $res[$values]['challenge_posts_sub_post']       = $this->s3_url."assets/images/challenge/type2/".$res[$values]['challenge_posts_sub_post'];
                   
                }

                $res[$values]['downloadlink']                       = $res[$values]['challenge_posts_sub_post'].'?postid='.$res[$values]['challenge_posts_id'].'&&type=3';
                $res[$values]['sharecode']                          = $this->video_base_url.'post-view?postid='.$res[$values]['challenge_posts_id'].'&&type=3';
    

                    $res[$values]['challenge_posts_video_thumbnail']    = $this->s3_url."assets/images/challenge/thumb/".$res[$values]['challenge_posts_video_thumbnail'];
                
                //$res[$value]['challenge_posts_post'][$values]['users_photo']                = $this->media_url()."assets/images/users/".$res[$value]['challenge_posts_post'][$values]['users_photo'];
        
            }

          


     
        $this->output->set_output(json_encode(['value' => $res ,'status'=> true])); 
    }
   

    else{
        
    $this->output->set_output(json_encode(['status'=> false , 'value' => 'no value found'])); 
}




}


    //delete challenge
    function deleteChallengepost(){

        $this->output->set_content_type('application/json');

        $challengeid = $this->input->post('postid');
        if(!empty($challengeid)){
       
        $res = $this->ChallengeApiModel->deleteChallengepost($challengeid);
        if($res > 0){

            $this->output->set_output(json_encode(['status'=> true , 'value' => 'successfully deleted'])); 
        }
        else{
            $this->output->set_output(json_encode(['status'=> false , 'value' => 'operation failed'])); 
            }
        }
        else{
            $this->output->set_output(json_encode(['status'=> false , 'value' => 'parameter missing']));
        }
        

    }

    // report posts
    function reportChallengeposts(){

        $this->output->set_content_type('application/json');

        $user_id          = $this->input->post('user_id');
        $challenge_id     = $this->input->post('post_id');
        $msg              = $this->input->post('message');
        
        if(!empty($user_id && $challenge_id)){
        
            
        $res         = $this->ChallengeApiModel->reportChallenge($user_id , $challenge_id , $msg);

           
        switch($res){

            case $res > 0:
            $this->ChallengeApiModel->updatepartyReportcount($challenge_id);
            //$removePosts = $this->PartyApiModel->removePostsbypartyreportcount($challenge_id);
            $this->output->set_output(json_encode(array('message' => "successfully reported",'status'=>true)));
            break;

            case -1:
            $this->output->set_output(json_encode(array('message' => "already reported",'status'=>false)));
            break;
            
            case 0:
            $this->output->set_output(json_encode(array('message' => "operation failed",'status'=>false)));
            break;

        }
    }
        else{
            $this->output->set_output(json_encode(array('message' => "parameter missing",'status'=>false)));
           
        }
     
    }






    //delete challenge
    function deleteChallenge(){

        $this->output->set_content_type('application/json');

        $challengeid = $this->input->post('challengeid');
        $userid = $this->input->post('userid');
        $country = $this->input->post('country');
        $state = $this->input->post('state');
        $district = $this->input->post('district');


        $page_start  = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;

        if(!empty($challengeid)){
       
        $res = $this->ChallengeApiModel->deleteChallenge($challengeid);
        if($res > 0){

            $posts = $this->returngetChallengelist(null,$userid,$page_start,$country,$state,$district);
             

            if(count($posts) >= 9 )
            $last_posts = $posts[9];
            else 
            $last_posts = null;
            
            if($last_posts == null)
            $this->output->set_output(json_encode(array('value' => "operation success",'is_completed' => 1,'status'=>true)));
            
            else{
            if($last_posts['is_completed'] > 0)
            $this->output->set_output(json_encode(array('value' => "operation success",'data' => array($last_posts),'is_completed' => 0,'status'=>true)));
            
            else
            $this->output->set_output(json_encode(array('value' => "operation success",'data' => array($last_posts),'is_completed' => 1,'status'=>true)));
            }

            
            // if(count($posts) >= 9 )
            // $last_posts = $posts[9];
            // else 
            // $last_posts = null;
            
            // if($last_posts == null)
            // $this->output->set_output(json_encode(array('value' => "operation success",'is_completed' => 1,'status'=>true)));
            
            // else{
            // if($last_posts['is_completed'] > 0)
            // $this->output->set_output(json_encode(array('value' => "operation success",'data' => array($last_posts),'is_completed' => 0,'status'=>true)));
            
            // else
            // $this->output->set_output(json_encode(array('value' => "operation success",'data' => array($last_posts),'is_completed' => 1,'status'=>true)));
            // }

            // if(count($posts) >= 9 )
            // $last_posts = $posts[9];
            // else 
            // $last_posts = null;
         
            
            // if($last_posts['challenge_id'] == $challengeid || $last_posts == null)
            // $this->output->set_output(json_encode(array('value' => "successfully deleted",'is_completed'=>1,'status'=>true)));
            // else
            // $this->output->set_output(json_encode(array('value' => "successfully deleted",'data' => array($last_posts),'is_completed' => $last_posts['is_completed'] > 0 ? 0 : 1,'status'=>true)));
            
        }
        else{
            $this->output->set_output(json_encode(['status'=> false , 'value' => 'operation failed'])); 
            }
        }
        else{
            $this->output->set_output(json_encode(['status'=> false , 'value' => 'parameter missing']));
        }
        

    }

    // report posts
    function reportChallenge(){

        $this->output->set_content_type('application/json');

        $logged_userid    = $this->input->post('logged_userid');
        $user_id          = $this->input->post('user_id');
        $challenge_id     = $this->input->post('challenge_id');
        $msg              = $this->input->post('message');
        $page_start       = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;

        $country          = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state            = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district         = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  


        if(!empty($user_id && $challenge_id)){
        
            
        $res         = $this->ChallengeApiModel->reportChallenge($logged_userid , $challenge_id , $msg);
    
           
        switch($res){

            case $res > 0:

            // $this->ChallengeApiModel->updateChallengeReportcount($post_id,$msg);

            // //check report count
            // $remove_post = $this->ChallengeApiModel->removeChallengePostbyCount($post_id);

            $posts = $this->returngetChallengelist($logged_userid,$user_id,$page_start,$country,$state,$district);
            
            if(count($posts) >= 9 )
            $last_posts = $posts[9];
            else 
            $last_posts = null;
         
            $this->ChallengeApiModel->updatepartyReportcount($challenge_id);
         
            if($last_posts['challenge_id'] == $challenge_id || $last_posts == null)
            $this->output->set_output(json_encode(array('value' => "successfully reported",'is_completed' =>1,'status'=>true)));
            else
            $this->output->set_output(json_encode(array('value' => "successfully reported",'data' => array($last_posts),'is_completed' => $last_posts['is_completed'] > 0 ? 0 : 1,'status'=>true)));
            
            break;

            case -1:
            $this->output->set_output(json_encode(array('value' => "already reported",'status'=>false)));
            break;
            
            case 0:
            $this->output->set_output(json_encode(array('value' => "operation failed",'status'=>false)));
            break;

        }
    }
        else{
            $this->output->set_output(json_encode(array('message' => "parameter missing",'status'=>false)));
           
        }
     
    }


    function makePaymentsuccess(){
        $this->output->set_content_type('application/json');
        $challengeid      = $this->input->post('challengeid');
        if(!empty($challengeid)){ 

            $update = array('challenge_ispayed' => 1);
            $where  = array('challenge_id' => $challengeid);
            $res    = $this->ChallengeApiModel->makePaymentsuccess($where ,$update);
            if($res > 0)
                $this->output->set_output(json_encode(array('message' => "payment success updated",'status'=>true)));
            else
                $this->output->set_output(json_encode(array('message' => "operation failed",'status'=>false)));
           
       }else{
                $this->output->set_output(json_encode(array('message' => "parameter missing",'status'=>false)));
           
       }
        
    }



    function likeComment(){
        
        $this->output->set_content_type('application/json');
        $commentid      = $this->input->post('commentid');
        $userid         = $this->input->post('userid');
        $commentuserid  = $this->input->post('commentuserid');
        
        if(!empty($commentid && $userid && $commentuserid)){
        $insert = array('comment_likes_comment_id'      => $commentid,
                        'comment_likes_user_id'         => $userid,
                        'comment_likes_comment_user_id' => $commentuserid
                        );
                        

        $res = $this->CommonModel->likeComment($insert ,'challenge_comment_likes');
            if($res == 1 )                
            $this->output->set_output(json_encode(array('message' => 'liked','status'=>true)));
            else if($res == -1) 
            $this->output->set_output(json_encode(array('message' => 'unliked','status'=>true)));  
        }else{
            $this->output->set_output(json_encode(array('message' => "parameter missing",'status'=>false)));
        }            

    }


    function likepostComment(){
        
        $this->output->set_content_type('application/json');
        $commentid      = $this->input->post('commentid');
        $userid         = $this->input->post('userid');
        $commentuserid  = $this->input->post('commentuserid');
        
        if(!empty($commentid && $userid && $commentuserid)){
        $insert = array('comment_likes_comment_id'      => $commentid,
                        'comment_likes_user_id'         => $userid,
                        'comment_likes_comment_user_id' => $commentuserid
                        );
                        

        $res = $this->CommonModel->likeComment($insert ,'challenge_posts_comment_likes');
            if($res == 1 )                
            $this->output->set_output(json_encode(array('message' => 'liked','status'=>true)));
            else if($res == -1) 
            $this->output->set_output(json_encode(array('message' => 'unliked','status'=>true)));  
        }else{
            $this->output->set_output(json_encode(array('message' => "parameter missing",'status'=>false)));
        }            

    }



    /*********************************       PAYMENT SECTION   ********************************* */

        function getPaymentdetails(){

            $this->output->set_content_type('application/json');


            $gst                    = 5;
            $service_amt            = 1;
            $total                  = $service_amt + ($service_amt * (5/100));
            $gst_amount             = $total - $service_amt;
            $gst_amount             = number_format((float)$gst_amount, 2, '.', ''); 
            
            $this->output->set_output(json_encode(array('gst_percent' => $gst,'gst' => $gst_amount,'service_amount' => $service_amt,'total' => $total,'status'=>true)));
            

        }

    /*********************************  PAYMENT SECTION ENDS HERE  ********************************* */

        function check(){
            //print date('Y-m-d H:i:s');
            $date= "2019-05-14 15:37:43";
            print (strtotime($date)-strtotime(date('Y-m-d H:i:s')));
        }
        

    public function getChallengeStatus($id){
        
      $status = $this->ChallengeApiModel->getChallengeStatus($id);
      $this->output->set_output(json_encode($status));
  }


  // delete challenge comment posts
    function deleteChallengeCommentPosts(){

        $this->output->set_content_type('application/json');

        $challangeid        = $this->input->post('challengeid');
        $user_id            = $this->input->post('userid');
        $comment_id         = $this->input->post('comment_id');
        

        $res  = $this->ChallengeApiModel->deleteChallengeCommentPosts($challangeid,$user_id,$comment_id);

        if($res > 0){

            $this->output->set_output(json_encode(array('message' => "succesfully deleted",'status'=>true)));
        }
        else{

            $this->output->set_output(json_encode(array('posts' => "no value found",'status'=>false)));
        }
    }
    // delete challenge comment posts
    function deleteChallengeComment(){

        $this->output->set_content_type('application/json');

        $challangeid        = $this->input->post('challengeid');
        $user_id            = $this->input->post('userid');
        $comment_id         = $this->input->post('comment_id');
        

        $res  = $this->ChallengeApiModel->deleteChallengeComment($challangeid,$user_id,$comment_id);

        if($res > 0){

            $this->output->set_output(json_encode(array('message' => "succesfully deleted",'status'=>true)));
        }
        else{

            $this->output->set_output(json_encode(array('posts' => "no value found",'status'=>false)));
        }
    }
        // Enable - Disable Challenge Comments
    function updateChallengeCommentStatus(){

        $this->output->set_content_type('application/json');

        $challangeid        = $this->input->post('challengeid');
        $user_id            = $this->input->post('userid');
        $status             = $this->input->post('status'); // true or false
        
        
        $res = $this->ChallengeApiModel->updateChallengeCommentStatus($challangeid,$user_id,$status);
            if($res == 1 ) {
                $this->output->set_output(json_encode(array('message' => 'enabled','status'=>true)));
            } else if($res == -1) {

                $this->output->set_output(json_encode(array('message' => 'disabled','status'=>true)));  

            }else{
                $this->output->set_output(json_encode(array('message' => "parameter missing",'status'=>false)));
        }
    }

    // Enable - Disable Challenge Posts Comments
    function updateChallengePostsCommentStatus(){

        $this->output->set_content_type('application/json');

        $challangeid        = $this->input->post('challengeid');
        $user_id            = $this->input->post('userid');
        $status             = $this->input->post('status'); // true or false
        
        
        $res = $this->ChallengeApiModel->updateChallengePostsCommentStatus($challangeid,$user_id,$status);
            if($res == 1 ) {
                $this->output->set_output(json_encode(array('message' => 'enabled','status'=>true)));
            } else if($res == -1) {

                $this->output->set_output(json_encode(array('message' => 'disabled','status'=>true)));  

            }else{
                $this->output->set_output(json_encode(array('message' => "parameter missing",'status'=>false)));
        }
    }


    //Expire notification 2 days before
    function challengeExpireNotification()
    {
        $expire_challenges = $this->ChallengeApiModel->getExpireChallenges();

        foreach ($expire_challenges as $challenge) 
        {
            $posts = $this->ChallengeApiModel->getChallengeposts($challenge->challenge_id);
        
            $android_devices = $this->UsersApiModel->getUserDevices($challenge->challenge_user_id,1);
            //$ios_devices = $this->UsersApiModel->getUserDevices($challenge->challenge_user_id,2);

            $message = array(
                    'type' => 'challenge expire',
                    'title' => 'Challenge Expire',
                    'message' =>'The challenge '. $challenge->challenge_content.' is going to expire within 2 days '
                );

            if (!empty($android_devices)) sendAndroidPush($android_devices, $message);

            $notification         = array('notifications_user_poll_id' => $challenge->challenge_user_id, 'notifications_challenge_id' => $challenge->challenge_id,'notifications_user_id' => $challenge->challenge_user_id,'notifications_type' => 70,'notification_text'=>$message['message']);  
            $insert_notifications = $this->db->insert('notifications',$notification);

            //if (!empty($ios_devices)) sendIosPush($ios_devices, $message);


            $this->ChallengeApiModel->updateChallenge($challenge->challenge_id,array('is_challenge_expire_notified' => 1));
        
            echo 'Send push to '.$challenge->challenge_user_id.'<br>';


            //push to challenge participants
            foreach ($posts as $post) 
            {
                
                $android_devices = $this->UsersApiModel->getUserDevices($post->challenge_posts_user_id,1);
                //$ios_devices = $this->UsersApiModel->getUserDevices($challenge->challenge_user_id,2);

                $message = array(
                        'type' => 'challenge expire',
                        'title' => 'Challenge Expire',
                        'message' =>'The challenge '. $challenge->challenge_content.' is going to expire within 2 days '
                    );

                if (!empty($android_devices)) sendAndroidPush($android_devices, $message);

                $notification         = array('notifications_user_poll_id' => $post->challenge_posts_user_id, 'notifications_challenge_id' => $challenge->challenge_id,'notifications_user_id' => $post->challenge_posts_user_id,'notifications_type' => 70,'notification_text'=>$message['message']);  
                $insert_notifications = $this->db->insert('notifications',$notification);

                //if (!empty($ios_devices)) sendIosPush($ios_devices, $message);


                $this->ChallengeApiModel->updateChallenge($challenge->challenge_id,array('is_challenge_expire_notified' => 1));
            
                echo 'Send push to '.$post->challenge_posts_user_id.'<br>';
            }

        }

        echo "success";

    }
    

    //Expire notification 2 hours before
    function challengeExpireNotificationHourly()
    {
        $expire_challenges = $this->ChallengeApiModel->getExpireChallengesHourly();

        foreach ($expire_challenges as $challenge) 
        {
            $android_devices = $this->UsersApiModel->getUserDevices($challenge->challenge_user_id,1);
            //$ios_devices = $this->UsersApiModel->getUserDevices($challenge->challenge_user_id,2);

            $message = array(
                    'type' => 'challenge expire',
                    'title' => 'Challenge Expire',
                    'message' =>'The challenge '. $challenge->challenge_content.' is going to expire within 2 hours '
                );

            if (!empty($android_devices)) sendAndroidPush($android_devices, $message);

            //if (!empty($ios_devices)) sendIosPush($ios_devices, $message);

            $notification         = array('notifications_user_poll_id' => $challenge->challenge_user_id, 'notifications_challenge_id' => $challenge->challenge_id,'notifications_user_id' => $challenge->challenge_user_id,'notifications_type' => 72,'notification_text'=>$message['message']);  
            $insert_notifications = $this->db->insert('notifications',$notification);


            $this->ChallengeApiModel->updateChallenge($challenge->challenge_id,array('is_challenge_expire_notified' => 2));
        
            echo 'Send push to '.$challenge->challenge_user_id.'<br>';


            $posts = $this->ChallengeApiModel->getChallengeposts($challenge->challenge_id);
            

            //push to challenge participants
            foreach ($posts as $post) 
            {
                $android_devices = $this->UsersApiModel->getUserDevices($post->challenge_posts_user_id,1);
                //$ios_devices = $this->UsersApiModel->getUserDevices($challenge->challenge_user_id,2);

                $message = array(
                        'type' => 'challenge expire',
                        'title' => 'Challenge Expire',
                        'message' =>'The challenge '. $challenge->challenge_content.' is going to expire within 2 hours '
                    );

                if (!empty($android_devices)) sendAndroidPush($android_devices, $message);

                $notification         = array('notifications_user_poll_id' => $post->challenge_posts_user_id, 'notifications_challenge_id' => $challenge->challenge_id,'notifications_user_id' => $post->challenge_posts_user_id,'notifications_type' => 72,'notification_text'=>$message['message']);  
                $insert_notifications = $this->db->insert('notifications',$notification);

                //if (!empty($ios_devices)) sendIosPush($ios_devices, $message);


                $this->ChallengeApiModel->updateChallenge($challenge->challenge_id,array('is_challenge_expire_notified' => 1));
            
                echo 'Send push to '.$post->challenge_posts_user_id.'<br>';
            }
        }

        echo "success";

    }
    

    //Challenge post poll notify
    function challengePostPollsNotification()
    {
        $polls = $this->ChallengeApiModel->getChallengePostPolls();

        foreach ($polls as $challenge) 
        {

            $users = $this->ChallengeApiModel->getPollUsers($challenge->challenge_polls_challenge_post_id);

            $android_devices = $this->UsersApiModel->getUserDevices($users->challenge_posts_user_id,1);
            //$ios_devices = $this->UsersApiModel->getUserDevices($challenge->challenge_user_id,2);

            if($challenge->count == 1)
                $message = array(
                        'type' => 'challenge post polls',
                        'title' => 'Challenge Post Polls',
                        'message' => $users->poll_user.' polled on your post '
                    );
            else
                $message = array(
                        'type' => 'challenge post polls',
                        'title' => 'Challenge Post Polls',
                        'message' => $users->poll_user.' and other '.$challenge->count.' people polled on your post '
                    );

            if (!empty($android_devices)) sendAndroidPush($android_devices, $message);


            $notification         = array('notifications_user_poll_id' => $users->challenge_posts_user_id, 'notifications_challenge_id' => $users->challenge_posts_challenge_id,'notifications_user_id' => $users->challenge_posts_user_id,'notifications_type' => 73,'notification_text'=>$message['message']);  
            $insert_notifications = $this->db->insert('notifications',$notification);

            //if (!empty($ios_devices)) sendIosPush($ios_devices, $message);


            $this->ChallengeApiModel->updateChallengePostPolls($challenge->challenge_polls_challenge_post_id,array('is_notified' => 1));
        
            echo 'Send push to '.$users->challenge_posts_user_id.'<br>';
        }

        echo "success";

    }


    //notify challenge winner
    function notifyChallengeWinner()
    {
        $expire_challenges = $this->ChallengeApiModel->getExpireChallengesNow();

        foreach ($expire_challenges as $challenge) 
        {
            $winner = $this->ChallengeApiModel->getWinner($challenge->challenge_id);


            $android_devices = $this->UsersApiModel->getUserDevices($challenge->challenge_user_id,1);
            //$ios_devices = $this->UsersApiModel->getUserDevices($challenge->challenge_user_id,2);

            $message = array(
                    'type' => 'challenge winners',
                        'title' => 'Challenge Winners',
                        'message' => 'The winner of the challenge '.$challenge->challenge_content.' is '.$winner[0]['users_name']
                );  

            if (!empty($android_devices)) sendAndroidPush($android_devices, $message);

            $notification         = array('notifications_user_poll_id' => $challenge->challenge_user_id, 'notifications_challenge_id' => $challenge->challenge_id,'notifications_user_id' => $challenge->challenge_user_id,'notifications_type' => 75,'notification_text'=>'The winner of the challenge '.$challenge->challenge_content.' is '.$winner[0]['users_name']);  
            $insert_notifications = $this->db->insert('notifications',$notification);

            //if (!empty($ios_devices)) sendIosPush($ios_devices, $message);
        
            echo 'Send push to '.$challenge->challenge_user_id.'<br>';


            $posts = $this->ChallengeApiModel->getChallengeposts($challenge->challenge_id);

            //push to challenge participants
            foreach ($posts as $post) 
            {
                $android_devices = $this->UsersApiModel->getUserDevices($post->challenge_posts_user_id,1);
                //$ios_devices = $this->UsersApiModel->getUserDevices($challenge->challenge_user_id,2);

                $message = array(
                        'type' => 'challenge winners',
                        'title' => 'Challenge Winners',
                        'message' => 'The winner of the challenge '.$challenge->challenge_content.' is '.$winner[0]['users_name']
                    );

                if (!empty($android_devices)) sendAndroidPush($android_devices, $message);

                $notification         = array('notifications_user_poll_id' => $post->challenge_posts_user_id, 'notifications_challenge_id' => $challenge->challenge_id,'notifications_user_id' => $post->challenge_posts_user_id,'notifications_type' => 75,'notification_text'=>'The winner of the challenge '.$challenge->challenge_content.' is '.$winner[0]['users_name']);  
                $insert_notifications = $this->db->insert('notifications',$notification);

                //if (!empty($ios_devices)) sendIosPush($ios_devices, $message);
            
                echo 'Send push to '.$post->challenge_posts_user_id.'<br>';
            }
        }

        echo "Success";
    }
    
}
