<?php
class ChatApi extends CI_Controller{

   
   
    public $media_url= "https://s3.amazonaws.com/daypoll/";
    public $s3_url   = "https://s3.amazonaws.com/daypoll/";
    public $video_base_url = "http://share.daypoll.com/";
    
    function __construct() {
        parent::__construct();
        $this->load->model('ChatApiModel');
        $this->load->library('form_validation');
        $this->load->library('aws3');
    }


        // send message chat api
    function sendmsg(){
        $zone = getallheaders();
        $GMT  = new DateTimeZone("GMT");
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";

        $this->output->set_content_type('application/json');
        $to_id      = $this->input->post('to_id');
        $from_id    = $this->input->post('from_id');
        $type       = $this->input->post('type');
        $width      = $this->input->post('width');
        $height     = $this->input->post('height');
                           // 0 => text 1 => image
        $is_completed = false;

        $this->form_validation->set_rules('to_id',"to id","required");
        $this->form_validation->set_rules('from_id',"from id","required");
        $this->form_validation->set_rules('type',"type","required");

        if($this->form_validation->run()==true){

            if($type == 1){
            if(isset($_FILES['message'])){   
                
                $errors_thumb    = array();
                $file_name_thumb = $_FILES['message']['name'];
                $file_size_thumb = $_FILES['message']['size'];
                $file_tmp_thumb  = $_FILES['message']['tmp_name'];
                $file_type_thumb = $_FILES['message']['type'];
                $tmp_thumb       = explode('.', $file_name_thumb);
                $file_ext_thumb  = end($tmp_thumb);
    
                
                $thumb_name_image = "chat-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                
                $expensions_thumb_image = array("JPEG","jpg","png");
                
                //if image to upload
           if(in_array($file_ext_thumb,$expensions_thumb_image)=== true){


                if(isset($_FILES['message'])){   
                          
                    // $errors_thumb    = array();
                    // $file_name_thumb = $_FILES['message']['name'];
                    // $file_size_thumb = $_FILES['message']['size'];
                    // $file_tmp_thumb  = $_FILES['message']['tmp_name'];
                    // $file_type_thumb = $_FILES['message']['type'];
                    // $tmp_thumb       = explode('.', $file_name_thumb);
                    // $file_ext_thumb  = end($tmp_thumb);
        
            
                    //     $thumb_name_image = "chat-image".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                    
                         
                    //     $data['tmp_name']  = $_FILES['message']['tmp_name'];
                    //     $data['file_name'] = $thumb_name_image;
         

                              
       
                    //     $val = $this->aws3->sendFile('daypoll/assets/images/chat',$data); 


                    //     //move_uploaded_file($file_tmp_thumb,"assets/images/chat/".$thumb_name);
                    //     $message   = $thumb_name_image;
                    //     $thumbnail = "";


                    $errors    = array();
                    $file_name = $_FILES['message']['name'];
                    $file_size = $_FILES['message']['size'];
                    $file_tmp  = $_FILES['message']['tmp_name'];
                    $file_type = $_FILES['message']['type'];
                    $tmp       = explode('.', $file_name);
                    $file_ext  = end($tmp);
    
    
                    $post_name = "chat_image-".mt_rand(100000,999999).date('H-i-s').'.'.$file_ext;
                    
                    //$expensions = array("jpeg","jpg","png","mp4","3gp");
                    
    
                        $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                            
                        
                  
                            $data['tmp_name']  = $_FILES['message']['tmp_name'];
                            $data['file_name'] = $post_name;
                            $message           = $post_name; 
                            $thumbnail         = "";
            
                            //move_uploaded_file($file_tmp_thumb,"assets/images/chat/".$post_name);
                            $val = $this->aws3->sendFile('daypoll/assets/images/chat',$data); 

                            }
                          
                    

                
               }
                
                //image upload ends here
                else{

                    // if message field have file to upload
                    if(isset($_FILES['message'])){   
                          
                        $errors_thumb    = array();
                        $file_name_thumb = $_FILES['message']['name'];
                        $file_size_thumb = $_FILES['message']['size'];
                        $file_tmp_thumb  = $_FILES['message']['tmp_name'];
                        $file_type_thumb = $_FILES['message']['type'];
                        $tmp_thumb       = explode('.', $file_name_thumb);
                        $file_ext_thumb  = end($tmp_thumb);
                        
                        if ($width < $height) {
                            
                            $thumb_name = "chat-video-portrait-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb; 
                        }else{

                            $thumb_name = "chat-video-landscape-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb; 
                        }
                
                                    
                            $expensions_thumb_video = array("mp4","MP4","3gp");
                        
                            // if uploaded file is a video
                            if(in_array($file_ext_thumb,$expensions_thumb_video)=== true){
                             
                            $data['tmp_name']  = $_FILES['message']['tmp_name'];
                            $data['file_name'] = $thumb_name;
             
                            $val = $this->aws3->sendFile('daypoll/assets/images/chat',$data);     
                            //move_uploaded_file($file_tmp_thumb,"assets/images/chat/".$thumb_name);
                            $message = $thumb_name;

                        }

                    }  
                    
                    

                    // if message field have file to upload
                    if(isset($_FILES['thumbNail'])){   
                          
                        $errors_thumb_thumb    = array();
                        $file_name_thumb_thumb = $_FILES['thumbNail']['name'];
                        $file_size_thumb_thumb = $_FILES['thumbNail']['size'];
                        $file_tmp_thumb_thumb  = $_FILES['thumbNail']['tmp_name'];
                        $file_type_thumb_thumb = $_FILES['thumbNail']['type'];
                        $tmp_thumb_thumb       = explode('.', $file_name_thumb_thumb);
                        $file_ext_thumb_thumb  = end($tmp_thumb_thumb);
            
                
                            $thumb_name_thumb = "chat-thumb".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb_thumb;
                        
                                    
                           
                             
                            $data_thumb['tmp_name']  = $_FILES['message']['tmp_name'];
                            $data_thumb['file_name'] = $thumb_name_thumb;
             
                            $val = $this->aws3->sendFile('daypoll/assets/images/chat/thumbnail',$data_thumb);     
                            //move_uploaded_file($file_tmp_thumb_thumb,"assets/images/chat/thumbnail/".$thumb_name_thumb);
                            $thumbnail = $thumb_name_thumb;

                        

                    }  

                
                }
            }
            }else{
                        $thumbnail  = "";
                        $message    = $this->input->post('message');
                        $this->form_validation->set_rules('message',"message","required");
                        
                }
                  
                // insert to database
                $insert = array('chat_sender_id' => $from_id , 'chat_reciever_id' => $to_id , 'chat_message' => $message ,'thumbnail' => $thumbnail,'chat_type' => $type,'is_completed' => $is_completed);
                $res = $this->ChatApiModel->sendmsg($insert);
                       
            if($res > 0){
            
                            $getlastMsg = $this->ChatApiModel->getlastMsg($res);
                            $lastmsg    = $getlastMsg[0];
                            $lastmsg['is_sender'] = 1;
                            if($lastmsg['chat_type'] == 1){
                                
                                $lastmsg['chat_message'] = $this->media_url."assets/images/chat/".$lastmsg['chat_message'];
                            }
            
                            if($lastmsg['thumbnail'] != ""){
                                $lastmsg['thumbnail']    = $this->media_url."assets/images/chat/thumbnail/".$lastmsg['thumbnail'];
                        
                            }
                            
                            $date = new DateTime($lastmsg['chat_date'], $GMT );
                            $date->setTimezone(new DateTimeZone($zone['timezone']));
                            $lastmsg['chat_date'] = $date->format('Y-m-d H:i:s');
                            
            
                        $this->output->set_output(json_encode(array('message' => "successfully send",'data'=>$lastmsg,'status' => true)));
                       }
                else{
                        $this->output->set_output(json_encode(array('message' => 'failed to insert' ,'status' => false )));
                       }
                    }
                    else{
                        $this->output->set_output(json_encode(array('message' => 'parameter missing' , 'status' => false ))); 
                    }
        }
    

     
//     // send message chat api
//     function sendmsg(){


//         $zone = getallheaders();
//         $GMT  = new DateTimeZone("GMT");
//         if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";

//         $this->output->set_content_type('application/json');
//         $to_id      = $this->input->post('to_id');
//         $from_id    = $this->input->post('from_id');
//         $type       = $this->input->post('type');                   // 0 => text 1 => image
        

//         $this->form_validation->set_rules('to_id',"to id","required");
//         $this->form_validation->set_rules('from_id',"from id","required");
//         $this->form_validation->set_rules('type',"type","required");

//         if($this->form_validation->run()==true){

//         if($type == 1){

//             // $this->load->helper('form');

//             // $config['upload_path']      = "./assets/images/chat/";
//             // $config['allowed_types']    = '*';
//             // $config['file_name']        = 'chat'.mt_rand(100000,999999).date('H-i-s');
//             // //$config['max_height']     = '250';
//             // //$config['max_width']      = '250';
//             // //$config['max_size']       = '100kb';
//             // $config["overwrite"]        = false;

//             // $this->load->library('upload',$config);
//             // if(!$this->upload->do_upload("message"))
//             // {
//             //     $error = array('error' => $this->upload->display_errors());

//             //     $this->output->set_output(json_encode(['message' => $error, 'status'=>'false']));
//             //     return false;
                
//             // }
//             // else
//             // {
                
//             //     $msg_data       = $this->upload->data();
//             //     $message        = $msg_data['file_name'];
//             // }

//             $thumbnail = "";
//             if(isset($_FILES['message'])){   
                          
                
//                 $errors_thumb    = array();
//                 $file_name_thumb = $_FILES['message']['name'];
//                 $file_size_thumb = $_FILES['message']['size'];
//                 $file_tmp_thumb  = $_FILES['message']['tmp_name'];
//                 $file_type_thumb = $_FILES['message']['type'];
//                 $tmp_thumb       = explode('.', $file_name_thumb);
//                 $file_ext_thumb  = end($tmp_thumb);
    
        
//                 $thumb_name = "chat-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
                
//                 $expensions_thumb = array("jpeg","jpg","png","mp4","3gp");
                
//                 if(in_array($file_ext_thumb,$expensions_thumb)=== false){
    
//                 $errors_thumb[] = "extension not allowed, please choose a JPEG or PNG file.";
                
//             }                     
                
//                 if(empty($errors_thumb)==true){
    
//                     $data['tmp_name']  = $_FILES['message']['tmp_name'];
//                     $data['file_name'] = $thumb_name;
//                     $message           = $thumb_name;  
      
//                 }   
               
                  
                
//             }


//         $expensions_thumb_video = array("mp4","MP4","3gp");
            
//         if(in_array($file_ext_thumb,$expensions_thumb_video)=== true){

          
//         if(isset($_FILES['thumbNail'])){   
                          
//             $errors_thumb_msg    = array();
//             $file_name_thumb_msg = $_FILES['thumbNail']['name'];
//             $file_size_thumb_msg = $_FILES['thumbNail']['size'];
//             $file_tmp_thumb_msg  = $_FILES['thumbNail']['tmp_name'];
//             $file_type_thumb_msg = $_FILES['thumbNail']['type'];
//             $tmp_thumb_msg       = explode('.', $file_name_thumb_msg);
//             $file_ext_thumb_msg  = end($tmp_thumb_msg);

    
//                 $thumb_name_msg = "chat_thumbnail-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb_msg;
            
                        
//                 //$expensions_thumb_video = array("mp4","MP4","3gp");
            
//                 if(in_array($file_ext_thumb,$expensions_thumb_video)=== true){
                 
//                 $data_msg['tmp_name']  = $_FILES['thumbNail']['tmp_name'];
//                 $data_msg['file_name'] = $thumb_name_msg;

                    
    
//                 move_uploaded_file($file_tmp_thumb,"assets/images/chat/thumbnail/".$thumb_name_msg);
//                 $thumbnail = $thumb_name_msg;
                
               
//             }else{
//                     $thumbnail = "";
                   
//                 }
          
//         }else{
//             $thumbnail = "";
//         }
//     }
//     $expensions_thumb_video_2 = array("jpg","png","JPEG");
            
//     if(in_array($file_ext_thumb,$expensions_thumb_video_2)=== true){

    
//     if(isset($_FILES['thumbNail'])){   
                      
//         $errors_thumb_msg    = array();
//         $file_name_thumb_msg = $_FILES['thumbNail']['name'];
//         $file_size_thumb_msg = $_FILES['thumbNail']['size'];
//         $file_tmp_thumb_msg  = $_FILES['thumbNail']['tmp_name'];
//         $file_type_thumb_msg = $_FILES['thumbNail']['type'];
//         $tmp_thumb_msg       = explode('.', $file_name_thumb_msg);
//         $file_ext_thumb_msg  = end($tmp_thumb_msg);


//             $thumb_name_msg = "chat_thumbnail-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb_msg;
        
                    
//             //$expensions_thumb_video = array("mp4","MP4","3gp");
        
//             if(in_array($file_ext_thumb,$expensions_thumb_video)=== true){
             
//             $data_msg['tmp_name']  = $_FILES['thumbNail']['tmp_name'];
//             $data_msg['file_name'] = $thumb_name_msg;

                

//             move_uploaded_file($file_tmp_thumb,"assets/images/chat/thumbnail/".$thumb_name_msg);
//             $thumbnail = $thumb_name_msg;
            
           
//         }else{
//                 $thumbnail = "";
                
//             }
      
//     }else{
//         $thumbnail = "";
//     }
// }     

//         }
//         else{
//             $thumbnail  = "";
//             $message    = $this->input->post('message');
//             $this->form_validation->set_rules('message',"message","required");
            
//         }
      
//             $insert = array('chat_sender_id' => $from_id , 'chat_reciever_id' => $to_id , 'chat_message' => $message ,'thumbnail' => $thumbnail,'chat_type' => $type);
//             $res = $this->ChatApiModel->sendmsg($insert);
           
//             if($res > 0){

//                 $getlastMsg = $this->ChatApiModel->getlastMsg($res);
//                 $lastmsg    = $getlastMsg[0];
//                 $lastmsg['is_sender'] = 1;
//                 if($lastmsg['chat_type'] == 1){
                    
//                     $lastmsg['chat_message'] = $this->media_url."assets/images/chat/".$lastmsg['chat_message'];
//                 }

//                 if($lastmsg['thumbnail'] != ""){
//                     $lastmsg['thumbnail']    = $this->media_url."assets/images/chat/thumbnail/".$lastmsg['thumbnail'];
            
//                 }
                
//                     $date = new DateTime($lastmsg['chat_date'], $GMT );
//                     $date->setTimezone(new DateTimeZone($zone['timezone']));
//                     $lastmsg['chat_date'] = $date->format('Y-m-d H:i:s');
                

//                 $this->output->set_output(json_encode(array('message' => "successfully send",'data'=>$lastmsg,'status' => true)));
//             }
//             else{
//                 $this->output->set_output(json_encode(array('message' => 'failed to insert' ,'status' => false )));
//             }
//         }
//         else{
//             $this->output->set_output(json_encode(array('message' => 'parameter missing' , 'status' => false))); 
//         }
//     }

  

    // delete chat by chat id
    function deleteChatbyid(){

        $this->output->set_content_type('application/json');

        $col_name    = ""; 
        $chatid      = $this->input->post('chatid');
        $userid      = $this->input->post('userid');
        $type        = $this->input->post('type');
        if($type == 1){
            $col_name = array('chat_sender_status' => 0);
        }else if($type == 0){
            $col_name = array('chat_reciever_status' => 0);
        }
        if(!empty($chatid)){
            
            $res  = $this->ChatApiModel->deleteChatbyid($chatid,$userid,$col_name);

            if($res > 0){
                $this->output->set_output(json_encode(array('message' => 'succesfully deleted', 'status' => true)));
            }
            else{
                $this->output->set_output(json_encode(array('message' => 'failed to delete', 'status' => false)));
            }
        }
        else{
            $this->output->set_output(json_encode(array('message' => 'parameter missing', 'status' => false)));
        }

    }

    // delete chat by chat id
    function deleteChatbyuser(){

        $this->output->set_content_type('application/json');

        $loggedid      = $this->input->post('loggedid');
        $userid        = $this->input->post('userid');
        
        if(!empty($userid && $loggedid)){

            $res_sender     = $this->ChatApiModel->get_sender_for_delete($loggedid , $userid);
            $res_reciever   = $this->ChatApiModel->get_reciever_for_delete($loggedid , $userid);

            $all_msg        = array_merge($res_sender,$res_reciever);
           
            
            foreach($all_msg as $value => $k){
                if($all_msg[$value]['chat_sender_id'] == $loggedid)
                $res = $this->ChatApiModel->deleteChatbyuser($all_msg[$value]['chat_id'] ,'chat_sender_status');
                else
                $res = $this->ChatApiModel->deleteChatbyuser($all_msg[$value]['chat_id'] ,'chat_reciever_status');
            }

           

            if($res > 0){
                $this->output->set_output(json_encode(array('message' => 'succesfully deleted', 'status' => true)));
            }
            else{
                $this->output->set_output(json_encode(array('message' => 'failed to delete', 'status' => false)));
            }
            }
            else{
                    $this->output->set_output(json_encode(array('message' => 'parameter missing', 'status' => false)));
            }

    }


    //chat inbox
    function chatInbox(){
        
        $this->output->set_content_type('application/json');

        $zone = getallheaders();
        $GMT  = new DateTimeZone("GMT");
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";


        $userid      = $this->input->post('user_id');
        if(!empty($userid)){

            $res_sender     = $this->ChatApiModel->get_sender($userid);
            $res_reciever   = $this->ChatApiModel->get_reciever($userid);

            $response       = array_merge($res_sender,$res_reciever);

            $res =[];
            foreach($response as $value){
                $res[$value['userid']] = $value;
            }
          
            if($res){
                
                foreach($res as $key => $value){

                    

                    $res[$key]['unread']    = $this->ChatApiModel->getUnreadMessages($res[$key]['loggedid'],$res[$key]['userid']);
                    $res[$key]['last_msg']  = $this->ChatApiModel->getlastMsgbyid($res[$key]['loggedid'],$res[$key]['userid']);
                    if($res[$key]['loggedid'] == $userid)
                    {
                    $res[$key]['user_id']    = $res[$key]['userid'];
                    $get_user                = $this->ChatApiModel->getusernameforChat($res[$key]['userid']);
                    $res[$key]['user_name']  = $get_user['username'];
                    $res[$key]['user_photo'] = $this->media_url."assets/images/users/".$get_user['userphoto'];
                    }else{

                    $res[$key]['user_id']    = $res[$key]['loggedid'];
                    $get_user                = $this->ChatApiModel->getusernameforChat($res[$key]['loggedid']);
                    $res[$key]['user_name']  = $get_user['username'];
                    $res[$key]['user_photo'] = $this->media_url."assets/images/users/".$get_user['userphoto'];
                    }

                    foreach($res[$key]['last_msg'] as $v => $k){

                        if($res[$key]['last_msg'][$v]['chat_type'] == 2 && empty($res[$key]['last_msg'][$v]['chat_message']))
                        {
                            $res[$key]['last_msg'][$v]['chat_type'] = 5;
                            $res[$key]['last_msg'][$v]['chat_message'] = $res[$key]['last_msg'][$v]['chat_post'];
                        }
                        $date = new DateTime($res[$key]['last_msg'][$v]['chat_date'], $GMT );
                        $date->setTimezone(new DateTimeZone($zone['timezone']));
                        $res[$key]['last_msg'][$v]['chat_date'] = $date->format('Y-m-d H:i:s');

                        if($res[$key]['last_msg'][$v]['chat_type'] == 1)
                        $res[$key]['last_msg'][$v]['chat_message'] = $this->media_url."assets/images/chat/".$res[$key]['last_msg'][$v]['chat_message'];
                        
                        if($res[$key]['last_msg'][$v]['thumbnail'] != ""){
                            $res[$key]['last_msg'][$v]['thumbnail']    = $this->media_url."assets/images/chat/thumbnail/".$res[$key]['last_msg'][$v]['thumbnail'];
                    
                        }
                        
                       //$res[$key]['last_msg'][$v]['thumbnail']    = $this->media_url."assets/images/chat/thumbnail/".$res[$key]['last_msg'][$v]['thumbnail'];
                        
                        $res[$key]['is_sender']         = $res[$key]['last_msg'][$v]['chat_sender_id'] == $userid ? 1 :0;
                        $res[$key]['chat_date']         = $res[$key]['last_msg'][$v]['chat_date'];
                        $res[$key]['message']           = $res[$key]['last_msg'][$v]['chat_message'];
                        $res[$key]['type']              = $res[$key]['last_msg'][$v]['chat_type'];
                        $res[$key]['seen_status']       = $res[$key]['last_msg'][$v]['chat_seen_status'];
                       
                        $sort[$key]                     = strtotime($res[$key]['chat_date']);
                        
                    }

                    unset($res[$key]['last_msg']);
                    
                }

               
                foreach($res as $row){
                    $final_res[] = $row;
                }
               array_multisort($sort,SORT_DESC,$final_res);

               
             
                $this->output->set_output(json_encode(array('message' => $final_res, 'status' => true)));
            }
            else{
                $this->output->set_output(json_encode(array('message' => 'no messages found', 'status' => false)));
            }
        }else{
                $this->output->set_output(json_encode(array('message' => 'parameter missing', 'status' => false)));
        }

    }

    //search chat user
    function searchUserinChat(){
        
        $this->output->set_content_type('application/json');

        $zone = getallheaders();
        $GMT  = new DateTimeZone("GMT");
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";
        
        $search = $this->input->post('search');
        $userid = $this->input->post('userid');

        $res_sender      = $this->ChatApiModel->searchUserinChatsender($search , $userid);
        $res_reciever    = $this->ChatApiModel->searchUserinChatreciever($search , $userid);
        $res = array_merge($res_sender,$res_reciever);
        
       

        $res_final =[];
            foreach($res as $value){
                $res_final[$value['userid']] = $value;
            }
     

        foreach($res_final as $value => $k){
            $get_user                         = $this->ChatApiModel->getusernameforChat($res_final[$value]['userid']);  
            $res_final[$value]['user_name']   = $get_user['username'];
            $res_final[$value]['user_photo']  = $this->media_url."assets/images/users/".$get_user['userphoto'];
            $res_final[$value]['last_msg']            = $this->ChatApiModel->getlastMsgbyid($res_final[$value]['loggedid'],$res_final[$value]['userid']);
        

        foreach($res_final[$value]['last_msg'] as $v => $k){

            $date = new DateTime($res[$key]['last_msg'][$v]['chat_date'], $GMT );
            $date->setTimezone(new DateTimeZone($zone['timezone']));
            $res_final[$value]['last_msg'][$v]['chat_date'] = $date->format('Y-m-d H:i:s');

            if($res_final[$value]['last_msg'][$v]['chat_type'] == 1)
            $res_final[$value]['last_msg'][$v]['chat_message'] = $this->media_url."assets/images/chat/".$res_final[$value]['last_msg'][$v]['chat_message'];
            
            $res_final[$value]['is_sender']         = $res_final[$value]['last_msg'][$v]['chat_sender_id'] == $userid ? 1 :0;
            $res_final[$value]['chat_date']         = $res_final[$value]['last_msg'][$v]['chat_date'];
            $res_final[$value]['message']           = $res_final[$value]['last_msg'][$v]['chat_message'];
            $res_final[$value]['type']              = $res_final[$value]['last_msg'][$v]['chat_type'];
            $res_final[$value]['seen_status']       = $res_final[$value]['last_msg'][$v]['chat_seen_status'];

        }
    
        unset($res_final[$value]['last_msg']);
    }
        foreach($res_final as $row){
            $res_last[] = $row;
        }
        
        if($res){
            $this->output->set_output(json_encode(array('message' => $res_last, 'status' => true)));
          
        }else{
            $this->output->set_output(json_encode(array('message' => 'no user found', 'status' => false)));
          
        }

    }

    // make message read
    function udpateMsgread(){
        
        $this->output->set_content_type('application/json');
        
        $fromid = $this->input->post('loggedid');
        $toid   = $this->input->post('userid');

        $res    = $this->ChatApiModel->udpateMsgread($fromid,$toid);

        if($res > 0){
            $this->output->set_output(json_encode(array('message' => "successfully updated", 'status' => true)));
          
        }else{
            $this->output->set_output(json_encode(array('message' => 'failed to update', 'status' => false)));
          
        }
    }

    // get individual chat 
    function getChatMessages(){
        $this->output->set_content_type('application/json');
        
        $fromid = $this->input->post('loggedid');
        $toid   = $this->input->post('userid');
        
        $res    = $this->ChatApiModel->getMessages($fromid,$toid);
        $this->ChatApiModel->udpateMsgread($fromid,$toid);

        if($res > 0){
            $this->output->set_output(json_encode(array('data' => $res, 'status' => true)));
          
        }else{
            $this->output->set_output(json_encode(array('data' => 'no messages found', 'status' => false)));
          
        }
    }


     // get chat by id
     function getChatlist(){

        $zone = getallheaders();
        $GMT  = new DateTimeZone("GMT");
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";

        $this->output->set_content_type('application/json');
        $userid      = $this->input->post('user_id');
        $sendid      = $this->input->post('sent_to');
        $time        = $this->input->post('time_zone');

        $this->form_validation->set_rules('user_id','user_id','required');
        $this->form_validation->set_rules('sent_to','sent_to','required');

        if($this->form_validation->run()== true){
        $this->ChatApiModel->udpateMsgread($userid,$sendid);
        $get_chat               = $this->ChatApiModel->getChatlist($userid,$sendid);
        $checkBlocked           = $this->ChatApiModel->isBlocked(array('userid' => $sendid , 'blocked_userid' => $userid)) == 0 ? 0 :1;
        $checksenderBlocked     = $this->ChatApiModel->isBlocked(array('userid' => $userid , 'blocked_userid' => $sendid)) == 0 ? 0 :1;
        
        foreach($get_chat as $key => $val){


            if($get_chat[$key]['chat_type'] == 1){
                $get_chat[$key]['chat_message'] = $this->media_url."assets/images/chat/".$get_chat[$key]['chat_message'];
                if($get_chat[$key]['thumbnail'] != ""){
                $get_chat[$key]['thumbnail']    = $this->media_url."assets/images/chat/thumbnail/".$get_chat[$key]['thumbnail'];
                }
                
            }

            if($get_chat[$key]['chat_sender_id'] == $userid)$get_chat[$key]['is_sender'] = 1;
            else $get_chat[$key]['is_sender'] = 0;

            
            $date = new DateTime($get_chat[$key]['chat_date'], $GMT );
            $date->setTimezone(new DateTimeZone($zone['timezone']));
            $get_chat[$key]['chat_date'] = $date->format('Y-m-d H:i:s');

        }

        if($get_chat != null){
            $this->output->set_output(json_encode(array('message' => 'value found','issenderblocked' => $checkBlocked,'isblocked' => $checksenderBlocked, 'data' => $get_chat, 'status' => true)));
        }
        else{
            $this->output->set_output(json_encode(array('message' => 'no data found' ,'status' => false )));
        }
    }
    else{
        $this->output->set_output(json_encode(array('message' => validation_errors(),'status' => false )));
    }
        
    }

    function clearChat(){
        
        $this->output->set_content_type('application/json');
        $userid = $this->input->post('userid');
        $res    = $this->ChatApiModel->clearChat($userid);
        if($res > 0){
            $this->output->set_output(json_encode(array('data' => "successfully cleared", 'status' => true)));
        }else{
            $this->output->set_output(json_encode(array('data' => 'operation failed', 'status' => false)));
             }
        }

    function getUnreadMessagesCount(){

        $this->output->set_content_type('application/json');
        $userid      = $this->input->post('userid');

        if(!empty($userid)){

            $isblocked      = $this->ChatApiModel->check_isBlocked(array('user_id' => $userid , 'users_active' => 0));


            $res_sender     = $this->ChatApiModel->get_sender($userid);
            $res_reciever   = $this->ChatApiModel->get_reciever($userid);

            $noti_count     = $this->ChatApiModel->get_notification_count($userid);
            
            $response       = array_merge($res_sender,$res_reciever);

                  
            $res =[];
            foreach($response as $value){
                $res[$value['userid']] = $value;
            }
          
            if($res){
                foreach($res as $key => $value){

                    $res[$key]['unread']    = $this->ChatApiModel->getUnreadMessagesCount($res[$key]['loggedid'],$res[$key]['userid']);
                   
                }

                foreach($res as $row){
                    $final_res[] = $row;
                }
              
                $count_res = [];
                foreach($final_res as $val => $k){
                    if($final_res[$val]['unread'] != 0){
                        $count_res[] = $final_res[$val]['userid'];
                    }
                }
                
            }else{
                $count_res = [];
            }  
            if(!empty($res) || !empty($noti_count)){ 
             
                $this->output->set_output(json_encode(array('data' => sizeof($count_res),'notification_count' => $noti_count,'isUserBlocked' => $isblocked > 0 ? 1 : 0, 'status' => true)));
            }
            else{
                $this->output->set_output(json_encode(array('data' => 'no messages found','notification_count' => $noti_count,'isUserBlocked' => $isblocked > 0 ? 1 : 0, 'status' => false)));
            }
        }else{
                $this->output->set_output(json_encode(array('data' => 'parameter missing', 'status' => false)));
        }
    }

    // send message through chat
    function sendmsgPost(){
        
        $this->output->set_content_type('application/json');
        
        $to_id      = $this->input->post('to_id');
        $from_id    = $this->input->post('from_id');
        $post_id    = $this->input->post('post_id');  
        $type       = $this->input->post('type');        // 0 => post , 1 => party post , 2 => group post          
        
       if(!empty($to_id && $from_id && $post_id)){

        if($type == 0){
        
        $res = $this->ChatApiModel->getPost('posts',array('posts_id'=>$post_id));
        $post = $res[0];
        
        switch($post['photo_or_video']){
            case -1:
            $token                         = mt_rand(100000,999999);
            $post['sharecode']             = $this->video_base_url.'post-view?postid='.$post['posts_id'].'&&type=0';
            $message                       = $post['sharecode']; 
            $post['post_url']              = ""; 
            $post['type']                  = 2; 
            break;

            case 0:
            $token                         = mt_rand(100000,999999);
            $post['post_url']              = $this->s3_url."assets/images/posts/".$post['posts_photo_or_video'];
            $post['sharecode']             = $this->video_base_url.'post-view?postid='.$post['posts_id'].'&&type=0';
            $message                       = $post['sharecode'];
            $post['type']                  = 2; 
            break;

            case 1:
            $token                         = mt_rand(100000,999999);
            $post['post_url']              = $this->s3_url."assets/images/posts/thumbnails/".$post['posts_video_thumbnail'];
            $post['sharecode']             = $this->media_url.'post-view?postid='.$post['posts_id'].'&&type=0';
            $message                       = $post['sharecode'];
            $post['type']                  = 2; 
            break;
        }
    }

    elseif($type == 1){
        
        $res = $this->ChatApiModel->getPost('party_posts',array('party_posts_id'=>$post_id));
        $post = $res[0];
        
        switch($post['photo_or_video']){
            case -1:
            $token                         = mt_rand(100000,999999);
            $post['sharecode']             = $this->video_base_url.'post-view?postid='.$post['party_posts_id'].'&&type=1';
            $message                       = $post['sharecode']; 
            $post['post_url']              = ""; 
            $post['type']                  = 2; 
            break;

            case 0:
            $token                         = mt_rand(100000,999999);
            $post['post_url']              = $this->s3_url."assets/images/partyposts/".$post['party_posts_photo_video'];
            $post['sharecode']             = $this->video_base_url.'post-view?postid='.$post['party_posts_id'].'type=1';
            $message                       = $post['sharecode'];
            $post['type']                  = 2; 
            break;

            case 1:
            $token                         = mt_rand(100000,999999);
            $post['post_url']              = $this->s3_url."assets/images/posts/partyposts/thumbnail/".$post['party_posts_video_thumbnail'];
            $post['sharecode']             = $this->video_base_url.'post-view?postid='.$post['party_posts_id'].'.&&type=1';
            $message                       = $post['sharecode'];
            $post['type']                  = 2; 
            break;
        }
    }

    elseif($type == 2){
        
        $res = $this->ChatApiModel->getPost('group_posts',array('group_posts_id'=>$post_id));
        $post = $res[0];
        
        switch($post['photo_or_video']){
            case -1:
            $token                         = mt_rand(100000,999999);
            $post['sharecode']             = $this->video_base_url.'post-view?postid='.$post['group_posts_id'].'&&type=2';
            $message                       = $post['sharecode']; 
            $post['post_url']              = ""; 
            $post['type']                  = 2; 
            break;

            case 0:
            $token                         = mt_rand(100000,999999);
            $post['post_url']              = $this->s3_url."assets/images/groupposts/".$post['group_posts_photo_video'];
            $post['sharecode']             = $this->video_base_url.'post-view?postid='.$post['group_posts_id'].'&&type=2';
            $message                       = $post['sharecode'];
            $post['type']                  = 2; 
            break;

            case 1:
            $token                         = mt_rand(100000,999999);
            $post['post_url']              = $this->s3_url."assets/images/posts/groupposts/thumbnail/".$post['group_posts_video_thumbnail'];
            $post['sharecode']             = $this->video_base_url.'post-view?postid='.$post['group_posts_id'].'&&type=2';
            $message                       = $post['sharecode'];
            $post['type']                  = 2; 
            break;
        }
    }

        

        for($i=0; $i<sizeof($to_id); $i++){
            $insert = array('chat_sender_id' => $from_id , 'chat_reciever_id' => $to_id[$i] ,'chat_type' => $post['type'], 'chat_message' => $post['post_url'],'chat_post' => $post['sharecode']);
            $res    = $this->ChatApiModel->sendmsg($insert);
            }
        
        if($res){
            $this->output->set_output(json_encode(array('message' => "successfully inserted" , 'status'=> true)));
        }
        else{
            $this->output->set_output(json_encode(array('message' => "operation failed" , 'status'=> false)));
        }
        }else{
        $this->output->set_output(json_encode(array('message' => "parameter missing" , 'status'=> false)));
        }
    }

    function blockUserChat(){
        $this->output->set_content_type('application/json');
        $userid         = $this->input->post('userid');
        $blockeduserid  = $this->input->post('blockeduserid');

        $res = $this->ChatApiModel->blockUserChat($userid , $blockeduserid);
        if($res == 1){
            $this->output->set_output(json_encode(array('message' => "successfully blocked" , 'status'=> true)));
        }
        else if($res == -1){
            $this->output->set_output(json_encode(array('message' => "successfully unblocked" , 'status'=> true)));
        }
        else if($res == 0){
            $this->output->set_output(json_encode(array('message' => "something went wrong" , 'status'=> false)));
        }
    }

    public function getConvertedVideo(){   
          
        $request = $this->input->post();
        $key = array_values($request['key'])[0]; 
        $key_folder = explode("/", $key);
        $file_name = $key_folder[4];
        if (strpos($key_folder[0], 'chat') !== false) { 

        $this->ChatApiModel->updateVideoStatus(explode("mp4",  $key_folder[3])[0].'mp4');

        }
    
    }

}
?>