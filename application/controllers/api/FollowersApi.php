<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class FollowersApi extends CI_Controller{
    
    public $media_url= "https://s3.amazonaws.com/daypoll/";
    

    function __construct() {
        parent::__construct();
        $this->load->model(array('FollowersModel','PartyApiModel','UsersApiModel'));
    }
    
    // userid followerid flag , poll another user
    function pollUser(){      
        
        $this->output->set_content_type('application/json');
        $userid         = $this->input->post('userid');
        $userfollowid   = $this->input->post('userfollowerid');
       
        if(!empty($userfollowid && $userid)){

        $res            = $this->FollowersModel->polluser($userid,$userfollowid);

        if($res['msg'] != 'unliked')
        {
            //Push notification
            $android_devices = $this->UsersApiModel->getUserDevices($userid,1);
            $ios_devices = $this->UsersApiModel->getUserDevices($userid,2);


            $user_details = $this->UsersApiModel->getUsername($userfollowid);

            $message = array(
                    'type' => 'follow',
                    'title' => 'Follow',
                    'message' =>$user_details.' followed you'
                );

            if (!empty($android_devices)) sendAndroidPush($android_devices, $message);

            //if (!empty($ios_devices)) sendIosPush($ios_devices, $message);
        }
        
        $this->output->set_output(json_encode(array('res'=>$res['msg'],'status'=>true)));
        }
        else{
            $this->output->set_output(json_encode(array('error' => 'parameter missing')));
        }
    }
    
    

    // userid partyid flag , join a party by user
     function joinParty(){      
        
        $this->output->set_content_type('application/json');
        
        $userid     = $this->input->post('userid');
        $partyid    = $this->input->post('partyid');
        $scope      = $this->input->post('scope');     // 1 -> country , 2 -> state , 3 -> district
       
        
        switch($scope){
            case 1:
            $res_check_party     = $this->FollowersModel->checkuserParty($userid ,'users_party_id',$scope);
            break;

            case 2:
            $res_check_party     = $this->FollowersModel->checkuserParty($userid ,'users_party_id', $scope);
            break;

            case 3:
            $res_check_party     = $this->FollowersModel->checkuserParty($userid ,'users_party_id', $scope);
            break;

            case 4:
            $res_check_party     = $this->FollowersModel->checkuserParty($userid ,'users_party_id', $scope);
            break;
        }   

       

        //$res_check_party     = $this->FollowersModel->checkuserParty($userid , $scope);
       
        if($res_check_party == 1){

            switch($scope){
                
                case 1:
                $res      = $this->FollowersModel->joinParty($userid ,'users_party_id','users_poll_party_id',$partyid);
                break;
    
                case 2:
                $res      = $this->FollowersModel->joinParty($userid ,'users_party_id','users_poll_party_id', $partyid);
                break;
    
                case 3:
                $res      = $this->FollowersModel->joinParty($userid ,'users_party_id','users_poll_party_id', $partyid);
                break;

                case 4:
                $res      = $this->FollowersModel->joinParty($userid ,'users_party_id','users_poll_party_id', $partyid);
                break;
           
            }   

            if($res > 0){

                $this->output->set_output(json_encode(array('res' => 'Successfully Joined','isjoin' => 1,'ispoll' => 1,'status' => true)));
            }
            else{

                $this->output->set_output(json_encode(array('res' => 'Failed to join','isjoin' => 0 ,'ispoll' => 0,'status' => false)));
            }

            }

            else{
                $this->output->set_output(json_encode(array('res' => 'You are already following other party. Are you sure you want to unjoin from that party ?','status' => false)));

            }
      
    }
    // if user join a party already confirm to add other (same model) 
    function confirmjoinParty(){

        $this->output->set_content_type('application/json');
        $userid     = $this->input->post('userid');
        $partyid    = $this->input->post('partyid');
        $scope      = $this->input->post('scope');
        
         
        switch($scope){
            case 1:
            $res        = $this->FollowersModel->joinParty($userid,'users_party_id','users_poll_party_id',$partyid);
            break;
            
            case 2:
            $res        = $this->FollowersModel->joinParty($userid,'users_party_id','users_poll_party_id',$partyid);
            break;
            
            case 3:
            $res        = $this->FollowersModel->joinParty($userid,'users_party_id','users_poll_party_id',$partyid);
            break;

            case 4:
            $res        = $this->FollowersModel->joinParty($userid,'users_party_id','users_poll_party_id',$partyid);
            break;
        }
        
        
        if($res > 0){

            $this->output->set_output(json_encode(array('res' => 'Successfully Joined','isjoin' => 1,'ispoll'=> 1, 'status' => true)));
        }
        else{

            $this->output->set_output(json_encode(array('res' => 'Failed to join','isjoin' => 0,'ispoll'=> 0,'status' => false)));
        }
    }



    // unjoin party
    function removeJoin(){

        $this->output->set_content_type('application/json');

        $userid = $this->input->post('userid');
        $partyid= $this->input->post('partyid');
        $scope  = $this->input->post('scope');
        
        //

       
        switch($scope){

            case 1:
            $res = $this->FollowersModel->removeJoin($userid ,'users_party_id','users_poll_party_id');
            break;

            case 2:
            $res = $this->FollowersModel->removeJoin($userid , 'users_party_id','users_poll_party_id');
            break;

            case 3:
            $res = $this->FollowersModel->removeJoin($userid , 'users_party_id','users_poll_party_id');
            break;

            case 4:
            $res = $this->FollowersModel->removeJoin($userid , 'users_party_id','users_poll_party_id');
            break;
        }
       
        if($res > 0){
            $this->FollowersModel->removeFollowers($userid , $partyid);
            $this->output->set_output(json_encode(array('res' => 'Successfully Removed','status' => true)));
        } 
        else{
          
            $this->output->set_output(json_encode(array('res' => 'Failed to update','status' => false)));
        }
   
    } 

    // poll party code
    function pollParty(){

        $this->output->set_content_type('application/json');
        $userid     = $this->input->post('userid');
        $partyid    = $this->input->post('partyid');
        $poll_type  = $this->input->post('polltype');  // 0 => public  , 1 => private
        $scope      = $this->input->post('scope');     // 1 -> country , 2 -> state , 3 -> district 

        if(!empty($userid && $partyid)){

        switch($scope){
            case 1:
            $check_party_joined     = $this->FollowersModel->check_party_joined($userid ,'users_party_id');
            break;

            case 2:
            $check_party_joined     = $this->FollowersModel->check_party_joined($userid ,'users_party_id');
            break;

            case 3:
            $check_party_joined     = $this->FollowersModel->check_party_joined($userid ,'users_party_id');
            break;

            case 4:
            $check_party_joined     = $this->FollowersModel->check_party_joined($userid ,'users_party_id');
            break;
        }   
           
            if($check_party_joined == $partyid || $check_party_joined == 1){
               

                
                switch($scope){
                    case 1:
                    $res        = $this->FollowersModel->polluserParty($userid,$partyid ,$poll_type,'users_poll_party_id','users_party_poll_type');
                    break;
                    
                    case 2:
                    $res        = $this->FollowersModel->polluserParty($userid,$partyid ,$poll_type,'users_poll_party_id','users_party_poll_type');
                    break;
                    
                    case 3:
                    $res        = $this->FollowersModel->polluserParty($userid,$partyid ,$poll_type,'users_poll_party_id','users_party_poll_type');
                    break;

                    case 4:
                    $res        = $this->FollowersModel->polluserParty($userid,$partyid ,$poll_type,'users_poll_party_id','users_party_poll_type');
                    break;
                }
                
                
                if($res > 0){
                   
                    $msg         = "Successfully polled";
                    $isPolled    = 1;
                    $status      = true;
                }
                else{

                    $msg         = "Failed to poll";
                    $isPolled    = 0;
                    $status      = false;
                }
                
                
            }
            else{

                $msg         = "Already joined in other party";
                $isPolled    = 0;
                $status      = false;      
            }

                $this->output->set_output(json_encode(array('res' => $msg,'status' => $status ,'ispolled' => $isPolled)));
        
            
        }
            else{

                $this->output->set_output(json_encode(array('res' => 'parameter missing','status' => false)));
        
                }
        }
    
        function unpollParty(){

            $this->output->set_content_type('application/json');
            
            $userid  = $this->input->post('userid');
            $scope  = $this->input->post('scope');
        
       
           
            if(!empty($userid)){
    
            switch($scope){
    
                case 1:
                $res = $this->FollowersModel->unpollUserParty($userid,'users_poll_party_id');
                break;
    
                case 2:
                $res = $this->FollowersModel->unpollUserParty($userid ,'users_poll_party_id');
                break;
    
                case 3:
                $res = $this->FollowersModel->unpollUserParty($userid ,'users_poll_party_id');
                break;
            }
            
            if($res > 0){
                $this->output->set_output(json_encode(array('res'=>'Succesfully Unpolled','ispolled' =>0,'status' => 'true')));
            } 
    
            else{
    
                $this->output->set_output(json_encode(array('res'=>'Failed to unpoll','status' => 'false')));
            }
         }
        else{
            $this->output->set_output(json_encode(array('res'=>'Parameter Missing','status' => 'false')));
        }
    }

    function getChatUsersList(){
        
        $this->output->set_content_type('application/json');
        
        $userid         = $this->input->post('userid');
        $search         = $this->input->post('search');
        $res_follower   = $this->FollowersModel->getChatFollowerUsersList($userid,$search); 
        $res_following  = $this->FollowersModel->getChatFollowingUsersList($userid,$search);
        
        foreach($res_follower as $row){
            $res_following[] = $row;
        }

        
        $i              = 0;
        $userIdArray    = [];  
        foreach($res_following as $row){
            if($row['userid'] == $userid){
                unset($res_following[$i]);
            }else{
                if(in_array($row['userid'],$userIdArray)){
                    unset($res_following[$i]);
                }else{
                    $userIdArray[]  =   $row['userid'];  
                }          
            }
            $i++;
        }

        $res = [];
        foreach($res_following as $row){
            $res[] = $row;
        }
        $get_blocked_users = $this->FollowersModel->getBlockedUserlist($userid);
       
       if($get_blocked_users){

        foreach($get_blocked_users as $row){
            $block_id[] = $row['blocked_userid']; 
        }

        foreach($get_blocked_users as $row){
            $block_block_id[] = $row['userid']; 
        }


        $final_array = [];
        foreach($res as $key){
            if(!in_array($key['userid'],$block_id)){
                $final_array[] = $key;
            }
        }
       

        $last_final_array = [];
        foreach($final_array as $key){
            if(!in_array($key['userid'],$block_block_id)){
                $last_final_array[] = $key;
            }
        }
        }else{
            $last_final_array = $res;
        }
        
        
        if($last_final_array){
            foreach($last_final_array as $value => $key){
                $last_final_array[$value]['users_photo'] = $this->media_url.'assets/images/users/'.$last_final_array[$value]['users_photo'];
            }
            
        $this->output->set_output(json_encode(array('res' => $last_final_array,'status' => true)));
        }
        else
        $this->output->set_output(json_encode(array('res' => 'no data found','status' => false)));
      
    }
    

}