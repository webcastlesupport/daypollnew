<?php
//Project Name: Daypoll
//File Path: [base_path]->application->controllers
//PHP Team, Iroid Technologies
//Last Updated: 30-Aug-19
class NewPostApi extends CI_Controller {

    public $media_url = "https://s3.amazonaws.com/daypoll/";
    public $s3_url = "https://s3.amazonaws.com/daypoll/";
    //public $media_url= "http://iroidtechnologies.in/";

    function __construct() {
        error_reporting(0);
        parent::__construct();
        $this->load->model(array('PostsApiModel', 'UsersApiModel', 'ChallengeApiModel', 'CommonModel'));
        $this->load->library('form_validation');
        $this->load->library('aws3');
    }

    function tags() {
        $tags = "#A #E #I #O #U";
        $t = str_replace("#", ",", $tags);
        $arr = explode(" ", $t);
        print_r($arr);
    }
    // upload user posts
    function uploadPosts() {
        $this->output->set_content_type('application/json');
        $thumb_name = null;
        $userid = $this->input->post('userid');
        $linecount = $this->input->post('linecount');
        $posttitle = $this->input->post('title');
        $photo_or_video = $this->input->post('photo_or_video'); // photo or video  [photo -> video -> 1 , other -> -1 ]
        $posts_content = $this->input->post('content');
        $poststype = $this->input->post('type');
        $posts_width = $this->input->post('posts_width');
        $posts_height = $this->input->post('posts_height');
        //$posts_tags     = $this->input->post('posts_tags');         
        if (!empty($posts_content)) {
            $hash = explode(" ", $posts_content);
            foreach ($hash as $val) {

                if ($val != "") {
                    if ($val[0] == '#') {
                        $tags[] = $val;
                    }
                } else {
                    $tags[] = null;
                }
            }
        }
        $country = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;
        $state = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;
        $area = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;
        $this->form_validation->set_rules("userid", "userid", "required");
        $this->form_validation->set_rules("photo_or_video", "photo_or_video", "required");
        if ($linecount == NULL) {
            $linecount = 0;
        }
        if ($this->form_validation->run() == TRUE) {
            if (!empty($tags)) {
                for ($i = 0; $i < sizeof($tags); $i++) {
                    $insert_tags = $this->PostsApiModel->insert_tags($tags[$i]);
                }
            }
            if ($photo_or_video == -1) {
                $insert =   array('posts_user_id' => $userid, 'photo_or_video' => $photo_or_video, 'posts_title' => $posttitle,
                                'posts_content' => $posts_content, 'posts_linecount' => $linecount, 'posts_type' => $poststype,
                                'posts_created_type' => 1, 'posts_country' => $country, 'posts_state' => $state,
                                'posts_city' => $district, 'posts_area' => $area, 'posts_tags' => implode(" ", $tags)
                            );

                $result = $this->PostsApiModel->uploadPost($insert);
                if ($result > 0) {
                    $this->output->set_output(json_encode(['message' => "successfully uploaded ", 'status' => "true"]));
                } else {
                    $this->output->set_output(json_encode(['message' => "operation failed!..", 'status' => "false"]));
                }
            } else if ($photo_or_video == 0) {
                if (isset($_FILES['photo_video'])) {
                    $errors = array();
                    $file_name      = $_FILES['photo_video']['name'];
                    $file_size      = $_FILES['photo_video']['size'];
                    $file_tmp       = $_FILES['photo_video']['tmp_name'];
                    $file_type      = $_FILES['photo_video']['type'];
                    $tmp            = explode('.', $file_name);
                    $file_ext       = end($tmp);
                    $post_name      = "posts-" . mt_rand(100000, 999999) . date('H-i-s') . '.' . $file_ext;
                    $expensions     = array("jpeg", "jpg", "png", "mp4", "3gp");
                    if (in_array($file_ext, $expensions) === false) {
                        $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
                    }
                    if (empty($errors) == true) {
                        $data['tmp_name']   = $_FILES['photo_video']['tmp_name'];
                        $data['file_name']  = $post_name;
                        $val                = $this->aws3->sendFile('daypoll/assets/images/posts', $data);
                        $insert             = array('posts_user_id' => $userid, 'photo_or_video' => $photo_or_video, 'posts_title' => $posttitle, 'posts_photo_or_video' => $post_name,
                                                    'posts_content' => $posts_content, 'posts_width' => $posts_width, 'posts_height' => $posts_height, 'posts_linecount' => $linecount,
                                                    'posts_type' => $poststype, 'posts_created_type' => 1, 'posts_country' => $country, 'posts_state' => $state,
                                                    'posts_city' => $district, 'posts_area' => $area, 'posts_tags' => $posts_tags
                                                );
                        $result = $this->PostsApiModel->uploadPost($insert);
                    }
                    if ($result > 0) {

                        $this->output->set_output(json_encode(['message' => "successfully uploaded ", 'status' => "true"]));
                    } else {
                        $this->output->set_output(json_encode(['message' => "operation failed!..", 'status' => "false"]));
                    }
                } else {
                    $this->output->set_output(json_encode(['message' => "no image found!..", 'status' => "false"]));
                }
            }
            // video upload
            else if ($photo_or_video == 1) {
                if (isset($_FILES['photo_video'])) {
                    $errors         = array();
                    $file_name      = $_FILES['photo_video']['name'];
                    $file_size      = $_FILES['photo_video']['size'];
                    $file_tmp       = $_FILES['photo_video']['tmp_name'];
                    $file_type      = $_FILES['photo_video']['type'];
                    $tmp            = explode('.', $file_name);
                    $file_ext       = end($tmp);
                    $post_name      = "posts-" . mt_rand(100000, 999999) . date('H-i-s') . '.' . $file_ext;
                    $expensions     = array("jpeg", "jpg", "png", "mp4", "3gp");
                    if (in_array($file_ext, $expensions) === false) {
                        $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
                    }
                    $flag = 0;
                    if (empty($errors) == true) {
                        $data['tmp_name']   = $_FILES['photo_video']['tmp_name'];
                        $data['file_name']  = $post_name;
                        $val                = $this->aws3->sendFile('daypoll/assets/images/posts', $data);
                        $flag               = 1;
                        if ($flag == 1) {
                            if (isset($_FILES['posts_video_thumbnail'])) {
                                $errors      = array();
                                $file_name   = $_FILES['posts_video_thumbnail']['name'];
                                $file_size   = $_FILES['posts_video_thumbnail']['size'];
                                $file_tmp    = $_FILES['posts_video_thumbnail']['tmp_name'];
                                $file_type   = $_FILES['posts_video_thumbnail']['type'];
                                $tmp         = explode('.', $file_name);
                                $file_ext    = end($tmp);
                                $posts_thumb = "posts_thumb" . mt_rand(1000, 9999) . date('H-i-s') . '.' . $file_ext;
                                $expensions = array("jpeg", "jpg", "png");
                                if (in_array($file_ext, $expensions) === false) {
                                    $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
                                }
                                if (empty($errors) == true) {
                                    $data['tmp_name']   = $_FILES['posts_video_thumbnail']['tmp_name'];
                                    $data['file_name']  = $posts_thumb;
                                    $val                = $this->aws3->sendFile('daypoll/assets/images/posts/thumbnails', $data);
                                    $thumb_name         = $posts_thumb;
                                } else {
                                    $thumb_name         = NULL;
                                }
                            }
                            $insert = array('posts_user_id' => $userid, 'photo_or_video' => $photo_or_video, 'posts_title' => $posttitle, 'posts_photo_or_video' => $post_name,
                                            'posts_content' => $posts_content, 'posts_width' => $posts_width, 'posts_height' => $posts_height, 'posts_linecount' => $linecount,
                                            'posts_type' => $poststype, 'posts_video_thumbnail' => $thumb_name, 'posts_created_type' => 1,
                                            'posts_country' => $country, 'posts_state' => $state,
                                            'posts_city' => $district, 'posts_area' => $area, 'posts_tags' => $posts_tags
                                        );
                            $result = $this->PostsApiModel->uploadPost($insert);
                        }
                        if ($result > 0) {
                            $this->output->set_output(json_encode(['message' => "successfully uploaded ", 'status' => "true"]));
                        } else {
                            $this->output->set_output(json_encode(['message' => "operation failed!..", 'status' => "false"]));
                        }
                    }
                } else {
                    $this->output->set_output(json_encode(['message' => validation_errors(), 'status' => 'false']));
                }
            }
        }
    }

    function getTags() {

        $this->output->set_content_type('application/json');
        $search     = $this->input->post('search');
        $query      = array('posts_tags_name' => $search, 'posts_tags_status' => 1);
        $res        = $this->PostsApiModel->getTags($search);
        if($res)
            $this->output->set_output(json_encode(['data' => $res, 'status' => true]));
        else
            $this->output->set_output(json_encode(['data' => "no data found", 'status' => false]));
    }

    function info() {
        echo phpinfo();
    }

    // get posts api by polls
    function getPostsbystatePolls($state) {

        $this->output->set_content_type('application/json');
        $posts_rank = $this->PostsApiModel->posts_sort_order_state();
        if ($posts_rank != null) {
            $k = 0;
            foreach ($posts_rank as $key1 => $value) {
                $res[$k] = $this->PostsApiModel->getPostsbypolls($posts_rank[$key1]['posts_polls_post_id']);
                $k++;
            }
            $data = [];
            foreach ($res as $keyss => $result) {
                if ($result) {
                    foreach ($result as $key11 => $value) {
                        $data[] = $value;
                    }
                }
            }
            foreach ($data as $a1 => $v1) {
                $data[$a1]['users_photo'] = $this->media_url . "assets/images/users/" . $data[$a1]['users_photo'];
            }
            $userid = $this->input->post('userid');
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $pollcount          = $this->PostsApiModel->pollsCount($data[$key]['posts_id']);
                    $commentcount       = $this->PostsApiModel->commentsCount($data[$key]['posts_id']);
                    $commentsCountouter = $this->PostsApiModel->commentsCountouter($data[$key]['posts_id']);
                    $is_user_polled     = $this->PostsApiModel->isUserpolled($data[$key]['posts_id'], $userid);
                    foreach ($commentcount['data'] as $co => $va) {
                        $commentcount['data'][$co]['users_photo'] = $this->media_url . "assets/images/users/" . $commentcount['data'][$co]['users_photo'];
                    }
                    $ispolled           = ($is_user_polled['count'] > 0) ? true : false;
                    $data[$key]['post_order_type'] = 1;  // 1 for posts by polls  
                    $data[$key]['is_polled'] = $ispolled;
                    $token  = mt_rand(100000, 999999);
                    $data[$key]['posts_photo_or_video'] = $this->s3_url . 'assets/images/posts/' . $data[$key]['posts_photo_or_video'];
                    $data[$key]['poll_count']           = $pollcount['count'];
                    $data[$key]['comment_count']        = $commentsCountouter;
                    $data[$key]['comment']              = $commentcount['data'];
                    $data[$key]['sharecode']            = $this->media_url . 'post-view?token=' . $token . '&&postid=' . base64_encode($data[$key]['posts_id']);
                    $data[$key]['downloadlink']         = $data[$key]['posts_photo_or_video'];
                }
                return $data;
            }
        } else {
            return null;
        }
    }

    // get party posts api by polls
    function getPartyPostsbypolls() {
        
        $this->output->set_content_type('application/json');
        $posts_rank = $this->PostsApiModel->party_posts_sort_order();
        if ($posts_rank != null) {
            $k = 0;
            foreach ($posts_rank as $key1 => $value) {
                $res[$k] = $this->PostsApiModel->getPartyPostsbypolls($posts_rank[$key1]['party_posts_polls_post_id']);
                $k++;
            }
            $data = [];
            foreach ($res as $keyss => $result) {
                if ($result) {
                    foreach ($result as $key11 => $value) {
                        $data[] = $value;
                    }
                }
            }
            foreach ($data as $a1 => $v1) {
                $data[$a1]['users_photo'] = $this->media_url . "assets/images/users/" . $data[$a1]['users_photo'];
            }
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $pollcount          = $this->PostsApiModel->partypollsCount($data[$key]['party_posts_id']);
                    $commentcount       = $this->PostsApiModel->PartycommentsCount($data[$key]['party_posts_id']);
                    $commentsCountouter = $this->PostsApiModel->PartycommentsCountouter($data[$key]['party_posts_id']);
                    $is_user_polled     = $this->PostsApiModel->PartyisUserpolled($data[$key]['party_posts_id'], $userid);
                    foreach ($commentcount['data'] as $co => $va) {
                        $commentcount['data'][$co]['users_photo'] = $this->media_url . "assets/images/users/" . $commentcount['data'][$co]['users_photo'];
                    }
                    $ispolled           = ($is_user_polled['count'] > 0) ? true : false;

                    $data[$key]['post_order_type']      = 3;  // 3 for party posts by polls  
                    $data[$key]['is_polled']            = $ispolled;
                    $token                              = mt_rand(100000, 999999);
                    $data[$key]['posts_photo_or_video'] = $this->s3_url . 'assets/images/partyposts/' . $data[$key]['party_posts_photo_video'];
                    $data[$key]['poll_count']           = $pollcount['count'];
                    $data[$key]['comment_count']        = $commentsCountouter;
                    $data[$key]['comment']              = $commentcount['data'];
                    $data[$key]['sharecode']            = $this->media_url . 'post-view?token=' . $token . '&&postid=' . base64_encode($data[$key]['party_posts_id']);
                    $data[$key]['downloadlink']         = $this->media_url . "assets/images/partyposts/" . $data[$key]['party_posts_photo_video'];
                }
                return $data;
            }
        } else {
            return null;
        }
    }

    // get posts api by polls
    function getPostsbypolls() {
        
        $this->output->set_content_type('application/json');
        $posts_rank = $this->PostsApiModel->posts_sort_order();
        if ($posts_rank != null) {
            $k = 0;
            foreach ($posts_rank as $key1 => $value) {
                $res[$k] = $this->PostsApiModel->getPostsbypolls($posts_rank[$key1]['posts_polls_post_id']);
                $k++;
            }
            $data = [];
            foreach ($res as $keyss => $result) {
                if ($result) {
                    foreach ($result as $key11 => $value) {
                        $data[] = $value;
                    }
                }
            }
            foreach ($data as $a1 => $v1) {
                $data[$a1]['users_photo'] = $this->media_url . "assets/images/users/" . $data[$a1]['users_photo'];
            }
            $userid = $this->input->post('userid');
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $pollcount          = $this->PostsApiModel->pollsCount($data[$key]['posts_id']);
                    $commentcount       = $this->PostsApiModel->commentsCount($data[$key]['posts_id']);
                    $commentsCountouter = $this->PostsApiModel->commentsCountouter($data[$key]['posts_id']);
                    $is_user_polled     = $this->PostsApiModel->isUserpolled($data[$key]['posts_id'], $userid);
                    foreach ($commentcount['data'] as $co => $va) {
                        $commentcount['data'][$co]['users_photo'] = $this->media_url . "assets/images/users/" . $commentcount['data'][$co]['users_photo'];
                    }
                    $ispolled           = ($is_user_polled['count'] > 0) ? true : false;

                    $data[$key]['post_order_type']      = 1;  // 1 for posts by polls  
                    $data[$key]['is_polled']            = $ispolled;
                    $token                              = mt_rand(100000, 999999);
                    $data[$key]['posts_photo_or_video'] = $this->s3_url . 'assets/images/posts/' . $data[$key]['posts_photo_or_video'];
                    $data[$key]['poll_count']           = $pollcount['count'];
                    $data[$key]['comment_count']        = $commentsCountouter;
                    $data[$key]['comment']              = $commentcount['data'];
                    $data[$key]['sharecode']            = $this->media_url . 'post-view?token=' . $token . '&&postid=' . base64_encode($data[$key]['posts_id']);
                    $data[$key]['downloadlink']         = $data[$key]['posts_photo_or_video'];
                }
                return $data;
            }
        } else {
            return null;
        }
    }

    // get posts by hash tag 
    function getPostsbyTags() {

        $this->output->set_content_type('application/json');
        $search = $this->input->post('search');
        $res = $this->PostsApiModel->getPostsbyTags($search);
        if ($res) {
            foreach ($res as $value => $key) {
                $res[$value]['users_name']              = $this->PostsApiModel->getusersnamebypostid($res[$value]['posts_user_id']);
                $res[$value]['users_photo']             = $this->media_url . "assets/images/users/" . $this->PostsApiModel->getusersphotobypostid($res[$value]['posts_user_id']);
                $res[$value]['posts_photo_or_video']    = $this->s3_url . "assets/images/posts/" . $res[$value]['posts_photo_or_video'];
                $res[$value]['posts_video_thumbnail']   = $this->s3_url . "assets/images/posts/thumbnails/" . $res[$value]['posts_video_thumbnail'];
                $pollCount                              = $res[$value]['total'];
                $res[$value]['current_time']            = strtotime($res[$value]['posts_uploaded_date']);
            }
            $this->output->set_output(json_encode(array('posts' => $res, 'status' => true)));
        } else
            $this->output->set_output(json_encode(array('posts' => 'no data found', 'status' => false)));
    }

    function check_isBlocked($userid) {

        return $this->PostsApiModel->check_isBlocked(array('user_id' => $userid, 'users_active' => 0));
    }

    // function get posts (news feed sort single post)
    function getPosts() {

        $this->output->set_content_type('application/json');
        $zone   = getallheaders();
        $GMT    = new DateTimeZone("GMT");
        if (empty($zone['timezone']))
            $zone['timezone'] = "Asia/Kolkata";
        
        $dt             = new DateTime();
        $current_date   = $dt->format('d-m-Y h:i A');
        $date           = new DateTime($current_date, $GMT);
        $date->setTimezone(new DateTimeZone($zone['timezone']));
        $datetime       = $date->format('Y-m-d h:i A');
        $country        = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;
        $state          = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district       = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;
        $area           = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;
        $page_start     = !empty($this->input->post('page_start')) ? $this->input->post('page_start') : 0;
        $userid         = $this->input->post('userid');
        if (empty($this->check_isBlocked($userid))) {
            $get_hide_post      = $this->PostsApiModel->gethiddenposts($userid);
            $get_report_post    = $this->PostsApiModel->getReportedposts($userid);
            $get_state_post     = $this->PostsApiModel->getStatePosts($state);          // state posts 
            $get_global_post    = $this->PostsApiModel->getGlobalPosts();              // global posts 
            $get_national_post  = $this->PostsApiModel->getNationalPosts($country);  // national posts 
            $get_district_post  = $this->PostsApiModel->getDistrictPosts($district); // district posts 
            // trending state posts starts here
            foreach ($get_state_post as $k => $v) {
                $get_state_post[$k]['users_name']               = $this->PostsApiModel->getusersnamebypostid($get_state_post[$k]['posts_user_id']);
                $get_state_post[$k]['users_photo']              = $this->media_url . "assets/images/users/" . $this->PostsApiModel->getusersphotobypostid($get_state_post[$k]['posts_user_id']);
                $get_state_post[$k]['posts_photo_or_video']     = $this->s3_url . "assets/images/posts/" . $get_state_post[$k]['posts_photo_or_video'];
                $get_state_post[$k]['posts_video_thumbnail']    = $this->s3_url . "assets/images/posts/thumbnails/" . $get_state_post[$k]['posts_video_thumbnail'];
                $datePercent = strtotime($datetime) - strtotime($get_state_post[$k]['posts_uploaded_date']);
                $pollCount                              = $get_state_post[$k]['total'];
                $get_state_post[$k]['poll_percent']     = $pollCount > 0 ? intval($datePercent / $pollCount) : 0;
                $get_state_post[$k]['sort_order_key']   = "state post";
                $get_state_post[$k]['current_time']     = strtotime($get_state_post[$k]['posts_uploaded_date']);
            }
            foreach ($get_state_post as $res_1 => $key_1) {

                $sort_array_state[$key_1['poll_percent']] = $get_state_post[$res_1];
            }
            ksort($sort_array_state);
            foreach ($sort_array_state as $final_last_array_state[])
            $hide_array_state       = [];
            foreach ($get_hide_post as $row) {
                $hide_array_state[] = $row['hide_posts_post_id'];
            }
            $report_array_state     = [];
            foreach ($get_report_post as $row) {
                $report_array_state[] = $row['posts_report_post_id'];
            }
            $final_array_state      = [];
            foreach ($final_last_array_state as $in_row) {
                if (!in_array($in_row['posts_id'], $hide_array_state)) {
                    $final_array_state[] = $in_row;
                }
            }
            $sort_array_state_final = [];
            foreach ($final_array_state as $in_row) {
                if (!in_array($in_row['posts_id'], $report_array_state)) {
                    $sort_array_state_final[] = $in_row;
                }
            }
            foreach ($get_global_post as $k => $v) { 
                $get_global_post[$k]['users_name']              = $this->PostsApiModel->getusersnamebypostid($get_global_post[$k]['posts_user_id']);
                $get_global_post[$k]['users_photo']             = $this->media_url . "assets/images/users/" . $this->PostsApiModel->getusersphotobypostid($get_global_post[$k]['posts_user_id']);
                $get_global_post[$k]['posts_photo_or_video']    = $this->s3_url . "assets/images/posts/" . $get_global_post[$k]['posts_photo_or_video'];
                $get_global_post[$k]['posts_video_thumbnail']   = $this->s3_url . "assets/images/posts/thumbnails/" . $get_global_post[$k]['posts_video_thumbnail'];
                $datePercent                                    = strtotime($datetime) - strtotime($get_global_post[$k]['posts_uploaded_date']);
                $pollCount                                      = $get_global_post[$k]['total'];
                $get_global_post[$k]['poll_percent']            = $pollCount > 0 ? intval($datePercent / $pollCount) : 0;
                $get_global_post[$k]['sort_order_key']          = "global post";
            }
            foreach ($get_global_post as $res_1 => $key_1) {
                $sort_array_global[$key_1['poll_percent']] = $get_global_post[$res_1];
            }
            ksort($sort_array_global);
            foreach ($sort_array_global as $final_last_array_global[])
            $hide_array_global       = [];
            foreach ($get_hide_post as $row) {
                $hide_array_global[] = $row['hide_posts_post_id'];
            }
            $report_array_global       = [];
            foreach ($get_report_post as $row) {
                $report_array_global[] = $row['posts_report_post_id'];
            }
            $final_array_global           = [];
            foreach ($final_last_array_global as $in_row) {
                if (!in_array($in_row['posts_id'], $hide_array_global)) {
                    $final_array_global[] = $in_row;
                }
            }
            $sort_array_global_final = [];
            foreach ($final_array_global as $in_row) {
                if (!in_array($in_row['posts_id'], $report_array_global)) {
                    $sort_array_global_final[] = $in_row;
                }
            }
            foreach ($get_national_post as $k => $v) { 
                $get_national_post[$k]['users_name']                = $this->PostsApiModel->getusersnamebypostid($get_national_post[$k]['posts_user_id']);
                $get_national_post[$k]['users_photo']               = $this->media_url . "assets/images/users/" . $this->PostsApiModel->getusersphotobypostid($get_national_post[$k]['posts_user_id']);
                $get_national_post[$k]['posts_photo_or_video']      = $this->s3_url . "assets/images/posts/" . $get_national_post[$k]['posts_photo_or_video'];
                $get_national_post[$k]['posts_video_thumbnail']     = $this->s3_url . "assets/images/posts/thumbnails/" . $get_national_post[$k]['posts_video_thumbnail'];
                $datePercent                                        = strtotime($datetime) - strtotime($get_national_post[$k]['posts_uploaded_date']);
                $pollCount                                          = $get_national_post[$k]['total'];
                $get_national_post[$k]['poll_percent']              = $pollCount > 0 ? intval($datePercent / $pollCount) : 0;
                $get_national_post[$k]['sort_order_key']            = "national post";
            }
            foreach ($get_national_post as $res_1 => $key_1) {
                $sort_array_national[$key_1['poll_percent']] = $get_national_post[$res_1];
            }
            ksort($sort_array_national);
            foreach ($sort_array_national as $final_last_array_national[])
            $hide_array_national        = [];
            foreach ($get_hide_post as $row) {
                $hide_array_national[]  = $row['hide_posts_post_id'];
            }
            $report_array_national       = [];
            foreach ($get_report_post as $row) {
                $report_array_national[] = $row['posts_report_post_id'];
            }
            $final_array_national = [];
            foreach ($final_last_array_national as $in_row) {
                if (!in_array($in_row['posts_id'], $hide_array_national)) {
                    $final_array_national[] = $in_row;
                }
            }
            $sort_array_national_final = [];
            foreach ($final_array_national as $in_row) {
                if (!in_array($in_row['posts_id'], $report_array_national)) {
                    $sort_array_national_final[] = $in_row;
                }
            }
            foreach ($get_district_post as $k => $v) {
                $get_district_post[$k]['users_name']            = $this->PostsApiModel->getusersnamebypostid($get_district_post[$k]['posts_user_id']);
                $get_district_post[$k]['users_photo']           = $this->media_url . "assets/images/users/" . $this->PostsApiModel->getusersphotobypostid($get_district_post[$k]['posts_user_id']);
                $get_district_post[$k]['posts_photo_or_video']  = $this->s3_url . "assets/images/posts/" . $get_district_post[$k]['posts_photo_or_video'];
                $get_district_post[$k]['posts_video_thumbnail'] = $this->s3_url . "assets/images/posts/thumbnails/" . $get_district_post[$k]['posts_video_thumbnail'];
                $datePercent                                    = strtotime($datetime) - strtotime($get_district_post[$k]['posts_uploaded_date']);
                $pollCount                                      = $get_district_post[$k]['total'];
                $get_district_post[$k]['poll_percent']          = $pollCount > 0 ? intval($datePercent / $pollCount) : 0;
                $get_district_post[$k]['sort_order_key']        = "district post";
            }
            foreach ($get_district_post as $res_1 => $key_1) {
                $sort_array_district[$key_1['poll_percent']] = $get_district_post[$res_1];
            }
            ksort($sort_array_district);
            foreach ($sort_array_district as $final_last_array_district[])
            $hide_array_district       = [];
            foreach ($get_hide_post as $row) {
                $hide_array_district[] = $row['hide_posts_post_id'];
            }

            $report_array_district = [];
            foreach ($get_report_post as $row) {
                $report_array_district[] = $row['posts_report_post_id'];
            }
            $final_array_district = [];
            foreach ($final_last_array_district as $in_row) {
                if (!in_array($in_row['posts_id'], $hide_array_district)) {
                    $final_array_district[] = $in_row;
                }
            }
            $sort_array_district_final = [];
            foreach ($final_array_district as $in_row) {
                if (!in_array($in_row['posts_id'], $report_array_district)) {
                    $sort_array_district_final[] = $in_row;
                }
            }
            // friends posts starts here
            $getFriends         = $this->PostsApiModel->getFriends($userid);
            $array_count        = count($getFriends);
            $getFriends[$array_count]['friend_id'] = $userid;
            if ($getFriends) {
                foreach ($getFriends as $value => $key) {
                    $res_data = $this->PostsApiModel->getPosts($getFriends[$value]['friend_id']);
                    foreach ($res_data['data'] as $row) {
                        $res['datas'][] = $row;
                    }
                }
            }
            foreach ($res['datas'] as $val => $k) {
                $res['datas'][$val]['post_order_type']       = "friend post";  // 2 for friends posts
                $res['datas'][$val]['users_photo']           = $this->media_url . "assets/images/users/" . $res['datas'][$val]['users_photo'];
                $res['datas'][$val]['posts_photo_or_video']  = $this->s3_url . "assets/images/posts/" . $res['datas'][$val]['posts_photo_or_video'];
                $res['datas'][$val]['posts_video_thumbnail'] = $this->s3_url . "assets/images/posts/thumbnails/" . $res['datas'][$val]['posts_video_thumbnail'];
            }
            $i = 0;
            foreach ($res['datas'] as $row) {
                $total_array[$i] = $row['posts_uploaded_date'];
                $i++;
            }
            arsort($total_array);
            foreach ($total_array as $key => $value) {
                $final_last_array_friend[] = $res['datas'][$key];
            }
            $hide_array_friend = [];
            foreach ($get_hide_post as $row) {
                $hide_array_friend[] = $row['hide_posts_post_id'];
            }
            $report_array_friend = [];
            foreach ($get_report_post as $row) {
                $report_array_friend[] = $row['posts_report_post_id'];
            }
            $final_array_friend = [];
            foreach ($final_last_array_friend as $in_row) {
                if (!in_array($in_row['posts_id'], $hide_array_friend)) {
                    $final_array_friend[] = $in_row;
                }
            }
            $friends_post_final = [];
            foreach ($final_array_friend as $in_row) {
                if (!in_array($in_row['posts_id'], $report_array_friend)) {
                    $friends_post_final[] = $in_row;
                }
            }
            $page_number    = $page_start;
            $per_page       = 10;
            $state_arryay_length        = $total_post_count_state    = sizeof($sort_array_state_final);
            $friends_arryay_length      = $total_post_count_friend   = sizeof($friends_post_final);
            $global_arryay_length       = $total_post_count_global   = sizeof($sort_array_global_final);
            $national_arryay_length     = $total_post_count_national = sizeof($sort_array_national_final);
            $district_arryay_length     = $total_post_count_district = sizeof($sort_array_district_final);
            $total_length               = $state_arryay_length + $friends_arryay_length + $global_arryay_length + $natioanl_arryay_length + $district_arryay_length;
            $total_unique_post_count    = $this->totalUniquePostCount($sort_array_state_final, $friends_post_final, $sort_array_global_final, $sort_array_national_final, $sort_array_district_final, $state_arryay_length, $friends_arryay_length, $global_arryay_length, $national_arryay_length, $district_arryay_length);
            $total_posts                = $state_arryay_length + $friends_arryay_length + $global_arryay_length + $national_arryay_length + $district_arryay_length;
            $p = $q = 2;
            $r = $s = $t = $total = 1;
            $state_array_count = $friends_array_count = $global_array_count = $national_array_count = $district_array_count = 0;
            $post_id_array     = [];
            $total_allocation  = ($page_number * $per_page);
            $reapeatingArray   = []; 
            $result_array = [];
            for ($i = 1; $i <= ($total_allocation - 10); $i++) {
                if ($p > 0 && $friends_arryay_length > 0) {   //state_arryay_length
                    if (!in_array($friends_post_final[$friends_array_count]['posts_id'], $post_id_array)) {
                        $post_id_array[]    = $friends_post_final[$friends_array_count]['posts_id'];
                        $result_array[]     = $friends_post_final[$friends_array_count];
                        $keyValue[$i]['id']     =  $friends_post_final[$friends_array_count]['posts_id'];  
                        $keyValue[$i]['value']  =  $friends_post_final[$friends_array_count]['post_order_type'];  
                        $p--;
                    } else {
                        $total_post_count_friend--;
                        $total_allocation++;
                    }
                    $friends_arryay_length--;
                    $friends_array_count++;
                } else if ($q > 0 && $state_arryay_length > 0) {
                    if (!in_array($sort_array_state_final[$state_array_count]['posts_id'], $post_id_array)) {
                        $post_id_array []   = $sort_array_state_final[$state_array_count]['posts_id'];
                        $result_array[]     = $sort_array_state_final[$state_array_count];
                        $keyValue[$i]['id']     =  $sort_array_state_final[$state_array_count]['posts_id'];  
                        $keyValue[$i]['value']  =  $sort_array_state_final[$state_array_count]['sort_order_key'];  
                        $q--;
                    } else {
                        $total_post_count_state--;
                        $total_allocation++;
                    }
                    $state_arryay_length--;
                    $state_array_count++;
                } else if ($r > 0 && $global_arryay_length > 0) {
                    if (!in_array($sort_array_global_final[$global_array_count]['posts_id'], $post_id_array)) {
                        $post_id_array []   = $sort_array_global_final[$global_array_count]['posts_id'];
                        $result_array[]     = $sort_array_global_final[$global_array_count];
                        $keyValue[$i]['id']     =  $sort_array_global_final[$global_array_count]['posts_id'];  
                        $keyValue[$i]['value']  =  $sort_array_global_final[$global_array_count]['sort_order_key'];  
                        $r--;
                    } else {
                        $total_post_count_global--;
                        $total_allocation++;
                    }
                    $global_arryay_length--;
                    $global_array_count++;
                } else if ($s > 0 && $national_arryay_length > 0) {
                    if (!in_array($sort_array_national_final[$national_array_count]['posts_id'], $post_id_array)) { 
                        $post_id_array []   = $sort_array_national_final[$national_array_count]['posts_id'];
                        $result_array[]     = $sort_array_national_final[$national_array_count];
                        $keyValue[$i]['id']     =  $sort_array_national_final[$national_array_count]['posts_id'];  
                        $keyValue[$i]['value']  =  $sort_array_national_final[$national_array_count]['sort_order_key'];  
                        $s--;
                    } else {
                        $total_post_count_national--;
                        $total_allocation++;
                    }
                    $national_arryay_length--;
                    $national_array_count++;
                } else if ($t > 0 && $district_arryay_length > 0) {
                    if (!in_array($sort_array_district_final[$district_array_count]['posts_id'], $post_id_array)) {
                        $post_id_array []   = $sort_array_district_final[$district_array_count]['posts_id'];
                        $result_array[]     = $sort_array_district_final[$district_array_count];
                         $keyValue[$i]['id']     =  $sort_array_district_final[$district_array_count]['posts_id'];  
                        $keyValue[$i]['value']  =  $sort_array_district_final[$district_array_count]['sort_order_key'];  
                        $t--;
                    } else {
                        $total_post_count_district--;
                        $total_allocation++;
                    }
                    $district_arryay_length--;
                    $district_array_count++;
                }
//                $total++;
//                if ($total == 10) {
//                    $p = $q = 2;
//                    $r = $s = $t = 1;
//                } else {
                    if ($p == 0 && $state_arryay_length == 0 && $global_arryay_length == 0 && $national_arryay_length == 0 && $district_arryay_length == 0) {
                        $p = 2;
                    } else if ($q == 0 && $global_arryay_length == 0 && $national_arryay_length == 0 && $district_arryay_length == 0) {
                        $p = $q = 2;
                    } else if ($r == 0 && $national_arryay_length == 0 && $district_arryay_length == 0) {
                        $r = 1;
                        $p = $q = 2;
                    } else if ($s == 0 && $district_arryay_length == 0) {
                        $s = $r = 1;
                        $p = $q = 2;
                    } else if ($t == 0) {
                        $t = $s = $r = 1;
                        $p = $q = 2;
                    }
//                }
            }
            $keyValue = [];
            $result_array = [];
            if($page_number == 1){
                $p = $q = 2;
                $r = $s = $t = 1;
            }
            $total_allocate_count = 10;
            for ($i = 1; $i <= $total_allocate_count; $i++) {
                if ($p > 0 && $friends_arryay_length > 0) {
                    if (!in_array($friends_post_final[$friends_array_count]['posts_id'], $post_id_array)) {
                        $post_id_array []   = $friends_post_final[$friends_array_count]['posts_id'];
                        $result_array[]     = $friends_post_final[$friends_array_count];
                        $keyValue[$i]['id']     =  $friends_post_final[$friends_array_count]['posts_id'];  
                        $keyValue[$i]['value']  =  $friends_post_final[$friends_array_count]['post_order_type'];  
                        $p--;
                    } else {
                        $total_post_count_friend--;
                        $total_allocate_count++;
                    }
                    $friends_arryay_length--;
                    $friends_array_count++;
                } elseif ($q > 0 && $state_arryay_length > 0) {
                    if (!in_array($sort_array_state_final[$state_array_count]['posts_id'], $post_id_array)) {
                        $post_id_array []   = $sort_array_state_final[$state_array_count]['posts_id'];
                        $result_array[]     = $sort_array_state_final[$state_array_count];
                         $keyValue[$i]['id']     =  $sort_array_state_final[$state_array_count]['posts_id'];  
                        $keyValue[$i]['value']  =  $sort_array_state_final[$state_array_count]['sort_order_key'];  
                        $q--;
                    } else {
                        $total_post_count_state--;
                        $total_allocate_count++;
                    }
                    $state_arryay_length--;
                    $state_array_count++;
                }
                else if ($r > 0 && $global_arryay_length > 0) {
                    if (!in_array($sort_array_global_final[$global_array_count]['posts_id'], $post_id_array)) {
                        $post_id_array[]    = $sort_array_global_final[$global_array_count]['posts_id'];
                        $result_array[]     = $sort_array_global_final[$global_array_count];
                         $keyValue[$i]['id']     =  $sort_array_global_final[$global_array_count]['posts_id'];  
                        $keyValue[$i]['value']  =  $sort_array_global_final[$global_array_count]['sort_order_key'];  
                        $r--;
                    } else {
                        $total_post_count_global--;
                        $total_allocate_count++;
                    }
                    $global_arryay_length--;
                    $global_array_count++;
                } else if ($s > 0 && $national_arryay_length > 0) {
                    if (!in_array($sort_array_national_final[$national_array_count]['posts_id'], $post_id_array)) {
                        $post_id_array []   = $sort_array_national_final[$national_array_count]['posts_id'];
                        $result_array[]     = $sort_array_national_final[$national_array_count];
                         $keyValue[$i]['id']     =  $sort_array_national_final[$national_array_count]['posts_id'];  
                        $keyValue[$i]['value']  =  $sort_array_national_final[$national_array_count]['sort_order_key'];  
                        $s--;
                    } else {
                        $total_post_count_national--;
                        $total_allocate_count++;
                    }
                    $national_arryay_length--;
                    $national_array_count++;
                } else if ($t > 0 && $district_arryay_length > 0) {
                    if (!in_array($sort_array_district_final[$district_array_count]['posts_id'], $post_id_array)) {
                        $post_id_array []   = $sort_array_district_final[$district_array_count]['posts_id'];
                        $result_array[]     = $sort_array_district_final[$district_array_count];
                         $keyValue[$i]['id']     =  $sort_array_district_final[$district_array_count]['posts_id'];  
                        $keyValue[$i]['value']  =  $sort_array_district_final[$district_array_count]['sort_order_key']; 
                        $t--;
                    } else {
                        $total_post_count_district--;
                        $total_allocate_count++;
                    }
                    $district_arryay_length--;
                    $district_array_count++;
                }
                if ($p == 0  && $state_arryay_length == 0 && $global_arryay_length == 0 && $national_arryay_length == 0 && $district_arryay_length == 0) {
                    $p = 2;
                } else if ($q == 0 && $global_arryay_length == 0 && $national_arryay_length == 0 && $district_arryay_length == 0) {
                    $p = $q = 2;
                } else if ($r == 0 && $national_arryay_length == 0 && $district_arryay_length == 0) {
                    $r = 1;
                    $p = $q = 2;
                } else if ($s == 0 && $district_arryay_length == 0) {
                    $s = $r = 1;
                    $p = $q = 2;
                } else if ($t == 0) {
                    $t = $s = $r = 1;
                    $p = $q = 2;
                }
            }
            print_r($keyValue);
            exit();
            $next_array = $this->check_get_posts_next_array($userid, $page_start, $country, $state, $district, $area);
            if (!empty($result_array)) {
                foreach ($result_array as $key => $val) {
                    $pollcount          = $this->PostsApiModel->pollsCount($result_array[$key]['posts_id']);
                    $commentcount       = $this->PostsApiModel->commentsCount($result_array[$key]['posts_id']);
                    $commentsCountouter = $this->PostsApiModel->commentsCountouter($result_array[$key]['posts_id']);
                    $is_user_polled     = $this->PostsApiModel->isUserpolled($result_array[$key]['posts_id'], $userid);
                    foreach ($commentcount['data'] as $co => $va) {
                        $commentcount['data'][$co]['users_photo'] = $this->media_url . "assets/images/users/" . $commentcount['data'][$co]['users_photo'];
                    }
                    $ispolled           = ($is_user_polled['count'] > 0) ? true : false;
                    $result_array[$key]['is_polled']           = $ispolled;
                    $token                                     = mt_rand(100000, 999999);
                    $result_array[$key]['poll_count']          = $pollcount['count'];
                    $result_array[$key]['comment_count']       = $commentsCountouter;
                    $result_array[$key]['comment']             = $commentcount['data'];
                    $date = new DateTime($result_array[$key]['posts_uploaded_date'], $GMT);
                    $date->setTimezone(new DateTimeZone($zone['timezone']));
                    $result_array[$key]['posts_uploaded_date'] = $date->format('Y-m-d H:i:s');
                    $result_array[$key]['sharecode']           = $this->media_url . 'post-view?postid=' . $result_array[$key]['posts_id'] . '&&type=0';
                    $result_array[$key]['downloadlink']        = $result_array[$key]['posts_photo_or_video'] . '?postid=' . $result_array[$key]['posts_id'] . '&&type=0';
                }
                $this->output->set_output(json_encode(array('hidden_post_id' => $get_hide_post, 'report_post_id' => $get_report_post, 'posts' => $result_array, 'friends' => $getFriends, 'is_completed' => $next_array > 0 ? 0 : 1, 'posts_count_old' => $total_posts, 'posts_count' => $total_unique_post_count, 'status' => true)));
            } else {
                $this->output->set_output(json_encode(array('posts' => 'no data found on server', 'status' => false)));
            }
        } else {
            $this->output->set_output(json_encode(array('posts' => 'you are blocked by admin', 'status' => false)));
        }
    }

    function returngetPostsnew($userid, $postid, $type, $country, $state, $district, $area, $page_start) {

        $this->output->set_content_type('application/json');
        $zone       = getallheaders();
        $GMT        = new DateTimeZone("GMT");
        if (empty($zone['timezone']))
            $zone['timezone'] = "Asia/Kolkata";

        $country    = (!empty($country)) ? $country : NULL;
        $state      = (!empty($state)) ? $state : NULL;
        $district   = (!empty($district)) ? $district : NULL;
        $area       = (!empty($area)) ? $area : NULL;
        $page_start = !empty($page_start) ? $page_start + 1 : 0;
        $date       = new DateTime(date("Y-m-d H:i:s", strtotime("now")), $GMT);
        $date->setTimezone(new DateTimeZone($zone['timezone']));
        $datetime   = $date->format('Y-m-d H:i:s');

        $get_hide_post      = $this->PostsApiModel->gethiddenposts($userid);
        $get_report_post    = $this->PostsApiModel->getReportedposts($userid);
        $get_state_post     = $this->PostsApiModel->getStatePosts($state);       // state posts 
        $get_global_post    = $this->PostsApiModel->getGlobalPosts();            // global   posts 
        $get_national_post  = $this->PostsApiModel->getNationalPosts($country);  // national posts 
        $get_district_post  = $this->PostsApiModel->getDistrictPosts($district); // district posts 
        // trending state posts starts here
        foreach ($get_state_post as $k => $v) {
            $uploaddate         = new DateTime($get_state_post[$k]['posts_uploaded_date'], $GMT);
            $uploaddate->setTimezone(new DateTimeZone($zone['timezone']));
            $get_state_post[$k]['posts_uploaded_date']   = $uploaddate->format('Y-m-d H:i:s');
            $get_state_post[$k]['users_name']            = $this->PostsApiModel->getusersnamebypostid($get_state_post[$k]['posts_user_id']);
            $get_state_post[$k]['users_photo']           = $this->media_url . "assets/images/users/" . $this->PostsApiModel->getusersphotobypostid($get_state_post[$k]['posts_user_id']);
            $get_state_post[$k]['posts_photo_or_video']  = $this->s3_url . "assets/images/posts/" . $get_state_post[$k]['posts_photo_or_video'];

            $get_state_post[$k]['posts_video_thumbnail'] = $this->s3_url . "assets/images/posts/thumbnails/" . $get_state_post[$k]['posts_video_thumbnail'];
            $datePercent                                 = strtotime($datetime) - strtotime($uploaddate);
            $pollCount                                   = $get_state_post[$k]['total'];
            $get_state_post[$k]['poll_percent']          = $pollCount > 0 ? intval($datePercent / $pollCount) : 0;
            $get_state_post[$k]['sort_order_key']        = "state post";
        }
        foreach ($get_state_post as $res_1 => $key_1) {
            $sort_array_state[$key_1['poll_percent']] = $get_state_post[$res_1];
        }
        ksort($sort_array_state);
        foreach ($sort_array_state as $final_last_array_state[])
        $hide_array_state = [];
        foreach ($get_hide_post as $row) {
            $hide_array_state[] = $row['hide_posts_post_id'];
        }
        $report_array_state = [];
        foreach ($get_report_post as $row) {
            $report_array_state[] = $row['posts_report_post_id'];
        }
        $final_array_state = [];
        foreach ($final_last_array_state as $in_row) {
            if (!in_array($in_row['posts_id'], $hide_array_state)) {
                $final_array_state[] = $in_row;
            }
        }
        $sort_array_state_final = [];
        foreach ($final_array_state as $in_row) {
            if (!in_array($in_row['posts_id'], $report_array_state)) {
                $sort_array_state_final[] = $in_row;
            }
        }
        //trending global posts
        foreach ($get_global_post as $k => $v) {

            $uploaddate         = new DateTime($get_global_post[$k]['posts_uploaded_date'], $GMT);
            $uploaddate->setTimezone(new DateTimeZone($zone['timezone']));
            $get_global_post[$k]['posts_uploaded_date']   = $uploaddate->format('Y-m-d H:i:s');
            $get_global_post[$k]['users_name']            = $this->PostsApiModel->getusersnamebypostid($get_global_post[$k]['posts_user_id']);
            $get_global_post[$k]['users_photo']           = $this->media_url . "assets/images/users/" . $this->PostsApiModel->getusersphotobypostid($get_global_post[$k]['posts_user_id']);
            $get_global_post[$k]['posts_photo_or_video']  = $this->s3_url . "assets/images/posts/" . $get_global_post[$k]['posts_photo_or_video'];
            $get_global_post[$k]['posts_video_thumbnail'] = $this->s3_url . "assets/images/posts/thumbnails/" . $get_global_post[$k]['posts_video_thumbnail'];
            $datePercent                                  = strtotime($datetime) - strtotime($get_global_post[$k]['posts_uploaded_date']);
            $pollCount                                    = $get_global_post[$k]['total'];
            $get_global_post[$k]['poll_percent']          = $pollCount > 0 ? intval($datePercent / $pollCount) : 0;
            $get_global_post[$k]['sort_order_key']        = "global post";
        }
        foreach ($get_global_post as $res_1 => $key_1) {
            $sort_array_global[$key_1['poll_percent']] = $get_global_post[$res_1];
        }
        ksort($sort_array_global);
        foreach ($sort_array_global as $final_last_array_global[])
        $hide_array_global       = [];
        foreach ($get_hide_post as $row) {
            $hide_array_global[] = $row['hide_posts_post_id'];
        }
        $report_array_global     = [];
        foreach ($get_report_post as $row) {
            $report_array_global[] = $row['posts_report_post_id'];
        }
        $final_array_global     = [];
        foreach ($final_last_array_global as $in_row) {
            if (!in_array($in_row['posts_id'], $hide_array_global)) {
                $final_array_global[] = $in_row;
            }
        }
        $sort_array_global_final = [];
        foreach ($final_array_global as $in_row) {
            if (!in_array($in_row['posts_id'], $report_array_global)) {
                $sort_array_global_final[] = $in_row;
            }
        }
        //trending national posts
        foreach ($get_national_post as $k => $v) {
            $uploaddate             = new DateTime($get_national_post[$k]['posts_uploaded_date'], $GMT);
            $uploaddate->setTimezone(new DateTimeZone($zone['timezone']));
            $get_national_post[$k]['posts_uploaded_date']   = $uploaddate->format('Y-m-d H:i:s');
            $get_national_post[$k]['users_name']            = $this->PostsApiModel->getusersnamebypostid($get_national_post[$k]['posts_user_id']);
            $get_national_post[$k]['users_photo']           = $this->media_url . "assets/images/users/" . $this->PostsApiModel->getusersphotobypostid($get_national_post[$k]['posts_user_id']);
            $get_national_post[$k]['posts_photo_or_video']  = $this->s3_url . "assets/images/posts/" . $get_national_post[$k]['posts_photo_or_video'];
            $get_national_post[$k]['posts_video_thumbnail'] = $this->s3_url . "assets/images/posts/thumbnails/" . $get_national_post[$k]['posts_video_thumbnail'];
            $datePercent                                    = strtotime($datetime) - strtotime($get_national_post[$k]['posts_uploaded_date']);
            $pollCount                                      = $get_national_post[$k]['total'];
            $get_national_post[$k]['poll_percent']          = $pollCount > 0 ? intval($datePercent / $pollCount) : 0;
            $get_national_post[$k]['sort_order_key']        = "national post";
        }
        foreach ($get_national_post as $res_1 => $key_1) {
            $sort_array_national[$key_1['poll_percent']] = $get_national_post[$res_1];
        }
        ksort($sort_array_national);
        foreach ($sort_array_national as $final_last_array_national[])
            
        $hide_array_national = [];
        foreach ($get_hide_post as $row) {
            $hide_array_national[] = $row['hide_posts_post_id'];
        }
        $report_array_national = [];
        foreach ($get_report_post as $row) {
            $report_array_national[] = $row['posts_report_post_id'];
        }
        $final_array_national = [];
        foreach ($final_last_array_national as $in_row) {
            if (!in_array($in_row['posts_id'], $hide_array_national)) {
                $final_array_national[] = $in_row;
            }
        }
        $sort_array_national_final = [];
        foreach ($final_array_national as $in_row) {
            if (!in_array($in_row['posts_id'], $report_array_national)) {
                $sort_array_national_final[] = $in_row;
            }
        }
        //trending district posts
        foreach ($get_district_post as $k => $v) {

            $uploaddate         = new DateTime($get_district_post[$k]['posts_uploaded_date'], $GMT);
            $uploaddate->setTimezone(new DateTimeZone($zone['timezone']));
            $get_district_post[$k]['posts_uploaded_date']   = $uploaddate->format('Y-m-d H:i:s');
            $get_district_post[$k]['users_name']            = $this->PostsApiModel->getusersnamebypostid($get_district_post[$k]['posts_user_id']);
            $get_district_post[$k]['users_photo']           = $this->media_url . "assets/images/users/" . $this->PostsApiModel->getusersphotobypostid($get_district_post[$k]['posts_user_id']);
            $get_district_post[$k]['posts_photo_or_video']  = $this->s3_url . "assets/images/posts/" . $get_district_post[$k]['posts_photo_or_video'];
            $get_district_post[$k]['posts_video_thumbnail'] = $this->s3_url . "assets/images/posts/thumbnails/" . $get_district_post[$k]['posts_video_thumbnail'];
            $datePercent                                    = strtotime($datetime) - strtotime($get_district_post[$k]['posts_uploaded_date']);
            $pollCount                                      = $get_district_post[$k]['total'];
            $get_district_post[$k]['poll_percent']          = $pollCount > 0 ? intval($datePercent / $pollCount) : 0;
            $get_district_post[$k]['sort_order_key']        = "district post";
        }
        foreach ($get_district_post as $res_1 => $key_1) {
            $sort_array_district[$key_1['poll_percent']] = $get_district_post[$res_1];
        }
        ksort($sort_array_district);
        foreach ($sort_array_district as $final_last_array_district[])
        $hide_array_district = [];
        foreach ($get_hide_post as $row) {
            $hide_array_district[] = $row['hide_posts_post_id'];
        }
        $report_array_district = [];
        foreach ($get_report_post as $row) {
            $report_array_district[] = $row['posts_report_post_id'];
        }
        $final_array_district = [];
        foreach ($final_last_array_district as $in_row) {
            if (!in_array($in_row['posts_id'], $hide_array_district)) {
                $final_array_district[] = $in_row;
            }
        }
        $sort_array_district_final = [];
        foreach ($final_array_district as $in_row) {
            if (!in_array($in_row['posts_id'], $report_array_district)) {
                $sort_array_district_final[] = $in_row;
            }
        }
        // friends posts starts here
        $getFriends                            = $this->PostsApiModel->getFriends($userid);
        $array_count                           = count($getFriends);
        $getFriends[$array_count]['friend_id'] = $userid;
        if ($getFriends) {
            foreach ($getFriends as $value => $key) {
                $res_data = $this->PostsApiModel->getPosts($getFriends[$value]['friend_id']);
                foreach ($res_data['data'] as $row) {
                    $res['datas'][] = $row;
                }
            }
        }
        foreach ($res['datas'] as $val => $k) {
            $res['datas'][$val]['post_order_type'] = "friend post";  // 2 for friends posts
            $res['datas'][$val]['users_photo'] = $this->media_url . "assets/images/users/" . $res['datas'][$val]['users_photo'];
            $res['datas'][$val]['posts_photo_or_video'] = $this->s3_url . "assets/images/posts/" . $res['datas'][$val]['posts_photo_or_video'];
            $res['datas'][$val]['posts_video_thumbnail'] = $this->s3_url . "assets/images/posts/thumbnails/" . $res['datas'][$val]['posts_video_thumbnail'];
        }
        $i = 0;
        foreach ($res['datas'] as $row) {
            $total_array[$i] = $row['posts_uploaded_date'];
            $i++;
        }
        arsort($total_array);
        foreach ($total_array as $key => $value) {
            $final_last_array_friend[] = $res['datas'][$key];
        }
        $hide_array_friend = [];
        foreach ($get_hide_post as $row) {
            $hide_array_friend[] = $row['hide_posts_post_id'];
        }
        $report_array_friend = [];
        foreach ($get_report_post as $row) {
            $report_array_friend[] = $row['posts_report_post_id'];
        }
        $final_array_friend = [];
        foreach ($final_last_array_friend as $in_row) {
            if (!in_array($in_row['posts_id'], $hide_array_friend)) {
                $final_array_friend[] = $in_row;
            }
        }
        $friends_post_final = [];
        foreach ($final_array_friend as $in_row) {
            if (!in_array($in_row['posts_id'], $report_array_friend)) {
                $friends_post_final[] = $in_row;
            }
        }
        $page_number             = $page_start;
        $per_page                = 10;
        $state_arryay_length     = $total_post_count_state = sizeof($sort_array_state_final);
        $friends_arryay_length   = $total_post_count_friend = sizeof($friends_post_final);
        $global_arryay_length    = $total_post_count_global = sizeof($sort_array_global_final);
        $national_arryay_length  = $total_post_count_national = sizeof($sort_array_national_final);
        $district_arryay_length  = $total_post_count_district = sizeof($sort_array_district_final);
        $total_length            = $state_arryay_length + $friends_arryay_length + $global_arryay_length + $natioanl_arryay_length + $district_arryay_length;
        $total_unique_post_count = $this->totalUniquePostCount($sort_array_state_final, $friends_post_final, $sort_array_global_final, $sort_array_national_final, $sort_array_district_final, $state_arryay_length, $friends_arryay_length, $global_arryay_length, $national_arryay_length, $district_arryay_length);
        $total_posts             = $state_arryay_length + $friends_arryay_length + $global_arryay_length + $national_arryay_length + $district_arryay_length;
        $p = $q = 2;
        $r = $s = $t = $total = 1;
        $state_array_count = $friends_array_count = $global_array_count = $national_array_count = $district_array_count = 0;
        $post_id_array = [];
        $total_allocation = ($page_number * $per_page);
        for ($i = 1; $i <= ($total_allocation - 10); $i++) {
            if ($p > 0 && $friends_arryay_length > 0) {            //state_arryay_length
                if (!in_array($friends_post_final[$friends_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array[] = $friends_post_final[$friends_array_count]['posts_id'];
                    $p--;
                } else {
                    $total_post_count_friend--;
                    $total_allocation++;
                }
                $friends_arryay_length--;
                $friends_array_count++;
            } else if ($q > 0 && $state_arryay_length > 0) {
                if (!in_array($sort_array_state_final[$state_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array [] = $sort_array_state_final[$state_array_count]['posts_id'];
                    $q--;
                } else {
                    $total_post_count_friend--;
                    $total_allocation++;
                }
                $state_arryay_length--;
                $state_array_count++;
            } else if ($r > 0 && $global_arryay_length > 0) {
                if (!in_array($sort_array_global_final[$global_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array [] = $sort_array_global_final[$global_array_count]['posts_id'];
                    $r--;
                } else {
                    $total_post_count_global--;
                    $total_allocation++;
                }
                $global_arryay_length--;
                $global_array_count++;
            } else if ($s > 0 && $national_arryay_length > 0) {
                if (!in_array($sort_array_national_final[$national_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array [] = $sort_array_national_final[$national_array_count]['posts_id'];
                    $s--;
                } else {
                    $total_post_count_national--;
                    $total_allocation++;
                }
                $national_arryay_length--;
                $national_array_count++;
            } else if ($t > 0 && $district_arryay_length > 0) {
                if (!in_array($sort_array_district_final[$district_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array [] = $sort_array_district_final[$district_array_count]['posts_id'];
                    $t--;
                } else {
                    $total_post_count_district--;
                    $total_allocation++;
                }
                $district_arryay_length--;
                $district_array_count++;
            }
            $total++;
            if ($total == 10) {
                $p = $q = 2;
                $r = $s = $t = 1;
            } else {
                if ($p == 0 && $friends_arryay_length == 0 && $global_arryay_length == 0 && $national_arryay_length == 0 && $district_arryay_length == 0) {
                    $p = 2;
                } else if ($q == 0 && $global_arryay_length == 0 && $national_arryay_length == 0 && $district_arryay_length == 0) {
                    $p = $q = 2;
                } else if ($r == 0 && $national_arryay_length == 0 && $district_arryay_length == 0) {
                    $r = 1;
                    $p = $q = 2;
                } else if ($s == 0 && $district_arryay_length == 0) {
                    $s = $r = 1;
                    $p = $q = 2;
                } else if ($t == 0) {
                    $t = $s = $r = 1;
                    $p = $q = 2;
                }
            }
        }
        $result_array = [];
        $p = $q = 2;
        $r = $s = $t = 1;
        $total_allocate_count = 10;
        for ($i = 1; $i <= $total_allocate_count; $i++) {
            if ($q > 0 && $friends_arryay_length > 0) {
                $friends_arryay_length--;
                if (!in_array($friends_post_final[$friends_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array [] = $friends_post_final[$friends_array_count]['posts_id'];
                    $result_array[] = $friends_post_final[$friends_array_count];
                    $q--;
                } else {
                    $total_post_count_friend--;
                    $total_allocate_count++;
                }
                $friends_array_count++;
            } elseif ($p > 0 && $state_arryay_length > 0) {
                $state_arryay_length--;
                if (!in_array($sort_array_state_final[$state_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array [] = $sort_array_state_final[$state_array_count]['posts_id'];
                    $result_array[] = $sort_array_state_final[$state_array_count];
                    $p--;
                } else {
                    $total_post_count_state--;
                    $total_allocate_count++;
                }
                $state_array_count++;
            }
            else if ($r > 0 && $global_arryay_length > 0) {
                $global_arryay_length--;
                if (!in_array($sort_array_global_final[$global_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array [] = $sort_array_global_final[$global_array_count]['posts_id'];
                    $result_array[] = $sort_array_global_final[$global_array_count];
                    $r--;
                } else {
                    $total_post_count_global--;
                    $total_allocate_count++;
                }
                $global_array_count++;
            } else if ($s > 0 && $national_arryay_length > 0) {
                $national_arryay_length--;
                if (!in_array($sort_array_national_final[$national_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array [] = $sort_array_national_final[$national_array_count]['posts_id'];
                    $result_array[] = $sort_array_national_final[$national_array_count];
                    $s--;
                } else {
                    $total_post_count_national--;
                    $total_allocate_count++;
                }
                $national_array_count++;
            } else if ($t > 0 && $district_arryay_length > 0) {
                $district_arryay_length--;
                if (!in_array($sort_array_district_final[$district_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array [] = $sort_array_district_final[$district_array_count]['posts_id'];
                    $result_array[] = $sort_array_district_final[$district_array_count];
                    $t--;
                } else {
                    $total_post_count_district--;
                    $total_allocate_count++;
                }
                $district_array_count++;
            }
            if ($p == 0 && $friends_arryay_length == 0 && $global_arryay_length == 0 && $national_arryay_length == 0 && $district_arryay_length == 0) {
                $p = 2;
            } else if ($q == 0 && $global_arryay_length == 0 && $national_arryay_length == 0 && $district_arryay_length == 0) {
                $p = $q = 2;
            } else if ($r == 0 && $national_arryay_length == 0 && $district_arryay_length == 0) {
                $r = 1;
                $p = $q = 2;
            } else if ($s == 0 && $district_arryay_length == 0) {
                $s = $r = 1;
                $p = $q = 2;
            } else if ($t == 0) {
                $t = $s = $r = 1;
                $p = $q = 2;
            }
        }
        $next_array = $this->check_get_posts_next_array($userid, $page_start, $country, $state, $district, $area);
        if (!empty($result_array)) {
            foreach ($result_array as $key => $val) {
                $pollcount          = $this->PostsApiModel->pollsCount($result_array[$key]['posts_id']);
                $commentcount       = $this->PostsApiModel->commentsCount($result_array[$key]['posts_id']);
                $commentsCountouter = $this->PostsApiModel->commentsCountouter($result_array[$key]['posts_id']);
                $is_user_polled     = $this->PostsApiModel->isUserpolled($result_array[$key]['posts_id'], $userid);
                foreach ($commentcount['data'] as $co => $va) {
                    $commentcount['data'][$co]['users_photo'] = $this->media_url . "assets/images/users/" . $commentcount['data'][$co]['users_photo'];
                }
                $ispolled                                  = ($is_user_polled['count'] > 0) ? true : false;
                $result_array[$key]['is_polled']           = $ispolled;
                $token                                     = mt_rand(100000, 999999);
                $result_array[$key]['poll_count']          = $pollcount['count'];
                $result_array[$key]['comment_count']       = $commentsCountouter;
                $result_array[$key]['comment']             = $commentcount['data'];
                $date                                      = new DateTime($result_array[$key]['posts_uploaded_date'], $GMT);
                $date->setTimezone(new DateTimeZone($zone['timezone']));
                $result_array[$key]['posts_uploaded_date'] = $date->format('Y-m-d H:i:s');

                $result_array[$key]['sharecode']           = $this->media_url . 'post-view?postid=' . $result_array[$key]['posts_id'] . '&&type=0';
                $result_array[$key]['downloadlink']        = $result_array[$key]['posts_photo_or_video'] . '?postid=' . $result_array[$key]['posts_id'] . '&&type=0';
            }
            return $result_array;
        } else {
            return $result_array;
        }
    }

    function check_get_posts_next_array($userid, $page_start, $country, $state, $district, $area) {
        
        $this->output->set_content_type('application/json');
        $zone       = getallheaders();
        $GMT        = new DateTimeZone("GMT");
        if (empty($zone['timezone']))
            $zone['timezone'] = "Asia/Kolkata";
        
        $country         = (!empty($country)) ? $country : NULL;
        $state           = (!empty($state)) ? $state : NULL;
        $district        = (!empty($district)) ? $district : NULL;
        $area            = (!empty($area)) ? $area : NULL;
        $page_start      = !empty($page_start) ? $page_start + 1 : 0;
        $date            = new DateTime(date("Y-m-d H:i:s", strtotime("now")), $GMT);
        $date->setTimezone(new DateTimeZone($zone['timezone']));
        $datetime        = $date->format('Y-m-d H:i:s');
        $get_hide_post   = $this->PostsApiModel->gethiddenposts($userid);
        $get_report_post = $this->PostsApiModel->getReportedposts($userid);

        $get_state_post     = $this->PostsApiModel->getStatePosts($state); // state posts 
        $get_global_post    = $this->PostsApiModel->getGlobalPosts(); // global   posts 
        $get_national_post  = $this->PostsApiModel->getNationalPosts($country); // national posts 
        $get_district_post  = $this->PostsApiModel->getDistrictPosts($district); // district posts 
        // trending state posts starts here
        foreach ($get_state_post as $k => $v) {
            $uploaddate                                 = new DateTime($get_state_post[$k]['posts_uploaded_date'], $GMT);
            $uploaddate->setTimezone(new DateTimeZone($zone['timezone']));
            $get_state_post[$k]['posts_uploaded_date']   = $uploaddate->format('Y-m-d H:i:s');
            $get_state_post[$k]['users_name']            = $this->PostsApiModel->getusersnamebypostid($get_state_post[$k]['posts_user_id']);
            $get_state_post[$k]['users_photo']           = $this->media_url . "assets/images/users/" . $this->PostsApiModel->getusersphotobypostid($get_state_post[$k]['posts_user_id']);
            $get_state_post[$k]['posts_photo_or_video']  = $this->s3_url . "assets/images/posts/" . $get_state_post[$k]['posts_photo_or_video'];
            $get_state_post[$k]['posts_video_thumbnail'] = $this->s3_url . "assets/images/posts/thumbnails/" . $get_state_post[$k]['posts_video_thumbnail'];
            $datePercent                                 = strtotime($datetime) - strtotime($get_state_post[$k]['posts_uploaded_date']);
            $pollCount                                   = $get_state_post[$k]['total'];
            $get_state_post[$k]['poll_percent']          = $pollCount > 0 ? intval($datePercent / $pollCount) : 0;
            $get_state_post[$k]['sort_order_key']        = "state post";
        }
        foreach ($get_state_post as $res_1 => $key_1) {
            $sort_array_state[$key_1['poll_percent']] = $get_state_post[$res_1];
        }
        ksort($sort_array_state);
        foreach ($sort_array_state as $final_last_array_state[])
        $hide_array_state = [];
        foreach ($get_hide_post as $row) {
            $hide_array_state[] = $row['hide_posts_post_id'];
        }
        $report_array_state = [];
        foreach ($get_report_post as $row) {
            $report_array_state[] = $row['posts_report_post_id'];
        }
        $final_array_state = [];
        foreach ($final_last_array_state as $in_row) {
            if (!in_array($in_row['posts_id'], $hide_array_state)) {
                $final_array_state[] = $in_row;
            }
        }
        $sort_array_state_final = [];
        foreach ($final_array_state as $in_row) {
            if (!in_array($in_row['posts_id'], $report_array_state)) {
                $sort_array_state_final[] = $in_row;
            }
        }
        //trending global posts
        foreach ($get_global_post as $k => $v) {
            $uploaddate                                   = new DateTime($get_global_post[$k]['posts_uploaded_date'], $GMT);
            $uploaddate->setTimezone(new DateTimeZone($zone['timezone']));
            $get_global_post[$k]['posts_uploaded_date']   = $uploaddate->format('Y-m-d H:i:s');
            $get_global_post[$k]['users_name']            = $this->PostsApiModel->getusersnamebypostid($get_global_post[$k]['posts_user_id']);
            $get_global_post[$k]['users_photo']           = $this->media_url . "assets/images/users/" . $this->PostsApiModel->getusersphotobypostid($get_global_post[$k]['posts_user_id']);
            $get_global_post[$k]['posts_photo_or_video']  = $this->s3_url . "assets/images/posts/" . $get_global_post[$k]['posts_photo_or_video'];
            $get_global_post[$k]['posts_video_thumbnail'] = $this->s3_url . "assets/images/posts/thumbnails/" . $get_global_post[$k]['posts_video_thumbnail'];
            $datePercent                                  = strtotime($datetime) - strtotime($get_global_post[$k]['posts_uploaded_date']);
            $pollCount                                    = $get_global_post[$k]['total'];
            $get_global_post[$k]['poll_percent']          = $pollCount > 0 ? intval($datePercent / $pollCount) : 0;
            $get_global_post[$k]['sort_order_key']        = "global post";
        }
        foreach ($get_global_post as $res_1 => $key_1) {
            $sort_array_global[$key_1['poll_percent']] = $get_global_post[$res_1];
        }
        ksort($sort_array_global);
        foreach ($sort_array_global as $final_last_array_global[])
        $hide_array_global = [];
        foreach ($get_hide_post as $row) {
            $hide_array_global[] = $row['hide_posts_post_id'];
        }
        $report_array_global = [];
        foreach ($get_report_post as $row) {
            $report_array_global[] = $row['posts_report_post_id'];
        }
        $final_array_global = [];
        foreach ($final_last_array_global as $in_row) {
            if (!in_array($in_row['posts_id'], $hide_array_global)) {
                $final_array_global[] = $in_row;
            }
        }
        $sort_array_global_final = [];
        foreach ($final_array_global as $in_row) {
            if (!in_array($in_row['posts_id'], $report_array_global)) {
                $sort_array_global_final[] = $in_row;
            }
        }
        //trending national posts
        foreach ($get_national_post as $k => $v) {
            $uploaddate                                     = new DateTime($get_national_post[$k]['posts_uploaded_date'], $GMT);
            $uploaddate->setTimezone(new DateTimeZone($zone['timezone']));
            $get_national_post[$k]['posts_uploaded_date']   = $uploaddate->format('Y-m-d H:i:s');
            $get_national_post[$k]['users_name']            = $this->PostsApiModel->getusersnamebypostid($get_national_post[$k]['posts_user_id']);
            $get_national_post[$k]['users_photo']           = $this->media_url . "assets/images/users/" . $this->PostsApiModel->getusersphotobypostid($get_national_post[$k]['posts_user_id']);
            $get_national_post[$k]['posts_photo_or_video']  = $this->s3_url . "assets/images/posts/" . $get_national_post[$k]['posts_photo_or_video'];
            $get_national_post[$k]['posts_video_thumbnail'] = $this->s3_url . "assets/images/posts/thumbnails/" . $get_national_post[$k]['posts_video_thumbnail'];
            $datePercent                                    = strtotime($datetime) - strtotime($get_national_post[$k]['posts_uploaded_date']);
            $pollCount                                      = $get_national_post[$k]['total'];
            $get_national_post[$k]['poll_percent']          = $pollCount > 0 ? intval($datePercent / $pollCount) : 0;
            $get_national_post[$k]['sort_order_key']        = "national post";
        }
        foreach ($get_national_post as $res_1 => $key_1) {

            $sort_array_national[$key_1['poll_percent']] = $get_national_post[$res_1];
        }
        ksort($sort_array_national);
        foreach ($sort_array_national as $final_last_array_national[])
        $hide_array_national = [];
        foreach ($get_hide_post as $row) {
            $hide_array_national[] = $row['hide_posts_post_id'];
        }
        $report_array_national = [];
        foreach ($get_report_post as $row) {
            $report_array_national[] = $row['posts_report_post_id'];
        }
        $final_array_national = [];
        foreach ($final_last_array_national as $in_row) {
            if (!in_array($in_row['posts_id'], $hide_array_national)) {
                $final_array_national[] = $in_row;
            }
        }
        $sort_array_national_final = [];
        foreach ($final_array_national as $in_row) {
            if (!in_array($in_row['posts_id'], $report_array_national)) {
                $sort_array_national_final[] = $in_row;
            }
        }
        //trending district posts
        foreach ($get_district_post as $k => $v) {
            $uploaddate                                     = new DateTime($get_district_post[$k]['posts_uploaded_date'], $GMT);
            $uploaddate->setTimezone(new DateTimeZone($zone['timezone']));
            $get_district_post[$k]['posts_uploaded_date']   = $uploaddate->format('Y-m-d H:i:s');
            $get_district_post[$k]['users_name']            = $this->PostsApiModel->getusersnamebypostid($get_district_post[$k]['posts_user_id']);
            $get_district_post[$k]['users_photo']           = $this->media_url . "assets/images/users/" . $this->PostsApiModel->getusersphotobypostid($get_district_post[$k]['posts_user_id']);
            $get_district_post[$k]['posts_photo_or_video']  = $this->s3_url . "assets/images/posts/" . $get_district_post[$k]['posts_photo_or_video'];
            $get_district_post[$k]['posts_video_thumbnail'] = $this->s3_url . "assets/images/posts/thumbnails/" . $get_district_post[$k]['posts_video_thumbnail'];
            $datePercent                                    = strtotime($datetime) - strtotime($get_district_post[$k]['posts_uploaded_date']);
            $pollCount                                      = $get_district_post[$k]['total'];
            $get_district_post[$k]['poll_percent']          = $pollCount > 0 ? intval($datePercent / $pollCount) : 0;
            $get_district_post[$k]['sort_order_key']        = "district post";
        }
        foreach ($get_district_post as $res_1 => $key_1) {
            $sort_array_district[$key_1['poll_percent']] = $get_district_post[$res_1];
        }
        ksort($sort_array_district);
        foreach ($sort_array_district as $final_last_array_district[])
        $hide_array_district = [];
        foreach ($get_hide_post as $row) {
            $hide_array_district[] = $row['hide_posts_post_id'];
        }
        $report_array_district = [];
        foreach ($get_report_post as $row) {
            $report_array_district[] = $row['posts_report_post_id'];
        }
        $final_array_district = [];
        foreach ($final_last_array_district as $in_row) {
            if (!in_array($in_row['posts_id'], $hide_array_district)) {
                $final_array_district[] = $in_row;
            }
        }
        $sort_array_district_final = [];
        foreach ($final_array_district as $in_row) {
            if (!in_array($in_row['posts_id'], $report_array_district)) {
                $sort_array_district_final[] = $in_row;
            }
        }
        // friends posts starts here
        $getFriends     = $this->PostsApiModel->getFriends($userid);
        $array_count    = count($getFriends);
        $getFriends[$array_count]['friend_id'] = $userid;
        if ($getFriends) {
            foreach ($getFriends as $value => $key) {

                $res_data = $this->PostsApiModel->getPosts($getFriends[$value]['friend_id']);
                foreach ($res_data['data'] as $row) {
                    $res['datas'][] = $row;
                }
            }
        }
        foreach ($res['datas'] as $val => $k) {
            $res['datas'][$val]['post_order_type'] = "friend post";  // 2 for friends posts
            $res['datas'][$val]['users_photo'] = $this->media_url . "assets/images/users/" . $res['datas'][$val]['users_photo'];
            $res['datas'][$val]['posts_photo_or_video'] = $this->s3_url . "assets/images/posts/" . $res['datas'][$val]['posts_photo_or_video'];
            $res['datas'][$val]['posts_video_thumbnail'] = $this->s3_url . "assets/images/posts/thumbnails/" . $res['datas'][$val]['posts_video_thumbnail'];
        }
        $i = 0;
        foreach ($res['datas'] as $row) {
            $total_array[$i] = $row['posts_uploaded_date'];
            $i++;
        }
        arsort($total_array);
        foreach ($total_array as $key => $value) {
            $final_last_array_friend[] = $res['datas'][$key];
        }
        $hide_array_friend = [];
        foreach ($get_hide_post as $row) {
            $hide_array_friend[] = $row['hide_posts_post_id'];
        }
        $report_array_friend = [];
        foreach ($get_report_post as $row) {
            $report_array_friend[] = $row['posts_report_post_id'];
        }
        $final_array_friend = [];
        foreach ($final_last_array_friend as $in_row) {
            if (!in_array($in_row['posts_id'], $hide_array_friend)) {
                $final_array_friend[] = $in_row;
            }
        }
        $friends_post_final = [];
        foreach ($final_array_friend as $in_row) {
            if (!in_array($in_row['posts_id'], $report_array_friend)) {
                $friends_post_final[] = $in_row;
            }
        }
        // friends posts ends here
        $page_number    = $page_start;
        $per_page       = 10;
        $state_arryay_length        = sizeof($sort_array_state_final);
        $friends_arryay_length      = sizeof($friends_post_final);
        $global_arryay_length       = sizeof($sort_array_global_final);
        $natioanl_arryay_length     = sizeof($sort_array_national_final);
        $district_arryay_length     = sizeof($sort_array_district_final);
        $total_length               = $state_arryay_length + $friends_arryay_length + $global_arryay_length + $natioanl_arryay_length + $district_arryay_length;

        $total_posts = $state_arryay_length + $friends_arryay_length + $global_arryay_length + $national_arryay_length + $district_arryay_length;
        $p = $q = 2;
        $r = $s = $t = $total = 1;
        $state_array_count = $friends_array_count = $global_array_count = $national_array_count = $district_array_count = 0;
        $post_id_array = [];
        $total_allocation = ($page_number * $per_page);
        for ($i = 1; $i <= ($total_allocation - 10); $i++) {
            if ($p > 0 && $state_arryay_length > 0) {
                if (!in_array($sort_array_state_final[$state_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array[] = $sort_array_state_final[$state_array_count]['posts_id'];
                    $p--;
                } else {
                    $total_allocation++;
                }
                $state_arryay_length--;
                $state_array_count++;
            } else if ($q > 0 && $friends_arryay_length > 0) {
                if (!in_array($friends_post_final[$friends_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array [] = $friends_post_final[$friends_array_count]['posts_id'];
                    $q--;
                } else {
                    $total_allocation++;
                }
                $friends_arryay_length--;
                $friends_array_count++;
            } else if ($r > 0 && $global_arryay_length > 0) {
                if (!in_array($sort_array_global_final[$global_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array [] = $sort_array_global_final[$global_array_count]['posts_id'];
                    $r--;
                } else {
                    $total_allocation++;
                }
                $global_arryay_length--;
                $global_array_count++;
            } else if ($s > 0 && $national_arryay_length > 0) {
                if (!in_array($sort_array_national_final[$national_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array [] = $sort_array_national_final[$national_array_count]['posts_id'];
                    $s--;
                } else {
                    $total_allocation++;
                }
                $national_arryay_length--;
                $national_array_count++;
            } else if ($t > 0 && $district_arryay_length > 0) {
                if (!in_array($sort_array_district_final[$district_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array [] = $sort_array_district_final[$district_array_count]['posts_id'];
                    $t--;
                } else {
                    $total_allocation++;
                }
                $district_arryay_length--;
                $district_array_count++;
            }
            $total++;
            if ($total == 10) {
                $p = $q = 2;
                $r = $s = $t = 1;
            } else {
                if ($p == 0 && $friends_arryay_length == 0 && $global_arryay_length == 0 && $national_arryay_length == 0 && $district_arryay_length == 0) {
                    $p = 2;
                } else if ($q == 0 && $global_arryay_length == 0 && $national_arryay_length == 0 && $district_arryay_length == 0) {
                    $p = $q = 2;
                } else if ($r == 0 && $national_arryay_length == 0 && $district_arryay_length == 0) {
                    $r = 1;
                    $p = $q = 2;
                } else if ($s == 0 && $district_arryay_length == 0) {
                    $s = $r = 1;
                    $p = $q = 2;
                } else if ($t == 0) {
                    $t = $s = $r = 1;
                    $p = $q = 2;
                }
            }
        }
        $result_array = [];
        $p = $q = 2;
        $r = $s = $t = 1;
        $total_allocate_count = 10;
        for ($i = 1; $i <= $total_allocate_count; $i++) {
            if ($p > 0 && $state_arryay_length > 0) {
                $state_arryay_length--;
                if (!in_array($sort_array_state_final[$state_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array [] = $sort_array_state_final[$state_array_count]['posts_id'];
                    $result_array[] = $sort_array_state_final[$state_array_count];
                    $p--;
                } else {
                    $total_allocate_count++;
                }
                $state_array_count++;
            } else if ($q > 0 && $friends_arryay_length > 0) {
                $friends_arryay_length--;
                if (!in_array($friends_post_final[$friends_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array [] = $friends_post_final[$friends_array_count]['posts_id'];
                    $result_array[] = $friends_post_final[$friends_array_count];
                    $q--;
                } else {
                    $total_allocate_count++;
                }
                $friends_array_count++;
            } else if ($r > 0 && $global_arryay_length > 0) {
                $global_arryay_length--;
                if (!in_array($sort_array_global_final[$global_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array [] = $sort_array_global_final[$global_array_count]['posts_id'];
                    $result_array[] = $sort_array_global_final[$global_array_count];
                    $r--;
                } else {
                    $total_allocate_count++;
                }
                $global_array_count++;
            } else if ($s > 0 && $national_arryay_length > 0) {
                $national_arryay_length--;
                if (!in_array($sort_array_national_final[$national_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array [] = $sort_array_national_final[$national_array_count]['posts_id'];
                    $result_array[] = $sort_array_national_final[$national_array_count];
                    $s--;
                } else {
                    $total_allocate_count++;
                }
                $national_array_count++;
            } else if ($t > 0 && $district_arryay_length > 0) {
                $district_arryay_length--;
                if (!in_array($sort_array_district_final[$district_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array [] = $sort_array_district_final[$district_array_count]['posts_id'];
                    $result_array[] = $sort_array_district_final[$district_array_count];
                    $t--;
                } else {
                    $total_allocate_count++;
                }
                $district_array_count++;
            }
            if ($p == 0 && $friends_arryay_length == 0 && $global_arryay_length == 0 && $national_arryay_length == 0 && $district_arryay_length == 0) {
                $p = 2;
            } else if ($q == 0 && $global_arryay_length == 0 && $national_arryay_length == 0 && $district_arryay_length == 0) {
                $p = $q = 2;
            } else if ($r == 0 && $national_arryay_length == 0 && $district_arryay_length == 0) {
                $r = 1;
                $p = $q = 2;
            } else if ($s == 0 && $district_arryay_length == 0) {
                $s = $r = 1;
                $p = $q = 2;
            } else if ($t == 0) {
                $t = $s = $r = 1;
                $p = $q = 2;
            }
        }

        if (!empty($result_array)) {
            foreach ($result_array as $key => $val) {
                $pollcount          = $this->PostsApiModel->pollsCount($result_array[$key]['posts_id']);
                $commentcount       = $this->PostsApiModel->commentsCount($result_array[$key]['posts_id']);
                $commentsCountouter = $this->PostsApiModel->commentsCountouter($result_array[$key]['posts_id']);
                $is_user_polled     = $this->PostsApiModel->isUserpolled($result_array[$key]['posts_id'], $userid);
                foreach ($commentcount['data'] as $co => $va) {
                    $commentcount['data'][$co]['users_photo'] = $this->media_url . "assets/images/users/" . $commentcount['data'][$co]['users_photo'];
                }
                $ispolled = ($is_user_polled['count'] > 0) ? true : false;
                $result_array[$key]['is_polled']             = $ispolled;
                $token                                       = mt_rand(100000, 999999);
                $result_array[$key]['posts_photo_or_video']  = $this->s3_url . 'assets/images/posts/' . $result_array[$key]['posts_photo_or_video'];
                $result_array[$key]['posts_video_thumbnail'] = $this->s3_url . 'assets/images/posts/thumbnails/' . $result_array[$key]['posts_video_thumbnail'];
                $result_array[$key]['poll_count']            = $pollcount['count'];
                $result_array[$key]['comment_count']         = $commentsCountouter;
                $result_array[$key]['comment']               = $commentcount['data'];
                $date                                        = new DateTime($result_array[$key]['posts_uploaded_date'], $GMT);
                $date->setTimezone(new DateTimeZone($zone['timezone']));
                $result_array[$key]['posts_uploaded_date']   = $date->format('Y-m-d H:i:s');
                $result_array[$key]['sharecode']             = $this->media_url . 'post-view?postid=' . $result_array[$key]['posts_id'] . '&&type=0';
                $result_array[$key]['downloadlink']          = $result_array[$key]['posts_photo_or_video'] . '?postid=' . $result_array[$key]['posts_id'] . '&&type=0';
            }
            return count($result_array);
        } else {

            return count($result_array);
        }
    }
    // get posts api
    function getPostsold() {
        $this->output->set_content_type('application/json');

        $zone   = getallheaders();
        $GMT    = new DateTimeZone("GMT");
        if (empty($zone['timezone']))
            $zone['timezone'] = "Asia/Kolkata";
        $country        = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;
        $state          = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district       = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;
        $area           = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;
        $page_start     = !empty($this->input->post('page_start')) ? $this->input->post('page_start') : 0;
        $userid         = $this->input->post('userid');
        $date           = new DateTime(date("Y-m-d H:i:s", strtotime("now")), $GMT);
        $date->setTimezone(new DateTimeZone($zone['timezone']));
        $datetime       = $date->format('Y-m-d H:i:s');
        $get_state_post = $this->PostsApiModel->getStatePosts($state);
        foreach ($get_state_post as $k => $v) {
            $uploaddate     = new DateTime($get_state_post[$k]['posts_uploaded_date'], $GMT);
            $uploaddate->setTimezone(new DateTimeZone($zone['timezone']));
            $get_state_post[$k]['posts_uploaded_date'] = $uploaddate->format('Y-m-d H:i:s');
            $get_state_post[$k]['users_name']          = $this->PostsApiModel->getusersnamebypostid($get_state_post[$k]['posts_user_id']);
            $get_state_post[$k]['users_photo']         = $this->media_url . "assets/images/users/" . $this->PostsApiModel->getusersphotobypostid($get_state_post[$k]['posts_user_id']);
            $datePercent                               = strtotime($datetime) - strtotime($get_state_post[$k]['posts_uploaded_date']);
            $pollCount                                 = $get_state_post[$k]['total'];
            $get_state_post[$k]['poll_percent']        = $pollCount > 0 ? intval($datePercent / $pollCount) : 0;
        }
        $sort_array = array();
        $res_2      = array();
        if ($get_state_post) {
            foreach ($get_state_post as $res_1 => $key_1) {
                $sort_array[$key_1['poll_percent']] = $get_state_post[$res_1];
            }
            ksort($sort_array);
            foreach ($sort_array as $state_post_final[])
                ;
        }
        $get_poll_party_id      = $this->PostsApiModel->getpollpartyid($userid);
        $get_hide_post          = $this->PostsApiModel->gethiddenposts($userid);
        $get_report_post        = $this->PostsApiModel->getReportedposts($userid);
        $get_party_hide_post    = $this->PostsApiModel->getpartyhiddenposts($userid);
        $get_party_report_post  = $this->PostsApiModel->getpartyReportedposts($userid);
        $res_by_polls           = $this->getPostsbypolls();
        $res_by_polls_state     = $this->getPostsbystatePolls($state);
        $res_by_party_polls     = $this->getPartyPostsbypolls($userid);
        $get_posts_by_location  = $this->PostsApiModel->getPostsbyLocation($state);
        $state_array = [];
        foreach ($get_posts_by_location as $row) {
            $state_array[] = $row['posts_id'];
        }
        $state_array_sort = [];
        foreach ($res_by_polls_state as $in_row) {
            if (in_array($in_row['posts_id'], $state_array)) {
                $state_array_sort[] = $in_row;
            }
        }
        $state_array_sort = array_slice($state_array_sort, 0, 5);
        $hide_party_array = [];
        foreach ($get_party_hide_post as $row) {
            $hide_party_array[] = $row['hide_posts_post_id'];
        }
        $report_party_array = [];
        foreach ($get_party_report_post as $row) {
            $report_party_array[] = $row['party_posts_report_post_id'];
        }
        $party_array_hide = [];
        foreach ($res_by_party_polls as $in_row) {
            if (!in_array($in_row['party_posts_id'], $hide_party_array)) {
                $party_array_hide[] = $in_row;
            }
        }
        $party_array_final = [];
        foreach ($party_array_hide as $in_row) {
            if (!in_array($in_row['party_posts_id'], $report_party_array)) {
                $party_array_final[] = $in_row;
            }
        }
        $party_array_final = array_slice($party_array_final, 0, 3);
        foreach ($party_array_final as $k1 => $v1) {
            $party_array_final[$k1]["posts_id"]              = $party_array_final[$k1]["party_posts_id"];
            $party_array_final[$k1]["posts_user_id"]         = $party_array_final[$k1]["party_posts_posted_by"];
            $party_array_final[$k1]["posts_title"]           = $party_array_final[$k1]["party_posts_title"];
            $party_array_final[$k1]["posts_content"]         = $party_array_final[$k1]["party_posts_title"];
            $party_array_final[$k1]["posts_photo_or_video"]  = $party_array_final[$k1]["posts_photo_or_video"];
            $party_array_final[$k1]["posts_video_thumbnail"] = $s3_url . "assets/images/partyposts/thumbnail/" . $party_array_final[$k1]["posts_videoparty_posts_video_thumbnail_thumbnail"];
            $party_array_final[$k1]["posts_uploaded_date"]   = $party_array_final[$k1]["party_posts_uploaded_date"];
            $party_array_final[$k1]["posts_type"]            = 1;
            $party_array_final[$k1]["posts_active"]          = $party_array_final[$k1]["party_posts_active"];
            $party_array_final[$k1]["posts_created_type"]    = 1;
            $party_array_final[$k1]["post_order_type"]       = 3;
        }
        $hide_array = [];
        foreach ($get_hide_post as $row) {
            $hide_array[] = $row['hide_posts_post_id'];
        }
        $report_array = [];
        foreach ($get_report_post as $row) {
            $report_array[] = $row['posts_report_post_id'];
        }
        $getFriends     = $this->PostsApiModel->getFriends($userid);
        $array_count    = count($getFriends);
        $getFriends[$array_count]['friend_id'] = $userid;
        if ($getFriends) {
            foreach ($getFriends as $value => $key) {
                $res_data = $this->PostsApiModel->getPosts($getFriends[$value]['friend_id']);
                foreach ($res_data['data'] as $row) {
                    $res['datas'][] = $row;
                }
            }
        }
        foreach ($res['datas'] as $val => $k) {
            $res['datas'][$val]['post_order_type'] = 2;  // 2 for normal posts
        }
        $i = 0;
        foreach ($res['datas'] as $row) {
            $total_array[$i] = $row['posts_uploaded_date'];
            $i++;
        }
        arsort($total_array);
        foreach ($total_array as $key => $value) {
            $res['data'][] = $res['datas'][$key];
        }
        foreach ($res['data'] as $row_1) {
            $res_by_polls[] = $row_1;
        }
        foreach ($res_by_polls as $a1 => $v1) {
            if ($res_by_polls[$a1]['post_order_type'] == 2)
                $res_by_polls[$a1]['users_photo'] = $this->media_url . "assets/images/users/" . $res_by_polls[$a1]['users_photo'];
        }
        $final_array = [];
        foreach ($res_by_polls as $in_row) {
            if (!in_array($in_row['posts_id'], $hide_array)) {
                $final_array[] = $in_row;
            }
        }
        $final_last_array_sort = [];
        foreach ($final_array as $in_row) {
            if (!in_array($in_row['posts_id'], $report_array)) {
                $final_last_array_sort[] = $in_row;
            }
        }
        foreach ($state_array_sort as $k => $v) {
            $state_array_sort[$k]['post_order_type'] = 4;
        }
        foreach ($state_array_sort as $row) {
            $final_last_array_sort[] = $row;
        }
        foreach ($state_post_final as $t => $k) {
            $state_post_final[$t]['post_order_type'] = 2;
        }
        $state_post_final_last      = array_slice($state_post_final, 0, 2);
        $global_post_final_third    = array_slice($state_post_final, 2, 3);
        $res['data']                = array_slice($res['data'], 0, 2);
        $last_posts_final           = array_slice($state_post_final, 3, 60);
        foreach ($res['data'] as $row) {
            $state_post_final_last[] = $row;
        }
        foreach ($global_post_final_third as $row) {
            $state_post_final_last[] = $row;
        }
        foreach ($last_posts_final as $row) {

            $state_post_final_last[] = $row;
        }
        $post_count             = count($state_post_final_last);
        $final_last_array       = array_slice($state_post_final_last, $page_start, 10);
        $final_last_array_next  = array_slice($state_post_final_last, $page_start + 10, 10);
        if (!empty($final_last_array)) {
            foreach ($final_last_array as $key => $value) {
                if ($final_last_array[$key]['post_order_type'] == 2) {
                    $pollcount              = $this->PostsApiModel->pollsCount($final_last_array[$key]['posts_id']);
                    $commentcount           = $this->PostsApiModel->commentsCount($final_last_array[$key]['posts_id']);
                    $commentsCountouter     = $this->PostsApiModel->commentsCountouter($final_last_array[$key]['posts_id']);
                    $is_user_polled         = $this->PostsApiModel->isUserpolled($final_last_array[$key]['posts_id'], $userid);
                    foreach ($commentcount['data'] as $co => $va) {
                        $commentcount['data'][$co]['users_photo'] = $this->media_url . "assets/images/users/" . $commentcount['data'][$co]['users_photo'];
                    }
                    $ispolled                                        = ($is_user_polled['count'] > 0) ? true : false;
                    $final_last_array[$key]['is_polled']             = $ispolled;
                    $token                                           = mt_rand(100000, 999999);
                    $final_last_array[$key]['posts_photo_or_video']  = $this->s3_url . 'assets/images/posts/' . $final_last_array[$key]['posts_photo_or_video'];
                    $final_last_array[$key]['posts_video_thumbnail'] = $this->s3_url . 'assets/images/posts/thumbnails/' . $final_last_array[$key]['posts_video_thumbnail'];
                    $final_last_array[$key]['poll_count']            = $pollcount['count'];
                    $final_last_array[$key]['comment_count']         = $commentsCountouter;
                    $final_last_array[$key]['comment']               = $commentcount['data'];
                    $date                                            = new DateTime($final_last_array[$key]['posts_uploaded_date'], $GMT);
                    $date->setTimezone(new DateTimeZone($zone['timezone']));
                    $final_last_array[$key]['posts_uploaded_date']   = $date->format('Y-m-d H:i:s');
                    $final_last_array[$key]['sharecode']             = $this->media_url . 'post-view?postid=' . $final_last_array[$key]['posts_id'] . '&&type=0';
                    $final_last_array[$key]['downloadlink']          = $final_last_array[$key]['posts_photo_or_video'] . '?postid=' . $final_last_array[$key]['posts_id'] . '&&type=0';
                }
            }
            $this->output->set_output(json_encode(array('hidden_post_id' => $get_hide_post, 'report_post_id' => $get_report_post, 'posts' => $final_last_array, 'friends' => $getFriends, 'is_completed' => count($final_last_array_next) > 0 ? 0 : 1, 'posts_count' => $post_count, 'status' => true)));
        } else {
            $this->output->set_output(json_encode(array('posts' => 'no data found on server', 'status' => false)));
        }
    }
    // get posts api
    function returngetPosts($userid, $postid, $type, $country, $state, $district, $area, $page) {
        $this->output->set_content_type('application/json');
        $zone   = getallheaders();
        $GMT    = new DateTimeZone("GMT");
        if (empty($zone['timezone']))
            $zone['timezone'] = "Asia/Kolkata";
        $country    = (!empty($country)) ? $country : NULL;
        $state      = (!empty($state)) ? $state : NULL;
        $district   = (!empty($district)) ? $district : NULL;
        $area       = (!empty($area)) ? $area : NULL;
        $page_start = (!empty($page)) ? $page : 0;

        $get_poll_party_id      = $this->PostsApiModel->getpollpartyid($userid);
        $get_hide_post          = $this->PostsApiModel->gethiddenposts($userid);
        $get_report_post        = $this->PostsApiModel->getReportedposts($userid);
        $get_party_hide_post    = $this->PostsApiModel->getpartyhiddenposts($userid);
        $get_party_report_post  = $this->PostsApiModel->getpartyReportedposts($userid);
        $res_by_polls           = $this->getPostsbypolls();
        $res_by_polls_state     = $this->getPostsbystatePolls($state);
        $res_by_party_polls     = $this->getPartyPostsbypolls($userid);
        $get_posts_by_location  = $this->PostsApiModel->getPostsbyLocation($state);
        $state_array = [];
        foreach ($get_posts_by_location as $row) {
            $state_array[] = $row['posts_id'];
        }
        $state_array_sort = [];
        foreach ($res_by_polls_state as $in_row) {
            if (in_array($in_row['posts_id'], $state_array)) {
                $state_array_sort[] = $in_row;
            }
        }
        $state_array_sort = array_slice($state_array_sort, 0, 5);
        $hide_party_array = [];
        foreach ($get_party_hide_post as $row) {
            $hide_party_array[] = $row['hide_posts_post_id'];
        }
        $report_party_array = [];
        foreach ($get_party_report_post as $row) {
            $report_party_array[] = $row['party_posts_report_post_id'];
        }
        $party_array_hide = [];
        foreach ($res_by_party_polls as $in_row) {
            if (!in_array($in_row['party_posts_id'], $hide_party_array)) {
                $party_array_hide[] = $in_row;
            }
        }
        $party_array_final = [];
        foreach ($party_array_hide as $in_row) {
            if (!in_array($in_row['party_posts_id'], $report_party_array)) {
                $party_array_final[] = $in_row;
            }
        }
        $party_array_final = array_slice($party_array_final, 0, 3);
        foreach ($party_array_final as $k1 => $v1) {
            $party_array_final[$k1]["posts_id"]              = $party_array_final[$k1]["party_posts_id"];
            $party_array_final[$k1]["posts_user_id"]         = $party_array_final[$k1]["party_posts_posted_by"];
            $party_array_final[$k1]["posts_title"]           = $party_array_final[$k1]["party_posts_title"];
            $party_array_final[$k1]["posts_content"]         = $party_array_final[$k1]["party_posts_title"];
            $party_array_final[$k1]["posts_photo_or_video"]  = $party_array_final[$k1]["posts_photo_or_video"];
            $party_array_final[$k1]["posts_video_thumbnail"] = $s3_url . "assets/images/partyposts/thumbnail/" . $party_array_final[$k1]["posts_videoparty_posts_video_thumbnail_thumbnail"];
            $party_array_final[$k1]["posts_uploaded_date"]   = $party_array_final[$k1]["party_posts_uploaded_date"];
            $party_array_final[$k1]["posts_type"]            = 1;
            $party_array_final[$k1]["posts_active"]          = $party_array_final[$k1]["party_posts_active"];
            $party_array_final[$k1]["posts_created_type"]    = 1;
            $party_array_final[$k1]["post_order_type"]       = 3;
        }
        $hide_array = [];
        foreach ($get_hide_post as $row) {
            $hide_array[] = $row['hide_posts_post_id'];
        }
        $report_array = [];
        foreach ($get_report_post as $row) {
            $report_array[] = $row['posts_report_post_id'];
        }
        $getFriends = $this->PostsApiModel->getFriends($userid);
        $array_count = count($getFriends);
        $getFriends[$array_count]['friend_id'] = $userid;
        if ($getFriends) {
            foreach ($getFriends as $value => $key) {
                $res_data = $this->PostsApiModel->getPosts($getFriends[$value]['friend_id']);
                foreach ($res_data['data'] as $row) {
                    $res['datas'][] = $row;
                }
            }
        }
        foreach ($res['datas'] as $val => $k) {
            $res['datas'][$val]['post_order_type'] = 2;  // 2 for normal posts
        }
        $i = 0;
        foreach ($res['datas'] as $row) {
            $total_array[$i] = $row['posts_uploaded_date'];
            $i++;
        }
        arsort($total_array);
        foreach ($total_array as $key => $value) {
            $res['data'][] = $res['datas'][$key];
        }
        foreach ($res['data'] as $row_1) {
            $res_by_polls[] = $row_1;
        }
        foreach ($res_by_polls as $a1 => $v1) {
            if ($res_by_polls[$a1]['post_order_type'] == 2)
                $res_by_polls[$a1]['users_photo'] = $this->media_url . "assets/images/users/" . $res_by_polls[$a1]['users_photo'];
        }
        $final_array = [];
        foreach ($res_by_polls as $in_row) {
            if (!in_array($in_row['posts_id'], $hide_array)) {
                $final_array[] = $in_row;
            }
        }
        $final_last_array_sort = [];
        foreach ($final_array as $in_row) {
            if (!in_array($in_row['posts_id'], $report_array)) {
                $final_last_array_sort[] = $in_row;
            }
        }
        foreach ($state_array_sort as $k => $v) {
            $state_array_sort[$k]['post_order_type'] = 4;
        }
        foreach ($state_array_sort as $row) {
            $final_last_array_sort[] = $row;
        }
        $post_count             = count($final_last_array_sort);
        $final_last_array       = array_slice($final_last_array_sort, $page_start, 10);
        $final_last_array_next  = array_slice($final_last_array_sort, $page_start + 10, 10);
        if (!empty($final_last_array)) {
            foreach ($final_last_array as $key => $value) {
                $final_last_array[$key]['is_completed'] = count($final_last_array_next);
                if ($final_last_array[$key]['post_order_type'] == 2) {
                    $pollcount              = $this->PostsApiModel->pollsCount($final_last_array[$key]['posts_id']);
                    $commentcount           = $this->PostsApiModel->commentsCount($final_last_array[$key]['posts_id']);
                    $commentsCountouter     = $this->PostsApiModel->commentsCountouter($final_last_array[$key]['posts_id']);
                    $is_user_polled         = $this->PostsApiModel->isUserpolled($final_last_array[$key]['posts_id'], $userid);
                    foreach ($commentcount['data'] as $co => $va) {
                        $commentcount['data'][$co]['users_photo'] = $this->media_url . "assets/images/users/" . $commentcount['data'][$co]['users_photo'];
                    }
                    $ispolled                                        = ($is_user_polled['count'] > 0) ? true : false;
                    $final_last_array[$key]['is_polled']             = $ispolled;
                    $token                                           = mt_rand(100000, 999999);
                    $final_last_array[$key]['posts_photo_or_video']  = $this->s3_url . 'assets/images/posts/' . $final_last_array[$key]['posts_photo_or_video'];
                    $final_last_array[$key]['posts_video_thumbnail'] = $this->s3_url . 'assets/images/posts/thumbnails/' . $final_last_array[$key]['posts_video_thumbnail'];
                    $final_last_array[$key]['poll_count']            = $pollcount['count'];
                    $final_last_array[$key]['comment_count']         = $commentsCountouter;
                    $final_last_array[$key]['comment']               = $commentcount['data'];
                    $date                                            = new DateTime($final_last_array[$key]['posts_uploaded_date'], $GMT);
                    $date->setTimezone(new DateTimeZone($zone['timezone']));
                    $final_last_array[$key]['posts_uploaded_date']   = $date->format('Y-m-d H:i:s');
                    $final_last_array[$key]['sharecode']             = $this->media_url . 'post-view?postid=' . $final_last_array[$key]['posts_id'] . '&&type=0';
                    $final_last_array[$key]['downloadlink']          = $final_last_array[$key]['posts_photo_or_video'] . '?postid=' . $final_last_array[$key]['posts_id'] . '&&type=0';
                }
            }
            return $final_last_array;
        } else {
            $this->output->set_output(json_encode(array('posts' => 'no data found on server', 'status' => false)));
        }
    }
    //poll posts
    function pollPosts() {

        $this->output->set_content_type('application/json');
        $post_user_id   = $this->input->post('postuserid');
        $posts_id       = $this->input->post('postid');
        $user_id        = $this->input->post('userid');
        if (!empty($posts_id && $user_id)) {
            $res = $this->PostsApiModel->pollPosts($user_id, $posts_id, $post_user_id);
            if ($res['res'] == 1) {
                $this->output->set_output(json_encode(array('is_polled' => true, 'message' => "successfully polled", 'status' => true)));
            } else if ($res['res'] == -1) {
                $this->output->set_output(json_encode(array('is_polled' => false, 'message' => 'successfully unpolled', 'status' => true)));
            }
        } else {
            $this->output->set_output(json_encode(array('message' => "parameter missing", 'status' => false)));
        }
    }
    // share uploaded posts
    function sharePost() {

        $this->output->set_content_type('application/json');
        $posts_id       = $this->input->post('posts_id');
        $user_id        = $this->input->post('posts_user_id');
        $getPosts       = $this->PostsApiModel->getPostsbyid($posts_id);
        $posts_details  = $getPosts[0];
        $country        = (!empty($this->input->post('country'))) ? $this->input->post('country') : $posts_details['posts_country'];
        $state          = (!empty($this->input->post('state'))) ? $this->input->post('state') : $posts_details['posts_state'];
        $district       = (!empty($this->input->post('district'))) ? $this->input->post('district') : $posts_details['posts_city'];
        $area           = (!empty($this->input->post('area'))) ? $this->input->post('area') : $posts_details['posts_area'];
        $insert         = array('posts_user_id' => $user_id, 'posts_title' => $posts_details['posts_title'], 'posts_photo_or_video' => $posts_details['posts_photo_or_video'],
                                'posts_content' => $posts_details['posts_content'], 'posts_type' => $posts_details['posts_type'],
                                'posts_shared_user_id' => $posts_details['posts_user_id'], 'posts_created_type' => 2, 'posts_country' => $country,
                                'posts_state' => $state, 'posts_city' => $district, 'posts_area' => $area
                            );
        if (!empty($posts_details['posts_id'])) {
            $res = $this->PostsApiModel->sharePosts($insert);
            if ($res > 0) {
                $this->output->set_output(json_encode(array('message' => "succesfully shared", 'status' => true)));
            } else {
                $this->output->set_output(json_encode(array('message' => 'failed to share', 'status' => false)));
            }
        } else {
            $this->output->set_output(json_encode(array('message' => 'no data found on this id', 'status' => false)));
        }
    }

    // user profile posts
    function profilePosts() {

        $this->output->set_content_type('application/json');
        $zone   = getallheaders();
        $GMT    = new DateTimeZone("GMT");
        if (empty($zone['timezone']))
            $zone['timezone'] = "Asia/Kolkata";
        $dt             = new DateTime();
        $current_date   = $dt->format('d-m-Y h:i A');
        $date           = new DateTime($current_date, $GMT);
        $date->setTimezone(new DateTimeZone($zone['timezone']));
        $date                 = $date->format('Y-m-d h:i A');
        $page_start           = !empty($this->input->post('page_start')) ? $this->input->post('page_start') : 0;
        $user_id              = $this->input->post('userid');
        $logged_userid        = $this->input->post('loggeduserid');
        $get_hide_post        = $this->PostsApiModel->gethiddenposts($logged_userid);
        $get_report_post      = $this->PostsApiModel->getReportedposts($logged_userid);
        $get_report_challenge = $this->PostsApiModel->getReportedchallenge($logged_userid);
        if (!empty($user_id && $logged_userid)) {
            $getProfile = $this->PostsApiModel->getprofilebyid($user_id);
            $is_friend = $this->PostsApiModel->isFriend($user_id, $logged_userid);
            if ($is_friend > 0) {
                $getPosts = $this->PostsApiModel->getprofilePosts($user_id);
            } else {
                $getPosts = $this->PostsApiModel->getprofilePostsnotfrnd($user_id);
            }
            foreach ($getPosts['data'] as $value => $row) {
                if ($getPosts['data'][$value]['posts_created_type'] == 2) {
                    $getPosts['data'][$value]['posts_shared_user_name'] = $this->PostsApiModel->getUsernameforsharedPost($getPosts['data'][$value]['posts_shared_user_id']);
                }
            }
            $getChallenge = $this->PostsApiModel->getChallengelist($user_id);
            if ($getChallenge != null) {
                foreach ($getChallenge as $value => $key) {
                    $getChallenge[$value]['created_date'] = strtotime($getChallenge[$value]['challenge_created_date']);
                    $now            = new DateTime($zone['timezone']);
                    $expdate        = $getChallenge[$value]['challenge_expired_date'] . " " . $getChallenge[$value]['challenge_expired_time'];
                    $expired_date   = date("Y-m-d H:i:s ", strtotime($expdate));
                    $getChallenge[$value]['cdate'] = $date;
                    $current_date                  = strtotime($date);
                    $exp_date                      = strtotime($expdate);
                    $diff                          = $exp_date - $current_date;
                    $getChallenge[$value]['diff'] = $diff;
                    $getChallenge[$value]['challenge_status'] = $diff > 0 ? 1 : 0;
                    $expired_date                             = new DateTime($expired_date);
                    $now_new          = $now->format('Y-m-d H:i:s');
                    $expired_new      = $expired_date->format('Y-m-d H:i:s');
                    $now_new_date     = new DateTime($now_new);
                    $expired_new_date = new DateTime($expired_new);
                    $interval         = $now_new_date->diff($expired_new_date);
                    $isPolled         = $this->PostsApiModel->isChallengepolled($getChallenge[$value]['challenge_id'], $user_id);
                    $getChallenge[$value]['ispolled'] = $isPolled > 0 ? 1 : 0;
                    if ($diff > 0) {
                        $getChallenge[$value]['validity_day']       = $interval->format('%d');
                        $getChallenge[$value]['validity_hour']      = $interval->format('%h');
                        $getChallenge[$value]['validity_minute']    = $interval->format('%i');
                        $getChallenge[$value]['validity_second']    = $interval->format('%s');
                        $getChallenge[$value]['challenge_winner']   = [];
                    } else {
                        $getChallenge[$value]['validity_day']       = 0;
                        $getChallenge[$value]['validity_hour']      = 0;
                        $getChallenge[$value]['validity_minute']    = 0;
                        $getChallenge[$value]['validity_second']    = 0;
                        $getChallenge[$value]['challenge_winner']   = $this->PostsApiModel->getWinner($getChallenge[$value]['challenge_id']);
                        if ($getChallenge[$value]['challenge_winner']) {
                            foreach ($getChallenge[$value]['challenge_winner'] as $key_1 => $value_1) {
                                if ($getChallenge[$value]['challenge_winner'][$key_1]['users_login_type'] == 0) {
                                    $getChallenge[$value]['challenge_winner'][$key_1]['users_photo'] = $this->media_url . "assets/images/users/" . $getChallenge[$value]['challenge_winner'][$key_1]['users_photo'];
                                }
                                if ($getChallenge[$value]['challenge_type'] == 1) {
                                    $getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post'] = $this->media_url . "assets/images/challenge/type1/" . $getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post'];
                                } else {
                                    $getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post'] = $this->media_url . "assets/images/challenge/type2/" . $getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post'];
                                }
                                $getChallenge[$value]['challenge_winner'][$key_1]['challenge_media_type']            = $getChallenge[$value]['challenge_media_type'];
                                $getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_video_thumbnail'] = $this->s3_url . "assets/images/challenge/thumb/" . $getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_video_thumbnail'];
                            }
                        }
                    }
                    if ($getChallenge[$value]['challenge_posts_sub_type'] == 1) {
                        $explenatory_video = $this->ChallengeApiModel->getExplanetoryvideo($getChallenge[$value]['challenge_id']);
                        if ($explenatory_video) {
                            $getChallenge[$value]['explanetory_video']              = $this->media_url . "assets/images/challenge/type1/" . $explenatory_video[0]['challenge_posts_sub_post'];
                            $getChallenge[$value]['explanetory_video_thumb']        = $this->media_url . "assets/images/challenge/thumb/" . $explenatory_video[0]['challenge_posts_video_thumbnail'];
                            $getChallenge[$value]['challenge_posts_exp_media_type'] = $explenatory_video[0]['challenge_posts_exp_media_type'];
                        }
                    }

                    $getChallenge[$value]['pollCount']              = $this->ChallengeApiModel->challengePollcount($getChallenge[$value]['challenge_id']);
                    $getChallenge[$value]['commentCount']           = $this->ChallengeApiModel->challengeCommentcount($getChallenge[$value]['challenge_id']);
                    $getChallenge[$value]['challenge_posts_post']   = $this->ChallengeApiModel->getChallengeposts($getChallenge[$value]['challenge_id']);
                    $getChallenge[$value]['comment']                = $this->ChallengeApiModel->getChallengecomments($getChallenge[$value]['challenge_id']);
                    $getChallenge[$value]['totalpollscount']        = $this->ChallengeApiModel->totalPostsCount($getChallenge[$value]['challenge_id']);
                    $getChallenge[$value]['post_sub_type']          = 1;
                    foreach ($getChallenge[$value]['comment'] as $pic => $key_pic) {
                        $getChallenge[$value]['comment'][$pic]['users_photo'] = $this->media_url . "assets/images/users/" . $getChallenge[$value]['comment'][$pic]['users_photo'];
                    }
                    if ($getChallenge[$value]['users_login_type'] == 0) {
                        $getChallenge[$value]['users_photo'] = $this->media_url . "assets/images/users/" . $getChallenge[$value]['users_photo'];
                    } else {
                        if ($getChallenge[$value]['users_photo']) {
                            $getChallenge[$value]['users_photo'] = $this->media_url . "assets/images/users/" . $getChallenge[$value]['users_photo'];
                        }
                    }
                    foreach ($getChallenge[$value]['challenge_posts_post'] as $values => $keys) {
                        $getChallenge[$value]['challenge_posts_post'][$values]['users_photo']   = $this->media_url . "assets/images/users/" . $getChallenge[$value]['challenge_posts_post'][$values]['users_photo'];
                        $getChallenge[$value]['challenge_posts_post'][$values]['pollCount']     = $this->ChallengeApiModel->challengepostPollcount($getChallenge[$value]['challenge_id'], $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_id']);
                        if ($getChallenge[$value]['totalpollscount'] > 0) {
                            $getChallenge[$value]['challenge_posts_post'][$values]['pollPercent'] = intval(($getChallenge[$value]['challenge_posts_post'][$values]['pollCount'] * 100 ) / $getChallenge[$value]['totalpollscount']);
                        } else {
                            $getChallenge[$value]['challenge_posts_post'][$values]['pollPercent'] = 0;
                        }
                        $getChallenge[$value]['challenge_posts_post'][$values]['challenge_media_type'] = $getChallenge[$value]['challenge_media_type'];
                        $isPolledpost = $this->ChallengeApiModel->isChallengepostpolled($getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_id'], $user_id);
                        $getChallenge[$value]['challenge_posts_post'][$values]['ispolled']      = $isPolledpost > 0 ? 1 : 0;
                        $getChallenge[$value]['challenge_posts_post'][$values]['commentCount']  = $this->ChallengeApiModel->challengepostCommentcount($getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_id']);
                        if ($getChallenge[$value]['challenge_type'] == 1) {
                            $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'] = $this->s3_url . "assets/images/challenge/type1/" . $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                        } else {
                            $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'] = $this->s3_url . "assets/images/challenge/type2/" . $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                        }
                        $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_video_thumbnail'] = $this->s3_url . "assets/images/challenge/thumb/" . $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_video_thumbnail'];
                        $getChallenge[$value]['challenge_posts_post'][$values]['downloadlink']                    = $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                        $getChallenge[$value]['challenge_posts_post'][$values]['downloadlink']                    = $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'] . '?postid=' . $getChallenge[$value]['challenge_id'] . '&&type=3';
                        $getChallenge[$value]['challenge_posts_post'][$values]['sharecode']                       = $this->media_url . 'post-view?postid=' . $getChallenge[$value]['challenge_id'] . '&&type=3';
                    }
                }
            }

            if ($getProfile) {
                $getProfile = $getProfile[0];
                if ($getProfile['users_username'] == null)
                    $getProfile['users_username'] = "";
                $getProfile['users_photo'] = $this->media_url . 'assets/images/users/' . $getProfile['users_photo'];
            }
            if (!empty($getPosts['data']) || !empty($getChallenge)) {
                foreach ($getPosts['data'] as $key => $value) {
                    $date   = new DateTime($getPosts['data'][$key]['posts_uploaded_date'], $GMT);
                    $date->setTimezone(new DateTimeZone($zone['timezone']));
                    $getPosts['data'][$key]['posts_uploaded_date']   = $date->format('Y-m-d H:i:s');
                    $getPosts['data'][$key]['posts_photo_or_video']  = $this->s3_url . 'assets/images/posts/' . $getPosts['data'][$key]['posts_photo_or_video'];
                    $pollcount                                       = $this->PostsApiModel->pollsCount($getPosts['data'][$key]['posts_id']);
                    $commentcount                                    = $this->PostsApiModel->commentsCount($getPosts['data'][$key]['posts_id']);
                    $commentsCountouter                              = $this->PostsApiModel->commentsCountouter($getPosts['data'][$key]['posts_id']);
                    $is_user_polled                                  = $this->PostsApiModel->isUserpolled($getPosts['data'][$key]['posts_id'], $logged_userid);
                    $getPosts['data'][$key]['posts_video_thumbnail'] = $this->s3_url . "assets/images/posts/thumbnails/" . $getPosts['data'][$key]['posts_video_thumbnail'];
                    foreach ($commentcount['data'] as $co => $va) {
                        $commentcount['data'][$co]['users_photo']    = $this->media_url . "assets/images/users/" . $commentcount['data'][$co]['users_photo'];
                    }
                    $ispolled = ($is_user_polled['count'] > 0) ? true : false;
                    $getPosts['data'][$key]['created_date']     = strtotime(date("d-m-Y h:i A", strtotime($getPosts['data'][$key]['posts_uploaded_date'])));
                    $getPosts['data'][$key]['comment_count']    = $commentsCountouter;
                    $getPosts['data'][$key]['poll_count']       = $pollcount['count'];
                    $getPosts['data'][$key]['users_username']   = $getProfile['users_username'];
                    $getPosts['data'][$key]['users_name']       = $getProfile['users_name'];
                    $getPosts['data'][$key]['users_photo']      = $getProfile['users_photo'];
                    $getPosts['data'][$key]['is_polled']        = $ispolled;
                    $getPosts['data'][$key]['comment']          = $commentcount['data'];
                    $getPosts['data'][$key]['post_sub_type']    = 0;
                    $getPosts['data'][$key]['sharecode']        = $this->media_url . 'post-view?postid=' . $getPosts['data'][$key]['posts_id'] . '&&type=0';
                    $getPosts['data'][$key]['downloadlink']     = $getPosts['data'][$key]['posts_photo_or_video'] . '?postid=' . $getPosts['data'][$key]['posts_id'] . '&&type=0';
                }
                foreach ($getChallenge as $row) {
                    $getPosts['data'][] = $row;
                }
                $i = 0;
                foreach ($getPosts['data'] as $row) {
                    $totalArray[$i] = $row['created_date'];
                    $i++;
                }
                arsort($totalArray);
                foreach ($totalArray as $tot_key => $tot_val) {
                    $resultArray[] = $getPosts['data'][$tot_key];
                }
                $k = 0;
                foreach ($resultArray as $row_count => $key_count) {
                    if ($resultArray[$row_count]['post_sub_type'] == 1) {
                        $resultArray[$row_count]['parent_position'] = $k;
                        foreach ($resultArray[$row_count]['challenge_posts_post'] as $sub_row_count => $key_row_count) {
                            $resultArray[$row_count]['challenge_posts_post'][$sub_row_count]['parent_position'] = $k;
                        }
                    }
                    $k++;
                }
                $hide_array = [];
                foreach ($get_hide_post as $row) {
                    $hide_array[] = $row['hide_posts_post_id'];
                }
                $report_array = [];
                foreach ($get_report_post as $row) {
                    $report_array[] = $row['posts_report_post_id'];
                }
                $challenge_report_array = [];
                foreach ($get_report_challenge as $row) {
                    $challenge_report_array[] = $row['challenge_report_challenge_id'];
                }
                $final_post_array = [];
                foreach ($resultArray as $in_row) {
                    if (!in_array($in_row['posts_id'], $hide_array)) {
                        $final_post_array[] = $in_row;
                    }
                }
                $final_challenge_array = [];
                foreach ($final_post_array as $in_row) {
                    if (!in_array($in_row['challenge_id'], $challenge_report_array)) {
                        $final_challenge_array[] = $in_row;
                    }
                }
                $final_last_array = [];
                foreach ($final_challenge_array as $in_row) {
                    if (!in_array($in_row['posts_id'], $report_array)) {
                        $final_last_array[] = $in_row;
                    }
                }
                $final_last_challenge_array = [];
                foreach ($final_last_array as $in_row) {
                    if (!in_array($in_row['challenge_id'], $report_array)) {
                        $final_last_challenge_array[] = $in_row;
                    }
                }
                $total_posts = count($final_last_challenge_array);
                $profile_posts_final = array_slice($final_last_challenge_array, $page_start, 10);
                $profile_posts_final_next = array_slice($final_last_challenge_array, $page_start + 10, 10);
            }
            if ($profile_posts_final) {
                $this->output->set_output(json_encode(array('report_challenge_id' => $get_report_challenge, 'hide_posts_id' => $get_hide_post, 'report_posts_id' => $get_report_post, 'posts' => $profile_posts_final, 'total_posts' => $total_posts, 'is_completed' => count($profile_posts_final_next) > 0 ? 0 : 1, 'status' => true)));
            } else {
                $this->output->set_output(json_encode(array('posts' => 'no data found', 'status' => false)));
            }
        } else {
            $this->output->set_output(json_encode(array('posts' => 'parameter missing', 'status' => false)));
        }
    }

    // user profile posts
    function returnprofilePosts($user_id, $logged_userid, $page_start) {

        $this->output->set_content_type('application/json');
        $zone   = getallheaders();
        $GMT    = new DateTimeZone("GMT");
        if (empty($zone['timezone']))
            $zone['timezone'] = "Asia/Kolkata";

        $dt                   = new DateTime();
        $current_date         = $dt->format('d-m-Y h:i A');
        $date                 = new DateTime($current_date, $GMT);
        $date->setTimezone(new DateTimeZone($zone['timezone']));
        $date                 = $date->format('Y-m-d h:i A');
        $page_start           = !empty($this->input->post('page_start')) ? $this->input->post('page_start') : 0;
        $get_hide_post        = $this->PostsApiModel->gethiddenposts($logged_userid);
        $get_report_post      = $this->PostsApiModel->getReportedposts($logged_userid);
        $get_report_challenge = $this->PostsApiModel->getReportedchallenge($logged_userid);
        if (!empty($user_id && $logged_userid)) {
            $getProfile = $this->PostsApiModel->getprofilebyid($user_id);
            $is_friend  = $this->PostsApiModel->isFriend($user_id, $logged_userid);
            if ($is_friend > 0) {
                $getPosts = $this->PostsApiModel->getprofilePosts($user_id);
            } else {
                $getPosts = $this->PostsApiModel->getprofilePostsnotfrnd($user_id);
            }
            $getChallenge = $this->PostsApiModel->getChallengelist($user_id);
            if ($getChallenge != null) {
                foreach ($getChallenge as $value => $key) {
                    $getChallenge[$value]['created_date'] = strtotime($getChallenge[$value]['challenge_created_date']);
                    $now              = new DateTime($zone['timezone']);
                    $expdate          = $getChallenge[$value]['challenge_expired_date'] . " " . $getChallenge[$value]['challenge_expired_time'];
                    $expired_date     = date("Y-m-d H:i:s ", strtotime($expdate));
                    $getChallenge[$value]['cdate'] = $date;
                    $current_date     = strtotime($date);
                    $exp_date         = strtotime($expdate);
                    $diff             = $exp_date - $current_date;
                    $getChallenge[$value]['diff'] = $diff;
                    $getChallenge[$value]['challenge_status'] = $diff > 0 ? 1 : 0;
                    $expired_date     = new DateTime($expired_date);
                    $now_new          = $now->format('Y-m-d H:i:s');
                    $expired_new      = $expired_date->format('Y-m-d H:i:s');
                    $now_new_date     = new DateTime($now_new);
                    $expired_new_date = new DateTime($expired_new);
                    $interval         = $now_new_date->diff($expired_new_date);
                    $isPolled         = $this->PostsApiModel->isChallengepolled($getChallenge[$value]['challenge_id'], $user_id);
                    $getChallenge[$value]['ispolled'] = $isPolled > 0 ? 1 : 0;
                    if ($diff > 0) {
                        $getChallenge[$value]['validity_day']       = $interval->format('%d');
                        $getChallenge[$value]['validity_hour']      = $interval->format('%h');
                        $getChallenge[$value]['validity_minute']    = $interval->format('%i');
                        $getChallenge[$value]['validity_second']    = $interval->format('%s');
                        $getChallenge[$value]['challenge_winner']   = [];
                    } else {
                        $getChallenge[$value]['validity_day']     = 0;
                        $getChallenge[$value]['validity_hour']    = 0;
                        $getChallenge[$value]['validity_minute']  = 0;
                        $getChallenge[$value]['validity_second']  = 0;
                        $getChallenge[$value]['challenge_winner'] = $this->PostsApiModel->getWinner($getChallenge[$value]['challenge_id']);
                        if ($getChallenge[$value]['challenge_winner']) {
                            foreach ($getChallenge[$value]['challenge_winner'] as $key_1 => $value_1) {
                                if ($getChallenge[$value]['challenge_winner'][$key_1]['users_login_type'] == 0) {
                                    $getChallenge[$value]['challenge_winner'][$key_1]['users_photo'] = $this->media_url . "assets/images/users/" . $getChallenge[$value]['challenge_winner'][$key_1]['users_photo'];
                                }
                                if ($getChallenge[$value]['challenge_type'] == 1) {
                                    $getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post'] = $this->media_url . "assets/images/challenge/type1/" . $getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post'];
                                } else {
                                    $getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post'] = $this->media_url . "assets/images/challenge/type2/" . $getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post'];
                                }
                                $getChallenge[$value]['challenge_winner'][$key_1]['challenge_media_type']            = $getChallenge[$value]['challenge_media_type'];
                                $getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_video_thumbnail'] = $this->media_url . "assets/images/challenge/thumb/" . $getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_video_thumbnail'];
                            }
                        }
                    }
                    if ($getChallenge[$value]['challenge_posts_sub_type'] == 1) {
                        $explenatory_video = $this->ChallengeApiModel->getExplanetoryvideo($getChallenge[$value]['challenge_id']);
                        if ($explenatory_video) {
                            $getChallenge[$value]['explanetory_video']              = $this->media_url . "assets/images/challenge/type1/" . $explenatory_video[0]['challenge_posts_sub_post'];
                            $getChallenge[$value]['explanetory_video_thumb']        = $this->media_url . "assets/images/challenge/thumb/" . $explenatory_video[0]['challenge_posts_video_thumbnail'];
                            $getChallenge[$value]['challenge_posts_exp_media_type'] = $explenatory_video[0]['challenge_posts_exp_media_type'];
                        }
                    }
                    $getChallenge[$value]['pollCount']              = $this->ChallengeApiModel->challengePollcount($getChallenge[$value]['challenge_id']);
                    $getChallenge[$value]['commentCount']           = $this->ChallengeApiModel->challengeCommentcount($getChallenge[$value]['challenge_id']);
                    $getChallenge[$value]['challenge_posts_post']   = $this->ChallengeApiModel->getChallengeposts($getChallenge[$value]['challenge_id']);
                    $getChallenge[$value]['comment']                = $this->ChallengeApiModel->getChallengecomments($getChallenge[$value]['challenge_id']);
                    $getChallenge[$value]['totalpollscount']        = $this->ChallengeApiModel->totalPostsCount($getChallenge[$value]['challenge_id']);
                    $getChallenge[$value]['post_sub_type']          = 1;
                    foreach ($getChallenge[$value]['comment'] as $pic => $key_pic) {
                        $getChallenge[$value]['comment'][$pic]['users_photo'] = $this->media_url . "assets/images/users/" . $getChallenge[$value]['comment'][$pic]['users_photo'];
                    }
                    if ($getChallenge[$value]['users_login_type'] == 0) {
                        $getChallenge[$value]['users_photo'] = $this->media_url . "assets/images/users/" . $getChallenge[$value]['users_photo'];
                    }
                    foreach ($getChallenge[$value]['challenge_posts_post'] as $values => $keys) {
                        $getChallenge[$value]['challenge_posts_post'][$values]['users_photo'] = $this->media_url . "assets/images/users/" . $getChallenge[$value]['challenge_posts_post'][$values]['users_photo'];
                        $getChallenge[$value]['challenge_posts_post'][$values]['pollCount'] = $this->ChallengeApiModel->challengepostPollcount($getChallenge[$value]['challenge_id'], $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_id']);
                        if ($getChallenge[$value]['totalpollscount'] > 0) {

                            $getChallenge[$value]['challenge_posts_post'][$values]['pollPercent'] = intval(($getChallenge[$value]['challenge_posts_post'][$values]['pollCount'] * 100 ) / $getChallenge[$value]['totalpollscount']);
                        } else {
                            $getChallenge[$value]['challenge_posts_post'][$values]['pollPercent'] = 0;
                        }
                        $getChallenge[$value]['challenge_posts_post'][$values]['challenge_media_type'] = $getChallenge[$value]['challenge_media_type'];
                        $isPolledpost   = $this->ChallengeApiModel->isChallengepostpolled($getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_id'], $user_id);
                        $getChallenge[$value]['challenge_posts_post'][$values]['isPolled'] = $isPolledpost > 0 ? 1 : 0;
                        if ($getChallenge[$value]['challenge_type'] == 1) {
                            $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'] = $this->media_url . "assets/images/challenge/type1/" . $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                        } else {
                            $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'] = $this->media_url . "assets/images/challenge/type2/" . $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                        }
                        $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_video_thumbnail'] = $this->media_url . "assets/images/challenge/thumb/" . $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_video_thumbnail'];
                        $getChallenge[$value]['challenge_posts_post'][$values]['downloadlink'] = $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                    }
                }
            }
            if ($getProfile) {
                $getProfile = $getProfile[0];
                if ($getProfile['users_username'] == null)
                    $getProfile['users_username'] = "";
                $getProfile['users_photo'] = $this->media_url . 'assets/images/users/' . $getProfile['users_photo'];
            }
            if (!empty($getPosts['data'])) {
                foreach ($getPosts['data'] as $key => $value) {
                    $date   = new DateTime($getPosts['data'][$key]['posts_uploaded_date'], $GMT);
                    $date->setTimezone(new DateTimeZone($zone['timezone']));
                    $getPosts['data'][$key]['posts_uploaded_date']   = $date->format('Y-m-d H:i:s');

                    $getPosts['data'][$key]['posts_photo_or_video']  = $this->s3_url . 'assets/images/posts/' . $getPosts['data'][$key]['posts_photo_or_video'];
                    $pollcount                                       = $this->PostsApiModel->pollsCount($getPosts['data'][$key]['posts_id']);
                    $commentcount                                    = $this->PostsApiModel->commentsCount($getPosts['data'][$key]['posts_id']);
                    $commentsCountouter                              = $this->PostsApiModel->commentsCountouter($getPosts['data'][$key]['posts_id']);
                    $is_user_polled                                  = $this->PostsApiModel->isUserpolled($getPosts['data'][$key]['posts_id'], $logged_userid);
                    $getPosts['data'][$key]['posts_video_thumbnail'] = $this->s3_url . "assets/images/posts/thumbnails/" . $getPosts['data'][$key]['posts_video_thumbnail'];
                    foreach ($commentcount['data'] as $co => $va) {
                        $commentcount['data'][$co]['users_photo'] = $this->media_url . "assets/images/users/" . $commentcount['data'][$co]['users_photo'];
                    }
                    $ispolled = ($is_user_polled['count'] > 0) ? true : false;
                    $getPosts['data'][$key]['created_date']     = strtotime(date("d-m-Y h:i A", strtotime($getPosts['data'][$key]['posts_uploaded_date'])));
                    $getPosts['data'][$key]['comment_count']    = $commentsCountouter;
                    $getPosts['data'][$key]['poll_count']       = $pollcount['count'];
                    $getPosts['data'][$key]['users_username']   = $getProfile['users_username'];
                    $getPosts['data'][$key]['users_name']       = $getProfile['users_name'];
                    $getPosts['data'][$key]['users_photo']      = $getProfile['users_photo'];
                    $getPosts['data'][$key]['is_polled']        = $ispolled;
                    $getPosts['data'][$key]['comment']          = $commentcount['data'];
                    $getPosts['data'][$key]['post_sub_type']    = 0;
                }
                foreach ($getChallenge as $row) {
                    $getPosts['data'][] = $row;
                }
                $i = 0;
                foreach ($getPosts['data'] as $row) {
                    $totalArray[$i] = $row['created_date'];
                    $i++;
                }
                arsort($totalArray);
                foreach ($totalArray as $tot_key => $tot_val) {
                    $resultArray[] = $getPosts['data'][$tot_key];
                }
                $k = 0;
                foreach ($resultArray as $row_count => $key_count) {
                    if ($resultArray[$row_count]['post_sub_type'] == 1) {
                        $resultArray[$row_count]['parent_position'] = $k;
                        foreach ($resultArray[$row_count]['challenge_posts_post'] as $sub_row_count => $key_row_count) {
                            $resultArray[$row_count]['challenge_posts_post'][$sub_row_count]['parent_position'] = $k;
                        }
                    }
                    $k++;
                }
                $hide_array = [];
                foreach ($get_hide_post as $row) {
                    $hide_array[] = $row['hide_posts_post_id'];
                }
                $report_array = [];
                foreach ($get_report_post as $row) {
                    $report_array[] = $row['posts_report_post_id'];
                }
                $challenge_report_array = [];
                foreach ($get_report_challenge as $row) {
                    $challenge_report_array[] = $row['challenge_report_challenge_id'];
                }
                $final_post_array = [];
                foreach ($resultArray as $in_row) {
                    if (!in_array($in_row['posts_id'], $hide_array)) {
                        $final_post_array[] = $in_row;
                    }
                }
                $final_challenge_array = [];
                foreach ($final_post_array as $in_row) {
                    if (!in_array($in_row['challenge_id'], $challenge_report_array)) {
                        $final_challenge_array[] = $in_row;
                    }
                }
                $final_last_array = [];
                foreach ($final_challenge_array as $in_row) {
                    if (!in_array($in_row['posts_id'], $report_array)) {
                        $final_last_array[] = $in_row;
                    }
                }
                $final_last_challenge_array = [];
                foreach ($final_last_array as $in_row) {
                    if (!in_array($in_row['challenge_id'], $report_array)) {
                        $final_last_challenge_array[] = $in_row;
                    }
                }
                $total_posts              = count($final_last_challenge_array);
                $profile_posts_final      = array_slice($final_last_challenge_array, $page_start, 10);
                $profile_posts_final_next = array_slice($final_last_challenge_array, $page_start + 10, 10);
                foreach ($profile_posts_final as $k => $v) {
                    $profile_posts_final[$k]['is_completed_profile'] = count($profile_posts_final_next);
                }
                return $profile_posts_final;
            } else {
                return "no data found";
            }
        } else {
            $this->output->set_output(json_encode(array('posts' => 'parameter missing', 'status' => false)));
        }
    }
    // hide profile posts
    function hideprofilePost() {

        $this->output->set_content_type('application/json');
        $user_id        = $this->input->post('user_id');
        $post_id        = $this->input->post('post_id');
        $blockPost      = $this->PostsApiModel->hideprofilePost($user_id, $post_id);
        if ($blockPost > 0) {
            $this->output->set_output(json_encode(array('message' => "successfully updated", 'status' => true)));
        } else {
            $this->output->set_output(json_encode(array('posts' => "failed to update", 'status' => false)));
        }
    }
    // report posts
    function reportPost() {

        $this->output->set_content_type('application/json');
        $user_id        = $this->input->post('user_id');
        $post_id        = $this->input->post('post_id');
        $msg            = $this->input->post('message');
        $type           = $this->input->post('type');
        $logged_userid  = $this->input->post('logged_userid');
        $country        = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;
        $state          = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district       = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;
        $area           = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;
        $page_start     = !empty($this->input->post('page_start')) ? $this->input->post('page_start') : 0;
        if (!empty($user_id && $post_id)) {
            $res = $this->PostsApiModel->reportPost($logged_userid, $post_id, $msg);
            switch ($res) {
                case $res > 0:
                                $this->PostsApiModel->updateReportcount($post_id, $msg);
                                //check report count
                                $remove_post = $this->PostsApiModel->removePostbyCount($post_id);
                    switch ($type) {
                        case 0:
                                $posts        = $this->returngetPostsnew($user_id, $post_id, $type, $country, $state, $district, $area, $page_start);
                                $is_completed = $this->check_get_posts_next_array($userid, $page_start, $country, $state, $district, $area);
                                if (count($posts) >= 9)
                                    $last_posts = $posts[9];
                                else
                                    $last_posts = null;
                                if ($last_posts == null)
                                    $this->output->set_output(json_encode(array('value' => "operation success", 'is_completed' => 1, 'status' => true)));
                                else {
                                    if ($is_completed > 0)
                                        $this->output->set_output(json_encode(array('value' => "operation success", 'data' => array($last_posts), 'is_completed' => 0, 'status' => true)));
                                    else
                                        $this->output->set_output(json_encode(array('value' => "operation success", 'data' => array($last_posts), 'is_completed' => 1, 'status' => true)));
                                }
                                break;
                        case 1:
                                $posts = $this->returnprofilePosts($user_id, $logged_userid, $page_start);
                                if (count($posts) >= 9)
                                    $last_posts = $posts[9];
                                else
                                    $last_posts = null;
                                if ($last_posts == null)
                                    $this->output->set_output(json_encode(array('value' => "successfully reported", 'is_completed' => 1, 'status' => true)));
                                else {
                                    if ($last_posts['is_completed_profile'] > 0)
                                        $this->output->set_output(json_encode(array('value' => "successfully reported", 'data' => array($last_posts), 'is_completed' => 0, 'status' => true)));
                                    else
                                        $this->output->set_output(json_encode(array('value' => "successfully reported", 'data' => array($last_posts), 'is_completed' => 1, 'status' => true)));
                                }
                                break;
                    }
                    break;
                case -1:
                            $this->output->set_output(json_encode(array('value' => "already reported", 'status' => false)));
                            break;
                case 0:
                            $this->output->set_output(json_encode(array('value' => "operation failed", 'status' => false)));
                            break;
            }
        }
        else {
            $this->output->set_output(json_encode(array('value' => "parameter missing", 'status' => false)));
        }
    }

    //comment posts api
    function commentPost() {
        $this->output->set_content_type('application/json');
        $post_id        = $this->input->post('post_id');
        $user_id        = $this->input->post('user_id');
        $post_user_id   = $this->input->post('postuserid');
        $comment        = $this->input->post('comment');
        if(!empty($post_id && $user_id && $comment)) {
            $res = $this->PostsApiModel->commentPost($user_id, $post_id, $comment, $post_user_id);
            if ($res > 0) {
                $this->output->set_output(json_encode(array('message' => "successfully commented", 'comment' => $res['data'], 'status' => true)));
            }else {
                $this->output->set_output(json_encode(array('posts' => "failed to comment", 'status' => false)));
            }
        }else {
            $this->output->set_output(json_encode(array('posts' => "parameter missing", 'status' => false)));
        }
    }
    function likeComment() {
        $this->output->set_content_type('application/json');
        $commentid      = $this->input->post('commentid');
        $userid         = $this->input->post('userid');
        $commentuserid  = $this->input->post('commentuserid');
        if (!empty($commentid && $userid && $commentuserid)) {
            $insert = array('comment_likes_comment_id' => $commentid,
                            'comment_likes_user_id' => $userid,
                            'comment_likes_comment_user_id' => $commentuserid
                        );
            $res = $this->CommonModel->likeComment($insert, 'posts_comment_likes');
            if ($res == 1)
                $this->output->set_output(json_encode(array('message' => 'liked', 'status' => true)));
            else if ($res == -1)
                $this->output->set_output(json_encode(array('message' => 'unliked', 'status' => true)));
        }else {
            $this->output->set_output(json_encode(array('message' => "parameter missing", 'status' => false)));
        }
    }
    // get post comments
    function getPostcomments() {
        
        $this->output->set_content_type('application/json');
        $post_id    = $this->input->post('post_id');
        $res        = $this->PostsApiModel->getPostcomments($post_id);
        if ($res) {
            foreach ($res as $key => $value) {
                $res[$key]['users_photo'] = $this->media_url . "assets/images/users/" . $res[$key]['users_photo'];
                $res_reply                = $this->PostsApiModel->getcommentsreply($res[$key]['posts_comments_id'], $res[$key]['posts_comments_post_id']);
                foreach ($res_reply as $val => $keys) {
                    $res_reply[$val]['users_photo']   = $this->media_url . "assets/images/users/" . $res_reply[$val]['users_photo'];
                    $res_reply[$val]['reply_to_user'] = $this->PostsApiModel->getusersnamebyid($res_reply[$val]['posts_comment_reply_comment_user_id']);
                }
                $res[$key]['reply'] = $res_reply;
            }
        }
        if ($res != null) {
            $this->output->set_output(json_encode(array('data' => $res, 'status' => true)));
        } else {
            $this->output->set_output(json_encode(array('data' => "no value found", 'status' => false)));
        }
    }
    // get post comments by id
    function getPostcommentsbyid() {

        $this->output->set_content_type('application/json');
        $post_id    = $this->input->post('comment_id');
        $res        = $this->PostsApiModel->getPostcommentsbyid($post_id);
        foreach ($res as $key => $value) {
            $res[$key]['users_photo'] = $this->media_url . "assets/images/users/" . $res[$key]['users_photo'];
            $res_reply                = $this->PostsApiModel->getcommentsreply($res[$key]['posts_comments_id'], $res[$key]['posts_comments_post_id']);
            foreach ($res_reply as $val => $keys) {
                $res_reply[$val]['users_photo']     = $this->media_url . "assets/images/users/" . $res_reply[$val]['users_photo'];
                $res_reply[$val]['reply_to_user']   = $this->PostsApiModel->getusersnamebyid($res_reply[$val]['posts_comment_reply_comment_user_id']);
            }
            $res[$key]['reply'] = $res_reply;
        }
        if ($res != null) {
            $this->output->set_output(json_encode(array('data' => $res, 'status' => true)));
        } else {
            $this->output->set_output(json_encode(array('data' => "no value found", 'status' => false)));
        }
    }
    // reply to comments
    function replyPostscomments() {

        $this->output->set_content_type('application/json');
        $post_id        = $this->input->post('post_id');
        $user_id        = $this->input->post('user_id');
        $comment_id     = $this->input->post('comment_id');
        $comment_user   = !empty($this->input->post('comment_user_id')) ? $this->input->post('comment_user_id') : 1;
        $reply          = $this->input->post('reply');
        $type           = (!empty($this->input->post('type'))) ? $this->input->post('type') : 0;    // 0 => comment user id , 1 => reply user id
        $insert = array('posts_comments_reply_posts_id' => $post_id,
                        'posts_comments_reply_comment_id' => $comment_id,
                        'posts_comments_reply' => $reply,
                        'posts_comment_reply_comment_user_id' => $comment_user,
                        'posts_comments_reply_user_id' => $user_id
                    );
        $res            = $this->PostsApiModel->replyPostscomments($insert, $comment_user, $type);
        if ($res > 0) {
            $this->output->set_output(json_encode(array('message' => "succesfully inserted", 'type' => $res, 'status' => true)));
        } else {
            $this->output->set_output(json_encode(array('posts' => "no value found", 'status' => false)));
        }
    }
    //sort by categories Post old
    function sortPostsold() {
        $status_user = true;
        $status_post = true;
        $this->output->set_content_type('application/json');
        $country    = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;
        $state      = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district   = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;
        $area       = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;
        $res        = $this->PostsApiModel->sortPosts($country, $state, $district, $area);
        if ($res > 0) {
            foreach ($res as $key => $value) {
                $res[$key]['posts_photo_or_video'] = $this->s3_url . 'assets/images/posts/' . $res[$key]['posts_photo_or_video'];
            }
        } else {
            $status_post = 'false';
        }
        $this->output->set_output(json_encode(array('posts' => $res, 'status_post' => $status_post)));
    }
    //sort by categories Post
    function sortPosts() {

        $zone   = getallheaders();
        $GMT    = new DateTimeZone("GMT");
        if (empty($zone['timezone']))
            $zone['timezone'] = "Asia/Kolkata";
        $userid          = !empty($this->input->post('userid')) ? $this->input->post('userid') : 1;
        $page_start      = !empty($this->input->post('page_start')) ? $this->input->post('page_start') : 0;
        $this->output->set_content_type('application/json');
        $country         = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;
        $state           = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district        = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;
        $area            = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;
        $res_user        = $this->UsersApiModel->sortUsers($country, $state, $district, $area);
        $res             = $this->PostsApiModel->sortPosts($country, $state, $district, $area);
        $get_report_post = $this->PostsApiModel->getReportedposts($userid);
        $get_hide_post = $this->PostsApiModel->gethiddenposts($userid);
        if ($res_user != 0) {
            $status_user = true;
            foreach ($res_user as $keys => $values) {
                $res_user[$keys]['users_photo'] = $this->media_url . 'assets/images/users/' . $res_user[$keys]['users_photo'];
                $res_user[$keys]['posts_count'] = $this->UsersApiModel->getpostCount($res_user[$keys]['user_id']);
            }
        } else {
            $status_user = false;
        }
        $date     = new DateTime(date("Y-m-d H:i:s", strtotime("now")), $GMT);
        $date->setTimezone(new DateTimeZone($zone['timezone']));
        $datetime = $date->format('Y-m-d H:i:s');
        if($res != 0){
            foreach ($res as $key => $value) {
                $uploaddate     = new DateTime($res[$key]['posts_uploaded_date'], $GMT);
                $uploaddate->setTimezone(new DateTimeZone($zone['timezone']));
                $res[$key]['posts_uploaded_date'] = $uploaddate->format('Y-m-d H:i:s');
                $uploaddate     = new DateTime($res[$key]['posts_uploaded_date'], $GMT);
                $uploaddate->setTimezone(new DateTimeZone($zone['timezone']));
                $datePercent    = strtotime($datetime) - strtotime($res[$key]['posts_uploaded_date']);
                $pollCount      = $this->PostsApiModel->getPostspollCount($res[$key]['posts_id']);
                $res[$key]['pollPercent']           = $pollCount > 0 ? intval($datePercent / $pollCount) : 0;
                $res[$key]['posts_photo_or_video']  = $this->s3_url . 'assets/images/posts/' . $res[$key]['posts_photo_or_video'];
                $res[$key]['posts_video_thumbnail'] = $this->s3_url . 'assets/images/posts/thumbnails/' . $res[$key]['posts_video_thumbnail'];
            }
        }
        $sort_array = array();
        $res_2      = array();
        if ($res) {
            foreach ($res as $res_1 => $key_1) {
                if ($key_1['pollPercent'] > 0) {
                    $sort_array[$key_1['pollPercent']] = $res[$res_1];
                }
            }
            ksort($sort_array);
            foreach ($sort_array as $res_2[]);
        }
        $hide_array = [];
        foreach ($get_hide_post as $row) {
            $hide_array[] = $row['hide_posts_post_id'];
        }
        $report_array = [];
        foreach ($get_report_post as $row) {
            $report_array[] = $row['posts_report_post_id'];
        }
        $final_array = [];
        foreach ($res_2 as $in_row) {
            if (!in_array($in_row['posts_id'], $hide_array)) {
                $final_array[] = $in_row;
            }
        }
        $final_last_array = [];
        foreach ($final_array as $in_row) {
            if (!in_array($in_row['posts_id'], $report_array)) {
                $final_last_array[] = $in_row;
            }
        }
        $total_posts      = count($final_last_array);
        $final_posts      = array_slice($final_last_array, $page_start, 10);
        $final_posts_next = array_slice($final_last_array, $page_start + 10, 10);
        if ($final_posts) {
            $status_post = true;
        } else {
            $status_post = false;
        }
        $this->output->set_output(json_encode(array('users' => $res_user, 'hidden_post_id' => $get_hide_post, 'report_post_id' => $get_report_post, 'posts' => $final_posts, 'total_posts' => $total_posts, 'status_user' => $status_user, 'status_post' => $status_post, 'is_completed' => count($final_posts_next) > 0 ? 0 : 1,)));
    }

    //sort by categories Post new code 
    function sortPosts_new() {
        $status_user    = true;
        $status_post    = true;
        $this->output->set_content_type('application/json');
        $country        = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;
        $state          = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district       = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;
        $area           = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;
        $res_user       = $this->UsersApiModel->sortUsers_new($country, $state, $district, $area);
        $res            = $this->PostsApiModel->sortPosts_new($country, $state, $district, $area);
        if ($res_user > 0) {
            foreach ($res_user as $keys => $values) {
                $res_user[$keys]['users_photo'] = $this->media_url . 'assets/images/users/' . $res_user[$keys]['users_photo'];
                $res_user[$keys]['posts_count'] = $this->UsersApiModel->getpostCount($res_user[$keys]['user_id']);
            }
        } else {
            $status_user = 'false';
        }
        if ($res > 0) {
            foreach ($res as $key => $value) {
                $res[$key]['posts_photo_or_video'] = $this->s3_url . 'assets/images/posts/' . $res[$key]['posts_photo_or_video'];
            }
        } else {
            $status_post = 'false';
        }
        $this->output->set_output(json_encode(array('users' => $res_user, 'posts' => $res, 'status_user' => $status_user, 'status_post' => $status_post)));
    }
    // get posts api by posts id
    function getnotificationPostsbyid() {

        $this->output->set_content_type('application/json');
        $postsid    = $this->input->post('postid');
        $userid     = $this->input->post('userid');
        if (!empty($postsid)) {
            $res                = $this->PostsApiModel->getnotificationPostsbyid($postsid);
            $pollcount          = $this->PostsApiModel->pollsCount($postsid);
            $isPostpolled       = $this->PostsApiModel->isPostpolled($postsid, $userid);
            $commentcount       = $this->PostsApiModel->commentsCount($postsid);
            $commentsCountouter = $this->PostsApiModel->commentsCountouter($postsid);
            foreach ($commentcount['data'] as $co => $va) {
                $commentcount['data'][$co]['users_photo'] = $this->media_url . "assets/images/users/" . $commentcount['data'][$co]['users_photo'];
            }
            if ($res > 0) {
                foreach ($res as $key => $val) {
                    $res[$key]['posts_photo_or_video']  = $this->s3_url . "assets/images/posts/" . $res[$key]['posts_photo_or_video'];
                    $res[$key]['posts_video_thumbnail'] = $this->s3_url . "assets/images/posts/thumbnails/" . $res[$key]['posts_video_thumbnail'];
                    $res[$key]['users_photo']           = $this->media_url . "assets/images/users/" . $res[$key]['users_photo'];
                    $res[$key]['sharecode']             = $this->media_url . 'post-view?postid=' . $res[$key]['posts_id'] . '&&type=0';
                    $res[$key]['poll_count']            = $pollcount['count'];
                    $res[$key]['comment_count']         = $commentsCountouter;
                    $res[$key]['comment']               = $commentcount['data'];
                    $res[$key]['isPolled']              = $isPostpolled > 0 ? true : false;
                }
                $this->output->set_output(json_encode(array('value' => $res, 'status' => true)));
            } else {
                $this->output->set_output(json_encode(array('value' => 'no data found', 'status' => false)));
            }
        } else {

            $this->output->set_output(json_encode(array('value' => 'parameter missing', 'status' => false)));
        }
    }

    // edit posts
    function editPosts() {

        $this->output->set_content_type('applcaition/json');
        $postid         = $this->input->post('postid');
        $title          = $this->input->post('title');
        $content        = $this->input->post('content');
        $linecount      = $this->input->post('posts_linecount');
        if (!empty($postid)) {
            $update = array('posts_title' => $title, 'posts_linecount' => $linecount, 'posts_uploaded_date' => date('Y-m-d h:m:s'), 'posts_content' => $content);
            $res    = $this->PostsApiModel->editPosts($postid, $update);
            if ($res > 0) {
                $this->output->set_output(json_encode(array('value' => 'successfully updated', 'status' => true)));
            } else {
                $this->output->set_output(json_encode(array('value' => 'operation failed', 'status' => false)));
            }
        } else {
            $this->output->set_output(json_encode(array('value' => 'parameter missing', 'status' => false)));
        }
    }

    // delete posts
    function deletePosts() {

        $this->output->set_content_type('applcation/json');
        $postid         = $this->input->post('postid');
        $type           = $this->input->post('type');
        $logged_userid  = $this->input->post('logged_userid');
        $country        = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;
        $state          = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district       = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;
        $area           = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;
        $page_start     = !empty($this->input->post('page_start')) ? $this->input->post('page_start') : 0;
        $userid         = $this->input->post('userid');
        if (!empty($postid)) {
            $res = $this->PostsApiModel->deletePosts($postid);
            if ($res > 0) {
                switch ($type) {
                    case 0:
                        $posts = $this->returngetPostsnew($userid, $postid, $type, $country, $state, $district, $area, $page_start);
                        $is_completed = $this->check_get_posts_next_array($userid, $page_start, $country, $state, $district, $area);
                        if (count($posts) >= 9)
                            $last_posts = $posts[9];
                        else
                            $last_posts = null;
                        if ($last_posts == null)
                            $this->output->set_output(json_encode(array('value' => "successfully deleted", 'is_completed' => 1, 'status' => true)));
                        else {
                            if ($is_completed > 0)
                                $this->output->set_output(json_encode(array('value' => "successfully deleted", 'data' => array($last_posts), 'is_completed' => 0, 'status' => true)));
                            else
                                $this->output->set_output(json_encode(array('value' => "successfully deleted", 'data' => array($last_posts), 'is_completed' => 1, 'status' => true)));
                        }
                        break;
                    case 1:
                        $posts = $this->returnprofilePosts($userid, $logged_userid, $page_start);
                        if (count($posts) >= 9)
                            $last_posts = $posts[9];
                        else
                            $last_posts = null;
                        if ($last_posts == null)
                            $this->output->set_output(json_encode(array('value' => "successfully deleted", 'is_completed' => 1, 'status' => true)));
                        else {
                            if ($last_posts['is_completed_profile'] > 0)
                                $this->output->set_output(json_encode(array('value' => "successfully deleted", 'data' => array($last_posts), 'is_completed' => 0, 'status' => true)));
                            else
                                $this->output->set_output(json_encode(array('value' => "successfully deleted", 'data' => array($last_posts), 'is_completed' => 1, 'status' => true)));
                        }
                        break;
                }
                $delete_notifications = $this->PostsApiModel->deletenotifications($postid);
            }
            else {
                $this->output->set_output(json_encode(array('value' => 'operation failed', 'status' => false)));
            }
        } else {
            $this->output->set_output(json_encode(array('value' => 'parameter missing', 'status' => false)));
        }
    }

    // hide posts
    function hidePosts() {

        $this->output->set_content_type('application/json');
        $postid         = $this->input->post('postid');
        $userid         = $this->input->post('userid');
        $type           = $this->input->post('type');
        $country        = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;
        $state          = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district       = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;
        $area           = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;
        $page_start     = !empty($this->input->post('page_start')) ? $this->input->post('page_start') : 0;
        if (!empty($postid && $userid)) {
            $res = $this->PostsApiModel->hidePosts($postid, $userid);
            if ($res > 0) {
                switch ($type) {
                    case 0:
                        $posts          = $this->returngetPostsnew($userid, $postid, $type, $country, $state, $district, $area, $page_start);
                        $is_completed   = $this->check_get_posts_next_array($userid, $page_start, $country, $state, $district, $area);
                        if (count($posts) >= 9)
                            $last_posts = $posts[9];
                        else
                            $last_posts = null;
                        if ($last_posts == null)
                            $this->output->set_output(json_encode(array('value' => "operation successs", 'is_completed' => 1, 'status' => true)));

                        else {
                            if ($is_completed > 0)
                                $this->output->set_output(json_encode(array('value' => "operation success", 'data' => array($last_posts), 'is_completed' => 0, 'status' => true)));
                            else
                                $this->output->set_output(json_encode(array('value' => "operation success", 'data' => array($last_posts), 'is_completed' => 1, 'status' => true)));
                        }
                        break;
                    case 1:
                        $posts = $this->returnprofilePosts($userid, $userid, $page_start);
                        if (count($posts) >= 9)
                            $last_posts = $posts[9];
                        else
                            $last_posts = null;
                        if ($last_posts == null)
                            $this->output->set_output(json_encode(array('value' => "operation success", 'is_completed' => 1, 'status' => true)));
                        else {
                            if ($last_posts['is_completed_profile'] > 0)
                                $this->output->set_output(json_encode(array('value' => "operation success", 'data' => array($last_posts), 'is_completed' => 0, 'status' => true)));
                            else
                                $this->output->set_output(json_encode(array('value' => "operation success", 'data' => array($last_posts), 'is_completed' => 1, 'status' => true)));
                        }
                        break;
                }
            }
            else {
                $this->output->set_output(json_encode(array('value' => 'operation failed', 'status' => false)));
            }
        } else {
            $this->output->set_output(json_encode(array('value' => 'parameter missing', 'status' => false)));
        }
    }

    public function totalUniquePostCount($sort_array_state_final, $friend_posts_final, $sort_array_global_final, $sort_array_national_final, $sort_array_district_final, $state_arryay_length, $friends_arryay_length, $global_arryay_length, $national_arryay_length, $district_arryay_length) {
        $total_post_count_state     = $state_arryay_length;
        $total_post_count_friend    = $friends_arryay_length;
        $total_post_count_global    = $global_arryay_length;
        $total_post_count_national  = $national_arryay_length;
        $total_post_count_district  = $district_arryay_length;
        $total_posts                = $state_arryay_length + $friends_arryay_length + $global_arryay_length + $national_arryay_length + $district_arryay_length;
        $p = $q = 2;
        $r = $s = $t = $total = 1;
        $state_array_count = $friends_array_count = $global_array_count = $national_array_count = $district_array_count = 0;
        $post_id_array = [];
        for ($i = 1; $i <= $total_posts; $i++) {
            if ($p > 0 && $state_arryay_length > 0) {
                if (!in_array($sort_array_state_final[$state_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array[] = $sort_array_state_final[$state_array_count]['posts_id'];
                    $p--;
                } else {
                    $total_post_count_state--;
                }
                $state_arryay_length--;
                $state_array_count++;
            } else if ($q > 0 && $friends_arryay_length > 0) {
                if (!in_array($friend_posts_final[$friends_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array [] = $friend_posts_final[$friends_array_count]['posts_id'];
                    $q--;
                } else {
                    $total_post_count_friend--;
                }
                $friends_arryay_length--;
                $friends_array_count++;
            } else if ($r > 0 && $global_arryay_length > 0) {
                if (!in_array($sort_array_global_final[$global_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array [] = $sort_array_global_final[$global_array_count]['posts_id'];
                    $r--;
                } else {
                    $total_post_count_global--;
                }
                $global_arryay_length;
                $global_array_count++;
            } else if ($s > 0 && $national_arryay_length > 0) {
                if (!in_array($sort_array_national_final[$national_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array [] = $sort_array_national_final[$national_array_count]['posts_id'];
                    $s--;
                } else {
                    $total_post_count_national--;
                }
                $national_arryay_length--;
                $national_array_count++;
            } else if ($t > 0 && $district_arryay_length > 0) {
                if (!in_array($sort_array_district_final[$district_array_count]['posts_id'], $post_id_array)) {
                    $post_id_array [] = $sort_array_district_final[$district_array_count]['posts_id'];
                    $t--;
                } else {
                    $total_post_count_district--;
                }
                $district_arryay_length--;
                $district_array_count++;
            }
            $total++;
            if ($total == 10) {
                $p = $q = 2;
                $r = $s = $t = 1;
            } else {
                if ($p == 0 && $friends_arryay_length == 0 && $global_arryay_length == 0 && $national_arryay_length == 0 && $district_arryay_length == 0) {
                    $p = 2;
                } else if ($q == 0 && $global_arryay_length == 0 && $national_arryay_length == 0 && $district_arryay_length == 0) {
                    $p = $q = 2;
                } else if ($r == 0 && $national_arryay_length == 0 && $district_arryay_length == 0) {
                    $r = 1;
                    $p = $q = 2;
                } else if ($s == 0 && $district_arryay_length == 0) {
                    $s = $r = 1;
                    $p = $q = 2;
                } else if ($t == 0) {
                    $t = $s = $r = 1;
                    $p = $q = 2;
                }
            }
        }
        $filter_array = array_filter($post_id_array);
        return sizeof($filter_array);
    }
}
