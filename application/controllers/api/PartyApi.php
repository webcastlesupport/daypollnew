<?php

class PartyApi extends CI_Controller{


    public $media_url= "https://s3.amazonaws.com/daypoll/";
    public $s3_url   = "https://s3.amazonaws.com/daypoll/";

    function __construct()
    {
        error_reporting(0);
        parent::__construct();
        $this->load->model(array('PartyApiModel','CommonModel'));
        $this->load->library('form_validation');
        $this->load->library('aws3');
    }

    // create party 
    function createParty(){

        $this->output->set_content_type('application/json');
        
        
        $userid     = $this->input->post('userid');
        $party_name = $this->input->post('partyname');
        $president  = $this->input->post('president');
        $secretary  = $this->input->post('secretary');
        $email      = $this->input->post('email');
        $scope      = $this->input->post('scope');
        // $mobile     = $this->input->post('mobile');
        // $password   = $this->input->post('password');
        // $privacy    = $this->input->post('privacy');
        $country    = $this->input->post('country');
        $state      = $this->input->post('state');
        $city       = $this->input->post('city');
        $area       = $this->input->post('area');
        
        $this->form_validation->set_rules("userid","userid","required");
        $this->form_validation->set_rules("partyname","partyname","required");
        $this->form_validation->set_rules("president","president","required");
        $this->form_validation->set_rules("secretary","secretary","required");
        $this->form_validation->set_rules("email","email","required");
        $this->form_validation->set_rules("scope","scope","required");
        // $this->form_validation->set_rules("mobile","mobile","required");
        // $this->form_validation->set_rules("password","password","required");
        //$this->form_validation->set_rules("privacy","privacy","required");
        $this->form_validation->set_rules("country","country","required");
        $this->form_validation->set_rules("state","state","required");
        $this->form_validation->set_rules("city","city","required");
        $this->form_validation->set_rules("area","area","required");
        
        if($this->form_validation->run()==TRUE){

            $insert = array('party_name' => $party_name ,'party_email' => $email ,'party_president' => $president,
                            'party_secretary' => $secretary , 'party_created_by' => $userid ,'party_country' => $country,
                            'party_state' => $state ,'party_city' => $city ,'party_area' => $area ,'party_scope' => $scope
                            );
            $res = $this->PartyApiModel->createParty($insert);

            if($res > 0){

                $getname = $this->PartyApiModel->getpartynameafterCreate($res);
                $this->output->set_output(json_encode(array('message'=>'successfully inserted','party_id' => $res,'party_name' => $getname,'status'=>true)));
            }
            else{

                $this->output->set_output(json_encode(array('message'=>'failed to insert','status'=>false)));
            }


        }
        else{

                $this->output->set_output(json_encode(array('message'=>'parameters missing','status'=>'false')));
        } 
       
        
    }



     // edit party 
     function editParty(){

        $secretary_pic = "";
        $president_pic = "";
        $profile_pic   = "";
        $cover_pic     = ""; 

        $update = array();

        $this->output->set_content_type('application/json');
        
        
        $partyid    = $this->input->post('partyid');
        $president  = $this->input->post('president');
        $secretary  = $this->input->post('secretary');
        $desc       = $this->input->post('description'); 
       
        $this->form_validation->set_rules("partyid","partyid","required");
        $this->form_validation->set_rules("president","president","required");
        $this->form_validation->set_rules("secretary","secretary","required");
        
        
        if($this->form_validation->run() == TRUE){

            $getPhoto = $this->PartyApiModel->geteditprofilepic($partyid);


            $update = array(
                            'party_president'       => $president,
                            'party_secretary'       => $secretary,
                            'party_description'     => $desc
                            );

            if(isset($_FILES['party_profile_image'])!=null){

                  
                //   profile pic upload
                    
                        $errors= array();
                        $file_name = $_FILES['party_profile_image']['name'];
                        $file_size = $_FILES['party_profile_image']['size'];
                        $file_tmp  = $_FILES['party_profile_image']['tmp_name'];
                        $file_type = $_FILES['party_profile_image']['type'];
                        
                        $tmp      = explode('.', $file_name);
                        $file_ext = end($tmp);
    
                  
                           
                        
                        if($getPhoto['profile'] != $file_name){
                          
                        
                       
                        $expensions  = array("jpeg","jpg","png");
                        
                        if(in_array($file_ext,$expensions) === false){
                           $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
                        }
                        
                        
                        
                        $path = "./assets/images/party/".$getPhoto['profile'];
                        
                        $option=false;
                        if($getPhoto['profile'] != "none.png")
                        {
                           
                         $option=true;
                         unlink($path);
                        }
                        
                        
                        if(empty($errors)==true){

                            
                            $profile_pic = 'party-'.mt_rand(10000,99999).date('h-i-s').'.'.$file_ext;
                            move_uploaded_file($file_tmp,"assets/images/party/".$profile_pic);
                          
                        
                            $update['party_photo'] = $profile_pic;
                            
                          
                        }
              
                    }
                  
                
            }
    
                // profile pic ends here     





                if(isset($_FILES['party_cover_image'])!=null){

                    
                       
                    //   cover pic uploaddd
                        
                            $errors= array();
                            $file_name = $_FILES['party_cover_image']['name'];
                            $file_size = $_FILES['party_cover_image']['size'];
                            $file_tmp  = $_FILES['party_cover_image']['tmp_name'];
                            $file_type = $_FILES['party_cover_image']['type'];
                            
                            $tmp      = explode('.', $file_name);
                            $file_ext = end($tmp);
        
                      
                            
                            if($getPhoto['cover'] != $file_name){
                              
                           
                            $expensions  = array("jpeg","jpg","png");
                            
                            if(in_array($file_ext,$expensions) === false){
                               $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
                            }
                            
                            
                            
                            $path="./assets/images/party/cover/".$getPhoto['cover'];
                          
                            $option=false;
                            if($getPhoto['cover'] != "cover-none.png")
                            {
                             $option=true;
                             unlink($path);
                            }
                            
                            
                            if(empty($errors)==true){
                               
                                $cover_pic = 'cover-'.mt_rand(10000,99999).date('h-i-s').'.'.$file_ext;
                               move_uploaded_file($file_tmp,"assets/images/party/cover/".$cover_pic);
                               $update['party_cover_photo'] = $cover_pic;
                            }
                  
                        }
                       
                }
        
                    // cover pic ends here     



                    if(isset($_FILES['party_president_image'])){

                    
                       
                        //   president pic uploaddd
                            
                                $errors= array();
                                $file_name = $_FILES['party_president_image']['name'];
                                $file_size = $_FILES['party_president_image']['size'];
                                $file_tmp  = $_FILES['party_president_image']['tmp_name'];
                                $file_type = $_FILES['party_president_image']['type'];
                                
                                $tmp      = explode('.', $file_name);
                                $file_ext = end($tmp);
            
                          
                                
                                if($getPhoto['president_photo'] != $file_name){
                                  
                               
                                $expensions  = array("jpeg","jpg","png");
                                
                                if(in_array($file_ext,$expensions) === false){
                                   $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
                                }
                                
                                
                                
                                $path = "./assets/images/party/president/".$getPhoto['president_photo'];
                              
                                $option=false;
                                if($getPhoto['president_photo'] != "none.png")
                                {
                                 $option=true;
                                 unlink($path);
                                }
                                
                                
                                if(empty($errors)==true){
                                    $president_pic = 'president-'.mt_rand(10000,99999).date('h-i-s').'.'.$file_ext;
                                
                                   move_uploaded_file($file_tmp,"assets/images/party/president/".$president_pic);
                                   $update['party_president_photo'] = $president_pic;
                              
                            

                                }
                      
                            }
                            
                        
                    }
            
                        // president pic ends here   





                        if(isset($_FILES['party_secretary_image'])!=null){

                    
                       
                            //   secretary pic uploaddd
                                
                                    $errors= array();
                                    $file_name = $_FILES['party_secretary_image']['name'];
                                    $file_size = $_FILES['party_secretary_image']['size'];
                                    $file_tmp  = $_FILES['party_secretary_image']['tmp_name'];
                                    $file_type = $_FILES['party_secretary_image']['type'];
                                    
                                    $tmp      = explode('.', $file_name);
                                    $file_ext = end($tmp);
                
                               
                                    
                                    if($getPhoto['secretary_photo'] != $file_name){
                                      
                                    
                                    $expensions  = array("jpeg","jpg","png");
                                    
                                    if(in_array($file_ext,$expensions) === false){
                                       $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
                                    }
                                    
                                    
                                    
                                    $path = "./assets/images/party/secretary/".$getPhoto['secretary_photo'];
                                  
                                    $option=false;
                                    if(file_exists($path) && $getPhoto['secretary_photo'] != "none.png")
                                    {
                                     $option=true;
                                     unlink($path);
                                    }
                                    
                                    
                                    if(empty($errors)==true){
                                        $secretary_pic = 'secretary-'.mt_rand(10000,99999).date('h-i-s').'.'.$file_ext;
                                    
                                        move_uploaded_file($file_tmp,"assets/images/party/secretary/".$secretary_pic);
                                        $update['party_secretary_photo'] = $secretary_pic;
                                    }
                          
                                }
                                
                            
                        }
                
                            // profile pic ends here   
        


            $res = $this->PartyApiModel->editParty($partyid,$update);

            if($res > 0){

                $this->output->set_output(json_encode(array('message'=>'successfully updated','status'=>true)));
            }
            else{

                $this->output->set_output(json_encode(array('message'=>'failed to update','status'=>false)));
            }


        }
        else{

                $this->output->set_output(json_encode(array('message'=>'parameters missing','status'=>'false')));
        }
       
        
    }

    // search party
    function searchParty(){

        $this->output->set_content_type('application/json');
        $search = $this->input->post('search');

        $res = $this->PartyApiModel->searchParty($search);

        if($res > 0){
            foreach($res as $key => $value){

                $res[$key]['poll_count'] = $this->PartyApiModel->partyPollCount($res[$key]['party_id']);
            }
            $this->output->set_output(json_encode(array('message'=>$res,'status'=>true)));
        }
        else{
            $this->output->set_output(json_encode(array('message'=>'no values found','status'=>false)));
        }
     
    }

   
    // search party posts api
    function searchPartyPosts(){                                                          

        $party_error = "true";
        $posts_error = "true";
        $this->output->set_content_type('application/json');

        $search     = $this->input->post('search');
        $userid     = $this->input->post('userid');
        //$res        = $this->PartyApiModel->searchPartyPosts($search);
        $res_party  = $this->PartyApiModel->getallParty($search);
        
        foreach($res_party as $keyss => $vals){
            
            $res_party[$keyss]['poll_count'] = $this->PartyApiModel->getPartypollcountbyid($res_party[$keyss]['party_id']);
            $ispolled = $this->PartyApiModel->get_party_poll_user($userid);
            $res_party[$keyss]['poll_count_public']   = $this->PartyApiModel->getPartypubliccountbyid($res_party[$keyss]['party_id']);
            $res_party[$keyss]['poll_count_secret']   = $this->PartyApiModel->getPartysecretcountbyid($res_party[$keyss]['party_id']);
           

        if($ispolled['poll'] == $res_party[$keyss]['party_id']){
            $res_party[$keyss]['isPolled'] = 1;
        }    
        else{
            $res_party[$keyss]['isPolled'] = 0;
        }
    }

   

        // foreach($res as $row){
        //     $res_party[] = $row;
        // }


        
       

        if(empty($res_party)){

            $posts_error = "false";
        }
        else{

           
        
        foreach ($res_party as $key => $value){
            
            if(isset($res_party[$key]['party_posts_id'])){

                //$pollcount            = $this->PartyApiModel->pollsCount($res_party[$key]['party_posts_id']);
                //$commentcount         = $this->PartyApiModel->commentsCount($res_party[$key]['party_posts_id']);
                //$commentsCountouter   = $this->PartyApiModel->commentsCountouter($res_party[$key]['party_posts_id']);
                                                    
                //$token                                        = mt_rand(100000,999999);
                //$res_party[$key]['party_posts_photo_video']   = base_url().'assets/images/partyposts/'.$res_party[$key]['party_posts_photo_video'];
                //$res_party[$key]['poll_count']                = $pollcount['count'];
                //$res_party[$key]['comment_count']             = $commentsCountouter;
                //$res_party[$key]['users_photo']               = base_url().'assets/images/users/'.$res_party[$key]['users_photo'];
                //$res_party[$key]['sharecode']                 = base_url().'post-view?token='.$token.'&&postid='.base64_encode($res_party[$key]['party_posts_id']);
                //$res_party[$key]['downloadlink']              = $res_party[$key]['party_posts_photo_video'];
                //$res_party[$key]['type']                      = 1;

            }
            else{

                $res_party[$key]['party_photo']        = $this->media_url.'assets/images/party/'.$res_party[$key]['party_photo'];
                //$res_party[$key]['party_cover_photo']  = base_url().'assets/images/party/'.$res_party[$key]['party_cover_photo'];
                $res_party[$key]['type']               = 0;

            }

        }
    }
    if($res_party != 0){
        $this->output->set_output(json_encode(array('posts'=>$res_party , 'status' => true)));
    }
    else{
        $this->output->set_output(json_encode(array('posts'=>'no party found' , 'status' => false)));
    }
    

    }

    //party list
    function getPartylist(){

        $this->output->set_content_type('application/json');

        $res = $this->PartyApiModel->getPartylist();
       
        
        if($res > 0){
            
        foreach($res as $key => $value){
            
            $res[$key]['party_photo']       = $this->media_url.'assets/images/party/'.$res[$key]['party_photo'];
            //$res[$key]['party_cover_photo'] = base_url().'assets/images/party/'.$res[$key]['party_cover_photo'];
        }    
        $this->output->set_output(json_encode(array('chart' => $res , 'status' => 'true')));
        
        }
        else{

        $this->output->set_output(json_encode(array('value' => 'no values found' , 'status' => 'false')));
       
        }
    
    }

   

  
    function getallParty(){
        
        $this->output->set_content_type('application/json');
        $res = $this->PartyApiModel->getallParty();
        foreach($res as $key => $value){

            $res[$key]['poll_count'] = $this->PartyApiModel->getPartypollcountbyid($res[$key]['party_id']); 
    
        }
        $this->output->set_output(json_encode(['value' => $res  , 'status' => 'true']));
    }

    //get public secret count
    function getPartyPollcountbyid(){

        $this->output->set_content_type('application/json');
        $partyid = $this->input->post('partyid');
        
        if(!empty($partyid)){
        $res_public = $this->PartyApiModel->getPartypubliccountbyid($partyid);
        $res_secret = $this->PartyApiModel->getPartysecretcountbyid($partyid);
        $this->output->set_output(json_encode(['public' => $res_public  ,'private' => $res_secret ,'status' => 'true']));
        }
        else{
            $this->output->set_output(json_encode(['msg' => 'parameter missing'  , 'status' => 'false']));
        }
    } 

   // upload party posts
    
   function uploadpartyPosts(){                                                             

    $this->output->set_content_type('application/json');

    $userid         = $this->input->post('userid');
    $posttitle      = $this->input->post('title');
    $photo_or_video = $this->input->post('photo_or_video');         // photo or video  [photo -> 0 , video -> 1 , -1 -> other]
    $posts_content  = $this->input->post('content');
    $partyid        = $this->input->post('partyid');  
    $video_thumb    = $this->input->post('video_thumbnail'); 
    $linecount      = $this->input->post('linecount'); 
    $posts_width    = $this->input->post('posts_width');    
    $posts_height   = $this->input->post('posts_height');     

    $country        = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
    $state          = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
    $district       = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
    $area           = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;




    $this->form_validation->set_rules("userid","userid","required");
    //$this->form_validation->set_rules("title","title","required");
    $this->form_validation->set_rules("photo_or_video","photo_or_video","required");
    //$this->form_validation->set_rules("content","content","required");
    $this->form_validation->set_rules("partyid","partyid","required");

    if($linecount == NULL){
        $linecount = 0;
    }

    if($this->form_validation->run() == TRUE){


        if($photo_or_video == -1){

            $insert = array('party_posts_posted_by'=>$userid,'photo_or_video'=>$photo_or_video,
                            'party_posts_title'=>$posttitle,'party_posts_country' => $country,
                            'party_posts_state' => $state,'party_posts_city' => $district,'party_posts_area' => $area,
                            'party_posts_content'=>$posts_content,'party_posts_line_count'=>$linecount,'party_posts_party_id'=>$partyid);

            $result = $this->PartyApiModel->uploadpartyPost($insert);


            if($result > 0) {

                $this->output->set_output(json_encode(['message' => "successfully uploaded ", 'status' => "true"]));
            }
            else{
                $this->output->set_output(json_encode(['message' => "operation failed!..", 'status' => "false"]));
            }
        }


        else if($photo_or_video == 1){

            if(isset($_FILES['photo_video'])){

                $errors    = array();
                $file_name = $_FILES['photo_video']['name'];
                $file_size = $_FILES['photo_video']['size'];
                $file_tmp  = $_FILES['photo_video']['tmp_name'];
                $file_type = $_FILES['photo_video']['type'];
                $tmp       = explode('.', $file_name);
                $file_ext  = end($tmp);


                
                $post_name = "partyposts-".mt_rand(100000,999999).date('H-i-s').'.'.$file_ext;
                
                $expensions = array("jpeg","jpg","png","mp4","3gp");
                
                if(in_array($file_ext,$expensions)=== false){

                $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                    
                }
                    $flag = 0;
                    if(empty($errors)==true){

                        $data['tmp_name']  = $_FILES['photo_video']['tmp_name'];
                        $data['file_name'] = $post_name;
                      
                        $val = $this->aws3->sendFile('daypoll/assets/images/partyposts',$data); 
                        $flag = 1; 

                            if($flag == 1){
                     
                                if(isset($_FILES['posts_video_thumbnail'])){
                                    $errors= array();
                                    $file_name = $_FILES['posts_video_thumbnail']['name'];
                                    $file_size = $_FILES['posts_video_thumbnail']['size'];
                                    $file_tmp  = $_FILES['posts_video_thumbnail']['tmp_name'];
                                    $file_type = $_FILES['posts_video_thumbnail']['type'];
                                    $tmp       = explode('.', $file_name);
                                    $file_ext  = end($tmp);
                                    
                                    
                                    $posts_thumb = "partyposts".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;
                                    
                                    $expensions  = array("jpeg","jpg","png");
                                    
                                    if(in_array($file_ext,$expensions)=== false){
                                       $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
                                    }
                                    
                                    
                                    
                                    if(empty($errors)==true){
                                       
                                        $data['tmp_name']  = $_FILES['posts_video_thumbnail']['tmp_name'];
                                        $data['file_name'] = $posts_thumb;
            
                                        $val = $this->aws3->sendFile('daypoll/assets/images/partyposts/thumbnail',$data); 
    

                                       //move_uploaded_file($file_tmp,"assets/images/partyposts/thumbnail/".$posts_thumb);
                                       $thumb_name = $posts_thumb;
                                       
                                    }else{
                                        $thumb_name= NULL;
                                    }
                          
                                }
        
        
                        
            
                                $insert = array('party_posts_posted_by'=>$userid,'photo_or_video'=>$photo_or_video,
                                'party_posts_title'=>$posttitle,'party_posts_photo_video'=>$post_name,
                                'party_posts_content'=>$posts_content,'posts_width' => $posts_width,'posts_height' => $posts_height,
                                'party_posts_line_count'=>$linecount,'party_posts_party_id'=>$partyid,'party_posts_video_thumbnail' => $thumb_name);
                                
                                
                                $result = $this->PartyApiModel->uploadpartyPost($insert);
            
                        }


           


            if($result > 0) {

                $this->output->set_output(json_encode(['message' => "successfully uploaded ", 'status' => "true"]));
            }
            else{
                $this->output->set_output(json_encode(['message' => "operation failed!..", 'status' => "false"]));
            }
        }

        }
    }



        else if($photo_or_video == 0){

            if(isset($_FILES['photo_video'])){

                $errors    = array();
                $file_name = $_FILES['photo_video']['name'];
                $file_size = $_FILES['photo_video']['size'];
                $file_tmp  = $_FILES['photo_video']['tmp_name'];
                $file_type = $_FILES['photo_video']['type'];
                $tmp       = explode('.', $file_name);
                $file_ext  = end($tmp);


                $post_name = "partyposts-".mt_rand(100000,999999).date('H-i-s').'.'.$file_ext;
                
                $expensions = array("jpeg","jpg","png","mp4","3gp");
                
                if(in_array($file_ext,$expensions)=== false){

                $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                    
                }
                    
                    if(empty($errors)==true){

                        $data['tmp_name']  = $_FILES['photo_video']['tmp_name'];
                        $data['file_name'] = $post_name;
        
       
                        $val = $this->aws3->sendFile('daypoll/assets/images/partyposts',$data); 
                                   
            
                                    $insert = array('party_posts_posted_by'=>$userid,'photo_or_video'=>$photo_or_video,
                                    'party_posts_title'=>$posttitle,'party_posts_photo_video'=>$post_name,
                                    'party_posts_content'=>$posts_content,'posts_width' => $posts_width,'posts_height' => $posts_height,
                                    'party_posts_line_count'=>$linecount,'party_posts_party_id'=>$partyid,'party_posts_video_thumbnail' => $video_thumb);
                                    
                                    
                                    $result = $this->PartyApiModel->uploadpartyPost($insert);
                
                    }
    
                if($result > 0) {
    
                    $this->output->set_output(json_encode(['message' => "successfully uploaded ", 'status' => "true"]));
                }
                else{
                    $this->output->set_output(json_encode(['message' => "operation failed!..", 'status' => "false"]));
                }
            

        }
    }
}
    else{
        $this->output->set_output(json_encode(['message'=>'parameter missing','status'=>'false']));
    }

}


    // get party profil`e
    function getPartyProfile(){

        $this->output->set_content_type('application/json');

        $zone = getallheaders();
        $GMT  = new DateTimeZone("GMT");
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";


        $partyid        = $this->input->post('partyid');
        $userid         = $this->input->post('userid');
        $scope          = $this->input->post('scope'); 

        $page_start  = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;


        if(!empty($partyid)){

        // get party rank
        
        $party_rank     = $this->PartyApiModel->getpartyrank($partyid);
        $rank_value     = 0;    
       
        
        $rank = 1;
        foreach($party_rank as $r => $k){
            if($party_rank[$r]['users_poll_party_id'] == $partyid){
            $rank_value = $rank;
            }
           
            $rank++;
        }
       
        $polledparty    = $this->PartyApiModel->getPolledparty($userid,$scope);
        
        
        $getjoin        = $this->PartyApiModel->getjoinpartyname($polledparty['join']);
        $getpoll        = $this->PartyApiModel->getpollpartyname($polledparty['poll']);


        

        $isjoin              = $polledparty['join'] == $partyid  ? 1 :0;
        $ispartypolled       = $polledparty['poll'] == $partyid  ? 1 :0;

        
        $res            = $this->PartyApiModel->getPartyProfile($partyid);
        $userlocation   = $this->PartyApiModel->getUserlocation($userid);

        if($res != 0){
        $members_count      = $this->PartyApiModel->getPartyJoinCount($partyid);
        //$members            = $this->PartyApiModel->getprofilePartyJoinmembers($partyid);
        //$party_poll_members = $this->PartyApiModel->getprofilePartyJoinmembersbypoll($partyid);
        //$members            = $this->PartyApiModel->getprofilePartyJoinmembersbypoll($partyid);
        $members_joined     = $this->PartyApiModel->getprofilePartyJoinmembers($partyid);
        
          
        
        switch($scope){

            case 1:
            $location  = $this->PartyApiModel->isLocation(array('party_id' => $partyid ,'party_country' => $userlocation['country']));
            
            break;

            case 2:
             $location  = $this->PartyApiModel->isLocation(array('party_id' => $partyid,'party_state' => $userlocation['state']));

            break;
          

            case 3:
            $location  = $this->PartyApiModel->isLocation(array('party_id' => $partyid,'party_city' => $userlocation['district']));

            break;

            case 4:
            $location  = $this->PartyApiModel->isLocation(array('party_id' => $partyid,'party_area' => $userlocation['area']));

            break;

        }
        $total_array  = [];
        $result_Array = [];
        foreach($members_joined as $memss => $mvalues){
            $members_joined[$memss]['users_photo'] = $this->media_url."assets/images/users/".$members_joined[$memss]['users_photo'];
            $members_joined[$memss]['total']       = $this->PartyApiModel->Joinmembersbypollnolimit($members_joined[$memss]['user_id'] , $partyid);     
            $members_joined[$memss]['is_poll']     = $members_joined[$memss]['total'] > 0 ? 1 :0;
      
        }
        
        $i=0;
        foreach($members_joined as $row){
            $total_array[$i] = $row['total'];
            $i++;
        }
        arsort($total_array);
        foreach($total_array as $key => $value){
            $result_Array[] = $members_joined[$key];
        }
        
        $result_Array = array_slice($result_Array,0,5);

        // foreach($members_joined as $row){

        //     $members[] =  $row;
        // }


        $poll_count      = $this->PartyApiModel->getPartyPollCount($partyid);
        //$comment_count  = $this->PartyApiModel->getCommentCount($partyid);
        $get_party_posts = $this->PartyApiModel->getPartyPosts($partyid);
        $get_report_post = $this->PartyApiModel->getReportedposts($userid);
        $get_hide_post   = $this->PartyApiModel->gethiddenposts($userid);
       

        $hide_array =   [];
        foreach($get_hide_post as $row){
            $hide_array[]   = $row['hide_posts_post_id'];
        }

        $report_array =   [];
        foreach($get_report_post as $row){
            $report_array[]   = $row['party_posts_report_post_id'];
        }



        $final_array =   [];
        foreach($get_party_posts as $in_row){
            if(!in_array($in_row['party_posts_id'],$hide_array)){
                $final_array[]    =  $in_row ;
            }
        }
        
        $final_last_array = [];
        foreach($final_array as $in_row){
           if(!in_array($in_row['party_posts_id'],$report_array)){
               $final_last_array[]    =  $in_row ;
           }
       }



        $total_posts     = count($final_last_array); 


              
        $party_posts          = array_slice($final_last_array,$page_start,10);

        $party_posts_next     = array_slice($final_last_array,$page_start+10,10);

        foreach($party_posts as $keys => $values){

            $party_posts[$keys]['users_photo']               = $this->media_url."assets/images/users/".$party_posts[$keys]['users_photo'];
            $date = new DateTime($party_posts[$keys]['party_posts_uploaded_date'], $GMT );
            $date->setTimezone(new DateTimeZone($zone['timezone']));
            $party_posts[$keys]['party_posts_uploaded_date'] = $date->format('Y-m-d H:i:s');
            
            $party_posts[$keys]['party_posts_photo_video']   = $this->s3_url."assets/images/partyposts/".$party_posts[$keys]['party_posts_photo_video'];
            $party_posts[$keys]['party_posts_video_thumbnail']   = $this->s3_url."assets/images/partyposts/thumbnail/".$party_posts[$keys]['party_posts_video_thumbnail'];
     
            $party_posts[$keys]['posts_poll_count']          = $this->PartyApiModel->partypollsCount($party_posts[$keys]['party_posts_id']);
            $ispolled                                        = $this->PartyApiModel->ispostpolled($party_posts[$keys]['party_posts_id'],$userid);  
            $party_posts[$keys]['isPolled']                  = $ispolled > 0 ? 1 : 0;   
            $party_posts[$keys]['posts_comment_count']       = $this->PartyApiModel->partypostscommentsCount($party_posts[$keys]['party_posts_id']);
            $party_posts[$keys]['downloadlink']              = $party_posts[$keys]['party_posts_photo_video'].'?postid='.$party_posts[$keys]['party_posts_id'].'&&type=1';
            $party_posts[$keys]['sharecode']                 = $this->media_url.'post-view?postid='.$party_posts[$keys]['party_posts_id'].'&&type=1';
        }

       
        foreach($res as $key => $value){

            $res[$key]['party_photo']                 = $this->media_url."assets/images/party/".$res[$key]['party_photo'];
            $res[$key]['party_cover_photo']           = $this->media_url."assets/images/party/cover/".$res[$key]['party_cover_photo'];
            $res[$key]['party_president_photo']       = $this->media_url."assets/images/party/president/".$res[$key]['party_president_photo'];  
            $res[$key]['party_secretary_photo']       = $this->media_url."assets/images/party/secretary/".$res[$key]['party_secretary_photo'];     
        }

       

       


        $this->output->set_output(json_encode(['profile'=>$res,'hidden_post_id' => $get_hide_post,'report_post_id' => $get_report_post,'posts'=>$party_posts,'total_posts' => $total_posts,'members' => $result_Array,'members_count' => $members_count,'poll_count' => $poll_count,'party_rank' => $rank_value,'partyjoin' => $getjoin ,'partypoll' => $getpoll,'isjoined' => $isjoin,'ispolled' => $ispartypolled,'isLocation' => $location,'is_completed' => count($party_posts_next) > 0 ? 0 :1,'status'=>'true']));
    }

        else{
            
        $this->output->set_output(json_encode(['status'=> false , 'message' => 'no value found'])); 
    }
   
    }
    else{
        $this->output->set_output(json_encode(['status'=> false , 'message' => 'parameter missing'])); 
    }
    }

    // get party profile
    function returngetPartyProfile($partyid,$userid,$scope,$page_start){

        $this->output->set_content_type('application/json');

        $zone = getallheaders();
        $GMT  = new DateTimeZone("GMT");
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";


        

        if(!empty($partyid)){

        // get party rank
        
        $party_rank     = $this->PartyApiModel->getpartyrank($partyid);
        $rank_value     = 0;    
       
        
        $rank = 1;
        foreach($party_rank as $r => $k){
            if($party_rank[$r]['users_poll_party_id'] == $partyid){
            $rank_value = $rank;
            }
           
            $rank++;
        }
       
        $polledparty    = $this->PartyApiModel->getPolledparty($userid,$scope);
        
        
        $getjoin        = $this->PartyApiModel->getjoinpartyname($polledparty['join']);
        $getpoll        = $this->PartyApiModel->getpollpartyname($polledparty['poll']);


        

        $isjoin              = $polledparty['join'] == $partyid  ? 1 :0;
        $ispartypolled       = $polledparty['poll'] == $partyid  ? 1 :0;

        
        $res            = $this->PartyApiModel->getPartyProfile($partyid);
        $userlocation   = $this->PartyApiModel->getUserlocation($userid);

        if($res != 0){
        $members_count      = $this->PartyApiModel->getPartyJoinCount($partyid);
        //$members            = $this->PartyApiModel->getprofilePartyJoinmembers($partyid);
        //$party_poll_members = $this->PartyApiModel->getprofilePartyJoinmembersbypoll($partyid);
        //$members            = $this->PartyApiModel->getprofilePartyJoinmembersbypoll($partyid);
        $members_joined     = $this->PartyApiModel->getprofilePartyJoinmembers($partyid);
        
          
        
        switch($scope){

            case 1:
            $location  = $this->PartyApiModel->isLocation(array('party_id' => $partyid ,'party_country' => $userlocation['country']));
            
            break;

            case 2:
             $location  = $this->PartyApiModel->isLocation(array('party_id' => $partyid,'party_state' => $userlocation['state']));

            break;
          

            case 3:
            $location  = $this->PartyApiModel->isLocation(array('party_id' => $partyid,'party_city' => $userlocation['district']));

            break;

            case 4:
            $location  = $this->PartyApiModel->isLocation(array('party_id' => $partyid,'party_area' => $userlocation['area']));

            break;

        }
        $total_array  = [];
        $result_Array = [];
        foreach($members_joined as $memss => $mvalues){
            $members_joined[$memss]['users_photo'] = $this->media_url."assets/images/users/".$members_joined[$memss]['users_photo'];
            $members_joined[$memss]['total']       = $this->PartyApiModel->Joinmembersbypollnolimit($members_joined[$memss]['user_id'] , $partyid);     
            $members_joined[$memss]['is_poll']     = $members_joined[$memss]['total'] > 0 ? 1 :0;
      
        }
        
        $i=0;
        foreach($members_joined as $row){
            $total_array[$i] = $row['total'];
            $i++;
        }
        arsort($total_array);
        foreach($total_array as $key => $value){
            $result_Array[] = $members_joined[$key];
        }
        

        // foreach($members_joined as $row){

        //     $members[] =  $row;
        // }


        $poll_count      = $this->PartyApiModel->getPartyPollCount($partyid);
        //$comment_count  = $this->PartyApiModel->getCommentCount($partyid);
        $get_party_posts = $this->PartyApiModel->getPartyPosts($partyid);
        $get_report_post = $this->PartyApiModel->getReportedposts($userid);
        $get_hide_post   = $this->PartyApiModel->gethiddenposts($userid);
       

        $hide_array =   [];
        foreach($get_hide_post as $row){
            $hide_array[]   = $row['hide_posts_post_id'];
        }

        $report_array =   [];
        foreach($get_report_post as $row){
            $report_array[]   = $row['party_posts_report_post_id'];
        }



        $final_array =   [];
        foreach($get_party_posts as $in_row){
            if(!in_array($in_row['party_posts_id'],$hide_array)){
                $final_array[]    =  $in_row ;
            }
        }
        
        $final_last_array = [];
        foreach($final_array as $in_row){
           if(!in_array($in_row['party_posts_id'],$report_array)){
               $final_last_array[]    =  $in_row ;
           }
       }



        $total_posts     = count($final_last_array); 


              
        $party_posts          = array_slice($final_last_array,$page_start,10);
        
        $party_posts_next     = array_slice($final_last_array,$page_start+10,10);

        foreach($party_posts as $keys => $values){
           
            $party_posts[$keys]['is_completed']              = count($party_posts_next);
            $party_posts[$keys]['users_photo']               = $this->media_url."assets/images/users/".$party_posts[$keys]['users_photo'];
            $date = new DateTime($party_posts[$keys]['party_posts_uploaded_date'], $GMT );
            $date->setTimezone(new DateTimeZone($zone['timezone']));
            $party_posts[$keys]['party_posts_uploaded_date'] = $date->format('Y-m-d H:i:s');
            
            $party_posts[$keys]['party_posts_photo_video']   = $this->s3_url."assets/images/partyposts/".$party_posts[$keys]['party_posts_photo_video'];
            $party_posts[$keys]['party_posts_video_thumbnail']   = $this->s3_url."assets/images/partyposts/thumbnail/".$party_posts[$keys]['party_posts_video_thumbnail'];
     
            $party_posts[$keys]['posts_poll_count']          = $this->PartyApiModel->partypollsCount($party_posts[$keys]['party_posts_id']);
            $ispolled                                        = $this->PartyApiModel->ispostpolled($party_posts[$keys]['party_posts_id'],$userid);  
            $party_posts[$keys]['isPolled']                  = $ispolled > 0 ? 1 : 0;   
            $party_posts[$keys]['posts_comment_count']       = $this->PartyApiModel->partypostscommentsCount($party_posts[$keys]['party_posts_id']);
            $party_posts[$keys]['downloadlink']              = $party_posts[$keys]['party_posts_photo_video'].'?postid='.$party_posts[$keys]['party_posts_id'].'&&type=1';
            $party_posts[$keys]['sharecode']                 = $this->media_url.'post-view?postid='.$party_posts[$keys]['party_posts_id'].'&&type=1';
     
        }

       
        foreach($res as $key => $value){

            $res[$key]['party_photo']                 = $this->media_url."assets/images/party/".$res[$key]['party_photo'];
            $res[$key]['party_cover_photo']           = $this->media_url."assets/images/party/".$res[$key]['party_cover_photo'];
            $res[$key]['party_president_photo']       = $this->media_url."assets/images/party/president/".$res[$key]['party_president_photo'];  
            $res[$key]['party_secretary_photo']       = $this->media_url."assets/images/party/secretary/".$res[$key]['party_secretary_photo'];     
        }

       

       


        return $party_posts;
    }

        else{
            
            return 'no value found'; 
    }
   
    }
    else{
        $this->output->set_output(json_encode(['status'=> false , 'message' => 'parameter missing'])); 
    }
    }

    // get party join members
    function getPartyJoinmembers(){

        $this->output->set_content_type('application/json');

        $partyid    = $this->input->post('partyid');
        $followerid = $this->input->post('userid');
        $scope      = $this->input->post('scope');
        
        if(!empty($partyid)){
        
            //$members_by_polls   = $this->PartyApiModel->getprofilePartyJoinmembersbypollnolimit($partyid);
           
           


        switch($scope){
            case 1:
            $res                = $this->PartyApiModel->getPartyJoinmemberslist($partyid , 'users_party_id');
            break;

            case 2:
            $res                = $this->PartyApiModel->getPartyJoinmemberslist($partyid , 'users_party_state_id');
            break;

            case 2:
            $res                = $this->PartyApiModel->getPartyJoinmemberslist($partyid,'users_party_district_id');
            break;
        }
        
        
       

        // if($members_by_polls){

        //     foreach($members_by_polls as $row){

        //         $res[] =  $row;
        //     }
        // }
        
        
        if($res != 0){
            
        foreach($res as $key => $value){
            $res[$key]['poll_count']        = $this->PartyApiModel->Joinmembersbypollnolimit($res[$key]['user_id'] , $partyid);
            $is_polled_user                 = $this->PartyApiModel->is_polled_user($followerid , $res[$key]['user_id'] , $partyid); 

            $res[$key]['is_Polled']         = $is_polled_user > 0 ? 1 :0;
            $res[$key]['users_photo']       = $this->media_url.'assets/images/users/'.$res[$key]['users_photo'];
            //$res[$key]['poll_count']        = $this->PartyApiModel->getpartyUserpollcount($res[$key]['user_id']);
        }    
        $this->output->set_output(json_encode(array('value' => $res , 'status' => 'true')));
        
        }
        else{
            

        $this->output->set_output(json_encode(array('value' => 'no values found' , 'status' => 'false')));


        }
    }
    else{
        $this->output->set_output(json_encode(array('value' => 'parameter missing' , 'status' => 'false')));
        
    }
    }

    // get party users followers
    function pollPartyusers(){

        $this->output->set_content_type('application/json');
        
        $followerid  = $this->input->post('followerid');
        $userid      = $this->input->post('userid');
        $partyid     = $this->input->post('partyid');

        if(!empty($followerid && $userid && $partyid)){

        $res = $this->PartyApiModel->pollPartyusers($userid , $followerid , $partyid);
        
        switch($res){
            
            case 1:
            $this->output->set_output(json_encode(array('message' => 'Successfully polled' , 'status' => 'true')));
            break;

            case -1:
            $this->output->set_output(json_encode(array('message' => 'Already polled' , 'status' => 'false')));
            break;

            case 0:
            $this->output->set_output(json_encode(array('message' => 'Failed to poll' , 'status' => 'false')));
            break;
                
        }
            }
            else{

            $this->output->set_output(json_encode(array('message' => 'parameter missing' , 'status' => 'false')));
            
        }
     
    }

    // confirm party user poll
    function confirmPartyuserpoll(){

        $this->output->set_content_type('application/json');

        $followerid  = $this->input->post('followerid');
        $userid      = $this->input->post('userid');
        $partyid     = $this->input->post('partyid');

        if(!empty($followerid && $userid && $partyid)){

            $res = $this->PartyApiModel->confirmpollPartyusers($userid , $followerid , $partyid);
            
            switch($res){
                
                case 1:
                $this->output->set_output(json_encode(array('message' => 'Successfully polled' , 'status' => 'true')));
                break;
    
                    
                case 0:
                $this->output->set_output(json_encode(array('message' => 'Failed to poll' , 'status' => 'false')));
                break;
                    
            }
                }
                else{
    
                $this->output->set_output(json_encode(array('message' => 'parameter missing' , 'status' => 'false')));
                
            }

          }

          // remove poll party user
          function unpollPartyuser(){

            $this->output->set_content_type('application/json');

            $followerid  = $this->input->post('followerid');
            $userid      = $this->input->post('userid');
            $partyid     = $this->input->post('partyid');
    
            if(!empty($followerid)){
    
                $res = $this->PartyApiModel->removePartyPoll($followerid);
                
                switch($res){
                    
                    case 1:
                    $this->output->set_output(json_encode(array('message' => 'Successfully unpolled' , 'status' => 'true')));
                    break;
        
                        
                    case 0:
                    $this->output->set_output(json_encode(array('message' => 'Failed to unpoll' , 'status' => 'false')));
                    break;
                        
                }
            }
                    else{
        
                    $this->output->set_output(json_encode(array('message' => 'parameter missing' , 'status' => 'false')));
                    
                }
    
          }


    
//      // get party chart
//      function getchart(){

//         $total  =  0;
//         $this->output->set_content_type('application/json');

//         $userid         = $this->input->post('userid');

//         $userlocation   = $this->PartyApiModel->getUserlocation($userid);
        
//         $scope          = $this->input->post('scope');

//         $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
//         $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
//         $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
//         $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;   


//          switch($scope){

//             case 1:
//             $res         = $this->PartyApiModel->getPartysum($scope ,$country , $state , $district , $area ,'users_poll_party_id');
//             break;

//             case 2:
//             $res         = $this->PartyApiModel->getPartysum($scope ,$country , $state , $district , $area ,'users_poll_party_state_id');
//             break;

//             case 3:
//             $res         = $this->PartyApiModel->getPartysum($scope ,$country , $state , $district , $area ,'users_party_district_poll_id');
//             break;



//         }
        
       
//         $res_party   = $this->PartyApiModel->getPartylist($scope , $country , $state , $district , $area);
      
          
//         if($res_party){
//         foreach($res_party as $keys => $values){
            
            
//             switch($scope){

//                 case 1:
//                 $res_get_count         = $this->PartyApiModel->getPartypollsumbyid($res_party[$keys]['party_id'],'users_poll_party_id');
//                 break;
    
//                 case 2:
//                 $res_get_count         = $this->PartyApiModel->getPartypollsumbyid($res_party[$keys]['party_id'] ,'users_poll_party_state_id');
//                 break;
    
//                 case 3:
//                 $res_get_count         = $this->PartyApiModel->getPartypollsumbyid($res_party[$keys]['party_id'] ,'users_party_district_poll_id');
//                 break;
    
    
    
//             }

//             switch($scope){

//                 case 1:
//                 $res_party[$keys]['poll_count_public']  = $this->PartyApiModel->getPartypubliccountbyid($res_party[$keys]['party_id'],'users_poll_party_id','users_party_poll_type');
           
//                 break;
    
//                 case 2:
//                 $res_party[$keys]['poll_count_public']  = $this->PartyApiModel->getPartypubliccountbyid($res_party[$keys]['party_id'],'users_poll_party_state_id','users_party_state_poll_type');
           
//                 break;
    
//                 case 3:
//                 $res_party[$keys]['poll_count_public']  = $this->PartyApiModel->getPartypubliccountbyid($res_party[$keys]['party_id'],'users_party_district_poll_id','users_party_district_poll_type');
//                 break;
    
//             }

//             switch($scope){

//                 case 1:
//                 $res_party[$keys]['poll_count_secret']  = $this->PartyApiModel->getPartysecretcountbyid($res_party[$keys]['party_id'],'users_poll_party_id','users_party_poll_type');

//                 break;
    
//                 case 2:
//                 $res_party[$keys]['poll_count_secret']  = $this->PartyApiModel->getPartysecretcountbyid($res_party[$keys]['party_id'],'users_poll_party_state_id','users_party_state_poll_type');

//                 break;
    
//                 case 3:
//                 $res_party[$keys]['poll_count_secret']  = $this->PartyApiModel->getPartysecretcountbyid($res_party[$keys]['party_id'],'users_party_district_poll_id','users_party_district_poll_type');

//                 break;
    
    
    
//             }

//             switch($scope){

//                 case 1:
//                 $res_party[$keys]['isLocation']  = $this->PartyApiModel->isLocation(array('party_id' => $res_party[$keys]['party_id'],'party_country' => $userlocation['country']));
                
//                 break;
    
//                 case 2:
//                  $res_party[$keys]['isLocation']  = $this->PartyApiModel->isLocation(array('party_id' => $res_party[$keys]['party_id'],'party_state' => $userlocation['state']));

//                 break;
              
    
//                 case 3:
//                 $res_party[$keys]['isLocation']  = $this->PartyApiModel->isLocation(array('party_id' => $res_party[$keys]['party_id'],'party_city' => $userlocation['district']));

//                 break;
    
//             }


           
//             if($res_get_count == null) $res_party[$keys]['poll_count']  = 0;
//             else $res_party[$keys]['poll_count'] = $res_get_count;

//             $res_party[$keys]['party_photo']     = base_url().'assets/images/party/'.$res_party[$keys]['party_photo'];
            
//             switch($scope){
            
//                 case 1:
            
//                 $ispolled = $this->PartyApiModel->get_party_poll_user($userid,'users_poll_party_id','users_party_id');
//                 break;
            
//                 case 2:
            
//                 $ispolled = $this->PartyApiModel->get_party_poll_user($userid,'users_poll_party_state_id','users_party_state_id');
//                 break;

//                 case 3:
            
//                 $ispolled = $this->PartyApiModel->get_party_poll_user($userid,'users_party_district_poll_id','users_party_district_id');
//                 break;
//             }

//             if($ispolled['poll'] == $res_party[$keys]['party_id']){
//                 $res_party[$keys]['isPolled'] = 1;
//             }    
//             else{
//                 $res_party[$keys]['isPolled'] = 0;
//             }

//             if($ispolled['join'] == $res_party[$keys]['party_id']){
//                 $res_party[$keys]['isJoined'] = 1;
//             }    
//             else{
//                 $res_party[$keys]['isJoined'] = 0;
//             }
            
//         }
    
//         for($i=0;$i < sizeof($res);$i++){
//             $total += $res[$i]['total'];
//         }
        
       
//         if($res[0]['total']!=0){
//         $k = 0;
//         for($i=0;$i<sizeof($res);$i++){

            
//             $percent                = $res[$i]['total']*100/$total;
//             $res[$i]['percentage']  = intval($percent);

           
            
//             if($k <= 2){
              
//                 switch ($scope){
//                     case 1:
//                     $name                   = $this->PartyApiModel->getPartyname($res[$i]['users_poll_party_id']);
//                     break;

//                     case 2:
//                     $name                   = $this->PartyApiModel->getPartyname($res[$i]['users_poll_party_state_id']);
//                     break;

//                     case 3:
//                     $name                   = $this->PartyApiModel->getPartyname($res[$i]['users_party_district_poll_id']);
//                     break;
//                 }
                
//                 $res[$i]['party_name']  = $name->party_name;
//             }
//             else{
                
//                 switch ($scope){
//                     case 1:
//                     $res[$i]['users_poll_party_id']  = '0';
//                     break;

//                     case 2:
//                     $res[$i]['users_poll_party_state_id']  = '0';

//                     case 3:
//                     $res[$i]['users_party_district_poll_id'] = '0';
//                 }
                
                
//                 $res[$i]['party_name']           = 'others';
//             }
            
//             $k++;
//         }
//     }
//     else{

//         $res = [];
        
//     }

    

//     $this->output->set_output(json_encode(['chart' => $res  ,'party' => $res_party, 'status' => 'true']));
// }
//     else{
//         $this->output->set_output(json_encode(['chart' => $res  ,'party' => array(), 'status' => 'false']));
//     }
   
        
        
//     }      

    function check_isBlocked($userid){

      return $this->PartyApiModel->check_isBlocked(array('user_id' => $userid , 'users_active' => 0));
     }

     // get party chart
     function getchart(){

        $total  =  0;
        $this->output->set_content_type('application/json');

        $userid         = $this->input->post('userid');

        $userlocation   = $this->PartyApiModel->getUserlocation($userid);
        
   

        $scope          = $this->input->post('scope');

        $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;   

        

        if($this->PartyApiModel->check_restricted($country) == 0){
      
         switch($scope){

            case 1:
            $res         = $this->PartyApiModel->getPartysum(1 ,$country , $state , $district , $area ,'users_poll_party_id');
            break;

            case 2:
            $res         = $this->PartyApiModel->getPartysum(1,$country , $state , $district , $area ,'users_poll_party_id');
            break;

            case 3:
            $res         = $this->PartyApiModel->getPartysum(1,$country , $state , $district , $area ,'users_poll_party_id');
            break;

            case 4:
            $res         = $this->PartyApiModel->getPartysum(1,$country , $state , $district , $area ,'users_poll_party_id');
            break;


        }
        
        $res_party       = $this->PartyApiModel->getPartylist(1, $country , $state , $district , $area);

        //$scope = 1;
          
        if($res_party){
        foreach($res_party as $keys => $values){
            
            
            switch($scope){

                case 1:
                $res_get_count         = $this->PartyApiModel->getPartypollsumbyid($res_party[$keys]['party_id'],'users_poll_party_id',$country,$state,$district,$area);
                break;
    
                case 2:
                $res_get_count         = $this->PartyApiModel->getPartypollsumbyid($res_party[$keys]['party_id'] ,'users_poll_party_id',$country,$state,$district,$area);
                break;
    
                case 3:
                $res_get_count         = $this->PartyApiModel->getPartypollsumbyid($res_party[$keys]['party_id'] ,'users_poll_party_id',$country,$state,$district,$area);
                break;
    
                case 4:
                $res_get_count         = $this->PartyApiModel->getPartypollsumbyid($res_party[$keys]['party_id'] ,'users_poll_party_id',$country,$state,$district,$area);
                break;
    
    
            }

            switch($scope){

                case 1:
                $res_party[$keys]['poll_count_public']  = $this->PartyApiModel->getPartypubliccountbyid($res_party[$keys]['party_id'],'users_poll_party_id','users_party_poll_type',$country,$state,$district,$area);
                break;
    
                case 2:
                $res_party[$keys]['poll_count_public']  = $this->PartyApiModel->getPartypubliccountbyid($res_party[$keys]['party_id'],'users_poll_party_state_id','users_party_state_poll_type',$country,$state,$district,$area);
                break;
    
                case 3:
                $res_party[$keys]['poll_count_public']  = $this->PartyApiModel->getPartypubliccountbyid($res_party[$keys]['party_id'],'users_party_district_id','users_party_district_poll_type',$country,$state,$district,$area);
                break;

                case 4:
                $res_party[$keys]['poll_count_public']  = $this->PartyApiModel->getPartypubliccountbyid($res_party[$keys]['party_id'],'users_party_area_id','users_party_area_poll_type',$country,$state,$district,$area);
                break;
    
            }

            switch($scope){

                case 1:
                $res_party[$keys]['poll_count_secret']  = $this->PartyApiModel->getPartysecretcountbyid($res_party[$keys]['party_id'],'users_poll_party_id','users_party_poll_type',$country,$state,$district,$area);

                break;
    
                case 2:
                
                $res_party[$keys]['poll_count_secret']  = $this->PartyApiModel->getPartysecretcountbyid($res_party[$keys]['party_id'],'users_poll_party_state_id','users_party_state_poll_type',$country,$state,$district,$area);
                break;
    
                case 3:
                $res_party[$keys]['poll_count_secret']  = $this->PartyApiModel->getPartysecretcountbyid($res_party[$keys]['party_id'],'users_party_district_poll_id','users_party_district_poll_type',$country,$state,$district,$area);

                break;
    
                case 4:
                $res_party[$keys]['poll_count_secret']  = $this->PartyApiModel->getPartysecretcountbyid($res_party[$keys]['party_id'],'users_party_area_poll_id','users_party_area_poll_type',$country,$state,$district,$area);

                break;
    
    
            }

            switch($scope){
                
                case 1:
                $check_location     = $this->PartyApiModel->isLocation(array('party_id' => $res_party[$keys]['party_id'],'party_country' => $userlocation['country']));
                $res_party[$keys]['isLocation'] = $check_location > 0 ? 1 : 0; 
                break;
    
                case 2:
                $check_location     = $this->PartyApiModel->isLocation(array('party_id' => $res_party[$keys]['party_id'],'party_state' => $userlocation['state']));
               
                $cmp = strcmp(strtolower($userlocation['state']),strtolower($state));
               
                $res_party[$keys]['isLocation'] =  ($cmp == 0) ? 1 : 0;
              
                
    
                break;
              
    
                case 3:
                $check_location     = $this->PartyApiModel->isLocation(array('party_id' => $res_party[$keys]['party_id'],'party_city' => $userlocation['district']));
              
                $cmp = strcmp(strtolower($userlocation['district']),strtolower($district));
                
                $res_party[$keys]['isLocation'] = ($cmp == 0 ) ? 1 : 0;
                break;

                case 4:
                $check_location     = $this->PartyApiModel->isLocation(array('party_id' => $res_party[$keys]['party_id'],'party_area' => $userlocation['area']));
              
                $cmp = strcmp(strtolower($userlocation['area']),strtolower($area)); 
                
                $res_party[$keys]['isLocation'] = ($cmp == 0 ) ? 1 : 0;
                break;
                
            }


           
            if($res_get_count == null) $res_party[$keys]['poll_count']  = 0;
            else $res_party[$keys]['poll_count'] = $res_get_count;

            $res_party[$keys]['party_photo']     = $this->media_url.'assets/images/party/'.$res_party[$keys]['party_photo'];
           
            switch($scope){
            
                case 1:
            
                $ispolled = $this->PartyApiModel->get_party_poll_user($userid,'users_poll_party_id','users_party_id');
                break;
            
                case 2:
            
                $ispolled = $this->PartyApiModel->get_party_poll_user($userid,'users_poll_party_id','users_party_id');
                break;

                case 3:
            
                $ispolled = $this->PartyApiModel->get_party_poll_user($userid,'users_poll_party_id','users_party_id');
                break;

                case 4:
            
                $ispolled = $this->PartyApiModel->get_party_poll_user($userid,'users_poll_party_id','users_party_id');
                break;
            }

            if($ispolled['poll'] == $res_party[$keys]['party_id']){
                $res_party[$keys]['isPolled'] = 1;
            }    
            else{
                $res_party[$keys]['isPolled'] = 0;
            }

            if($ispolled['join'] == $res_party[$keys]['party_id']){
                $res_party[$keys]['isJoined'] = 1;
            }    
            else{
                $res_party[$keys]['isJoined'] = 0;
            }
            
        }
    
        for($i=0;$i < sizeof($res);$i++){
            $total += $res[$i]['total'];
        }
        
       
        if($res[0]['total']!=0){
        $k = 0;
        for($i=0;$i<sizeof($res);$i++){

            
            $percent                = $res[$i]['total']*100/$total;
            $res[$i]['percentage']  = intval($percent);

           
            
            if($k <= 2){
              
                switch ($scope){
                    case 1:
                    $name                   = $this->PartyApiModel->getPartyname($res[$i]['users_poll_party_id']);
                    break;

                    case 2:
                    $name                   = $this->PartyApiModel->getPartyname($res[$i]['users_poll_party_id']);
                    break;

                    case 3:
                    $name                   = $this->PartyApiModel->getPartyname($res[$i]['users_poll_party_id']);
                    break;

                    case 4:
                    $name                   = $this->PartyApiModel->getPartyname($res[$i]['users_poll_party_id']);
                    break;
                }
                
                $res[$i]['party_name']  = $name->party_name;
            }
            else{
                
                switch ($scope){
                    case 1:
                    $res[$i]['users_poll_party_id']  = '0';
                    break;

                    case 2:
                    $res[$i]['users_poll_party_id']  = '0';

                    case 3:
                    $res[$i]['users_poll_party_id'] = '0';
                    
                    case 4:
                    $res[$i]['users_poll_party_id'] = '0';
                }
                
                
                $res[$i]['party_name']           = 'others';
            }
            
            $k++;
        }
    }
    else{

        $res = [];
        
    }

    

    $this->output->set_output(json_encode(['chart' => $res,'isRestricted' => 0  ,'party' => $res_party, 'status' => 'true']));
}
    else{
        $this->output->set_output(json_encode(['chart' => $res,'isRestricted' => 0  ,'party' => array(), 'status' => 'false']));
    }
}
    else{
        $this->output->set_output(json_encode(['chart' => '' ,'isRestricted' => 1, 'msg'=> 'party not available'  ,'party' => array(), 'status' => 'false']));
 
    }
   
        
        
    }      



    // get party chart
    function getchartupdate(){

        $total=0;
        $this->output->set_content_type('application/json');

        $userid = $this->input->post('userid');
        $scope  = $this->input->post('scope');

        $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;   

        if($this->PartyApiModel->check_restricted($country) == 0){
        switch($scope){

            case 1:
            $res         = $this->PartyApiModel->getPartysum($scope ,$country , $state , $district , $area ,'users_poll_party_id');
            break;

            case 2:
            $res         = $this->PartyApiModel->getPartysum($scope ,$country , $state , $district , $area ,'users_poll_party_state_id');
            break;

            case 3:
            $res         = $this->PartyApiModel->getPartysum($scope ,$country , $state , $district , $area ,'users_party_district_poll_id');
            break;

            case 4:
            $res         = $this->PartyApiModel->getPartysum($scope ,$country , $state , $district , $area ,'users_party_area_poll_id');
            break;

        }
      
        
       
        for($i=0;$i < sizeof($res);$i++){
            $total += $res[$i]['total'];
        }
        
       
        if($res[0]['total']!=0){
        $k = 0;
        for($i=0;$i<sizeof($res);$i++){

            
            $percent                = $res[$i]['total']*100/$total;
            $res[$i]['percentage']  = intval($percent);

           
            
            if($k <= 2){
              
                switch ($scope){
                    case 1:
                    $name                   = $this->PartyApiModel->getPartyname($res[$i]['users_poll_party_id']);
                    break;

                    case 2:
                    $name                   = $this->PartyApiModel->getPartyname($res[$i]['users_poll_party_state_id']);
                    break;

                    case 3:
                    $name                   = $this->PartyApiModel->getPartyname($res[$i]['users_party_district_poll_id']);
                    break;

                    case 4:
                    $name                   = $this->PartyApiModel->getPartyname($res[$i]['users_party_area_poll_id']);
                    break;
                }
                
                $res[$i]['party_name']  = $name->party_name;
            }
            else{
                
                switch ($scope){

                    case 1:
                    $res[$i]['users_poll_party_id']  = '0';
                    break;

                    case 2:
                    $res[$i]['users_poll_party_state_id']  = '0';

                    case 3:
                    $res[$i]['users_party_district_poll_id'] = '0';

                    case 4:
                    $res[$i]['users_party_area_poll_id'] = '0';
                   
                }
                
                
                $res[$i]['party_name']           = 'others';
            }
            
            $k++;
        }
    }
        else{

            //$res = [];
            
        }
        
        $this->output->set_output(json_encode(['chart' => $res,'status' => 'true']));

}
else{
    $this->output->set_output(json_encode(['chart' => '' , 'msg'=> 'party not available'  ,'party' => array(), 'status' => 'false']));

}
        
    }  

    // claim your party
    function claimParty(){

        $this->output->set_content_type('application/json');

        $userid         = $this->input->post('userid');
        $partyid        = $this->input->post('partyid');
        $presidentname  = $this->input->post('presidentname');
        $secretaryname  = $this->input->post('secretaryname');
        $email          = $this->input->post('email');
        $address        = $this->input->post('address');
        $comment        = $this->input->post('comment');

        if(!empty($userid && $partyid)){

            if(isset($_FILES['claim_party_attachment'])){
                $errors= array();
                $file_name = $_FILES['claim_party_attachment']['name'];
                $file_size = $_FILES['claim_party_attachment']['size'];
                $file_tmp  = $_FILES['claim_party_attachment']['tmp_name'];
                $file_type = $_FILES['claim_party_attachment']['type'];
                $tmp       = explode('.', $file_name);
                $file_ext  = end($tmp);
                
                
                $rand_name_attachment = "attachments".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;
                
                $expensions= array("jpeg","jpg","png");
                
                if(in_array($file_ext,$expensions)=== false){
                   $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                }
                
                
                
                if(empty($errors)==true){
                   
                   move_uploaded_file($file_tmp,"assets/images/party/attachments/".$rand_name_attachment);
                   $attachment_photo = $rand_name_attachment;
                   
                }else{
                    $attachment_photo = "null";
                }


                
               
            }
            


            if(isset($_FILES['claim_party_president_image'])){
                $errors= array();
                $file_name = $_FILES['claim_party_president_image']['name'];
                $file_size =$_FILES['claim_party_president_image']['size'];
                $file_tmp =$_FILES['claim_party_president_image']['tmp_name'];
                $file_type=$_FILES['claim_party_president_image']['type'];
                $tmp = explode('.', $file_name);
                $file_ext = end($tmp);
                
                
                $rand_name_president = "president".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;
                
                $expensions= array("jpeg","jpg","png");
                
                if(in_array($file_ext,$expensions)=== false){
                   $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                }
                
                
                
                if(empty($errors)==true){
                   
                   move_uploaded_file($file_tmp,"assets/images/party/president/".$rand_name_president);
                   $president_photo= $rand_name_president;
                   
                }else{
                    $president_photo= "none.png";
                }
      
            }



            if(isset($_FILES['claim_party_secretary_image'])){
                $errors= array();
                $file_name = $_FILES['claim_party_secretary_image']['name'];
                $file_size =$_FILES['claim_party_secretary_image']['size'];
                $file_tmp =$_FILES['claim_party_secretary_image']['tmp_name'];
                $file_type=$_FILES['claim_party_secretary_image']['type'];
                $tmp = explode('.', $file_name);
                $file_ext = end($tmp);
                
                
                $rand_name_secretary = "secretary".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;
                
                $expensions= array("jpeg","jpg","png");
                
                if(in_array($file_ext,$expensions)=== false){
                   $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                }
                
                
                
                if(empty($errors)==true){
                   
                   move_uploaded_file($file_tmp,"assets/images/party/secretary/".$rand_name_secretary);
                   $secretary_photo = $rand_name_secretary;
                   
                }else{
                    $secretary_photo= "none.png";
                }
      
            }



        $insert = array('claim_party_party_id'          => $partyid,
                        'claim_party_user_id'           => $userid,
                        'claim_party_president_name'    => $presidentname,
                        'claim_party_president_image'   => $president_photo,
                        'claim_party_secretary_image'   => $secretary_photo,
                        'claim_party_attachment'        => $attachment_photo,
                        'claim_party_secretary_name'    => $secretaryname,
                        'claim_party_email'             => $email,
                        'claim_party_address'           => $address,
                        'claim_party_comment'           => $comment    
                        );

        $res = $this->PartyApiModel->claimParty($insert);
        
        if($res > 0){

            $this->output->set_output(json_encode(['message' => 'successfully inserted','status' => 'true']));

        }
        else{
            $this->output->set_output(json_encode(['message' => 'failed to insert','status' => 'false']));

        }
    
}
    else{
        $this->output->set_output(json_encode(['message' => 'userid and partyid required','status' => false ]));
    }
        
    }


    // party posts new api



    //poll posts
    function pollpartyPosts(){

        $this->output->set_content_type('application/json');
        
        $post_user_id    = $this->input->post('postuserid');
        $posts_id        = $this->input->post('postid');
        $user_id         = $this->input->post('userid');
        
        if(!empty($posts_id && $user_id)){
           
        $res        = $this->PartyApiModel->pollpartyPosts($user_id,$posts_id,$post_user_id);
        
        if($res['res'] == 1){
       
            $this->output->set_output(json_encode(array('is_polled'=>true,'message'=>"successfully polled",'status'=>true)));
        }
        
        else if($res['res'] == -1){

            $this->output->set_output(json_encode(array('is_polled'=>false,'message'=>'successfully unpolled','status'=>true)));
        
        }
        }
        else{
            $this->output->set_output(json_encode(array('message'=>"parameter missing",'status'=>false)));
        }
        
    }


    function likeComment(){
        
        $this->output->set_content_type('application/json');
        $commentid      = $this->input->post('commentid');
        $userid         = $this->input->post('userid');
        $commentuserid  = $this->input->post('commentuserid');
        
        if(!empty($commentid && $userid && $commentuserid)){
        $insert = array('comment_likes_comment_id'      => $commentid,
                        'comment_likes_user_id'         => $userid,
                        'comment_likes_comment_user_id' => $commentuserid
                        );
                        

        $res = $this->CommonModel->likeComment($insert ,'party_posts_comment_likes');
            if($res == 1 )                
            $this->output->set_output(json_encode(array('message' => 'liked','status'=>true)));
            else if($res == -1) 
            $this->output->set_output(json_encode(array('message' => 'unliked','status'=>true)));  
        }else{
            $this->output->set_output(json_encode(array('message' => "parameter missing",'status'=>false)));
        }            

    }






    //comment party posts api
    function commentpartyPost(){

        $this->output->set_content_type('application/json');

        $post_id          = $this->input->post('post_id');
        $user_id          = $this->input->post('user_id');
        $post_user_id     = $this->input->post('postuserid');
        $comment          = $this->input->post('comment');
        
        if(!empty($post_id && $user_id && $comment)){

        $res              = $this->PartyApiModel->commentpartyPost($user_id , $post_id , $comment , $post_user_id);

        

        if($res > 0){

            $this->output->set_output(json_encode(array('message' => "successfully commented",'comment'=>$res['data'],'status'=>true)));
        }
        else{
            
            $this->output->set_output(json_encode(array('posts' => "failed to comment",'status'=>false)));
            }
        }
    else{
            $this->output->set_output(json_encode(array('posts' => "parameter missing",'status'=>false)));
    }
    }

    // get post comments
    function getpartyPostcomments(){ 

        $this->output->set_content_type('application/json');

        $post_id     = $this->input->post('post_id');
        
        $res         = $this->PartyApiModel->getpartyPostcomments($post_id);

        foreach($res as $key => $value){
            $res[$key]['users_photo']   = $this->media_url."assets/images/users/".$res[$key]['users_photo'];
            $res_reply                  = $this->PartyApiModel->getpartycommentsreply($res[$key]['party_posts_comments_id'],$res[$key]['party_posts_comments_post_id']);
            foreach($res_reply as $key_1 => $value_1){
                $res_reply[$key_1]['reply_to_user']     = $this->PartyApiModel->getusersnamebyid($res_reply[$key_1]['party_posts_comment_reply_comment_user_id']);
                $res_reply[$key_1]['users_photo']       = $this->media_url."assets/images/users/".$res_reply[$key_1]['users_photo'];
            }
            $res[$key]['reply']         = $res_reply;
        }

        if($res!=null){

            $this->output->set_output(json_encode(array('data' => $res,'status'=>true)));
        }
        else{
            $this->output->set_output(json_encode(array('data' => "no value found",'status'=>false)));
        }
    }

    // get post comments by id
    function getpartyPostcommentsbyid(){ 

        $this->output->set_content_type('application/json');

        $post_id     = $this->input->post('comment_id');
        
        $res         = $this->PartyApiModel->getpartyPostcommentsbyid($post_id);

        foreach($res as $key => $value){
            $res[$key]['users_photo'] = $this->media_url."assets/images/users/".$res[$key]['users_photo'];
            $res_reply          = $this->PartyApiModel->getpartycommentsreply($res[$key]['party_posts_comments_id'],$res[$key]['party_posts_comments_post_id']);
            
            foreach($res_reply as $key_1 => $value_1){
                
                $res_reply[$key_1]['reply_to_user']     = $this->PartyApiModel->getusersnamebyid($res_reply[$key_1]['party_posts_comment_reply_comment_user_id']);
               
                $res_reply[$key_1]['users_photo']       = $this->media_url."assets/images/users/".$res_reply[$key_1]['users_photo'];
            }
            $res[$key]['reply'] = $res_reply;
        }

        if($res!=null){

            $this->output->set_output(json_encode(array('data' => $res,'status'=>true)));
        }
        else{
            $this->output->set_output(json_encode(array('data' => "no value found",'status'=>false)));
        }
    }
    
    // reply to comments
    function replypartyPostscomments(){

        $this->output->set_content_type('application/json');

        $post_id            = $this->input->post('post_id');
        $user_id            = $this->input->post('user_id');
        $comment_id         = $this->input->post('comment_id');
        $comment_user       = !empty($this->input->post('comment_user_id')) ? $this->input->post('comment_user_id') : 1;
        $reply              = $this->input->post('reply');
        
        $insert      = array('party_posts_comments_reply_posts_id'=>$post_id,
                        'party_posts_comments_reply_comment_id'=>$comment_id,
                        'party_posts_comments_reply'=>$reply,
                        'party_posts_comment_reply_comment_user_id' => $comment_user,
                        'party_posts_comments_reply_user_id' => $user_id);

        $res         = $this->PartyApiModel->replypartyPostscomments($insert,$comment_user);

        if($res > 0){

            $this->output->set_output(json_encode(array('message' => "succesfully inserted",'status'=>true)));
        }
        else{

            $this->output->set_output(json_encode(array('posts' => "no value found",'status'=>false)));
        }
    }

    // get created party list
    function getcreatedParties(){

        $this->output->set_content_type('application/json');
        $id = $this->input->post('userid');
        if(!empty ($id)){

        
        $res_created = $this->PartyApiModel->getcreatedParties($id);
        $res_joined  = $this->PartyApiModel->getjoinedParties($id);
        
       
        foreach($res_created as $value => $key) {

            switch($res_created[$value]['party_scope']){
            
                case 1:
                $res_created[$value]['party_photo']       = $this->media_url."assets/images/party/".$res_created[$value]['party_photo'];
                $res_created[$value]['pollpublicCount']   = $this->PartyApiModel->publicCountpartycreated($res_created[$value]['party_id'],'users_poll_party_id','users_party_poll_type');
                $res_created[$value]['pollsecretCount']   = $this->PartyApiModel->secretCountpartycreated($res_created[$value]['party_id'],'users_poll_party_id','users_party_poll_type');
                $res_created[$value]['poll_count']        = $res_created[$value]['pollpublicCount'] + $res_created[$value]['pollsecretCount'];  
                break;  
                
                case 2:
                $res_created[$value]['party_photo']       = $this->media_url."assets/images/party/".$res_created[$value]['party_photo'];
                $res_created[$value]['pollpublicCount']   = $this->PartyApiModel->publicCountpartycreated($res_created[$value]['party_id'],'users_poll_party_state_id','users_party_state_poll_type');
                $res_created[$value]['pollsecretCount']   = $this->PartyApiModel->secretCountpartycreated($res_created[$value]['party_id'],'users_poll_party_state_id','users_party_state_poll_type');
                $res_created[$value]['poll_count']        = $res_created[$value]['pollpublicCount'] + $res_created[$value]['pollsecretCount'];
                break; 

                case 3:
                $res_created[$value]['party_photo']       = $this->media_url."assets/images/party/".$res_created[$value]['party_photo'];
                $res_created[$value]['pollpublicCount']   = $this->PartyApiModel->publicCountpartycreated($res_created[$value]['party_id'],'users_party_district_poll_id','users_party_district_poll_type');
                $res_created[$value]['pollsecretCount']   = $this->PartyApiModel->secretCountpartycreated($res_created[$value]['party_id'],'users_party_district_poll_id','users_party_district_poll_type');
                $res_created[$value]['poll_count']        = $res_created[$value]['pollpublicCount'] + $res_created[$value]['pollsecretCount'];
                break; 

                case null:
                $res_created[$value]['party_photo']       = $this->media_url."assets/images/party/".$res_created[$value]['party_photo'];
                $res_created[$value]['pollpublicCount']   = $this->PartyApiModel->publicCountpartycreated($res_created[$value]['party_id'],'users_party_district_poll_id','users_party_district_poll_type');
                $res_created[$value]['pollsecretCount']   = $this->PartyApiModel->secretCountpartycreated($res_created[$value]['party_id'],'users_party_district_poll_id','users_party_district_poll_type');
                $res_created[$value]['poll_count']        = $res_created[$value]['pollpublicCount'] + $res_created[$value]['pollsecretCount'];
                break;
        }    
        }

        foreach($res_joined as $value => $key) {

            // $res_joined[$value]['party_photo']       = base_url()."assets/images/party/".$res_joined[$value]['party_photo'];
            // $res_joined[$value]['pollpublicCount']   = $this->PartyApiModel->publicCountpartycreated($res_created[$value]['party_id']);
            // $res_joined[$value]['pollsecretCount']   = $this->PartyApiModel->secretCountpartycreated($res_created[$value]['party_id']);
     
            switch($res_joined[$value]['party_scope']){
            
                case 1:
                $res_joined[$value]['party_photo']       = $this->media_url."assets/images/party/".$res_joined[$value]['party_photo'];
                $res_joined[$value]['pollpublicCount']   = $this->PartyApiModel->publicCountpartycreated($res_joined[$value]['party_id'],'users_poll_party_id','users_party_poll_type');
                $res_joined[$value]['pollsecretCount']   = $this->PartyApiModel->secretCountpartycreated($res_joined[$value]['party_id'],'users_poll_party_id','users_party_poll_type');
                $res_joined[$value]['poll_count']        = $res_joined[$value]['pollpublicCount'] + $res_created[$value]['pollsecretCount'];
                break;  
                
                case 2:
                $res_joined[$value]['party_photo']       = $this->media_url."assets/images/party/".$res_joined[$value]['party_photo'];
                $res_joined[$value]['pollpublicCount']   = $this->PartyApiModel->publicCountpartycreated($res_joined[$value]['party_id'],'users_poll_party_state_id','users_party_state_poll_type');
                $res_joined[$value]['pollsecretCount']   = $this->PartyApiModel->secretCountpartycreated($res_joined[$value]['party_id'],'users_poll_party_state_id','users_party_state_poll_type');
                $res_joined[$value]['poll_count']        = $res_joined[$value]['pollpublicCount'] + $res_created[$value]['pollsecretCount'];
                break; 

                case 3:
                $res_joined[$value]['party_photo']       = $this->media_url."assets/images/party/".$res_joined[$value]['party_photo'];
                $res_joined[$value]['pollpublicCount']   = $this->PartyApiModel->publicCountpartycreated($res_joined[$value]['party_id'],'users_party_district_poll_id','users_party_district_poll_type');
                $res_joined[$value]['pollsecretCount']   = $this->PartyApiModel->secretCountpartycreated($res_joined[$value]['party_id'],'users_party_district_poll_id','users_party_district_poll_type');
                $res_joined[$value]['poll_count']        = $res_joined[$value]['pollpublicCount'] + $res_created[$value]['pollsecretCount'];
                break; 

                case null:
                $res_joined[$value]['party_photo']       = $this->media_url."assets/images/party/".$res_joined[$value]['party_photo'];
                $res_joined[$value]['pollpublicCount']   = $this->PartyApiModel->publicCountpartycreated($res_joined[$value]['party_id'],'users_party_district_poll_id','users_party_district_poll_type');
                $res_joined[$value]['pollsecretCount']   = $this->PartyApiModel->secretCountpartycreated($res_joined[$value]['party_id'],'users_party_district_poll_id','users_party_district_poll_type');
                $res_joined[$value]['poll_count']        = $res_joined[$value]['pollpublicCount'] + $res_created[$value]['pollsecretCount'];
                break;
        }    

        }
        
            $this->output->set_output(json_encode(array('message' => 'value found','created_party' => $res_created,'joined_party' => $res_joined,'status'=>true)));
       
      
        }
        else{
            $this->output->set_output(json_encode(array('message' => "userid required",'status'=>false)));
       
        }


    }


    // get public polled members
    function getPolledmembers(){

        $this->output->set_content_type('application/json');
        $partyid = $this->input->post('partyid');
        $scope   = $this->input->post('scope');
      
        if(!empty ($partyid && $scope)){
            
            switch($scope){
                case 1:
                $res = $this->PartyApiModel->getPolledmembers($partyid , 'users_poll_party_id' , 'users_party_poll_type'); 
                break;

                case 2:
                $res = $this->PartyApiModel->getPolledmembers($partyid , 'users_poll_party_state_id' ,'users_party_state_poll_type');
                break;

                case 3:
                $res = $this->PartyApiModel->getPolledmembers($partyid , 'users_party_district_poll_id' , 'users_party_district_poll_type');
                break;

            }   

            if($res != null){

            
           foreach($res as  $key => $value){
               
               $res[$key]['users_photo']  = $this->media_url."assets/images/users/".$res[$key]['users_photo'];
               $res[$key]['poll_count']   = $this->PartyApiModel->Joinmembersbypollnolimit($res[$key]['user_id'],$partyid);   
           }

           $this->output->set_output(json_encode(array('message' => "value found",'users' => $res,'status'=>true)));
       
        }
        else{
            $this->output->set_output(json_encode(array('message' => "no value found",'status'=>false)));
       
        }
        }
        else{
            $this->output->set_output(json_encode(array('message' => "parameter missing",'status'=>false)));
       
        }
    }


     // delete party posts posts
     function deletepartyPosts(){

        $this->output->set_content_type('applcaition/json');
        
        $postid         = $this->input->post('postid');
        $partyid        = $this->input->post('partyid');
        $userid         = $this->input->post('userid');
        $scope          = $this->input->post('scope'); 

        $page_start  = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;


        if(!empty($postid)){

            
        $res        = $this->PartyApiModel->deletepartyPosts($postid);
        if($res > 0){

            $posts = $this->returngetPartyProfile($partyid,$userid,$scope,$page_start);
              
            if(count($posts) >= 9 )
            $last_posts = $posts[9];
            else 
            $last_posts = null;
          
            $delete_notifications = $this->PartyApiModel->deletenotifications($postid);

            if($last_posts == null)
            $this->output->set_output(json_encode(array('value' => "successfully deleted",'is_completed' => 1,'status'=>true)));
            
            else{
            if($last_posts['is_completed'] > 0)
            $this->output->set_output(json_encode(array('value' => "successfully deleted",'data' => array($last_posts),'is_completed' => 0,'status'=>true)));
            
            else
            $this->output->set_output(json_encode(array('value' => "successfully deleted",'data' => array($last_posts),'is_completed' => 1,'status'=>true)));
            }


            
            // if($last_posts['party_posts_id'] == $postid || $last_posts == null)
            // $this->output->set_output(json_encode(array('value' => "successfully deleted",'is_completed'=>1,'status'=>true)));
            // else
            // $this->output->set_output(json_encode(array('value' => "successfully deleted",'data' => array($last_posts),'is_completed' => $last_posts['is_completed'] > 0 ? 0 : 1,'status'=>true)));
            
            } 
        else{
            $this->output->set_output(json_encode(array('value' => 'operation failed' , 'status' => false))); 
        }
        }
        else{
            $this->output->set_output(json_encode(array('value' => 'parameter missing' , 'status' => false))); 
   
        }
    
    }

     // report posts
     function reportpartyPost(){

        $this->output->set_content_type('application/json');

        $user_id     = $this->input->post('user_id');
        $post_id     = $this->input->post('post_id');
        $msg         = $this->input->post('message');
        $scope       = $this->input->post('scope');
        $partyid     = $this->input->post('partyid');
        $page_start  = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;



        if(!empty($user_id && $post_id)){
           
        $res         = $this->PartyApiModel->reportpartyPost($user_id , $post_id , $msg);

           
        switch($res){

            case $res > 0:
            $this->PartyApiModel->updatepartyReportcount($post_id , $msg);
            
            $posts = $this->returngetPartyProfile($partyid,$user_id,$scope,$page_start);
              
            if(count($posts) >= 9 )
            $last_posts = $posts[9];
            else 
            $last_posts = null;
        
            $removePosts = $this->PartyApiModel->removePostsbypartyreportcount($post_id);
            
            if($last_posts == null)
            $this->output->set_output(json_encode(array('value' => "successfully reported",'is_completed' => 1,'status'=>true)));
            
            else{
            if($last_posts['is_completed'] > 0)
            $this->output->set_output(json_encode(array('value' => "successfully reported",'data' => array($last_posts),'is_completed' => 0,'status'=>true)));
            
            else
            $this->output->set_output(json_encode(array('value' => "successfully reported",'data' => array($last_posts),'is_completed' => 1,'status'=>true)));
            }
            $remove_post = $this->PartyApiModel->removePartyPostbyCount($post_id);
     
            break;

            case -1:
            $this->output->set_output(json_encode(array('value' => "already reported",'status'=>false)));
            break;
            
            case 0:
            $this->output->set_output(json_encode(array('value' => "operation failed",'status'=>false)));
            break;

        }
    }
        else{
            $this->output->set_output(json_encode(array('value' => "parameter missing",'status'=>false)));
           
        }
     
    }



     // hide posts
     function hidepartyPosts(){

        $this->output->set_content_type('application/json');
        
        $postid     = $this->input->post('postid');
        $userid     = $this->input->post('userid');
        $partyid    = $this->input->post('partyid');
        $scope      = $this->input->post('scope');  
        $page_start  = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;

        if(!empty($postid && $userid)){

        $res        = $this->PartyApiModel->hidePosts($postid , $userid);
        if($res > 0){


            $posts = $this->returngetPartyProfile($partyid,$userid,$scope,$page_start);
              
           
            if(count($posts) >= 9 )
            $last_posts = $posts[9];
            else 
            $last_posts = null;
            
            if($last_posts == null)
            $this->output->set_output(json_encode(array('value' => "operation success",'is_completed' => 1,'status'=>true)));
            
            else{
            if($last_posts['is_completed'] > 0)
            $this->output->set_output(json_encode(array('value' => "operation success",'data' => array($last_posts),'is_completed' => 0,'status'=>true)));
            
            else
            $this->output->set_output(json_encode(array('value' => "operation success",'data' => array($last_posts),'is_completed' => 1,'status'=>true)));
            }
        
            } 
        else{
            $this->output->set_output(json_encode(array('value' => 'operation failed' , 'status' => false))); 
        }
        }
        else{
            $this->output->set_output(json_encode(array('value' => 'parameter missing' , 'status' => false))); 
   
        }
    
    }


    // edit posts
    function editpartyPosts(){

        $this->output->set_content_type('applcation/json');
        
       
        $postid       = $this->input->post('postid');
        $title        = $this->input->post('title');
        $content      = $this->input->post('content');  
        $linecount    = $this->input->post('party_posts_line_count'); 
        
        if(!empty($postid)){

        
        $update     = array('party_posts_title' => $title ,'party_posts_line_count' => $linecount,'party_posts_uploaded_date' => date('Y-m-d h:m:s'),'party_posts_content' => $content);
        $res        = $this->PartyApiModel->editPosts($postid , $update);
        if($res > 0){

            $this->output->set_output(json_encode(array('value' => 'successfully updated' , 'status' => true)));   
        } 
        else{
            $this->output->set_output(json_encode(array('value' => 'operation failed' , 'status' => false))); 
        }
     }
        else{
            $this->output->set_output(json_encode(array('value' => 'parameter missing' , 'status' => false))); 
   
        }
       
        
     
    }

      // get posts api by posts id
      function getnotificationpartyPostsbyid(){

            
        $this->output->set_content_type('application/json');
       
        $postsid = $this->input->post('postid');
        $userid  = $this->input->post('userid');
        
        if(!empty($postsid)){

        $res                  = $this->PartyApiModel->getnotificationpartyPostsbyid($postsid);
        $pollcount            = $this->PartyApiModel->partypollsCount($postsid);
        $isPostpolled         = $this->PartyApiModel->isPostpolled($postsid,$userid);
        $commentcount         = $this->PartyApiModel->partypostscommentsCount($postsid);
        $comments             = $this->PartyApiModel->getpartyPostcomments($postsid);
        $commentsCountouter   = $this->PartyApiModel->commentsCountouter($postsid);

        foreach($comments as $co => $va){
            
            $comments[$co]['users_photo'] = $this->media_url."assets/images/users/".$comments[$co]['users_photo']; 
        }
        if($res > 0){
            
            
            foreach($res as $key => $val){

                $res[$key]['party_posts_photo_video']       = $this->s3_url."assets/images/partyposts/".$res[$key]['party_posts_photo_video'];
                $res[$key]['party_posts_video_thumbnail']   = $this->s3_url."assets/images/partyposts/thumbnail/".$res[$key]['party_posts_video_thumbnail'];
                $res[$key]['users_photo']                   = $this->media_url."assets/images/users/".$res[$key]['users_photo'];
                $res[$key]['posts_poll_count']              = $pollcount;
                $res[$key]['posts_comment_count']           = $commentcount;//$commentsCountouter;
                if($comments == 0)$comments = [];
                $res[$key]['comment']                       = $comments;
                $res[$key]['isPolled']                      = $isPostpolled > 0 ? 1 : 0;
                
            }
            $this->output->set_output(json_encode(array('value' => $res , 'status' => true))); 
        }
        else{
            $this->output->set_output(json_encode(array('value' => 'no data found' , 'status' => false))); 
        }
       
        }
        else{

            $this->output->set_output(json_encode(array('value' => 'parameter missing' ,'status' => false)));
        }
    }

    // get posts api by posts id
    function isPartyRestricted(){

        $this->output->set_content_type('application/json');
        $country = $this->input->post('country');
        $res = $this->PartyApiModel->check_restricted($country);
        if($res > 0){
            $this->output->set_output(json_encode(array('message' => 'party restricted' ,'status' => false)));
        }else{
            $this->output->set_output(json_encode(array('message' => 'party not restricted' ,'status' => true)));
        }
    }    


}



?>