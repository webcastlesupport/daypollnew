<?php

function sendAndroidPush($devices,$data)
{
    //dd($data);
    $fields = array('registration_ids' => $devices,
                    'data'              => $data);
   

    $headers = array('Authorization: key=' .'AIzaSyBF0ugiTToEZrd7ckJ1TPZmn6pdJo535jo','Content-Type: application/json');
    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL,'https://fcm.googleapis.com/fcm/send');
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
    $result = curl_exec($ch );
    curl_close( $ch ); 
   //dd($result);
    return $result;
}


function sendIosPush($devices,$data)
{
    foreach ($devices as $key => $value) 
    {
        if(strlen($value) > 30)
        {
            Log::info('IOS Push --> 1');
            $msg=$data['message'];
             // $apnsServer = 'ssl://gateway.sandbox.push.apple.com:2195';
            $apnsServer ='ssl://gateway.push.apple.com:2195';
            $privateKeyPassword = '';
            $message = $msg;
            $deviceToken =$value;
            $certificate =getcwd();
           // dd($certificate);
            $pushCertAndKeyPemFile = $certificate.'/iOS_certificates/Cukomx.pem';
            $stream = stream_context_create();
            stream_context_set_option($stream, 'ssl', 'passphrase', $privateKeyPassword);

            stream_context_set_option($stream, 'ssl', 'local_cert', $pushCertAndKeyPemFile);

            $connectionTimeout = 20;
            $connectionType = STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT;
            $connection = stream_socket_client($apnsServer, $errorNumber, $errorString, $connectionTimeout, $connectionType, $stream);

            $messageBody['aps'] = array('alert' => $message, 'sound' => 'default', 'badge' => 2);



            $data['aps']['badge']=0;
            $payload = json_encode($messageBody);
           // dd($payload);
            Log::info('IOS Push --> payload', $messageBody);
            $notification = chr(0) .
            pack('n', 32) .

           // $deviceToken = pack('H*', str_replace(' ', '', sprintf('%u', CRC32($deviceToken))));

            pack('H*', $deviceToken) .
            pack('n', strlen($payload)) .
            $payload;
            $wroteSuccessfully = fwrite($connection, $notification, strlen($notification));
            Log::info('IOS Push --> 2');
            if (!$wroteSuccessfully){
                Log::info('IOS Push --> 3');
                $result[]= "Could not send the message<br/>";
            }
            else {
                Log::info('IOS Push --> 4');
                $result[]= "Successfully sent the message<br/>";
            }
            Log::info('IOS Push --> 5');
            fclose($connection);
        }
    }

}
?>