<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Functions extends CI_Controller
{
    public function __construct()
    {

    }

    //check_exp_time_and_is_login
    public function check_exp_time_and_is_login($exp_time,$is_login)
    {
        $CI =& get_instance();
        $CI->load->model('model');
        $CI->load->library('session');
        //@first param - expire time in seconds - [time]/false
        //@second param - need to check logined or not - true/false
        if($exp_time!=false)
        {
            if($CI->session->userdata('most_recent_activity') && (time() - $CI->session->userdata('most_recent_activity') > $exp_time))
            {
                //1800 seconds = 30 Minutes
                $data=array('logs_logout_datetime'=>date('Y-m-d H:i:s'));
                $CI->model->logs_UPDATE($data);
                $CI->session->unset_userdata('sess_user_id');
                $CI->session->unset_userdata('sess_user_name');
                $CI->session->unset_userdata('sess_user_role');
                $CI->session->unset_userdata('most_recent_activity');
                $CI->session->set_flashdata('msg','Sorry! Session Expired');
                $CI->session->set_flashdata('msg_type','danger');
                redirect('welcome/login');
            }
            $CI->session->set_userdata('most_recent_activity', time());
        }

        if($is_login==true)
        {
            if($CI->session->userdata('sess_user_id')=='' || $CI->session->userdata('sess_user_role')=='' || $CI->session->userdata('sess_user_name')=='')
            {
                //$CI->session->set_flashdata('msg','Sorry! Session Expired');
                //$CI->session->set_flashdata('msg_type','danger');
                redirect('welcome/login');
            }
        }
    }

    //base64_url_encode
    public function base64_url_encode($input)
    {
        return strtr(base64_encode($input), '+/=', '-_~');
    }

    //base64_url_decode
    public function base64_url_decode($input)
    {
        return base64_decode(strtr($input, '-_~', '+/='));
    }

    // image_resize
    public function image_resize($src, $dst, $width, $height, $crop=0)
    {
        $width_for=$width;
        $height_for=$height;
        if(!list($w, $h) = getimagesize($src)) return "Unsupported picture type!";
        $type = strtolower(substr(strrchr($src,"."),1));
        if($type == 'jpeg') $type = 'jpg';
        switch($type)
        {
            case 'bmp': $img = imagecreatefromwbmp($src); break;
            case 'gif': $img = imagecreatefromgif($src); break;
            case 'jpg': $img = imagecreatefromjpeg($src); break;
            case 'png': $img = imagecreatefrompng($src); break;
            default : return "Unsupported picture type!";
        }

        // resize
        if($crop)
        {
            if($w < $width or $h < $height)
            {

                $img = imagecreatefromstring(file_get_contents($src));
                if (!$img)
                {
                    echo "ERROR:could not create image handle.";
                    exit(0);
                }
                $width = imageSX($img);
                $height = imageSY($img);
                $target_width = $width_for;
                $target_height = $height_for;
                $target_ratio = $target_width / $target_height;
                $img_ratio = $width / $height;
                if ($target_ratio > $img_ratio)
                {
                    $new_height = $target_height;
                    $new_width = $img_ratio * $target_height;
                }
                else
                {
                    $new_height = $target_width / $img_ratio;
                    $new_width = $target_width;
                }
                if ($new_height > $target_height)
                {
                    $new_height = $target_height;
                }
                if ($new_width > $target_width)
                {
                    $new_height = $target_width;
                }

                $new_img = ImageCreateTrueColor($width_for, $height_for);
                $white = imagecolorallocate($new_img, 255, 255, 255);

                if (!@imagefilledrectangle($new_img, 0, 0, $target_width-1, $target_height-1, $white))
                {
                    // Fill the image black
                    echo "ERROR:Could not fill new image";
                    exit(0);
                }
                if (!@imagecopyresampled($new_img, $img, ($target_width-$new_width)/2, ($target_height-$new_height)/2, 0, 0, $new_width, $new_height, $width, $height))
                {
                    echo "ERROR:Could not resize image";
                    exit(0);
                }

                switch($type)
                {
                    case 'bmp': imagewbmp($new_img, $dst); break;
                    case 'gif': imagegif($new_img, $dst); break;
                    case 'jpg': imagejpeg($new_img, $dst); break;
                    case 'png': imagepng($new_img, $dst); break;
                }
                return true;
            }
            else
            {
                $ratio = max($width/$w, $height/$h);
                $h = $height / $ratio;
                $x = ($w - $width / $ratio) / 2;
                $w = $width / $ratio;
            }
        }
        else
        {
            if($w < $width and $h < $height)
            {
                $img = imagecreatefromstring(file_get_contents($src));
                if (!$img)
                {
                    echo "ERROR:could not create image handle.";
                    exit(0);
                }
                $width = imageSX($img);
                $height = imageSY($img);
                $target_width = $width_for;
                $target_height = $height_for;
                $target_ratio = $target_width / $target_height;
                $img_ratio = $width / $height;
                if ($target_ratio > $img_ratio)
                {
                    $new_height = $target_height;
                    $new_width = $img_ratio * $target_height;
                }
                else
                {
                    $new_height = $target_width / $img_ratio;
                    $new_width = $target_width;
                }
                if ($new_height > $target_height)
                {
                    $new_height = $target_height;
                }
                if ($new_width > $target_width)
                {
                    $new_height = $target_width;
                }

                $new_img = ImageCreateTrueColor($width_for, $height_for);
                $white = imagecolorallocate($new_img, 255, 255, 255);

                if (!@imagefilledrectangle($new_img, 0, 0, $target_width-1, $target_height-1, $white))
                {
                    // Fill the image black
                    echo "ERROR:Could not fill new image";
                    exit(0);
                }
                if (!@imagecopyresampled($new_img, $img, ($target_width-$new_width)/2, ($target_height-$new_height)/2, 0, 0, $new_width, $new_height, $width, $height))
                {
                    echo "ERROR:Could not resize image";
                    exit(0);
                }

                switch($type)
                {
                    case 'bmp': imagewbmp($new_img, $dst); break;
                    case 'gif': imagegif($new_img, $dst); break;
                    case 'jpg': imagejpeg($new_img, $dst); break;
                    case 'png': imagepng($new_img, $dst); break;
                }
                return true;
            }
            else
            {
                $ratio = min($width/$w, $height/$h);
                $width = $w * $ratio;
                $height = $h * $ratio;
                $x = 0;
            }
        }

        $new = imagecreatetruecolor($width, $height);

        // preserve transparency
        if($type == "gif" or $type == "png")
        {
            imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
            imagealphablending($new, false);
            imagesavealpha($new, true);
        }

        imagecopyresampled($new, $img, 0, 0, $x, 0, $width, $height, $w, $h);

        switch($type)
        {
            case 'bmp': imagewbmp($new, $dst); break;
            case 'gif': imagegif($new, $dst); break;
            case 'jpg': imagejpeg($new, $dst); break;
            case 'png': imagepng($new, $dst); break;
        }
        return true;
    }

    //add http
    public function addhttp($url)
    {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url))
        {
            $url = "http://" . $url;
        }
        $url = parse_url($url);
        if(!isset($url['path']))
        {
            $url['path'] = '/';
        }
        $url = $url['scheme']."://".$url['host'].$url['path'];
        $url .= (substr($url, -1) == '/' ? '' : '/');
        return $url;
    }

    //adjustBrightness
    function adjustBrightness($hex, $steps)
    {
        // Steps should be between -255 and 255. Negative = darker, positive = lighter
        $steps = max(-255, min(255, $steps));

        // Normalize into a six character long hex string
        $hex = str_replace('#', '', $hex);
        if (strlen($hex) == 3) {
            $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
        }

        // Split into three parts: R, G and B
        $color_parts = str_split($hex, 2);
        $return = '#';

        foreach ($color_parts as $color) {
            $color   = hexdec($color); // Convert to decimal
            $color   = max(0,min(255,$color + $steps)); // Adjust color
            $return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
        }
        return $return;
    }
}
?>