<?php
/**
 * Created by PhpStorm.
 * User: user56
 * Date: 08-08-2018
 * Time: 15:05
 */

class AdminModel extends CI_Model{ 

    // update admin pass admin id and datas
    function updateAdmin($data=null,$id=null){

        $this->db->where(array('admin_id'=>$id));
        $this->db->update('admin',$data);
        return $this->db->affected_rows();
    }

    // get admin login photo at login time
    function getAdminloginPhoto($user){

        $this->db->where(array('admin_username'=>$user));
        $this->db->select('admin_photo');
        $res    =   $this->db->get('admin');
        return $res->row('admin_photo');
    }

    // retrieve admin credentials
    function retrieveAdmin($id=null){

        $select =   array('admin_id','admin_role','admin_username','admin_firstname','admin_lastname','admin_photo','admin_logdate');
        $this->db->select($select);
        $this->db->where(array('admin_id'=>$id));
        $res    =   $this->db->get('admin');

        if($res->num_rows()>0){

            $return['data'] = $res->result_array();
        }
        else{
            
            $return['error'] = "no data found on database";
        }
        return $return;
    }

    // update login date on ogin success
    function updateLogindate($id,$date){

        $this->db->where(array('admin_id'=>$id));
        $update = array('admin_logdate'=>$date);
        $this->db->update('admin',$update);
        return true;

    }


    // upload admin profile pic 
    public function uploadprofilepic($filename,$id)
    {
        $this->db->where(array("admin_id"=>$id));
        $this->db->update('admin',array('admin_photo'=>$filename));
        return $this->db->affected_rows();
    }



    

    // get admin photo 
    function getAdminphoto($id){

        $this->db->where(array("admin_id"=>$id));
        $res = $this->db->get('admin');
        return $res->row('admin_photo');

    }

    
    
   

    // update admin password pass id and password  
    function updateAdminpassword($update = null,$id = null,$oldpass = null){


        $where = array('admin_id'=>$id,'admin_password'=>$oldpass);       //check where the row exists or old password is correct
        $this->db->where($where);

        $res   = $this->db->get('admin');

        if($res->num_rows()>0){

            $this->db->update('admin',$update);

            if($this->db->affected_rows()>0){

            return 1;

            }
            else{

                return 0;
            }

        }
        else{

            return -1;

        }

    }
    
    //dashboard chart 
    function getChartcount(){
        
        $query = $this->db->query("SELECT COUNT(users_created_date) FROM users");
        return $query->num_rows();
    }

    //posts  count
    function getpostsCount(){
        $res = $this->db->get('posts');
        return $res->num_rows();
    }

    //users count
    function getusersCount(){
        $res = $this->db->get('users');
        return $res->num_rows();
    }

     //party count
     function getpartyCount(){
        $this->db->where('party_id !=', 1);
        $res = $this->db->get('party');
        return $res->num_rows();
    }

    //party count
    function getgroupCount(){
        $res = $this->db->get('group_table');
        return $res->num_rows();
    }

      
}