<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class BusinessProfileModel extends CI_Model{

    function fetch_from_table($table_name = null , $where = null){
        if(!empty($where)){
            $this->db->where($where);
        }
        return $this->db->get($table_name);
    }

    function insert_table($table_name = null , $insert = null){
        $this->db->insert($table_name , $insert);
        return $this->db->insert_id();
    }   

    function update_table($table_name = null , $where = null ,$data = null){
        
        $this->db->where($where);
        return $this->db->update($table_name , $data);
        
    } 


    function pollBusinessProfile($user_id = null, $posts_id = null,$user_posts_id = null){

        $insert               = array('bpp_bp_id' => $posts_id,'bpp_user_id' => $user_id , 'bpp_bp_user_id' => $user_posts_id);
        $update_notifications = array('notifications_type' => 31,'notifications_post_id' => $posts_id ,'notifications_user_id' => $user_id,'notifications_user_poll_id' => $user_posts_id);

        if($this->checkPolled($user_id,$posts_id) == 0){

            $this->db->insert('business_profile_polls',$insert);
            $this->db->insert('notifications',$update_notifications);
            $return['res'] = 1;
        }
        else{
            $this->db->where(array('bpp_bp_id' => $posts_id , 'bpp_user_id' => $user_id));
            $this->db->delete('business_profile_polls');
            $return['res'] = -1;
        }
        return $return;
    }

    //check already polled or not
    function checkPolled($user_id = null,$posts_id = null){

        $this->db->where(array('bpp_bp_id' => $posts_id , 'bpp_user_id' => $user_id));
        $res = $this->db->get('business_profile_polls');
        return $res->row('bpp_id');
    }

    // get business profile by polls
    function getProfilebyPolls($scope = null ,$location = null , $category = null , $subcategory1 = null , $subcategory2 = null , $subcategory3 = null){
      
        $this->db->select('business_profile.*,COUNT(bpp_bp_id) as total');
        
        if(!empty($category))
        $this->db->where('bf_category',$category);
    
        if(!empty($subcategory1))
        $this->db->where('bf_sub_category_1',$subcategory1);
    
        if(!empty($subcategory2))
        $this->db->where('bf_sub_category_2',$subcategory2);
    
        if(!empty($subcategory3))
        $this->db->where('bf_sub_category_3',$subcategory3);



        if($scope == 1)
        $this->db->where('users_country',$location);

        if($scope == 2)
        $this->db->where('users_state',$location);

        if($scope == 3)
        $this->db->where('users_city',$location);
        

        if($scope == 4)
        $this->db->where('users_area',$location);

        $this->db->where('bf_status',1);
        $this->db->group_by('bpp_bp_id');
        $this->db->order_by('total','desc');
        $this->db->join('business_profile','business_profile.bf_id = business_profile_polls.bpp_bp_id');
        $this->db->join('users','users.user_id = business_profile_polls.bpp_user_id');
        $res = $this->db->get('business_profile_polls');
        return $res->result_array();
    }

     // posts comments
     function commentPost($user_id = null , $post_id = null , $msg = null ,$user_posts_id = null){

        $insert = array('bp_posts_comments_post_id' => $post_id , 'bp_posts_comments_user_id' => $user_id , 'bp_posts_comments_comment' => $msg);
        $this->db->insert('business_profile_posts_comments' , $insert);
        
        $update_notifications = array('notifications_type' => 41,'notifications_post_id' => $post_id ,'notifications_user_id' => $user_id,'notifications_user_poll_id' => $user_posts_id);

        if($this->db->affected_rows() > 0){
            $this->db->insert('notifications',$update_notifications);
            $this->db->select(array('business_profile_posts_comments.*','users.users_name'));
            $this->db->limit(3);
            $this->db->order_by('bp_posts_comments_id',"desc");
            $this->db->from('business_profile_posts_comments');
            $this->db->where(array('bp_posts_comments_post_id'=>$post_id));
            $this->db->join('users','users.user_id=business_profile_posts_comments.bp_posts_comments_user_id');
            $res = $this->db->get();
            $return['data'] = $res->result_array(); 
           
        }
        else{

            return 0;
        }
        return $return;

    }

     // get reported posts by userid
     function gethiddenposts($userid = null){

        $this->db->select('DISTINCT(hide_posts_post_id)');
        $this->db->where('hide_posts_user_id',$userid);
        $this->db->where('hide_posts_type',4);
        $res = $this->db->get('hide_posts');
        return $res->result_array();
    }

    // get posts comments Count outer
    function commentsCountouter($postid = null){
       
        $res = $this->db->get_where('business_profile_posts_comments',array('bp_posts_comments_post_id' => $postid));
        return $res->num_rows();
        
    }

    // get reported posts by userid
    function getReportedposts($userid = null){

        $this->db->select('DISTINCT(posts_report_post_id)');
        $this->db->where('posts_report_user_id',$userid);
        $res = $this->db->get('business_profile_posts_report');
        return $res->result_array();
    }

     // get reported challenge by userid
     function getReportedchallenge($userid = null){

        $this->db->select('DISTINCT(challenge_report_challenge_id)');
        $this->db->where('challenge_report_user_id',$userid);
        $res = $this->db->get('business_profile_challenge_report');
        return $res->result_array();
    }

         // get post comments by id
         function getPostcommentsbyid($comment_id = null){

            $this->db->select(array('business_profile_posts_comments.*','users.users_name','users.users_photo'));
            $this->db->join('users','users.user_id=business_profile_posts_comments.bp_posts_comments_user_id');
            $res = $this->db->get_where('business_profile_posts_comments',array('bp_posts_comments_id' => $comment_id));
            return $res->num_rows() > 0 ? $res->result_array() : null;
    
        }

         // get challenge by id
    function getChallengebyid($challenegid = null){

        $this->db->select('business_profile_challenge.*,users.users_photo,users.users_name');
        $this->db->where('challenge_id',$challenegid);
        $this->db->join('users','users.user_id = business_profile_challenge.challenge_user_id');
        $res = $this->db->get('business_profile_challenge');
        return $res->result_array();
    }


     //poll posts
     function pollPosts($user_id = null, $posts_id = null,$user_posts_id = null){

        $insert               = array('bp_posts_polls_post_id' => $posts_id,'bp_posts_polls_user_id' => $user_id);
        $update_notifications = array('notifications_type' => 40,'notifications_post_id' => $posts_id ,'notifications_user_id' => $user_id,'notifications_user_poll_id' => $user_posts_id);

        if($this->checkpostPolled($user_id,$posts_id) == 0){

            $this->db->insert('business_profile_posts_polls',$insert);
            $this->db->insert('notifications',$update_notifications);
            $return['res'] = 1;
        }
        else{
            $this->db->where(array('bp_posts_polls_post_id' => $posts_id , 'bp_posts_polls_user_id' => $user_id));
            $this->db->delete('business_profile_posts_polls');
            $return['res'] = -1;
        }
       
        return $return;

    }

    //check already polled or not
    function checkpostPolled($user_id = null,$posts_id = null){

        $this->db->where(array('bp_posts_polls_post_id' => $posts_id , 'bp_posts_polls_user_id' => $user_id));
        $res = $this->db->get('business_profile_posts_polls');
        return $res->row('bp_posts_polls_id');
    }

    //check already polled or not
    function checkchallengepostPolled($user_id = null,$challenge_id = null,$posts_id = null){

        $this->db->where(array('bp_challenge_polls_challenge_post_id' => $posts_id ,'bp_challenge_polls_challenge_id' => $challenge_id, 'bp_challenge_post_polled_by' => $user_id));
        $res = $this->db->get('business_profile_challenge_post_polls');
        return $res->row('bp_challenge_post_polls_id');
    }

     // check challnge polled
    function isChallengepolled($challengeid = null , $userid = null){

        $this->db->where('bp_challenge_polls_challenge_id',$challengeid);
        $this->db->where('bp_challenge_polls_polled_by',$userid);
        return $this->db->get('business_profile_challenge_polls')->num_rows();
    }

     // get challenge winner
     function getWinner($cid = null){

        $this->db->select(array('users.users_name','users.users_photo','users.users_login_type','business_profile_challenge_posts.*','bp_challenge_polls_challenge_post_id','COUNT(bp_challenge_polls_challenge_post_id) as total'));
        $this->db->where('bp_challenge_polls_challenge_id',$cid);
        $this->db->group_by('bp_challenge_polls_challenge_post_id');
        $this->db->order_by('total','desc');
        $this->db->limit(1);
        $this->db->join('users','users.user_id = business_profile_challenge_post_polls.bp_challenge_post_polled_by');
        $this->db->join('business_profile_challenge_posts','business_profile_challenge_posts.bp_challenge_posts_id = business_profile_challenge_post_polls.bp_challenge_polls_challenge_post_id');
        $res = $this->db->get('business_profile_challenge_post_polls');
        return $res->result_array();
    }

     // getchallenge explanatory video
     function getExplanetoryvideo($id = null){

        $this->db->where('bp_challenge_posts_challenge_id',$id);
        $this->db->where('bp_challenge_posts_type',1);
        $res = $this->db->get('business_profile_challenge_posts');
        return $res->result_array();
    }

    // get challehnge poll count
    function challengePollcount($challengeid = null){

        $this->db->where('bp_challenge_polls_challenge_id',$challengeid);
        return $this->db->get('business_profile_challenge_polls')->num_rows();

    }


    // get challehnge comment count
    function challengeCommentcount($challengeid = null){

        $this->db->where('bp_challenge_comments_challenge_id',$challengeid);
        return $this->db->get('business_profile_challenge_comments')->num_rows();

    }

     // get challenge posts
     function getChallengeposts($id = null){
        $this->db->select(array('business_profile_challenge_posts.*','users.users_name','users.users_photo','users.user_id'));
        $this->db->where('bp_challenge_posts_challenge_id',$id);
        $this->db->where('bp_challenge_posts_type !=',1);
        $this->db->where('bp_challenge_posts_active',1);
        $this->db->join('users','users.user_id = business_profile_challenge_posts.bp_challenge_posts_user_id');
        $res = $this->db->get('business_profile_challenge_posts');
        return $res->result_array();
    }

      // get challenge comments
      function getChallengecomments($id = null){

        $this->db->select('business_profile_challenge_comments.*,users.users_name,users.users_photo');
        $this->db->where('bp_challenge_comments_challenge_id',$id);
        $this->db->order_by('bp_challenge_comments_id','desc');
        $this->db->join('users','users.user_id = business_profile_challenge_comments.bp_challenge_comments_user_id');
        $res = $this->db->get('business_profile_challenge_comments');
        return $res->result_array();
    }

       // get challenge posts count
       function totalPostsCount($cid = null){
        
        $this->db->where('bp_challenge_polls_challenge_id',$cid);
        $res = $this->db->get('business_profile_challenge_post_polls');
        return $res->num_rows();
    }

     // get challehnge poll count
     function challengepostPollcount($challengeid = null , $challengepostid = null){

        if(!empty($challenegid)){
            $this->db->where('bp_challenge_posts_challenge_id' , $challengeid);
        }
     
        $this->db->where('bp_challenge_polls_challenge_post_id' , $challengepostid);
        return $this->db->get('business_profile_challenge_post_polls')->num_rows();

    }


     // get challehnge comment count
     function challengepostCommentcount($challengeid = null){

        $this->db->where('bp_challenge_posts_comments_challenge_id',$challengeid);
        return $this->db->get('business_profile_challenge_posts_comments')->num_rows();

    }

        // delete challenge
        function deleteChallenge($challengeid = null){

            $this->db->where('challenge_id',$challengeid);
            $this->db->update('business_profile_challenge',array('challenge_active' => 0));
            return $this->db->affected_rows();
        }
    

    // invite members
    function inviteMembers($userid = null , $challengeid = null , $loggeduserid = null ,$type= null){

        $insert = array('challenge_invite_user_id' => $userid , 'challenge_invite_challenge_id' => $challengeid);
        $this->db->insert('business_profile_challenge_invite',$insert);

        if($this->db->affected_rows() > 0){

            $notification         = array('notifications_user_id' => $loggeduserid ,'notifications_user_poll_id' => $userid,'notifications_group_link'=>mt_rand(10000,99999),'notifications_challenge_id' => $challengeid,'notifications_type' => $type);  
            $insert_notifications = $this->db->insert('notifications',$notification);
            return 1;
        }
        else{
            return 0;
        }
    }

        // insert challenge photos and videos
        function uploadChallengeposts($insert = null){

            $this->db->insert('business_profile_challenge_posts',$insert);
            return $this->db->insert_id();
        }

            // get last iserted post challenge
    function getLastinsertedpost($id = null){

        $this->db->select('business_profile_challenge_posts.*,users.user_id,users.users_photo,users.users_name');
        $this->db->where('bp_challenge_posts_id' , $id);
        $this->db->join('users','users.user_id = business_profile_challenge_posts.bp_challenge_posts_user_id');
        $res = $this->db->get('business_profile_challenge_posts');
        return $res->result_array();
    }
    
         // delete challenge
         function deleteChallengepost($challengeid = null){
    
            $this->db->where('bp_challenge_posts_id',$challengeid);
            $this->db->update('business_profile_challenge_posts',array('bp_challenge_posts_active' => 0));
            return $this->db->affected_rows();
        }


     // check challnge polled
     function isChallengepostpolled($challengeid = null , $userid = null){

        $this->db->where('bp_challenge_polls_challenge_post_id',$challengeid);
        $this->db->where('bp_challenge_post_polled_by',$userid);
        return $this->db->get('business_profile_challenge_post_polls')->num_rows();
    }

      // report post
      function reportPost($user_id = null, $post_id = null , $msg = null){

        if($this->check_report_exist($user_id , $post_id) == 0){
        $insert = array('posts_report_post_id' => $post_id , 'posts_report_user_id' => $user_id , 'posts_report_comment' => $msg);
        $this->db->insert('business_profile_posts_report',$insert);
        return $this->db->affected_rows(); 
        }
        else{
            return -1;
        }   
    }

    // check report exists or not
    function check_report_exist($user_id = null, $post_id = null){

        $this->db->where(array('posts_report_post_id' => $post_id , 'posts_report_user_id' => $user_id));
        $res = $this->db->get('business_profile_posts_report');
        return $res->num_rows();
    } 
    
     // update post report count
     function updateReportcount($postid = null , $msg = null){

        if($msg == "Sexually explicite")
        $query = "UPDATE business_profile_posts SET bp_posts_report_count = bp_posts_report_count + 1 , bp_posts_remove_count = bp_posts_remove_count + 1  WHERE bp_posts_id = $postid";
        else
        $query = "UPDATE business_profile_posts SET bp_posts_report_count = bp_posts_report_count + 1  WHERE bp_posts_id = $postid";
        $this->db->query($query);

    }

        //reply to comment
        function replyPostscomments($insert = null , $comment_user_id = null){
            $update_notifications = array('notifications_type' => 46,'notifications_post_id' => $insert['bp_posts_comments_reply_posts_id'] ,'notifications_user_id' => $insert['bp_posts_comments_reply_user_id'],'notifications_user_poll_id' => $comment_user_id);
    
            $this->db->insert('business_profile_posts_comments_reply',$insert);
            $res = $this->db->affected_rows();
            if($res > 0){
    
                $this->db->insert('notifications',$update_notifications);
                return 1;
            }
            else{
                return 0;
            }
        }


   //remove post by report count
   function removePostbyCount($id = null){
    if($this->check_report_count($id) > 0){
     $this->db->where('bp_posts_id', $id);
     $this->db->update('business_profile_posts',array('bp_posts_active'=>0));
    }
    else{
        return false;
    }
}

 //check report count
 function check_report_count($id){
    $this->db->where('bp_posts_id', $id);
    $this->db->where('bp_posts_remove_count >=', 5);   
    $res = $this->db->get('business_profile_posts');
    return $res->num_rows();
}
function getPolledUsers($where = null){

    $this->db->select('users.user_id,users.users_name,users.users_photo,');
    $this->db->where($where);
    $this->db->join("users","users.user_id = business_profile_polls.bpp_user_id");
    $res = $this->db->get('business_profile_polls');
    return $res->result_array();
}

  // comment challenge
  function commentChallenge($userid = null , $cid = null , $comment = null , $cuserid = null){

    $insert = array('bp_challenge_comments_user_id' => $userid , 'bp_challenge_comments_comment' => $comment , 'bp_challenge_comments_challenge_id' => $cid);
    $this->db->insert('business_profile_challenge_comments',$insert);
   
    $res =  $this->db->insert_id();
    if($res > 0){

        $update_notifications = array('notifications_type' => 43,'notifications_post_id' => $cid ,'notifications_user_id' => $userid,'notifications_user_poll_id' => $cuserid);
        $this->db->insert('notifications',$update_notifications);
    }

    return $res;
}

//comment challenge posts
function commentChallengePosts($userid = null , $cid = null , $comment = null , $cuserid = null , $cpid = null){

    $insert = array('bp_challenge_posts_comments_user_id' => $userid , 'bp_challenge_posts_comments_comment' => $comment , 'bp_challenge_posts_comments_challenge_id' => $cpid , 'bp_challenge_posts_comments_challenge_userid' => $cuserid);
    $this->db->insert('business_profile_challenge_posts_comments',$insert);
   
    $res =  $this->db->insert_id();
    if($res > 0){

        $update_notifications = array('notifications_type' => 45,'notifications_post_id' => $cpid,'notifications_challenge_id' => $cid ,'notifications_user_id' => $userid,'notifications_user_poll_id' => $cuserid);
        $this->db->insert('notifications',$update_notifications);
    }

    return $res;

}

// get last comment
function getLastcomment($cid = null){

    $this->db->select('business_profile_challenge_comments.*,users.users_name,users.users_photo');
    $this->db->where('bp_challenge_comments_id',$cid);
    $this->db->join('users','users.user_id = business_profile_challenge_comments.bp_challenge_comments_user_id');
    return $this->db->get('business_profile_challenge_comments')->result_array();
}

 // get last comment
 function getpostLastcomment($cid = null){

    $this->db->select('business_profile_challenge_posts_comments.*,users.users_name,users.users_photo');
    $this->db->where('bp_challenge_posts_comments_id',$cid);
    $this->db->join('users','users.user_id = business_profile_challenge_posts_comments.bp_challenge_posts_comments_user_id');
    return $this->db->get('business_profile_challenge_posts_comments')->result_array();
}



// get challenge comments
function getChallengecommentss($id = null){

    $this->db->select('challenge_comments.*,users.users_name,users.users_photo');
    $this->db->where('challenge_comments_challenge_id',$id);
    $this->db->order_by('challenge_comments_id','desc');
    $this->db->join('users','users.user_id = challenge_comments.challenge_comments_user_id');
    $res = $this->db->get('challenge_comments');
    return $res->result_array();
}


 // get challenge comments
 function getChallengepostscomments($id = null){

    $this->db->select('challenge_posts_comments.*,users.users_name,users.users_photo');
    $this->db->where('challenge_posts_comments_challenge_id',$id);
    $this->db->order_by('challenge_posts_comments_id','desc');
    $this->db->join('users','users.user_id = challenge_posts_comments.challenge_posts_comments_user_id');
    $res = $this->db->get('challenge_posts_comments');
    return $res->result_array();
}

 //reply to comment
 function replyChallengecomments($insert = null , $comment_user_id = null){
    $update_notifications = array('notifications_type' => 47,'notifications_post_id' => $insert['challenge_comments_reply_challenge_id'] ,'notifications_user_id' => $insert['challenge_comment_reply_comment_user_id'],'notifications_user_poll_id' => $comment_user_id);

    $this->db->insert('business_profile_challenge_comments_reply',$insert);
    $res = $this->db->affected_rows();
    if($res > 0){

        $this->db->insert('notifications',$update_notifications);
        return 1;
    }
    else{
        return 0;
    }
}

 //reply to comment
 function replyChallengecommentsPosts($insert = null , $comment_user_id = null){
    $update_notifications = array('notifications_type' => 48,'notifications_post_id' => $insert['challenge_posts_comments_reply_challenge_id'] ,'notifications_user_id' => $insert['challenge_posts_comment_reply_comment_user_id'],'notifications_user_poll_id' => $comment_user_id);

    $this->db->insert('business_profile_challenge_posts_comments_reply',$insert);
    $res = $this->db->affected_rows();
    if($res > 0){
        $this->db->insert('notifications',$update_notifications);
        return 1;
    }
    else{
        return 0;
    }
}

 // poll challenge 
 function pollChallenge($userid , $cid , $cuserid){

    if($this->checkchallengePolled($userid , $cid)== 0){
        
        $this->db->insert('business_profile_challenge_polls' , array('bp_challenge_polls_polled_by' => $userid , 'bp_challenge_polls_challenge_id' => $cid));
        if ($this->db->affected_rows() > 0){
            $update_notifications = array('notifications_type' => 42,'notifications_post_id' => $cid ,'notifications_user_id' => $userid,'notifications_user_poll_id' => $cuserid);
            $this->db->insert('notifications',$update_notifications);
            return 2;
        }
    }
    else{

        $this->db->where(array('bp_challenge_polls_polled_by' => $userid , 'bp_challenge_polls_challenge_id' => $cid));
        $this->db->delete('business_profile_challenge_polls');
        if ($this->db->affected_rows() > 0){
            return 3;
        }

    }
}

 // delete posts by id
 function deletePosts($postid = null){

    $this->db->where('bp_posts_id',$postid);
    $update = array('bp_posts_active'=>0);
    $this->db->update('business_profile_posts',$update);
    return $this->db->affected_rows();

}


    // update post report count
    function updateChallengeReportcount($challengeid = null){

        $query = "UPDATE business_profile_challenge SET challenge_report_count = challenge_report_count + 1  WHERE challenge_id = $challengeid";
        $this->db->query($query);


    }

    // edit posts by id
    function editPosts($postid = null, $update = null){

        $this->db->where('bp_posts_id',$postid);
        $this->db->update('business_profile_posts',$update);
        return $this->db->affected_rows();

    }

function getBusinessProfileRank(){

    $this->db->select('COUNT(bpp_bp_id) as total,bpp_bp_id');
    $this->db->group_by('bpp_bp_id');
    $this->db->order_by('total','desc');
    return $this->db->get('business_profile_polls')->result_array();
}

   // function delete notifications after block or delete posts
   function deletenotifications($postid = null){

    $this->db->where('notifications_post_id',$postid);
    $this->db->delete('notifications');
}

// check user polled or  not
function checkchallengePolled($userid , $cid){

    $this->db->where(array('bp_challenge_polls_polled_by' => $userid , 'bp_challenge_polls_challenge_id' => $cid));
    return $this->db->get('business_profile_challenge_polls')->num_rows();
}


 // poll post challenge  (type 2)
 function pollpostChallenge($userid , $cid ,$cpid , $cuserid){

    if($this->checkchallengepostPolled($userid , $cid ,$cpid)== 0){
        
        $this->db->insert('business_profile_challenge_post_polls' , array('bp_challenge_post_polled_by' => $userid ,'bp_challenge_polls_challenge_id' => $cid,'bp_challenge_polls_challenge_post_id' => $cpid));
        if ($this->db->affected_rows() > 0){
            $update_notifications = array('notifications_type' => 44,'notifications_post_id' => $cid ,'notifications_user_id' => $userid,'notifications_user_poll_id' => $cuserid);
            $this->db->insert('notifications',$update_notifications);
            return 2;
        }
    }
    else{

        $this->db->where(array('bp_challenge_post_polled_by' => $userid ,'bp_challenge_polls_challenge_id' => $cid,'bp_challenge_polls_challenge_post_id' => $cpid));
        $this->db->delete('business_profile_challenge_post_polls');
        if ($this->db->affected_rows() > 0){
            $this->db->where(array('notifications_type' => 44,'notifications_post_id' => $cid ,'notifications_user_id' => $userid,'notifications_user_poll_id' => $cuserid));
            $this->db->delete('notifications');
            return 3;
        }

    }
}

// update poll post challenge
function updatepollpostChallenge($userid , $cid ,$cpid){

    $this->db->where(array('bp_challenge_post_polled_by' => $userid ,'bp_challenge_polls_challenge_id' => $cid));
    $this->db->update('business_profile_challenge_post_polls',array('bp_challenge_polls_challenge_post_id' => $cpid));  
    return $this->db->affected_rows() > 0 ? 4 : 5; 
}

  // get post comments
  function getPostcomments($post_id = null){

    $this->db->select(array('business_profile_posts_comments.*','users.users_name','users.users_photo'));
    $this->db->join('users','users.user_id=business_profile_posts_comments.bp_posts_comments_user_id');
    $res = $this->db->get_where('business_profile_posts_comments',array('bp_posts_comments_post_id' => $post_id));
    return $res->num_rows() > 0 ? $res->result_array() : null;

}

  // get comments reply via comment id
  function getcommentsreply($id = null , $post_id = null){

    $this->db->from('business_profile_posts_comments_reply');
    $this->db->order_by('bp_posts_comments_reply_id',"desc");
    $this->db->select(array('business_profile_posts_comments_reply.*','users.users_name','users.users_photo'));
    $this->db->where(array('bp_posts_comments_reply_comment_id' => $id , 'bp_posts_comments_reply_posts_id' => $post_id));
    $this->db->join('users','users.user_id=business_profile_posts_comments_reply.bp_posts_comments_reply_user_id');
    
    $res    = $this->db->get();
    return $res->result_array();
}

// get users name by id
function getusersnamebyid($id = null){

    $this->db->select('users_name');
    $this->db->where('user_id',$id);
    $res = $this->db->get('users');
    return $res->row('users_name');
}

// get challnege comment list
function getCommentlist($cid = null , $type = null){
        
    if($type == 0){
    $this->db->select('business_profile_challenge_comments.*,users.users_name,users.users_photo');
    $this->db->where('bp_challenge_comments_challenge_id',$cid);
    $this->db->join('users','users.user_id = business_profile_challenge_comments.bp_challenge_comments_user_id');
    return $this->db->get('business_profile_challenge_comments')->result_array();
    }
    else{

    $this->db->select('business_profile_challenge_posts_comments.*,users.users_name,users.users_photo');
    $this->db->where('bp_challenge_posts_comments_challenge_id',$cid);
    $this->db->join('users','users.user_id = business_profile_challenge_posts_comments.bp_challenge_posts_comments_user_id');
    return $this->db->get('business_profile_challenge_posts_comments')->result_array();
    }
    
}

function getBusinessProfilebylist($category , $subcategory1 , $subcategory2 , $subcategory3){

  

    if(!empty($category))
    $this->db->where('bf_category',$category);

    if(!empty($subcategory1))
    $this->db->where('bf_sub_category_1',$subcategory1);

    if(!empty($subcategory2))
    $this->db->where('bf_sub_category_2',$subcategory2);

    if(!empty($subcategory3))
    $this->db->where('bf_sub_category_3',$subcategory3);

    $this->db->where('bf_status',1);
    return $this->db->get('business_profile')->result_array();
    
}

    // hide posts
    function hidePosts($postid = null, $userid = null){

        $insert= array('hide_posts_post_id' => $postid , 'hide_posts_user_id' =>$userid , 'hide_posts_type' => 4);
        $this->db->insert('hide_posts',$insert);
        return $this->db->affected_rows();
    }


    // report challenge
    function reportChallenge($user_id = null, $challengeid = null , $msg = null){

        if($this->check_report_exist($user_id , $challengeid) == 0){
        $insert = array('challenge_report_challenge_id' => $challengeid , 'challenge_report_user_id' => $user_id , 'challenge_report_comment' => $msg);
        $this->db->insert('business_profile_challenge_report',$insert);
        return $this->db->affected_rows(); 
        }
        else{
            return -1;
        }   
    }

      // get user poll count
      function getPollCount($userid = null){

        $this->db->where('users_followers_users_id',$userid);
        
        return $this->db->get('users_followers')->num_rows();
    }   

     // get group invite members
     function getBusinessProfileInvitemembers($userid = null , $followerid = null){

        $this->db->select(array('DISTINCT(users.user_id)','users.users_name','users.users_photo'));
        
       if(!empty($userid)){
        $this->db->where('users_followers_users_id',$userid);
        $this->db->or_where('users_followers_follower_id',$userid);
        $this->db->join('users','users.user_id = users_followers.users_followers_users_id');
       }
       if(!empty($followerid)){
        $this->db->where('users_followers_users_id',$followerid);
        $this->db->or_where('users_followers_follower_id',$followerid);
        $this->db->join('users','users.user_id = users_followers.users_followers_follower_id');
       }
        
        $res = $this->db->get('users_followers');
        return $res->result_array();
    }

    function FollowedMembers($profileid = null){
      $this->db->select(array('DISTINCT(users.user_id)','users.users_name','users.users_photo'));
      $this->db->join('users','users.user_id = business_profile_polls.bpp_user_id');
      return  $this->db->get_where('business_profile_polls',array('bpp_bp_id' => $profileid))->result_array();

    }

    // get posts by id
    function getnotificationPostsbyid($posts_id = null){

        $this->db->select(array("business_profile_posts.*","users.users_name","users.users_photo"));
        $this->db->where(array('bp_posts_id' => $posts_id,'bp_posts_active' => 1));
        $this->db->order_by("bp_posts_id","DESC");
        $this->db->from("business_profile_posts");
        $this->db->join("users","users.user_id = business_profile_posts.bp_posts_user_id");
        $res = $this->db->get();
       
        return $res->num_rows() > 0 ? $res->result_array() : 0;
        }


        //get posts poll count
    function pollsCount($postid = null){

        $res = $this->db->get_where('business_profile_posts_polls',array('bp_posts_polls_post_id' => $postid));  
        $return['count'] = $res->num_rows();
        return $return;
    }

    // function get post polled or not
    function isPostpolled($postid = null ,$userid = null){

        $this->db->where(array('bp_posts_polls_post_id' => $postid , 'bp_posts_polls_user_id' => $userid));
        return $this->db->get('business_profile_posts_polls')->num_rows();
    }
    

    // get posts comments Count
    function commentsCount($postid = null){

        $this->db->select(array('business_profile_posts_comments.*','users.users_name','users_photo'));
        $this->db->order_by('bp_posts_comments_id',"desc");
        $this->db->from('business_profile_posts_comments');
        $this->db->limit(3);
        $this->db->where(array('bp_posts_comments_post_id' => $postid));
        $this->db->join("users","users.user_id = business_profile_posts_comments.bp_posts_comments_user_id");
        $res             = $this->db->get();
        $return['count'] = $res->num_rows();
        $return['data']  = $res->result_array();
        return $return;
    }

    
}