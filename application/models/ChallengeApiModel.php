<?php

class ChallengeApiModel extends CI_Model{

    function createChallenge($insert = null ){

        $this->db->insert('challenge',$insert);
        return $this->db->insert_id();

    }

    // insert challenge photos and videos
    function uploadChallengeposts($insert = null){

        $this->db->insert('challenge_posts',$insert);
        return $this->db->insert_id();
    }

    // get last iserted post challenge
    function getLastinsertedpost($id = null){

        $this->db->select('challenge_posts.*,users.user_id,users.users_photo,users.users_name');
        $this->db->where('challenge_posts_id' , $id);
        $this->db->join('users','users.user_id = challenge_posts.challenge_posts_user_id');
        $res = $this->db->get('challenge_posts');
        return $res->result_array();
    }

    // get challenge list
    function getChallengelist($country = null, $state = null, $district = null ,$area = null){

        $this->db->select(array('challenge.*','users.users_name',
                                'users.users_photo','users.users_login_type',
                                'users.users_country','users.users_state',
                                'users.users_city','users.users_area'
                                ));
        $this->db->where('challenge_active',1);
        $this->db->join('users','users.user_id = challenge.challenge_user_id');
        
        if(!empty($country)) $this->db->where('users_country',$country);
        if(!empty($state)) $this->db->where('users_state',$state);
        if(!empty($district)) $this->db->where('users_city',$district);
        if(!empty($area)) $this->db->where('users_area',$area);
        
        
        $this->db->order_by('challenge_id','desc');
        $res = $this->db->get('challenge');
        return $res->result_array();
    }



    // get challenge list
    function getChallengelistprofileposts($userid = null , $country = null, $state = null, $district = null){

        $this->db->select(array('challenge.*','users.users_name',
                                'users.users_photo','users.users_login_type',
                                'users.users_country','users.users_state',
                                'users.users_city'
                                ));
        $this->db->where('challenge_active',1);
        $this->db->where('challenge_active',1);
        $this->db->where('challenge_user_id',$userid);
        $this->db->join('users','users.user_id = challenge.challenge_user_id');
        
        if(!empty($country)) $this->db->where('users_country',$country);
        if(!empty($state)) $this->db->where('users_state',$state);
        if(!empty($district)) $this->db->where('users_city',$district);
        
        
        $this->db->order_by('challenge_id','desc');
        $res = $this->db->get('challenge');
        return $res->result_array();
    }

    // get challenge type
    function getchallengetype($id = null){

        $this->db->select('challenge_type');
        $this->db->where('challenge_id',$id);
        $res = $this->db->get('challenge');
        return $res->row('challenge_type');
    }



    // get challenge posts
    function getChallengeposts($id = null){
        $this->db->select(array('challenge_posts.*','users.users_name','users.users_photo','users.user_id'));
        $this->db->where('challenge_posts_challenge_id',$id);
        $this->db->where('challenge_posts_type !=',1);
        $this->db->where('challenge_posts_active',1);
        $this->db->where('status',1);
        $this->db->join('users','users.user_id = challenge_posts.challenge_posts_user_id');
        $res = $this->db->get('challenge_posts');
        return $res->result_array();
    }

    // comment challenge
    function commentChallenge($userid = null , $cid = null , $comment = null , $cuserid = null){

        $insert = array('challenge_comments_user_id' => $userid , 'challenge_comments_comment' => $comment , 'challenge_comments_challenge_id' => $cid);
        $this->db->insert('challenge_comments',$insert);
       
        $res =  $this->db->insert_id();
        if($res > 0){

            $update_notifications = array('notifications_type' => 22,'notifications_post_id' => $cid ,'notifications_user_id' => $userid,'notifications_user_poll_id' => $cuserid);
            $this->db->insert('notifications',$update_notifications);
        }

        return $res;
    }

    //comment challenge posts
    function commentChallengePosts($userid = null , $cid = null , $comment = null , $cuserid = null , $cpid = null){

        $insert = array('challenge_posts_comments_user_id' => $userid , 'challenge_posts_comments_comment' => $comment , 'challenge_posts_comments_challenge_id' => $cpid , 'challenge_posts_comments_challenge_userid' => $cuserid);
        $this->db->insert('challenge_posts_comments',$insert);
       
        $res =  $this->db->insert_id();
        if($res > 0){

            $update_notifications = array('notifications_type' => 24,'notifications_post_id' => $cid ,'notifications_user_id' => $userid,'notifications_user_poll_id' => $cuserid);
            $this->db->insert('notifications',$update_notifications);
        }

        return $res;

    }

    // get last comment
    function getLastcomment($cid = null){

        $this->db->select('challenge_comments.*,users.users_name,users.users_photo');
        $this->db->where('challenge_comments_id',$cid);
        $this->db->join('users','users.user_id = challenge_comments.challenge_comments_user_id');
        return $this->db->get('challenge_comments')->result_array();
    }

     // get last comment
     function getpostLastcomment($cid = null){

        $this->db->select('challenge_posts_comments.*,users.users_name,users.users_photo');
        $this->db->where('challenge_posts_comments_id',$cid);
        $this->db->join('users','users.user_id = challenge_posts_comments.challenge_posts_comments_user_id');
        return $this->db->get('challenge_posts_comments')->result_array();
    }

    

    // get challenge comments
    function getChallengecomments($id = null){

        $this->db->select('challenge_comments.*,users.users_name,users.users_photo');
        $this->db->where('challenge_comments_challenge_id',$id);
        $this->db->where('challenge_comments_active', 1);
        $this->db->order_by('challenge_comments_id','desc');
        $this->db->join('users','users.user_id = challenge_comments.challenge_comments_user_id');
        $res = $this->db->get('challenge_comments');
        return $res->result_array();
    }


     // get challenge comments
     function getChallengepostscomments($id = null){

        $this->db->select('challenge_posts_comments.*,users.users_name,users.users_photo');
        $this->db->where('challenge_posts_comments_challenge_id',$id);
        $this->db->where('challenge_posts_comments_challenge_active', 1);
        $this->db->order_by('challenge_posts_comments_id','desc');
        $this->db->join('users','users.user_id = challenge_posts_comments.challenge_posts_comments_user_id');
        $res = $this->db->get('challenge_posts_comments');
        return $res->result_array();
    }

    // get challehnge poll count
    function challengePollcount($challengeid = null){

        $this->db->where('challenge_polls_challenge_id',$challengeid);
        return $this->db->get('challenge_polls')->num_rows();

    }

     // get challehnge poll count
     function challengepostPollcount($challengeid = null , $challengepostid = null){

        if(!empty($challenegid)){
            $this->db->where('challenge_posts_challenge_id' , $challengeid);
        }
     
        $this->db->where('challenge_polls_challenge_post_id' , $challengepostid);
        return $this->db->get('challenge_post_polls')->num_rows();

    }

     // get challehnge comment count
     function challengeCommentcount($challengeid = null){

        $this->db->where('challenge_comments_challenge_id',$challengeid);
        $this->db->where('challenge_comments_active', 1);
        return $this->db->get('challenge_comments')->num_rows();

    }

     // get challehnge comment count
     function challengepostCommentcount($challengeid = null){

        $this->db->where('challenge_posts_comments_challenge_id',$challengeid);
        $this->db->where('challenge_posts_comments_challenge_active', 1);
        return $this->db->get('challenge_posts_comments')->num_rows();

    }



    // poll challenge 
    function pollChallenge($userid , $cid , $cuserid){

        if($this->checkPolled($userid , $cid)== 0){
            
            $this->db->insert('challenge_polls' , array('challenge_polls_polled_by' => $userid , 'challenge_polls_challenge_id' => $cid));
            if ($this->db->affected_rows() > 0){
                $update_notifications = array('notifications_type' => 21,'notifications_post_id' => $cid ,'notifications_user_id' => $userid,'notifications_user_poll_id' => $cuserid);
                $this->db->insert('notifications',$update_notifications);
                return 2;
            }
        }
        else{

            $this->db->where(array('challenge_polls_polled_by' => $userid , 'challenge_polls_challenge_id' => $cid));
            $this->db->delete('challenge_polls');
            if ($this->db->affected_rows() > 0){
                return 3;
            }

        }
    }

    // check user polled or  not
    function checkPolled($userid , $cid){

        $this->db->where(array('challenge_polls_polled_by' => $userid , 'challenge_polls_challenge_id' => $cid));
        return $this->db->get('challenge_polls')->num_rows();
    }


     // poll post challenge  (type 2)
     function pollpostChallenge($userid , $cid ,$cpid , $cuserid){

        if($this->checkpostPolled($userid , $cid ,$cpid)== 0){
            
            $this->db->insert('challenge_post_polls' , array('challenge_post_polled_by' => $userid ,'challenge_polls_challenge_id' => $cid,'challenge_polls_challenge_post_id' => $cpid));
            if ($this->db->affected_rows() > 0){
                $update_notifications = array('notifications_type' => 23,'notifications_post_id' => $cid ,'notifications_user_id' => $userid,'notifications_user_poll_id' => $cuserid);
                $this->db->insert('notifications',$update_notifications);
                return 2;
            }
        }
        else{

            $this->db->where(array('challenge_post_polled_by' => $userid ,'challenge_polls_challenge_id' => $cid,'challenge_polls_challenge_post_id' => $cpid));
            $this->db->delete('challenge_post_polls');
            if ($this->db->affected_rows() > 0){
                $this->db->where(array('notifications_type' => 23,'notifications_post_id' => $cid ,'notifications_user_id' => $userid,'notifications_user_poll_id' => $cuserid));
                $this->db->delete('notifications');
                return 3;
            }

        }
    }

    // update poll post challenge
    function updatepollpostChallenge($userid , $cid ,$cpid){

        $this->db->where(array('challenge_post_polled_by' => $userid ,'challenge_polls_challenge_id' => $cid));
        $this->db->update('challenge_post_polls',array('challenge_polls_challenge_post_id' => $cpid));  
        return $this->db->affected_rows() > 0 ? 4 : 5; 
    }

    // check user polled or  not
    function checkpostPolled($userid , $cid ,$cpid){

        $this->db->where(array('challenge_post_polled_by' => $userid ,'challenge_polls_challenge_post_id' => $cpid, 'challenge_polls_challenge_id' => $cid));
        return $this->db->get('challenge_post_polls')->num_rows();
    }

    // get challnege comment list
    function getCommentlist($cid = null , $type = null){
        
        if($type == 0){
        $this->db->select('challenge_comments.*,users.users_name,users.users_photo');
        $this->db->where('challenge_comments_challenge_id',$cid);
        $this->db->where('challenge_comments_active', 1);
        $this->db->join('users','users.user_id = challenge_comments.challenge_comments_user_id');
        return $this->db->get('challenge_comments')->result_array();
        }
        else{

        $this->db->select('challenge_posts_comments.*,users.users_name,users.users_photo');
        $this->db->where('challenge_posts_comments_challenge_id',$cid);
        $this->db->where('challenge_posts_comments_challenge_active', 1);
        $this->db->join('users','users.user_id = challenge_posts_comments.challenge_posts_comments_user_id');
        return $this->db->get('challenge_posts_comments')->result_array();
        }
        
    }

    // get challenge posts count
    function totalPostsCount($cid = null){
        
        $this->db->where('challenge_polls_challenge_id',$cid);
        $res = $this->db->get('challenge_post_polls');
        return $res->num_rows();
    }

    // getchallenge explanatory video
    function getExplanetoryvideo($id = null){

        $this->db->where('challenge_posts_challenge_id',$id);
        $this->db->where('challenge_posts_type',1);
        $res = $this->db->get('challenge_posts');
        return $res->result_array();
    }



    // invite members
    function inviteMembers($userid = null , $challengeid = null , $loggeduserid = null ,$type = null){

        $insert = array('challenge_invite_user_id' => $userid , 'challenge_invite_challenge_id' => $challengeid);
        $this->db->insert('challenge_invite',$insert);

        if($this->db->affected_rows() > 0){

            $notification         = array('notifications_user_id' => $loggeduserid ,'notifications_user_poll_id' => $userid,'notifications_group_link'=>mt_rand(10000,99999),'notifications_challenge_id' => $challengeid,'notifications_type' => $type);  
            $insert_notifications = $this->db->insert('notifications',$notification);
            return 1;
        }
        else{
            return 0;
        }
    }


    // check user blocked ot not
    function check_isBlocked($where = null){
        $this->db->select('users_active');
        $this->db->where($where);
        return $this->db->get('users')->num_rows();
    }


    // get challenge by id
    function getChallengebyid($challenegid = null){

        $this->db->select('challenge.*,users.users_photo,users.users_name');
        $this->db->where('challenge_id',$challenegid);
        $this->db->join('users','users.user_id = challenge.challenge_user_id');
        $res = $this->db->get('challenge');
        return $res->result_array();
    }

    // check challnge polled
    function isChallengepolled($challengeid = null , $userid = null){

        $this->db->where('challenge_polls_challenge_id',$challengeid);
        $this->db->where('challenge_polls_polled_by',$userid);
        return $this->db->get('challenge_polls')->num_rows();
    }

     // check challnge polled
     function isChallengepostpolled($challengeid = null , $userid = null){

        $this->db->where('challenge_polls_challenge_post_id',$challengeid);
        $this->db->where('challenge_post_polled_by',$userid);
        return $this->db->get('challenge_post_polls')->num_rows();
    }

    // get challenge winner
    function getWinner($cid = null){

        $this->db->select(array('users.users_name','users.users_photo','users.users_login_type','challenge_posts.*','challenge_polls_challenge_post_id','COUNT(challenge_polls_challenge_post_id) as total'));
        $this->db->where('challenge_polls_challenge_id',$cid);
        $this->db->group_by('challenge_polls_challenge_post_id');
        $this->db->order_by('total','desc');
        $this->db->limit(1);
        $this->db->join('challenge_posts','challenge_posts.challenge_posts_id = challenge_post_polls.challenge_polls_challenge_post_id');
        $this->db->join('users','users.user_id = challenge_posts.challenge_posts_user_id');
        $res = $this->db->get('challenge_post_polls');
        return $res->result_array();
    }

    // get winner name by id

    function getWinnername($id = null){
        $this->db->select('users_name');
        $this->db->where('user_id',$id);
        $res = $this->db->get('users');
        return $res->row('users_name');
    }

      //reply to comment
      function replyChallengecomments($insert = null , $comment_user_id = null){
        $update_notifications = array('notifications_type' => 25,'notifications_post_id' => $insert['challenge_comments_reply_challenge_id'] ,'notifications_user_id' => $insert['challenge_comment_reply_comment_user_id'],'notifications_user_poll_id' => $comment_user_id);

        $this->db->insert('challenge_comments_reply',$insert);
        $res = $this->db->affected_rows();
        if($res > 0){

            $this->db->insert('notifications',$update_notifications);
            return 1;
        }
        else{
            return 0;
        }
    }

      //reply to comment
      function replyChallengecommentsPosts($insert = null , $comment_user_id = null){
        $update_notifications = array('notifications_type' => 26,'notifications_post_id' => $insert['challenge_posts_comments_reply_challenge_id'] ,'notifications_user_id' => $insert['challenge_posts_comment_reply_comment_user_id'],'notifications_user_poll_id' => $comment_user_id);

        $this->db->insert('challenge_posts_comments_reply',$insert);
        $res = $this->db->affected_rows();
        if($res > 0){
            $this->db->insert('notifications',$update_notifications);
            return 1;
        }
        else{
            return 0;
        }
    }
    

    // get posts by id
    function getnotificationpartyPostsbyid($posts_id = null){

        $this->db->select(array("party_posts.*","users.users_name","users.users_photo"));
        $this->db->where(array('party_posts_id' => $posts_id,'party_posts_active' => 1));
        $this->db->order_by("party_posts_id","DESC");
        $this->db->from("party_posts");
        $this->db->join("users","users.user_id = party_posts.party_posts_posted_by");
        $res = $this->db->get();
       
        return $res->num_rows() > 0 ? $res->result_array() : 0;
        }

         //get posts poll count
    function pollsCount($postid = null){

        $res = $this->db->get_where('posts_polls',array('posts_polls_post_id' => $postid));  
        $return['count'] = $res->num_rows();
        return $return;
    }

      //is party post polled
      function ispostpolled($postid = null , $userid = null){

        $this->db->where('party_posts_polls_post_id',$postid);
        $this->db->where('party_posts_polls_user_id',$userid);
        $res = $this->db->get('party_posts_polls');
        return $res->num_rows();
     }

      // get posts comments Count
      function commentsCount($postid = null){

        $this->db->select(array('posts_comments.*','users.users_name'));
        $this->db->order_by('posts_comments_id',"desc");
        $this->db->from('posts_comments');
        $this->db->limit(3);
        $this->db->where(array('posts_comments_post_id' => $postid , 'posts_comments_active' => 1));
        $this->db->join("users","users.user_id = posts_comments.posts_comments_user_id");
        $res             = $this->db->get();
        $return['count'] = $res->num_rows();
        $return['data']  = $res->result_array();
        return $return;
    }


     // get posts comments Count outer
     function commentsCountouter($postid = null){
       
        $res = $this->db->get_where('posts_comments',array('posts_comments_post_id' => $postid , 'posts_comments_active' => 1));
        return $res->num_rows();
        
    }


     // get challenge by id
     function getnotificationChallengebyid($challengeid = null){

        $this->db->select(array('challenge_posts.*','users.users_name',
                                'users.users_photo','users.users_login_type',
                                'users.users_country','users.users_state',
                                'users.users_city'
                                ));
        $this->db->join('users','users.user_id = challenge_posts.challenge_posts_user_id');
        
      
        
        
        $this->db->where('challenge_posts_id',$challengeid);
        $res = $this->db->get('challenge_posts');
        
        return $res->result_array();
    }

    // delete challenge
    function deleteChallenge($challengeid = null){

        $this->db->where('challenge_id',$challengeid);
        $this->db->update('challenge',array('challenge_active' => 0));
        return $this->db->affected_rows();
    }


     // delete challenge
     function deleteChallengepost($challengeid = null){

        $this->db->where('challenge_posts_id',$challengeid);
        $this->db->update('challenge_posts',array('challenge_posts_active' => 0));
        return $this->db->affected_rows();
    }



     // report challenge
     function reportChallenge($user_id = null, $challengeid = null , $msg = null){

        if($this->check_report_exist($user_id , $challengeid) == 0){
        $insert = array('challenge_report_challenge_id' => $challengeid , 'challenge_report_user_id' => $user_id , 'challenge_report_comment' => $msg);
        $this->db->insert('challenge_report',$insert);
        return $this->db->affected_rows(); 
        }
        else{
            return -1;
        }   
    }

     // check report exists or not
     function check_report_exist($user_id = null, $challengeid = null){

        $this->db->where(array('challenge_report_challenge_id' => $challengeid , 'challenge_report_user_id' => $user_id));
        $res = $this->db->get('challenge_report');
        return $res->num_rows();
    }   



     // report challenge
     function reportChallengepost($user_id = null, $challengeid = null , $msg = null){

        if($this->check_report_exist($user_id , $challengeid) == 0){
        $insert = array('challenge_report_challenge_id' => $challengeid , 'challenge_report_user_id' => $user_id , 'challenge_report_comment' => $msg);
        $this->db->insert('challenge_report',$insert);
        return $this->db->affected_rows(); 
        }
        else{
            return -1;
        }   
    }

     // check report exists or not
     function check_post_report_exist($user_id = null, $challengeid = null){

        $this->db->where(array('challenge_report_challenge_id' => $challengeid , 'challenge_report_user_id' => $user_id));
        $res = $this->db->get('challenge_report');
        return $res->num_rows();
    }   





    // update post report count
    function updatepartyReportcount($challengeid = null){

        $query = "UPDATE challenge SET challenge_report_count = challenge_report_count + 1  WHERE challenge_id = $challengeid";
        $this->db->query($query);


    }

    // get challenge report array
    function getChallengereportArray($userid = null){

        $this->db->select('DISTINCT(challenge_report_challenge_id)');
        $this->db->where('challenge_report_user_id',$userid);
        $res = $this->db->get('challenge_report');
        return $res->result_array();
    }



   //remove post by report count
   function removeChallengePostbyCount($id = null){
    if($this->check_report_count($id) > 0){
     $this->db->where('challenge_id', $id);
     $this->db->update('challenge',array('challenge_active'=>0));
    }
    else{
        return false;
    }
    }

    //check report count
    function check_report_count($id){
        $this->db->where('challenge_id', $id);
        $this->db->where('challenge_remove_count >=', 5);   
        $res = $this->db->get('challenge');
        return $res->num_rows();
    }

    //challenge make ispayed
    function makePaymentsuccess($where = null , $update = null){
        $this->db->where($where);
        $this->db->update('challenge',$update);
        return $this->db->affected_rows();

    }

    // change converted challange file status
    function updateChallengesfileStatus($file_name){

        $this->db->set('status', 1);
        $this->db->where('challenge_posts_sub_post',$file_name);
        $this->db->update('challenge_posts');

   }
   // get challenge status
  function getChallengeStatus($id){

        $this->db->select('status');
        $this->db->from('challenge_posts');
        $this->db->where('challenge_posts_id',$id);
        $res = $this->db->get();
        return $res->row();
   }

   function deleteChallengeCommentPosts($challangeid,$user_id,$comment_id){

        $this->db->where(array('challenge_posts_comments_challenge_id' => $challangeid , 'challenge_posts_comments_user_id' => $user_id , 'challenge_posts_comments_id' => $comment_id));
       $this->db->update('challenge_posts_comments',array('challenge_posts_comments_challenge_active' => 0));
       return $this->db->affected_rows(); 
   }

   function deleteChallengeComment($challangeid,$user_id,$comment_id){

        $this->db->where(array('challenge_comments_challenge_id' => $challangeid , 'challenge_comments_user_id' => $user_id , 'challenge_comments_id' => $comment_id));
       $this->db->update('challenge_comments',array('challenge_comments_active' => 0));
       return $this->db->affected_rows(); 
   }

   function updateChallengeCommentStatus($challangeid,$user_id,$status){

        if ($status == 0) {
            
             $this->db->where(array('challenge_id' => $challangeid , 'challenge_user_id' => $user_id ));
             $this->db->update('challenge',array('challenge_comments_enable' => 0));
             return -1;
        }else{

            $this->db->where(array('challenge_id' => $challangeid , 'challenge_user_id' => $user_id ));
             $this->db->update('challenge',array('challenge_comments_enable' => 1));
             return 1;
        }

   }

   function updateChallengePostsCommentStatus($challangeid,$user_id,$status){

        if ($status == 0) {
            
             $this->db->where(array('challenge_posts_id' => $challangeid , 'challenge_posts_user_id' => $user_id ));
             $this->db->update('challenge_posts',array('challenge_posts_comments_enable' => 0));
             return -1;
        }else{

            $this->db->where(array('challenge_id' => $challangeid , 'challenge_user_id' => $user_id ));
             $this->db->update('challenge_posts',array('challenge_posts_comments_enable' => 1));
             return 1;
        }

   }

   function isChallengeCommentEnabled($challangeid,$type){ 

        if($type == 1){
            $this->db->select('challenge_posts_comments_enable');
            $this->db->from('challenge_posts');
            $this->db->where('challenge_posts_id',$challangeid);
            $res = $this->db->get();
            return $res->result();
       }else{  

            $this->db->select('challenge_comments_enable');
            $this->db->from('challenge');
            $this->db->where('challenge_id',$challangeid);
            $res = $this->db->get();
            return $res->result();
       }
   }

    function getExpireChallenges()
    {
        $date = date("d-m-Y", strtotime(date("d-m-Y", strtotime(date('d-m-Y'))) . " + 2 days"));

        $today = date('d-m-Y');

        $this->db->select('challenge_id,challenge_content,challenge_user_id');
        $this->db->from('challenge');
        $this->db->where('challenge_expired_date >=',$today);
        $this->db->where('challenge_expired_date <=',$date);
        $this->db->where('is_challenge_expire_notified',0);
        return $this->db->get()->result();

    }

    function getExpireChallengesHourly()
    {
        $today = date('d-m-Y');

        $DateTime = new DateTime();
        $current = $DateTime->format("H:i a");
        $DateTime->modify('-2 hours');
        $time = $DateTime->format("H:i a");

        $this->db->select('challenge_id,challenge_content,challenge_user_id');
        $this->db->from('challenge');
        $this->db->where('challenge_expired_date =',$today);
        $this->db->where('challenge_expired_time >=',$time);
        $this->db->where('challenge_expired_time <=',$current);
        $this->db->where('is_challenge_expire_notified !=',2);
        return $this->db->get()->result();

    }

    function updateChallenge($challange_id,$data)
    {
        $this->db->where('challenge_id',$challange_id);
        $this->db->update('challenge',$data);
        return $this->db->affected_rows();
    }


    function getChallengePolls()
    {
        $this->db->select('challenge_id,challenge_content,challenge_user_id,count(is_notified) as count');
        $this->db->from('challenge_polls');
        $this->db->join('challenge','challenge.challenge_id = challenge_polls.challenge_polls_challenge_id');
        $this->db->where('is_notified',0);
        $this->db->group_by('challenge_polls_challenge_id');
        return $this->db->get()->result();
    }


    function getChallengePostPolls()
    {
        $this->db->select('count(is_notified) as count,challenge_polls_challenge_post_id');
        $this->db->from('challenge_post_polls');
        $this->db->where('is_notified',0);
        $this->db->group_by('challenge_polls_challenge_post_id');
        return $this->db->get()->result();
    }

    function getPollUsers($post_id)
    {
        $this->db->select('users.users_name as poll_user,challenge_posts.challenge_posts_user_id,challenge_posts.challenge_posts_challenge_id');
        $this->db->from('challenge_post_polls');
        $this->db->join('users','users.user_id = challenge_post_polls.challenge_post_polled_by');
        $this->db->join('challenge_posts','challenge_posts.challenge_posts_id = challenge_post_polls.challenge_polls_challenge_post_id');
        $this->db->where('is_notified',0);
        $this->db->where('challenge_post_polls.challenge_polls_challenge_post_id',$post_id);
        $this->db->order_by('challenge_post_polls_id','desc');
        return $this->db->get()->row();

    }

    function updateChallengePostPolls($challange_id,$data)
    {
        $this->db->where('challenge_polls_challenge_post_id',$challange_id);
        $this->db->update('challenge_post_polls',$data);
        return $this->db->affected_rows();
    }

    function getUnNotifiedPolls($post_id)
    {
        $this->db->select('count(challenge_post_polls_id) as count');
        $this->db->from('challenge_post_polls');
        $this->db->where('is_notified',0);
        $this->db->where('challenge_post_polls.challenge_polls_challenge_post_id',$post_id);
        $this->db->group_by('challenge_polls_challenge_post_id');
        return $this->db->get()->row();
    }

    function getExpireChallengesNow()
    {
        $today = date('d-m-Y');

        $this->db->select('challenge_id,challenge_content,challenge_user_id');
        $this->db->from('challenge');
        $this->db->where('challenge_expired_date',$today);
        return $this->db->get()->result();
    }
}

?>