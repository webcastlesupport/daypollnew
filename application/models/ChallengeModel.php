<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ChallengeModel extends CI_Model{
    
    //get all party list
    function getallChallenge($type = null){
        
        $this->db->select('challenge.*,users.users_name');
        if(!empty($type)){
          $this->db->where('challenge_type',$type);
        }
        $this->db->from('challenge');
        $this->db->order_by('challenge_id','desc');
        $this->db->join("users","users.user_id = challenge.challenge_user_id");
        $res = $this->db->get();
        return $res->result_array();
        
    }

  // get challenge details by id
  function getchallengedetails($id = null){

    $this->db->where('challenge_id',$id);
    $res = $this->db->get('challenge');
    return $res->result_array();    
  }

  // get challenge poll count
  function getChallengepollCount($cid = null){

    $this->db->where('challenge_polls_challenge_id',$cid);
    return $this->db->get('challenge_polls')->num_rows();

  }

   // block challenge by id
   function blockchallenge($cid = null , $flag = null){

    $this->db->where('challenge_id',$cid);
    $this->db->update('challenge',array('challenge_active' => $flag));
    return $this->db->affected_rows();
}

 // block challenge by id
 function verifychallenge($cid = null , $flag = null){

  $this->db->where('challenge_id',$cid);
  $this->db->update('challenge',array('challenge_isverified' => $flag));
  return $this->db->affected_rows();
}

   // get groupmembers
   function viewjoinedMembers($id = null){
    $this->db->select('users_name,users_country,users_state,users_city,users_photo,users_mobile');
    $this->db->where('group_followers_group_id',$id);
    $this->db->join("users","users.user_id = group_followers.group_followers_user_id");
    $res = $this->db->get('group_followers');
    return $res->result_array();
}

    function viewMembers($id = null){
     
      $this->db->select('user_id,users_name,users_country,users_state,users_city,users_photo,users_mobile');
      $this->db->where('challenge_posts_challenge_id',$id);
      $this->db->join("users","users.user_id = challenge_posts.challenge_posts_user_id");
      $res = $this->db->get('challenge_posts');
      return $res->result_array();
    }

    // get challenge winner
    function getWinner($cid = null){

      $this->db->select(array('users.users_name','users.users_mobile','users.users_country','users.users_state','users.users_city','users.users_photo','users.users_login_type','challenge_posts.*','challenge_polls_challenge_post_id','COUNT(challenge_polls_challenge_post_id) as total'));
      $this->db->where('challenge_polls_challenge_id',$cid);
      $this->db->group_by('challenge_polls_challenge_post_id');
      $this->db->order_by('total','desc');
      $this->db->limit(1);
      $this->db->join('users','users.user_id = challenge_post_polls.challenge_post_polled_by');
      $this->db->join('challenge_posts','challenge_posts.challenge_posts_id = challenge_post_polls.challenge_polls_challenge_post_id');
      $res = $this->db->get('challenge_post_polls');
      return $res->result_array();
  }
    

  function getUsername($postid = null){

    $this->db->select('users_name,users_photo');
    $this->db->where('challenge_posts_id' , $postid);
    $this->db->join('users','users.user_id = challenge_posts.challenge_posts_user_id');
    $res = $this->db->get('challenge_posts');
    $return['username'] =  $res->row('users_name');
    $return['photo']    =  $res->row('users_photo');
    return $return;
  }

}