<?php
class ChatApiModel extends CI_Model{

    function sendmsg($insert = null){

        $this->db->insert('chat_table',$insert);
        return $this->db->insert_id();

    }

    function getlastMsg($chatid = null){

        $this->db->select(array('chat_id','chat_sender_id','thumbnail','chat_reciever_id','chat_message','chat_post','chat_date','chat_type','chat_seen_status','is_completed'));
        $this->db->where('chat_id',$chatid);
       
        $res = $this->db->get('chat_table');
        return $res->result_array();
    }

    function getChatlist($userid =null , $sendid = null){

        $query = "SELECT `chat_id`,`chat_sender_id`,`chat_reciever_id`,`thumbnail`,`chat_message`,`chat_post`,`chat_date`,`chat_type`,`chat_seen_status`,`is_completed` FROM `chat_table` WHERE (`chat_sender_id` = $userid and `chat_reciever_id`= $sendid and `chat_sender_status`= 1) or (`chat_sender_id`=$sendid and `chat_reciever_id`= $userid and `chat_reciever_status`= 1)";
        $res =  $this->db->query($query);
        return $res->result_array();
    }


    function deleteChatbyid($chatid = null , $userid = null , $col_name = null){
        if(!empty($col_name)){
        $this->db->where(array('chat_id' => $chatid));
        $this->db->update('chat_table',$col_name);
        return $this->db->affected_rows();
        }else{
            return 0;
        }
    }

    function deleteChatbyuser($chatid = null , $col_name){
        
        $this->db->where('chat_id',$chatid);
        $this->db->update('chat_table',array($col_name=>0));
        return $this->db->affected_rows();
    }

    function getchatInbox($userid = null){

        $this->db->select('chat_sender_id,chat_reciever_id');
        //$this->db->distinct('chat_sender_id');
        $this->db->where('chat_reciever_id',$userid);
        $this->db->or_where('chat_sender_id',$userid);
        $res = $this->db->get('chat_table');
        return $res->result_array();
    }

    function getChatInboxMsg($id = null){

        $this->db->where('chat_reciever_id',$id);
        $res = $this->db->get('chat_table');
        return $res->result_array();
    }

    function searchUserinChatsender($search = null, $userid = null){
        
        $this->db->select('chat_table.chat_sender_id as userid,chat_table.chat_reciever_id as loggedid,chat_table.chat_date,users.users_name as user_name,users.users_photo as user_photo'); 
        $this->db->where('chat_reciever_id',$userid);
        $this->db->where('chat_reciever_status',1);
        // $this->db->where('chat_reciever_status',1);
        // $this->db->or_where('chat_sender_status',1);
        $this->db->join('users','users.user_id = chat_table.chat_sender_id');
        $this->db->like('users_name',$search);
        $res = $this->db->get('chat_table');
        return $res->result_array();

    }

    function searchUserinChatreciever($search = null, $userid = null){
        
        $this->db->select('chat_table.chat_reciever_id as userid,chat_table.chat_sender_id as loggedid,chat_table.chat_date,users.users_name as user_name,users.users_photo as user_photo'); 
    
        $this->db->where('chat_sender_id',$userid);
        $this->db->where('chat_sender_status',1);
        // $this->db->where('chat_reciever_status',1);
        // $this->db->or_where('chat_sender_status',1);
        $this->db->join('users','users.user_id = chat_table.chat_sender_id');
        $this->db->like('users_name',$search);
        $res = $this->db->get('chat_table');
        return $res->result_array();

    }

    function udpateMsgread($fromid = null, $toid = null){

        $query = "UPDATE `chat_table` SET `chat_seen_status` = 1 WHERE  (`chat_sender_id`=$toid and `chat_reciever_id`= $fromid)";
        $res = $this->db->query($query);
        return $this->db->affected_rows();
        // $this->db->where('chat_id',$msgid);
        // $res = $this->db->update('chat_table',array('chat_seen_status' => 1));
        // return $this->db->affected_rows();

    }

    // check user blocked ot not
    function check_isBlocked($where = null){
        $this->db->select('users_active');
        $this->db->where($where);
        return $this->db->get('users')->num_rows();
    }

    function get_notification_count($id = null){

       $this->db->where('notifications_user_poll_id', $id);
       $this->db->where('notifications_user_id!=',$id);
       $this->db->where('notifications_isread' , 1);
       return $this->db->get('notifications')->num_rows();
       
    }

    function getMsgunreadCount($sender_id = null , $userid = null){

        $this->db->where('chat_reciever_id',$userid);
        $this->db->where('chat_sender_id',$sender_id);
        $this->db->where('chat_seen_status' , 0);
       
        $res = $this->db->get('chat_table');
        return $res->num_rows();
    }

    function update_read($id = null){
        $this->db->where('notifications_user_poll_id',$id);
        $this->db->update('notifications',array('notifications_isread'=>0));
    }

    function get_sender($userid = null){
        $this->db->select('chat_table.chat_sender_id as userid,chat_table.chat_reciever_id as loggedid,chat_date');
        $this->db->where('chat_reciever_id' , $userid);
        $this->db->where('chat_reciever_status' , 1);
        $res = $this->db->get('chat_table');
        return $res->result_array();
    }

    function get_reciever($userid = null){

        $this->db->select('chat_table.chat_sender_id as loggedid,chat_table.chat_reciever_id as userid,chat_date');
        $this->db->where('chat_sender_id' , $userid);
        $this->db->where('chat_sender_status' , 1);
        $res = $this->db->get('chat_table');
        return $res->result_array();
    }

    function getlastMsgbyid($sender = null , $reciever = null){
       
        $query = "SELECT `chat_message`,`chat_post`,`chat_seen_status`,`thumbnail`,`chat_sender_id`,`chat_date`,`chat_type` FROM `chat_table` WHERE (`chat_sender_id` = $sender and `chat_reciever_id`= $reciever and `chat_sender_status` = 1) or (`chat_sender_id`=$reciever and `chat_reciever_id`= $sender and `chat_reciever_status` = 1) ORDER BY `chat_date` DESC LIMIT 1";
        $res = $this->db->query($query);
        return $res->result_array();
        
    }

    function getUnreadMessages($sender = null , $reciever = null){
       
        $query = "SELECT `chat_message` FROM `chat_table` WHERE  (`chat_sender_id`=$reciever and `chat_reciever_id`= $sender and `chat_seen_status` = 0)";
        $res = $this->db->query($query);
        return $res->num_rows();
        
    }

    function getUnreadMessagesCount($sender = null , $reciever = null){
       
        $query = "SELECT * FROM `chat_table` WHERE  (`chat_sender_id`=$reciever and `chat_reciever_id`= $sender and `chat_seen_status` = 0)";
        $res = $this->db->query($query);
        return $res->num_rows();
        
    }
    

    function getusernameforChat($userid = null){
        $res = $this->db->get_where('users',array('user_id' => $userid));
        $return['username'] = $res->row('users_name');
        $return['userphoto'] = $res->row('users_photo');
        return $return;
    }

    function getMessages($fromid = null ,$toid = null){

        $query = "SELECT * FROM `chat_table` WHERE (`chat_sender_id` = $fromid and `chat_reciever_id`= $toid) or (`chat_sender_id`=$toid and `chat_reciever_id`= $fromid) ORDER BY `chat_date` DESC";
        $res = $this->db->query($query);
        return $res->result_array();

    }

    function clearChat($userid = null){

        //$query = "DELETE FROM `chat_table`  WHERE (`chat_sender_id` = $userid) or (`chat_reciever_id`= $userid)";
        
        $this->db->query("UPDATE `chat_table` SET `chat_sender_status` = 0 WHERE `chat_sender_id` = $userid");
        $this->db->query("UPDATE `chat_table` SET `chat_reciever_status` = 0 WHERE `chat_reciever_id` = $userid");
        return $this->db->affected_rows();
    }

    function get_sender_for_delete($loggedid = null ,$userid = null){
       
        $this->db->where(array('chat_sender_id' => $loggedid,'chat_reciever_id' => $userid));
        $res = $this->db->get('chat_table');
        return $res->result_array();
    }

    function get_reciever_for_delete($loggedid = null ,$userid = null){

        $this->db->where(array('chat_sender_id' => $userid,'chat_reciever_id' => $loggedid));
        $res = $this->db->get('chat_table');
        return $res->result_array();
    }

    function getPost($tb_name =null, $where = null){
        $res = $this->db->get_where($tb_name,$where);
        return $res->result_array();
    }

    function blockUserChat($userid = null, $blockeduserid = null){

        $return = 0;
        $check = array('userid' => $userid , 'blocked_userid' => $blockeduserid);
        if($this->isBlocked($check) == 0){
            $this->db->insert('chat_blocked_users',$check);
            $return = 1;
        }else{
            $this->db->where($check);
            $this->db->delete('chat_blocked_users');
            $return = -1;
        }
        return $return;
    }

    function isBlocked($check){
        $res = $this->db->get_where('chat_blocked_users',$check);
        return $res->num_rows();
    }
    function updateChatVideoStatus($file_name){

        $this->db->set('is_completed', 1);
        $this->db->where('chat_message',$file_name);
        $this->db->update('chat_table');
    }

}
?>


