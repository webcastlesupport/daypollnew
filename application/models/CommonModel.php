<?php
class CommonModel extends CI_Model{

    function likeComment($insert = null , $tb_name = null){

        if($this->checkLiked($tb_name,$insert)==0){
            $this->db->insert($tb_name,$insert);
            $return = 1;
        }
        else{
            
            $where_status   = array('comment_likes_comment_id'      => $insert['comment_likes_comment_id'],
                                    'comment_likes_user_id'         => $insert['comment_likes_user_id'],
                                    'comment_likes_comment_user_id' => $insert['comment_likes_comment_user_id'],
                                    'comment_likes_status'  => 1    
                                    );

            if($this->checkLikedStatus($tb_name,$where_status)==1){
                
                $this->db->where('comment_likes_status',1);
                $this->db->update($tb_name,array('comment_likes_status' => 0));
                $return = -1;
            }
            else{
                
                $this->db->where('comment_likes_status',0);
                $this->db->update($tb_name,array('comment_likes_status' => 1));
                $return = 1;
            }
        }
        return $return; 
    }

    function checkLikedStatus($tb_name,$where){

       $res =  $this->db->get_where($tb_name,$where);
       return $res->num_rows();  
    }

    function checkLiked($tb_name,$where){

       $res =  $this->db->get_where($tb_name,$where);
       return $res->num_rows();  
    }

    function check_token($id = null , $token = null){

        $check  = array('user_id' => $id , 'users_login_token' => $token);
        $res    = $this->db->get_where('users',$check);
        return $res->num_rows();
    }
}