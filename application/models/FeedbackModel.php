<?php

class FeedbackModel extends CI_Model{

    function getfeedback(){

        $this->db->where('feedback_active',1);
        $this->db->order_by('feedback_id','desc');
        $res = $this->db->get('feedback');
        return $res->result_array();
        
    }
}
?>