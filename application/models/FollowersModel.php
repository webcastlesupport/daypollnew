<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class FollowersModel extends CI_Model{
    
    
     /************************************** Make user followers ************************************************/
    
    
    // poll user code or unpoll
    function polluser($userid=null,$followerid=null){   
        
        if($this->isPollexist($userid,$followerid)==0){    //check already once followed or not
                
                $insert = array('users_followers_users_id'=>$userid,'users_followers_follower_id'=>$followerid,'users_followers_active'=>1);
                $this->db->insert('users_followers',$insert);
                $notifications = array('notifications_user_id' =>$followerid , 'notifications_user_poll_id' => $userid , 'notifications_type' => 2);
                $this->db->insert('notifications',$notifications);
                $return['msg'] = "new like";
        }
        else{
             if($this->isPollexist2($userid,$followerid,1)==0){  // check follow flag followed or not
                 
                $this->db->where(array('users_followers_users_id' => $userid,'users_followers_follower_id'=>$followerid));
                $update = array('users_followers_active' => 1);
                $notifications = array('notifications_user_id' => $followerid , 'notifications_user_poll_id' => $userid , 'notifications_type' => 2);
                $this->db->insert('notifications',$notifications);
               
                $this->db->update('users_followers',$update);
                $return['msg'] = "liked";
             }
             else{
                $this->deleteNotifications($userid , $followerid); 
                $this->db->where(array('users_followers_users_id'=>$userid,'users_followers_follower_id'=>$followerid));
                $update = array('users_followers_active' => 0);
                $this->db->update('users_followers',$update);
                $return['msg'] = "unliked";
             }
       
        }
        return $return;
      }
    
      // delete user poll notification
      function deleteNotifications($userid = null, $followerid = null){
                $this->db->where(array('notifications_user_id' => $followerid , 'notifications_user_poll_id' => $userid , 'notifications_type' => 2));
                $this->db->delete('notifications');
    }


      // follower already followed or not
    function isPollexist($userid=null,$followerid=null){
        
        $this->db->where(array('users_followers_users_id'=>$userid,'users_followers_follower_id'=>$followerid));
        $res = $this->db->get('users_followers'); 
        return $res->num_rows();
    }
    
      // follower follows or not 
    function isPollexist2($userid=null,$followerid=null,$flag=null){
        
        $this->db->where(array('users_followers_users_id'=>$userid,'users_followers_follower_id'=>$followerid,'users_followers_active'=>$flag));
        $res = $this->db->get('users_followers');
        return $res->num_rows();
    }
    
    
    /************************************** Make party followers ************************************************/
    
    
    
    // get user followed party id
    function getuserpartyid($userid = null){
        
       $this->db->where(array('user_id'=>$userid));
       $this->db->select('users_party_id');
       $res=$this->db->get('users');
       return $res->row('users_party_id');
    }

    // check user followed or not 
    function checkuserParty($userid = null ,$columnname = null){

        $this->db->select(array('user_id',$columnname));
        $this->db->where(array('user_id' => $userid));
        $res = $this->db->get('users');
        return $res->row($columnname);

    }

    // check  party joined
    function check_party_joined($userid = null , $columnname = null){

        $this->db->select($columnname);
        $this->db->where('user_id',$userid);
        return $this->db->get('users')->row($columnname);
    }

    
    // join party
    function joinParty($userid = null,$joincolumnname = null, $pollcolumnname = null , $partyid = null){   
        
        
        $update = array($joincolumnname => $partyid,$pollcolumnname => $partyid);
        $this->db->where(array('user_id' => $userid));
        $this->db->update('users',$update);
        return $this->db->affected_rows();
    }


    // poll party
    function pollParty($userid = null , $partyid = null ,$get_polled_party = null, $polltype = null){
        
        if($get_polled_party == $partyid){
            
            $update=array('users_poll_party_id' => 1,'users_party_poll_type' => 0);
            $this->db->where(array('user_id' => $userid));
            $res = $this->db->update('users',$update);
            $return['msg'] = -1; 
        }
        else{
            
            $update=array('users_poll_party_id' => $partyid,'users_party_poll_type' => $polltype);
            $this->db->where(array('user_id' => $userid));
            $res = $this->db->update('users',$update);
            $return['msg'] = 1; 
        }
        
       
        return $return;

    }

    //get Polled party
    function getPolledparty($userid = null){

       
        $res = $this->db->get_where('users',array('user_id'=>$userid));
        return $res->row('users_poll_party_id');
    }

     //get Polled party
     function getPolledpartyname($partyid = null){

        $this->db->select('party_name');
        $res = $this->db->get_where('party',array('party_id'=>$partyid));
        return $res->row('party_name');
    }

    // remove join
    function removeJoin($userid = null ,$joincolumnname = null, $pollcolumnname = null){

        $this->db->where('user_id',$userid);
        $update = array($joincolumnname => 1 , $pollcolumnname => 1);
        $this->db->update('users',$update);
        return $this->db->affected_rows();
    }
    
     //remove followers after unjoin
    function removeFollowers($userid = null , $partyid = null){
        
        $query = "DELETE  FROM `users_party_followers` WHERE 
        (`users_party_followers_user_id` = $userid AND `users_party_followers_party_id` = $partyid) OR 
        (`users_party_followers_user_follower_id` = $userid AND `users_party_followers_party_id` = $partyid)";

        $this->db->query($query);
    }

    // unpoll user party
    function unpollUserParty($userid = null , $columnname = null){
        
        $this->db->where('user_id',$userid);
        $update = array($columnname => 1);
        $this->db->update('users',$update);
        return $this->db->affected_rows() >0 ? 1 : 0;

    } 

    // unpoll party
    function unpollParty($userid = null ,$partyid = null){

        if(!empty($partyid)){
            $this->db->where('user_id',$userid);
            $update = array('users_poll_party_id' => $partyid);
            $this->db->update('users',$update);
            return $this->db->affected_rows() >0 ? -1 : 0;
        }
        else{
            $this->db->where('user_id',$userid);
            $update = array('users_poll_party_id' => 1);
            $this->db->update('users',$update);
            return $this->db->affected_rows() > 0 ? -2 : 0;
        }
        }
        
        function polluserParty($userid = null , $partyid = null,$poll_type = null , $columnname = null ,$typecolumn = null){

            $this->db->where('user_id',$userid);
            $this->db->update('users',array($columnname => $partyid , $typecolumn => $poll_type));
            return $this->db->affected_rows();

        }

        function getChatFollowerUsersList($userid = null,$search = null){
            
            $this->db->select('users_followers.users_followers_follower_id as userid, users.users_name,users.users_photo');
            if(!empty($search)){
                $this->db->like('users_name',$search);
            }
            $this->db->where(array('users_followers_users_id' => $userid , 'users_followers_active' => 1));
            $this->db->join('users','users.user_id = users_followers.users_followers_follower_id');
            $res = $this->db->get('users_followers');
            return $res->result_array();  
    
        }

        function getChatFollowingUsersList($userid = null , $search = null){
            
            $this->db->select('users_followers.users_followers_users_id as userid, users.users_name,users.users_photo');
            if(!empty($search)){
                $this->db->like('users_name',$search);
            }
            $this->db->where(array('users_followers_follower_id' => $userid , 'users_followers_active' => 1));
            $this->db->join('users','users.user_id = users_followers.users_followers_users_id');
            $res = $this->db->get('users_followers');
            return $res->result_array();  
    
        }

        function getBlockedUserlist($userid){
            $this->db->where(array('userid' => $userid));
            $this->db->or_where(array('blocked_userid' => $userid));
            $res = $this->db->get('chat_blocked_users');
            return $res->result_array();
        }

        // follower already followed or not
        function getFollowStatus($userid=null,$followerid=null){ 
            $this->db->select('*');
            $this->db->from('users_followers');
            $this->db->where(array('users_followers_users_id'=>$followerid,'users_followers_follower_id'=>$userid,'users_followers_active' => 1));
            $res = $this->db->get(); 
            return $res->num_rows(); 
            
            
        }
    
  
  }