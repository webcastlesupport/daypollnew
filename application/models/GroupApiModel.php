<?php

class GroupApiModel extends CI_Model{

    // create new group
    function createGroup($insert = null){

        $this->db->insert('group_table',$insert);
        return $this->db->insert_id();
    }

    // get created group name
    function getcreatedGroupname($id = null){
        $this->db->where('group_id',$id);
        $res = $this->db->get('group_table');
        return $res->row('group_name');
    }

    // add admin as member after creating group
    function addAdminasmember($groupid = null , $userid = null){
        $this->db->insert('group_followers',array('group_followers_group_id' => $groupid , 'group_followers_user_id' => $userid));
        return $this->db->affected_rows();
    }

    // get group by id
    function getgroupProfile($id = null){

        $this->db->select(array('group_table.*','users.users_name'));
        $this->db->where('group_id',$id);
        
        $this->db->from('group_table');
        $this->db->join('users','users.user_id = group_table.group_created_by');
        $res = $this->db->get();
        return $res->num_rows() > 0 ? $res->result_array() : 0;
    }

    // get profile pic for remove from folder
    function geteditprofilepic($id = null){
       
        $this->db->where(array('group_id' => $id));
        $res = $this->db->get('group_table');
        $return['profile']          =  $res->row('group_profile_photo');
        $return['cover']            =  $res->row('group_cover_photo');
        return $return;
    }

    // update group
    function updateGroup($id = null , $update = null){
        $this->db->where('group_id',$id);
        $this->db->update('group_table',$update);
        return $this->db->affected_rows();
    }

    // join group
    function joinGroup($userid = null , $groupid = null){

        $res = $this->isJoinexists($userid , $groupid);
        

        switch($res){

            case 0:
            $insert = array('group_followers_group_id' => $groupid ,'group_followers_user_id' => $userid);
            $this->db->insert('group_followers',$insert);
            return 1;
            break;

            case $res > 0:
            
            return -1;
            break;
        }
        
        
    }

     // unjoin group
     function unjoinGroup($userid = null , $groupid = null){

      $this->db->where(array('group_followers_group_id' => $groupid ,'group_followers_user_id' => $userid));
      $this->db->delete('group_followers');
      return $this->db->affected_rows();  
    }
        
     //remove followers after unjoin
     function removeFollowers($userid = null , $partyid = null){
        
        $query = "DELETE  FROM `users_group_followers` WHERE 
        (`users_group_followers_user_id` = $userid AND `users_group_followers_group_id` = $partyid) OR 
        (`users_group_followers_user_follower_id` = $userid AND `users_group_followers_group_id` = $partyid)";

        $this->db->query($query);
    }
 

    // already join exists
    function isJoinexists($userid = null , $groupid = null){

        $this->db->where(array('group_followers_group_id' => $groupid ,'group_followers_user_id' => $userid ));
        $res = $this->db->get('group_followers');
        return $res->num_rows();

    }

    //is user joined
    function isGroupjoined($userid = null , $groupid = null){
        $this->db->where(array('group_followers_group_id' => $groupid ,'group_followers_user_id' => $userid ));
        $res = $this->db->get('group_followers');
        return $res->num_rows();

    }

    // get group joined members
    function getgroupMembersbypoll($groupid = null){

        $this->db->select(array('user_id','users_name','users_photo','COUNT(users_group_followers_user_id) as total'));
        $this->db->where('users_group_followers_group_id' , $groupid);
        $this->db->group_by('users_group_followers_user_id');
        $this->db->order_by('total','desc');
        $this->db->limit(5);
        $this->db->join('users','users.user_id = users_group_followers.users_group_followers_user_id');
        $res = $this->db->get('users_group_followers');
        $return['row']  = $res->row('user_id');
        $return['data'] = $res->result_array();
        return $return;



        // $this->db->select(array('user_id','users_name','users_photo'));
        // $this->db->where('group_followers_group_id' , $groupid);
        // $this->db->join('users','users.user_id = group_followers.group_followers_user_id');
        // $res = $this->db->get('group_followers');
        // return $res->result_array();

    }

    function getGroupmembers($groupid = null){

        $this->db->select(array('DISTINCT(user_id)','users_name','users_photo'));
        $this->db->where(array('group_followers_group_id'=>$groupid));
        //$this->db->where('group_followers_user_id!=',$userid);
        $this->db->join('users','users.user_id = group_followers.group_followers_user_id');
        $res = $this->db->get('group_followers');
        $return['data']  =  $res->result_array();
        $return['count'] =  $res->num_rows();
        return $return;
    }

    // get group members poll count
    function getGroupmemberspollcount($userid = null){

        $this->db->where('users_group_followers_user_id',$userid);
        $res = $this->db->get('users_group_followers');
        return $res->num_rows();
    }

    // upload group posts
    function uploadGroupposts($insert = null){

        $this->db->insert('group_posts',$insert);
        return $this->db->affected_rows();
    }

    // get group posts
    function getgroupPosts($groupid = null){
        
        $this->db->select(array('group_posts.*','user_id','users_name','users_photo'));
        $this->db->where(array('group_posts_group_id' => $groupid , 'group_posts_active' => 1,'group_posts_isblocked' => 0));
        $this->db->join('users','users.user_id = group_posts.group_posts_posted_by');
        $this->db->order_by('group_posts_id','desc');
        $res = $this->db->get('group_posts');
        return $res->result_array();
    }

    // posts comments
    function commentgroupPost($user_id = null , $post_id = null , $msg = null ,$user_posts_id = null){

        $insert = array('group_posts_comments_post_id' => $post_id , 'group_posts_comments_user_id' => $user_id , 'group_posts_comments_comment' => $msg);
        $this->db->insert('group_posts_comments' , $insert);
        
        $update_notifications = array('notifications_type' => 12,'notifications_post_id' => $post_id ,'notifications_user_id' => $user_id,'notifications_user_poll_id' => $user_posts_id);

        if($this->db->affected_rows() > 0){
            $this->db->insert('notifications',$update_notifications);
            $this->db->select(array('group_posts_comments.*','users.users_name'));
            $this->db->limit(3);
            $this->db->order_by('group_posts_comments_id',"desc");
            $this->db->from('group_posts_comments');
            $this->db->where(array('group_posts_comments_post_id'=>$post_id));
            $this->db->join('users','users.user_id = group_posts_comments.group_posts_comments_user_id');
            $res = $this->db->get();
            $return['data'] = $res->result_array();
           
        }
        else{

            return 0;
        }
        return $return;

    }

     // get post comments
     function getgroupPostcomments($post_id = null){

        $this->db->select(array('group_posts_comments.*','users.users_name','users.users_photo'));
        $this->db->join('users','users.user_id = group_posts_comments.group_posts_comments_user_id');
        $res = $this->db->get_where('group_posts_comments',array('group_posts_comments_post_id' => $post_id));
        return $res->num_rows() > 0 ? $res->result_array() : null;

    }

      // get comments reply via comment id
      function getgroupcommentsreply($id = null , $post_id = null){

        $this->db->from('group_posts_comments_reply');
        $this->db->order_by('group_posts_comments_reply_id',"desc");
        $this->db->select(array('group_posts_comments_reply.*','users.users_name','users.users_photo'));
        $this->db->where(array('group_posts_comments_reply_comment_id' => $id , 'group_posts_comments_reply_posts_id' => $post_id));
        $this->db->join('users','users.user_id = group_posts_comments_reply.group_posts_comments_reply_user_id');
        $res    = $this->db->get();
        return $res->result_array();
    }

      // get comments reply via comment id
      function getgroupcommentsreplyIos($id = null , $post_id = null){

        $this->db->from('group_posts_comments_reply');
        //$this->db->order_by('group_posts_comments_reply_id',"desc");
        $this->db->select(array('group_posts_comments_reply.*','users.users_name','users.users_photo'));
        $this->db->where(array('group_posts_comments_reply_comment_id' => $id , 'group_posts_comments_reply_posts_id' => $post_id));
        $this->db->join('users','users.user_id = group_posts_comments_reply.group_posts_comments_reply_user_id');
        $res    = $this->db->get();
        return $res->result_array();
    }

      // get post comments by id
      function getgroupPostcommentsbyid($comment_id = null){

        $this->db->select(array('group_posts_comments.*','users.users_name','users.users_photo'));
        $this->db->join('users','users.user_id = group_posts_comments.group_posts_comments_user_id');
        $res = $this->db->get_where('group_posts_comments',array('group_posts_comments_id' => $comment_id));
        return $res->num_rows() > 0 ? $res->result_array() : null;

    }

      //reply to comment
      function replygroupPostscomments($insert = null , $comment_user_id = null){
        
        $update_notifications = array('notifications_type' => 7,'notifications_post_id' => $insert['group_posts_comments_reply_posts_id'] ,'notifications_user_id' => $insert['group_posts_comments_reply_user_id'],'notifications_user_poll_id' => $comment_user_id);

        $this->db->insert('group_posts_comments_reply',$insert);
        $res = $this->db->affected_rows();

        if($res > 0){

            $this->db->insert('notifications',$update_notifications);
            return 1;
        }
        else{
            return 0;
        }
    }


      //poll posts
      function pollgroupPosts($user_id = null, $posts_id = null,$user_posts_id = null){

        $insert               = array('group_posts_polls_post_id' => $posts_id,'group_posts_polls_user_id' => $user_id);
        $update_notifications = array('notifications_type' => 11,'notifications_post_id' => $posts_id ,'notifications_user_id' => $user_id,'notifications_user_poll_id' => $user_posts_id);

        if($this->checkPolled($user_id,$posts_id) == 0){

            $this->db->insert('group_posts_polls',$insert);
            $this->db->insert('notifications',$update_notifications);
            $return['res'] = 1;
        }
        else{
            $this->db->where(array('group_posts_polls_post_id' => $posts_id , 'group_posts_polls_user_id' => $user_id));
            $this->db->delete('group_posts_polls');
            $return['res'] = -1;
        }
       
        return $return;

    }


    //check already polled or not
    function checkPolled($user_id = null,$posts_id = null){

        $this->db->where(array('group_posts_polls_post_id' => $posts_id , 'group_posts_polls_user_id' => $user_id));
        $res = $this->db->get('group_posts_polls');
        return $res->row('group_posts_polls_id');
    }


     //get group posts poll count
     function grouppollsCount($postid = null){

        $res = $this->db->get_where('group_posts_polls',array('group_posts_polls_post_id' => $postid));  
        return $res->num_rows();
        
    }

    // get group posts comments count

    function grouppostscommentsCount($postsid = null){

        $this->db->where('group_posts_comments_post_id',$postsid);
        return $this->db->get('group_posts_comments')->num_rows();

    }

    // get group posts comments posts count and data

    function grouppostscommentsCountanddata($postsid = null){

        $this->db->select(array('group_posts_comments.*','users.users_name','users_photo'));
        $this->db->order_by('group_posts_comments_id',"desc");
        $this->db->from('group_posts_comments');
        $this->db->limit(3);
        $this->db->where(array('group_posts_comments_post_id' => $postsid));
        $this->db->join("users","users.user_id = group_posts_comments.group_posts_comments_user_id");
           
        $res             = $this->db->get();
        $return['count'] = $res->num_rows();
        $return['data']  = $res->result_array();
        return $return;

    }



      // poll group members
      function pollgroupMembers($userid = null, $followerid = null , $groupid = null){

        $this->db->where(array('users_group_followers_group_id' => $groupid,'users_group_followers_user_follower_id' => $followerid));
        $res = $this->db->get('users_group_followers')->num_rows();
        

        if($res > 0){

            $return = -1;
        }
        else{
            
            $insert = array('users_group_followers_user_id' => $userid , 'users_group_followers_user_follower_id' => $followerid ,'users_group_followers_group_id' => $groupid);
            $this->db->insert('users_group_followers',$insert);
            
            if($this->db->affected_rows() > 0){
                $return =  1;
            }
            else{
                $return =  0;
            }
            
        }
        
        return $return;

    }
    // confirm poll group members
    function confirmpollgroupusers($userid = null, $followerid = null , $groupid = null){

            $this->db->where(array('users_group_followers_user_follower_id' => $followerid));

            $update = array('users_group_followers_user_id' => $userid ,
                             'users_group_followers_user_follower_id' => $followerid ,
                             'users_group_followers_group_id' => $groupid);

            $this->db->update('users_group_followers',$update);
                
            return $this->db->affected_rows() > 0 ? 1 : 0;

    }

    // remove party user poll
    function removePollGroupmember($followerid = null , $userid = null , $groupid = null){

        $this->db->where(array('users_group_followers_group_id' => $groupid,'users_group_followers_user_follower_id' => $followerid , 'users_group_followers_user_id' => $userid));
        $this->db->delete('users_group_followers');
        return $this->db->affected_rows() > 0 ? 1 : 0;

    }

    //get party posts poll count
    function partypollsCount($postid = null){

        $res = $this->db->get_where('posts_polls',array('posts_polls_post_id' => $postid));  
        return $res->num_rows();
        
    }

  
    // check  is admin or not
    function isGroupadmin($userid = null,$groupid = null){

        $res = $this->db->get_where('group_table',array('group_created_by' => $userid,'group_id' => $groupid));  
        return $res->num_rows();
    }

    // block group posts
    function blockgroupPosts($postid = null){

        if($this->checkPostblocked($postid) == 0){
           
        $this->db->where('group_posts_id',$postid);
        $update = array('group_posts_isblocked'=>1);
        $this->db->update('group_posts',$update);
        if($this->db->affected_rows() > 0)return 1;
        else return -1;
        }
        else{
         
            $this->db->where('group_posts_id',$postid);
            $update = array('group_posts_isblocked'=>0);
            $this->db->update('group_posts',$update);
            if($this->db->affected_rows() > 0)return 0;
            else return -1;
        }
        
    }

    // check blocked
    function checkPostblocked($postid){

        $this->db->where(array('group_posts_id' => $postid ,'group_posts_isblocked' => 1));
        return $this->db->get('group_posts')->num_rows();
        
    }

    // remove group member
    function removegroupMember($groupid = null, $userid = null){

        $this->db->where(array('group_followers_group_id' => $groupid , 'group_followers_user_id' => $userid));
        $this->db->delete('group_followers');
        return $this->db->affected_rows();
    }

    // remove group member followers if admin removed
    function removegroupMemberfollowers($groupid = null, $userid = null){

        $query = "DELETE FROM `users_group_followers`
        WHERE (`users_group_followers_group_id` = $groupid
        AND `users_group_followers_user_id` = $userid)
        OR (`users_group_followers_group_id` = $groupid
        AND `users_group_followers_user_follower_id` = $userid)";

        $this->db->query($query);
        return $this->db->affected_rows();

    }

    // invite group members
    function inviteMembers($userid = null , $groupid =null , $link = null , $loggeduserid = null){

        $insert = array('group_invite_user_id' => $userid , 'group_invite_group_id' => $groupid ,'group_invite_link' =>$link);
        $this->db->insert('group_invite',$insert);

        if($this->db->affected_rows() > 0){

            $notification         = array('notifications_user_id' => $loggeduserid ,'notifications_user_poll_id' => $userid,'notifications_group_id' => $groupid,'notifications_group_link' => $link,'notifications_type' => 3);  
            $insert_notifications = $this->db->insert('notifications',$notification);
            return 1;
        }
        else{
            return 0;
        }
    }

    // get latest added group list
    function groupList($scope = null ,$country = null, $state = null, $district = null, $area = null){

        $this->db->select('group_id,group_name,group_profile_photo,group_scope');

        if(!empty($scope)) $this->db->where('group_scope',$scope);
        if(!empty($country)) $this->db->where('group_country',$country);
        if(!empty($state)) $this->db->where('group_state',$state);
        if(!empty($district)) $this->db->where('group_district',$district);
        if(!empty($area)) $this->db->where('group_area',$area);
        $this->db->where('group_active' , 1);
        $this->db->order_by('group_id','desc');
       
        $res = $this->db->get('group_table');
        return $res->result_array();
    }

    // get joined group list
    function joinedgroupList($userid = null, $scope = null ,$country = null, $state = null, $district = null, $area = null){
        
        $this->db->select('group_id,group_name,group_profile_photo,group_scope');

        if(!empty($scope)) $this->db->where('group_scope',$scope);
        if(!empty($country)) $this->db->where('group_country',$country);
        if(!empty($state)) $this->db->where('group_state',$state);
        if(!empty($district)) $this->db->where('group_district',$district);
        if(!empty($area)) $this->db->where('group_area',$area);
        $this->db->where('group_followers_user_id',$userid);
        $this->db->where('group_active' , 1);
        $this->db->order_by('group_followers_id','desc');
        $this->db->join('group_table','group_table.group_id = group_followers.group_followers_group_id');
        $res = $this->db->get('group_followers');
        return $res->result_array();
    }

    // get top ranking groups
    function topgroupList($scope = null , $country = null, $state = null, $district = null, $area = null){

        $this->db->select('group_id,group_name,group_profile_photo,group_scope,group_followers_group_id,COUNT(group_followers_group_id) as total');
        $this->db->group_by('group_followers_group_id');
        
        if(!empty($scope)) $this->db->where('group_scope',$scope);
        if(!empty($country)) $this->db->where('group_country',$country);
        if(!empty($state)) $this->db->where('group_state',$state);
        if(!empty($district)) $this->db->where('group_district',$district);
        if(!empty($area)) $this->db->where('group_area',$area);
        $this->db->where('group_active' , 1);
        $this->db->order_by('total','desc');
       
        $this->db->join('group_table','group_table.group_id = group_followers.group_followers_group_id');
        $res = $this->db->get('group_followers');
        return $res->result_array();

    }

      // join private group by link
      function joinPrivategroup($userid , $groupid ,$token){

        if($this->check_link_exist($userid , $groupid ,$token)!=0){

            if($this->check_joined($userid , $groupid)==0){

            $insert = array('group_followers_group_id' => $groupid ,'group_followers_user_id' => $userid);
            $this->db->insert('group_followers',$insert);
            return $this->db->affected_rows() > 0 ? 1 :0;
            }
            else{
                return -2;
            }
        }
        else{
            return -1;
        }
        
        }

    // check link exists
    function check_link_exist($userid , $groupid ,$token){
        $this->db->where(array('notifications_user_poll_id' => $userid ,'notifications_group_id' =>$groupid,'notifications_group_link' => $token));
        $res = $this->db->get('notifications');
        return $res->num_rows();
    }

      // check already joined
      function check_joined($userid , $groupid ){
        $this->db->where(array('group_followers_group_id' => $groupid ,'group_followers_user_id' =>$userid));
        $res = $this->db->get('group_followers');
        return $res->num_rows();
    }

    // delete notification join by link after success
    function deleteNotification($id = null){
        $this->db->where('notifications_id',$id);
        $this->db->delete('notifications');
    }


    // is group post polled
    function isPolledgrouppost($userid =null , $groupid = null){

        $this->db->where(array('group_posts_polls_post_id' => $groupid , 'group_posts_polls_user_id' => $userid));
        $res = $this->db->get('group_posts_polls');
        return $res->num_rows();
    }

     // is group post polled
     function isPostpolled($groupid = null , $userid =null){

        $this->db->where(array('group_posts_polls_post_id' => $groupid , 'group_posts_polls_user_id' => $userid));
        $res = $this->db->get('group_posts_polls');
        return $res->num_rows();
    }

    // get group joined members
    function isMemberPolled($loggeduserid = null , $userid = null , $groupid = null){
        
        $this->db->where(array(
                        'users_group_followers_user_id' => $userid,
                        'users_group_followers_user_follower_id' => $loggeduserid,
                        'users_group_followers_group_id' => $groupid
                        ));
        return $this->db->get('users_group_followers')->num_rows();
    }

    // member poll count
    function memberPollcount($userid = null ,$groupid = null){

        $this->db->where(array('users_group_followers_user_id' => $userid , 'users_group_followers_group_id' => $groupid));
        return $this->db->get('users_group_followers')->num_rows();
    }

    // get group invite members
    function getGroupInvitemembers($userid = null , $followerid = null){

        $this->db->select(array('DISTINCT(users.user_id)','users.users_name','users.users_photo'));
        
       if(!empty($userid)){
        $this->db->where('users_followers_users_id',$userid);
        $this->db->or_where('users_followers_follower_id',$userid);
        $this->db->join('users','users.user_id = users_followers.users_followers_users_id');
       }
       if(!empty($followerid)){
        $this->db->where('users_followers_users_id',$followerid);
        $this->db->or_where('users_followers_follower_id',$followerid);
        $this->db->join('users','users.user_id = users_followers.users_followers_follower_id');
       }
        
        $res = $this->db->get('users_followers');
        return $res->result_array();
    }

    // get user poll count
    function getPollCount($userid = null){

        $this->db->where('users_followers_users_id',$userid);
        
        return $this->db->get('users_followers')->num_rows();
    }   

     // get created party
     function getcreatedGroup($id = null){

        $this->db->select(array('group_id','group_name','group_profile_photo'));
        return $this->db->get_where('group_table',array('group_created_by' => $id,'group_active' => 1))->result_array();
      }

      // check user blocked ot not
        function check_isBlocked($where = null){
            $this->db->select('users_active');
            $this->db->where($where);
            return $this->db->get('users')->num_rows();
        }

      // get joined group
      function getjoinedGroup($id = null){

        $this->db->select(array('group_name','group_id','group_profile_photo'));
        $this->db->where(array('group_followers_user_id' => $id , 'group_active' => 1));
        $this->db->join('group_table','group_table.group_id = group_followers.group_followers_group_id');
        $res = $this->db->get('group_followers');
        return $res->result_array();
       
      }

      function requestjoinGroup($userid = null, $groupid = null){

        if($this->checkrequestExists($userid,$groupid) == 0){

            $this->db->insert('group_requests',array('group_request_group_id' => $groupid , 'group_request_user_id' => $userid));
            return $this->db->affected_rows() > 0 ? 1 : 0;
        }
        else{
            return -1;
        }
        
      }

      function checkrequestExists($userid,$groupid){

        $this->db->where(array('group_request_group_id' => $groupid , 'group_request_user_id' => $userid));
        $res = $this->db->get('group_requests');
        return $res->num_rows();
      }

      // get group join requets
      function getGrouprequests($groupid = null){

        $this->db->select('group_requests.*,users.user_id,users.users_photo,users.users_name');
        $this->db->join('users','users.user_id = group_requests.group_request_user_id');
        $res = $this->db->get_where('group_requests',array('group_request_group_id' => $groupid));
        return $res->num_rows() > 0 ? $res->result_array() : 0;
      }

      // reject group request
      function rejectGrouprequests($req_id = null){
        
        $this->db->where('group_request_id',$req_id);
        $this->db->delete('group_requests');
        return $this->db->affected_rows();

      }

      // approve group requests
      function approveGrouprequests($req_id = null , $insert = null){
        
        $this->db->where(array('group_request_id'       => $req_id ,
                               'group_request_group_id' => $insert['group_followers_group_id'] ,
                               'group_request_user_id'  => $insert['group_followers_user_id']
                        
                              ));
        $res = $this->db->get('group_requests');
        if($res->num_rows() > 0){

        $res_insert = $this->db->insert('group_followers',$insert);
        $this->db->where('group_request_id',$req_id);
        $this->db->delete('group_requests');
        return $this->db->affected_rows() > 0 ? 1 :0;
        }
        else{
            
            return -1;
        }
        
      }

      // rank group by user 
      function rankgroupbyLocation($country = null, $state = null, $district = null, $area = null){ 

        $this->db->select('user_id');
        if(!empty($country))$this->db->where('users_country',$country);
        if(!empty($state))$this->db->where('users_state',$state);
        if(!empty($district))$this->db->where('users_city',$district);
        if(!empty($area))$this->db->where('users_area',$area);

        $res = $this->db->get('users');
        return $res->result_array();

      }

      // get group followers count
      function gettopgroup(){

        // $this->db->select('group_table.group_id,group_table.group_name,group_table.group_profile_photo,group_followers_group_id , COUNT(group_followers_group_id) as total');
               
        // $this->db->group_by('group_followers_group_id');
        // $this->db->order_by('total','desc');
        // $this->db->join('group_table','group_table.group_id = group_followers.group_followers_group_id');
        // $res = $this->db->get('group_followers');
        // return $res->result_array();


        $query =     "SELECT a.`group_name`,a.`group_id`,a.`group_profile_photo`,a.`group_scope`,(SELECT COUNT(c.`user_id`) 
        FROM `users` c join `group_followers` d on c.`user_id`=d.`group_followers_user_id`
        WHERE d.`group_followers_group_id`= a.`group_id`) as total 
        FROM `group_table` a join `group_followers` b on a.`group_id`=b.`group_followers_group_id`
        group by a.`group_id` order by total desc";

        $res = $this->db->query($query);
        return $res->result_array();




      }
  

        // get users
        function getUsers($column = null , $value = null){

            $this->db->select('user_id');
            $this->db->where($column , $value);
            $res = $this->db->get('users');
            return $res->result_array();
        }

        // get top ranked group
        function getToprankedGroup(){

            $this->db->get();
        }

        // get groups
        function getGroups(){
            
            $this->db->select('DISTINCT(group_followers_group_id)');
            $res = $this->db->get('group_followers');
            return $res->result_array();
        }

        // get group count
        function gettopCountGroup($column = null , $value = null){

           $query =     "SELECT a.`group_name`,a.`group_id`,a.`group_profile_photo`,a.`group_scope`,(SELECT COUNT(c.`user_id`) 
                        FROM `users` c join `group_followers` d on c.`user_id`=d.`group_followers_user_id`
                        WHERE c.`$column` = '$value' AND d.`group_followers_group_id`= a.`group_id`) as total 
                        FROM `group_table` a join `group_followers` b on a.`group_id`=b.`group_followers_group_id`
                        group by a.`group_id` order by total desc";

            $res = $this->db->query($query);
            return $res->result_array();
        }


        // get group count
        function gettopCountGroup_new($column = null , $value = null){

            $this->db->select('group_table.*,COUNT(group_followers_group_id) as total');
            if(!empty($column)){
                $this->db->where($column,$value);
            }
            $this->db->group_by('group_followers_group_id');
            $this->db->order_by('total','desc');
            $this->db->join('users','users.user_id = group_followers.group_followers_user_id');
            $this->db->join('group_table','group_table.group_id = group_followers.group_followers_group_id');
            $res = $this->db->get('group_followers');
            return $res->result_array();

            // $query =     "SELECT a.`group_name`,a.`group_id`,a.`group_profile_photo`,a.`group_scope`,(SELECT COUNT(c.`user_id`) 
            //              FROM `users` c join `group_followers` d on c.`user_id`=d.`group_followers_user_id`
            //              WHERE c.`$column` = '$value' AND d.`group_followers_group_id`= a.`group_id`) as total 
            //              FROM `group_table` a join `group_followers` b on a.`group_id`=b.`group_followers_group_id`
            //              group by a.`group_id` order by total desc";
 
            //  $res = $this->db->query($query);
             
         }



         // get group count witout scope (global)
         function gettopCountGroupglobal(){

            $query =     "SELECT a.`group_name`,a.`group_id`,a.`group_profile_photo`,a.`group_scope`,(SELECT COUNT(c.`user_id`) 
                         FROM `users` c join `group_followers` d on c.`user_id`=d.`group_followers_user_id`
                         WHERE d.`group_followers_group_id`= a.`group_id`) as total 
                         FROM `group_table` a join `group_followers` b on a.`group_id`=b.`group_followers_group_id`
                         group by a.`group_id` order by total desc";
 
             $res = $this->db->query($query);
             return $res->result_array();
         }


           // get posts comments Count outer
    function commentsCountouter($postid = null){
       
        $res = $this->db->get_where('group_posts_comments',array('group_posts_comments_post_id' => $postid));
        return $res->num_rows();
        
    }
         

    // edit posts by id
     function deletegroupPosts($postid = null){

        $this->db->where('group_posts_id',$postid);
        $update = array('group_posts_active'=>0);
        $this->db->update('group_posts',$update);
        return $this->db->affected_rows();

    }
    // function delete notifications after block or delete posts
    function deletenotifications($postid = null){

        $this->db->where('notifications_post_id',$postid);
        $this->db->delete('notifications');
    }


     // report post
     function reportgroupPost($user_id = null, $post_id = null , $msg = null){

        if($this->check_report_exist($user_id , $post_id) == 0){
        $insert = array('group_posts_report_post_id' => $post_id , 'group_posts_report_user_id' => $user_id , 'group_posts_report_comment' => $msg);
        $this->db->insert('group_posts_report',$insert);
        return $this->db->affected_rows(); 
        }
        else{
            return -1;
        }   
    }

    // check report exists or not
    function check_report_exist($user_id = null, $post_id = null){

        $this->db->where(array('group_posts_report_post_id' => $post_id , 'group_posts_report_user_id' => $user_id));
        $res = $this->db->get('group_posts_report');
        return $res->num_rows();
    }   

    // update post report count
    function updategroupReportcount($postid = null , $msg = null){

        if($msg == "Sexually explicite"){
        $query = "UPDATE group_posts SET group_posts_report_count = group_posts_report_count + 1 , group_posts_remove_count = group_posts_remove_count + 1 WHERE group_posts_id = $postid";
        }else{
        $query = "UPDATE group_posts SET group_posts_report_count = group_posts_report_count + 1 WHERE group_posts_id = $postid";
        }
        $this->db->query($query);


    }

    // remove posts by report count from user
    function removePostsbygroupreportcount($postid = null){

        $this->db->where('group_posts_id' , $postid);
        $this->db->where('group_posts_report_count' , 15);
        $this->db->update('group_posts',array('group_posts_active' => 0));

    }

    // get reported posts by userid
    function getReportedposts($userid = null){

        $this->db->select('DISTINCT(group_posts_report_post_id)');
        $this->db->where('group_posts_report_user_id',$userid);
        $res = $this->db->get('group_posts_report');
        return $res->result_array();
    }

     // get reported posts by userid
     function gethiddenposts($userid = null){

        $this->db->select('DISTINCT(hide_posts_post_id)');
        $this->db->where('hide_posts_user_id',$userid);
        $this->db->where('hide_posts_type',3);
        $res = $this->db->get('hide_posts');
        return $res->result_array();
    }

      // hide posts
      function hidePosts($postid = null, $userid = null){

        $insert= array('hide_posts_post_id' => $postid , 'hide_posts_user_id' =>$userid , 'hide_posts_type' => 3);
        $this->db->insert('hide_posts',$insert);
        return $this->db->affected_rows();
    }

     // edit posts by id
     function editPosts($postid = null, $update = null){

        $this->db->where('group_posts_id',$postid);
        $this->db->update('group_posts',$update);
        return $this->db->affected_rows();

    }

     //get party rank
     function getgrouprank(){

        
        // $this->db->select('group_followers_group_id,COUNT(group_followers_group_id) as total');
        // $this->db->group_by('group_followers_group_id');
        // $this->db->order_by('total','desc');
        // $res = $this->db->get('group_followers');
        // return $res->result_array();
      
        $query =     "SELECT a.`group_name`,a.`group_id`,a.`group_profile_photo`,a.`group_scope`,(SELECT COUNT(c.`user_id`) 
                         FROM `users` c join `group_followers` d on c.`user_id`=d.`group_followers_user_id`
                         WHERE d.`group_followers_group_id`= a.`group_id`) as total 
                         FROM `group_table` a join `group_followers` b on a.`group_id`=b.`group_followers_group_id`
                         group by a.`group_id` order by total desc";
 
             $res = $this->db->query($query);
             return $res->result_array();
    
    } 


    // get users name by id
    function getusersnamebyid($id = null){

        $this->db->select('users_name');
        $this->db->where('user_id',$id);
        $res = $this->db->get('users');
        return $res->row('users_name');
    }


     // get posts by id
     function getnotificationgroupPostsbyid($posts_id = null){

        $this->db->select(array("group_posts.*","users.users_name","users.users_photo"));
        $this->db->where(array('group_posts_id' => $posts_id,'group_posts_active' => 1));
        $this->db->order_by("group_posts_id","DESC");
        $this->db->from("group_posts");
        $this->db->join("users","users.user_id = group_posts.group_posts_posted_by");
        $res = $this->db->get();
       
        return $res->num_rows() > 0 ? $res->result_array() : 0;
        }


        //remove post by report count
        function removeGroupPostbyCount($id = null){
        if($this->check_report_count($id) > 0){
        $this->db->where('group_posts_id', $id);
        $this->db->update('group_posts',array('group_posts_active'=>0));
        }
        else{
            return false;
        }
        }

        //check report count
        function check_report_count($id){
        $this->db->where('group_posts_id', $id);
        $this->db->where('group_posts_remove_count >=', 15);   
        $res = $this->db->get('group_posts');
        return $res->num_rows();
    }
}

?>