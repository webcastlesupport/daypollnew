<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class GroupModel extends CI_Model{
    
    //get all party list
    function getallGroup(){
        
        $this->db->from('group_table');
        $this->db->join("users","users.user_id = group_table.group_created_by");
        $res = $this->db->get();
        return $res->result_array();
        
    }

    // party list for users section
     function getGrouplist(){
        
        $this->db->from('group');
        $res = $this->db->get();
        return $res->result_array();
        
    }

    //block posts by id 
    function blockgroupposts($id = null , $flag = null){

        $update = array('group_posts_active' => $flag);
        $this->db->where(array('group_posts_id' => $id));
        $this->db->update('group_posts',$update);
        return $this->db->affected_rows();
    }

  
    //get bloceked group posts
    function getblockedgroupPosts(){

        $this->db->join('group_table','group_table.group_id = group_posts.group_posts_group_id');
        $res = $this->db->get_where('group_posts',array('group_posts_active' => 0));
        return $res->result_array();
    }

    // get group members list
    function getGroupmembersCount($groupid = null){

        $res = $this->db->get_where('group_followers',array('group_followers_group_id' => $groupid));
        return $res->num_rows();
    }

    // block group by id
    function blockgroup($groupid = null , $flag = null){

        $this->db->where('group_id',$groupid);
        $this->db->update('group_table',array('group_active' => $flag));
        return $this->db->affected_rows();
    }

    // get group posts
    function viewPosts($id = null){

        $this->db->select('users_name,group_posts.*');
        $this->db->where('group_posts_group_id',$id);
        $this->db->where('group_posts_active',1);
        $this->db->join("users","users.user_id = group_posts.group_posts_posted_by");
        $res = $this->db->get('group_posts');
        return $res->result_array();
    }

     //block posts by id 
     function blockposts($id = null , $flag = null){

        $update = array('group_posts_active' => $flag);
        $this->db->where(array('group_posts_id' => $id));
        $this->db->update('group_posts',$update);
        return $this->db->affected_rows();
    }

    // get groupmembers
    function viewMembers($id = null){
        $this->db->select('users_name,users_country,users_state,users_city,users_photo,users_mobile');
        $this->db->where('group_followers_group_id',$id);
        $this->db->join("users","users.user_id = group_followers.group_followers_user_id");
        $res = $this->db->get('group_followers');
        return $res->result_array();
    }


    // post details by id
    function getgroupPostsdetails($id = null){

        $this->db->select('users_name,user_id,users_photo,group_posts.*');
        $this->db->from('group_posts');
        $this->db->where(array('group_posts_id'=>$id));
        $this->db->join("users","users.user_id=group_posts.group_posts_posted_by");
        $res    = $this->db->get();
        return $res->result_array();
    }
}