<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class HashTagModel extends CI_Model{
    
    //get all party list
    function getHashTags(){
        
        $this->db->from('posts_tags');
        $res = $this->db->get();
        return $res->result_array();
        
    }

    //block hashtag
    function blockHashTag($id = null, $flag = null){

        $this->db->where('posts_tags_id',$id);
        $this->db->update('posts_tags',array('posts_tags_status'=>$flag));
        return $this->db->affected_rows();
    }

    //get hashtag by id for edit
    function getHashTagbyId($id = null){

        $this->db->where('posts_tags_id',$id);
        $res = $this->db->get('posts_tags');
        return $res->result_array();
    }

    // update hash tag
    function updateHashTag($id = null,$name = null){
        $this->db->where('posts_tags_id',$id);
        $this->db->update('posts_tags',array('posts_tags_name'=>$name));
        return $this->db->affected_rows();
    }

    // check hashtag exists
    function checkHashTag($name = null){
        $res = $this->db->get_where('posts_tags',array('posts_tags_name' => $name));
        return $res->num_rows();
    }

    // party list for users section
     function getGrouplist(){
        
        $this->db->from('group');
        $res = $this->db->get();
        return $res->result_array();
        
    }

    //block posts by id 
    function blockgroupposts($id = null , $flag = null){

        $update = array('group_posts_active' => $flag);
        $this->db->where(array('group_posts_id' => $id));
        $this->db->update('group_posts',$update);
        return $this->db->affected_rows();
    }

  
    //get bloceked group posts
    function getblockedgroupPosts(){

        $this->db->join('group_table','group_table.group_id = group_posts.group_posts_group_id');
        $res = $this->db->get_where('group_posts',array('group_posts_active' => 0));
        return $res->result_array();
    }

    // get group members list
    function getGroupmembersCount($groupid = null){

        $res = $this->db->get_where('group_followers',array('group_followers_group_id' => $groupid));
        return $res->num_rows();
    }

    // block group by id
    function blockgroup($groupid = null , $flag = null){

        $this->db->where('group_id',$groupid);
        $this->db->update('group_table',array('group_active' => $flag));
        return $this->db->affected_rows();
    }

    // get group posts
    function viewPosts($id = null){

        $this->db->select('users_name,group_posts.*');
        $this->db->where('group_posts_group_id',$id);
        $this->db->where('group_posts_active',1);
        $this->db->join("users","users.user_id = group_posts.group_posts_posted_by");
        $res = $this->db->get('group_posts');
        return $res->result_array();
    }

     //block posts by id 
     function blockposts($id = null , $flag = null){

        $update = array('group_posts_active' => $flag);
        $this->db->where(array('group_posts_id' => $id));
        $this->db->update('group_posts',$update);
        return $this->db->affected_rows();
    }

    // get groupmembers
    function viewMembers($id = null){
        $this->db->select('users_name,users_country,users_state,users_city,users_photo,users_mobile');
        $this->db->where('group_followers_group_id',$id);
        $this->db->join("users","users.user_id = group_followers.group_followers_user_id");
        $res = $this->db->get('group_followers');
        return $res->result_array();
    }


    // post details by id
    function getgroupPostsdetails($id = null){

        $this->db->select('users_name,user_id,users_photo,group_posts.*');
        $this->db->from('group_posts');
        $this->db->where(array('group_posts_id'=>$id));
        $this->db->join("users","users.user_id=group_posts.group_posts_posted_by");
        $res    = $this->db->get();
        return $res->result_array();
    }
}