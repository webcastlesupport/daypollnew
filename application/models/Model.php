<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Model extends CI_Model{
    
 
    function fetch_from_table($table = null , $where = null){

        if(!empty($where))
            $this->db->where($where);

        return $this->db->get($table);
    }

    function insert_table($table = null , $data = null){

       return $this->db->insert($table, $data);
        
    }

    function update_table($table = null , $where = null , $data = null){

        if(!empty($where))
            $this->db->where($where);
        
         $this->db->update($table,$data);
         print_r($this->db->last_query());exit;
       
    }

    function delete_table($table = null , $where = null){
        
        if (!empty($where)) 
            $this->db->where($where);
        
        $this->db->delete($table);
        return $this->db->affected_rows();
    }


    

     // get posts reports
     function getChallengereports($count = null){

        $this->db->where("challenge_report_count >=",$count);
        $this->db->from('business_profile_challenge');
        $this->db->join("users","users.user_id=business_profile_challenge.challenge_user_id");
        $this->db->join("business_profile","business_profile.bf_created_user=business_profile_challenge.challenge_user_id");
        $res    = $this->db->get();
        return $res->result_array();
    
    }


    // get posts reports
    function getPostreports($count = null){

        $this->db->where("bp_posts_report_count >=",$count);
        $this->db->from('business_profile_posts');
        $this->db->join("users","users.user_id=business_profile_posts.bp_posts_user_id");
        $res    = $this->db->get();
        return $res->result_array();
    
    }

        //get all party list
        function getallChallenge($type = null){
        
            $this->db->select('business_profile_challenge.*,users.users_name,business_profile.*');
            if(!empty($type)){
              $this->db->where('challenge_type',$type);
            }
            $this->db->from('business_profile_challenge');
            $this->db->order_by('challenge_id','desc');
            $this->db->join("users","users.user_id = business_profile_challenge.challenge_user_id");
            $this->db->join("business_profile","business_profile.bf_created_user=business_profile_challenge.challenge_user_id");
            $res = $this->db->get();
            return $res->result_array();
            
        }

    // block challenge by id
    function verifychallenge($cid = null , $flag = null){

      $this->db->where('challenge_id',$cid);
      $this->db->update('business_profile_challenge',array('challenge_isverified' => $flag));
      return $this->db->affected_rows();
    }

    
  // get challenge details by id
  function getchallengedetails($id = null){

    $this->db->where('challenge_id',$id);
    $res = $this->db->get('business_profile_challenge');
    return $res->result_array();    
  }

  // get challenge poll count
  function getChallengepollCount($cid = null){

    $this->db->where('bp_challenge_polls_challenge_id',$cid);
    return $this->db->get('business_profile_challenge_polls')->num_rows();

  }

  // get challenge winner
  function getWinner($cid = null){

    $this->db->select(array('users.users_name','users.users_mobile','users.users_country','users.users_state','users.users_city','users.users_photo','users.users_login_type','business_profile_challenge_posts.*','bp_challenge_polls_challenge_post_id','COUNT(bp_challenge_polls_challenge_post_id) as total'));
    $this->db->where('bp_challenge_polls_challenge_id',$cid);
    $this->db->group_by('bp_challenge_polls_challenge_post_id');
    $this->db->order_by('total','desc');
    $this->db->limit(1);
    $this->db->join('users','users.user_id = business_profile_challenge_post_polls.bp_challenge_post_polled_by');
    $this->db->join('business_profile_challenge_posts','business_profile_challenge_posts.bp_challenge_posts_id = business_profile_challenge_post_polls.bp_challenge_polls_challenge_post_id');
    $res = $this->db->get('business_profile_challenge_post_polls');
    return $res->result_array();
}
  

function getUsername($postid = null){

  $this->db->select('users_name,users_photo');
  $this->db->where('bp_challenge_posts_id' , $postid);
  $this->db->join('users','users.user_id = business_profile_challenge_posts.bp_challenge_posts_user_id');
  $res = $this->db->get('business_profile_challenge_posts');
  $return['username'] =  $res->row('users_name');
  $return['photo']    =  $res->row('users_photo');
  return $return;
}

function viewMembers($id = null){
     
    $this->db->select('user_id,users_name,users_country,users_state,users_city,users_photo,users_mobile');
    $this->db->where('bp_challenge_posts_challenge_id',$id);
    $this->db->join("users","users.user_id = business_profile_challenge_posts.bp_challenge_posts_user_id");
    $res = $this->db->get('business_profile_challenge_posts');
    return $res->result_array();
  }

    
}