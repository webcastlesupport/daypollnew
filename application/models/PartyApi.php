<?php

class PartyApi extends CI_Controller{

    function __construct()
    {
        error_reporting(0);
        parent::__construct();
        $this->load->model('PartyApiModel');
        $this->load->library('form_validation');
    }

    // create party 
    function createParty(){

        $this->output->set_content_type('application/json');
        
        
        $userid     = $this->input->post('userid');
        $party_name = $this->input->post('partyname');
        $president  = $this->input->post('president');
        $secretary  = $this->input->post('secretary');
        $email      = $this->input->post('email');
        $scope      = $this->input->post('scope');
        // $mobile     = $this->input->post('mobile');
        // $password   = $this->input->post('password');
        // $privacy    = $this->input->post('privacy');
        $country    = $this->input->post('country');
        $state      = $this->input->post('state');
        $city       = $this->input->post('city');
        $area       = $this->input->post('area');
        
        $this->form_validation->set_rules("userid","userid","required");
        $this->form_validation->set_rules("partyname","partyname","required");
        $this->form_validation->set_rules("president","president","required");
        $this->form_validation->set_rules("secretary","secretary","required");
        $this->form_validation->set_rules("email","email","required");
        $this->form_validation->set_rules("scope","scope","required");
        // $this->form_validation->set_rules("mobile","mobile","required");
        // $this->form_validation->set_rules("password","password","required");
        //$this->form_validation->set_rules("privacy","privacy","required");
        $this->form_validation->set_rules("country","country","required");
        $this->form_validation->set_rules("state","state","required");
        $this->form_validation->set_rules("city","city","required");
        $this->form_validation->set_rules("area","area","required");
        
        if($this->form_validation->run()==TRUE){

            $insert = array('party_name' => $party_name ,'party_email' => $email ,'party_president' => $president,
                            'party_secretary' => $secretary , 'party_created_by' => $userid ,'party_country' => $country,
                            'party_state' => $state ,'party_city' => $city ,'party_area' => $area ,'party_scope' => $scope
                            );
            $res = $this->PartyApiModel->createParty($insert);

            if($res > 0){

                $getname = $this->PartyApiModel->getpartynameafterCreate($res);
                $this->output->set_output(json_encode(array('message'=>'successfully inserted','party_id' => $res,'party_name' => $getname,'status'=>true)));
            }
            else{

                $this->output->set_output(json_encode(array('message'=>'failed to insert','status'=>false)));
            }


        }
        else{

                $this->output->set_output(json_encode(array('message'=>'parameters missing','status'=>'false')));
        } 
       
        
    }



     // edit party 
     function editParty(){

        $secretary_pic = "";
        $president_pic = "";
        $profile_pic   = "";
        $cover_pic     = ""; 

        $update = array();

        $this->output->set_content_type('application/json');
        
        
        $partyid    = $this->input->post('partyid');
        $president  = $this->input->post('president');
        $secretary  = $this->input->post('secretary');
        $desc       = $this->input->post('description'); 
       
        $this->form_validation->set_rules("partyid","partyid","required");
        $this->form_validation->set_rules("president","president","required");
        $this->form_validation->set_rules("secretary","secretary","required");
        
        
        if($this->form_validation->run() == TRUE){

            $getPhoto = $this->PartyApiModel->geteditprofilepic($partyid);


            $update = array(
                            'party_president'       => $president,
                            'party_secretary'       => $secretary,
                            'party_description'     => $desc
                            );

            if(isset($_FILES['party_profile_image'])!=null){

                  
                //   profile pic upload
                    
                        $errors= array();
                        $file_name = $_FILES['party_profile_image']['name'];
                        $file_size = $_FILES['party_profile_image']['size'];
                        $file_tmp  = $_FILES['party_profile_image']['tmp_name'];
                        $file_type = $_FILES['party_profile_image']['type'];
                        
                        $tmp      = explode('.', $file_name);
                        $file_ext = end($tmp);
    
                  
                           
                        
                        if($getPhoto['profile'] != $file_name){
                          
                        
                       
                        $expensions  = array("jpeg","jpg","png");
                        
                        if(in_array($file_ext,$expensions) === false){
                           $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
                        }
                        
                        
                        
                        $path = "./assets/images/party/".$getPhoto['profile'];
                        
                        $option=false;
                        if($getPhoto['profile'] != "none.png")
                        {
                           
                         $option=true;
                         unlink($path);
                        }
                        
                        
                        if(empty($errors)==true){

                            
                            $profile_pic = 'party-'.mt_rand(10000,99999).date('h-i-s').'.'.$file_ext;
                            move_uploaded_file($file_tmp,"assets/images/party/".$profile_pic);
                          
                        
                            $update['party_photo'] = $profile_pic;
                            
                          
                        }
              
                    }
                  
                
            }
    
                // profile pic ends here     





                if(isset($_FILES['party_cover_image'])!=null){

                    
                       
                    //   cover pic uploaddd
                        
                            $errors= array();
                            $file_name = $_FILES['party_cover_image']['name'];
                            $file_size = $_FILES['party_cover_image']['size'];
                            $file_tmp  = $_FILES['party_cover_image']['tmp_name'];
                            $file_type = $_FILES['party_cover_image']['type'];
                            
                            $tmp      = explode('.', $file_name);
                            $file_ext = end($tmp);
        
                      
                            
                            if($getPhoto['cover'] != $file_name){
                              
                           
                            $expensions  = array("jpeg","jpg","png");
                            
                            if(in_array($file_ext,$expensions) === false){
                               $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
                            }
                            
                            
                            
                            $path="./assets/images/party/cover/".$getPhoto['cover'];
                          
                            $option=false;
                            if($getPhoto['cover'] != "cover-none.png")
                            {
                             $option=true;
                             unlink($path);
                            }
                            
                            
                            if(empty($errors)==true){
                               
                                $cover_pic = 'cover-'.mt_rand(10000,99999).date('h-i-s').'.'.$file_ext;
                               move_uploaded_file($file_tmp,"assets/images/party/cover/".$cover_pic);
                               $update['party_cover_photo'] = $cover_pic;
                            }
                  
                        }
                       
                }
        
                    // cover pic ends here     



                    if(isset($_FILES['party_president_image'])){

                    
                       
                        //   president pic uploaddd
                            
                                $errors= array();
                                $file_name = $_FILES['party_president_image']['name'];
                                $file_size = $_FILES['party_president_image']['size'];
                                $file_tmp  = $_FILES['party_president_image']['tmp_name'];
                                $file_type = $_FILES['party_president_image']['type'];
                                
                                $tmp      = explode('.', $file_name);
                                $file_ext = end($tmp);
            
                          
                                
                                if($getPhoto['president_photo'] != $file_name){
                                  
                               
                                $expensions  = array("jpeg","jpg","png");
                                
                                if(in_array($file_ext,$expensions) === false){
                                   $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
                                }
                                
                                
                                
                                $path = "./assets/images/party/president/".$getPhoto['president_photo'];
                              
                                $option=false;
                                if($getPhoto['president_photo'] != "none.png")
                                {
                                 $option=true;
                                 unlink($path);
                                }
                                
                                
                                if(empty($errors)==true){
                                    $president_pic = 'president-'.mt_rand(10000,99999).date('h-i-s').'.'.$file_ext;
                                
                                   move_uploaded_file($file_tmp,"assets/images/party/president/".$president_pic);
                                   $update['party_president_photo'] = $president_pic;
                              
                            

                                }
                      
                            }
                            
                        
                    }
            
                        // president pic ends here   





                        if(isset($_FILES['party_secretary_image'])!=null){

                    
                       
                            //   secretary pic uploaddd
                                
                                    $errors= array();
                                    $file_name = $_FILES['party_secretary_image']['name'];
                                    $file_size = $_FILES['party_secretary_image']['size'];
                                    $file_tmp  = $_FILES['party_secretary_image']['tmp_name'];
                                    $file_type = $_FILES['party_secretary_image']['type'];
                                    
                                    $tmp      = explode('.', $file_name);
                                    $file_ext = end($tmp);
                
                               
                                    
                                    if($getPhoto['secretary_photo'] != $file_name){
                                      
                                    
                                    $expensions  = array("jpeg","jpg","png");
                                    
                                    if(in_array($file_ext,$expensions) === false){
                                       $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
                                    }
                                    
                                    
                                    
                                    $path = "./assets/images/party/secretary/".$getPhoto['secretary_photo'];
                                  
                                    $option=false;
                                    if(file_exists($path) && $getPhoto['secretary_photo'] != "none.png")
                                    {
                                     $option=true;
                                     unlink($path);
                                    }
                                    
                                    
                                    if(empty($errors)==true){
                                        $secretary_pic = 'secretary-'.mt_rand(10000,99999).date('h-i-s').'.'.$file_ext;
                                    
                                        move_uploaded_file($file_tmp,"assets/images/party/secretary/".$secretary_pic);
                                        $update['party_secretary_photo'] = $secretary_pic;
                                    }
                          
                                }
                                
                            
                        }
                
                            // profile pic ends here   
        


            $res = $this->PartyApiModel->editParty($partyid,$update);

            if($res > 0){

                $this->output->set_output(json_encode(array('message'=>'successfully updated','status'=>true)));
            }
            else{

                $this->output->set_output(json_encode(array('message'=>'failed to update','status'=>false)));
            }


        }
        else{

                $this->output->set_output(json_encode(array('message'=>'parameters missing','status'=>'false')));
        }
       
        
    }

    // search party
    function searchParty(){

        $this->output->set_content_type('application/json');
        $search = $this->input->post('search');

        $res = $this->PartyApiModel->searchParty($search);

        if($res > 0){
            foreach($res as $key => $value){

                $res[$key]['poll_count'] = $this->PartyApiModel->partyPollCount($res[$key]['party_id']);
            }
            $this->output->set_output(json_encode(array('message'=>$res,'status'=>true)));
        }
        else{
            $this->output->set_output(json_encode(array('message'=>'no values found','status'=>false)));
        }
     
    }

   
    // search party posts api
    function searchPartyPosts(){                                                          

        $party_error = "true";
        $posts_error = "true";
        $this->output->set_content_type('application/json');

        $search     = $this->input->post('search');
        $userid     = $this->input->post('userid');
        //$res        = $this->PartyApiModel->searchPartyPosts($search);
        $res_party  = $this->PartyApiModel->getallParty($search);
        
        foreach($res_party as $keyss => $vals){
            
            $res_party[$keyss]['poll_count'] = $this->PartyApiModel->getPartypollcountbyid($res_party[$keyss]['party_id']);
            $ispolled = $this->PartyApiModel->get_party_poll_user($userid);
            $res_party[$keyss]['poll_count_public']   = $this->PartyApiModel->getPartypubliccountbyid($res_party[$keyss]['party_id']);
            $res_party[$keyss]['poll_count_secret']   = $this->PartyApiModel->getPartysecretcountbyid($res_party[$keyss]['party_id']);
           

        if($ispolled['poll'] == $res_party[$keyss]['party_id']){
            $res_party[$keyss]['isPolled'] = 1;
        }    
        else{
            $res_party[$keyss]['isPolled'] = 0;
        }
    }

   

        // foreach($res as $row){
        //     $res_party[] = $row;
        // }


        
       

        if(empty($res_party)){

            $posts_error = "false";
        }
        else{

           
        
        foreach ($res_party as $key => $value){
            
            if(isset($res_party[$key]['party_posts_id'])){

                //$pollcount            = $this->PartyApiModel->pollsCount($res_party[$key]['party_posts_id']);
                //$commentcount         = $this->PartyApiModel->commentsCount($res_party[$key]['party_posts_id']);
                //$commentsCountouter   = $this->PartyApiModel->commentsCountouter($res_party[$key]['party_posts_id']);
                                                    
                //$token                                        = mt_rand(100000,999999);
                //$res_party[$key]['party_posts_photo_video']   = base_url().'assets/images/partyposts/'.$res_party[$key]['party_posts_photo_video'];
                //$res_party[$key]['poll_count']                = $pollcount['count'];
                //$res_party[$key]['comment_count']             = $commentsCountouter;
                //$res_party[$key]['users_photo']               = base_url().'assets/images/users/'.$res_party[$key]['users_photo'];
                //$res_party[$key]['sharecode']                 = base_url().'post-view?token='.$token.'&&postid='.base64_encode($res_party[$key]['party_posts_id']);
                //$res_party[$key]['downloadlink']              = $res_party[$key]['party_posts_photo_video'];
                //$res_party[$key]['type']                      = 1;

            }
            else{

                $res_party[$key]['party_photo']        = base_url().'assets/images/party/'.$res_party[$key]['party_photo'];
                //$res_party[$key]['party_cover_photo']  = base_url().'assets/images/party/'.$res_party[$key]['party_cover_photo'];
                $res_party[$key]['type']               = 0;

            }

        }
    }
    if($res_party != 0){
        $this->output->set_output(json_encode(array('posts'=>$res_party , 'status' => true)));
    }
    else{
        $this->output->set_output(json_encode(array('posts'=>'no party found' , 'status' => false)));
    }
    

    }

    //party list
    function getPartylist(){

        $this->output->set_content_type('application/json');

        $res = $this->PartyApiModel->getPartylist();
       
        
        if($res > 0){
            
        foreach($res as $key => $value){
            
            $res[$key]['party_photo']       = base_url().'assets/images/party/'.$res[$key]['party_photo'];
            //$res[$key]['party_cover_photo'] = base_url().'assets/images/party/'.$res[$key]['party_cover_photo'];
        }    
        $this->output->set_output(json_encode(array('chart' => $res , 'status' => 'true')));
        
        }
        else{

        $this->output->set_output(json_encode(array('value' => 'no values found' , 'status' => 'false')));
       
        }
    
    }

   

  
    function getallParty(){
        
        $this->output->set_content_type('application/json');
        $res = $this->PartyApiModel->getallParty();
        foreach($res as $key => $value){

            $res[$key]['poll_count'] = $this->PartyApiModel->getPartypollcountbyid($res[$key]['party_id']); 
    
        }
        $this->output->set_output(json_encode(['value' => $res  , 'status' => 'true']));
    }

    //get public secret count
    function getPartyPollcountbyid(){

        $this->output->set_content_type('application/json');
        $partyid = $this->input->post('partyid');
        
        if(!empty($partyid)){
        $res_public = $this->PartyApiModel->getPartypubliccountbyid($partyid);
        $res_secret = $this->PartyApiModel->getPartysecretcountbyid($partyid);
        $this->output->set_output(json_encode(['public' => $res_public  ,'private' => $res_secret ,'status' => 'true']));
        }
        else{
            $this->output->set_output(json_encode(['msg' => 'parameter missing'  , 'status' => 'false']));
        }
    } 

   // upload party posts
    
   function uploadpartyPosts(){                                                             

    $this->output->set_content_type('application/json');

    $userid         = $this->input->post('userid');
    $posttitle      = $this->input->post('title');
    $photo_or_video = $this->input->post('photo_or_video');         // photo or video  [photo -> 0 , video -> 1 , -1 -> other]
    $posts_content  = $this->input->post('content');
    $partyid        = $this->input->post('partyid');  
    $video_thumb    = $this->input->post('video_thumbnail'); 

    $this->form_validation->set_rules("userid","userid","required");
    //$this->form_validation->set_rules("title","title","required");
    $this->form_validation->set_rules("photo_or_video","photo_or_video","required");
    //$this->form_validation->set_rules("content","content","required");
    $this->form_validation->set_rules("partyid","partyid","required");

    
    if($this->form_validation->run() == TRUE){


        if($photo_or_video == -1){

            $insert = array('party_posts_posted_by'=>$userid,'photo_or_video'=>$photo_or_video,
                            'party_posts_title'=>$posttitle,
                            'party_posts_content'=>$posts_content,'party_posts_party_id'=>$partyid);

            $result = $this->PartyApiModel->uploadpartyPost($insert);


            if($result > 0) {

                $this->output->set_output(json_encode(['message' => "successfully uploaded ", 'status' => "true"]));
            }
            else{
                $this->output->set_output(json_encode(['message' => "operation failed!..", 'status' => "false"]));
            }
        }
        else{

        $this->load->helper('form');

        $config['upload_path']      = "./assets/images/partyposts/";
        $config['allowed_types']    = '*';
        $config['file_name']        = 'partyposts'.mt_rand(100000,999999).date('H-i-s');
        //$config['max_height']     = '250';
        //$config['max_width']      = '250';
        //$config['max_size']       = '100kb';
        $config["overwrite"]        = false;

        $this->load->library('upload',$config);
        if(!$this->upload->do_upload("photo_video"))
        {
            $error = array('error' => $this->upload->display_errors());

            $this->output->set_output(json_encode(['message' => "file is not specified ", 'status'=>'false']));
            return false;
        }
        else
        {
            $data   = $this->upload->data();

            $insert = array('party_posts_posted_by'=>$userid,'photo_or_video'=>$photo_or_video,
                            'party_posts_title'=>$posttitle,'party_posts_photo_video'=>$data['file_name'],
                            'party_posts_content'=>$posts_content,'party_posts_party_id'=>$partyid,'party_posts_video_thumbnail' => $video_thumb);

            $result = $this->PartyApiModel->uploadpartyPost($insert);


            if($result > 0) {

                $this->output->set_output(json_encode(['message' => "successfully uploaded ", 'status' => "true"]));
            }
            else{
                $this->output->set_output(json_encode(['message' => "operation failed!..", 'status' => "false"]));
            }
        }

        }
    }
    else{
        $this->output->set_output(json_encode(['message'=>'parameter missing','status'=>'false']));
    }

}

    // get party profil`e
    function getPartyProfile(){

        $this->output->set_content_type('application/json');

        $zone = getallheaders();
        $GMT  = new DateTimeZone("GMT");
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";


        $partyid        = $this->input->post('partyid');
        $userid         = $this->input->post('userid');

        if(!empty($partyid)){

        // get party rank
        
        $party_rank     = $this->PartyApiModel->getpartyrank($partyid);
        $rank_value     = 0;    
       
        
        $rank = 1;
        foreach($party_rank as $r => $k){
            if($party_rank[$r]['users_poll_party_id'] == $partyid){
            $rank_value = $rank;
            }
           
            $rank++;
        }
       
        $polledparty    = $this->PartyApiModel->getPolledparty($userid);
        
        $getjoin        = $this->PartyApiModel->getjoinpartyname($polledparty['join']);
        $getpoll        = $this->PartyApiModel->getpollpartyname($polledparty['poll']);

        

        $isjoin         = $polledparty['join'] == $partyid  ? 1 :0;
        $ispolled       = $polledparty['poll'] == $partyid  ? 1 :0;

        $res            = $this->PartyApiModel->getPartyProfile($partyid);
       
        if($res != 0){
        $members_count      = $this->PartyApiModel->getPartyJoinCount($partyid);
        //$members            = $this->PartyApiModel->getprofilePartyJoinmembers($partyid);
        //$party_poll_members = $this->PartyApiModel->getprofilePartyJoinmembersbypoll($partyid);
        $members            = $this->PartyApiModel->getprofilePartyJoinmembersbypoll($partyid);
        $members_joined     = $this->PartyApiModel->getprofilePartyJoinmembers($partyid);
        
        
       
        foreach($members as $mems => $mvalue){
            $members[$mems]['users_photo'] = base_url()."assets/images/users/".$members[$mems]['users_photo'];
            $members[$mems]['is_poll'] = 1;
      
        }

        foreach($members_joined as $memss => $mvalues){
            $members_joined[$memss]['users_photo'] = base_url()."assets/images/users/".$members_joined[$memss]['users_photo'];
            $members_joined[$memss]['is_poll'] = 0;
      
        }

       

        foreach($members_joined as $row){

            $members[] =  $row;
        }

       
       

        $poll_count     = $this->PartyApiModel->getPartyPollCount($partyid);
        //$comment_count  = $this->PartyApiModel->getCommentCount($partyid);
        $party_posts    = $this->PartyApiModel->getPartyPosts($partyid);
        
       

        foreach($party_posts as $keys => $values){

            $party_posts[$keys]['users_photo']              = base_url()."assets/images/users/".$party_posts[$keys]['users_photo'];
            $date = new DateTime($party_posts[$keys]['party_posts_uploaded_date'], $GMT );
            $date->setTimezone(new DateTimeZone($zone['timezone']));
            $party_posts[$keys]['party_posts_uploaded_date'] = $date->format('Y-m-d H:i:s');
            
            $party_posts[$keys]['party_posts_photo_video']   = base_url()."assets/images/partyposts/".$party_posts[$keys]['party_posts_photo_video'];
            $party_posts[$keys]['posts_poll_count']          = $this->PartyApiModel->partypollsCount($party_posts[$keys]['party_posts_id']);
            
            $party_posts[$keys]['posts_comment_count']       = $this->PartyApiModel->partypostscommentsCount($party_posts[$keys]['party_posts_id']);
            $party_posts[$keys]['downloadlink']              = $party_posts[$keys]['party_posts_photo_video'];
        }

       
        foreach($res as $key => $value){

            $res[$key]['party_photo']                 = base_url()."assets/images/party/".$res[$key]['party_photo'];
            $res[$key]['party_cover_photo']           = base_url()."assets/images/party/".$res[$key]['party_cover_photo'];
            $res[$key]['party_president_photo']       = base_url()."assets/images/party/president/".$res[$key]['party_president_photo'];  
            $res[$key]['party_secretary_photo']       = base_url()."assets/images/party/secretary/".$res[$key]['party_secretary_photo'];     
        }
        $this->output->set_output(json_encode(['profile'=>$res,'posts'=>$party_posts,'members' => $members,'members_count' => $members_count,'poll_count' => $poll_count,'party_rank' => $rank_value,'partyjoin' => $getjoin ,'partypoll' => $getpoll,'isjoined' => $isjoin,'ispolled' => $ispolled,'status'=>'true']));
    }

        else{
            
        $this->output->set_output(json_encode(['status'=> false , 'message' => 'no value found'])); 
    }
   
    }
    else{
        $this->output->set_output(json_encode(['status'=> false , 'message' => 'parameter missing'])); 
    }
    }

    // get party join members
    function getPartyJoinmembers(){

        $this->output->set_content_type('application/json');

        $partyid    = $this->input->post('partyid');
        $followerid = $this->input->post('userid');
        $scope      = $this->input->post('scope');
        
        if(!empty($partyid)){
        
            //$members_by_polls   = $this->PartyApiModel->getprofilePartyJoinmembersbypollnolimit($partyid);
           
           


        switch($scope){
            case 1:
            $res                = $this->PartyApiModel->getPartyJoinmemberslist($partyid , 'users_party_id');
            break;

            case 2:
            $res                = $this->PartyApiModel->getPartyJoinmemberslist($partyid , 'users_party_state_id');
            break;

            case 2:
            $res                = $this->PartyApiModel->getPartyJoinmemberslist($partyid,'users_party_district_id');
            break;
        }
        
        
       

        // if($members_by_polls){

        //     foreach($members_by_polls as $row){

        //         $res[] =  $row;
        //     }
        // }
        
        
        if($res != 0){
            
        foreach($res as $key => $value){
            $res[$key]['poll_count']        = $this->PartyApiModel->Joinmembersbypollnolimit($res[$key]['user_id'] , $partyid);
            $is_polled_user                 = $this->PartyApiModel->is_polled_user($followerid , $res[$key]['user_id'] , $partyid); 

            $res[$key]['is_Polled']         = $is_polled_user > 0 ? 1 :0;
            $res[$key]['users_photo']       = base_url().'assets/images/users/'.$res[$key]['users_photo'];
            //$res[$key]['poll_count']        = $this->PartyApiModel->getpartyUserpollcount($res[$key]['user_id']);
        }    
        $this->output->set_output(json_encode(array('value' => $res , 'status' => 'true')));
        
        }
        else{

        $this->output->set_output(json_encode(array('value' => 'no values found' , 'status' => 'false')));


        }
    }
    else{
        $this->output->set_output(json_encode(array('value' => 'parameter missing' , 'status' => 'false')));
        
    }
    }

    // get party users followers
    function pollPartyusers(){

        $this->output->set_content_type('application/json');
        
        $followerid  = $this->input->post('followerid');
        $userid      = $this->input->post('userid');
        $partyid     = $this->input->post('partyid');

        if(!empty($followerid && $userid && $partyid)){

        $res = $this->PartyApiModel->pollPartyusers($userid , $followerid , $partyid);
        
        switch($res){
            
            case 1:
            $this->output->set_output(json_encode(array('message' => 'Successfully polled' , 'status' => 'true')));
            break;

            case -1:
            $this->output->set_output(json_encode(array('message' => 'Already polled' , 'status' => 'false')));
            break;

            case 0:
            $this->output->set_output(json_encode(array('message' => 'Failed to poll' , 'status' => 'false')));
            break;
                
        }
            }
            else{

            $this->output->set_output(json_encode(array('message' => 'parameter missing' , 'status' => 'false')));
            
        }
     
    }

    // confirm party user poll
    function confirmPartyuserpoll(){

        $this->output->set_content_type('application/json');

        $followerid  = $this->input->post('followerid');
        $userid      = $this->input->post('userid');
        $partyid     = $this->input->post('partyid');

        if(!empty($followerid && $userid && $partyid)){

            $res = $this->PartyApiModel->confirmpollPartyusers($userid , $followerid , $partyid);
            
            switch($res){
                
                case 1:
                $this->output->set_output(json_encode(array('message' => 'Successfully polled' , 'status' => 'true')));
                break;
    
                    
                case 0:
                $this->output->set_output(json_encode(array('message' => 'Failed to poll' , 'status' => 'false')));
                break;
                    
            }
                }
                else{
    
                $this->output->set_output(json_encode(array('message' => 'parameter missing' , 'status' => 'false')));
                
            }

          }

          // remove poll party user
          function unpollPartyuser(){

            $this->output->set_content_type('application/json');

            $followerid  = $this->input->post('followerid');
            $userid      = $this->input->post('userid');
            $partyid     = $this->input->post('partyid');
    
            if(!empty($followerid)){
    
                $res = $this->PartyApiModel->removePartyPoll($followerid);
                
                switch($res){
                    
                    case 1:
                    $this->output->set_output(json_encode(array('message' => 'Successfully unpolled' , 'status' => 'true')));
                    break;
        
                        
                    case 0:
                    $this->output->set_output(json_encode(array('message' => 'Failed to unpoll' , 'status' => 'false')));
                    break;
                        
                }
            }
                    else{
        
                    $this->output->set_output(json_encode(array('message' => 'parameter missing' , 'status' => 'false')));
                    
                }
    
          }


    
     // get party chart
     function getchart(){

        $total  =  0;
        $this->output->set_content_type('application/json');

        $userid         = $this->input->post('userid');

        $userlocation   = $this->PartyApiModel->getUserlocation($userid);
        
        $scope          = $this->input->post('scope');

        $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;   


         switch($scope){

            case 1:
            $res         = $this->PartyApiModel->getPartysum($scope ,$country , $state , $district , $area ,'users_poll_party_id');
            break;

            case 2:
            $res         = $this->PartyApiModel->getPartysum($scope ,$country , $state , $district , $area ,'users_poll_party_state_id');
            break;

            case 3:
            $res         = $this->PartyApiModel->getPartysum($scope ,$country , $state , $district , $area ,'users_party_district_poll_id');
            break;



        }
        
       
        $res_party   = $this->PartyApiModel->getPartylist($scope , $country , $state , $district , $area);
      
          
        if($res_party){
        foreach($res_party as $keys => $values){
            
            
            switch($scope){

                case 1:
                $res_get_count         = $this->PartyApiModel->getPartypollsumbyid($res_party[$keys]['party_id'],'users_poll_party_id');
                break;
    
                case 2:
                $res_get_count         = $this->PartyApiModel->getPartypollsumbyid($res_party[$keys]['party_id'] ,'users_poll_party_state_id');
                break;
    
                case 3:
                $res_get_count         = $this->PartyApiModel->getPartypollsumbyid($res_party[$keys]['party_id'] ,'users_party_district_poll_id');
                break;
    
    
    
            }

            switch($scope){

                case 1:
                $res_party[$keys]['poll_count_public']  = $this->PartyApiModel->getPartypubliccountbyid($res_party[$keys]['party_id'],'users_poll_party_id','users_party_poll_type');
           
                break;
    
                case 2:
                $res_party[$keys]['poll_count_public']  = $this->PartyApiModel->getPartypubliccountbyid($res_party[$keys]['party_id'],'users_poll_party_state_id','users_party_state_poll_type');
           
                break;
    
                case 3:
                $res_party[$keys]['poll_count_public']  = $this->PartyApiModel->getPartypubliccountbyid($res_party[$keys]['party_id'],'users_party_district_poll_id','users_party_district_poll_type');
                break;
    
            }

            switch($scope){

                case 1:
                $res_party[$keys]['poll_count_secret']  = $this->PartyApiModel->getPartysecretcountbyid($res_party[$keys]['party_id'],'users_poll_party_id','users_party_poll_type');

                break;
    
                case 2:
                $res_party[$keys]['poll_count_secret']  = $this->PartyApiModel->getPartysecretcountbyid($res_party[$keys]['party_id'],'users_poll_party_state_id','users_party_state_poll_type');

                break;
    
                case 3:
                $res_party[$keys]['poll_count_secret']  = $this->PartyApiModel->getPartysecretcountbyid($res_party[$keys]['party_id'],'users_party_district_poll_id','users_party_district_poll_type');

                break;
    
    
    
            }

            switch($scope){

                case 1:
                $res_party[$keys]['isLocation']  = $this->PartyApiModel->isLocation(array('party_id' => $res_party[$keys]['party_id'],'party_country' => $userlocation['country']));
                
                break;
    
                case 2:
                 $res_party[$keys]['isLocation']  = $this->PartyApiModel->isLocation(array('party_id' => $res_party[$keys]['party_id'],'party_state' => $userlocation['state']));

                break;
              
    
                case 3:
                $res_party[$keys]['isLocation']  = $this->PartyApiModel->isLocation(array('party_id' => $res_party[$keys]['party_id'],'party_city' => $userlocation['district']));

                break;
    
            }


           
            if($res_get_count == null) $res_party[$keys]['poll_count']  = 0;
            else $res_party[$keys]['poll_count'] = $res_get_count;

            $res_party[$keys]['party_photo']     = base_url().'assets/images/party/'.$res_party[$keys]['party_photo'];
            
            switch($scope){
            
                case 1:
            
                $ispolled = $this->PartyApiModel->get_party_poll_user($userid,'users_poll_party_id','users_party_id');
                break;
            
                case 2:
            
                $ispolled = $this->PartyApiModel->get_party_poll_user($userid,'users_poll_party_state_id','users_party_state_id');
                break;

                case 3:
            
                $ispolled = $this->PartyApiModel->get_party_poll_user($userid,'users_party_district_poll_id','users_party_district_id');
                break;
            }

            if($ispolled['poll'] == $res_party[$keys]['party_id']){
                $res_party[$keys]['isPolled'] = 1;
            }    
            else{
                $res_party[$keys]['isPolled'] = 0;
            }

            if($ispolled['join'] == $res_party[$keys]['party_id']){
                $res_party[$keys]['isJoined'] = 1;
            }    
            else{
                $res_party[$keys]['isJoined'] = 0;
            }
            
        }
    
        for($i=0;$i < sizeof($res);$i++){
            $total += $res[$i]['total'];
        }
        
       
        if($res[0]['total']!=0){
        $k = 0;
        for($i=0;$i<sizeof($res);$i++){

            
            $percent                = $res[$i]['total']*100/$total;
            $res[$i]['percentage']  = intval($percent);

           
            
            if($k <= 2){
              
                switch ($scope){
                    case 1:
                    $name                   = $this->PartyApiModel->getPartyname($res[$i]['users_poll_party_id']);
                    break;

                    case 2:
                    $name                   = $this->PartyApiModel->getPartyname($res[$i]['users_poll_party_state_id']);
                    break;

                    case 3:
                    $name                   = $this->PartyApiModel->getPartyname($res[$i]['users_party_district_poll_id']);
                    break;
                }
                
                $res[$i]['party_name']  = $name->party_name;
            }
            else{
                
                switch ($scope){
                    case 1:
                    $res[$i]['users_poll_party_id']  = '0';
                    break;

                    case 2:
                    $res[$i]['users_poll_party_state_id']  = '0';

                    case 3:
                    $res[$i]['users_party_district_poll_id'] = '0';
                }
                
                
                $res[$i]['party_name']           = 'others';
            }
            
            $k++;
        }
    }
    else{

        $res = [];
        
    }

    

    $this->output->set_output(json_encode(['chart' => $res  ,'party' => $res_party, 'status' => 'true']));
}
    else{
        $this->output->set_output(json_encode(['chart' => $res  ,'party' => array(), 'status' => 'false']));
    }
   
        
        
    }      


    // get party chart
    function getchartupdate(){

        $total=0;
        $this->output->set_content_type('application/json');

        $userid = $this->input->post('userid');
        $scope  = $this->input->post('scope');
        $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;   


        switch($scope){

            case 1:
            $res         = $this->PartyApiModel->getPartysum($scope ,$country , $state , $district , $area ,'users_poll_party_id');
            break;

            case 2:
            $res         = $this->PartyApiModel->getPartysum($scope ,$country , $state , $district , $area ,'users_poll_party_state_id');
            break;

            case 3:
            $res         = $this->PartyApiModel->getPartysum($scope ,$country , $state , $district , $area ,'users_party_district_poll_id');
            break;



        }
      
        
       
        for($i=0;$i < sizeof($res);$i++){
            $total += $res[$i]['total'];
        }
        
       
        if($res[0]['total']!=0){
        $k = 0;
        for($i=0;$i<sizeof($res);$i++){

            
            $percent                = $res[$i]['total']*100/$total;
            $res[$i]['percentage']  = intval($percent);

           
            
            if($k <= 2){
              
                switch ($scope){
                    case 1:
                    $name                   = $this->PartyApiModel->getPartyname($res[$i]['users_poll_party_id']);
                    break;

                    case 2:
                    $name                   = $this->PartyApiModel->getPartyname($res[$i]['users_poll_party_state_id']);
                    break;

                    case 3:
                    $name                   = $this->PartyApiModel->getPartyname($res[$i]['users_party_district_poll_id']);
                    break;
                }
                
                $res[$i]['party_name']  = $name->party_name;
            }
            else{
                
                switch ($scope){
                    case 1:
                    $res[$i]['users_poll_party_id']  = '0';
                    break;

                    case 2:
                    $res[$i]['users_poll_party_state_id']  = '0';

                    case 3:
                    $res[$i]['users_party_district_poll_id'] = '0';
                }
                
                
                $res[$i]['party_name']           = 'others';
            }
            
            $k++;
        }
    }
        else{

            //$res = [];
            
        }
        
        $this->output->set_output(json_encode(['chart' => $res,'status' => 'true']));
        
    }

    // claim your party
    function claimParty(){

        $this->output->set_content_type('application/json');

        $userid         = $this->input->post('userid');
        $partyid        = $this->input->post('partyid');
        $presidentname  = $this->input->post('presidentname');
        $secretaryname  = $this->input->post('secretaryname');
        $email          = $this->input->post('email');
        $address        = $this->input->post('address');
        $comment        = $this->input->post('comment');

        if(!empty($userid && $partyid)){

            if(isset($_FILES['claim_party_attachment'])){
                $errors= array();
                $file_name = $_FILES['claim_party_attachment']['name'];
                $file_size = $_FILES['claim_party_attachment']['size'];
                $file_tmp  = $_FILES['claim_party_attachment']['tmp_name'];
                $file_type = $_FILES['claim_party_attachment']['type'];
                $tmp       = explode('.', $file_name);
                $file_ext  = end($tmp);
                
                
                $rand_name_attachment = "attachments".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;
                
                $expensions= array("jpeg","jpg","png");
                
                if(in_array($file_ext,$expensions)=== false){
                   $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                }
                
                
                
                if(empty($errors)==true){
                   
                   move_uploaded_file($file_tmp,"assets/images/party/attachments/".$rand_name_attachment);
                   $attachment_photo = $rand_name_attachment;
                   
                }else{
                    $attachment_photo = "null";
                }


                
               
            }
            


            if(isset($_FILES['claim_party_president_image'])){
                $errors= array();
                $file_name = $_FILES['claim_party_president_image']['name'];
                $file_size =$_FILES['claim_party_president_image']['size'];
                $file_tmp =$_FILES['claim_party_president_image']['tmp_name'];
                $file_type=$_FILES['claim_party_president_image']['type'];
                $tmp = explode('.', $file_name);
                $file_ext = end($tmp);
                
                
                $rand_name_president = "president".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;
                
                $expensions= array("jpeg","jpg","png");
                
                if(in_array($file_ext,$expensions)=== false){
                   $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                }
                
                
                
                if(empty($errors)==true){
                   
                   move_uploaded_file($file_tmp,"assets/images/party/president/".$rand_name_president);
                   $president_photo= $rand_name_president;
                   
                }else{
                    $president_photo= "none.png";
                }
      
            }



            if(isset($_FILES['claim_party_secretary_image'])){
                $errors= array();
                $file_name = $_FILES['claim_party_secretary_image']['name'];
                $file_size =$_FILES['claim_party_secretary_image']['size'];
                $file_tmp =$_FILES['claim_party_secretary_image']['tmp_name'];
                $file_type=$_FILES['claim_party_secretary_image']['type'];
                $tmp = explode('.', $file_name);
                $file_ext = end($tmp);
                
                
                $rand_name_secretary = "secretary".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;
                
                $expensions= array("jpeg","jpg","png");
                
                if(in_array($file_ext,$expensions)=== false){
                   $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                }
                
                
                
                if(empty($errors)==true){
                   
                   move_uploaded_file($file_tmp,"assets/images/party/secretary/".$rand_name_secretary);
                   $secretary_photo = $rand_name_secretary;
                   
                }else{
                    $secretary_photo= "none.png";
                }
      
            }



        $insert = array('claim_party_party_id'          => $partyid,
                        'claim_party_user_id'           => $userid,
                        'claim_party_president_name'    => $presidentname,
                        'claim_party_president_image'   => $president_photo,
                        'claim_party_secretary_image'   => $secretary_photo,
                        'claim_party_attachment'        => $attachment_photo,
                        'claim_party_secretary_name'    => $secretaryname,
                        'claim_party_email'             => $email,
                        'claim_party_address'           => $address,
                        'claim_party_comment'           => $comment    
                        );

        $res = $this->PartyApiModel->claimParty($insert);
        
        if($res > 0){

            $this->output->set_output(json_encode(['message' => 'successfully inserted','status' => 'true']));

        }
        else{
            $this->output->set_output(json_encode(['message' => 'failed to insert','status' => 'false']));

        }
    
}
    else{
        $this->output->set_output(json_encode(['message' => 'userid and partyid required','status' => false ]));
    }
        
    }


    // party posts new api



    //poll posts
    function pollpartyPosts(){

        $this->output->set_content_type('application/json');
        
        $post_user_id    = $this->input->post('postuserid');
        $posts_id        = $this->input->post('postid');
        $user_id         = $this->input->post('userid');
        
        if(!empty($posts_id && $user_id)){
           
        $res        = $this->PartyApiModel->pollpartyPosts($user_id,$posts_id,$post_user_id);
        
        if($res['res'] == 1){
       
            $this->output->set_output(json_encode(array('is_polled'=>true,'message'=>"successfully polled",'status'=>true)));
        }
        
        else if($res['res'] == -1){

            $this->output->set_output(json_encode(array('is_polled'=>false,'message'=>'successfully unpolled','status'=>true)));
        
        }
        }
        else{
            $this->output->set_output(json_encode(array('message'=>"parameter missing",'status'=>false)));
        }
        
    }









    //comment party posts api
    function commentpartyPost(){

        $this->output->set_content_type('application/json');

        $post_id          = $this->input->post('post_id');
        $user_id          = $this->input->post('user_id');
        $post_user_id     = $this->input->post('postuserid');
        $comment          = $this->input->post('comment');
        
        if(!empty($post_id && $user_id && $comment)){

        $res              = $this->PartyApiModel->commentpartyPost($user_id , $post_id , $comment , $post_user_id);

        

        if($res > 0){

            $this->output->set_output(json_encode(array('message' => "successfully commented",'comment'=>$res['data'],'status'=>true)));
        }
        else{
            
            $this->output->set_output(json_encode(array('posts' => "failed to comment",'status'=>false)));
            }
        }
    else{
            $this->output->set_output(json_encode(array('posts' => "parameter missing",'status'=>false)));
    }
    }

    // get post comments
    function getpartyPostcomments(){ 

        $this->output->set_content_type('application/json');

        $post_id     = $this->input->post('post_id');
        
        $res         = $this->PartyApiModel->getpartyPostcomments($post_id);

        foreach($res as $key => $value){
            $res[$key]['users_photo']   = base_url()."assets/images/users/".$res[$key]['users_photo'];
            $res_reply                  = $this->PartyApiModel->getpartycommentsreply($res[$key]['party_posts_comments_id'],$res[$key]['party_posts_comments_post_id']);
            $res[$key]['reply']         = $res_reply;
        }

        if($res!=null){

            $this->output->set_output(json_encode(array('data' => $res,'status'=>true)));
        }
        else{
            $this->output->set_output(json_encode(array('data' => "no value found",'status'=>false)));
        }
    }

    // get post comments by id
    function getpartyPostcommentsbyid(){ 

        $this->output->set_content_type('application/json');

        $post_id     = $this->input->post('comment_id');
        
        $res         = $this->PartyApiModel->getpartyPostcommentsbyid($post_id);

        foreach($res as $key => $value){

            $res_reply          = $this->PartyApiModel->getpartycommentsreply($res[$key]['party_posts_comments_id'],$res[$key]['party_posts_comments_post_id']);
            $res[$key]['reply'] = $res_reply;
        }

        if($res!=null){

            $this->output->set_output(json_encode(array('data' => $res,'status'=>true)));
        }
        else{
            $this->output->set_output(json_encode(array('data' => "no value found",'status'=>false)));
        }
    }
    
    // reply to comments
    function replypartyPostscomments(){

        $this->output->set_content_type('application/json');

        $post_id     = $this->input->post('post_id');
        $user_id     = $this->input->post('user_id');
        $comment_id  = $this->input->post('comment_id');
        $reply       = $this->input->post('reply');
        
        $insert      = array('party_posts_comments_reply_posts_id'=>$post_id,
                        'party_posts_comments_reply_comment_id'=>$comment_id,
                        'party_posts_comments_reply'=>$reply,
                        'party_posts_comments_reply_user_id' => $user_id);

        $res         = $this->PartyApiModel->replypartyPostscomments($insert);

        if($res > 0){

            $this->output->set_output(json_encode(array('message' => "succesfully inserted",'status'=>true)));
        }
        else{

            $this->output->set_output(json_encode(array('posts' => "no value found",'status'=>false)));
        }
    }

    // get created party list
    function getcreatedParties(){

        $this->output->set_content_type('application/json');
        $id = $this->input->post('userid');
        if(!empty ($id)){

        $res_created = $this->PartyApiModel->getcreatedParties($id);
        $res_joined  = $this->PartyApiModel->getjoinedParties($id);
        
       
        foreach($res_created as $value => $key) {

            switch($res_created[$value]['party_scope']){
            
                case 1:
                $res_created[$value]['party_photo']       = base_url()."assets/images/party/".$res_created[$value]['party_photo'];
                $res_created[$value]['pollpublicCount']   = $this->PartyApiModel->publicCountpartycreated($res_created[$value]['party_id'],'users_poll_party_id','users_party_poll_type');
                $res_created[$value]['pollsecretCount']   = $this->PartyApiModel->secretCountpartycreated($res_created[$value]['party_id'],'users_poll_party_id','users_party_poll_type');
                break;  
                
                case 2:
                $res_created[$value]['party_photo']       = base_url()."assets/images/party/".$res_created[$value]['party_photo'];
                $res_created[$value]['pollpublicCount']   = $this->PartyApiModel->publicCountpartycreated($res_created[$value]['party_id'],'users_poll_party_state_id','users_party_state_poll_type');
                $res_created[$value]['pollsecretCount']   = $this->PartyApiModel->secretCountpartycreated($res_created[$value]['party_id'],'users_poll_party_state_id','users_party_state_poll_type');
                break; 

                case 3:
                $res_created[$value]['party_photo']       = base_url()."assets/images/party/".$res_created[$value]['party_photo'];
                $res_created[$value]['pollpublicCount']   = $this->PartyApiModel->publicCountpartycreated($res_created[$value]['party_id'],'users_party_district_poll_id','users_party_district_poll_type');
                $res_created[$value]['pollsecretCount']   = $this->PartyApiModel->secretCountpartycreated($res_created[$value]['party_id'],'users_party_district_poll_id','users_party_district_poll_type');
                break; 

                case null:
                $res_created[$value]['party_photo']       = base_url()."assets/images/party/".$res_created[$value]['party_photo'];
                $res_created[$value]['pollpublicCount']   = $this->PartyApiModel->publicCountpartycreated($res_created[$value]['party_id'],'users_party_district_poll_id','users_party_district_poll_type');
                $res_created[$value]['pollsecretCount']   = $this->PartyApiModel->secretCountpartycreated($res_created[$value]['party_id'],'users_party_district_poll_id','users_party_district_poll_type');
                break;
        }    
        }

        foreach($res_joined as $value => $key) {

            // $res_joined[$value]['party_photo']       = base_url()."assets/images/party/".$res_joined[$value]['party_photo'];
            // $res_joined[$value]['pollpublicCount']   = $this->PartyApiModel->publicCountpartycreated($res_created[$value]['party_id']);
            // $res_joined[$value]['pollsecretCount']   = $this->PartyApiModel->secretCountpartycreated($res_created[$value]['party_id']);
     
            switch($res_joined[$value]['party_scope']){
            
                case 1:
                $res_joined[$value]['party_photo']       = base_url()."assets/images/party/".$res_joined[$value]['party_photo'];
                $res_joined[$value]['pollpublicCount']   = $this->PartyApiModel->publicCountpartycreated($res_joined[$value]['party_id'],'users_poll_party_id','users_party_poll_type');
                $res_joined[$value]['pollsecretCount']   = $this->PartyApiModel->secretCountpartycreated($res_joined[$value]['party_id'],'users_poll_party_id','users_party_poll_type');
                break;  
                
                case 2:
                $res_joined[$value]['party_photo']       = base_url()."assets/images/party/".$res_joined[$value]['party_photo'];
                $res_joined[$value]['pollpublicCount']   = $this->PartyApiModel->publicCountpartycreated($res_joined[$value]['party_id'],'users_poll_party_state_id','users_party_state_poll_type');
                $res_joined[$value]['pollsecretCount']   = $this->PartyApiModel->secretCountpartycreated($res_joined[$value]['party_id'],'users_poll_party_state_id','users_party_state_poll_type');
                break; 

                case 3:
                $res_joined[$value]['party_photo']       = base_url()."assets/images/party/".$res_joined[$value]['party_photo'];
                $res_joined[$value]['pollpublicCount']   = $this->PartyApiModel->publicCountpartycreated($res_joined[$value]['party_id'],'users_party_district_poll_id','users_party_district_poll_type');
                $res_joined[$value]['pollsecretCount']   = $this->PartyApiModel->secretCountpartycreated($res_joined[$value]['party_id'],'users_party_district_poll_id','users_party_district_poll_type');
                break; 

                case null:
                $res_joined[$value]['party_photo']       = base_url()."assets/images/party/".$res_joined[$value]['party_photo'];
                $res_joined[$value]['pollpublicCount']   = $this->PartyApiModel->publicCountpartycreated($res_joined[$value]['party_id'],'users_party_district_poll_id','users_party_district_poll_type');
                $res_joined[$value]['pollsecretCount']   = $this->PartyApiModel->secretCountpartycreated($res_joined[$value]['party_id'],'users_party_district_poll_id','users_party_district_poll_type');
                break;
        }    






        }
        
            $this->output->set_output(json_encode(array('message' => 'value found','created_party' => $res_created,'joined_party' => $res_joined,'status'=>true)));
       
      
        }
        else{
            $this->output->set_output(json_encode(array('message' => "userid required",'status'=>false)));
       
        }


    }


    // get public polled members
    function getPolledmembers(){

        $this->output->set_content_type('application/json');
        $partyid = $this->input->post('partyid');
        $scope   = $this->input->post('scope');
      
        if(!empty ($partyid && $scope)){
            
            switch($scope){
                case 1:
                $res = $this->PartyApiModel->getPolledmembers($partyid , 'users_poll_party_id' , 'users_party_poll_type'); 
                break;

                case 2:
                $res = $this->PartyApiModel->getPolledmembers($partyid , 'users_poll_party_state_id' ,'users_party_state_poll_type');
                break;

                case 3:
                $res = $this->PartyApiModel->getPolledmembers($partyid , 'users_party_district_poll_id' , 'users_party_district_poll_type');
                break;

            }   

            if($res != null){

            
           foreach($res as  $key => $value){
               $res[$key]['users_photo'] = base_url()."assets/images/users/".$res[$key]['users_photo'];
               $res[$key]['pollCount']   = $this->PartyApiModel->getUserpollcount($res[$key]['user_id']);   
           }

           $this->output->set_output(json_encode(array('message' => "value found",'users' => $res,'status'=>true)));
       
        }
        else{
            $this->output->set_output(json_encode(array('message' => "no value found",'status'=>false)));
       
        }
        }
        else{
            $this->output->set_output(json_encode(array('message' => "parameter missing",'status'=>false)));
       
        }
    }
}



?>