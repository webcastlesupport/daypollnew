<?php

class PartyApiModel extends CI_Model{

    //create party
    function createParty($insert = null){

        $this->db->insert('party',$insert);
        return $this->db->insert_id();

    }

    // get name , poll count
    function getpartynameafterCreate($id = null){
        $this->db->select(array('party_id','party_name'));
        $res = $this->db->get_where("party",array('party_id' => $id));
        return  $res->row('party_name');
    }

     //edit party
     function editParty($partyid = null , $update = null){

        $this->db->where('party_id', $partyid);
        $this->db->update('party',$update);
        return $this->db->affected_rows();

    }

    //search party by name
    function searchParty($search = null){

        $this->db->select('party_id,party_name');
        $this->db->where('party_id !=',1);
        $this->db->like('party_name',$search);
        $res = $this->db->get('party');
        return ($res->num_rows() > 0) ? $res->result_array() : 0;

    }



     // get party posts api
     function searchPartyPosts($search = null){

        $this->db->select(array("party_posts.party_posts_id","party_posts.party_posts_uploaded_date","party_posts.party_posts_id","party_posts.party_posts_title","users.users_name","users.users_photo"));
        $this->db->like('party_posts_title',$search);
        $this->db->where(array('party_posts_active'=>1));
        $this->db->order_by("party_posts_id","DESC");
        $this->db->from("party_posts");
        $this->db->join("users","users.user_id=party_posts.party_posts_posted_by");
        $res = $this->db->get();
        return $res->result_array();
        // $return['data'] =  $res->result_array();
        // $return['count'] = $res->num_rows();
        // return $return;
    }




    // // get party posts api
    // function searchPartyPosts($search = null){

    //     $this->db->select(array("posts.*","users.users_name","users.users_photo"));
    //     $this->db->like('posts_title',$search);
    //     $this->db->where(array('posts_active'=>1,'posts_type'=>1));
    //     $this->db->order_by("posts_id","DESC");
    //     $this->db->from("posts");
    //     $this->db->join("users","users.user_id=posts.posts_user_id");
    //     $res = $this->db->get();
    //     return $res->result_array();
    //     // $return['data'] =  $res->result_array();
    //     // $return['count'] = $res->num_rows();
    //     // return $return;
    // }

    

     


    //get posts poll count
    function pollsCount($postid = null){

        $res = $this->db->get_where('posts_polls',array('posts_polls_post_id' => $postid));  
        $return['count'] = $res->num_rows();
        return $return;
    }

     // get posts comments Count
     function commentsCount($postid = null){

        $this->db->select(array('posts_comments.*','users.users_name'));
        $this->db->order_by('posts_comments_id',"desc");
        $this->db->from('posts_comments');
        $this->db->limit(3);
        $this->db->where(array('posts_comments_post_id' => $postid));
        $this->db->join("users","users.user_id = posts_comments.posts_comments_user_id");
        $res             = $this->db->get();
        $return['count'] = $res->num_rows();
        $return['data']  = $res->result_array();
        return $return;
    }

    // get posts comments Count outer
    function commentsCountouter($postid = null){
       
        $res = $this->db->get_where('posts_comments',array('posts_comments_post_id' => $postid));
        return $res->num_rows();
        
    }

      // get posts comments Count outer
      function partycommentsCountouter($postid = null){
       
        $res = $this->db->get_where('party_posts_comments',array('party_posts_comments_post_id' => $postid));
        return $res->num_rows();
        
    }

    // get party poll count
    function partyPollCount($id = null){

        $this->db->where('users_poll_party_id',$id);
        $res = $this->db->get('users');
        return $res->num_rows();
    }

    // get party list
    function getPartylist($scope = null ,$country = null , $state = null , $district = null , $area = null){

/*
        
        $this->db->select(array('party.*','users_party_id','COUNT(users_party_id) as total'));
        $this->db->where('users_party_id !=',1);
        $this->db->from('party');
        $this->db->join('users','users.users_party_id = party.party_id',"right");
        $this->db->group_by('users_party_id');
        $this->db->order_by('totals','desc'); 

  */      
        //$query = "SELECT `party_isd`,`party_photo`,`party_cover_photo`,`users_name`,`users_party_id` ,`COUNT(users_party_id) as total` FROM party LEFT JOIN users ON users.users_party_id = party.party_id GROUP BY users_party_id";
        
        $this->db->select('party_id,party_photo,party_name,party_code,party_isapproved,party_scope');
        if(!empty($scope))$this->db->where('party_scope',$scope);
        $this->db->where('party_id !=',1);
        $this->db->where('party_active',1);
        
        
        if(!empty($country)){

        $this->db->where('party_country',$country);

        }
        if(!empty($state && $country)){
         
         $this->db->or_where('party_country',$country);
         $this->db->where('party_state',$state);

        }
        if(!empty($district && $country && $state)) {
            
            $this->db->or_where('party_country',$country);
            $this->db->or_where('party_state',$state);
            $this->db->where('party_city',$district);
        }
        // if(!empty($area)) $this->db->where('party_area',$area);
        $res = $this->db->get('party');
        
        
        // $query = "SELECT `party_id`,`party_photo`,`party_name`,`party_isapproved` FROM party WHERE party_id != 1";
        // $res = $this->db->query($query);
        return  $res->num_rows() > 0  ? $res->result_array() :0;
    }

    //get party sum
    function getPartysum($scope = null ,$country  = null, $state  = null, $district  = null, $area = null ,$pollcolumn = null){

        $row_count = $this->userspartyCount();
        
        $this->db->select(array('party.party_scope',$pollcolumn,'COUNT('.$pollcolumn.') as total'));
        $this->db->where($pollcolumn.' !=',1);

        if(!empty($scope))$this->db->where('party_scope',$scope);
        if(!empty($country)) $this->db->where('users_country',$country);
        if(!empty($state)) $this->db->where('users_state',$state);
        if(!empty($district)) $this->db->where('users_city',$district);
        if(!empty($area)) $this->db->where('users_area',$area);
        
        $this->db->join('party','party.party_id = users.'.$pollcolumn);
        $this->db->from('users');
        $this->db->group_by($pollcolumn);
        $this->db->order_by('total','desc');
        $this->db->limit(3);

        $res = $this->db->get()->result_array(); 

        $total = 0;
        $this->db->select(array('party.party_scope',$pollcolumn,'COUNT('.$pollcolumn.') as total'));
        $this->db->where($pollcolumn.'!=',1);
        $this->db->where('party_scope',$scope);
        if(!empty($country)) $this->db->where('users_country',$country);
        if(!empty($state)) $this->db->where('users_state',$state);
        if(!empty($district)) $this->db->where('users_city',$district);
        if(!empty($area)) $this->db->where('users_area',$area);
        $this->db->join('party','party.party_id = users.'.$pollcolumn);
        $this->db->from('users');
        $this->db->group_by($pollcolumn);
        $this->db->order_by('total','desc');
        $this->db->limit($row_count,3);

        $res1 = $this->db->get();
        foreach($res1->result() as $row){
            $total += $row->total;
        }
        if($total>0){
            array_push($res,array('total' => $total));
        }
        return $res;
       

    }

    function check_restricted($country = null){
       return $this->db->get_where('party_restricted_nations',array('country_name'=>$country))->num_rows();
    }



    // //get party sum
    // function getPartysum($scope = null ,$country  = null, $state  = null, $district  = null, $area = null ,$pollcolumn = null){

    //     $row_count = $this->userspartyCount();
        
    //     $this->db->select(array('party.party_scope',$pollcolumn,'COUNT('.$pollcolumn.') as total'));
    //     $this->db->where($pollcolumn.' !=',1);

    //     if(!empty($scope))$this->db->where('party_scope',$scope);
    //     if(!empty($country)) $this->db->where('party_country',$country);
    //     if(!empty($state)) $this->db->where('party_state',$state);
    //     if(!empty($district)) $this->db->where('party_city',$district);
    //     if(!empty($area)) $this->db->where('party_area',$area);

    //     $this->db->join('party','party.party_id = users.'.$pollcolumn);
    //     $this->db->from('users');
    //     $this->db->group_by($pollcolumn);
    //     $this->db->order_by('total','desc');
    //     $this->db->limit(3);

    //     $res = $this->db->get()->result_array(); 

    //     $total = 0;
    //     $this->db->select(array('party.party_scope',$pollcolumn,'COUNT('.$pollcolumn.') as total'));
    //     $this->db->where($pollcolumn.'!=',1);
    //     $this->db->where('party_scope',$scope);
    //     if(!empty($country)) $this->db->where('party_country',$country);
    //     if(!empty($state)) $this->db->where('party_state',$state);
    //     if(!empty($district)) $this->db->where('party_city',$district);
    //     if(!empty($area)) $this->db->where('party_area',$area);
    //     $this->db->join('party','party.party_id = users.'.$pollcolumn);
    //     $this->db->from('users');
    //     $this->db->group_by($pollcolumn);
    //     $this->db->order_by('total','desc');
    //     $this->db->limit($row_count,3);

    //     $res1 = $this->db->get();
    //     foreach($res1->result() as $row){
    //         $total += $row->total;
    //     }
    //     if($total>0){
    //         array_push($res,array('total' => $total));
    //     }
    //     return $res;
       

    // }

    //get users party count
    function userspartyCount(){

        $this->db->where('users_poll_party_id !=',1);
        $res = $this->db->get('users');
        return $res->num_rows();
    }

    

    //get party name
    function getPartyname($id = null){
        $this->db->select('party_name,party_photo');
        $res = $this->db->get_where('party',array('party_id' => $id));
        return $res->row();
        
    }

    //get all party
    function getallParty($search = null){

        $this->db->select(array('party_id','party_name','party_photo','party_description'));
        $this->db->where('party_id !=',1);
        $this->db->like('party_name',$search);
        $res = $this->db->get('party');
        return  $res->num_rows() > 0  ? $res->result_array() :0;
    }

     //get party poll sum by id
     function getPartypollcountbyid($id = null){
       
       
        $this->db->where('users_poll_party_id',$id);
        $this->db->from('users');
        $res = $this->db->get();
        return  $res->num_rows() > 0  ? $res->num_rows() :0;
    }

    // get party poll , join user 
    function get_party_poll_user($userid = null , $pollcolumn = null,$joincolumn = null){
       
        $this->db->select(array($pollcolumn,$joincolumn));
        $this->db->where('user_id',$userid);
        $res = $this->db->get('users');
       
        $return['poll'] = $res->row($pollcolumn);
        $return['join'] = $res->row($joincolumn);
        return $return;
    }



    // get party sum by id
    function getPartypollsumbyid($id = null , $columnname = null,$country = null ,$state = null,$district = null ,$area = null){
      
        $this->db->select('COUNT('.$columnname.')');
        $this->db->where($columnname,$id);
        if(!empty($country)) $this->db->where('users_country',$country);
        if(!empty($state)) $this->db->where('users_state',$state);
        if(!empty($district)) $this->db->where('users_city',$district);
        if(!empty($area)) $this->db->where('users_area',$area);
        $this->db->group_by($columnname);
        $res = $this->db->get('users');
        return $res->row('COUNT('.$columnname.')');
    }

    // get party secret by id
    function getPartypubliccountbyid($id = null , $pollcolumn = null , $typecolumn = null , $country = null ,$state = null,$district = null,$area = null){

        $this->db->where(array($pollcolumn=>$id , $typecolumn => 0));
        if(!empty($country)) $this->db->where('users_country',$country);
        if(!empty($state)) $this->db->where('users_state',$state);
        if(!empty($district)) $this->db->where('users_city',$district);
        if(!empty($area)) $this->db->where('users_area',$area);
        $res = $this->db->get('users');
        return $res->num_rows();  
    }

     // get party  secret by id
     function getPartysecretcountbyid($id = null , $pollcolumn = null , $typecolumn = null , $country = null ,$state = null,$district = null,$area = null){

        $this->db->where(array($pollcolumn => $id , $typecolumn => 1));
        if(!empty($country)) $this->db->where('users_country',$country);
        if(!empty($state)) $this->db->where('users_state',$state);
        if(!empty($district)) $this->db->where('users_city',$district);
        if(!empty($area)) $this->db->where('users_area',$area);
        $res = $this->db->get('users');
        return $res->num_rows();  
    }



     // get party secret by id
     function publicCountpartycreated($id = null , $pollcolumn = null , $typecolumn = null){

        $this->db->where(array($pollcolumn=>$id , $typecolumn => 0));
        $res = $this->db->get('users');
        return $res->num_rows();  
    }

     // get party  secret by id
     function secretCountpartycreated($id = null , $pollcolumn = null , $typecolumn = null){

        $this->db->where(array($pollcolumn => $id , $typecolumn => 1));
        $res = $this->db->get('users');
        return $res->num_rows();  
    }

     // upload posts api
     function uploadpartyPost($insert = null){

        $this->db->insert('party_posts',$insert);
        return $this->db->affected_rows();
    }

    // get Party Profile
    function getPartyProfile($id = null){

        $this->db->select('party_id,party_scope,party_code,party_name,party_president,party_president_photo,
        party_secretary,party_secretary_photo,
        party_photo,party_cover_photo,party_isapproved,party_created_by,party_description');
        
        $this->db->where('party_id',$id);
       
        $res = $this->db->get('party');
        return $res->num_rows() > 0 ? $res->result_array() : 0;
    }

    // get Party Join Count
    function getPartyJoinCount($id = null){

        $this->db->where('users_party_id',$id);
        $res = $this->db->get('users');
        return $res->num_rows();
    }

     // get party members
     function getprofilePartyJoinmembers($id = null){

        $this->db->select('user_id,users_name,users_photo');
        //$this->db->limit(5);
        $this->db->where('users_party_id',$id);
        $res = $this->db->get('users');
        return $res->result_array();
    }

    // get party poll members count
    function getprofilePartyJoinmembersbypoll($partyid = null){

        $this->db->where('users_party_followers_party_id',$partyid);
        $this->db->select('users.users_name,users.user_id,users.users_photo,users_party_followers_user_id,COUNT(users_party_followers_user_id) as total');
        $this->db->group_by('users_party_followers_user_id');
        $this->db->order_by('total','desc');
        $this->db->limit(5);
        $this->db->join('users','users.user_id = users_party_followers.users_party_followers_user_id');
        $res = $this->db->get('users_party_followers');
        return $res->result_array();
    }

    // get party poll members count
    function Joinmembersbypollnolimit($userid = null ,$partyid = null){

        $this->db->where('users_party_followers_party_id',$partyid);
        $this->db->where('users_party_followers_user_id',$userid);
       
        $res = $this->db->get('users_party_followers');
        return $res->num_rows();
    }

    

    // get party memebers
    function getPartyJoinmembers($id = null){

        $this->db->select('user_id,users_name,users_photo');
        $this->db->where('user_party_id',$id);
        $res = $this->db->get('users');
        return $res->num_rows() > 0 ? $res->result_array() : 0;
    }


     // get party memebers
     function getPartyJoinmemberslist($id = null , $columname = null){

        $this->db->select('user_id,users_name,users_photo');
        $this->db->where($columname,$id);
        $res = $this->db->get('users');
        return $res->num_rows() > 0 ? $res->result_array() : 0;
    }

      // get Party poll Count
      function getPartyPollCount($id = null){

        $this->db->where('users_poll_party_id',$id);
        $res = $this->db->get('users');
        return $res->num_rows();
    }

    // get party posts by id
    function getPartyPosts($id = null){

        $this->db->select('party_posts.*,users.users_name,users.users_photo');
        $this->db->where('party_posts_party_id',$id);
        $this->db->where('party_posts_active',1);
        $this->db->order_by('party_posts_id',"desc");
        $this->db->join("users",'users.user_id = party_posts.party_posts_posted_by');
        $res = $this->db->get('party_posts');
        return $res->result_array();    
    }

    // get joined party
    function getPolledparty($userid = null , $scope = null){

        $res = $this->db->get_where('users',array('user_id' => $userid));
        switch($scope){
            
            case 1:
            $return['join']      = $res->row('users_party_id');
            $return['poll']      = $res->row('users_poll_party_id');
            break;

            case 2:
            $return['join']      = $res->row('users_party_state_id');
            $return['poll']      = $res->row('users_poll_party_state_id');
            break;

            case 3:
            $return['join']      = $res->row('users_party_district_id');
            $return['poll']      = $res->row('users_party_district_poll_id');
            break;

        }
       
        return $return;

    }


    //get joined party for profile
    function getjoinpartyname($id = null){
       
        $this->db->select('party_name');
        $this->db->where('party_id',$id);
        $res = $this->db->get('party');
        return $res->row('party_name');

    }

    // get polled party  for profile
    function getpollpartyname($id = null){
        
        $this->db->select('party_name');
        $this->db->where('party_id',$id);
        $res = $this->db->get('party');
        return $res->row('party_name');
    }

    // get user poll by id
    function getUserpollcount($userid = null){

        $this->db->where('users_followers_users_id',$userid);
        return $this->db->get('users_followers')->num_rows();

    }

     // get party user poll by id
     function getpartyUserpollcount($userid = null){

        $this->db->where('users_party_followers_user_id',$userid);
        return $this->db->get('users_party_followers')->num_rows();

    }

    

    // check party user polled

    function is_polled_user($followerid = null , $userid = null ,$partyid =null){

        $this->db->where(array('users_party_followers_user_follower_id' =>$followerid , 'users_party_followers_user_id' => $userid , 'users_party_followers_party_id' => $partyid));
        return $this->db->get('users_party_followers')->num_rows();
    }
    // poll party users
    function pollPartyusers($userid = null, $followerid = null , $partyid = null){

        $this->db->where('users_party_followers_user_follower_id',$followerid);
        $res = $this->db->get('users_party_followers')->num_rows();
        

        if($res > 0){
            $return = -1;
        }
        else{
            
            $insert = array('users_party_followers_user_id' => $userid , 'users_party_followers_user_follower_id' => $followerid ,'users_party_followers_party_id' => $partyid);
            $this->db->insert('users_party_followers',$insert);
            
            if($this->db->affected_rows() > 0){
                $notifications = array('notifications_user_id' =>$followerid , 'notifications_user_poll_id' => $userid , 'notifications_type' => 2);
                $this->db->insert('notifications',$notifications);
                $return =  1;
            }
            else{
                $return =  0;
            }
            
        }
        
        return $return;

    }
    // confirm poll Party users
    function confirmpollPartyusers($userid = null, $followerid = null , $partyid = null){

            $this->db->where(array('users_party_followers_user_follower_id' => $followerid));

            $update = array('users_party_followers_user_id' => $userid ,
                             'users_party_followers_user_follower_id' => $followerid ,
                             'users_party_followers_party_id' => $partyid);

            $this->db->update('users_party_followers',$update);
                
            if($this->db->affected_rows()> 0){
                $notifications = array('notifications_user_id' =>$followerid , 'notifications_user_poll_id' => $userid , 'notifications_type' => 2);
                $this->db->insert('notifications',$notifications);
            }
            return $this->db->affected_rows() > 0 ? 1 : 0;

    }

    // remove party user poll
    function removePartyPoll($followerid = null){

        $this->db->where(array('users_party_followers_user_follower_id' => $followerid));
        $this->db->delete('users_party_followers');
        return $this->db->affected_rows() > 0 ? 1 : 0;

    }

    //get party posts poll count
    function partypollsCount($postid = null){

        $res = $this->db->get_where('party_posts_polls',array('party_posts_polls_post_id' => $postid));  
        return $res->num_rows();
        
    }

    // get party posts comments count

    function partypostscommentsCount($postsid = null){

        $this->db->where('party_posts_comments_post_id',$postsid);
        return $this->db->get('party_posts_comments')->num_rows();

    }

    // claim your party
    function claimParty($insert = null){

        $this->db->insert('claim_party' , $insert);
        return $this->db->affected_rows();
    }


    // get profile pic for remove from folder
    function geteditprofilepic($id = null){
        
        //$this->db->select(array('users_photo','users_cover_photo'));
        $this->db->where(array('party_id'=>$id));
        $res = $this->db->get('party');
        $return['profile']          =  $res->row('party_photo');
        $return['cover']            =  $res->row('party_cover_photo');
        $return['president_photo']  =  $res->row('party_president_photo');
        $return['secretary_photo']  =  $res->row('party_secretary_photo');
        return $return;
    }

 

     //poll posts
     function pollpartyPosts($user_id = null, $posts_id = null,$user_posts_id = null){

        $insert               = array('party_posts_polls_post_id' => $posts_id,'party_posts_polls_user_id' => $user_id);
        $update_notifications = array('notifications_type' => 13,'notifications_post_id' => $posts_id ,'notifications_user_id' => $user_id,'notifications_user_poll_id' => $user_posts_id);

        if($this->checkPolled($user_id,$posts_id) == 0){

            $this->db->insert('party_posts_polls',$insert);
            $this->db->insert('notifications',$update_notifications);
            $return['res'] = 1;
        }
        else{
            $this->db->where(array('party_posts_polls_post_id' => $posts_id , 'party_posts_polls_user_id' => $user_id));
            $this->db->delete('party_posts_polls');
            $return['res'] = -1;
        }
       
        return $return;

    }

    //check already polled or not
    function checkPolled($user_id = null,$posts_id = null){

        $this->db->where(array('party_posts_polls_post_id' => $posts_id , 'party_posts_polls_user_id' => $user_id));
        $res = $this->db->get('party_posts_polls');
        return $res->row('party_posts_polls_id');
    }

     // posts comments
     function commentpartyPost($user_id = null , $post_id = null , $msg = null ,$user_posts_id = null){

        $insert = array('party_posts_comments_post_id' => $post_id , 'party_posts_comments_user_id' => $user_id , 'party_posts_comments_comment' => $msg);
        $this->db->insert('party_posts_comments' , $insert);
        
        $update_notifications = array('notifications_type' => 14,'notifications_post_id' => $post_id ,'notifications_user_id' => $user_id,'notifications_user_poll_id' => $user_posts_id);

        if($this->db->affected_rows() > 0){
            $this->db->insert('notifications',$update_notifications);
            $this->db->select(array('party_posts_comments.*','users.users_name'));
            $this->db->limit(3);
            $this->db->order_by('party_posts_comments_id',"desc");
            $this->db->from('party_posts_comments');
            $this->db->where(array('party_posts_comments_post_id'=>$post_id));
            $this->db->join('users','users.user_id=party_posts_comments.party_posts_comments_user_id');
            $res = $this->db->get();
            $return['data'] = $res->result_array();
           
        }
        else{

            return 0;
        }
        return $return;

    }


      // get post comments
      function getpartyPostcomments($post_id = null){

        $this->db->select(array('party_posts_comments.*','users.users_name','users.users_photo'));
        $this->db->join('users','users.user_id=party_posts_comments.party_posts_comments_user_id');
        $res = $this->db->get_where('party_posts_comments',array('party_posts_comments_post_id' => $post_id));
        return $res->num_rows() > 0 ? $res->result_array() : 0;

    }

     // get comments reply via comment id
     function getpartycommentsreply($id = null , $post_id = null){

        $this->db->from('party_posts_comments_reply');
        $this->db->order_by('party_posts_comments_reply_id',"desc");
        $this->db->select(array('party_posts_comments_reply.*','users.users_name','users.users_photo'));
        $this->db->where(array('party_posts_comments_reply_comment_id' => $id , 'party_posts_comments_reply_posts_id' => $post_id));
        $this->db->join('users','users.user_id=party_posts_comments_reply.party_posts_comments_reply_user_id');
        $res    = $this->db->get();
        return $res->result_array();
    }

    // get post comments by id
    function getpartyPostcommentsbyid($comment_id = null){

        $this->db->select(array('party_posts_comments.*','users.users_name','users.users_photo'));
        $this->db->join('users','users.user_id=party_posts_comments.party_posts_comments_user_id');
        $res = $this->db->get_where('party_posts_comments',array('party_posts_comments_id' => $comment_id));
        return $res->num_rows() > 0 ? $res->result_array() : null;

    }

      //reply to comment
      function replypartyPostscomments($insert = null , $comment_user_id = null){
        $update_notifications = array('notifications_type' => 6,'notifications_post_id' => $insert['party_posts_comments_reply_posts_id'] ,'notifications_user_id' => $insert['party_posts_comments_reply_user_id'],'notifications_user_poll_id' => $comment_user_id);

        $this->db->insert('party_posts_comments_reply',$insert);
        $res = $this->db->affected_rows();
        if($res > 0){

            $this->db->insert('notifications',$update_notifications);
            return 1;
        }
        else{
            return 0;
        }
    }

     //get party rank
      function getpartyrank($partyid = null){

        $this->db->where('users_poll_party_id !=',1);
        $this->db->select('users_poll_party_id,COUNT(users_poll_party_id) as total');
        $this->db->group_by('users_poll_party_id');
        $this->db->order_by('total','desc');
        $res = $this->db->get('users');
        return $res->result_array();
      } 
    
      // get created party
      function getcreatedParties($id = null){

        $this->db->select(array('party_id','party_name','party_photo','party_scope'));
        return $this->db->get_where('party',array('party_created_by' => $id))->result_array();
      }

      // get joined party list
      function getjoinedParties($id = null){
        $this->db->select(array('party_name','party_id','party_photo','party_scope'));
        $this->db->where(array('user_id' => $id));
        $this->db->where('party_id !=', 1);
        $this->db->join('party','party.party_id = users.users_party_id');
        $res = $this->db->get('users');
        return $res->result_array();
      }

      // check party location to join
      function isLocation($where = null){

        $this->db->select('party_id');
        $this->db->where($where);
        $res = $this->db->get('party');
        return $res->num_rows() > 0 ? 1 : 0;
      }

      // get user location for getchart
      function getUserlocation($id = null){

        $this->db->select('users_country,users_state,users_city,users_area');
        $res = $this->db->get_where('users',array('user_id' => $id));
        $return['country']  = $res->row('users_country');
        $return['state']    = $res->row('users_state');
        $return['district'] = $res->row('users_city');
        $return['area']     = $res->row('users_area');
        return $return;
  
      }


    // check user blocked ot not
    function check_isBlocked($where = null){
        $this->db->select('users_active');
        $this->db->where($where);
        return $this->db->get('users')->num_rows();
    }

      // get party polled members
      function getPolledmembers($partyid = null , $pollcolumn = null , $typecolumn = null){

        $this->db->select('users.users_name,users.user_id,users.users_photo');
        $this->db->where($pollcolumn , $partyid);
        $this->db->where($typecolumn , 0);
        $res = $this->db->get('users');
        return $res->result_array();
      }

     //is party post polled
     function ispostpolled($postid = null , $userid = null){

        $this->db->where('party_posts_polls_post_id',$postid);
        $this->db->where('party_posts_polls_user_id',$userid);
        $res = $this->db->get('party_posts_polls');
        return $res->num_rows();
     }

       // delete posts by id
       function deletepartyPosts($postid = null){

        $this->db->where('party_posts_id',$postid);
        $update = array('party_posts_active'=>0);
        $this->db->update('party_posts',$update);
        return $this->db->affected_rows();

    }

      // function delete notifications after block or delete posts
      function deletenotifications($postid = null){

        $this->db->where('notifications_post_id',$postid);
        $this->db->delete('notifications');
    }
      

      // report post
      function reportpartyPost($user_id = null, $post_id = null , $msg = null){

        if($this->check_report_exist($user_id , $post_id) == 0){
        $insert = array('party_posts_report_post_id' => $post_id , 'party_posts_report_user_id' => $user_id , 'party_posts_report_comment' => $msg);
        $this->db->insert('party_posts_report',$insert);
        return $this->db->affected_rows(); 
        }
        else{
            return -1;
        }   
    }

    // check report exists or not
    function check_report_exist($user_id = null, $post_id = null){

        $this->db->where(array('party_posts_report_post_id' => $post_id , 'party_posts_report_user_id' => $user_id));
        $res = $this->db->get('party_posts_report');
        return $res->num_rows();
    }   

    // update post report count
    function updatepartyReportcount($postid = null , $msg = null){

        if($msg == "Sexually explicite")
        $query = "UPDATE party_posts SET party_posts_report_count = party_posts_report_count + 1 , party_posts_remove_count = party_posts_remove_count + 1 WHERE party_posts_id = $postid";
        else
        $query = "UPDATE party_posts SET party_posts_report_count = party_posts_report_count + 1  WHERE party_posts_id = $postid";
        $this->db->query($query);

    }

    // remove posts by report count from user
    function removePostsbypartyreportcount($postid = null){

        $this->db->where('party_posts_id' , $postid);
        $this->db->where('party_posts_report_count' , 15);
        $this->db->update('party_posts',array('party_posts_active' => 0));

    }
    

     // get reported posts by userid
     function getReportedposts($userid = null){

        $this->db->select('DISTINCT(party_posts_report_post_id)');
        $this->db->where('party_posts_report_user_id',$userid);
        $res = $this->db->get('party_posts_report');
        return $res->result_array();
    }

     // hide posts
     function hidePosts($postid = null, $userid = null){

        $insert= array('hide_posts_post_id' => $postid , 'hide_posts_user_id' =>$userid , 'hide_posts_type' => 2);
        $this->db->insert('hide_posts',$insert);
        return $this->db->affected_rows();
    }

     // get hidden posts by userid
     function gethiddenposts($userid = null){

        $this->db->select('DISTINCT(hide_posts_post_id)');
        $this->db->where('hide_posts_user_id',$userid);
        $this->db->where('hide_posts_type',2);
        $res = $this->db->get('hide_posts');
        return $res->result_array();
    }

     // edit posts by id
     function editPosts($postid = null, $update = null){

        $this->db->where('party_posts_id',$postid);
        $this->db->update('party_posts',$update);
        return $this->db->affected_rows();

    }


     // get posts by id
     function getnotificationpartyPostsbyid($posts_id = null){

        $this->db->select(array("party_posts.*","users.users_name","users.users_photo"));
        $this->db->where(array('party_posts_id' => $posts_id,'party_posts_active' => 1));
        $this->db->order_by("party_posts_id","DESC");
        $this->db->from("party_posts");
        $this->db->join("users","users.user_id = party_posts.party_posts_posted_by");
        $res = $this->db->get();
       
        return $res->num_rows() > 0 ? $res->result_array() : 0;
        }

    // get users name by id
    function getusersnamebyid($id = null){

        $this->db->select('users_name');
        $this->db->where('user_id',$id);
        $res = $this->db->get('users');
        return $res->row('users_name');
    }
   

     //remove post by report count
   function removePartyPostbyCount($id = null){
    if($this->check_report_count($id) > 0){
     $this->db->where('party_posts_id', $id);
     $this->db->update('party_posts',array('party_posts_active'=>0));
    }
    else{
        return false;
    } 
    }

    //check report count
    function check_report_count($id){
    $this->db->where('party_posts_id', $id);
    $this->db->where('party_posts_remove_count >=', 15);   
    $res = $this->db->get('party_posts');
    return $res->num_rows();
}
   
}