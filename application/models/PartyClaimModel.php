<?php

class PartyClaimModel extends CI_Model{

    function getPartyClaimlist(){
        
        $this->db->select(array("claim_party.*","users.users_name","party.party_name"));
        $this->db->from('claim_party');
        $this->db->join("party","party.party_id = claim_party.claim_party_party_id");
        $this->db->join("users","users.user_id = claim_party.claim_party_user_id");
        $res = $this->db->get();
        return $res->result_array();
    }
}

?>