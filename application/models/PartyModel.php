<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class PartyModel extends CI_Model{
    
    //get all party list
    function getallParty(){
        
        $this->db->from('party');
        $this->db->where('party_id !=',1);
        $this->db->join("users","users.user_id=party.party_created_by");
        $res = $this->db->get();
        return $res->result_array();
        
    }

     // get all posts model
     function getpartyPosts(){

        $this->db->from('party_posts');
        $this->db->join("party","party.party_id=party_posts.party_posts_party_id");
        $this->db->join("users","users.user_id=party_posts.party_posts_posted_by");
        $res    = $this->db->get();
        return $res->result_array();
    }

    // party list for users section
     function getPartylist(){
        
        $this->db->from('party');
        $res = $this->db->get();
        return $res->result_array();
        
    }
 

     // get blocked posts
     function getblockedpartyPosts(){

        $this->db->where(array("party_posts_active" => 0));
        $this->db->from('party_posts');
        $this->db->join("party","party.party_id=party_posts.party_posts_party_id");
        $this->db->join("users","users.user_id=party_posts_posted_by");
        $res    = $this->db->get();
        return $res->result_array();

    }


     //block posts by id 
     function blockpartyposts($id = null , $flag = null){

        $update = array('party_posts_active' => $flag);
        $this->db->where(array('party_posts_id' => $id));
        $this->db->update('party_posts',$update);
        return $this->db->affected_rows();
    }

     // post details by id
     function getPartyPostsdetails($id = null){


        $this->db->select('users_name,user_id,users_photo,party_posts.*');
        $this->db->from('party_posts');
        $this->db->where(array('party_posts_id'=>$id));
        $this->db->join("users","users.user_id = party_posts.party_posts_posted_by");
        $res    = $this->db->get();
        return $res->result_array();
    }

    // block party pass id
    function blockparty($id=null,$flag=null){
        

        $update = array('party_active'=>$flag);
        $this->db->where(array('party_id'=>$id));
        $this->db->update('party',$update);
        return $this->db->affected_rows();
    }
    
    // approve party pass id and flag
    function approveParty($id=null,$flag=null){

        $update = array('party_isapproved'=>$flag);
        $this->db->where(array('party_id'=>$id));
        $this->db->update('party',$update);
        return $this->db->affected_rows();
    }
    
    // get party details pass id
    function getpartydetails($id=null){
        
        $res = $this->db->get_where('party',array('party_id'=>$id));
        return $res->result_array();
    }
    
    // get party followers count
    function getfollowercount($id=null){
        
        $res = $this->db->get_where('users',array('users_party_id'=>$id));
        return $res->num_rows();
    }

   
    // get party followers count
    function getpollcount($id=null){
        
        $res = $this->db->get_where('users',array('users_poll_party_id'=>$id));
        return $res->num_rows();
    }

      // get party followers count
      function getpublicpollcount($id=null){
        
        $res = $this->db->get_where('users',array('users_poll_party_id'=>$id , 'users_party_poll_type' => 0));
        return $res->num_rows();
    }

      // get party followers count
      function getsecretpollcount($id=null){
        
        $res = $this->db->get_where('users',array('users_poll_party_id'=>$id , 'users_party_poll_type' => 1));
        return $res->num_rows();
    }

    // get country
    function getCountry(){

        $res = $this->db->get('countries');
        return $res->result_array();
    }

    // get state by id
    function getstatebyid($cid = null){

        $this->db->where('country_id',$cid);
        $res = $this->db->get('states');
        return $res->result_array(); 
    }

    // get state by id
    function getdistrictbyid($cid = null){

        $this->db->where('state_id',$cid);
        $res = $this->db->get('cities');
        return $res->result_array(); 
    }

    //create party
    function createParty($insert = null){

        $this->db->insert('party',$insert);
        return $this->db->insert_id();

    }
    
    // get state name
    function getstatename($id = null){

        $this->db->where('id',$id);
        return $this->db->get('states')->row('name');
    }

    // get country name
    function getcountryname($id = null){

        $this->db->where('id',$id);
        return $this->db->get('countries')->row('name');
    }

     // get admin photo 
     function getPartyphoto($id){

        $this->db->where(array("party_id"=>$id));
        $res = $this->db->get('party');
        return $res->row('party_photo');

    }

      // get admin photo 
      function getPartycoverphoto($id){

        $this->db->where(array("party_id"=>$id));
        $res = $this->db->get('party');
        return $res->row('party_cover_photo');

    }


     // upload admin profile pic 
     public function uploadpartypic($filename,$id)
     {
         $this->db->where(array("party_id"=>$id));
         $this->db->update('party',array('party_photo'=>$filename));
         return $this->db->affected_rows();
     }

       // upload admin profile pic 
       public function uploadpartycoverpic($filename,$id)
       {
           $this->db->where(array("party_id"=>$id));
           $this->db->update('party',array('party_cover_photo'=>$filename));
           return $this->db->affected_rows();
       }

     //edit party
     function editParty($id = null , $insert = null){

        $this->db->where('party_id',$id);
        $this->db->update('party',$insert);
        return $this->db->affected_rows();

    }

    // get country id by name
    function getcountryidbyname($name){

        $this->db->where(array("name"=>$name));
        $res = $this->db->get('countries');
        return $res->row('id');
    }

     // get country id by name
     function getstateidbyname($name){

        $this->db->where(array("name"=>$name));
        $res = $this->db->get('states');
        return $res->row('id');
    }

    // get party restricted nations
    function getrestrictednations(){
        return $this->db->get('party_restricted_nations')->result_array();

    }

    // check country already exists or not
    function CheckCountryExists($data){
      return  $this->db->get_where('party_restricted_nations',array('country_name' => $data))->num_rows();
    }

    // save save Restricted Country
    function saveRestrictedCountry($data = null){
        if($this->CheckCountryExists($data) == 0){
        $this->db->insert('party_restricted_nations',array('country_name' => $data));
        return $this->db->affected_rows();
        }
    }

    //remove restricted party
    function removerestrictedParty($id = null){
        $this->db->where('id',$id);
        $this->db->delete('party_restricted_nations');
        return $this->db->affected_rows();
    }

    // get polled users
    function getPollpartyMembers($id = null){

        $this->db->select('users_name,users_photo,users_party_poll_type');
        $return['public']  = $this->db->get_where('users',array('users_poll_party_id' => $id,'users_party_poll_type' => 0))->result_array();
        $this->db->select('users_name,users_photo,users_party_poll_type');
        $return['secret']  = $this->db->get_where('users',array('users_poll_party_id' => $id,'users_party_poll_type' => 1))->result_array();
        return $return;
    }
  
}
