<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 8/10/2018
 * Time: 2:34 PM
 */


class PostModel extends CI_Model{

    // get all posts model
    function getPosts(){

        $this->db->from('posts');
        $this->db->order_by('posts_uploaded_date','desc');
        $this->db->join("users","users.user_id=posts_user_id");
        $res    = $this->db->get();
        return $res->result_array();
    }

    // get blocked posts
    function getblockedPosts(){

        $this->db->where(array("posts_active" => 0));
        $this->db->from('posts');
        $this->db->order_by('posts_uploaded_date','desc');
        $this->db->join("users","users.user_id=posts_user_id");
        $res    = $this->db->get();
        return $res->result_array();

    }

   
    // post details by id
    function getPostsdetails($id = null){


        $this->db->from('posts');
        $this->db->where(array('posts_id'=>$id));
        $this->db->join("users","users.user_id=posts.posts_user_id");
        $res    = $this->db->get();
        return $res->result_array();
    }

    //block posts by id 
    function blockposts($id = null , $flag = null){

        $update = array('posts_active' => $flag);
        $this->db->where(array('posts_id' => $id));
        $this->db->update('posts',$update);
        return $this->db->affected_rows();
    }

    //SELECT COUNT(posts_report_post_id) as total FROM `posts_report` GROUP BY `posts_report_post_id`

    


    // get posts reports
    function getPostreports($count = null){

        $this->db->where("posts_report_count >=",$count);
        $this->db->order_by('posts_id','desc');
        $this->db->from('posts');
        $this->db->join("users","users.user_id=posts_user_id");
        $res    = $this->db->get();
        return $res->result_array();
    
        //return $query->num_rows() > 0 ? $query->result_array() : NULL;
    }

     // post report details by id
    function getpostreportdetails($id = null){

        $this->db->where(array('posts_report_post_id' => $id));
        $res    = $this->db->get('posts_report');
        return $res->result_array();
    }

    function updatePollCount($count = null){
        $this->db->where('id' , 1);
        $this->db->update('poll_count_trending',array('count' => $count , 'edited_date' => date('Y-m-d H:i:s')));
        return $this->db->affected_rows();
    }

    function getPollCount(){
        $this->db->select('count');
        $this->db->where('id',1);
        $res = $this->db->get('poll_count_trending');
        return $res->row('count');
    }
}