<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 8/13/2018
 * Time: 4:09 PM
 */
class PostsApiModel extends CI_Model{


    // upload posts api
    function uploadPost($insert=null){

        $this->db->insert('posts',$insert);
        return $this->db->insert_id();
    }

    //get posts order 
    function posts_sort_order(){
        
        $this->db->select('posts_polls_post_id,COUNT(posts_polls_post_id) as total');
        $this->db->from('posts_polls');
        $this->db->group_by('posts_polls_post_id');
        $this->db->order_by('total','desc');
        $this->db->limit(5);
        $res = $this->db->get();
        return $res->result_array();

    }

    // get posts by location
    function getPostsbyLocation($state = null){
        
        $this->db->select('users.users_state,posts.*');
        $this->db->from('posts');
        $this->db->where('users_state',$state);
        $this->db->join('users','users.user_id = posts.posts_user_id');
        $res = $this->db->get();
        return $res->result_array();
    }

     //get posts order 
     function posts_sort_order_state(){
        
        $this->db->select('posts_polls_post_id,COUNT(posts_polls_post_id) as total');
        $this->db->from('posts_polls');
        $this->db->group_by('posts_polls_post_id');
        $this->db->order_by('total','desc');
        //$this->db->limit(3);
        $res = $this->db->get();
        return $res->result_array();

    }


    //get polled party id
    function getpollpartyid($userid = null){

        $this->db->select('users.users_poll_party_id');
        $res = $this->db->get_where('users',array('user_id' => $userid));
        return $res->row('users_poll_party_id');

    }


    //get posts order 
    function party_posts_sort_order(){
        
        $this->db->select('party_posts_polls_post_id,COUNT(party_posts_polls_post_id) as total');
        $this->db->from('party_posts_polls');
        $this->db->group_by('party_posts_polls_post_id');
        $this->db->order_by('total','desc');
        //$this->db->limit(3);
        $res = $this->db->get();
        return $res->result_array();

    }

    

    // get posts api
    function getPosts_1($id = null){

        $this->db->select(array("posts.*","users.users_name"));
        $this->db->where(array('posts_id'=>$id,'posts_active'=>1,'posts_type'=>1));
        $this->db->order_by("posts_id","DESC");
        $this->db->from("posts");
        $this->db->join("users","users.user_id=posts.posts_user_id");
        $res = $this->db->get();
        //return $res->result_array();
        $return['data']  =  $res->result_array();
        $return['count'] = $res->num_rows();
        return $return;
    }


    // get posts api by polls
    function getPartyPostsbypolls($id = null){

        $this->db->select(array("party_posts.*","users.users_name","users.users_photo"));
        $this->db->where(array('party_posts_active'=>1,'party_posts_id'=>$id));
        $this->db->order_by("party_posts_id","DESC");
        
        $this->db->from("party_posts");
        $this->db->join("users","users.user_id=party_posts.party_posts_posted_by");
        $res = $this->db->get();
        return $res->result_array();
    }
    


     // get posts api by polls
     function getPostsbypolls($id = null){

        $this->db->select(array("posts.*","users.users_name","users.users_photo"));
        $this->db->where(array('posts_active'=>1,'posts_type'=>1,'posts_id'=>$id));
        $this->db->order_by("posts_id","DESC");
        
        $this->db->from("posts");
        $this->db->join("users","users.user_id=posts.posts_user_id");
        $res = $this->db->get();
        return $res->result_array();
    }


    // get posts by id 
    function getPostsbyid($post_id = null){

        $this->db->where(array('posts_id' => $post_id));
        $res = $this->db->get('posts');
        return $res->result_array();

    }

    // share posts
    function sharePosts($insert = null){
        
        $this->db->insert('posts',$insert);
        return $this->db->affected_rows();  

    }



     // get challenge list
     function getChallengelist($userid = null){

        $this->db->select(array('challenge.*','users.users_name',
                                'users.users_photo','users.users_login_type',
                                'users.users_country','users.users_state',
                                'users.users_city'
                                ));
        $this->db->where('challenge_active',1);
        $this->db->where('challenge_user_id',$userid);
        $this->db->join('users','users.user_id = challenge.challenge_user_id');
     
        $this->db->order_by('challenge_id','desc');
        $res = $this->db->get('challenge');
        return $res->result_array();
    }

     // check challenge polled
     function isChallengepolled($challengeid = null , $userid = null){

        $this->db->where('challenge_polls_challenge_id',$challengeid);
        $this->db->where('challenge_polls_polled_by',$userid);
        return $this->db->get('challenge_polls')->num_rows();
    }



     // get challenge winner
     function getWinner($cid = null) { 

        $this->db->select(array('users.users_name','users.users_photo','users.users_login_type','challenge_posts.*','challenge_polls_challenge_post_id','COUNT(challenge_polls_challenge_post_id) as total'));
        $this->db->where('challenge_polls_challenge_id',$cid);
        $this->db->group_by('challenge_polls_challenge_post_id');
        $this->db->order_by('total','desc');
        $this->db->limit(1);
        $this->db->join('challenge_posts','challenge_posts.challenge_posts_id = challenge_post_polls.challenge_polls_challenge_post_id');
        $this->db->join('users','users.user_id = challenge_posts.challenge_posts_user_id');
        $res = $this->db->get('challenge_post_polls');
        return $res->result_array();
    }

    // profile posts
    function getprofilePosts($userid = null){

        $this->db->order_by('posts_id','desc');
        $res = $this->db->get_where('posts',array('posts_user_id'=>$userid , 'posts_active' => 1));
        
        $return['count'] = $res->num_rows();
        $return['data']  = $res->result_array();
        return $return;
    }

     // profile posts
     function getprofilePostsnotfrnd($userid = null){

        $this->db->order_by('posts_id','desc');
        $res = $this->db->get_where('posts',array('posts_user_id'=>$userid,'posts_type' => 1,'posts_active' => 1));
        
        $return['count'] = $res->num_rows();
        $return['data']  = $res->result_array();
        return $return;
    }

    // get shared user name
    function getUsernameforsharedPost($userid = null){
        $this->db->select('users_name');
        $res = $this->db->get_where('users',array('user_id'=>$userid));
        return $res->row('users_name');
    }

    // get user poll to count
    function getuserPolltocount($userid = null){

        $res = $this->db->get_where('users_followers',array('users_followers_users_id'=>$userid));
        return $res->num_rows();
    }

     // get user poll by count
     function getuserPollbycount($userid = null){

        $res = $this->db->get_where('users_followers',array('users_followers_follower_id'=>$userid));
        return $res->num_rows();
    }

    // get profile by id
    function getprofilebyid($user_id = null){

        $this->db->select(array('users.user_id','users.users_name','users.users_username','users.users_photo'));
        $res = $this->db->get_where('users',array('user_id' => $user_id));
        return $res->result_array();

    }

    // hide profile posts
    function hideprofilePost($userid = null , $post_id = null){

        $update = array('posts_active' => 0);
        $this->db->where(array('posts_user_id' => $userid , 'posts_id' => $post_id));
        $this->db->update('posts',$update);
        return $this->db->affected_rows();

    }

    // report post
    function reportPost($user_id = null, $post_id = null , $msg = null){

        if($this->check_report_exist($user_id , $post_id) == 0){
        $insert = array('posts_report_post_id' => $post_id , 'posts_report_user_id' => $user_id , 'posts_report_comment' => $msg);
        $this->db->insert('posts_report',$insert);
        return $this->db->affected_rows(); 
        }
        else{
            return -1;
        }   
    }

    // check report exists or not
    function check_report_exist($user_id = null, $post_id = null){

        $this->db->where(array('posts_report_post_id' => $post_id , 'posts_report_user_id' => $user_id));
        $res = $this->db->get('posts_report');
        return $res->num_rows();
    }   

    // update post report count
    function updateReportcount($postid = null , $msg = null){

        if($msg == "Sexually explicite")
        $query = "UPDATE posts SET posts_report_count = posts_report_count + 1 , posts_remove_count = posts_remove_count + 1  WHERE posts_id = $postid";
        else
        $query = "UPDATE posts SET posts_report_count = posts_report_count + 1  WHERE posts_id = $postid";
        $this->db->query($query);

    }

    // remove posts by report count from user
    function removePostsbyreportcount($postid = null){

        $this->db->where('posts_id' , $postid);
        $this->db->where('posts_report_count' , 15);
        $this->db->update('posts',array('posts_active' => 0));

    }

   
    // posts comments
    function commentPost($user_id = null , $post_id = null , $msg = null ,$user_posts_id = null){

        $insert = array('posts_comments_post_id' => $post_id , 'posts_comments_user_id' => $user_id , 'posts_comments_comment' => $msg);
        $this->db->insert('posts_comments' , $insert);
        
        $update_notifications = array('notifications_type' => 1,'notifications_post_id' => $post_id ,'notifications_user_id' => $user_id,'notifications_user_poll_id' => $user_posts_id);

        if($this->db->affected_rows() > 0){
            $this->db->insert('notifications',$update_notifications);
            $this->db->select(array('posts_comments.*','users.users_name'));
            $this->db->limit(3);
            $this->db->order_by('posts_comments_id',"desc");
            $this->db->from('posts_comments');
            $this->db->where(array('posts_comments_post_id'=>$post_id));
            $this->db->join('users','users.user_id=posts_comments.posts_comments_user_id');
            $res = $this->db->get();
            $return['data'] = $res->result_array();
           
        }
        else{

            return 0;
        }
        return $return;

    }

    // get post comments
    function getPostcomments($post_id = null){

        // $this->db->distinct();
        // $this->db->select('*');
        // $this->db->from('posts_comments');
        // $this->db->join('users','users.user_id=posts_comments.posts_comments_user_id','left');
        // $this->db->join('posts_comments_reply','posts_comments_reply.posts_comments_reply_posts_id = posts_comments.posts_comments_post_id AND posts_comments_reply_active = 1 AND posts_comments_reply_posts_id="'.$post_id.'"');
        // $this->db->where(array('posts_comments_post_id' => $post_id ,'posts_comments_active' => 1));
        // $this->db->group_by('posts_comments_id'); 
        // $res = $this->db->get();
        // return $res->num_rows() > 0 ? $res->result_array() : null;
        $this->db->select('*');
        $this->db->from('posts_comments');
        $this->db->join('users','users.user_id=posts_comments.posts_comments_user_id');
        $this->db->where(array('posts_comments_post_id' => $post_id ,'posts_comments_active' => 1));
        $res = $this->db->get();
        return $res->num_rows() > 0 ? $res->result_array() : null;

    }

     // get post comments by id
     function getPostcommentsbyid($comment_id = null){

        $this->db->select(array('posts_comments.*','users.users_name','users.users_photo'));
        $this->db->join('users','users.user_id=posts_comments.posts_comments_user_id');
        $res = $this->db->get_where('posts_comments',array('posts_comments_id' => $comment_id , 'posts_comments_active' => 1));
        return $res->num_rows() > 0 ? $res->result_array() : null;

    }

    //reply to comment
    function replyPostscomments($insert = null , $comment_user_id = null , $type = null){

        $not_type = $type == 0 ? 5 : 127;
        $update_notifications = array('notifications_type' => $not_type,'notifications_post_id' => $insert['posts_comments_reply_posts_id'] ,'notifications_user_id' => $insert['posts_comments_reply_user_id'],'notifications_user_poll_id' => $comment_user_id);

        $this->db->insert('posts_comments_reply',$insert);
        $res = $this->db->affected_rows();
        if($res > 0){

            $this->db->insert('notifications',$update_notifications);
            return 1;
        }
        else{
            return 0;
        }
    }

    // get comments reply via comment id
    function getcommentsreply($id = null , $post_id = null){

        $this->db->from('posts_comments_reply');
        $this->db->order_by('posts_comments_reply_id',"desc");
        $this->db->select(array('posts_comments_reply.*','users.users_name','users.users_photo'));
        $this->db->where(array('posts_comments_reply_comment_id' => $id , 'posts_comments_reply_posts_id' => $post_id , 'posts_comments_reply_active' => 1));
        $this->db->join('users','users.user_id=posts_comments_reply.posts_comments_reply_user_id');
        
        $res    = $this->db->get();
        return $res->result_array();
    }


    //poll posts
    function pollPosts($user_id = null, $posts_id = null,$user_posts_id = null){

        $insert               = array('posts_polls_post_id' => $posts_id,'posts_polls_user_id' => $user_id);
        $update_notifications = array('notifications_type' => 0,'notifications_post_id' => $posts_id ,'notifications_user_id' => $user_id,'notifications_user_poll_id' => $user_posts_id);

        if($this->checkPolled($user_id,$posts_id) == 0){

            $this->db->insert('posts_polls',$insert);
            $this->db->insert('notifications',$update_notifications);
            $return['res'] = 1;
        }
        else{
            $this->db->where(array('posts_polls_post_id' => $posts_id , 'posts_polls_user_id' => $user_id));
            $this->db->delete('posts_polls');
            $return['res'] = -1;
        }
       
        return $return;

    }

    //check already polled or not
    function checkPolled($user_id = null,$posts_id = null){

        $this->db->where(array('posts_polls_post_id' => $posts_id , 'posts_polls_user_id' => $user_id));
        $res = $this->db->get('posts_polls');
        return $res->row('posts_polls_id');
    }

    //get posts poll count
    function pollsCount($postid = null){

        $res = $this->db->get_where('posts_polls',array('posts_polls_post_id' => $postid));  
        $return['count'] = $res->num_rows();
        return $return;
    }

      //get posts poll count
      function partypollsCount($postid = null){

        $res = $this->db->get_where('party_posts_polls',array('party_posts_polls_post_id' => $postid));  
        $return['count'] = $res->num_rows();
        return $return;
    }
    

    // is user polled
    function isUserpolled($posts_id = null ,$userid = null){

        $this->db->where(array('posts_polls_post_id' => $posts_id , 'posts_polls_user_id' => $userid));
        $res = $this->db->get('posts_polls');
        $return['count'] = $res->num_rows(); 
        return $return; 
    }

    // is user polled
    function PartyisUserpolled($posts_id = null ,$userid = null){

        $this->db->where(array('party_posts_polls_post_id' => $posts_id , 'party_posts_polls_user_id' => $userid));
        $res = $this->db->get('party_posts_polls');
        $return['count'] = $res->num_rows(); 
        return $return; 
    }

    

    // get posts comments Count
    function commentsCount($postid = null){

        $this->db->select(array('posts_comments.*','users.users_name','users_photo'));
        $this->db->order_by('posts_comments_id',"desc");
        $this->db->from('posts_comments');
        $this->db->limit(3);
        $this->db->where(array('posts_comments_post_id' => $postid, 'posts_comments_active' => 1));
        $this->db->join("users","users.user_id = posts_comments.posts_comments_user_id");
        $res             = $this->db->get();
        $return['count'] = $res->num_rows();
        $return['data']  = $res->result_array();
        return $return;
    }


      // get posts comments Count
      function PartycommentsCount($postid = null){

        $this->db->select(array('party_posts_comments.*','users.users_name','users_photo'));
        $this->db->order_by('party_posts_comments_id',"desc");
        $this->db->from('party_posts_comments');
        $this->db->limit(3);
        $this->db->where(array('party_posts_comments_post_id' => $postid));
        $this->db->join("users","users.user_id = party_posts_comments.party_posts_comments_user_id");
        $res             = $this->db->get();
        $return['count'] = $res->num_rows();
        $return['data']  = $res->result_array();
        return $return;
    }
 
    // get posts comments Count outer
    function commentsCountouter($postid = null){
       
        $res = $this->db->get_where('posts_comments',array('posts_comments_post_id' => $postid , 'posts_comments_active' => 1));
        return $res->num_rows();
        
    }

     // get posts comments Count outer
     function PartycommentsCountouter($postid = null){
       
        $res = $this->db->get_where('party_posts_comments',array('party_posts_comments_post_id' => $postid));
        return $res->num_rows();
        
    }

    //sort posts by categories
    function sortPosts($country = null , $state = null , $district = null , $area = null){

        $this->db->select(array('posts.*','users.user_id','users.users_country','users.users_photo','users.users_state','users.users_city','users.users_area'));
        $this->db->from('posts');
        $this->db->where('photo_or_video !=' , -1);
        $this->db->where(array('posts_active' => 1 , 'posts_type' => 1));
        $this->db->join("users","users.user_id = posts.posts_user_id");
        if(!empty($country)) $this->db->where('posts_country',$country);
        if(!empty($state)) $this->db->where('posts_state',$state);
        if(!empty($district)) $this->db->where('posts_city',$district);
        if(!empty($area)) $this->db->where('posts_area',$area);
        $res = $this->db->get();
        return ($res->num_rows() > 0) ?  $res->result_array() : 0;
    }

    // get posts poll count
    function getPostspollCount($postid = null){

        $this->db->where('posts_polls_post_id',$postid);
        $res = $this->db->get('posts_polls');
        return $res->num_rows();
    } 

      //sort posts by categories
      function sortPosts_new($country = null , $state = null , $district = null , $area = null){

        $this->db->select(array('posts.*','users.user_id','users.users_country','users.users_state','users.users_city','users.users_area'));
        $this->db->from('posts');
        $this->db->join("users","users.user_id = posts.posts_user_id");
        if(!empty($country)) $this->db->where('users_country',$country);
        if(!empty($state)) $this->db->where('users_state',$state);
        if(!empty($district)) $this->db->where('users_city',$district);
        if(!empty($area)) $this->db->where('users_area',$area);
        $res = $this->db->get();
        return ($res->num_rows() > 0) ?  $res->result_array() : 0;
    }

    // get posts api for sort
    function getPostsbysort($country = null , $state = null , $district = null , $area = null){

        $this->db->select(array("posts.*","users.users_name"));
        $this->db->where(array('posts_active'=>1,'posts_type'=>1));
        $this->db->order_by("posts_id","DESC");
        $this->db->from("posts");
        $this->db->join("users","users.user_id=posts.posts_user_id");
        if(!empty($country)) $this->db->where('users_country',$country);
        if(!empty($state)) $this->db->where('users_state',$state);
        if(!empty($district)) $this->db->where('users_city',$district);
        if(!empty($area)) $this->db->where('users_area',$area);
        $res = $this->db->get();
        if($res->num_rows()>0){
        $return['data'] =  $res->result_array();
        $return['count'] = $res->num_rows();
            return $return;
        }
        else{
            return null;
        }
    }

      // get posts api
      function getPosts($frndid = null){

        $this->db->select(array("posts.*","users.users_name","users.users_photo"));
        $this->db->where(array('posts_active' => 1,'posts_type' => 1));
        $this->db->where('posts_user_id',$frndid);
        $this->db->order_by("posts_id","DESC");
        $this->db->where('status', 1);
        $this->db->from("posts");
        $this->db->join("users","users.user_id = posts.posts_user_id");
        $res = $this->db->get();
        $return['data']  =  $res->result_array();
        $return['count'] = $res->num_rows();
        return $return;
    }

    // get posts by id
    function getnotificationPostsbyid($posts_id = null){

    $this->db->select(array("posts.*","users.users_name","users.users_photo"));
    $this->db->where(array('posts_id' => $posts_id,'posts_active' => 1,'posts_type' => 1));
    $this->db->order_by("posts_id","DESC");
    $this->db->from("posts");
    $this->db->join("users","users.user_id = posts.posts_user_id");
    $res = $this->db->get();
   
    return $res->num_rows() > 0 ? $res->result_array() : 0;
    }

    // edit posts by id
    function editPosts($postid = null, $update = null){

        $this->db->where('posts_id',$postid);
        $this->db->update('posts',$update);
        return $this->db->affected_rows();

    }

     // edit posts by id
     function deletePosts($postid = null){

        $this->db->where('posts_id',$postid);
        $update = array('posts_active' => 0);
        $this->db->update('posts',$update);
        return $this->db->affected_rows();

    }
    // function delete notifications after block or delete posts
    function deletenotifications($postid = null){

        $this->db->where('notifications_post_id',$postid);
        $this->db->delete('notifications');
    }

    // get reported posts by userid
    function getReportedposts($userid = null){

        $this->db->select('DISTINCT(posts_report_post_id)');
        $this->db->where('posts_report_user_id',$userid);
        $res = $this->db->get('posts_report');
        return $res->result_array();
    }
    // get reported challenge by userid
    function getReportedchallenge($userid = null){

        $this->db->select('DISTINCT(challenge_report_challenge_id)');
        $this->db->where('challenge_report_user_id',$userid);
        $res = $this->db->get('challenge_report');
        return $res->result_array();
    }

    

      // get reported posts by userid
      function gethiddenposts($userid = null){

        $this->db->select('DISTINCT(hide_posts_post_id)');
        $this->db->where('hide_posts_user_id',$userid);
        $this->db->where('hide_posts_type',1);
        $res = $this->db->get('hide_posts');
        return $res->result_array();
    }


     // get reported posts by userid
     function getpartyReportedposts($userid = null){

        $this->db->select('DISTINCT(party_posts_report_post_id)');
        $this->db->where('party_posts_report_user_id',$userid);
        $res = $this->db->get('party_posts_report');
        return $res->result_array();
    }

    

     // get hidden posts by userid
     function getpartyhiddenposts($userid = null){

        $this->db->select('DISTINCT(hide_posts_post_id)');
        $this->db->where('hide_posts_user_id',$userid);
        $this->db->where('hide_posts_type',2);
        $res = $this->db->get('hide_posts');
        return $res->result_array();
    }
    

    // function get post polled or not
    function isPostpolled($postid = null ,$userid = null){

        $this->db->where(array('posts_polls_post_id' => $postid , 'posts_polls_user_id' => $userid));
        return $this->db->get('posts_polls')->num_rows();
    }

    // check is friend or not
    function isFriend($userid = null , $loggeduserid = null){

        $query = "SELECT * FROM users_followers WHERE `users_followers_users_id` = $userid AND `users_followers_follower_id` = $loggeduserid AND `users_followers_active` = 1 OR  `users_followers_users_id` = $loggeduserid AND `users_followers_follower_id` = $userid AND `users_followers_active` = 1";
        $res   = $this->db->query($query);
        return $res->num_rows();  
    }

      // check is friend or not
      function getFriends($userid = null){

        $query = "SELECT  users_followers_users_id  as friend_id FROM users_followers WHERE (`users_followers_users_id` != '$userid' AND `users_followers_follower_id` = '$userid' AND `users_followers_active` = 1)"; //`users_followers_users_id` = $userid OR 
        $res   = $this->db->query($query);
        return $res->result_array();  
    }
    
    // hide posts
    function hidePosts($postid = null, $userid = null){

        $insert= array('hide_posts_post_id' => $postid , 'hide_posts_user_id' =>$userid , 'hide_posts_type' => 1);
        $this->db->insert('hide_posts',$insert);
        return $this->db->affected_rows();
    }

    // get users name by id
    function getusersnamebyid($id = null){

        $this->db->select('users_name');
        $this->db->where('user_id',$id);
        $res = $this->db->get('users');
        return $res->row('users_name');
    }

    //get state posts
    function getStatePosts($state = null){

        $this->db->select('posts.*,posts_polls.*,COUNT(posts_polls_post_id) as total');
        $this->db->where(array('posts_state' => $state , 'posts_active' => 1));
        $this->db->join('posts_polls','posts_polls.posts_polls_post_id = posts.posts_id');
        $this->db->group_by('posts_polls_post_id');
        $this->db->order_by('total','desc');
        $res = $this->db->get('posts');
        return $res->result_array();
    }


     //get state posts
     function getGlobalPosts(){

        $this->db->select('posts.*,posts_polls.*,COUNT(posts_polls_post_id) as total');
        $this->db->where(array('posts_active' => 1));
        $this->db->join('posts_polls','posts_polls.posts_polls_post_id = posts.posts_id');
        $this->db->group_by('posts_polls_post_id');
        $this->db->order_by('total','desc');
        $res = $this->db->get('posts');
        return $res->result_array();
    }

    // get national posts
    function getNationalPosts($country = null){

        $this->db->select('posts.*,posts_polls.*,COUNT(posts_polls_post_id) as total');
        $this->db->where(array('posts_country' => $country,'posts_active' => 1));
        $this->db->join('posts_polls','posts_polls.posts_polls_post_id = posts.posts_id');
        $this->db->group_by('posts_polls_post_id');
        $this->db->order_by('total','desc');
        $res = $this->db->get('posts');
        return $res->result_array();
    }

      // get national posts
      function getDistrictPosts($city = null){

        $this->db->select('posts.*,posts_polls.*,COUNT(posts_polls_post_id) as total');
        $this->db->where(array('posts_city' => $city,'posts_active' => 1));
        $this->db->join('posts_polls','posts_polls.posts_polls_post_id = posts.posts_id');
        $this->db->group_by('posts_polls_post_id');
        $this->db->order_by('total','desc');
        $res = $this->db->get('posts');
        return $res->result_array();
    }
    



   // get postuser name
   function getusersnamebypostid($id = null){

    $this->db->select('users_name');
    $this->db->where('user_id',$id);
    $res = $this->db->get('users');
    return $res->row('users_name');
   }

    // get postuser name
   function getusersphotobypostid($id = null){

    $this->db->select('users_photo');
    $this->db->where('user_id',$id);
    $res = $this->db->get('users');
    return $res->row('users_photo');
   }

   // insert posts tags
   function insert_tags($tag = null){
       if($this->check_tag($tag)==0)
       $this->db->insert('posts_tags',array('posts_tags_name' => $tag));
   }

   // check tag exists or not
   function check_tag($tag){
       $res = $this->db->get_where('posts_tags',array('posts_tags_name'=>$tag));
       return $res->num_rows();
   }

   //get tags
   function getTags($search = null){

        $this->db->like('posts_tags_name' , $search);
        $this->db->where('posts_tags_status' , 1);
        $res = $this->db->get('posts_tags');
        return $res->result();
   }

   //get posts by tags
   function getPostsbyTags($search = null){

        $this->db->like('posts_tags' , $search);
        $res = $this->db->get('posts');
        return $res->result_array();
   }
  

   //remove post by report count
   function removePostbyCount($id = null){
       if($this->check_report_count($id) > 0){
        $this->db->where('posts_id', $id);
        $this->db->update('posts',array('posts_active'=>0));
       }
       else{
           return false;
       }
   }

   //check report count
   function check_report_count($id){
    $this->db->where('posts_id', $id);
    $this->db->where('posts_report_count >=', 5);   
    $res = $this->db->get('posts');
    return $res->num_rows();
}

// check user blocked ot not
function check_isBlocked($where = null){
    $this->db->select('users_active');
    $this->db->where($where);
    return $this->db->get('users')->num_rows();
}

 // change converted posts status
 function updatePostsStatus($file_name){

        $this->db->set('status', 1);
        $this->db->where('posts_photo_or_video',$file_name);
       $this->db->update('posts');

  }
  // get posts file status
  function getPostsStatus($id){

        $this->db->select('status');
        $this->db->from('posts');
        $this->db->where('posts_id',$id);
        $res = $this->db->get();
        return $res->row();
   }

    function deletePostsComment($post_id,$userid,$commentid,$reply_id){

        if ($commentid != null) { 

           $this->db->where(array('posts_comments_post_id' => $post_id , 'posts_comments_user_id' => $userid , 'posts_comments_id' => $commentid));
            $this->db->update('posts_comments',array('posts_comments_active' => 0));
            return 1;

        }else{ 

            $this->db->where(array('posts_comments_reply_posts_id' => $post_id , 'posts_comments_reply_user_id' => $userid , 'posts_comments_reply_id' => $reply_id));
            $this->db->delete('posts_comments_reply');
            return 1;
        }
        
   }

   function updatePostsCommentStatus($post_id,$user_id,$status){ 

        
        if ($status == 'false') {  
            
             $this->db->where(array('posts_id' => $post_id , 'posts_user_id' => $user_id ));
             $this->db->update('posts',array('post_comments_enable' => 0)); 
             return 1;
             
        }else{  

             $this->db->where(array('posts_id' => $post_id , 'posts_user_id' => $user_id ));
             $this->db->update('posts',array('post_comments_enable' => 1)); 
             return 0;
        }

   }
   function isPostCommentEnabled($postid){

        $this->db->select('post_comments_enable');
        $this->db->from('posts');
        $this->db->where('posts_id',$postid);
        $res = $this->db->get();
        return $res->result();
   }

}



