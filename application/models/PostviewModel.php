<?php

class PostviewModel extends CI_Model{



    function getPostsbyid($id = null){

        $res = $this->db->get_where('posts',array('posts_id'=>$id));
        return $res->result_array();

    }

    function getpartyPostsbyid($id = null){

        $res = $this->db->get_where('party_posts',array('party_posts_id'=>$id));
        return $res->result_array();

    }

    function getgroupPostsbyid($id = null){

        $res = $this->db->get_where('group_posts',array('group_posts_id'=>$id));
        return $res->result_array();

    }

    function getchallengebyid($id = null){

        $this->db->select('challenge_posts.*,challenge.challenge_content,challenge.challenge_title,challenge.challenge_type,challenge.challenge_created_date');
        $this->db->join('challenge','challenge.challenge_id = challenge_posts.challenge_posts_challenge_id');
        $res = $this->db->get_where('challenge_posts',array('challenge_posts_id'=>$id));
        return $res->result_array();

    }

    function getPollcount($id = null){

        $res = $this->db->get_where('posts_polls',array('posts_polls_post_id'=>$id));
        return $res->num_rows();

    }

    function getcommentcount($id = null){

        $res = $this->db->get_where('posts_comments',array('posts_comments_post_id'=>$id));
        return $res->num_rows();

    }


    function getpartyPollcount($id = null){

        $res = $this->db->get_where('party_posts_polls',array('party_posts_polls_post_id'=>$id));
        return $res->num_rows();

    }

    function getpartycommentcount($id = null){

        $res = $this->db->get_where('party_posts_comments',array('party_posts_comments_post_id'=>$id));
        return $res->num_rows();

    }

    function getgroupPollcount($id = null){

        $res = $this->db->get_where('group_posts_polls',array('group_posts_polls_post_id' => $id));
        return $res->num_rows();

    }

    function getgroupcommentcount($id = null){

        $res = $this->db->get_where('group_posts_comments',array('group_posts_comments_post_id' => $id));
        return $res->num_rows();

    }
    

    function getchallengePollcount($id = null){

        $res = $this->db->get_where('challenge_polls',array('challenge_polls_challenge_id' => $id));
        return $res->num_rows();

    }

    function getchallengePostPollcount($id = null){

        $res = $this->db->get_where('challenge_post_polls',array('challenge_polls_challenge_post_id' => $id));
        return $res->num_rows();

    }

    function getchallengecommentcount($id = null){

        $res = $this->db->get_where('challenge_comments',array('challenge_comments_challenge_id' => $id));
        return $res->num_rows();

    }

    function getchallengePostcommentcount($id = null){

        $res = $this->db->get_where('challenge_posts_comments',array('challenge_posts_comments_challenge_id' => $id));
        return $res->num_rows();

    }

    function getBusinessProfilePostsbyid($id = null){

        $res = $this->db->get_where('business_profile_posts',array('bp_posts_id'=>$id));
        return $res->result_array();

    }

    function getBusinessProfilePostsPollcount($id = null){

        $res = $this->db->get_where('business_profile_posts_polls',array('bp_posts_polls_post_id' => $id));
        return $res->num_rows();

    }

    function getBusinessProfilePostscommentcount($id = null){

        $res = $this->db->get_where('business_profile_posts_comments',array('bp_posts_comments_post_id' => $id));
        return $res->num_rows();

    }


    function getBusinessProfilechallengebyid($id = null){

        $this->db->select('business_profile_challenge_posts.*,business_profile_challenge.*');
        $this->db->join('business_profile_challenge','business_profile_challenge.challenge_id = business_profile_challenge_posts.bp_challenge_posts_challenge_id');
        $res = $this->db->get_where('business_profile_challenge_posts',array('bp_challenge_posts_id'=>$id));
        return $res->result_array();

    }


    function getBusinessProfilechallengePollcount($id = null){

        $res = $this->db->get_where('business_profile_challenge_post_polls',array('bp_challenge_polls_challenge_post_id' => $id));
        return $res->num_rows();

    }

    function getBusinessProfilechallengecommentcount($id = null){

        $res = $this->db->get_where('business_profile_challenge_posts_comments',array('bp_challenge_posts_comments_challenge_id' => $id));
        return $res->num_rows();

    }
    

}
?>