<?php
/**
 * Created by PhpStorm.
 * User: user56
 * Date: 06-08-2018
 * Time: 12:56
 */

class UserModel extends CI_Model{

    // admin login
    function check_user($username = null,$password = null){

        $select = array('admin_id','admin_role','admin_username','admin_firstname','admin_lastname','admin_photo','admin_logdate');
        $this->db->select($select);
        $check  = array('admin_username'=>$username,'admin_password'=>$password);
        $this->db->where($check);
        $res    = $this->db->get('admin');
        if($res->num_rows()>0){

            $return['data']  = $res->result_array();
        }
        else{
            $return['error'] = "no data found on database";
        }
        return $return;

    }

   



    // get all users
    function getallUsers($page = null ,$search = null,$option = null,$select = null , $from = null , $to = null , $country = null , $state =null , $district = null){

        $pagesize = 9;
        $offset = ($page - 1) * $pagesize;

        $this->db->limit($pagesize, $offset);
        


        if(!empty($select)){
            
            $this->db->where('users_party_id', $select);
        }

        if(!empty($from)){
            
            $this->db->where('users_created_date >=', $from);
            $this->db->where('users_created_date <=', $to);
        }
        
        if(!empty($country)){
            $this->db->where('users_country', $country);
        }
        if(!empty($state)){
            $this->db->where('users_state', $state);
        }
        if(!empty($district)){
            $this->db->where('users_city', $district);
        }
        
        if(!empty($search)){

            $this->db->like('users_name',$search);

        }

    //    if(!empty($option)) {

           $this->db->where('users_active', $option);
           $this->db->where('user_id!=', 1);

    //    }
      
       
        $this->db->from('users');
        $this->db->join('party',"party.party_id = users.users_party_id");
        $res = $this->db->get();
        if($res->num_rows()>0){

            $return['data']      = $res->result_array();
            $return['count']     = $res->num_rows();
            $return['query']     = $this->db->last_query();   
        }
        else{
            $return['error'] = "not found";
        }
        return $return;
    }

    // get all users
    function getallUsersCount($page = null ,$search = null,$option = null,$select = null , $from = null , $to = null , $country = null , $state =null , $district = null){

        
        if(!empty($select)){
            
            $this->db->where('users_party_id', $select);
        }

        if(!empty($from)){
            
            $this->db->where('users_created_date >=', $from);
            $this->db->where('users_created_date <=', $to);
        }
        
        if(!empty($country)){
            $this->db->where('users_country', $country);
        }
        if(!empty($state)){
            $this->db->where('users_state', $state);
        }
        if(!empty($district)){
            $this->db->where('users_city', $district);
        }
        
        if(!empty($search)){

            $this->db->like('users_name',$search);

        }

    //    if(!empty($option)) {

           $this->db->where('users_active', $option);
           $this->db->where('user_id!=', 1);

    //    }
      
       
        $this->db->from('users');
        $this->db->join('party',"party.party_id = users.users_party_id");
        $res = $this->db->get();
        return $res->num_rows();
    }
    
    // get user details pass id
    function getuserDetails($id = null){
        
        $res = $this->db->get_where('users',array('user_id'=>$id));
        return $res->result_array();
    }
    
    // get followers pass id
    function getFollowers($id = null){
        $res = $this->db->get_where('users_followers',array('users_followers_users_id'=>$id,'users_followers_active'=>1));
        return $res->num_rows();
    }
    
    //get posts count
    function getPostscount($id = null){
         $res = $this->db->get_where('posts',array('posts_user_id'=>$id));
         return $res->num_rows();
    }

    //block user
    function blockuser($id = null){

        $this->db->where('user_id',$id);
        $this->db->update('users',array('users_active' => 0));
        return $this->db->affected_rows(); 

    }

    function unblockuser($id = null){

        $this->db->where('user_id',$id);
        $this->db->update('users',array('users_active' => 1));
        return $this->db->affected_rows(); 
    }

    //getCountry
    function getCountry(){

        $res = $this->db->get('countries');
        return ($res->num_rows() > 0) ?  $res->result_array() : 0;
    }

    // get state by id
    function getstatebyid($cid = null){

        $this->db->where('country_id',$cid);
        $res = $this->db->get('states');
        return $res->result_array(); 
    }

     // get state by id
     function getdistrictbyid($cid = null){

        $this->db->where('state_id',$cid);
        $res = $this->db->get('cities');
        return $res->result_array(); 
    }

     // get state name
     function getstatename($id = null){

        $this->db->where('id',$id);
        return $this->db->get('states')->row('name');
    }

    // get country name
    function getcountryname($id = null){

        $this->db->where('id',$id);
        return $this->db->get('countries')->row('name');
    }
}