<?php
/**
 * Created by PhpStorm.
 * User: Amal
 * Date: 8/9/2018
 * Time: 2:24 PM
 */

class UsersApi extends CI_Controller{
    
    public $media_url= "https://d.daypoll.com/";
    
    function __construct()
    {
        error_reporting(0);
        parent::__construct();
        $this->load->model(array('UsersApiModel','PostsApiModel','CommonModel'));
        $this->load->library('form_validation');
        $this->load->library('aws3');


    }

    //check user token
    function check_token($id , $token){
        
        if(!empty($id && $token)){
        $res    = $this->CommonModel->check_token($id , $token);
        return  $res > 0 ? 1 : 0;
        }
       
    }

    // country phone code
    function getCountrycode(){

        $this->output->set_content_type('application/json'); 
        $res = $this->UsersApiModel->getCountrycode();
        if($res != 0){
            $this->output->set_output(json_encode(array('value'=>$res , 'status'=>true)));
        }
        else{
            $this->output->set_output(json_encode(array('value'=>'no value found','status'=>false)));
        }
        
    }



    function isVerified(){

        $this->output->set_content_type('application/json');
        
        $mobile       = $this->input->post('mobile');
        $mob_code     = $this->input->post('code');
        $flag         = $this->input->post('flag'); 
        $userid       = $this->input->post('userid'); 
       

        if(!empty($mobile)){
        $res = $this->UsersApiModel->isVerified($mobile , $mob_code , $flag , $userid);

        if($res > 0){

            $this->output->set_output(json_encode(array('message'=>'succesfully updated' , 'status'=>true)));
        }
        else{
            $this->output->set_output(json_encode(array('message'=>'failed to update' , 'status'=>false)));
        }
        }
        else{

        $this->output->set_output(json_encode(array('message'=>'parameter missing' , 'status'=>false)));
        
        }
    }

    
   


    function sendMsg(){

    $this->output->set_content_type('application/json');
    $mobile       = $this->input->post('mobile');
    $mob_code     = !empty($this->input->post('code')) ? $this->input->post('code') : '91';

    
    if(!empty($mobile)){

    
    $res = $this->UsersApiModel->checkMobilenum($mobile);
    if($res['count'] > 0){

        $this->output->set_output(json_encode(array('message'=>'Mobile already registered','status'=>false)));
 
    }
    else{



        /*   MSG 91 */


        $authKey = "262246AXEIhGiy1xc5c610ffa";
        $mobileNumbers = $mob_code.$mobile;
        
        
        
        $senderId = "DAYPOLL";
        $otp        = mt_rand(1000,9999);
        $message = urlencode($otp . "  is your OTP for verifying your phone No. on daypoll. Enjoy the new social media experience");
        $route="4";
        
        $postData = array('authkey' => $authKey,'mobiles' => $mobileNumbers,'message' => $message,'sender' => $senderId,'route' =>$route);
        $url="http://api.msg91.com/api/sendhttp.php";
        $ch = curl_init();
        curl_setopt_array($ch, array(CURLOPT_URL => $url,CURLOPT_RETURNTRANSFER => true,CURLOPT_POST => true,CURLOPT_POSTFIELDS => $postData));
        $output = curl_exec($ch);
        
        if(curl_errno($ch)){
            $this->output->set_output(json_encode(array('message'=>'failed to send','error' => curl_error($ch),'status'=>false)));
        //echo 'error:' . curl_error($ch);
        }
        else{
            $this->output->set_output(json_encode(array('message'=>'otp send','otp'=>$otp,'status'=>true)));
        }
        curl_close($ch);
        //echo $output;





    // // /ng8LB7Tk6Y-F5p1dp6EQjmoUL6nGXOehmv3j8Sup5

    // // Account details
	// $apiKey = urlencode('/ng8LB7Tk6Y-F5p1dp6EQjmoUL6nGXOehmv3j8Sup5');
	
	// // Message details
	// $numbers    = array($mobile);  
    // $sender     = urlencode('TXTLCL');
    // $otp        = "4455";//mt_rand(1000,9999);
    
	// $message    = rawurlencode($otp.' is your verification code to login to your Daypoll Account.');
 
	// $numbers = implode(',', $numbers);
 
	// // Prepare data for POST request
	// $data = array('apikey' => $apiKey,'otp'=>$otp,'numbers' => $numbers, "sender" => $sender, "message" => $message);
 
	// // Send the POST request with cURL
	// // $ch = curl_init('https://api.textlocal.in/send/');
	// // curl_setopt($ch, CURLOPT_POST, true);
	// // curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	// // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// // $response = curl_exec($ch);
	// // curl_close($ch);
	// $this->output->set_output(json_encode(array('message'=>'otp send','otp'=>$otp,'status'=>true)));
         
	//Process your response here
    //echo $response;
    

     }
    }
    else{
        $this->output->set_output(json_encode(array('message'=>'parameter missing' , 'status'=>false))); 
    }

    }


    
    function test(){

        $header = getallheaders();
        $id     = $header['id'];
        $token  = $header['token'];
        if($this->check_token($id , $token) == 0) print 0;
        else print 1;
      
    }

    //check username exists
    function checkUsername(){
        
        $this->output->set_content_type('application/json');
        $username       = $this->input->post('username');

        if(!empty($username)){

            $res    = $this->UsersApiModel->checkUsername($username);
            
            
            if($res == 1){

                $this->output->set_output(json_encode(array('message'=>'username availiable','status'=>'true')));
            }
            else{
                $available = $username.mt_rand(100,999);
                $this->output->set_output(json_encode(array('message'=>$username.' already taken','available'=>$available,'status'=>'false')));
            }
         }
        else{
            $this->output->set_output(json_encode(array('message'=>'parameter missing','status'=>'false')));
        }

    }



    // register user
    function registerUser(){                                                                       

        $this->output->set_content_type('application/json');

        $type       = $this->input->post('type');
        
        if($type == 0){
        
    // user details

        $name       = $this->input->post('name');
        $email      = $this->input->post('email');
        $mobile     = $this->input->post('mobile');
        $dob        = $this->input->post('dob');
        $gender     = $this->input->post('gender');
        $mob_code   = $this->input->post('code');
        
    // location details
        
        $country  = $this->input->post('country');
        $state    = $this->input->post('state');
        $city     = $this->input->post('city');
        $loctype  = $this->input->post('locationtype');    //     1 => manual , 0 => geo location
        $area     = $this->input->post('area');
       


    // save password
    
        
        $username    = $this->input->post('username');
        $password    = $this->input->post('password');
        $cpassword   = $this->input->post('cpassword');
         
            
        $this->form_validation->set_rules("name","name","required");
        $this->form_validation->set_rules("email","email","required");
        $this->form_validation->set_rules("mobile","mobile","required");
        $this->form_validation->set_rules("dob","dob","required");
        $this->form_validation->set_rules("gender","gender","required");
        
        $this->form_validation->set_rules("username","username","required");
        $this->form_validation->set_rules("password","password","required");
        $this->form_validation->set_rules("cpassword","cpassword","required");
       
        

        if($this->form_validation->run()==TRUE){

            $token  = md5($name.mt_rand(100000, 999999));
            
            if($cpassword == $password){

            $insert = array('users_login_type' => 0,'users_name'=>$name,
                            'users_email'=>$email,'users_mobile'=>$mobile,
                            'users_mobile_code' => $mob_code,'users_dob'=>$dob,
                            'users_gender'=>$gender,'users_country'=>$country,
                            'users_state'=>$state,'users_city'=>$city,'users_area'=>$area,
                            'users_token' => $token ,'users_username'=>$username,
                            'users_password'=>md5($password),'users_isprofile_complete'=>$loctype,
                            'users_isactivated'=>1
                            );

            $res    = $this->UsersApiModel->registeruser($insert);
            
            $insert['country_id']  = $this->UsersApiModel->getCountryid($country);
            $insert['state_id']    = $this->UsersApiModel->getStateid($state);
                
            $insert['users_photo'] = $this->media_url."assets/images/users/none.png";
            
            
            if($res > 0){

                //insert new area
                $this->UsersApiModel->check_area_exist($insert['state_id'],$area);
                $this->output->set_output(json_encode(array('message'=>"successfully inserted",'token'=>$token,'res' => $insert,'status'=>'true')));
            }
            else if($res == -1){

                $this->output->set_output(json_encode(array('message'=>"user already exits",'status'=>'false')));
            }
            else{
                $this->output->set_output(json_encode(array('message'=>"failed to insert",'status'=>'false')));
            }
        }
        else{
            $this->output->set_output(json_encode(array('message'=>"password doesnt match",'status'=>'false')));
          
        }
            }
        else{

            $this->output->set_output(json_encode(array('message'=>'parameter missing','status'=>'false')));
        }

    }

    else if($type == 1){

        $email_other  = $this->input->post('email');
        $name_other   = $this->input->post('name');
        $profile_pic  = $this->input->post('profile_pic');
        $country      = $this->input->post('country');
        $state        = $this->input->post('state');
        $city         = $this->input->post('city');

        if($profile_pic == NULL){
            $profile_pic = 'none.png';
        }

        if(!empty($email_other && $name_other)){
        $token_other  = md5($name_other.mt_rand(100000, 999999));

        $insert       = array('users_country' => $country , 'users_state' => $state ,'users_city' => $city,'users_photo' => $profile_pic,'users_login_type'=>1,'users_name'=>$name_other,'users_email'=>$email_other,'users_token'=>$token_other);


        $res    = $this->UsersApiModel->registerusersothers($insert);

        if($res['count'] > 0){

            $res_get_user                 = $this->UsersApiModel->get_user_by_token($token_other);
            
            foreach($res_get_user as $k => $v){
                $res_get_user[$k]['country_id']   = $this->UsersApiModel->getCountryid($country);
                $res_get_user[$k]['state_id']     = $this->UsersApiModel->getStateid($state);
                $res_get_user[$k]['users_photo']  = $this->media_url."assets/images/users/".$res_get_user[$k]['users_photo'];
            }

            $this->output->set_output(json_encode(array('message' =>$res_get_user , 'res'=>"successfully inserted",'token'=>$token_other,'status'=>'true')));
        }
        else if($res['count'] == -1){
            
            foreach($res['data'] as $ke => $ve){
                
                $res['data'][$ke]['country_id']    = $this->UsersApiModel->getCountryid($country);
                $res['data'][$ke]['state_id']      = $this->UsersApiModel->getStateid($state);
                $res['data'][$ke]['users_photo']   = $this->media_url."assets/images/users/".$res['data'][$ke]['users_photo'];
            }

            $this->output->set_output(json_encode(array('message'=>$res['data'],'res' => 'login success','status'=>'true')));
        }
        else{

            $this->output->set_output(json_encode(array('message'=>"operation failed",'status'=>'false')));
        }
        }
        else{

            $this->output->set_output(json_encode(array('message'=>"parameter missing",'status'=>'false')));
        }

        }
}
    

    // register users
    function registerUsers(){                                                                       

        $this->output->set_content_type('application/json');

        $type       = $this->input->post('type');
        
        if($type == 0){

        $name       = $this->input->post('name');
        $email      = $this->input->post('email');
        $mobile     = $this->input->post('mobile');
        $dob        = $this->input->post('dob');
        $gender     = $this->input->post('gender');
        $mob_code   = $this->input->post('code');
       

        $this->form_validation->set_rules("name","name","required");
        $this->form_validation->set_rules("email","email","required");
        $this->form_validation->set_rules("mobile","mobile","required");
        $this->form_validation->set_rules("dob","dob","required");
        $this->form_validation->set_rules("gender","gender","required");
       

        if($this->form_validation->run()==TRUE){

            $token  = md5($name.mt_rand(100000, 999999));
            
            $insert = array('users_login_type' => 0,'users_name'=>$name,'users_email'=>$email,
                'users_mobile' => $mobile,'users_mobile_code' => $mob_code,'users_dob'=>$dob,'users_gender'=>$gender,'users_token'=>$token);

            $res    = $this->UsersApiModel->registerusers($insert);

            if($res > 0){

                $this->output->set_output(json_encode(array('message'=>"successfully inserted",'token'=>$token,'status'=>'true')));
            }
            else if($res == -1){

                $this->output->set_output(json_encode(array('message'=>"user already exits",'status'=>'false')));
            }
            else{
                $this->output->set_output(json_encode(array('message'=>"failed to insert",'status'=>'false')));
            }

            }
        else{

            $this->output->set_output(json_encode(array('message'=>'parameter missing','status'=>'false')));
        }

    }
    else if($type == 1){

        $email_other  = $this->input->post('email');
        $name_other   = $this->input->post('name');

        if(!empty($email_other && $name_other)){
        $token_other  = md5($name_other.mt_rand(10000, 99999));

        $insert       = array('users_login_type'=>1,'users_login_token'=> $token_other,'users_name'=>$name_other,'users_email'=>$email_other,'users_token'=>$token_other);


        $res    = $this->UsersApiModel->registerusersothers($insert);

        if($res['count'] > 0){

            $res_get_user    = $res['data'];  //$this->UsersApiModel->get_user_by_token($token_other);
            
            foreach($res_get_user as $k => $v){

                $res_get_user[$k]['users_photo'] = $this->media_url."assets/images/users/".$res_get_user[$k]['users_photo'];
            }

            $this->output->set_output(json_encode(array('message' =>$res_get_user , 'res'=>"successfully inserted",'user_id' => $res['count'],'users_login_token' => $token_other,'token'=>$token_other,'status'=>'true')));
        }
        else if($res['count'] == -1){
            
            foreach($res['data'] as $ke => $ve){
                
                $res['data'][$ke]['users_photo'] = $this->media_url."assets/images/users/".$res['data'][$ke]['users_photo'];
            }

            $this->output->set_output(json_encode(array('message'=>$res['data'],'res' => 'login success','status'=>'true')));
        }
        else{

            $this->output->set_output(json_encode(array('message'=>"operation failed",'status'=>'false')));
        }
        }
        else{

            $this->output->set_output(json_encode(array('message'=>"parameter missing",'status'=>'false')));
        }

        }

    }


     //save location by token after login with facebook or google
     function saveLocation(){

        $this->output->set_content_type('application/json');
        
        $token      = $this->input->post('token');
        $userid     = $this->input->post('userid');
        $country    = $this->input->post('country');
        $state      = $this->input->post('state');
        $district   = $this->input->post('district');
        $area       = $this->input->post('area');

        $type       = $this->input->post('type') == 1 ? 1 : 0;               // 0 => geo location , 1 => manual entry
        
        

        if(!empty($token)){
        
            
        $update     = array('users_country' => $country , 'users_state' => $state , 'users_city' => $district ,'users_area' => $area ,'users_isprofile_complete' => $type); 
        $res        = $this->UsersApiModel->saveLocation($token , $update);
            
        if($res > 0){
            $this->UsersApiModel->updatepartypolls($token);
            //$this->UsersApiModel->removepartyuserfollowers($userid);
            $this->output->set_output(json_encode(['value' =>'succesfully updated' ,'status' => true]));
        }
        else{

            $this->output->set_output(json_encode(['value' =>"succesfully updated" , 'status' => true]));
        }
    }
    else{
        $this->output->set_output(json_encode(['value' =>"parameter missing" , 'status' => false]));
       
        }
    }
    
    //get country api
    function getCountry(){

        $this->output->set_content_type('application/json');
        $res = $this->UsersApiModel->getCountry();

        if($res != 0){

            $this->output->set_output(json_encode(['value' =>$res ,'status' => true]));
        }
        else{

            $this->output->set_output(json_encode(['value' =>"no data found" , 'status' => false]));
        }
    }

    //get state api
    function getState(){

        $this->output->set_content_type('application/json');
        $cid    = $this->input->post('country_id');
        $res    = $this->UsersApiModel->getState($cid);

        if($res != 0){
            
            $this->output->set_output(json_encode(['value' =>$res ,'status' => true]));
        }
        else{

            $this->output->set_output(json_encode(['value' =>"no data found" , 'status' => false]));
        }
    }

     //get city api
     function getCity(){

        $this->output->set_content_type('application/json');
        $cid    = $this->input->post('state_id');
        
        
        $res    = $this->UsersApiModel->getCity($cid);

        if($res != 0){
            
            foreach($res as $val => $key){
                
                $res[$val]['name'] = strtolower($res[$val]['name']);
                $res[$val]['name'] = ucfirst($res[$val]['name']);
            }
            $this->output->set_output(json_encode(['value' =>$res ,'status' => true]));
        }
        else{

            $this->output->set_output(json_encode(['value' =>"no data found" , 'status' => false]));
        }
    }
    
    //register user location
    function registerLocation(){
        $this->output->set_content_type('application/json');
        
        $country  = $this->input->post('country');
        $state    = $this->input->post('state');
        $city     = $this->input->post('city');
        //$ward     = $this->input->post('ward');
        $area     = $this->input->post('area');
        $token    = $this->input->post('token');
        
        $this->form_validation->set_rules("country","country","required");
        $this->form_validation->set_rules("state","state","required");
        $this->form_validation->set_rules("city","city","required");
        //$this->form_validation->set_rules("ward","ward","required");
        $this->form_validation->set_rules("area","area","required");
        $this->form_validation->set_rules("token","token","required");
       

         if($this->form_validation->run() == TRUE){
             
              $insert = array('users_country'=>$country,'users_state'=>$state,
                               'users_city'=>$city,'users_area'=>$area
                             );

              $res    = $this->UsersApiModel->registerLocation($token,$insert);
              
              if($res > 0){
                  
                   $this->output->set_output(json_encode(['message' =>"succesfuly inserted",'status'=>"true"]));
                   
              }
              else{
                  
                   $this->output->set_output(json_encode(['message' =>"failed to insert",'status'=>"false"]));
              }
         }
         else{
            
        $this->output->set_output(json_encode(['message' =>"parameter missing",'status'=>"false"]));
        }
    }
    
    //save password
    function savePassword(){
        
        $this->output->set_content_type('application/json');
        
        $username    = $this->input->post('username');
        $password    = $this->input->post('password');
        $cpassword   = $this->input->post('cpassword');
        $token       = $this->input->post('token');
        
        $this->form_validation->set_rules("username","username","required");
        $this->form_validation->set_rules("password","password","required");
        $this->form_validation->set_rules("cpassword","cpassword","required");
        $this->form_validation->set_rules("token","token","required");
        
         if($this->form_validation->run() == TRUE){
            
            if($password == $cpassword){
                
                $insert = array('users_username'=>$username,'users_password'=>md5($password));
                $res    = $this->UsersApiModel->savePassword($token,$insert);
                
                if($res > 0){
               $this->output->set_output(json_encode(['message' =>"succesfully updated",'status'=>"true"]));
                }
                else {
                
               $this->output->set_output(json_encode(['message' =>"failed to insert",'status'=>"false"]));
               }
            }
                else {
               $this->output->set_output(json_encode(['message' =>"passwords not equal",'status'=>"false"]));
            }
                } 
                else { 
               $this->output->set_output(json_encode(['message' =>"parameter missing",'status'=>"false"]));
         }
        
    }
    
    //otp verification
    function otpVerification(){
        
        $this->output->set_content_type('application/json');
        $mobile   = $this->input->post('mobile');
        $this->form_validation->set_rules("mobile","mobile","required");
        
        if($this->form_validation->run() == TRUE){
            
            $check  = array('users_mobile'=>$mobile);
            $res    = $this->UsersApiModel->getToken($check);
            
            if(!empty($res)){
                
                 $this->output->set_output(json_encode(['message' =>"value found","token"=>$res,'status'=>"true"]));
                 
            }
            else{
                
                 $this->output->set_output(json_encode(['message' =>"no value found",'status'=>"false"]));
            }
                
            }
        else{
                 $this->output->set_output(json_encode(['message' =>"parameter missing",'status'=>"false"]));
        }
        
    }
    
    
    
    // return token based on token data parse
    function loginUsers(){                   
        
        $this->output->set_content_type('application/json');
        $username   = $this->input->post('username');
        $password   = $this->input->post('password');
        
        $this->form_validation->set_rules("username","username","required");
        $this->form_validation->set_rules("password","password","required");
        
        if($this->form_validation->run() == TRUE){
            
           
            $res            = $this->UsersApiModel->check_login($username,md5($password));
            
            if(empty($res['error'])){
                
                $date           = "20-10-2018 12:45:23";
                $login_token    = md5(mt_rand(10000,99999)); 
                $this->UsersApiModel->updateLogindate($res['data'][0]['user_id'], $date , $login_token);
                $data   = $res['data'];
                
                foreach ($data as $key => $value){
                    if($data[$key]['users_login_type'] == 0){    
                    $data[$key]['users_photo']          = $this->media_url."assets/images/users/".$data[$key]['users_photo'];
                    $data[$key]['users_login_token']    = $login_token;
                }
                }
                $this->output->set_output(json_encode(['message' =>"login success",'error'=>"0",'data'=>$data,'status' => true]));
            }
            else{
                
                $this->output->set_output(json_encode(['message' => $res['error'],'error'=>$res,'status' => false]));
            }
        }
            else{
            
                $this->output->set_output(json_encode(['message' =>"username and password required",'error'=>"1",'status' => false]));
        }
    }

    
    
    
    //upload users profile photo
    function uploadProfilephoto(){
        
        $this->output->set_content_type('application/json');
       
        $this->load->helper('form');
        $token_id                   = $this->input->post('token');

        $config['upload_path']      = "./assets/images/users/";
        $config['allowed_types']    = 'jpg|gif|png';
        $config['file_name']        = 'user'.mt_rand(100000,999999);
        $config['max_height']       = '250';
        $config['max_width']        = '250';
        $config['max_size']         = '100kb';
        $config["overwrite"]        = false;

        $this->load->library('upload',$config);
        
        if(!$this->upload->do_upload("propic"))
        {
            $error = array('error' => $this->upload->display_errors());
            $this->output->set_output(json_encode(['message' => "file size below 100kb , width = 250 and height = 250 ", 'error'=>1]));
            return false;
        }
        else
        {
            $data   = $this->upload->data();
            $getPhoto=$this->UsersApiModel->getprofilepic($token_id);
            $path="./assets/images/users/".$getPhoto;
            $option=false;
            if(file_exists($path) && $getPhoto != "none.png")
            {
                $option=true;
                unlink($path);
            }

            $result =  $this->UsersApiModel->uploadprofilepic($data['file_name'],$token_id);
                      
            if($result > 0){
                
                    $this->output->set_output(json_encode(['message' =>"succesfully updated",'photo'=>$this->media_url."assets/images/users/".$data['file_name'],'error'=>"0",'status'=>'true']));
       
             }
             else{
                 
                    $this->output->set_output(json_encode(['message' =>"failed to upload",'error'=>"1",'status'=>'false']));
       
             }
 
        }
    }

     //sort users by categories 
     function sortUsers(){

        $this->output->set_content_type('application/json');

        $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;   

        $res         = $this->UsersApiModel->sortUsers($country , $state , $district , $area);

        if($res > 0){

            foreach($res as $key => $value){

                $res[$key]['users_photo'] = $this->media_url.'assets/images/users/'.$res[$key]['users_photo'];
                $res[$key]['posts_count'] = $this->UsersApiModel->getpostCount($res[$key]['user_id']);
            }
            $this->output->set_output(json_encode(array('posts' => $res,'status' => true)));
        }
        else{

            $this->output->set_output(json_encode(array('posts' => "no value found",'status' => false)));
        }
        
    }


    // search users home page
    function searchUsers(){

        $this->output->set_content_type('application/json');
        $search = $this->input->post('search');
        $res    = $this->UsersApiModel->searchUsers($search);

        if($res > 0){
            foreach($res as $key => $value){
                $res[$key]['users_photo'] = $this->media_url.'assets/images/users/'.$res[$key]['users_photo'];
            }
        $this->output->set_output(json_encode(array('res'=>$res , 'status' => true)));
        }
        else{
            $this->output->set_output(json_encode(array('res'=>'no data found' , 'status' => false)));
        }
    }

    // get notifications
    function getNotifications(){

        $this->output->set_content_type('application/json');
        
        $zone = getallheaders();
        $GMT  = new DateTimeZone("GMT");
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";


       
        $userid = $this->input->post('userid');
        if(!empty($userid)){
        $res    = $this->UsersApiModel->getNotifications($userid);
        
        if($res){
        foreach($res as $key => $val){


               $GMT = new DateTimeZone("GMT");
               $date = new DateTime($res[$key]['notifications_date'], $GMT );
               $date->setTimezone(new DateTimeZone($zone['timezone']));
               $res[$key]['notifications_date']= $date->format('Y/m/d H:i:s'); 

        if($res[$key]['notifications_type'] == 3){
   
            $res[$key]['group_name']                = $this->UsersApiModel->getGroupname($res[$key]['notifications_group_id']);
            $res[$key]['users_name']                = $this->UsersApiModel->getUsername($res[$key]['notifications_user_id']);
           
            
        }

        if($res[$key]['notifications_type'] == 4 || $res[$key]['notifications_type'] == 21 || $res[$key]['notifications_type'] == 22 || $res[$key]['notifications_type'] == 23 || $res[$key]['notifications_type'] == 24){

            $get_challenge_type = $this->UsersApiModel->getchallengetype($res[$key]['notifications_post_id']);
            
            if($get_challenge_type == 1)
            $res[$key]['challenge_type'] = 1;
            else
            $res[$key]['challenge_type'] = 2;
        }


        


        if($res[$key]['notifications_type'] == 42 || $res[$key]['notifications_type'] == 43 || $res[$key]['notifications_type'] == 44 || $res[$key]['notifications_type'] == 45 || $res[$key]['notifications_type'] == 50){

            $get_challenge_type = $this->UsersApiModel->getbusinessprofilechallengetype($res[$key]['notifications_post_id']);
            
            if($get_challenge_type == 1)
            $res[$key]['challenge_type'] = 1;
            else
            $res[$key]['challenge_type'] = 2;
        }

        
            $res[$key]['users_photo']              = $this->media_url."assets/images/users/".$res[$key]['users_photo'];
        
            if($res[$key]['notifications_type'] == 50 || $res[$key]['notifications_type'] == 51){

            
                $get_challenge_type = $this->UsersApiModel->getbusinessprofiledetails($res[$key]['notifications_user_id']);
                $res[$key]['users_name']     = $get_challenge_type['name'];
                $res[$key]['users_photo']    = $this->media_url."assets/images/business_profile/".$get_challenge_type['photo'];
            }
        
        }
    }

        if($res > 0){
            
        $this->output->set_output(json_encode(array('res'=>$res , 'status' => true)));
        }
        else{
            $this->output->set_output(json_encode(array('res'=>'no data found' , 'status' => false)));
        }
        }
        else{
        $this->output->set_output(json_encode(array('res'=>'parameter missing' , 'status' => false)));
        }
    }


    // get poll to
    function userPollto(){

        $this->output->set_content_type('application/json');
        $userid = $this->input->post('userid');
        if(!empty($userid)){
        $res    = $this->UsersApiModel->userPollto($userid);

        if($res > 0){
            
            foreach($res as $k => $v){

                $res[$k]['users_photo'] = $this->media_url."assets/images/users/".$res[$k]['users_photo'];
            }
            $this->output->set_output(json_encode(array('res'=>$res , 'status' => true)));
        }
        else{
            $this->output->set_output(json_encode(array('res'=>'no data found' , 'status' => false)));
        }
    
        }
        else{
            $this->output->set_output(json_encode(array('res'=>'parameter missing' , 'status' => false)));
        }
    }

    // get poll by
    function userPollby(){

        $this->output->set_content_type('application/json');
        $userid = $this->input->post('userid');
        if(!empty($userid)){
        $res    = $this->UsersApiModel->userPollby($userid);

        if($res > 0){
            foreach($res as $k => $v){

                $res[$k]['users_photo'] = $this->media_url."assets/images/users/".$res[$k]['users_photo'];
            }
        $this->output->set_output(json_encode(array('res'=>$res , 'status' => true)));
        }
        else{

        $this->output->set_output(json_encode(array('res'=>'no data found' , 'status' => false)));
        }
    
        }
        else{

        $this->output->set_output(json_encode(array('res'=>'parameter missing' , 'status' => false)));
        
    }
    }

     function getUserrank(){

        $this->output->set_content_type('application/json');
        

        $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;
        
        $check       = array('country'=>$country , 'state'=>$state , 'city'=>$district , 'area'=>$area);   

        $res         = $this->UsersApiModel->getUserrank();
        
     
        if($res > 0){
            $k=0;   
            
         foreach($res as $key => $value){
                
                if($this->UsersApiModel->getUserrankbyid($res[$key]['users_followers_users_id'],$check)){

                    $getusers[$k]               = $this->UsersApiModel->getUserrankbyid($res[$key]['users_followers_users_id'],$check);
                    $getusers[$k]->users_photo  = $this->media_url.'assets/images/users/'.$getusers[$k]->users_photo;
                    $getusers[$k]->poll_count   = $this->UsersApiModel->getUserpollcount($res[$key]['users_followers_users_id']);
                    $getusers[$k]->posts_count  = $this->UsersApiModel->getpostCount($res[$key]['users_followers_users_id']);
                    $k++;
                }
               
            }
           
            if($getusers != null){
           
            $this->output->set_output(json_encode(array('res'=>$getusers , 'status' => true)));
            }
            else{
                $this->output->set_output(json_encode(array('res'=>"no values found" , 'status' => false)));
            }
        
        }
        else{

            $this->output->set_output(json_encode(array('res'=>'no data found' , 'status' => false)));
        }
    
        }

        // get user profile
        function getUserProfile(){

            $this->output->set_content_type('application/json');             
           
            $logged_user_id = $this->input->post('loggeduserid');
            $userid         = $this->input->post('userid');  


            if(!empty($userid)){  
            
                
            $check_polled = $this->UsersApiModel->getuserPolled($userid , $logged_user_id);
            
            if($check_polled > 0)$is_polled = 1;
            else{ $is_polled    = 0;  }
            

            $res            = $this->UsersApiModel->getUserProfile($userid);

            $res_posts      = $this->UsersApiModel->getcountUserPosts($userid);
            $res_challenge  = $this->UsersApiModel->getcountUserChallenge($userid);
            $res_poll_by    = $this->UsersApiModel->getuserPollby($userid);  
            $res_poll_to    = $this->UsersApiModel->getuserPollto($userid);  
            
            

            foreach($res as $key => $value){
                   
                  $res[$key]['country_id']        = $this->UsersApiModel->getCountryid($res[$key]['users_country']);                  
                  $res[$key]['state_id']          = $this->UsersApiModel->getStateid($res[$key]['users_state']);                  
                  if( $res[$key]['users_mobile_code'] == null) $res[$key]['users_mobile_code'] = "";                  
                  $res[$key]['users_photo']       = $this->media_url.'assets/images/users/'.$res[$key]['users_photo'];
                  $res[$key]['users_cover_photo'] = $this->media_url.'assets/images/users/cover/'.$res[$key]['users_cover_photo'];
                  $res[$key]['isLocationChange']  = (strtotime($res[$key]['users_location_time'])-strtotime(date('Y-m-d H:i:s'))) > 0 ? 0 : 1;  
                }

               
                  $this->output->set_output(json_encode(array('profile'=>$res , 'postscount'=>$res_posts + $res_challenge ,

                  'pollby'      => $res_poll_by['count'],
                  'pollto'      => $res_poll_to['count'],
                  'isPolled'    => $is_polled, 
                  'status'      => true
                )));
                }
            
            else{
    
                $this->output->set_output(json_encode(array('res'=>'no user id found' , 'status' => false)));
            }
        
            }


    // user forgot password

    function forgotPassword(){

        $this->output->set_content_type('application/json');  
        $mobile     = $this->input->post('mobile');

        $mob_code     = !empty($this->input->post('code')) ? $this->input->post('code') : '91';
        if(!empty($mobile && $mob_code)){

        $check = $this->UsersApiModel->checkMobilenum($mobile , $mob_code);
        
        if($check['count'] > 0){

         


        $authKey = "262246AXEIhGiy1xc5c610ffa";
        $mobileNumbers = $mob_code.$mobile;
        $senderId = "DAYPOLL";
        $otp        = mt_rand(1000,9999);
        $message = urlencode($otp . " is your OTP for reset password on daypoll. Enjoy the new social media experience");
        $route = "4";
        
        $postData = array('authkey' => $authKey,'mobiles' => $mobileNumbers,'message' => $message,'sender' => $senderId,'route' =>$route);
        $url="http://api.msg91.com/api/sendhttp.php";
        $ch = curl_init();
        curl_setopt_array($ch, array(CURLOPT_URL => $url,CURLOPT_RETURNTRANSFER => true,CURLOPT_POST => true,CURLOPT_POSTFIELDS => $postData));
        $output = curl_exec($ch);
        
        if(curl_errno($ch)){
            $this->output->set_output(json_encode(array('message'=>'failed to send','error' => curl_error($ch),'status'=>false)));
        //echo 'error:' . curl_error($ch);
        }
        else{
            $this->output->set_output(json_encode(array('message'=>'otp send','otp'=>$otp,'status'=>true)));
        }
        curl_close($ch);



	        $this->output->set_output(json_encode(array('message'=>'otp send','otp'=>$otp,'userid' => $check['userid'],'status'=>true)));
         
	        //Process your response here
            //echo $response;
            }
            else{
            $this->output->set_output(json_encode(array('res'=>'no user found' , 'status' => false)));
            }
            }
            else{
            $this->output->set_output(json_encode(array('res'=>'mobile number and code required' , 'status' => false)));
            }   
        }

        function resetPassword(){

            $this->output->set_content_type('application/json'); 

            $userid    = $this->input->post('userid');
            $password  = $this->input->post('password');
            $cpassword = $this->input->post('cpassword');
            
            if(!empty($password && $cpassword && $userid)){
               
                if($password == $cpassword){
                   
                    $get_old_password   = $this->UsersApiModel->getOldpassword($userid);
                    
                    if(md5($cpassword) === $get_old_password){

                        $this->output->set_output(json_encode(array('res'=>'same as old password' , 'status' => false)));
                        
                    }
                    else{


                    $res                = $this->UsersApiModel->resetPassword($userid , $cpassword);
                    if($res > 0){

                        $this->output->set_output(json_encode(array('res'=>'successfully changed' , 'status' => true)));
                    }
                    else{
                        $this->output->set_output(json_encode(array('res'=>'failed to reset' , 'status' => false)));
                    }

                }
            }
                else{
                    $this->output->set_output(json_encode(array('res'=>'password and confirm password not match' , 'status' => false)));
                }
            
            }
            else{
                $this->output->set_output(json_encode(array('res'=>'parameter missing' , 'status' => false)));
            }
           
        }

        // get user rank
        function getUserrankbyid(){

            $this->output->set_content_type('application/json'); 
            $rank_array = $this->UsersApiModel->getUserrank();
            print_r($rank_array);
            exit();
            $this->output->set_output(json_encode(array('res'=>'password and confirm password not match' , 'status' => false)));
        }

        //check email exists
        function emailExists(){

            $this->output->set_content_type('application/json'); 
            $email  = $this->input->post('email');
            if(!empty($email)){
                           
            $res = $this->UsersApiModel->emailExists($email);
            if($res > 0){
                $this->output->set_output(json_encode(array('message'=>'email already registered' , 'status' => false ,'error' => 1)));
            }
            else{
            $this->output->set_output(json_encode(array('message'=>'email available' , 'status' => true , 'error' => 0)));
            }
        }
        else{
            $this->output->set_output(json_encode(array('message'=>'parameter missing' , 'status' => false))); 
        }
        }

        //check email exists
        function mobileExists(){

            $this->output->set_content_type('application/json');

            $mobile  = $this->input->post('mobile');
            $code    = $this->input->post('code');
            
           
            if(!empty($mobile)){
            $res = $this->UsersApiModel->mobileExists($mobile , $code);
           
            
            if($res > 0){
                $this->output->set_output(json_encode(array('message'=>'mobile already registered' , 'status' => false , 'error' => 1)));
            }
            else{
            $this->output->set_output(json_encode(array('message'=>'mobile available' , 'status' => true , 'error' => 0)));
            }
        }
        else{
            $this->output->set_output(json_encode(array('message'=>'parameter missing' , 'status' => false)));
        }
        }


        // edit user profile
        function editUserprofile(){

            $this->output->set_content_type('application/json'); 
            $name  = $this->input->post('name');
            $email = $this->input->post();
            $this->output->set_output(json_encode(array('res'=>'password and confirm password not match' , 'status' => false)));
        }


        function gettime(){
            //2018-12-26 06:40:29
            print date('Y-m-d H:m:s');
            
        }

        // update user profile
        function updateUserProfile(){

            $profile_pic = "";
            $cover_pic   = "";
            $this->output->set_content_type('application/json');

            $userid     = $this->input->post('userid');
            $name       = $this->input->post('name');
            $email      = $this->input->post('email');
            $mobile     = $this->input->post('mobile');
            $country    = $this->input->post('country');
            $state      = $this->input->post('state');
            $district   = $this->input->post('district');
            $area       = $this->input->post('area');
            $mob_code   = $this->input->post('code');

            $this->form_validation->set_rules("userid","userid","required");
            $this->form_validation->set_rules("name","name","required");
            $this->form_validation->set_rules("email","email","required");
           


            if($this->form_validation->run()==true){


                $checkuseremail = $this->UsersApiModel->editemailExists($email , $userid);
                if($checkuseremail == 0) {

                    $getPhoto    = $this->UsersApiModel->geteditprofilepic($userid);

                    $getlocation = $this->UsersApiModel->getuserlocation($userid);

                    $update = array(
                        'users_name'       => $name,
                        'users_email'      => $email,
                        'users_mobile'     => $mobile,
                        'users_country'    => $country,
                        'users_state'      => $state,
                        'users_city'       => $district,
                        'users_area'       => $area,
                        'users_isprofile_complete' => 1,
                        'users_mobile_code'=> $mob_code  
                       );
                   
            //   profile pic uploaddd
                
            if(isset($_FILES['user_profile_image'])!=null){

                    $errors= array();
                    $file_name = $_FILES['user_profile_image']['name'];
                    $file_size = $_FILES['user_profile_image']['size'];
                    $file_tmp  = $_FILES['user_profile_image']['tmp_name'];
                    $file_type = $_FILES['user_profile_image']['type'];
                    
                    $tmp      = explode('.', $file_name);
                    $file_ext = end($tmp);

                                    
                    if($getPhoto['profile'] != $file_name){
                      
                    
                    
                    $expensions  = array("jpeg","jpg","png");
                    
                    if(in_array($file_ext,$expensions) === false){
                       $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
                    }
                    
                    
                    
                    $path="./assets/images/users/".$getPhoto['profile'];
                    $option=false;
                    if($getPhoto['profile'] != "none.png")
                    {
                     $option=true;
                     unlink($path);
                    }
                    
                    
                    if(empty($errors)==true){
                        $profile_pic            = 'user'.mt_rand(10000,99999).date('h-i-s').'.'.$file_ext;
                        move_uploaded_file($file_tmp,"assets/images/users/".$profile_pic);
                        $update['users_photo']  = $profile_pic;
                      
                    }
          
                }
            }

            // profile pic ends here        

                //   cover pic uploaddd
           
                if(isset($_FILES['user_cover_image'])!=null){
                    
                    
                    $errors_cover = array();
                    $file_name_cover = $_FILES['user_cover_image']['name'];
                    $file_size_cover = $_FILES['user_cover_image']['size'];
                    $file_tmp_cover  = $_FILES['user_cover_image']['tmp_name'];
                    $file_type_cover = $_FILES['user_cover_image']['type'];
                    
                    $tmp_cover      = explode('.', $file_name_cover);
                    $file_ext_cover = end($tmp_cover);
                
                
                  
                if($getPhoto['cover'] != $file_name_cover){
                  
               
                
                $expensions_cover  = array("jpeg","jpg","png");
                
                if(in_array($file_ext_cover,$expensions_cover) === false){
                   $errors_cover[] = "extension not allowed, please choose a JPEG or PNG file.";
                }
                
                
                
                $path_cover ="./assets/images/users/cover/".$getPhoto['cover'];
                $option_cover = false;
                if($getPhoto['cover'] != "none.png")
                {
                 $option_cover = true;
                 unlink($path_cover);
                }
                
                
                if(empty($errors_cover)==true){
                    $cover_pic                  = 'user_cover'.mt_rand(10000,99999).date('h-i-s').'.'.$file_ext_cover;
                   move_uploaded_file($file_tmp_cover,"assets/images/users/cover/".$cover_pic);
                   $update['users_cover_photo'] = $cover_pic;
                  
                }
      
            } 
        }

        // profile pic ends here   
       

            
            $res =$this->UsersApiModel->updateUserProfile($userid , $update);
            if($res > 0){

                $state_id    = $this->UsersApiModel->getStateid($state);

                //insert new area
                $this->UsersApiModel->check_area_exist($state_id,$area);

                if($getlocation['country'] != $country || $getlocation['state'] != $state || $getlocation['district'] != $district || $getlocation['area'] != $area){
          
            
                    // update party polls and set location change limit to 90 days
                    $this->UsersApiModel->updateuserpartypolls($userid);
                }
                $this->output->set_output(json_encode(array('message'=>'succesfully updated' ,'status' => true)));

            }   
            else{
                $this->output->set_output(json_encode(array('message'=>'no changes to update' , 'status' => true)));

            }     

        }
        else{
          //  $this->output->set_output(array(json_encode('message' => $uiserid)));
            $this->output->set_output(json_encode(array('message'=>'email already exists' , 'status' => false)));

        }
        }
        else{
            $this->output->set_output(json_encode(array('message'=>'parameter missing' , 'status' => false)));
        }
           
        }



        // search all profiles home page
    function searchAllnew(){

        $this->output->set_content_type('application/json');
        $search      = trim($this->input->post('search'));
        $userid      = $this->input->post('userid');
        $type        = $this->input->post('type');    // 0 => user , 1 => party , 2 => challenge , 3 => group , 4 => hashtag

        
        switch($type){

            case 0:
            $res_user         = $this->UsersApiModel->searchuserAll($search);
            break;

            case 1:
            $res_party        = $this->UsersApiModel->searchpartyAll($search);
            break;

            case 2:
            $res_challenge    = $this->UsersApiModel->searchchallengeAll($search);
            break;

            case 3:
            $res_group        = $this->UsersApiModel->searchgroupAll($search);
            break;

            case 4:
            $res_hashtag      = $this->UsersApiModel->getTags($search);
            break;

        }

       
       
        if($res_user > 0){
          
            foreach($res_user as $key => $value){
                $res_user[$key]['users_photo'] = $this->media_url.'assets/images/users/'.$res_user[$key]['users_photo'];
                $res_user[$key]['type']        = 0; 
                
            }
            if($res_user != null){

                $this->output->set_output(json_encode(array('res'=>$res_user , 'status' => true)));
               }
               else{
               
                $this->output->set_output(json_encode(array('res'=>'no user found' , 'status' => false)));
               
            }
        }

        if($res_party > 0){
            
            foreach($res_party as $key => $value){
                // $pollcount                              = $this->PostsApiModel->pollsCount($res_party[$key]['party_id']);
                // $res_party[$key]['pollcount']           = $pollcount['count'];
                $res_party[$key]['party_photo']         = $this->media_url.'assets/images/party/'.$res_party[$key]['party_photo'];
                $ispolled                               = $this->UsersApiModel->getpolledparty($userid); 
                $isjoined                               = $this->UsersApiModel->getjoinedparty($userid); 
                
                $res_party[$key]['poll_count_public']   = $this->UsersApiModel->getPartypubliccountbyid($res_party[$key]['party_id']);
                $res_party[$key]['poll_count_secret']   = $this->UsersApiModel->getPartysecretcountbyid($res_party[$key]['party_id']);
                
                $res_party[$key]['poll_count']          = $res_party[$key]['poll_count_public'] + $res_party[$key]['poll_count_secret'];
                if($ispolled == $res_party[$key]['party_id']) $is_polled = 1;
                else $is_polled = 0;

                if($isjoined == $res_party[$key]['party_id']) $is_joined = 1;
                else $is_joined = 0;
                
                $res_party[$key]['partyjoinid']      = $isjoined;
                $res_party[$key]['partyjoin']        = $this->UsersApiModel->getjoinpartyname($userid);
                $res_party[$key]['partypoll']        = $this->UsersApiModel->getpollpartyname($userid);
                
                $res_party[$key]['ispolled']    = $is_polled; 
                $res_party[$key]['isjoined']    = $is_joined;
                $res_party[$key]['type']        = 1; 
            }

          

           if($res_party != null){

            $this->output->set_output(json_encode(array('res'=>$res_party , 'status' => true)));
           }
           else{
            $this->output->set_output(json_encode(array('res'=>"no data found" , 'status' => false)));
           }
        }


        if($res_challenge > 0){
            
            foreach($res_challenge as $key => $value){
                
            
                $res_challenge[$key]['users_photo']            = $this->media_url.'assets/images/users/'.$res_challenge[$key]['users_photo'];
               
                $res_challenge[$key]['pollcount']              = $this->UsersApiModel->challengePollcount( $res_challenge[$key]['challenge_id']);
                
                $res_challenge[$key]['ispolled']                = $this->UsersApiModel->isChallengePolled($userid,$res_challenge[$key]['challenge_id']);
                $res_challenge[$key]['type']                    = 2; 
            }

           if($res_challenge != null){

            $this->output->set_output(json_encode(array('res'=>$res_challenge , 'status' => true)));
           }
           else{
            $this->output->set_output(json_encode(array('res'=>"no data found" , 'status' => false)));
           }
        }


        if($res_group > 0){
            
            foreach($res_group as $key => $value){
                // $pollcount                              = $this->PostsApiModel->pollsCount($res_party[$key]['party_id']);
                // $res_party[$key]['pollcount']           = $pollcount['count'];
                $res_group[$key]['group_profile_photo']         = $this->media_url.'assets/images/group/profile/'.$res_group[$key]['group_profile_photo'];
                $ispolled                                       = $this->UsersApiModel->getjoinedgroup($userid); 
                
                
                $res_group[$key]['members_count']               = $this->UsersApiModel->getGroupMembersCount($res_group[$key]['group_id']);
               if($ispolled >0) $is_polled                      = 1;
                else $is_polled                                 = 0;
                $res_group[$key]['isPolled']                    = $is_polled; 
                $res_group[$key]['type']                        = 3; 
            }

          

           if($res_group != null){

            
            $this->output->set_output(json_encode(array('res'=>$res_group , 'status' => true)));
           }
           else{
            $this->output->set_output(json_encode(array('res'=>"no data found" , 'status' => false)));
           }
        }

        if($res_hashtag > 0){
        if($res_hashtag != null){
          
            $this->output->set_output(json_encode(array('res'=>$res_hashtag , 'status' => true)));
        }
        else{

            $this->output->set_output(json_encode(array('res'=>'no values found' , 'status' => false)));
        }
    }
      
        
    }





        // search all profiles home page
    function searchAll(){

        $this->output->set_content_type('application/json');
        $search      = $this->input->post('search');
        $userid      = $this->input->post('userid');
        $type        = $this->input->post('type');    // 0 => user , 1 => party , 2 => challenge , 3 => group



        $res_user    = $this->UsersApiModel->searchuserAll($search);
        
        $res_party   = $this->UsersApiModel->searchpartyAll($search);
        
        
        if($res_user > 0){
            foreach($res_user as $key => $value){
                $res_user[$key]['users_photo'] = $this->media_url.'assets/images/users/'.$res_user[$key]['users_photo'];
                $res_user[$key]['type']        = 0; 
                
            }
        $this->output->set_output(json_encode(array('res'=>$res_user , 'status' => true)));
        }

        if($res_party > 0){
            
            foreach($res_party as $key => $value){
                // $pollcount                              = $this->PostsApiModel->pollsCount($res_party[$key]['party_id']);
                // $res_party[$key]['pollcount']           = $pollcount['count'];
                $res_party[$key]['party_photo']         = $this->media_url.'assets/images/party/'.$res_party[$key]['party_photo'];
                $ispolled                               = $this->UsersApiModel->getpolledparty($userid); 
                $res_party[$key]['poll_count_public']   = $this->UsersApiModel->getPartypubliccountbyid($res_party[$key]['party_id']);
                $res_party[$key]['poll_count_secret']   = $this->UsersApiModel->getPartysecretcountbyid($res_party[$key]['party_id']);
                
                $res_party[$key]['pollcount']           = $res_party[$key]['poll_count_public'] + $res_party[$key]['poll_count_secret'];
                if($ispolled == $res_party[$key]['party_id']) $is_polled = 1;
                else $is_polled = 0;
                $res_party[$key]['isPolled']   = $is_polled; 
                $res_party[$key]['type']        = 1; 
            }

            foreach($res_party as $row){
                $res_user[] = $row;
            }

           if($res_user != null){

            
            $this->output->set_output(json_encode(array('res'=>$res_user , 'status' => true)));
           }
           else{
            $this->output->set_output(json_encode(array('res'=>"no data found" , 'status' => false)));
           }
        }
        else{
            $this->output->set_output(json_encode(array('res'=>'no data found' , 'status' => false)));
        }
    }


    // get user profile rank
    function userProfilerank(){

        $this->output->set_content_type('application/json');

        $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;
        $check       = array('country'=>$country , 'state'=>$state , 'city'=>$district , 'area'=>$area);
        $userid      = $this->input->post('userid');
        
        $res         = $this->UsersApiModel->allrankList();

      

        // global rank
        $k = 0;
        foreach($res as $key => $val){
            if($this->UsersApiModel->getglobalUserrankbyid($res[$key]['users_followers_users_id'])){

                    $getusers[$k]               = $this->UsersApiModel->getglobalUserrankbyid($res[$key]['users_followers_users_id']);
                  
                    $k++;
                }
        }
        if($getusers != null){
        $i_global = 0;
        $rank_global = 1;
        foreach($getusers as $vals => $r){

            if($getusers[$i_global]->user_id == $userid){
                $rank_count_global = $rank_global;
               
            }
           
            $rank_global++;
            $i_global++;
        }
    }
    else{
        $rank_count_global = 0; 
    }
       if( $rank_count_global == null)$rank_count_global = 0; 

        // // country rank
        $k_country = 0;
        $rank_count_country = "";
        $rank_count_state = "";
        $rank_count_district = "";
        $rank_count_area = "";

        foreach($res as $key => $val){
            if($this->UsersApiModel->userrankbycountry($res[$key]['users_followers_users_id'],$country)){

                    $getusers_country[$k_country]               = $this->UsersApiModel->userrankbycountry($res[$key]['users_followers_users_id'],$country);
                  
                    $k_country++;
                }

        }
      
       
        if($getusers_country != null){
            
        $i_country = 0;
        $rank_country = 1;
        foreach($getusers_country as $vals => $r){

            if($getusers_country[$i_country]->user_id == $userid){
                $rank_count_country = $rank_country;
               
            }
           
            $rank_country++;
            $i_country++;
        }
    }
    else{
       $rank_count_country = 0; 
    }

    if($rank_count_country == null)$rank_count_country = 0;


        // state rank
        $k_state = 0;
        foreach($res as $key => $val){
            if($this->UsersApiModel->userrankbystate($res[$key]['users_followers_users_id'],$state)){

                    $getusers_state[$k_state]               = $this->UsersApiModel->userrankbystate($res[$key]['users_followers_users_id'],$state);
                  
                    $k_state++;
                }
        }
        
        if($getusers_state != null){
        $i_state = 0;
        $rank_state = 1;
        foreach($getusers_state as $vals => $r){

            if($getusers_state[$i_state]->user_id == $userid){
                
                $rank_count_state = $rank_state;
               
            }
           
            $rank_state++;
            $i_state++;
        }
    }
    else{
        $rank_count_state = 0;
    }
if($rank_count_state == null)$rank_count_state = 0;


        //district rank

        $k_district = 0;
        foreach($res as $key => $val){
            if($this->UsersApiModel->userrankbydistrict($res[$key]['users_followers_users_id'],$district)){

                    $getusers_district[$k_district]               = $this->UsersApiModel->userrankbydistrict($res[$key]['users_followers_users_id'],$district);
                  
                    $k_district++;
                }
        }

        

        if($getusers_district != null){
        
        $i_district = 0;
        $rank_district = 1;
        foreach($getusers_district as $vals => $r){

            if($getusers_district[$i_district]->user_id == $userid){
                $rank_count_district = $rank_district;
               
            }
           //if($rank_rank_count_district == null)$rank_count_district = 0;
          

           
            $rank_district++;
            $i_district++;
        }

    }
    else{
        $rank_count_district = 0;
    }
      
    if($rank_count_district == null)$rank_count_district = 0;


        //area rank
        $getusers = array();    
        $k = 0;
        foreach($res as $key => $val){
            if($this->UsersApiModel->userrankbyarea($res[$key]['users_followers_users_id'],$area)){

                    $getusers[$k]               = $this->UsersApiModel->userrankbyarea($res[$key]['users_followers_users_id'],$area);
                  
                    $k++;
                }
                
        }
        if($getusers != null){
           
        
      
        $i_area = 0;
        $rank_area = 1;
        foreach($getusers as $vals => $r){

            if($getusers[$i_area]->user_id == $userid){
                $rank_count_area = $rank_area;
               
            }
           
            $rank_area++;
            $i_area++;
        }
    }
    else{
        $rank_count_area = 0;
    }

    if($rank_count_area == null)$rank_count_area = 0;
        
        $this->output->set_output(json_encode(array('global_rank' => $rank_count_global,
                                                    'country_rank' => $rank_count_country  ,
                                                    'state_rank' => $rank_count_state ,
                                                    'district_rank' => $rank_count_district,
                                                    'area_rank'=> $rank_count_area
                                                    )));
    }




     // edit user cover
    
     function editUsercoverPhoto(){                                                             

        $this->output->set_content_type('application/json');

        
        $userid         = $this->input->post('userid');
       

        $this->form_validation->set_rules("userid","userid","required");
       

        if($this->form_validation->run() == TRUE){



            $this->load->helper('form');

            $config['upload_path']      = "./assets/images/users/cover/";
            $config['allowed_types']    = '*';
            $config['file_name']        = 'user_cover'.mt_rand(100000,999999).date('H-i-s');
            //$config['max_height']     = '250';
            //$config['max_width']      = '250';
            //$config['max_size']       = '100kb';
            $config["overwrite"]        = false;

            $this->load->library('upload',$config);
            if(!$this->upload->do_upload("user_cover"))
            {
                $error = array('error' => $this->upload->display_errors());

                $this->output->set_output(json_encode(['message' => 'file not specified', 'status'=>'false']));
                return false;
            }
            else
            {
                
                $data   = $this->upload->data();

                $result = $this->UsersApiModel->editUsercoverPhoto($userid , $data['file_name']);


                if($result > 0) {

                    $this->output->set_output(json_encode(['message' => "successfully uploaded ", 'status' => "true"]));
                }
                else{
                    $this->output->set_output(json_encode(['message' => "operation failed!..", 'status' => "false"]));
                }
            }
       
            }
            else{
                $this->output->set_output(json_encode(['message' => "userid required ", 'status' => "false"]));
        
            }
        }

    // change password
    function changePassword(){

        $this->output->set_content_type('application/json');

        $userid         = $this->input->post('userid');
        $oldpassword    = $this->input->post('oldpassword');
        $password       = $this->input->post('password');
        $cpassword      = $this->input->post('confirmpassword');

        $this->form_validation->set_rules("password","password","required");
        $this->form_validation->set_rules("confirmpassword","confirmpassword","required");
        $this->form_validation->set_rules("userid","userid","required");
        $this->form_validation->set_rules("oldpassword","oldpassword","required");
       

        if($this->form_validation->run() == TRUE){

            if($cpassword == $password){
                $where = array('users_password' => md5($oldpassword) ,'user_id' => $userid);
                $res = $this->UsersApiModel->changePassword($where , md5($password));

                switch($res){
                    case 1:
                    $this->output->set_output(json_encode(['message' => 'Succesfully updated', 'status' => "true"]));
                    break;

                    case 0:
                    $this->output->set_output(json_encode(['message' => 'Failed to update', 'status' => "true"]));
                    break;

                    case -1:
                    $this->output->set_output(json_encode(['message' => 'Incorrect password', 'status' => "true"]));
                    break;
                }
                
            }
            else{

                $this->output->set_output(json_encode(['message' => 'password not matching', 'status' => "false"]));
        
            }
            
        }
        else{
           
            $this->output->set_output(json_encode(['message' => 'parameter missing', 'status' => "false"]));
        
        }
    } 


    function userFeedback(){
        $this->output->set_content_type('application/json');

        $desc         = $this->input->post('description');
        $email        = $this->input->post('email');
        $name         = $this->input->post('name');



        if(isset($_FILES['doc'])){   
                            
            $errors_thumb    = array();
            $file_name_thumb = $_FILES['doc']['name'];
            $file_size_thumb = $_FILES['doc']['size'];
            $file_tmp_thumb  = $_FILES['doc']['tmp_name'];
            $file_type_thumb = $_FILES['doc']['type'];
            $tmp_thumb       = explode('.', $file_name_thumb);
            $file_ext_thumb  = end($tmp_thumb);

    
            $thumb_name = "feedback-".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext_thumb;
           
            move_uploaded_file($file_tmp_thumb,"assets/feedback/".$thumb_name);
            
        }
                
            
                
                $insert = array('feedback_user_email' => $email ,'feedback_user_name' => $name,'feedback_user_desc' =>$desc ,'feedback_doc' => $thumb_name);
                $res = $this->UsersApiModel->saveFeedback($insert);

                if($res > 0){
                   
                    $this->output->set_output(json_encode(['message' => 'successfully saved', 'status'=>true]));
               
                }
                else{

                    $this->output->set_output(json_encode(['message' => 'failed to upload', 'status'=>false]));
               
                }
            }

       
    


    public function add()
   {
       $this->functions->check_exp_time_and_is_login(1800,true);
       if($_FILES['file']['name'] != '') {
           if($_FILES['file']['size'] > 2000000) {
                   $this->session->set_flashdata('msg','You can not upload this file, Max Size : 2 MB');
                   $this->session->set_flashdata('msg_type','danger');
                   redirect('admin/home_slider');
               }
               $config['upload_path']   =  './upload/admin/slider/';
               $config['allowed_types'] =  'jpeg|jpg';
               $config['max_size']      =   2000000;
               $file_name  =   $_FILES['file']['name'];
               $file_name  =   preg_replace("/[^a-zA-Z0-9.]/", "", $file_name);
               $file_name  =   rand().'_slider_'.$file_name;
               $config['file_name'] = $file_name;
               $this->load->library('upload', $config);
               if($this->upload->do_upload('file')) {
                   $image  = $file_name;

           }else{
               $image = '';
               $this->session->set_flashdata('msg',$this->upload->display_errors());
               $this->session->set_flashdata('msg_type','danger');
               redirect('admin/home_slider');
           }
       }
       $values = array(
           'creater_id'  => $this->session->userdata('sess_user_id'),
           'image'       => $image,
           'ip'          => $this->input->ip_address(),
       );
       if($this->model->insert_table('pmp_testimony', $values)) {
           $this->session->set_flashdata('msg', 'Slider added');
           $this->session->set_flashdata('msg_type', 'success');
       } else {
           $this->session->set_flashdata('msg', 'Failed! Please try again');
           $this->session->set_flashdata('msg_type', 'danger');
       }

       redirect('admin/home_slider');    
   }



    function getSwitch(){

        $case = "daypoll_video";

        switch($case){

            case 'daypoll_post':
            echo "Post identified";
            break;
            
            case 'daypoll_challenge':
            echo  "Challenge identified";
            break;

            case 'daypoll_video':
            echo "Video identified";
            break;
            
            default:
            echo "Daypoll all contents";

        }


    }

    //delete user account
    function deleteUseraccount(){

        $this->output->set_content_type('application/json');
        $userid = $this->input->post('userid');
        if(!empty($userid)){

            $res = $this->UsersApiModel->deleteUseraccount($userid);
            if($res > 0){
                $removegroup            = $this->UsersApiModel->removeGroup($userid);
                
                $removeposts            = $this->UsersApiModel->removePosts($userid);
                $removepartyposts       = $this->UsersApiModel->removepartyPosts($userid);
                $removegroupposts       = $this->UsersApiModel->removegroupPosts($userid);
                $removechallengeposts   = $this->UsersApiModel->removechallengePosts($userid);

                $removepostspolls               = $this->UsersApiModel->removePostspolls($userid);
                $removepartypostspolls          = $this->UsersApiModel->removepartyPostspolls($userid);
                $removegrouppostspolls          = $this->UsersApiModel->removegroupPostspolls($userid);
                $removechallengepolls           = $this->UsersApiModel->removechallengepolls($userid);
                $removechallengepostspolls      = $this->UsersApiModel->removechallengePostspolls($userid);

                $removepostscomments            = $this->UsersApiModel->removePostscomments($userid);
                $removepartypostscomments       = $this->UsersApiModel->removepartyPostscomments($userid);
                $removegrouppostscomments       = $this->UsersApiModel->removegroupPostscomments($userid);
                $removechallengecomments        = $this->UsersApiModel->removechallengecomments($userid);
                //$removechallengepostscomments   = $this->UsersApiModel->removechallengePostscomments();

                $removeuserpolls               = $this->UsersApiModel->removeuserPolls($userid);
                $removegrouppolls              = $this->UsersApiModel->removegroupPolls($userid);
                $removegroupuserpolls          = $this->UsersApiModel->removegroupuserPolls($userid);
                $removepartyuserpolls          = $this->UsersApiModel->removepartyuserPolls($userid);
                $removegroupuserfollowerPolls  = $this->UsersApiModel->removegroupuserfollowerPolls($userid);
                $removepartyuserfollowerPolls  = $this->UsersApiModel->removepartyuserfollowerPolls($userid);
               

                $removepostsreport             = $this->UsersApiModel->removePostsreport($userid);
                $removepartypostsreport        = $this->UsersApiModel->removepartyPostsreport($userid);
                $removegrouppostsreport        = $this->UsersApiModel->removegroupPostsreport($userid);
                //$removechallengepostsreport    = $this->UsersApiModel->removechallengePostsreport();


                $removepostsreply             = $this->UsersApiModel->removePostsreply($userid);
                $removepartypostsreply        = $this->UsersApiModel->removepartyPostsreply($userid);
                $removegrouppostsreply        = $this->UsersApiModel->removegroupPostsreply($userid);


                //remove business profile account

                $removeaccount = $this->UsersApiModel->removeBusinessProfile($userid);


                $this->output->set_output(json_encode(['message' => 'successfully deleted', 'status' => true]));
            
            }
            else{
                $this->output->set_output(json_encode(['message' => 'failed to delete', 'status' => false]));
            
            }
          
        }
        else{

            $this->output->set_output(json_encode(['message' => 'parameter missing', 'status' => false]));
     
        }
       

    }

 
    function getAlluserdata(){

        $res = $this->UsersApiModel->getusersData($id);
        if($res >= 0){
            $this->output->set_output(array('response' => $getAlluserdata));
        }
        else{
            $this->output->set_content_type('application/json');
            $this->output->set_output($res);
        }
    }

    


    function s3upload(){


        if(isset($_FILES['image'])){
        $config['upload_path']      = "./assets/images/posts/";
        $config['allowed_types']    = '*';
        $config['file_name']        = 'posts'.mt_rand(100000,999999).date('H-i-s');
        //$config['max_height']     = '250';
        //$config['max_width']      = '250';
        //$config['max_size']       = '100kb';
        //$config["overwrite"]        = false;

        $this->load->library('upload',$config);
        if(!$this->upload->do_upload("image"))
        {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
            exit();
            //$this->output->set_output(json_encode(['message' => 'file not specified', 'status'=>'false']));
            //return false;
        }
        else
        {
            // print_r($_FILES['image']);
            // exit();
            $data   = $this->upload->data();
            $this->aws3->sendFile('daypoll',$_FILES['image']);
            print_r($_FILES['image']);
            exit();
        }
    }
    else{
        print 0 ;
        exit(); 
    }


}




    //sort by categories Post
    function sortUserbyCategory(){

        $zone = getallheaders();
        $GMT  = new DateTimeZone("GMT");
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";

        $userid      = !empty($this->input->post('userid')) ? $this->input->post('userid') : 1;
        $page_start  = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;

        $this->output->set_content_type('application/json');

        $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;

        $res_user         = $this->UsersApiModel->sortUsers($country , $state , $district , $area);
     

        if($res_user != 0){

            $status_user = true; 
       
            foreach($res_user as $keys => $values){

                $res_user[$keys]['users_photo'] = $this->media_url.'assets/images/users/'.$res_user[$keys]['users_photo'];
                $res_user[$keys]['posts_count'] = $this->UsersApiModel->getpostCount($res_user[$keys]['user_id']);
            }
      
        $date = new DateTime(date("Y-m-d H:i:s", strtotime("now")), $GMT );
        $date->setTimezone(new DateTimeZone($zone['timezone']));
        $datetime = $date->format('Y-m-d H:i:s'); 

        $this->output->set_output(json_encode(array('users' => $res_user,'status' => true)));
        }
        else{
        $this->output->set_output(json_encode(array('users' => "no data found" ,'status' => false))); 
        }
    }



    //make notifications read
    function makeread(){

        $this->output->set_content_type('application/json');
        $id               = $this->input->post('id'); 
        if(!empty($id)){       
        $res_user         = $this->UsersApiModel->makeread($id);
        $this->output->set_output(json_encode(array('message' => "success" ,'status' => true))); 
        }else{
            $this->output->set_output(json_encode(array('message' => "parameter missing" ,'status' => false))); 
        }

    }


    function testHeader(){

        $zone = getallheaders();
        print $zone['token'];
        
    }




}