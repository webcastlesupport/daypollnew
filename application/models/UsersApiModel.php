<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 8/9/2018
 * Time: 2:34 PM
 */

class UsersApiModel extends CI_Model{

    // check username exits
    function checkUsername($username = null){

        $this->db->select('users_username');
        $this->db->where('users_username',$username);
        $res = $this->db->get('users');
        return ($res->num_rows() > 0) ? $res->row('users_username') : 1;
    }
    
     // get counry code api
    function getCountrycode(){

        
        $res = $this->db->get('countries_code');
        return $res->num_rows() > 0 ? $res->result_array() : 0;

    } 
    
     // save location after facebook register
    function saveLocation($token = null , $update = null){

        $this->db->where('users_token',$token);
        $this->db->update('users',$update);
        return $this->db->affected_rows();
    } 


    // update party poll if location changed

    function updatepartypolls($token = null){

        $this->db->where('users_token',$token);
        $update = array(    'users_party_id' => 1 , 'users_poll_party_id' => 1 ,
                            'users_party_poll_type' => 1 , 'users_party_state_id' => 1 , 
                            'users_poll_party_state_id' => 1 ,  'users_party_state_poll_type' => 1 , 
                            'users_party_district_id' => 1 , 'users_party_district_poll_id' => 1 ,  
                            'users_party_district_poll_type' => 1);
        $this->db->update('users',$update);                    
    }

      // update party poll if location changed

      function updateuserpartypolls($userid = null){

        $this->db->where('user_id',$userid);
        $update = array(    'users_party_id' => 1 , 'users_poll_party_id' => 1 ,
                            'users_party_poll_type' => 1 , 'users_party_state_id' => 1 , 
                            'users_poll_party_state_id' => 1 ,  'users_party_state_poll_type' => 1 , 
                            'users_party_district_id' => 1 , 'users_party_district_poll_id' => 1 ,  
                            'users_party_district_poll_type' => 1 , 'users_location_time' => date('Y-m-d H:m:s'));
        $this->db->update('users',$update);                    
    }
    

    // get user data by token
    function get_user_by_token($token = null){
        $res = $this->db->get_where('users',array('users_token' => $token));
        return $res->result_array();
    }

    
    // register user api
    function registerusers($insert = null){
        if($this->checkuserisExists2($insert['users_email'] ,$insert['users_username'],$insert['users_mobile'])==0) {
                return 0;
        }
        else{
            return -1;
        }
    }

     // register user api
     function registeruser($insert = null){ 
        
        if($this->checkuserisExists2( $insert['users_mobile'])==0) {
            
            $this->db->insert('users', $insert);
            if ($this->db->affected_rows() > 0) {
                
                $id = $this->db->insert_id();
                $res = $this->db->get_where('users',array('user_id' => $id));
                return $res->result_array();

            }
        }
        else{
            return -1;
        }
    }

    function check_area_exist($state_id = null,$area = null){

        if(!empty($area)){
        $this->db->where(array('name' => $area , 'district_id' => $state_id));
        if($this->db->get('area')->num_rows() == 0)
        $this->db->insert('area',array('name' => $area , 'district_id' => $state_id));
        }
    }


    function update_read($id = null){
        $this->db->where('notifications_user_poll_id',$id);
        $this->db->update('notifications',array('notifications_isread'=>0));
    }

    function check_district_exist($sid = null , $area = null){

        if(!empty($area)){
        $this->db->where(array('name' => $area , 'state_id' => $sid));
        if($this->db->get('cities')->num_rows() == 0)
        $this->db->insert('cities',array('name' => $area , 'state_id' => $sid));
        }
    }

    
    
    // check mobile number
    function checkMobilenum($mobile = null , $code = null){
        
        $this->db->where('users_mobile',$mobile);
        if(!empty($code)){
        $this->db->where('users_mobile_code',$code);
        }
        $res = $this->db->get('users');
        $return['count']  =  $res->num_rows();
        $return['userid'] = $res->row('user_id');
        return $return; 
    }

   

     // make verified
     function isVerified($mobile = null , $code = null , $flag = null , $userid = null){

        $this->db->where('user_id',$userid);
       
        $this->db->update('users',array('users_mobile' => $mobile ,'users_mobile_code' => $code,'users_isactivated' => $flag));
       
        return $this->db->affected_rows(); 
      
    }

    //login with facebook or google
    function registerusersothers($insert = null){

        if($this->checkuserisExists($insert['users_email']) == 0) {

            $this->db->insert('users', $insert);
            $return['count'] = $this->db->insert_id();
        }
        else{
            $this->db->select(array('user_id','users_name',
            'users_email','users_mobile','users_dob',
            'users_country','users_state','users_city','users_area',
            'users_created_date','users_login_date','users_isprofile_complete',
            'users_photo','users_active',
            'users_isactivated','users_isapproved','users_token','users_login_token'));
            $res = $this->db->get_where('users',array('users_email' => $insert['users_email']));
            $return['count'] =  -1;
            $return['data']  =  $res->result_array();
        }
        return $return;
    }
    
    
    //register user location
    function registerLocation($token = null,$insert = null){
        
        $this->db->where(['users_token'=>$token]);
        $this->db->update('users',$insert);
        return $this->db->affected_rows();
    }
    
    //save password
    function savePassword($token = null , $insert = null){
        
        $this->db->where(['users_token' => $token]);
        $this->db->update('users',$insert);
        return $this->db->affected_rows();
    }
    
    // get old password

    function getOldpassword($userid = null){

        $this->db->select('users_password');
        $res = $this->db->get_where('users',array('user_id' => $userid));
        return $res->row('users_password');
    }
    
    //getCountry
    function getCountry(){

        $res = $this->db->get('countries');
        return ($res->num_rows() > 0) ?  $res->result_array() : 0;
    }

    //getState by country id
    function getState($cid = null){
        $this->db->where('country_id',$cid);
        $res = $this->db->get('states');
        return ($res->num_rows() > 0) ?  $res->result_array() : 0;
    }

    //getcity by state id
    function getCity($sid = null){
        $this->db->where('state_id',$sid);
        $res = $this->db->get('cities');
        return ($res->num_rows() > 0) ?  $res->result_array() : 0;
    }

    //getarea by state id
    function getArea($sid = null){
        $this->db->where('district_id',$sid);
        $res = $this->db->get('area');
        return ($res->num_rows() > 0) ?  $res->result_array() : 0;
    }

    
    
    function getToken($check = null){
        
        $this->db->select(array('users_token','users_mobile'));
        $this->db->where($check);
        $res = $this->db->get('users');
        return $res->row('users_token');
    }

    // check user is exist or not
    function checkuserisExists($check = null){

        $this->db->where(array('users_email'=>$check));
        $res = $this->db->get('users');
        return $res->num_rows();
    }
    
    //  // check user is exist or not
    //  function checkuserisExists2($email = null ,$mobile = null , $username = null){

    //     $this->db->where(array('users_email'=>$email));
    //     $this->db->or_where(array('users_mobile'=>$mobile));
    //     $this->db->or_where(array('users_username'=>$username));
    //     $res = $this->db->get('users');
    //     return $res->num_rows();
    // }
    
    
     // check user is exist or not
     function checkuserisExists2($mobile = null){
            
        $this->db->where(array('users_mobile'=>$mobile));
        $res = $this->db->get('users');
        return $res->num_rows();
   
    }
    
    // check user login username and password
    // function check_login($username = null , $pass =null){    
        
    //     $query = "SELECT `user_id`,`users_name`,`users_email`,`users_username`,`users_mobile_code`,`users_mobile`,`users_dob`,`users_country`,`users_state`,`users_city`,`users_area`,`users_ward`,`users_gender`,`users_created_date`,`users_photo`,`users_cover_photo`,`users_isprofile_complete`,`users_party_id`,`users_poll_party_id`,`users_token`,`users_login_type` FROM users WHERE (`users_username` = '".$username."' OR `users_mobile` = '".$username."') AND `users_password` = '".$pass."' ";
    //       $res = $this->db->query($query);
    //     if($res->num_rows()>0){
    //     $return['data']  = $res->result_array();
    //     }
    //     else{
    //     $return['error'] = "no values found";
    //     }
    //     return $return;
    // }
    
    // check user login username and password
    function check_login($username = null , $pass =null){    
        if($this->checkBlocked($username , $pass) == 0){
        $query = "SELECT `user_id`,`users_name`,`users_email`,`users_username`,`users_mobile_code`,`users_isactivated`,`users_mobile`,`users_dob`,`users_country`,`users_state`,`users_city`,`users_area`,`users_ward`,`users_gender`,`users_isprofile_complete`,`users_created_date`,`users_photo`,`users_cover_photo`,`users_party_id`,`users_poll_party_id`,`users_token`,`users_login_token`,`users_login_type` FROM users WHERE (`users_username` = '".$username."' OR `users_mobile` = '".$username."') AND `users_password` = '".$pass."' ";
        $res = $this->db->query($query);
        if($res->num_rows()>0){
        $return['data']  = $res->result_array();
        }
        else{
        $return['error'] = "login failed";
        }
    }
    else{
        $return['error'] = "You are blocked by admin";
    }
        return $return;
    }

    // check is blocked
    function checkBlocked($username , $password){

        $query = "SELECT `user_id` FROM users WHERE (`users_username` = '".$username."' OR `users_mobile` = '".$username."') AND `users_password` = '".$password."' AND `users_active` = 0 ";
        $res   = $this->db->query($query);
        return $res->num_rows();
    }
    
 
    
    //upload user profile pic 
    function uploadprofilepic($propicname = null,$token = null){
        
        $this->db->where(array('users_token'=>$token));
        $update = array('users_photo'=>$propicname);
        $this->db->update('users',$update);
        return $this->db->affected_rows();
       
    }
    
    // update login date and login token when login
    function updateLogindate($id=null,$date=null,$token = null){
        
        $this->db->where(array('user_id'=>$id));
        $update = array('users_login_date'=>date('Y-m-d H:i:s') , 'users_login_token' => $token);
        $this->db->update('users',$update);
        return true;
    }

    
    // get profile pic for remove from folder
    function getprofilepic($token = null){
        
        $this->db->where(array('users_token'=>$token));
        $res = $this->db->get('users');
        return $res->row('users_photo');
    }

    // sort users by category
    function sortUsers($country = null, $state = null, $district = null, $area = null , $limit = null){

        if(!empty($country)) $this->db->where('users_country',$country);
        if(!empty($state)) $this->db->where('users_state',$state);
        if(!empty($district)) $this->db->where('users_city',$district);
        if(!empty($area)) $this->db->where('users_area',$area);

        $this->db->select(array('users.user_id','users_photo','users.users_name','users_followers.users_followers_users_id','users.users_country','users.users_state','users.users_city','users.users_area','COUNT(users_followers.users_followers_users_id) as poll_count'));
        $this->db->from('users_followers');
        $this->db->join('users','users.user_id = users_followers.users_followers_users_id AND users_followers.users_followers_active = 1');
        $this->db->order_by('poll_count','desc');
        $this->db->group_by('users_followers_users_id');
        $this->db->limit($limit);
        $res = $this->db->get();
        return ($res->num_rows() > 0) ? $res->result_array() : 0;
    }

    //get post Count
    function getpostCount($userid = null){

        $this->db->where('posts_user_id',$userid);
        $this->db->where('posts_active',1);
        $res = $this->db->get('posts');
        return $res->num_rows();
        
    }

     //get post Count
     function getcountUserChallenge($userid = null){

        $this->db->where('challenge_user_id',$userid);
        $this->db->where('challenge_active',1);
        $res = $this->db->get('challenge');
        return $res->num_rows();
        
    }


    //search users index
    function searchUsers($search = null){

        $this->db->select(array('users_name','user_id','users_photo'));
        $this->db->where('user_id !=',1);
        $this->db->like(array('users_name' => $search));
        $res = $this->db->get('users');
        return ($res->num_rows() > 0) ? $res->result_array() : 0;

    }

    //get notifications
    function getNotifications($userid=null){

        $this->db->select(array('notifications_id','users.users_name','users.user_id','users.users_photo','notifications.notifications_challenge_id','notifications.notifications_type','notifications.notifications_date','notifications.notifications_post_id','notifications.notifications_group_link','notifications.notifications_group_id','notifications.notifications_user_poll_id','notifications_user_id','notifications_isview','notifications_isread','notification_text'));
        
        $this->db->where('notifications_user_poll_id',$userid);
        //$this->db->where('notifications_user_id !=',$userid);  commented for expire notification, quick fix
         
        $this->db->from('notifications');
        $this->db->order_by('notifications_id','desc');
        $this->db->join('users','users.user_id=notifications.notifications_user_id');
        $res = $this->db->get();
        return ($res->num_rows()>0) ? $res->result_array() : 0;

    }

    //function get invited user name
    function getUsername($userid = null){

        $this->db->select('users_name');
        $this->db->where('user_id',$userid);
        return $this->db->get('users')->row('users_name');
    }

    //get user rank
    function getUserrank(){
        $this->db->select('users_followers_users_id, COUNT(users_followers_users_id) as total');
        $this->db->group_by('users_followers_users_id');
        $this->db->order_by('total', 'desc'); 
        $res = $this->db->get('users_followers');
        return $res->result_array();

    }

    // get users by id
    function getUserrankbyid($userid = null ,$where = null){
        $this->db->select(array('user_id','users_photo','users_name','users_country','users_state','users_city','users_area'));

        $this->db->where(array('user_id'=>$userid));
        $this->db->where('users_country !=',null);   
        $this->db->where('users_state !=',null);
        $this->db->where('users_city !=',null);
        $this->db->where('users_area !=',null);

        if(!empty($where['country'])) $this->db->where('users_country',$where['country']);
        if(!empty($where['state'])) $this->db->where('users_state',$where['state']);
        if(!empty($where['city'])) $this->db->where('users_city',$where['city']);
        if(!empty($where['area'])) $this->db->where('users_area',$where['area']);

        
        $res = $this->db->get('users');
        return $res->row();

    }
    
      // get user posts
    function getcountUserPosts($userid = null){

        //$this->db->order_by("posts_id","DESC");
        $res = $this->db->get_where('posts',array('posts_user_id'=>$userid,'posts_active' => 1));
         return $res->num_rows();
    }
    
   
   
   
   // user poll to 
      function getuserPollto($userid = null){

        $this->db->select(array('users.user_id','users.users_name','users_photo'));
        $this->db->from('users_followers');
        $this->db->where(array('users_followers_follower_id' => $userid,'users_followers_active' => 1));
        $this->db->join('users','user_id=users_followers.users_followers_users_id');
        $res = $this->db->get();
        $return['count'] = $res->num_rows();
        $return['data']  = $res->result_array();
        return $return; 
    }

    // user poll by 
    function getuserPollby($userid = null){

        $this->db->select(array('users.user_id','users.users_name','users_photo'));
        $this->db->from('users_followers');
        $this->db->where(array('users_followers_users_id' => $userid , 'users_followers_active' => 1));
        $this->db->join('users','users.user_id = users_followers.users_followers_users_id');
        $res = $this->db->get();
        $return['count'] = $res->num_rows();
        $return['data']  = $res->result_array();
        return $return; 
    }



    // user poll to 
    function userPollto($userid = null){

        $this->db->select(array('users.user_id','users.users_name','users_photo'));
        $this->db->from('users_followers');
        $this->db->where(array('users_followers_follower_id' => $userid,'users_followers_active' => 1));
        $this->db->join('users','user_id = users_followers.users_followers_users_id');
        $res = $this->db->get();
        return ($res->num_rows() > 0) ? $res->result_array() : 0;    
    }

    // user poll by 
    function userPollby($userid = null){

        $this->db->select(array('users.user_id','users.users_name','users_photo'));
        $this->db->from('users_followers');
        $this->db->where(array('users_followers_users_id'=> $userid ,'users_followers_active' => 1));
       
        $this->db->join('users','user_id = users_followers.users_followers_follower_id');
        $res = $this->db->get();
        return ($res->num_rows() > 0) ? $res->result_array() : 0;    
    }
    
    
     // get user profile by id
     function getUserProfile($userid = null){

        $this->db->select('user_id,users_name,users_photo,users_cover_photo,users_email,users_mobile,users_mobile_code,users_country,users_state,users_city,users_area,users_location_time');
        $res = $this->db->get_where('users',array('user_id' => $userid));
        return $res->result_array();
    }
    
    // reset password
    function resetPassword($id = null , $pass = null){
        
        $this->db->where('user_id',$id);
        $update = array('users_password'=>md5($pass));
        $this->db->update('users',$update);
        return $this->db->affected_rows();
    }
    
      // sort users by category
     function sortUsers_new($country = null, $state = null, $district = null, $area = null){

        /*$this->db->where('users_country !=',$country);
        $this->db->where('users_state !=',$state);
        $this->db->where('users_city !=',$district);
        $this->db->where('users_area !=',$area);*/

        if(!empty($country)) $this->db->where('users_country',$country);
        if(!empty($state)) $this->db->where('users_state',$state);
        if(!empty($district)) $this->db->where('users_city',$district);
        if(!empty($area)) $this->db->where('users_area',$area);

        $this->db->select(array('users.user_id','users_photo','users.users_name','users_followers.users_followers_users_id','users.users_country','users.users_state','users.users_city','users.users_area','COUNT(users_followers.users_followers_users_id) as poll_count'));
        $this->db->from('users_followers');
        $this->db->join('users','users.user_id = users_followers.users_followers_users_id');
        $this->db->order_by('poll_count','desc');
        $this->db->group_by('users_followers_users_id');
        $res = $this->db->get();
        return ($res->num_rows() > 0) ? $res->result_array() : 0;
    }
    
    
    
    // check email exists
    function emailExists($email = null){

        $this->db->where(array('users_email' => $email));
        return $this->db->get('users')->num_rows();
    }

     // check email exists on edit
    function editemailExists($email = null , $userid = null){

        $this->db->where('user_id !=',$userid);
        $this->db->where(array('users_email' => $email));
        return $this->db->get('users')->num_rows();
    }
    
    
    // check mobile exists
    function mobileExists($mobile = null , $code = null){

        $this->db->select('users_mobile');
        $this->db->where('users_mobile' , $mobile);
        if(!empty($code))$this->db->where('users_mobile_code' , $code);
        return $this->db->get('users')->num_rows();
    }

    // update user profile
    function updateUserProfile($userid = null , $update = null){

        $this->db->where('user_id',$userid);
        $this->db->update('users',$update);
        return $this->db->affected_rows();
    }
    
      // get user polls count
    function getUserpollcount($userid = null){

        $this->db->where('users_followers_users_id',$userid);
        return $this->db->get('users_followers')->num_rows();
         
         
    }
    
    // get profile pic for remove from folder
    function geteditprofilepic($id = null){
        
        $this->db->select(array('users_photo','users_cover_photo'));
        $this->db->where(array('user_id'=>$id));
        $res = $this->db->get('users');
        $return['profile'] =  $res->row('users_photo');
        $return['cover'] =  $res->row('users_cover_photo');
        return $return;
    }
    
    
    // check user polled
    function getuserPolled($userid =null , $logged_user_id = null){

        $this->db->select('users_followers_id');
        $this->db->where(array('users_followers_users_id' => $userid ,'users_followers_active' => 1,'users_followers_follower_id' => $logged_user_id));
        return $this->db->get('users_followers')->num_rows();
    }
    
     // search all users front page
    function searchuserAll($search = null){

        $this->db->select(array('users_name','user_id','users_photo'));
        $this->db->where('user_id !=',1);
        $this->db->like('users_name',$search);
        $res = $this->db->get('users');
        return $res->result_array();
    }

    // search all party front page
    function searchpartyAll($search = null){

        $this->db->where('party_id!=',1);
        $this->db->where('party_active',1);
        $this->db->select(array('party_id','party_name','party_photo','party_description'));
        $this->db->like('party_name',$search);
        $res = $this->db->get('party');
        return $res->result_array();
    }

     // search all party front page
     function searchchallengeAll($search = null){

      
        $this->db->where('challenge_active',1);
        $this->db->select(array('challenge_id','user_id','users_name','users_photo','challenge_user_id','challenge_title','challenge_type','challenge_content'));
        $this->db->like('challenge_title',$search);
        $this->db->or_like('users_name',$search);
        $this->db->join("users","users.user_id = challenge.challenge_user_id");
        $res = $this->db->get('challenge');
        return $res->result_array();
    }

     // search all party front page
     function searchgroupAll($search = null){

      
        $this->db->where('group_active',1);
        $this->db->select(array('group_id','group_name','group_profile_photo','group_desc'));
        $this->db->like('group_name',$search);
        $res = $this->db->get('group_table');
        return $res->result_array();
    }



     // get users by id for global
     function getglobalUserrankbyid($userid = null){
        $this->db->select(array('user_id','users_photo','users_name','users_country','users_state','users_city','users_area'));

        $this->db->where(array('user_id' => $userid));
      

        
        $res = $this->db->get('users');
        return $res->row();

    }

    // get users rank by place and id
    function userrankbycountry($userid = null ,$country = null){
        $this->db->select(array('user_id','users_photo','users_name','users_country','users_state','users_city','users_area'));

        $this->db->where(array('user_id' => $userid , 'users_country' => $country));
      
        $res = $this->db->get('users');
      
        return $res->num_rows() > 0 ? $res->row() : 0;

    }

      // get users rank by place and id
      function userrankbystate($userid = null ,$state = null){
        $this->db->select(array('user_id','users_photo','users_name','users_country','users_state','users_city','users_area'));

        $this->db->where(array('user_id' => $userid , 'users_state' => $state));
      
        $res = $this->db->get('users');
      
        return $res->num_rows() > 0 ? $res->row() : 0;

    }

     // get users rank by place and id
     function userrankbydistrict($userid = null ,$district = null){
        $this->db->select(array('user_id','users_photo','users_name','users_country','users_state','users_city','users_area'));

        $this->db->where(array('user_id' => $userid , 'users_city' => $district));
      
        $res = $this->db->get('users');
      
        return $res->num_rows() > 0 ? $res->row() : 0;

    }

     // get users rank by place and id
     function userrankbyarea($userid = null ,$area = null){

        $this->db->select(array('user_id','users_photo','users_name','users_country','users_state','users_city','users_area'));

        $this->db->where(array('user_id' => $userid , 'users_area' => $area));
      
        $res = $this->db->get('users');
      
        return $res->num_rows() > 0 ? $res->row() : 0;

    }
    
     // all rank list
    function allrankList($country = null , $state = null , $district = null , $area = null){

        if(!empty($country)) $this->db->where('users_country',$country);
        if(!empty($state)) $this->db->where('users_state',$state);
        if(!empty($district)) $this->db->where('users_city',$district);
        if(!empty($area)) $this->db->where('users_area',$area);



        $this->db->where('users_followers_active',1);
        $this->db->select(array('users_followers_users_id','COUNT(users_followers_users_id) as total'));
        $this->db->group_by('users_followers_users_id');
        $this->db->order_by('total','desc');
        $res = $this->db->get('users_followers');
        return $res->result_array();
    }
    
     // get polled party by id
    function getpolledparty($userid = null){
        $this->db->select('users_poll_party_id');
        $this->db->where('user_id',$userid);
        $res = $this->db->get('users');
        return $res->row('users_poll_party_id');

    }

       // get polled party by id
       function getjoinedparty($userid = null){
        $this->db->select('users_party_id');
        $this->db->where('user_id',$userid);
        $res = $this->db->get('users');
        return $res->row('users_party_id');

    }
    
       // get party secret by id
     function getPartypubliccountbyid($id = null){

        $this->db->where(array('users_poll_party_id'=>$id , 'users_party_poll_type' => 0));
        $res = $this->db->get('users');
        return $res->num_rows();  
    }

     // get party  secret by id
     function getPartysecretcountbyid($id = null){

        $this->db->where(array('users_poll_party_id'=>$id , 'users_party_poll_type' => 1));
        $res = $this->db->get('users');
        return $res->num_rows();  
    }
    
    // edit user cover photo
    function editUsercoverPhoto($userid = null , $photo = null){

        $this->db->where('user_id',$userid);
        $this->db->update('users',array('users_cover_photo' => $photo));
        return $this->db->affected_rows();
    }

    // edit user user photo
    function editUserProfilePhoto($userid = null , $photo = null){

        $this->db->where('user_id',$userid);
        $this->db->update('users',array('users_photo' => $photo));
        return $this->db->affected_rows();
    }


    // get group name
    function getGroupname($gid = null){

        $this->db->select('group_name');
        $this->db->where('group_id',$gid);
        $res = $this->db->get('group_table');
        return $res->row('group_name');
    }   
    
    
    
    // change password
    function changePassword($where = null , $password = null){

        if($this->checkOldpassword($where) > 0){
        $this->db->where($where);
        $this->db->update('users',array('users_password' => $password));
        if($this->db->affected_rows() > 0){
            return 1;
        }else{

            return 0;
        }
        }
        else{
            return -1;
        }
        
    }

    // check old password
    function checkOldpassword($where){
        
        $this->db->where($where);
        return $this->db->get('users')->num_rows();

    }
    
     //function get country id
    function getCountryid($cname = null){

        $this->db->where('name',$cname);
        $res = $this->db->get('countries');
        return $res->row('id');
    }

     //function get state id
     function getStateid($cname = null){

        $this->db->where('name',$cname);
        $res = $this->db->get('states');
        return $res->row('id');
    }

    // get user location for party follow update
    function getuserlocation($userid = null){

        $this->db->select('users_country,users_state,users_city,users_area');
        $this->db->where('user_id',$userid); 
        $res = $this->db->get('users');
        $return['country']  = $res->row('users_country');
        $return['state']    = $res->row('users_state');
        $return['district'] = $res->row('users_city');
        $return['area']     = $res->row('users_area');
        return $return;

    }

    // delete user account
    function deleteUseraccount($userid = null){

        $this->db->where('user_id',$userid);
        $this->db->delete('users');
        return $this->db->affected_rows();
    
    } 

    // remove user posts on delete
    function removePosts($userid = null){

        $this->db->where('posts_user_id',$userid);
        $this->db->delete('posts');

    }

    //remove group on user delete acc
    function removeGroup($userid = null){
        
        $this->db->where('group_created_by',$userid);
        $update = array('group_active' => 0);
        $this->db->update('group_table',$update);
    }

     // get challenge type
     function getchallengetype($id = null){

        $this->db->select('challenge_type');
        $this->db->where('challenge_id',$id);
        $res = $this->db->get('challenge');
        return $res->row('challenge_type');
    }

     // get challenge type
     function getbusinessprofilechallengetype($id = null){

        $this->db->select('challenge_type');
        $this->db->where('challenge_id',$id);
        $res = $this->db->get('business_profile_challenge');
        return $res->row('challenge_type');
    }

     // remove user party posts on delete
     function removepartyPosts($userid = null){

        $this->db->where('party_posts_posted_by',$userid);
        $this->db->delete('party_posts');

    }

    // save feedback

    function saveFeedback($insert = null){

        $this->db->insert('feedback',$insert);
        return $this->db->affected_rows();

    }


     // remove user group posts on delete
     function removegroupPosts($userid = null){

        $this->db->where('group_posts_posted_by',$userid);
        $this->db->delete('group_posts');

    }

     // remove user challenge posts on delete
     function removechallengePosts($userid = null){

        $this->db->where('challenge_user_id',$userid);
        $this->db->delete('challenge');

    }

    // remove user challenge posts on delete
    function removePostspolls($userid = null){

        $this->db->where('posts_polls_user_id',$userid);
        $this->db->delete('posts_polls');

    }

    // remove user challenge posts on delete
    function removepartyPostspolls($userid = null){

        $this->db->where('party_posts_polls_user_id',$userid);
        $this->db->delete('party_posts_polls');

    }

    // remove user challenge posts on delete
    function removegroupPostspolls($userid = null){

        $this->db->where('group_posts_polls_user_id',$userid);
        $this->db->delete('group_posts_polls');

    }

    // remove user challenge posts on delete
    function removechallengepolls($userid = null){

        $this->db->where('challenge_polls_polled_by',$userid);
        $this->db->delete('challenge_polls');

    }

     // remove user challenge posts on delete
     function removechallengePostspolls($userid = null){

        $this->db->where('challenge_post_polled_by',$userid);
        $this->db->delete('challenge_post_polls');

    }



     // remove user challenge posts on delete
     function removePostscomments($userid = null){

        $this->db->where('posts_comments_user_id',$userid);
        $this->db->delete('posts_comments');

    }
     // remove user challenge posts on delete
     function removepartyPostscomments($userid = null){

        $this->db->where('party_posts_comments_user_id',$userid);
        $this->db->delete('party_posts_comments');

    }
     // remove user challenge posts on delete
     function removegroupPostscomments($userid = null){

        $this->db->where('group_posts_comments_user_id',$userid);
        $this->db->delete('group_posts_comments');

    }
     // remove user challenge posts on delete
     function removechallengecomments($userid = null){

        $this->db->where('challenge_comments_user_id',$userid);
        $this->db->delete('challenge_comments');

    }

    

    // remove user challenge posts on delete
    function removechallengepostscomments($userid = null){

        $this->db->where('challenge_comments_user_id',$userid);
        $this->db->delete('challenge_comments');

    }

     // remove user challenge posts on delete
     function removeuserPolls($userid = null){

        $this->db->where('users_followers_users_id',$userid);
        $this->db->delete('users_followers');

    }

     // remove user challenge posts on delete
     function removegroupPolls($userid = null){

        $this->db->where('users_followers_users_id',$userid);
        $this->db->delete('users_followers');

    }

     // remove user challenge posts on delete
     function removegroupuserPolls($userid = null){

        $this->db->where('users_group_followers_user_id',$userid);
        $this->db->delete('users_group_followers');

    }

    // remove user challenge posts on delete
    function removegroupuserfollowerPolls($userid = null){

        $this->db->where('users_group_followers_user_follower_id',$userid);
        $this->db->delete('users_group_followers');

    }

     // remove user challenge posts on delete
     function removepartyuserPolls($userid = null){

        $this->db->where('users_party_followers_user_id',$userid);
        $this->db->delete('users_party_followers');

    }


     // remove user challenge posts on delete
     function removepartyuserfollowerPolls($userid = null){

        $this->db->where('users_party_followers_user_follower_id',$userid);
        $this->db->delete('users_party_followers');

    }

     // remove user challenge posts on delete
     function removePostsreport($userid = null){

        $this->db->where('posts_report_user_id',$userid);
        $this->db->delete('posts_report');

    }

     // remove user challenge posts on delete
     function removepartyPostsreport($userid = null){

        $this->db->where('party_posts_report_user_id',$userid);
        $this->db->delete('party_posts_report');

    }

     // remove user challenge posts on delete
     function removegroupPostsreport($userid = null){

        $this->db->where('group_posts_report_user_id',$userid);
        $this->db->delete('group_posts_report');

    }

     // remove user challenge posts on delete
     function removechallengePostsreport($userid = null){

        $this->db->where('users_party_followers_user_follower_id',$userid);
        $this->db->delete('users_party_followers');

    }

    // remove user challenge posts on delete
    function removePostsreply($userid = null){

        $this->db->where('posts_comments_reply_user_id',$userid);
        $this->db->delete('posts_comments_reply');

    }

    // remove user challenge posts on delete
    function removepartyPostsreply($userid = null){

        $this->db->where('party_posts_comments_reply_user_id',$userid);
        $this->db->delete('party_posts_comments_reply');

    }

    // remove user challenge posts on delete
    function removegroupPostsreply($userid = null){

        $this->db->where('group_posts_comments_reply_user_id',$userid);
        $this->db->delete('group_posts_comments_reply');

    }

    // is group user joined or not
    function getjoinedgroup($userid = null , $groupid = null){

        $this->db->where(array('group_followers_active' => 1,'group_followers_group_id'=>$groupid , 'group_followers_user_id' => $userid));
        $res = $this->db->get('group_followers');
        return $res->num_rows(); 
    }

    // get group members count
    function getGroupMembersCount($group_id = null){

        $res = $this->db->get_where('group_followers',array('group_followers_group_id' => $group_id));
        return $res->num_rows();
    }

     // get challenge poll count
     function challengePollcount($challengeid = null){

        $this->db->where('challenge_polls_challenge_id',$challengeid);
        return $this->db->get('challenge_polls')->num_rows();

    }

    // get party join name
    function getjoinpartyname($userid = null){

        $this->db->select('party_name');
        $this->db->where('user_id',$userid);
        $this->db->join("party","party.party_id = users.users_party_id");
        $res = $this->db->get('users');
        return $res->row('party_name');
    }

     // get party join name
     function getpollpartyname($userid = null){

        $this->db->select('party_name');
        $this->db->where('user_id',$userid);
        $res = $this->db->join("party","party.party_id = users.users_poll_party_id");
        $res = $this->db->get('users');
        return $res->row('party_name');
    }

    // is challenge polled
    function isChallengePolled($userid = null , $challengeid = null){

        $res = $this->db->get_where('challenge_polls',array('challenge_polls_polled_by' => $userid , 'challenge_polls_challenge_id' => $challengeid));
        return $res->num_rows() > 0 ? 1 : 0;
    }
    

    //get posts by tags
    function getPostsbyTags($search = null){

        $this->db->like('posts_tags' , $search);
        $res = $this->db->get('posts');
        return $res->result_array();
    }

      //get tags
   function getTags($search = null){

    $this->db->like('posts_tags_name' , $search);
    $this->db->where('posts_tags_status' , 1);
    $res = $this->db->get('posts_tags');
    return $res->result();
}

    function removeBusinessProfile($userid = null){

        $this->db->where('bf_created_user',$userid);
        $this->db->delete('business_profile');
    }

    function makeread($id = null){
        
        $this->db->where('notifications_id',$id);
        $this->db->update('notifications',array('notifications_isview' => 1));
    }

    function getbusinessprofiledetails($id = null){
        $this->db->where(array('bf_created_user'=>$id,'bf_status'=>1));
        $res = $this->db->get("business_profile");
        $return['name'] = $res->row('bf_name');
        $return['photo'] = $res->row('bf_photo');
        return $return;
    }

    function getExistingUserProfile($mobile = null){

        $this->db->select('user_id,users_login_type,users_name,users_email,users_mobile,users_mobile_code,users_dob,users_gender,users_country,users_state,users_city,users_area,users_token,users_username,users_password,users_isprofile_complete,users_isactivated,users_photo');
        $res = $this->db->get_where('users',array('users_mobile' => $mobile));
        return $res->result_array();
    }

    function updateUserEmail($user_id = null, $email = null){
        
        $this->db->where('user_id',$user_id);
        $update = array('users_email' => $email);
        $this->db->update('users',$update);
        return $this->db->affected_rows();

    }

    function saveDeviceToken($user_id,$device_type,$device_id)
    {
        if($device_type != null && $device_id != null)
        {
            $insert = array(
                'user_id' => $user_id,
                'device_type' => $device_type,
                'device_id' => $device_id,
                'login_time' => date('Y-m-d H:i:s')
            );

            $this->db->insert('user_devices', $insert);
        }
        return 1;
        
    }

    function getUserDevices($userid,$type)
    {
        if ($this->db->table_exists('user_devices') )
        {
            $devices = $this->db->select('device_id')->from('user_devices')->where('user_id',$userid)->where('device_type',$type)->group_by('device_id')->get()->result_array();

            $device = array();

            foreach ($devices as $key => $value) 
            {
                $device[] = $value['device_id'];
            }

            return $device;
        }
        else
            return array();
    }

}