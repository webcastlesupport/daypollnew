

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        <small>View posts reports</small>
           
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>posts reports</a></li>
           
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header bg-blue">
                        <h3 class="box-title">Challenge reports</h3>
                    </div>

<br>
                    <?php $count = ($this->uri->segment(3) == null )? 1 :$this->uri->segment(3); ?>
                   <!--  <form action="" method="post">
                     <div class="form-group">
                  <label>Select report count</label>
                  <select class="form-control" style="font-weight:bold" id="reportcount">
                    <option  <?php if ($count == "") { echo ' selected="selected"'; } ?> value="">Select report count</option>
                   
                    <option style="font-weight:bold"  <?php if ($count == 2) { echo ' selected="selected"'; } ?> value="2">2</option>
                    <option style="font-weight:bold"  <?php if ($count == 3) { echo ' selected="selected"'; } ?> value="3">3</option>
                    <option style="font-weight:bold"  <?php if ($count == 4) { echo ' selected="selected"'; } ?> value="4">4</option>
                    <option style="font-weight:bold"  <?php if ($count == 5) { echo ' selected="selected"'; } ?> value="5">5</option>
                    <option style="font-weight:bold"  <?php if ($count == 6) { echo ' selected="selected"'; } ?> value="6">6</option>
                    <option style="font-weight:bold"  <?php if ($count == 7) { echo ' selected="selected"'; } ?> value="7">7</option>
                    <option style="font-weight:bold"  <?php if ($count == 8) { echo ' selected="selected"'; } ?> value="8">8</option>
                    <option style="font-weight:bold"  <?php if ($count == 9) { echo ' selected="selected"'; } ?> value="9">9</option>
                    <option style="font-weight:bold"  <?php if ($count == 10){ echo ' selected="selected"'; } ?> value="10">Above 10</option>
                  </select>
                    </div>
                    </form> -->
                    <!-- /.box-header -->
                 
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Page Name</th>
                                <th>Uploaded date</th>
                                <th>View</th>
                                <th>View reports</th>
                                <th>Block</th>
                               
                            </tr>
                            </thead>
                            <tbody>

                            <?php

                            
                            $posts=$this->Model->getChallengereports($count);
                            
                           
                            foreach ($posts as $value){

                                $value['challenge_created_date']=date('d/m/Y  h:i A' ,strtotime($value['challenge_created_date']));

                                ?>

                                <tr>

                                    
                                    <td><?=$value['bf_name']?></td>
                                    <td><?=$value['challenge_created_date']?></td>
                                    <td><button class="btn bg-blue btn-flat" onclick="getchallengedetails(<?=$value['challenge_id']?>)"><i class="fa fa-eye"></i></button></td>
                                    <td><button class="btn bg-orange btn-flat" onclick="getpostsreportdetails(<?=$value['challenge_id']?>)">View report</button></td></button></td>
                                    <?php
                                if($value['challenge_active']==1) {


                                    ?>
                                    <td><button class="btn bg-green btn-flat" onclick="blockposts(<?=$value['challenge_id']?>,<?=$value['challenge_active']?>)"><i class = "fa fa-check"></i></button>

                                        <?php
                                }

                                            else{
                                            ?>
                                    <td><button class="btn bg-red btn-flat" onclick="blockposts(<?=$value['challenge_id']?>,<?=$value['challenge_active']?>)"><i class = "fa fa-times"></i></button>

                                        <?php

                                            }

                                            ?>
                                        </button></td>
                                </tr>
                            <?php
                            }


                            ?>


                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #005384">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: white">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel" style="color: white">  <i class="fa fa-user"></i> &nbsp; Challenge details</h4>


            </div>
            <div class="modal-body">
                <section class="invoice">
                    
                   
                    
                 
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-blue">
              
              <!-- /.widget-user-image -->
           
             
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a>Expired on <span class="pull-right badge" id="challengeexpired"></span></a></li>
                <li><a>Created on <span class="pull-right badge" id="challengecreated"></span></a></li>
              
            
                <li><a>Polls <span class="pull-right badge" id="challengepolls"></span></a></li>
                <li><a>IsPayed <span class="pull-right badge" id="challengeispayed"></span></a></li>
                <li id="challengeispayedamountli"><a>Amount <span class="pull-right badge" id="challengeispayedamount"></span></a></li>
              
               
              
              </ul>
            </div>
          </div>
          <!-- /.widget-user -->
        
                    
                    
                </section>
            </div>
            <div class="modal-footer" style="background-color: #005384">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
                        </div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #005384">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: white">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel" style="color: white">  <i class="fa fa-user"></i>  Posts details</h4>


            </div>
            <div class="modal-body">
                <section class="invoice">
                    <div class="row invoice-info">
                    <h4><span class="fa fa-book"></span>        &nbsp;<span id="postheading"></span></h4>
                    </div>
                    <div class="row invoice-info">

                        <img src=""  id="postphoto"   style="margin-bottom:20px;border-radius: 4px;width:100px;height: 100px;">

                        <video id="videodiv"  style="display:none;" width="280" height="200" controls>
                        <source  src="" type="video/mp4">
                        <source src="movie.ogg" type="video/ogg">
                        Your browser does not support the video tag.
                        </video>

                        <div class="col-md-12">

                            <p><strong>User : </strong> <span id="uploadeduser"></span></p>
                            <p><strong>Posted on : </strong> <span id="postuploadeddate"></span></p>
                            <p><strong>Content : </strong> <span id="postcontent"></span></p>
                            <p><strong>Type : </strong> <span id="posttype"></span></p>
                            <p><strong>Report count : </strong> <span id="postreportcount"></span></p>


                        </div>

                    </div>
                </section>
            </div>
            <div class="modal-footer" style="background-color: #005384">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #005384">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: white">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel" style="color: white">  <i class="fa fa-user"></i>  Posts reports</h4>


            </div>
            <div class="modal-body">
                <section class="invoice">
                    <div class="row invoice-info">
                   
                    </div>
                    <div class="row invoice-info">

                       

                        <div class="col-md-12" id="reportcontent">

                            


                        </div>

                    </div>
                </section>
            </div>
            <div class="modal-footer" style="background-color: #005384">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>






<!-- Control Sidebar -->

<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<script>
    $("#businessprofileul").addClass('active');
    $("#bpchallengereportli").addClass('active');
</script>

<script>
    var blockposts=function(postid,active){

        $.post(site_url+"BusinessProfile/blockchallenge",{cid:postid,active:active},function (data) {

            if(data.details==1){

                var msg="Operation success";

            }else{
                 msg="Operation failed";

            }
            $("#custom_messages").html(msg);
            $("#messagebox").modal({  keyboard: false,backdrop:'static'});
            $("#dismmissbtn").click(function () {

                location.reload();
            });
        });

    }
</script>

<script>

var getchallengedetails=function(id){
  
    $.post(site_url+"BusinessProfile/getchallengedetails",{id:id},function(data){
        console.log(data);
        $("#challengeexpired").html(data.values.challenge_expired_date +' '+ data.values.challenge_expired_time);
        $("#challengecreated").html(data.values.challenge_created_date);
        $("#challengepolls").html(data.values.pollcount);
      
        if(data.values.challenge_ispayed == null) 
        {
            $("#challengeispayed").html("No");
        }
        else
        {  
            $("#challengeispayed").html("Yes");
            $("#challengeispayedamountli").show();
            $("#challengeispayedamount").html(data.values.challenge_payment_total);
        
        }
         $('#myModal').modal({  keyboard: false,backdrop:'static'});
    });
};

</script>

<script>


    var getpostsreportdetails = function (postid) {
       
        var mydata = "";
        $.post(site_url+"BusinessProfile/getchallengereportdetails",{id:postid},function (data) {
           
            var details = data.details;
            var i = 1;
            for(var mydat in details){
                    var datas = details[mydat];

                            mydata += '<p><strong> Report : '+i+'</strong></p>';
                            mydata += '<p><strong>Report : </strong> <span>'+datas.challenge_report_comment+'</span></p>';
                            mydata += '<p><strong>Reply : </strong> <span>'+datas.challenge_report_reply+'</span></p>';
                            i++;
            }
            
            $("#reportcontent").html(mydata);
         });
         
         $('#myModal2').modal({  keyboard: false,backdrop:'static'});
         };


</script>

<!-- page script -->
<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
        })
    })
</script>

<script>
$("#reportcount").change(function(){
    var getcount = $("#reportcount").val();
    window.location.href = site_url+"Posts/postsreports/"+getcount;
})
</script>