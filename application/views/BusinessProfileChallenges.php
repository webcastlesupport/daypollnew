


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small>View all uploaded challenges</small>
            
        </h1>
        <ol class="breadcrumb">
        <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>challenge</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
<div class="col-md-5 bg-gray-ligh border-left border-right">


<div class="form-group">

    <div  class="col-lg-4"><label><input <?php echo ($type == '') ?  "checked" : "" ;  ?> type="radio"  name="option" id="option" class="minimal" value=""> All</label></div>

    <div  class="col-lg-4"><label><input <?php echo ($type == 1) ?  "checked" : "" ;  ?> type="radio"  name="option" id="option" class="minimal" value="1"> Challenge</label></div>

    <div  class="col-lg-4"><label><input <?php echo ($type == 2) ?  "checked" : "" ;  ?> type="radio" name="option" id="option" class="minimal" value="2"> Ballot </label></div>


</div>
<br>
</div>  
  
    <div class="row">
            <div class="col-xs-12">

                <div class="box">

                    <div class="box-header bg-blue">
                        <h3 class="box-title">Challenge list</h3>
                        
                    </div>
                    <!-- /.box-header -->
                   
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                            <th>Sl.NO</th>
                            <th>Type</th>
                            <th>Page Name</th>
                            <th>Created date</th>
                            <th>Participants</th>
                            <th>View</th> 
                            <th>Verified/Closed</th>                  
                            <th>Block/Unblock</th>
                            
                            </tr>
                            </thead>
                          
                           <?php 
                
                foreach ($res as $key=>$value){
                  
                ?>
                <tr>
                  <td><?=$key+1;?></td>
                  <td><?=$value['challenge_type'] == 1 ? "challenge" : "ballot";?></td>
                  <td><?=$value['bf_name'];?></td>
                  <td><?=$value['challenge_created_date'];?></td>
                  <td><button type="button" class="btn bg-blue btn-flat " onclick="challengemembers(<?=$value['challenge_id'];?>);"><i class = "fa fa-users"><i></button></td>
                   
                  <td><button type="button" class="btn bg-blue btn-flat " onclick="challengedetails(<?=$value['challenge_id'];?>);"><i class = "fa fa-eye"><i></button></td>
                 
                  <?php if($value['challenge_isverified'] == 0){?>
                  <td>
                  <button type="button" class="btn bg-green btn-flat " onclick="verifyChallenge(<?=$value['challenge_isverified'];?>,<?=$value['challenge_id'];?>);">Verified</button>    
                  
                  </td>  
                    <?php }
                  elseif($value['challenge_isverified'] == 1){
                      ?>  
                  <td>
                  <button type="button" class="btn bg-red btn-flat " onclick="verifyChallenge(<?=$value['challenge_isverified'];?>,<?=$value['challenge_id'];?>);">closed</button>    
                  </td>
                      <?php   
                  }
                  
                
                  ?>
                 
                 
                  <?php if($value['challenge_active'] == 0){?>
                  <td>
                  <button type="button" class="btn bg-red btn-flat " onclick="blockChallenge(<?=$value['challenge_active'];?>,<?=$value['challenge_id'];?>);"><i class = "fa fa-times"></i></button>    
                  
                  </td>  
                    <?php }
                  else{
                      ?>  
                  <td>
                  <button type="button" class="btn bg-green btn-flat " onclick="blockChallenge(<?=$value['challenge_active'];?>,<?=$value['challenge_id'];?>);"><i class = "fa fa-check"></i></button>    
                  </td>
                      <?php   
                  }
                  ?>
                  
                  
                </tr>
              <?php
              
                }
                ?>


                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<!-- Modal -->
 

    
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #005384">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: white">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel" style="color: white">  <i class="fa fa-user"></i> &nbsp; Challenge details</h4>


            </div>
            <div class="modal-body">
                <section class="invoice">
                    
                   
                    
                 
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-blue">
              
              <!-- /.widget-user-image -->
           
             
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a>Expired on <span class="pull-right badge" id="challengeexpired"></span></a></li>
                <li><a>Created on <span class="pull-right badge" id="challengecreated"></span></a></li>
              
            
                <li><a>Polls <span class="pull-right badge" id="challengepolls"></span></a></li>
                <li><a>IsPayed <span class="pull-right badge" id="challengeispayed"></span></a></li>
                <li id="challengeispayedamountli"><a>Amount <span class="pull-right badge" id="challengeispayedamount"></span></a></li>
              
                <li><a>Ref number <span class="pull-right badge" id="challengeref"></span></a></li>
                <li><a>GST <span class="pull-right badge" id="challengegst"></span></a></li>
                <li><a>Service amount <span class="pull-right badge" id="challengeservice"></span></a></li>
               
              
              </ul>
            </div>
          </div>
          <!-- /.widget-user -->
        
                    
                    
                </section>
            </div>
            <div class="modal-footer" style="background-color: #005384">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
    
<script>
$(function(){

    $("#challengeispayedamountli").hide();
});
</script>

<script>

var challengedetails=function(id){
  
    $.post(site_url+"BusinessProfile/getchallengedetails",{id:id},function(data){
    
        $("#challengeexpired").html(data.values.challenge_expired_date +' '+ data.values.challenge_expired_time);
        $("#challengecreated").html(data.values.challenge_created_date);
        $("#challengepolls").html(data.values.pollcount);
      console.log(data);

        if(data.values.challenge_type == 1){
            $("#challengeref").html(data.values.challenge_payment_ref_number);
            $("#challengegst").html(data.values.challenge_payment_gst);
            $("#challengeservice").html(data.values.challenge_payment_service_amt);
        }else{

            $("#challengeref").html(0);
            $("#challengegst").html(0);
            $("#challengeservice").html(0);
        }

        if(data.values.challenge_ispayed == null) 
        {
            $("#challengeispayed").html("No");
        }
        else
        {  
            $("#challengeispayed").html("Yes");
            $("#challengeispayedamountli").show();
            $("#challengeispayedamount").html(data.values.challenge_payment_total);
        
        }
         $('#myModal').modal({  keyboard: false,backdrop:'static'});
    });
};

</script>

<script>
   $('.minimal').on('ifChecked ', function(event)
        {
            var v = $("input[name='option']:checked").val();
            location.href = site_url+"BusinessProfile/challenge/"+v;
            
        });
</script>

<script>
//iCheck for checkbox and radio inputs
$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
checkboxClass: 'icheckbox_minimal-blue',
radioClass   : 'iradio_minimal-blue'
});
</script>



<script>
    $(function () {
        
        $('#example1').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : true,
            'ordering'    : false,
            'info'        : true,
            'autoWidth'   : false
        })
    })
</script>

<script>
    var challengemembers = function(id){
        window.location.href = site_url+"BusinessProfile/viewMembers/"+id;
    }
</script>

<script>
var blockChallenge=function(active,cid){
    
        $.post(site_url+"BusinessProfile/blockchallenge",{cid:cid,active:active},function (data) {
           
            if(data.error == 0){
                var msg="operation success";
            }
            else{
                 msg="operation failed";

            }
            $("#custom_messages").html(msg);
            $("#messagebox").modal({  keyboard: false,backdrop:'static'});
            $("#dismmissbtn").click(function () {

                location.reload();
            });
        });
    
};
</script>
   


<script>
var verifyChallenge=function(active,cid){
    
        $.post(site_url+"BusinessProfile/verifychallenge",{cid:cid,active:active},function (data) {
          
            if(data.values == 1){

                var msg="operation success";

            }
            else{
                 msg="operation failed";

            }
            $("#custom_messages").html(msg);
            $("#messagebox").modal({  keyboard: false,backdrop:'static'});
            $("#dismmissbtn").click(function () {

                location.reload();
            });
        });
    
};
</script>

   <script>
    $("#businessprofileul").addClass('active');
    $("#bpchallengeli").addClass('active');
</script>






