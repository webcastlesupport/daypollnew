
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h4>
           Business Profile lists
            <small>View all profiles</small>
            
        </h1>
        <ol class="breadcrumb">
        <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>business</a></li>
        </ol>
    </section>

   
    <!-- Main content -->
    <section class="content">
    
    <div class="row">
            <!-- left column -->
        <div class="col-md-12">
                     
                       

            </div>
        </div>

    <div class="row">
            <div class="col-xs-12">

                <div class="box">

                    <div class="box-header bg-blue">
                        <h3 class="box-title">Category list</h3>
                        
                    </div>
                    <!-- /.box-header -->
                   
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Sl.No</th>
                                <th>Business profile</th>
                                <th>Created user</th>
                                <th>Created date</th>
                                <th>Category</th>
                                <th>View</th>
                                <th>Block/Unblock</th>
                            </tr>
                            </thead>
                          <?php 
                            $i = 1;
                            foreach ($data as $value){
                          ?>
                            <tr>
                                <td><?=$i;?></td>
                                <td><?=$value->bf_name;?></td>
                                <td><?=$value->created_user_name;?></td>
                                <td><?= date('d/m/Y',strtotime($value->bf_created_date));?></td>
                                <td><?=$value->category;?></td>
                                <td><button onclick = "bpdetails(<?=$value->bf_id?>)" class = "btn bg-blue btn-flat"><i class = "fa fa-eye"></i></button></td>
                                <?php if($value->bf_status == 0){?>
                                <td style="text-align:center;">
                                <button type="button" class="btn bg-red btn-flat " onclick="blockProfile(<?=$value->bf_status;?>,<?=$value->bf_id;?>);"><i class = "fa fa-times"></i></button>    
                                </td>  
                                <?php }
                                else{
                                    ?>  
                                <td>
                                <button type="button" class="btn bg-green btn-flat " onclick="blockProfile(<?=$value->bf_status;?>,<?=$value->bf_id;?>);"><i class = "fa fa-check"></i></button>    
                                </td>
                            <?php   
                             }
                            ?>
                            </tr>
                        <?php
                         $i++;
                        }
                           
                        ?>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>



<div class="modal fade" id="myModaldetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #005384">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: white">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel" style="color: white">  <i class="fa fa-user"></i> &nbsp; User details</h4>


            </div>
            <div class="modal-body">
                <section class="invoice">
          
                 
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            
            
            <div class="widget-user-header bg-black" id="bpcoverimage" style="background: url('../dist/img/photo1.png') center center;">
            
              <div class="widget-user-image">
                <img class="img-circle" height ="65px;" src="<?=base_url();?>assets/images/party/none.png" id="bpimage" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username" id="bpname"></h3>
             
              <br>
            </div>




            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a>Created on <span class="pull-right badge" id="bpcreated"></span></a></li>
                <li><a>Email <span class="pull-right badge" id="bpemail"></span></a></li>
                <li><a>Phone <span class="pull-right badge" id="bpphone"></span></a></li>
                <li><a>Website <span class="pull-right badge" id="bpwebsite"></span></a></li>
                <li><a>Category<span class="pull-right badge" id="bpcategory"></span></a></li>
                <li><a>Sub category 1<span class="pull-right badge" id="bpcategory1"></span></a></li>
                <li><a>Sub category 2<span class="pull-right badge" id="bpcategory2"></span></a></li>
                <li><a>Sub category 3<span class="pull-right badge" id="bpcategory3"></span></a></li>
                <li><a>Created user <span class="pull-right badge" id="bpcreateduser"></span></a></li>
                <li><a>Total polls<span class="pull-right badge" id="bppolls"></span></a></li>
               
              
              </ul>
            </div>
          </div>
          <!-- /.widget-user -->
        
                    
                    
                </section>
            </div>
            <div class="modal-footer" style="background-color: #005384">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

  

    

<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
        })
    })
</script>

<script>
    var editCategory = function(id){
        location.href=site_url+"Category/Category/"+id
    }
    </script>

<script>
var blockProfile = function(active,cid){
    
        $.post(site_url+"BusinessProfile/blockProfile",{cid:cid,active:active},function (data) {

        
            if(data.error == 0){
                var msg="operation success";
            }
            else{
                 msg="operation failed";
            }
            $("#custom_messages").html(msg);
            $("#messagebox").modal({  keyboard: false,backdrop:'static'});
            $("#dismmissbtn").click(function () {

                location.reload();
            });
        });
};
</script>

<script>
    $("#category-form").submit(function(){
        event.preventDefault();
        $.ajax({
            url  : site_url+"Category/saveCategory",
            data : {cname : $("#cname").val(),cid : $("#cid").val()},
            type : "POST",
            success : function(data){
               
                $("#custom_messages").html(data.message);
                $("#messagebox").modal({  keyboard: false,backdrop:'static'});
                setTimeout(function(){
                    location.href = site_url+"Category/Category";
                },1000);
            }
        });
    });
</script>
   

   <script>

var bpdetails=function(id){

    $.post(site_url+"BusinessProfile/businessprofiledetails",{id:id},function(data){
        
        
       console.log(data);
        var active="";
            
            if(data.values.bf_status==1)active="Active";
             else active="Blocked";
            
          
            
            $('#bpcoverimage').css({"background":"url('"+data.values.bf_cover_photo+"') center center"});

            $("#bpimage").attr("src",data.values.bf_photo); 
            $("#bpemail").html(data.values.bf_email);
            $("#bpcreated").html(data.values.bf_created_date);
            $("#bpactive").html(active);
            $("#bpcategory").html(data.values.category);
            $("#bpcategory1").html(data.values.subcategory1);
            $("#bpcategory2").html(data.values.subcategory2);
            $("#bpcategory3").html(data.values.subcategory3);
            $("#bpphone").html(data.values.bf_phone);
            $("#bppolls").html(data.values.polls);
            $("#bpwebsite").html(data.values.bf_website);
            $("#bpcreateduser").html(data.values.created_user_name);
         
            
         $('#myModaldetails').modal({  keyboard: false,backdrop:'static'});
    });
};

</script>   

<script>
    $("#businessprofileul").addClass('active');
    $("#businessprofileli").addClass('active');
</script>






