
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h4>
           Business Profile Category
            <small>View all categories</small>
            
        </h1>
        <ol class="breadcrumb">
        <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>category</a></li>
        </ol>
    </section>

   
    <!-- Main content -->
    <section class="content">
    
    <div class="row">
            <!-- left column -->
        <div class="col-md-12">
                     
            <div class="panel box box-primary">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a id ="addcategorybtn" style="text-decoration:none" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed">
                        <button class = "btn bg-blue btn-flat"> Add category</button>
                      </a>
                    </h4>
 
                    <div class="box-tools pull-right">
                            <button data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="btn btn-box-tool">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="box-body">
                    <form role="form" action="javascript:void(0)" method="post" id="category-form">
                        <input type="hidden" name="cid" id="cid" value="<?= isset($category_id) ?  $category_id :  NULL;?>">
                        <div class="box-body" style="display: block;">
                            
                        <div class="form-group">
                                <label for="title">Category name</label>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
                                        <input value = "<?= isset($cname) ?  $cname :  NULL;?>" type="text" placeholder="category name" id="cname" name="cname" class="form-control">
                                    </div>
                                </div>
                            </div>
                           
                            
                        </div>
                        <div class="box-footer" style="display: block;">
                            <button type="submit" class="btn btn-flat bg-green pull-right">
                            <?= isset($category_id) ?  'Update' :  'Add';?>                     
                            </button>
                            <button style ="margin-right:4px;" type="submit" class="btn btn-flat pull-right"  onclick="location.href=''">
                                Cancel                   
                            </button>
                        </div>
                    </form>
                    </div>
                  </div>
                </div>              

            </div>
        </div>

    <div class="row">
            <div class="col-xs-12">

                <div class="box">

                    <div class="box-header bg-blue">
                        <h3 class="box-title">Category list</h3>
                        
                    </div>
                    <!-- /.box-header -->
                   
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                            <td>SL.NO</td>
                            <th>Category</th>
                            <th>Created date</th>
                            <th>Modify</th>
                            <th>Status</th>
                            
                            </tr>
                            </thead>
                          
                           <?php 
                
                
                foreach ($data as $key=>$value){
                   
                ?>
                <tr>
                  <td><?=$key+1;?></td>
                  <td><?=$value['bfc_name'];?></td>
                  <td><?=$value['bfc_created_date'];?></td>
                           
                  <td><button onclick = "editCategory(<?=$value['bfc_id'];?>)" class = "btn bg-blue btn-flat"><i class = "fa fa-edit"></i></button></td>
                  <?php if($value['bfc_status'] == 0){?>
                  <td style="text-align:center;">
                  <button type="button" class="btn bg-red btn-flat " onclick="blockCategory(<?=$value['bfc_status'];?>,<?=$value['bfc_id'];?>);"><i class = "fa fa-times"></i></button>    
                  
                  </td>  
                    <?php }
                  else{
                      ?>  
                  <td>
                  <button type="button" class="btn bg-green btn-flat " onclick="blockCategory(<?=$value['bfc_status'];?>,<?=$value['bfc_id'];?>);"><i class = "fa fa-check"></i></button>    
                  </td>
                      <?php   
                  }
                  ?>
                  
                  
                </tr>
              <?php
          
                }
                
                ?>


                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #005384">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: white">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel" style="color: white">  <i class="fa fa-user"></i> &nbsp; Challenge details</h4>


            </div>
            <div class="modal-body">
                <section class="invoice">
                    
                   
                    
                 
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-blue">
              
              <!-- /.widget-user-image -->
           
             
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a>Expired on <span class="pull-right badge" id="challengeexpired"></span></a></li>
                <li><a>Created on <span class="pull-right badge" id="challengecreated"></span></a></li>
              
            
                <li><a>Polls <span class="pull-right badge" id="challengepolls"></span></a></li>
                <li><a>IsPayed <span class="pull-right badge" id="challengeispayed"></span></a></li>
                <li id="challengeispayedamountli"><a>Amount <span class="pull-right badge" id="challengeispayedamount"></span></a></li>
              
               
              
              </ul>
            </div>
          </div>
                </section>
            </div>
            <div class="modal-footer" style="background-color: #005384">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
    
<?php
if($category_id){

?>
<script>
   
    $("#addcategorybtn").click();
</script>
<?php
}
?>    

<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
        })
    })
</script>

<script>
    var editCategory = function(id){
        location.href=site_url+"Category/Category/"+id
    }
    </script>

<script>
var blockCategory=function(active,cid){
    
        $.post(site_url+"Category/blockCategory",{cid:cid,active:active},function (data) {

        
            if(data.error == 0){
                var msg="operation success";
            }
            else{
                 msg="operation failed";
            }
            $("#custom_messages").html(msg);
            $("#messagebox").modal({  keyboard: false,backdrop:'static'});
            $("#dismmissbtn").click(function () {

                location.reload();
            });
        });
};
</script>

<script>
    $("#category-form").submit(function(){
        event.preventDefault();
        $.ajax({
            url  : site_url+"Category/saveCategory",
            data : {cname : $("#cname").val(),cid : $("#cid").val()},
            type : "POST",
            success : function(data){
               
                $("#custom_messages").html(data.message);
                $("#messagebox").modal({  keyboard: false,backdrop:'static'});
                setTimeout(function(){
                    location.href = site_url+"Category/Category";
                },1000);
            }
        });
    });
</script>
   

<script>
    $("#businessprofileul").addClass('active');
    $("#categoryli").addClass('active');
</script>






