

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          <small>view challenge participants</small>
            
        </h1>
        <ol class="breadcrumb">
        <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>Challenge participants</a></li>
        </ol>
    </section>


    


    <!-- Main content -->
    <section class="content">

     <?php if($winner_msg) { ?>
        <div class="callout callout-info">
                <h4><i class ="fa fa-user-circle"></i>  No winner in this challenge</h4>

                <p></p>
     </div>
     <?php } ?>
              
    <div class="row">
<!---------------------------------  winner table ------------------------------>

<?php if($winner){  
    
    ?>
        <div class="col-xs-12">

        <div class="box">


    <div class="box-header bg-red">
        <h3 class="box-title">Challenge winner</h3>
    </div>
    <!-- /.box-header -->

  
    <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Photo</th>
                <th>Name</th>
                <th>Country</th>
                <th>State</th>
                <th>City</th>
                <th>Mobile</th>

            </tr>
            </thead>
            <tbody>

            <?php

            if($winner){
              
            foreach ($winner as $value){

                ?>

               <td><img src="<?=base_url().'assets/images/users/'.$value['users_photo']?>"  style="margin-bottom:20px;border-radius: 4px;width:100px;height: 100px;">
               <td><?=$value['users_name']?></td>
               
               <td><?=$value['users_country']?></td>
               <td><?=$value['users_state']?></td> 
               <td><?=$value['users_city']?></td>
               <td><?=$value['users_mobile']?></td>
               </tr>
                <?php
                   }
                }
                ?>
        </table>
    </div>
            <?php } ?>
<!--------------------------------- winner table ends here------------------------------>

            <div class="col-xs-12">

                <div class="box">


                    <div class="box-header bg-blue">
                        <h3 class="box-title">Challenge participants</h3>
                    </div>
                    <!-- /.box-header -->

                  
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Photo</th>
                                <th>Name</th>
                                <th>Country</th>
                                <th>State</th>
                                <th>City</th>
                                <th>Mobile</th>

                            </tr>
                            </thead>
                            <tbody>

                            <?php
        

                            if($data){
                               
                            foreach ($data as $value){

                                ?>

                               <td><img src="<?=base_url().'assets/images/users/'.$value['users_photo']?>"  style="margin-bottom:20px;border-radius: 4px;width:100px;height: 100px;">
                               <td><?=$value['users_name']?></td>
                               
                               <td><?=$value['users_country']?></td>
                               <td><?=$value['users_state']?></td> 
                               <td><?=$value['users_city']?></td>
                               <td><?=$value['users_mobile']?></td>
                               </tr>
                                <?php
                                   }
                                }
                                ?>


                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #005384">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: white">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel" style="color: white">  <i class="fa fa-user"></i>  Posts details</h4>


            </div>
            <div class="modal-body">
                <section class="invoice">
                    <div class="row invoice-info">
                    <h4><span class="fa fa-book"></span>        &nbsp;<span id="postheading"></span></h4>
                    </div>
                    <div class="row invoice-info">

                        <img src=""  id="postphoto"   style="margin-bottom:20px;border-radius: 4px;width:100px;height: 100px;">

                        <video id="videodiv"  style="display:none;" width="280" height="200" controls>
                        <source  src="" type="video/mp4">
                        <source src="movie.ogg" type="video/ogg">
                        Your browser does not support the video tag.
                        </video>

                        <div class="col-md-12">

                            <p><strong>User : </strong> <span id="uploadeduser"></span></p>
                            <p><strong>Posted on : </strong> <span id="postuploadeddate"></span></p>
                            <p><strong>Content : </strong> <span id="postcontent"></span></p>
                            <p><strong>Type : </strong> <span id="posttype"></span></p>




                        </div>

                    </div>
                </section>
            </div>
            <div class="modal-footer" style="background-color: #005384">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>









<!-- Control Sidebar -->

<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<script>
    //$("#postsul").addClass('active');
    $("#challengeli").addClass('active');
</script>

<script>
    var blockposts=function(postid,active){

        

        $.post(site_url+"Group/blockposts",{id:postid,active:active},function (data) {

            if(data.details==1){

                var msg="Operation success";

            }
            else{
                 msg="Operation failed";

            }
            $("#custom_messages").html(msg);
            $("#messagebox").modal({  keyboard: false,backdrop:'static'});
            $("#dismmissbtn").click(function () {

                location.reload();
            });
        });

    }
</script>

<script>
    var getpostdetails = function (postid) {



        $.post(site_url+"Posts/getPostsdetails",{id:postid},function (data) {


            var posttype = "";
            $("#uploadeduser").html(data.details.users_name);
            $("#postuploadeddate").html(data.details.posts_uploaded_date);
            $("#postcontent").html(data.details.posts_content);
            if(data.details.posts_type==1)  posttype = "public";
            else posttype = "private";
            $("#posttype").html(posttype);
            $("#postheading").html(data.details.posts_title);
            if(data.photo_or_video=='video'){
                $("#videodiv").attr("src",data.details.posts_photo_or_video);
                $("#videodiv").show();
                $("#postphoto").hide();
            }else{
            $("#postphoto").attr("src",data.details.posts_photo_or_video);
            $("#videodiv").hide();
            $("#postphoto").show();
        }




        });
        $('#myModal').modal({  keyboard: false,backdrop:'static'});
    };
</script>

<!-- page script -->
<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
        })
    })
</script>