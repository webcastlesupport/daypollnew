

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Group page
            <small>edit group here</small>
        </h1>
        <ol class="breadcrumb">
        <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>group</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">

                    <div class="box-header" style="background-color: darkgray">
                        <h3 class="box-title">Group list</h3>
                    </div>
                    <!-- /.box-header -->

                  
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                            <th>Group name</th>
                            <th>Created by</th>
                            <th>View members</th>
                            <th>View posts</th>
                           
                            <th>Block/Unblock</th>
                            
                            </tr>
                            </thead>

                            <?php 
                $res = $this->GroupModel->getallGroup();
                
                foreach ($res as $value){
                    
                
                ?>
                <tr>
                  <td><?=$value['group_name'];?></td>
                  <td><?=$value['users_name'];?></td>

                  <td><button class = "btn bg-blue btn-flat"><a style="color:white" href="<?=base_url().'Group/viewMembers/'.$value['group_id']?>"><i class = "fa fa-eye" ></i></a></button></td>
                 
                  <td><button class = "btn bg-blue btn-flat"><a style="color:white" href="<?=base_url().'Group/viewPosts/'.$value['group_id']?>"><i class = "fa fa-eye" ></i></a></button></td>
                  <?php if($value['group_active'] == 0){?>
                  <td><button class = "btn bg-red btn-flat" onclick="blockGroup(<?=$value['group_id'];?>,<?=$value['group_active'];?>);"><i class ="fa fa-times"></i></button></td>
                    <?php }
                  else{
                      ?>  
                  <td><button class = "btn bg-green btn-flat" onclick="blockGroup(<?=$value['group_id'];?>,<?=$value['group_active'];?>);"><i class ="fa fa-check"></i></button></td>
                  <?php   
                  }
                  ?>
                  
                </tr>
              <?php
              
                }
                ?>



                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<!-- Modal -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #005384">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: white">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel" style="color: white">  <i class="fa fa-user"></i> &nbsp; User details</h4>


            </div>
            <div class="modal-body">
                <section class="invoice">
                    
                   
                    
                 
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-blue">
              
              <!-- /.widget-user-image -->
           
              <h4 class="widget-user-desc" id="partydetname">Party name</h4>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a>Created on <span class="pull-right badge" id="partycreated"></span></a></li>
                <li><a>Active <span class="pull-right badge" id="partyactive"></span></a></li>
              
                <li><a>Approved <span class="pull-right badge" id="partyapproved"></span></a></li>
                <li><a>Polls <span class="pull-right badge" id="partypolls"></span></a></li>
                <li><a>Total joins<span class="pull-right badge" id="partyjoins"></span></a></li>
               
              
              </ul>
            </div>
          </div>
          <!-- /.widget-user -->
        
                    
                    
                </section>
            </div>
            <div class="modal-footer" style="background-color: #005384">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

 

<script>
    $("#groupul").addClass('active');
    $("#groupli").addClass('active');
</script>


<script>
var blockGroup=function(partyid,active){
    


        $.post(site_url+"Group/blockgroup",{id:partyid,active:active},function (data) {


            if(data.values == 1){

                var msg="operation success";

            }
            else{
                 msg="operation failed";

            }
            $("#custom_messages").html(msg);
            $("#messagebox").modal({  keyboard: false,backdrop:'static'});
            $("#dismmissbtn").click(function () {

                location.reload();
            });
        });
    
};
</script>

<script>

    var groupdetails=function(id){
        
        $.post(site_url+"Party/getpartydetails",{id:id},function(data){
            
            
            var active="";
            if(data.values.party_active==1)active="Active";
             else active="Blocked";
            
            var approved="";
            if(data.values.party_isapproved==1)approved="Approved";
             else approved="Not approved";
             
            $("#partydetname").html(data.values.party_name);
            $("#partycreated").html(data.values.party_created_date);
            $("#partyactive").html(active);
            $("#partyapproved").html(approved);
            $("#partypolls").html(data.polls);
            $("#partyjoins").html(data.followers);
            
            
             $('#myModal').modal({  keyboard: false,backdrop:'static'});
        });
    };
   
</script>


<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
        })
    })
</script>


    