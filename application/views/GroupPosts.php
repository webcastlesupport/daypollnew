

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          <small>view group posts</small>
            
        </h1>
        <ol class="breadcrumb">
        <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>Group posts</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">


                    <div class="box-header" style="background-color: darkgray">
                        <h3 class="box-title">Group posts</h3>
                    </div>
                    <!-- /.box-header -->

                  
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Post</th>
                                <th>Posted by</th>
                                <th>Post title</th>
                                <th>Uploaded date</th>
                                <th>View</th>
                                <th>Block</th>

                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            $s3_url   = "https://s3.amazonaws.com/daypoll/";
                            if($data){
                            foreach ($data as $value){

                                $value['group_posts_uploaded_date']=date('d/m/Y  h:i A' ,strtotime($value['group_posts_uploaded_date']));

                                ?>

                                <tr>
                                    <?php
                                        switch ($value['photo_or_video']){

                                            case 0:
                                            ?>
                                   <td><img src="<?=$s3_url.'assets/images/groupposts/'.$value['group_posts_photo_video']?>"  style="margin-bottom:20px;border-radius: 4px;width:100px;height: 100px;">
                                    </td>
                                            <?php
                                        break;
                                            case 1:
                                            ?>
                                            <td><img src="<?=$s3_url.'assets/images/groupposts/thumbnail/'.$value['group_posts_video_thumbnail']?>"  style="margin-bottom:20px;border-radius: 4px;width:100px;height: 100px;">
                                            </td>
                                            <?php
                                            break;
                                            
                                            case -1:
                                            ?>
                                            <td><img src="<?=$s3_url?>assets/images/groupposts/none.png"  style="margin-bottom:20px;border-radius: 4px;width:100px;height: 100px;">
                                            </td>
                                            <?php
                                    }
                                    ?>

                                    
                                     
                                    <td><?=$value['users_name']?></td>
                                    <td><?=$value['group_posts_title']?></td>
                                    <td><?=$value['group_posts_uploaded_date']?></td>
                                    <td><button class="btn-sm btn-info" onclick="getpostdetails(<?=$value['group_posts_id']?>)">View</button></td>


                                <?php
                                if($value['group_posts_active']==1) {


                                    ?>
                                    <td><button class="btn-sm btn-danger" onclick="blockposts(<?=$value['group_posts_id']?>,<?=$value['group_posts_active']?>)">Block</button>

                                        <?php
                                }

                                            else{
                                            ?>
                                    <td><button class="btn-sm btn-success" onclick="blockposts(<?=$value['group_posts_id']?>,<?=$value['group_posts_active']?>)">Unblock</button>

                                        <?php

                                            }

                                            ?>
                                        </button></td>

                                </tr>
                            <?php
                            }
                        }
                        else{
                            
                        }


                            ?>



                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #005384">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: white">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel" style="color: white">  <i class="fa fa-user"></i>  Posts details</h4>


            </div>
            <div class="modal-body">
                <section class="invoice">
                    <div class="row invoice-info">
                    <h4><span class="fa fa-book"></span>        &nbsp;<span id="postheading"></span></h4>
                    </div>
                    <div class="row invoice-info">

                        <img src=""  id="postphoto"   style="margin-bottom:20px;border-radius: 4px;width:100px;height: 100px;">

                        <video id="videodiv"  style="display:none;" width="280" height="200" controls>
                        <source  src="" type="video/mp4">
                        <source src="movie.ogg" type="video/ogg">
                        Your browser does not support the video tag.
                        </video>

                        <div class="col-md-12">

                            <p><strong>User : </strong> <span id="uploadeduser"></span></p>
                            <p><strong>Posted on : </strong> <span id="postuploadeddate"></span></p>
                            <p><strong>Content : </strong> <span id="postcontent"></span></p>
                        
                        </div>

                    </div>
                </section>
            </div>
            <div class="modal-footer" style="background-color: #005384">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>









<!-- Control Sidebar -->

<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<script>
    //$("#postsul").addClass('active');
    $("#groupli").addClass('active');
</script>

<script>
    var blockposts=function(postid,active){

        console.log(active);

        $.post(site_url+"Group/blockposts",{id:postid,active:active},function (data) {

            if(data.details==1){

                var msg="Operation success";

            }
            else{
                 msg="Operation failed";

            }
            $("#custom_messages").html(msg);
            $("#messagebox").modal({  keyboard: false,backdrop:'static'});
            $("#dismmissbtn").click(function () {

                location.reload();
            });
        });

    }
</script>

<script>
    var getpostdetails = function (postid) {



        $.post(site_url+"Group/getgroupPostsdetails",{id:postid},function (data) {

            console.log(data);

            var posttype = "";
            $("#uploadeduser").html(data.details.users_name);
            $("#postuploadeddate").html(data.details.group_posts_uploaded_date);
            $("#postcontent").html(data.details.group_posts_content);
            // if(data.details.posts_type==1)  posttype = "public";
            // else posttype = "private";
            // $("#posttype").html(posttype);
            $("#postheading").html(data.details.group_posts_title);
            if(data.photo_or_video=='video'){
                $("#videodiv").attr("src",data.details.group_posts_photo_video);
                $("#videodiv").show();
                $("#postphoto").hide();
            }else{
            $("#postphoto").attr("src",data.details.group_posts_photo_video);
            $("#videodiv").hide();
            $("#postphoto").show();
        }




        });
        $('#myModal').modal({  keyboard: false,backdrop:'static'});
    };
</script>

<!-- page script -->
<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
        })
    })
</script>