

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h4>
            Hashtag page
            <small>edit hashtag here</small>
        </h4>
        <ol class="breadcrumb">
        <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>hashtag</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">

                    <div class="box-header bg-blue">
                        <h3 class="box-title">Hashtag list</h3>
                    </div>
                    <!-- /.box-header -->

                  
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                            <th>#</th>
                            <th>Hashtag</th>
                            <th>Created date</th>
                            
                           
                            <th>Block/Unblock</th>
                            <th>Modify</th>
                            </tr>
                            </thead>

                            <?php 
                $res = $this->HashTagModel->getHashTags();
                $i = 1;
                foreach ($res as $value){
                  
                ?>
                <tr>
                  <td><?=$i;?></td>
                  <td><?=$value['posts_tags_name'];?></td>
                  <td><?=$value['posts_tags_date'];?></td>

                  <?php if($value['posts_tags_status'] == 0){?>
                  <td><button onclick="blockHashTag(<?=$value['posts_tags_id'];?>,<?=$value['posts_tags_status'];?>);" class="btn bg-red btn-flat"><i class = "fa fa-times"></i></button></td>
                    <?php } 
                  else{
                      ?>  
                  <td><button onclick="blockHashTag(<?=$value['posts_tags_id'];?>,<?=$value['posts_tags_status'];?>);" class="btn bg-green btn-flat"><i class ="fa fa-check"></i></button></td>
                  <?php   
                  }
                  ?>
                   <td><button onclick="editHashTag(<?=$value['posts_tags_id'];?>);" class="btn bg-blue btn-flat"><i class = "fa fa-pencil-square-o"></i></button></td>
                 
                  
                </tr>
              <?php
              $i++;
                }
                
                ?>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<!-- Modal -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #005384">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: white">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel" style="color: white">  <i class="fa fa-user"></i> &nbsp; Edit hashtag</h4>

            </div>
            <div class="modal-body">
            <section class="invoice">
                
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
           
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li>
                <form action="javascript:void(0)" id="editHashTagForm" method="post">
                <input type="hidden" id="hashtageditid" name="hashtageditid">
                <label>Hashtag:</label>
                <input type ="text" id="hashtagedit" name="hashtagedit" class="form-control" placeholder="Enter hashtag name">
                </form>
                </li>
               </ul>
            </div>
          </div>
          <!-- /.widget-user -->
        
                    
                    
                </section>
            </div>
            <div class="modal-footer" style="background-color: #005384">
                <button type="button" id="updateHashtagbtn" class="btn btn-success">Update</button>
                <button type="button" id="dissmissmodal" class="btn btn-default" data-dismiss="modal">Cancel</button>

            </div>
        </div>
    </div>
</div>

 

<script>
  
    $("#hashtagli").addClass('active');
</script>


<script>

$('#updateHashtagbtn').click(function(){

    $.ajax({
        type    :'post',
        url     :'<?=base_url();?>Hashtag/updateHashTag',
        data    :{id:$("#hashtageditid").val(),name:$("#hashtagedit").val()},
        success :function(data){
            $("#dissmissmodal").click();
            $("#custom_messages").html(data.msg);
            $("#messagebox").modal({  keyboard: false,backdrop:'static'});
            $("#dismmissbtn").click(function () {
                location.reload();
            });
        }
    });
});
</script>


<script>


var blockHashTag=function(id,active){
    
        $.ajax({
            type    : "POST",
            url     : "<?=base_url();?>Hashtag/blockHashTag",
            data    : {id:id,active:active},
            success : function(data){
                if(data.status == true){

                    var msg="operation success";

                    }
                    else{
                    msg="operation failed";

                    }
                    $("#custom_messages").html(msg);
                    $("#messagebox").modal({  keyboard: false,backdrop:'static'});
                    $("#dismmissbtn").click(function () {

                    location.reload();
                    });
            }
        });
    
};
</script>

<script>

    var editHashTag=function(id){
        
        $.ajax({

            type    : "POST",
            url     : "<?=base_url();?>Hashtag/getHashTagbyId",
            data    : {id:id},
            success :function(data){

                $("#hashtageditid").val(data.data.posts_tags_id);
                $("#hashtagedit").val(data.data.posts_tags_name);
                $('#myModal').modal({  keyboard: false,backdrop:'static'});
            }
        });
    };
   
</script>


<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
        })
    })
</script>


    