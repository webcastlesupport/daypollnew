<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Daypolls | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?=base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?=base_url();?>assets/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url();?>assets/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]-->
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <!--[endif]-->
<style>
    .errorusername{color: red;}
    .errorpassword{color: red;}
</style>
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">

        <img class="profile-user-img img-responsive img-circle " src="<?=base_url();?>assets/images/logo-150-x108.png">
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>


        <img class="profile-user-img img-responsive img-circle " id="photo" src="<?=base_url();?>assets/dist/img/none.png" alt="cover  picture">
        <br>
    
       
       <div id="adminlogindiv" >
        <form action="javascript:void(0)" method="post" id="adminloginform">
            <div class="form-group has-feedback">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                <input type="text" name="username" id="username" class="form-control" placeholder="Username">


            </div>
            <div class="form-group has-feedback">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                <input type="password" name="password" id="password" class="form-control" placeholder="Password">


            </div>
            <div class="row">

                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" id="loginsubmitbtn" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
                <div class="col-xs-8">
                <a style="float:right" href="javascript:void(0);" onClick="loadform(2)">Forgot password</a><br>
                <div>
            </div>

            
        </form>
       
       </div>



    </div>

     

    </div>
    <div id="adminpwdforgot" >
        <form action="javascript:void(0)" method="post" id="adminpwdforgotform">
            <div class="form-group has-feedback">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                <input type="text" name="pwdemail" id="pwdemail" class="form-control" placeholder="Enter email">


            </div>
            
            <div class="row">

                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" id="pwdforgetbtn" class="btn btn-primary btn-block btn-flat">Send</button>
                </div>
                <!-- /.col -->
                <div class="col-xs-8">
                <a style="float:right" href="javascript:void(0);" onClick="loadform(1)">Back to login</a><br>
                <div>
            </div>

            
        </form>
       
       </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?=base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url();?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?=base_url();?>assets/plugins/iCheck/icheck.min.js"></script>

<script>
    $(function () {

    loadform(1);
       getphoto();


        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' /* optional */
        });

    });

</script>

<script>

var loadform =function(pageid){

if(pageid===1){$("#adminpwdforgot").hide();$("#adminlogindiv").show();}
if(pageid===2){$("#adminlogindiv").hide();$("#adminpwdforgot").show();}

}
</script>

<script>

    var getphoto=function () {

        $("#username").focusout(function () {
            var user = $("#username").val();
            $.post(site_url + "Login/getAdminloginphoto", {user: user}, function (data) {

                    $('#photo').attr('src', data.photo);


            });
        });
    };
</script>

<script src="<?=base_url();?>assets/dist/js/jquery.toaster.js"></script>

<script src="<?=base_url();?>assets/dist/js/jquery.validate.min.js"></script>



<script>
    $.validator.setDefaults({

        // errorClass:'help-block',
        highlight: function (element) {
            $(element)
                .closest('.form-group')
                .addClass('has-error');

        },
        unhighlight: function (element) {
            $(element)
                .closest('.form-group')
                .removeClass('has-error')
                .addClass('has-success');

        }
    });


    $("#adminpwdforgotform").validate({

        rules: {

           pwdemail: {required: true}
      
        },

        messages: {

            pwdemail: {required: "Please enter your emailid"}

        }

    });

</script>



<script>
    $.validator.setDefaults({

        // errorClass:'help-block',
        highlight: function (element) {
            $(element)
                .closest('.form-group')
                .addClass('has-error');

        },
        unhighlight: function (element) {
            $(element)
                .closest('.form-group')
                .removeClass('has-error')
                .addClass('has-success');

        }
    });


    $("#adminloginform").validate({

        rules: {


            username: {required: true},
            password:{required:true}

        },

        messages: {

            username: {required: "Please enter your username"},
            password:{required:"Please enter a password"}


        }

    });




</script>


<script>
    var site_url="<?=base_url();?>";
    $("#adminloginform").on("submit",function (event) {

        event.preventDefault();
        if(!$("#adminloginform").valid()){return false;}

        $.post(site_url+"Login/check_login",{username:$("#username").val(),password:$("#password").val()},function (data) {


                    if(data.error===1) {


                var priority = 'danger';
                var title = 'error';
                var message = data.message;
            }
            else{
                 priority = 'success';
                 title = 'success';
                 message = data.message;

                 setTimeout(function () {
                     window.location.replace(data.url);
                 },500);
            }
            $.toaster({ priority : priority, title : title, message : message });
        });
    });
</script>

<script>


$("#adminpwdforgotform").on("submit",function (event) {

event.preventDefault();
if(!$("#adminpwdforgotform").valid()){return false;}

$.post(site_url+"Login/sendEmailpwd",{email:$("#pwdemail").val()},function (data) {


            if(data.error===1) {


        var priority = 'danger';
        var title = 'error';
        var message = data.message;
    }
    else{
         priority = 'success';
         title = 'success';
         message = data.message;

         setTimeout(function () {
             window.location.replace(data.url);
         },500);
    }
    $.toaster({ priority : priority, title : title, message : message });
});
});

  

</script>


</body>
</html>
