<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h4>
            Party page
            <small>edit party here</small>
        </h4>
        <ol class="breadcrumb">
        <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>party</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">


                    <div class="box-header bg-blue">
                        <h3 class="box-title">Registered party list  &nbsp;  <span><button id="new_party_button" class="btn  btn-sm" style="color:#273449">New party</button></span></h3>
                       
                    </div>
                    <!-- /.box-header -->

                  
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Party name</th>
                                <th>Party short name</th>
                                <th>Created by</th>
                                <th>View</th>
                                <th>Status</th>
                                <th>Approve / Reject</th>
                                <th>Change image</th>
                                <th>Cover photo</th>
                                <th>Edit</th>

                            </tr>
                            </thead>
                            <?php

                            $res = $this->PartyModel->getallParty();

                            foreach ($res as $value){
                    
                
                            ?>
    
   
                            <tr>
                            <td><?=$value['party_name'];?></td>
                            <td><?=$value['party_code'];?></td>
                            <td><?=$value['users_name'];?></td>
                            <td><button class ="btn bg-blue btn-flat"  onclick="partydetails(<?=$value['party_id'];?>);"><i class = "fa fa-eye"></i></button></</td>
                            
                            <?php if($value['party_active'] == 0){?>
                            <td><button class ="btn bg-green btn-flat" onclick="blockParty(<?=$value['party_id'];?>,<?=$value['party_active'];?>);"><i class="fa fa-check"></i></button></td>
                                <?php }
                            else{
                                        ?>  
                                    <td><button class ="btn bg-red btn-flat" onclick="blockParty(<?=$value['party_id'];?>,<?=$value['party_active'];?>);"> <i class="fa fa-times"></i></button></td>
                                    <?php   
                                    }
                                    ?>
                                    
                                    
                                    <?php if($value['party_isapproved'] == 1){?>
                                    <td><button class ="btn bg-green btn-flat" onclick="approveParty(<?=$value['party_id'];?>,<?=$value['party_isapproved'];?>)"><i class = "fa fa-check"></i></button></td>
                                    <?php }
                                    else{
                                        ?>
                                        <td><button class ="btn bg-red btn-flat" onclick="approveParty(<?=$value['party_id'];?>,<?=$value['party_isapproved'];?>)"><i class = "fa fa-times"></i></button></td>
                                    <?php       
                                    }
                                    ?>
                                   <td><button class="btn bg-orange btn-flat" onclick="uploadImage(<?=$value['party_id'];?>)" href="javascript:void(0)"><i class = "fa fa-picture-o"></i></button></td>
                                  
                                   <td><button class="btn bg-orange btn-flat" onclick="uploadcoverImage(<?=$value['party_id'];?>)" href="javascript:void(0)"><i class = "fa fa-picture-o"></i></button></td>
                                 


                                   <td><button class="btn bg-blue btn-flat" onclick="editParty(<?=$value['party_id'];?>)"><i class = "fa fa-edit"></i></button></td>
                            
                                    </tr>
                                <?php
                                
                                    }
                                    ?>





                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<!--details model-->


<div class="modal fade" id="myModaldetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #005384">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: white">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel" style="color: white">  <i class="fa fa-user"></i> &nbsp; User details</h4>


            </div>
            <div class="modal-body">
                <section class="invoice">
          
                 
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            
            
            <div class="widget-user-header bg-black" id="partycoverimage" style="background: url('../dist/img/photo1.png') center center;">
            
              <div class="widget-user-image">
                <img class="img-circle" src="<?=base_url();?>assets/images/party/none.png" id="partyimage" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username" id="partydetname"></h3>
             
              <br>
            </div>




            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a>Created on <span class="pull-right badge" id="partycreated"></span></a></li>
                <li><a>Active <span class="pull-right badge" id="partyactive"></span></a></li>                   
                <li><a>Approved <span class="pull-right badge" id="partyapproved"></span></a></li>
                <li><a>Public polls <span class="pull-right badge" id="partypublicpolls"></span></a></li>
                <li><a>Secret polls <span class="pull-right badge" id="partysecretpolls"></span></a></li>
                <li><a>Total joins<span class="pull-right badge" id="partyjoins"></span></a></li>
                <li><a id="viewmembers" href ="">View polled members<span class="pull-right badge bg-blue">view</span></a></li>                    
              
              </ul>
            </div>
          </div>
          <!-- /.widget-user -->
        
                    
                    
                </section>
            </div>
            <div class="modal-footer" style="background-color: #005384">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<form name="propicform" id="propicform" >
    <input type="text" value="" name="propicid" id="propicid" style="display:none;visiblity:hidden;z-index:99999;">
     <input type="file" name="propic" id="propic" style="display:none;visiblity:hidden;z-index:99999;">
</form>


<form name="coverpicform" id="coverpicform" >
    <input type="text" value="" name="coverpicid" id="coverpicid" style="display:none;visiblity:hidden;z-index:99999;">
     <input type="file" name="coverpic" id="coverpic" style="display:none;visiblity:hidden;z-index:99999;">
</form>



<!-- Modal -->
 
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header bg-blue-active">
<button type="button" class="close" id="subadmindismiss" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">×</span></button>
<h4 class="modal-title"><i class="fa  fa-user-plus"></i> Create party</h4>
</div>
<div class="modal-body">
<form action="javascript:void(0)" method="POST" name="partyform" id="partyform">


<div class="form-group ">
<label class="control-label" for="partyshortname"><i class="fa  fa-user"></i> Party short name</label>
<input type="text" class="form-control" name="partyshortname" id="partyshortname" placeholder="Enter party short name">
<span class="help-block"></span>
</div>

<div class="form-group ">
<label class="control-label" for="partyname"><i class="fa  fa-user"></i> Party name</label>
<input type="text" class="form-control" name="partyname" id="partyname" placeholder="Enter party name">
<span class="help-block"></span>
</div>


<div class="form-group ">
<label class="control-label" for="pname"><i class="fa  fa-user"></i> President name</label>
<input type="text" class="form-control" name="pname" id="pname" placeholder="Enter president name">
<span class="help-block"></span>
</div>


<div class="form-group ">
<label class="control-label" for="sname"><i class="fa  fa-user"></i> Secretary name</label>
<input type="text" class="form-control" name="sname" id="sname" placeholder="Enter secretary name">
<span class="help-block"></span>
</div>


<div class="form-group ">
<label class="control-label" for="email"><i class="fa  fa-user"></i> Email</label>
<input type="text" class="form-control" name="email" id="email" placeholder="Enter email">
<span class="help-block"></span>
</div>

<div class="form-group">
<label class="control-label" for="scope"><i class="fa  fa-user"></i> Scope </label>
<select class="form-control" name="scope" id="scope">
<option value="">Choose scope</option>

<option value="1">Country</option>
<option value="2">State</option>
<option value="3">District</option>
<!--<option value="4">Area</option> -->
</select>
</div>

<div class="form-group">
<label class="control-label" for="country"><i class="fa  fa-user"></i> Country </label>
<select class="form-control" name="country" id="country">
<option value="">Choose country</option>
<?php 
$res = $this->PartyModel->getCountry();
foreach($res as $val => $key){
    ?>
    <option value="<?= $res[$val]['id']?>"><?=$res[$val]['name']?></option>
    <?php
}
?>

</select>
</div>

<div class="form-group">
<label class="control-label" for="state"><i class="fa  fa-user"></i> State </label>
<select class="form-control" name="state" id="state">
<option value="">Choose state</option>

</select>
</div>


<div class="form-group">
<label class="control-label" for="district"><i class="fa  fa-user"></i> District </label>
<select class="form-control" name="district" id="district">
<option value="">Choose district</option>

</select>
</div>


<div class="modal-footer">
<button type="button" class="btn btn-default pull-left" data-dismiss="modal"><i class="fa  fa-close"></i> Cancel </button>
<button type="submit" class="btn btn-primary" id="save_button"><i class="fa  fa-floppy-o"></i> Save</button>
</div>
</form>

</div>
</div>

</div>
</div>







<!-- Edit Modal -->
 
<div class="modal fade" id="myeditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
<div class="modal-header bg-blue-active">
<button type="button" class="close" id="subadmindismiss2" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">×</span></button>
<h4 class="modal-title"><i class="fa  fa-user-plus"></i> Create party</h4>
</div>
<div class="modal-body">
<form action="javascript:void(0)" method="POST" name="editpartyform" id="editpartyform">

<div class="form-group ">
<label class="control-label" for="partyname"><i class="fa  fa-user"></i> Edit short name</label>
<input type="text" class="form-control" name="editpartyshortname" id="editpartyshortname" placeholder="Enter party name">
<span class="help-block"></span>
</div>

<div class="form-group ">
<label class="control-label" for="partyname"><i class="fa  fa-user"></i> Edit name</label>
<input type="text" class="form-control" name="editpartyname" id="editpartyname" placeholder="Enter party name">
<span class="help-block"></span>
</div>


<div class="form-group ">
<label class="control-label" for="pname"><i class="fa  fa-user"></i> President name</label>
<input type="text" class="form-control" name="editpname" id="editpname" placeholder="Enter president name">
<span class="help-block"></span>
</div>

<input type = "hidden" id="partyeditid" name = "partyeditid" value="">  
<div class="form-group ">
<label class="control-label" for="sname"><i class="fa  fa-user"></i> Secretary name</label>
<input type="text" class="form-control" name="editsname" id="editsname" placeholder="Enter secretary name">
<span class="help-block"></span>
</div>


<div class="form-group ">
<label class="control-label" for="email"><i class="fa  fa-user"></i> Email</label>
<input type="text" class="form-control" name="editemail" id="editemail" placeholder="Enter email">
<span class="help-block"></span>
</div>

<div class="form-group">
<label class="control-label" for="scope"><i class="fa  fa-user"></i> Scope </label>
<select class="form-control" name="editscope" id="editscope">
<option value="">Choose scope</option>

<option value="1">Country</option>
<option value="2">State</option>
<option value="3">District</option>
<!--<option value="4">Area</option> -->
</select>
</div>

<div class="form-group">
<label class="control-label" for="country"><i class="fa  fa-user"></i> Country </label>
<select class="form-control" name="editcountry" id="editcountry">
<option value="">Choose country</option>
<?php 
$res = $this->PartyModel->getCountry();
foreach($res as $val => $key){
    ?>
    <option value="<?= $res[$val]['id']?>"><?=$res[$val]['name']?></option>
    <?php
}
?>

</select>
</div>

<div class="form-group" id="editstatediv">
<label class="control-label" for="state"><i class="fa  fa-user"></i> State </label>
<select class="form-control" name="editstate" id="editstate">
<option value="">Choose state</option>

</select>
</div>



<div class="form-group" id="editstatestaticdiv">
<label class="control-label" for="email"><i class="fa  fa-user"></i> State</label>
<input type="text" class="form-control" readonly name="editstatestatic" id="editstatestatic">
<span class="help-block"></span>
</div>


<div class="form-group" id="editdistrictstaticdiv">
<label class="control-label" for="email"><i class="fa  fa-user"></i> District</label>
<input type="text" class="form-control" readonly name="editdistrictstatic" id="editdistrictstatic">
<span class="help-block"></span>
</div>

<div class="form-group" id="editdistrictdiv">
<label class="control-label" for="district"><i class="fa  fa-user"></i> District </label>
<select class="form-control" name="editdistrict" id="editdistrict">
<option value="">Choose district</option>

</select>
</div>


<div class="modal-footer">
<button type="button" class="btn btn-default pull-left" data-dismiss="modal"><i class="fa  fa-close"></i> Cancel </button>
<button type="submit" class="btn btn-primary" id="edit_save_button"><i class="fa  fa-floppy-o"></i> Update</button>
</div>
</form>

</div>
</div>
</div>
</div>
<!-- edit model ends here -->


<script>

$(function()
 {
   
$.validator.setDefaults({errorClass: 'help-block',highlight: function(element){ $(element).closest('.form-group').addClass('has-error'); }, unhighlight: function(element){ $(element).closest('.form-group').removeClass('has-error');  } });
$.validator.addMethod( "pattern", function( value, element, param ) { if ( this.optional( element ) ) { return true; } if ( typeof param === "string" ) { param = new RegExp( "^(?:" + param + ")$" );  }       return param.test( value );       }, "Invalid input information." );

$("#partyform").validate({ 
rules:{
    partyname:{required:true,maxlength:30}, pname:{required:true}, sname:{required:true}, email:{required:true,email:true},
    scope:{required: true},country:{required:true},state:{required:true}, district:{required:true},partyshortname:{required:true,maxlength:10}
},
messages:{
    partyshortname:{required:"Please enter  the party  short name."},
    partyname:{required:"Please enter  the party  name."},
    pname:{required:"Please choose   the president name."},
    sname:{required:"Please enter  the secretary  name."},
    email:{required:"Please enter  the email.",email:"Enter valid email"},
    scope:{required:"Please select  the scope ."},
    country:{required:"Please select  the country ."},
    state:{required:"Please select  the state."},
    district:{required:"Please select  the district."}
    
}
    
});  



$("#editpartyform").validate({ 
rules:{
    editpartyname:{required:true ,maxlength:30}, editpname:{required:true}, editsname:{required:true}, editemail:{required:true,email:true},
    editscope:{required: true},editcountry:{required:true},editstate:{required:true}, editdistrict:{required:true} , editpartyshortname:{required:true,maxlength:10}
},
messages:{
    editpartyshortname:{required:"Please enter  the party short name."},
    editpartyname:{required:"Please enter  the party name."},
    editpname:{required:"Please choose   the president name."},
    editsname:{required:"Please enter  the secretary  name."},
    editemail:{required:"Please enter  the email.",email:"Enter valid email"},
    editscope:{required:"Please select  the scope ."},
    editcountry:{required:"Please select  the country ."},
    editstate:{required:"Please select  the state."},
    editdistrict:{required:"Please select  the district."}
    
}
    
});





});
</script>

<script>
$("#new_party_button").click(function(){
    $("#myModal").modal({keyboard:!1,backdrop:"static"});
});

</script>

<script>
var editParty = function(id){
$.post(site_url+"Party/getpartydetails",{id:id},function(data){
    

$("#partyeditid").val(id);
$("#editpartyname").val(data.values.party_name);
$("#editpartyshortname").val(data.values.party_code);
$("#editpname").val(data.values.party_president);
$("#editsname").val(data.values.party_secretary);
$("#editemail").val(data.values.party_email);
$("#editscope").val(data.values.party_scope);
$("#editcountry").val(data.values.country);
$("#editstatestatic").val(data.values.party_state);
$("#editdistrictstatic").val(data.values.party_city);
$("#editdistrict").val(data.values.party_city);


});
$("#myeditModal").modal({keyboard:!1,backdrop:"static"});

};
</script>
<script>
var newentry=function(){$("#new_party_button").on("click",function(){
   
    $("#myModal").modal({keyboard:!1,backdrop:"static"})})}
</script>




<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
        })
    })
</script>


    
    
    <!-- /.content -->



<script>
  
    
var approveParty=function(partyid,active){
    


        $.post(site_url+"Party/approveParty",{id:partyid,active:active},function (data) {

            if(data.details==1){

                var msg="operation success";

            }
            else{
                 msg="operation failed";

            }
            $("#custom_messages").html(msg);
            $("#messagebox").modal({  keyboard: false,backdrop:'static'});
            $("#dismmissbtn").click(function () {

                location.reload();
            });
        });
    
};
</script>


   

<script>
 $("#partyul").addClass('active');
    $("#partyli").addClass('active');
</script>


<script>
var blockParty=function(partyid,active){
    


        $.post(site_url+"Party/blockparty",{id:partyid,active:active},function (data) {

            if(data.details==1){

                var msg="operation success";

            }
            else{
                 msg="operation failed";

            }
            $("#custom_messages").html(msg);
            $("#messagebox").modal({  keyboard: false,backdrop:'static'});
            $("#dismmissbtn").click(function () {

                location.reload();
            });
        });
    
};
</script>

<script>
$("#save_button").on("click",function(){
    if (!$("#partyform").valid()) {
            return false;
        }

        var formdata = $("#partyform").serialize();
        $.post(site_url+"Party/saveParty",formdata,function(data){

            $("#subadmindismiss").click();
            $("#custom_messages").html(data.message);
            $("#messagebox").modal({  keyboard: false,backdrop:'static'});
            $("#dismmissbtn").click(function () {

                location.reload();
            });
        });
      
});
</script>




<script>
$("#edit_save_button").on("click",function(){
    if (!$("#editpartyform").valid()) {
            return false;
        }

        var formdata = $("#editpartyform").serialize();
        $.post(site_url+"Party/editParty",formdata,function(data){

            $("#subadmindismiss2").click();
            $("#custom_messages").html(data.message);
            $("#messagebox").modal({  keyboard: false,backdrop:'static'});
            $("#dismmissbtn").click(function () {

                location.reload();
            });
        });
      
});
</script>







<script>

    var partydetails=function(id){
        
        $.post(site_url+"Party/getpartydetails",{id:id},function(data){

            var active="";
            if(data.values.party_active==1)active="Active";
             else active="Blocked";
            
            var approved="";
            if(data.values.party_isapproved==1)approved="Approved";
             else approved="Not approved";
            
            $('#partycoverimage').css({"background":"url('"+data.values.party_cover_photo+"') center center"});

            $("#partyimage").attr("src",data.values.party_photo); 
            $("#partydetname").html(data.values.party_name);
            $("#partycreated").html(data.values.party_created_date);
            $("#partyactive").html(active);
            $("#partyapproved").html(approved);
            $("#partypublicpolls").html(data.publicpolls);
            $("#partysecretpolls").html(data.secretpolls);
            $("#partyjoins").html(data.followers);
            
            $("#viewmembers").attr("href", site_url+"Party/pollpartylist/"+id)
            
            
             $('#myModaldetails').modal({  keyboard: false,backdrop:'static'});
        });
    };
   
</script>

<script>
$("#country").change(function(){



$.post(site_url+"Party/getstatebyid" , {cid:$("#country").val()} , function(data){
    console.log($("#country").val());
    var dat = '<option value ="">Choose state</option>';
if(data.error == 0){

   
    var values = data.values;
    for(var dats in values){
        var mydata = values[dats];
        dat += '<option value ="'+mydata.id+'">'+mydata.name+'</option>';
    }
   
}
$("#state").html(dat);
});
});
</script>
<script>
$(function(){

    $("#editstatediv").hide();
    $("#editdistrictdiv").hide();
    savephoto();
    savecoverphoto();
});

</script>
<script>

var uploadImage = function(id){

    $("#propicid").val(id);
    $('#propic').trigger('click');
    //alert($("#propicid").val());
}

var uploadcoverImage = function(id){

$("#coverpicid").val(id);
$('#coverpic').trigger('click');
//alert($("#propicid").val());
}


</script>

<script>
  
        
        

    var savephoto=function()
    {
        $("#propic").change(function(event)
        {
          
            var data=new FormData(document.getElementById("propicform"));

            $.ajax({url:site_url+'Party/uploadpartypic',
                type: 'POST',
                data: data,
                processData: false,
                contentType: false,
                enctype: 'multipart/form-data',

                success: function (data) {


                $("#custom_messages").html(data.message);
                $('#messagebox').modal({backdrop: 'static', keyboard: false});}
            });
            $('#messagebox').on('click', function () {
                location.reload(true);
            });
        });
    };



    var savecoverphoto=function()
    {
        $("#coverpic").change(function(event)
        {
        
            var data=new FormData(document.getElementById("coverpicform"));

            $.ajax({url:site_url+'Party/uploadcoverpic',
                type: 'POST',
                data: data,
                processData: false,
                contentType: false,
                enctype: 'multipart/form-data',

                success: function (data) {


                $("#custom_messages").html(data.message);
                $('#messagebox').modal({backdrop: 'static', keyboard: false});}
            });
            $('#messagebox').on('click', function () {
                location.reload(true);
            });
        });
    };


</script>

<script>
$("#state").change(function(){

$.post(site_url+"Party/getdistrictbyid" , {cid:$("#state").val()} , function(data){
    var dat = '<option value ="">Choose district</option>';
if(data.error == 0){

   
    var values = data.values;
    for(var dats in values){
        var mydata = values[dats];
        dat += '<option value ="'+mydata.name+'">'+mydata.name+'</option>';
    }
   
}
$("#district").html(dat);
});
});
</script>




<script>
$("#editstate").change(function(){

$.post(site_url+"Party/getdistrictbyid" , {cid:$("#editstate").val()} , function(data){
    var dat = '<option value ="">Choose district</option>';
if(data.error == 0){

   
    var values = data.values;
    for(var dats in values){
        var mydata = values[dats];
        dat += '<option value ="'+mydata.name+'">'+mydata.name+'</option>';
    }
   
}
$("#editdistrict").html(dat);
});
});
</script>

<script>
$("#editcountry").change(function(){


$("#editstatestaticdiv").hide();
$("#editdistrictstaticdiv").hide();

$("#editstatediv").show();
$("#editdistrictdiv").show();
$.post(site_url+"Party/getstatebyid" , {cid:$("#editcountry").val()} , function(data){
    
    var dat = '<option value ="">Choose state</option>';
if(data.error == 0){

   
    var values = data.values;
    for(var dats in values){
        var mydata = values[dats];
        dat += '<option value ="'+mydata.id+'">'+mydata.name+'</option>';
    }
   
}
$("#editstate").html(dat);
});
});
</script>
