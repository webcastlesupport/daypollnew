 


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h4>
            Party Claim
            <small>Party claim details </small>
        </h4>
        <ol class="breadcrumb">
            <li><a><i class="fa fa-dashboard"></i> Home</a></li>

        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">

                    <div class="box-header bg-blue">
                        <h3 class="box-title">party claim list </h3>
                       
                    </div>
                    <!-- /.box-header -->

                  
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                            <th>SL.NO</th>
                            <th>Party name</th>
                            <th>claimed by</th>
                            <th>claimed date</th>
                            <th>Attachment</th>
                            <th>View</th>
                            <th>Status</th>
                            <th>Approve</th>

                            </tr>
                            </thead>
                           
                            <?php 
                $res = $this->PartyClaimModel->getPartyClaimlist();
               
                
                foreach ($res as $key=>$value){
                  
                  if($value['claim_party_isapproved'] == 0) $status = '<span style ="background-color:orange;border-radius:2px;padding:2px;">Pending</span>';
                  elseif($value['claim_party_isapproved'] == 1) $status = '<span style ="background-color:#58ad63;border-radius:2px;padding:2px;">Approved</span>';
                  elseif($value['claim_party_isapproved'] == -1) $status = '<span style ="background-color:#e85a27;border-radius:2px;padding:2px;">Rejected</span>';
                  $GMT = new DateTimeZone("GMT");
                                $date = new DateTime($value['claim_party_date'], $GMT);
                                $date->setTimezone(new DateTimeZone('Asia/Kolkata'));
                                $value['claim_party_date'] = $date->format('Y/m/d h:i:s a');
                ?>
                <tr>
                  <td><?=$key+1;?></td>
                  <td><?=$value['party_name'];?></td>
               
                  <td><?=$value['users_name'];?></td>
                  
                  <td><?=$value['claim_party_date'];?></td>

                  <td>
                    <?php if($value['claim_party_attachment']){?><a class = "btn bg-blue btn-flat" target="_blank" href="<?=base_url();?>assets/images/party/attachments/<?=$value['claim_party_attachment']?>"><span class="label"><i class = "fa fa-download"></i></span></a>
                    <?php } else {?>
                      <a class = "btn bg-blue btn-flat"  ><span class="label"><i class = "fa fa-ban"></i></span></a>
                      <?php } ?>
                  </td>
                 
                  <td><button class = "btn bg-blue btn-flat" onclick="partydetails(<?=$value['claim_party_party_id'];?>);"><i class = "fa fa-eye"></i></button></</td>
                 
                  <td><?=$status;?></td>
                  
                  <td>
               
                  <a href="javascript:void(0)" onclick="approvepartyclaim(<?=$value['claim_party_id']?>,<?=$value['claim_party_party_id']?>,<?=$value['claim_party_user_id'];?>,1);"><span class="label" style="background-color: green ">Approve</span></a>
                  <a href="javascript:void(0)" onclick="approvepartyclaim(<?=$value['claim_party_id']?>,<?=$value['claim_party_party_id']?>,<?=$value['claim_party_user_id'];?>,-1);"><span class="label" style="background-color: #f48341 ">Reject</span></a>
                  </td>
                  
                <?php 
                }
               ?>

                    </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

      
    
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #005384">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: white">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel" style="color: white">  <i class="fa fa-user"></i> &nbsp; User details</h4>


            </div>
            <div class="modal-body">
                <section class="invoice">
                    
                   
                    
                 
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-black" id="partycoverimage" style="background: url('../dist/img/photo1.png') center center;">
            
              <div class="widget-user-image">
                <img class="img-circle" src="<?=base_url();?>assets/images/party/none.png" id="partyimage" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username" id="partydetname"></h3>
             
              <br>
            </div>


            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
              
                <li><a>Created by <span class="pull-right badge" id="partycreatedby"></span></a></li>
                <li><a>Polls <span class="pull-right badge" id="partypolls"></span></a></li>
                <li><a>Total joins<span class="pull-right badge" id="partyjoins"></span></a></li>
               
              
              </ul>
            </div>
          </div>
          <!-- /.widget-user -->
        
                    
                    
                </section>
            </div>
            <div class="modal-footer" style="background-color: #005384">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

    
    
    
    
    <!-- /.content -->
</div>


<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
        })
    })
</script>

<script>
  
    
var approveParty=function(partyid,active){
    


        $.post(site_url+"Party/approveParty",{id:partyid,active:active},function (data) {

            if(data.details==1){

                var msg="operation success";

            }
            else{
                 msg="operation failed";

            }
            $("#custom_messages").html(msg);
            $("#messagebox").modal({  keyboard: false,backdrop:'static'});
            $("#dismmissbtn").click(function () {

                location.reload();
            });
        });
    
};
</script>


   

<script>
    $("#partyul").addClass('active');
    $("#partyclaimli").addClass('active');
</script>


<script>
var blockParty=function(partyid,active){
    


        $.post(site_url+"Party/blockparty",{id:partyid,active:active},function (data) {

            if(data.details==1){

                var msg="operation success";

            }
            else{
                 msg="operation failed";

            }
            $("#custom_messages").html(msg);
            $("#messagebox").modal({  keyboard: false,backdrop:'static'});
            $("#dismmissbtn").click(function () {

                location.reload();
            });
        });
    
};
</script>

<script>

var approvepartyclaim = function(id,partyid,userid,flag){

console.log(id);

console.log(partyid);

console.log(userid);

console.log(flag);


var d = confirm('Are you sure..!');
if(d == true){

  $.post(site_url+"PartyClaim/approvepartyclaim",{id:id,flag:flag,partyid:partyid,userid:userid},function (data) {

  if(data.status==1){

    var msg="operation success";

  }
  else{
     msg="operation failed";

  }
  $("#custom_messages").html(msg);
  $("#messagebox").modal({  keyboard: false,backdrop:'static'});
  $("#dismmissbtn").click(function () {

    location.reload();
  });
  });

  }
  else{
    return false;
  }

  }

</script>



<script>

    var partydetails=function(id){
        
        
        $.post(site_url+"Party/getpartydetails",{id:id},function(data){
            
            
            
             
            $('#partycoverimage').css({"background":"url('"+data.values.party_cover_photo+"') center center"});
            $("#partyimage").attr("src",data.values.party_photo);  
            $("#partydetname").html(data.values.party_name);
            $("#partycreatedby").html(data.values.users_name);
         
          
            $("#partypolls").html(data.polls);
            $("#partyjoins").html(data.followers);
            
            
             $('#myModal').modal({  keyboard: false,backdrop:'static'});
        });
    };
   
</script>