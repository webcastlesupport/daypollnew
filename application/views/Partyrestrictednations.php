
<link rel="stylesheet" href="<?=base_url();?>assets/bower_components/select2/dist/css/select2.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          <small>view all party restricted nations</small>
            
        </h1>
        <ol class="breadcrumb">
        <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>party restricted nations</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">


    <div class="box-body">
          <div class="row">
            
            <!-- /.col -->
            <div class="col-md-12">
            <form action="javascript:void(0)" id="countrysaveform" method="POST">
              <div class="form-group">
                <label>Add nation</label>
                <select class="form-control select2" id="countrylist" multiple="multiple" data-placeholder="Select a Country"
                style="width: 100%;">
                  <?php
                  foreach($countries as $row){
                  ?>
                  <option><?=$row['name']?></option>
                  <?php
                  }
                  ?>
                </select>
              </div>
              <button  class="btn bg-green btn-flat pull-right" type ="submit">Add</button>
            </form> 
            
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        

        <div class="row">
            <div class="col-xs-12">

                <div class="box">

                    <div class="box-header bg-blue">
                        <h3 class="box-title">Restricted nations</h3>
                    </div>
                    <!-- /.box-header -->

                  
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>SL.NO</th>
                                <th>Country name</th>
                                <th>Created date</th>
                                <th>Remove</th>

                            </tr>
                            </thead>
                            <tbody>

                            <?php

                            foreach ($data as $key=>$value){

                                $value['created_date']=date('d/m/Y  h:i A' ,strtotime($value['created_date']));

                              
                                ?>

                                <tr>
                                    <td><?=$key+1?></td>
                                    <td><?=$value['country_name']?></td>
                                  
                                    <td><?=$value['created_date']?></td>
                                    <td><button class = "btn bg-red btn-flat" onclick="removenation(<?=$value['id']?>)">Remove</button></td>

                                        </button></td>

                                </tr>
                            <?php
                            }


                            ?>



                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>



<!-- Control Sidebar -->

<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<script src="<?=base_url();?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
  });
    </script>
<script>
    $("#partyul").addClass('active');
    $("#partyrestrictedli").addClass('active');
</script>

<script>
    var removenation = function(id){
    var d = confirm('Are you sure');
    if(d == true){
      $.ajax({
          url           : site_url+"Party/removerestrictedParty",
          data          : {id:id},
          type          : 'POST',
		  cache		    :  false,
	 
        success: function(data)
	      {
            $("#custom_messages").html(data.message);
            $("#messagebox").modal({  keyboard: false,backdrop:'static'});
            $("#dismmissbtn").click(function () {
            location.reload();
            });          
	      },
	    error: function(e) 
	      {
	      console.log(e.responseText);
	      }  
      });

    }
    }
</script>
<script>
$("#countrysaveform").submit(function(e){
    e.preventDefault();
    var data = $("#countrylist").val();
    $.ajax({
        url     : site_url+"Party/saveRestrictedCountry",
        type    : "POST",
        data    : {data:data},
        success : function(data){
            $("#custom_messages").html(data.message);
            $("#messagebox").modal({  keyboard: false,backdrop:'static'});
            $("#dismmissbtn").click(function () {
            location.reload();
            });  
        },
        error:function(e){

        }
    })
})
</script>

<!-- page script -->
<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
        })
    })
</script>