﻿<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 8/13/2018
 * Time: 3:58 PM
 */

class PostsApi extends CI_Controller{

    public $media_url= "https://d.daypoll.com/";
    
   

    //public $media_url= "http://iroidtechnologies.in/";
    
    function __construct()
    {
         error_reporting(0);
        parent::__construct();
        $this->load->model(array('PostsApiModel','UsersApiModel','ChallengeApiModel'));
        $this->load->library('form_validation');
    }


     // upload user posts
    
     function uploadPosts(){                                                             

        $this->output->set_content_type('application/json');

        $thumb_name     = null;
        $userid         = $this->input->post('userid');
        $linecount      = $this->input->post('linecount');
        $posttitle      = $this->input->post('title');
        $photo_or_video = $this->input->post('photo_or_video');         // photo or video  [photo -> 0 , video -> 1 , other -> -1 ]
        $posts_content  = $this->input->post('content');
        $poststype      = $this->input->post('type');   
        $posts_width    = $this->input->post('posts_width');    
        $posts_height   = $this->input->post('posts_height');         
        
        
        $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;



        $this->form_validation->set_rules("userid","userid","required");
        //$this->form_validation->set_rules("title","title","required");
        $this->form_validation->set_rules("photo_or_video","photo_or_video","required");
        //$this->form_validation->set_rules("content","content","required");

        if($linecount == NULL){
            $linecount = 0;
        }

        if($this->form_validation->run() == TRUE){


            if($photo_or_video == -1){

                $insert = array('posts_user_id'=>$userid,'photo_or_video'=>$photo_or_video,'posts_title'=>$posttitle,
                'posts_content'=>$posts_content,'posts_linecount' => $linecount,'posts_type'=>$poststype,
                'posts_created_type'=>1,'posts_country' => $country , 'posts_state' => $state , 
                'posts_city' => $district ,'posts_area' => $area);

                $result = $this->PostsApiModel->uploadPost($insert);


                if($result > 0) {

                    $this->output->set_output(json_encode(['message' => "successfully uploaded ", 'status' => "true"]));
                }
                else{
                    $this->output->set_output(json_encode(['message' => "operation failed!..", 'status' => "false"]));
                }
            }
            else if($photo_or_video == 0){

            $this->load->helper('form');

            $config['upload_path']      = "./assets/images/posts/";
            $config['allowed_types']    = '*';
            $config['file_name']        = 'posts'.mt_rand(100000,999999).date('H-i-s');
            //$config['max_height']     = '250';
            //$config['max_width']      = '250';
            //$config['max_size']       = '100kb';
            $config["overwrite"]        = false;

            $this->load->library('upload',$config);
            if(!$this->upload->do_upload("photo_video"))
            {
                $error = array('error' => $this->upload->display_errors());

                $this->output->set_output(json_encode(['message' => 'file not specified', 'status'=>'false']));
                return false;
            }
            else
            {
                
                $data   = $this->upload->data();
               
               
            // $config['image_library']='gd2';
            // $config['source_image']=$data['full_path'];
            // $config['create_thumb']=FALSE;
            // $config['maintain_ratio']=FALSE;
            // $config['quality']='60%';
            // $config['width']=500;
            // $config['height']=300;
            // $config['new_image']="./assets/images/posts/".$data['file_name'];
            // $this->load->library('image_lib',$config);
            // // $this->image_lib->clear();
            // $this->image_lib->initialize($config);
            // $this->image_lib->resize();

                $insert = array('posts_user_id'=>$userid,'photo_or_video'=>$photo_or_video,'posts_title'=>$posttitle,'posts_photo_or_video'=>$data['file_name'],
                    'posts_content'=>$posts_content,'posts_width' => $posts_width,'posts_height' => $posts_height,'posts_linecount' => $linecount,
                    'posts_type'=>$poststype,'posts_created_type'=>1 , 'posts_country' => $country , 'posts_state' => $state , 
                    'posts_city' => $district ,'posts_area' => $area);


                $result = $this->PostsApiModel->uploadPost($insert);


                if($result > 0) {

                    $this->output->set_output(json_encode(['message' => "successfully uploaded ", 'status' => "true"]));
                }
                else{
                    $this->output->set_output(json_encode(['message' => "operation failed!..", 'status' => "false"]));
                }
            }
       
            }



            // video upload


            else if($photo_or_video == 1){

                
                $this->load->helper('form');
    
                $config['upload_path']      = "./assets/images/posts/";
                $config['allowed_types']    = '*';
                $config['file_name']        = 'posts'.mt_rand(100000,999999).date('H-i-s');
                //$config['max_height']     = '250';
                //$config['max_width']      = '250';
                //$config['max_size']       = '100kb';
                $config["overwrite"]        = false;
    
                $this->load->library('upload',$config);
                if(!$this->upload->do_upload("photo_video"))
                {
                    $error = array('error' => $this->upload->display_errors());
    
                    $this->output->set_output(json_encode(['message' => 'file not specified', 'status'=>'false']));
                    return false;
                    $flag = 0;
                }
                else
                {
                    
                    
                    $data   = $this->upload->data();
                    $flag = 1;
                   
                }
                    // video thumbnail

                    if($flag = 1){
                     
                        if(isset($_FILES['posts_video_thumbnail'])){
                            $errors= array();
                            $file_name = $_FILES['posts_video_thumbnail']['name'];
                            $file_size = $_FILES['posts_video_thumbnail']['size'];
                            $file_tmp  = $_FILES['posts_video_thumbnail']['tmp_name'];
                            $file_type = $_FILES['posts_video_thumbnail']['type'];
                            $tmp       = explode('.', $file_name);
                            $file_ext  = end($tmp);
                            
                            
                            $posts_thumb = "posts_thumb".mt_rand(1000,9999).date('H-i-s').'.'.$file_ext;
                            
                            $expensions  = array("jpeg","jpg","png");
                            
                            if(in_array($file_ext,$expensions)=== false){
                               $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
                            }
                            
                            
                            
                            if(empty($errors)==true){
                               
                               move_uploaded_file($file_tmp,"assets/images/posts/thumbnails/".$posts_thumb);
                               $thumb_name = $posts_thumb;
                               
                            }else{
                                $thumb_name= NULL;
                            }
                  
                        }


                
    
                    $insert = array('posts_user_id'=>$userid,'photo_or_video'=>$photo_or_video,'posts_title'=>$posttitle,'posts_photo_or_video'=>$data['file_name'],
                        'posts_content'=>$posts_content,'posts_width' => $posts_width,'posts_height' => $posts_height,'posts_linecount' => $linecount,
                        'posts_type'=>$poststype,'posts_video_thumbnail'=>$thumb_name,'posts_created_type'=>1,
                        'posts_country' => $country , 'posts_state' => $state , 
                        'posts_city' => $district ,'posts_area' => $area);
    
    
                    $result = $this->PostsApiModel->uploadPost($insert);
    
                }
                    if($result > 0) {
    
                        $this->output->set_output(json_encode(['message' => "successfully uploaded ", 'status' => "true"]));
                    }
                    else{
                        $this->output->set_output(json_encode(['message' => "operation failed!..", 'status' => "false"]));
                    }
                
           
                }
        }
           
        else{
            $this->output->set_output(json_encode(['message'=>validation_errors(),'status'=>'false']));
        }

    }

    function info(){
        echo phpinfo();
    }

    


     // get posts api by polls
     function getPostsbystatePolls($state){                                                          


     
        $this->output->set_content_type('application/json');

        $posts_rank = $this->PostsApiModel->posts_sort_order_state();

       
        
        
        if($posts_rank != null){

            
        $k=0;
        foreach($posts_rank as $key1 => $value){
            
            $res[$k]        = $this->PostsApiModel->getPostsbypolls($posts_rank[$key1]['posts_polls_post_id']);
            
            //$res[$k]->posts_photo_or_video = base_url().'assets/images/posts/'.$res[$k]['posts_photo_or_video'];
            $k++;
        }

       
        $data=[];
       foreach($res as $keyss => $result){
             if($result){
                 foreach($result as $key11=>$value){

                    $data[]=$value;
                 }
             }
           
       } 

       foreach($data as $a1 => $v1 ){

            $data[$a1]['users_photo'] = $this->media_url."assets/images/users/".$data[$a1]['users_photo'];
       }

        
         $userid     = $this->input->post('userid');
        
        if(!empty($data)){
        foreach ($data as $key => $value){
            
            $pollcount            = $this->PostsApiModel->pollsCount($data[$key]['posts_id']);
            $commentcount         = $this->PostsApiModel->commentsCount($data[$key]['posts_id']);
            $commentsCountouter   = $this->PostsApiModel->commentsCountouter($data[$key]['posts_id']);
            $is_user_polled       = $this->PostsApiModel->isUserpolled($data[$key]['posts_id'],$userid);
            
          
            foreach($commentcount['data'] as $co => $va){
                $commentcount['data'][$co]['users_photo'] = $this->media_url."assets/images/users/".$commentcount['data'][$co]['users_photo']; 
            }
            
            
            $ispolled = ($is_user_polled['count'] > 0) ? true : false;
         
            $data[$key]['post_order_type']       = 1;  // 1 for posts by polls  
            $data[$key]['is_polled']             = $ispolled;            
            $token                               = mt_rand(100000,999999);
            $data[$key]['posts_photo_or_video']  = $this->media_url.'assets/images/posts/'.$data[$key]['posts_photo_or_video'];
            $data[$key]['poll_count']            = $pollcount['count'];
            $data[$key]['comment_count']         = $commentsCountouter;
            $data[$key]['comment']               = $commentcount['data'];
            //$res['data'][$key]['posts_uploaded_date']   = date('d-m-Y h:i A', strtotime($res['data'][$key]['posts_uploaded_date']));
            $data[$key]['sharecode']             = $this->media_url.'post-view?token='.$token.'&&postid='.base64_encode($data[$key]['posts_id']);
            $data[$key]['downloadlink']          = $data[$key]['posts_photo_or_video'];
        }
       
        return $data;
        
            //$this->output->set_output(json_encode(array('posts'=>$data,'status'=>true)));
        }
    }
        else{
            return null;
            //$this->output->set_output(json_encode(array('posts'=>'no data found on server','status'=>false)));
        }
    }


    


     // get party posts api by polls
     function getPartyPostsbypolls(){                                                          


     
        $this->output->set_content_type('application/json');

        $posts_rank = $this->PostsApiModel->party_posts_sort_order();

       

        if($posts_rank != null){

            
        $k=0;
        foreach($posts_rank as $key1 => $value){
            
            $res[$k]        = $this->PostsApiModel->getPartyPostsbypolls($posts_rank[$key1]['party_posts_polls_post_id']);
            
            //$res[$k]->posts_photo_or_video = base_url().'assets/images/posts/'.$res[$k]['posts_photo_or_video'];
            $k++;
        }

       
        $data=[];
       foreach($res as $keyss => $result){
             if($result){
                 foreach($result as $key11=>$value){

                    $data[]=$value;
                 }
             }
           
       } 

       foreach($data as $a1 => $v1 ){

            $data[$a1]['users_photo'] = $this->media_url."assets/images/users/".$data[$a1]['users_photo'];
       }

        
         
        
        if(!empty($data)){
        foreach ($data as $key => $value){
            
            $pollcount            = $this->PostsApiModel->partypollsCount($data[$key]['party_posts_id']);
            $commentcount         = $this->PostsApiModel->PartycommentsCount($data[$key]['party_posts_id']);
            $commentsCountouter   = $this->PostsApiModel->PartycommentsCountouter($data[$key]['party_posts_id']);
            $is_user_polled       = $this->PostsApiModel->PartyisUserpolled($data[$key]['party_posts_id'],$userid);
            
          
            foreach($commentcount['data'] as $co => $va){
                $commentcount['data'][$co]['users_photo'] = $this->media_url."assets/images/users/".$commentcount['data'][$co]['users_photo']; 
            }
            
            
            $ispolled = ($is_user_polled['count'] > 0) ? true : false;
         
            $data[$key]['post_order_type']       = 3;  // 3 for party posts by polls  
            $data[$key]['is_polled']             = $ispolled;            
            $token                               = mt_rand(100000,999999);
            $data[$key]['posts_photo_or_video']  = $this->media_url.'assets/images/partyposts/'.$data[$key]['party_posts_photo_video'];
            $data[$key]['poll_count']            = $pollcount['count'];
            $data[$key]['comment_count']         = $commentsCountouter;
            $data[$key]['comment']               = $commentcount['data'];
            //$res['data'][$key]['posts_uploaded_date']   = date('d-m-Y h:i A', strtotime($res['data'][$key]['posts_uploaded_date']));
            $data[$key]['sharecode']             = $this->media_url.'post-view?token='.$token.'&&postid='.base64_encode($data[$key]['party_posts_id']);
            $data[$key]['downloadlink']          = $this->media_url."assets/images/partyposts/".$data[$key]['party_posts_photo_video'];
        }

        
        return $data;
            //$this->output->set_output(json_encode(array('posts'=>$data,'status'=>true)));
        }
    }
        else{
            return null;
            //$this->output->set_output(json_encode(array('posts'=>'no data found on server','status'=>false)));
        }
    }





    
    // get posts api by polls
    function getPostsbypolls(){                                                          


     
        $this->output->set_content_type('application/json');

        $posts_rank = $this->PostsApiModel->posts_sort_order();

       
        if($posts_rank != null){

            
        $k=0;
        foreach($posts_rank as $key1 => $value){
            
            $res[$k]        = $this->PostsApiModel->getPostsbypolls($posts_rank[$key1]['posts_polls_post_id']);
            
            //$res[$k]->posts_photo_or_video = base_url().'assets/images/posts/'.$res[$k]['posts_photo_or_video'];
            $k++;
        }

       

        $data=[];
       foreach($res as $keyss => $result){
             if($result){
                 foreach($result as $key11=>$value){

                    $data[] = $value;
                 }
             }
           
       } 

      
       

       foreach($data as $a1 => $v1 ){

            $data[$a1]['users_photo'] = $this->media_url."assets/images/users/".$data[$a1]['users_photo'];
       }

        
         $userid     = $this->input->post('userid');
        
        if(!empty($data)){
        foreach ($data as $key => $value){
            
            $pollcount            = $this->PostsApiModel->pollsCount($data[$key]['posts_id']);
            $commentcount         = $this->PostsApiModel->commentsCount($data[$key]['posts_id']);
            $commentsCountouter   = $this->PostsApiModel->commentsCountouter($data[$key]['posts_id']);
            $is_user_polled       = $this->PostsApiModel->isUserpolled($data[$key]['posts_id'],$userid);
            
          
            foreach($commentcount['data'] as $co => $va){
                $commentcount['data'][$co]['users_photo'] = $this->media_url."assets/images/users/".$commentcount['data'][$co]['users_photo']; 
            }
            
            
            $ispolled = ($is_user_polled['count'] > 0) ? true : false;
         
            $data[$key]['post_order_type']       = 1;  // 1 for posts by polls  
            $data[$key]['is_polled']             = $ispolled;            
            $token                               = mt_rand(100000,999999);
            $data[$key]['posts_photo_or_video']  = $this->media_url.'assets/images/posts/'.$data[$key]['posts_photo_or_video'];
            $data[$key]['poll_count']            = $pollcount['count'];
            $data[$key]['comment_count']         = $commentsCountouter;
            $data[$key]['comment']               = $commentcount['data'];
            //$res['data'][$key]['posts_uploaded_date']   = date('d-m-Y h:i A', strtotime($res['data'][$key]['posts_uploaded_date']));
            $data[$key]['sharecode']             = $this->media_url.'post-view?token='.$token.'&&postid='.base64_encode($data[$key]['posts_id']);
            $data[$key]['downloadlink']          = $data[$key]['posts_photo_or_video'];
        }
        return $data;
            //$this->output->set_output(json_encode(array('posts'=>$data,'status'=>true)));
        }
    }
        else{
            return null;
            //$this->output->set_output(json_encode(array('posts'=>'no data found on server','status'=>false)));
        }
    }




    // get posts api
    function getPosts(){                                                          

     
        $this->output->set_content_type('application/json');

        $zone = getallheaders();
        $GMT  = new DateTimeZone("GMT");
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";


        $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;

        $page_start  = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;

        $userid      = $this->input->post('userid');
          
        
       
        $date = new DateTime(date("Y-m-d H:i:s", strtotime("now")), $GMT );
        $date->setTimezone(new DateTimeZone($zone['timezone']));
        $datetime = $date->format('Y-m-d H:i:s'); 


        $get_state_post          = $this->PostsApiModel->getStatePosts($state);



        foreach($get_state_post as $k => $v){
            
            $uploaddate = new DateTime($get_state_post[$k]['posts_uploaded_date'], $GMT );
            $uploaddate->setTimezone(new DateTimeZone($zone['timezone']));
            $get_state_post[$k]['posts_uploaded_date'] = $uploaddate->format('Y-m-d H:i:s');  
            
            $get_state_post[$k]['users_name']       = $this->PostsApiModel->getusersnamebypostid($get_state_post[$k]['posts_user_id']);     
            $get_state_post[$k]['users_photo']       = $this->media_url."assets/images/users/".$this->PostsApiModel->getusersphotobypostid($get_state_post[$k]['posts_user_id']);     
            
            
            
            $datePercent                        = strtotime($datetime) - strtotime($get_state_post[$k]['posts_uploaded_date']);
            $pollCount                          = $get_state_post[$k]['total']; 
            $get_state_post[$k]['poll_percent'] = $pollCount > 0 ? intval($datePercent / $pollCount) : 0; 
        }

        $sort_array = array();
        $res_2      = array();
       
       
        if($get_state_post){

                 
        foreach($get_state_post as $res_1 => $key_1){

            $sort_array[$key_1['poll_percent']] = $get_state_post[$res_1];

          
        }
        
        
        ksort($sort_array);
        
       
        
        foreach($sort_array as $state_post_final[]);
        
        }

       


        $get_poll_party_id       = $this->PostsApiModel->getpollpartyid($userid);

      
        $get_hide_post           = $this->PostsApiModel->gethiddenposts($userid);
        
        $get_report_post         = $this->PostsApiModel->getReportedposts($userid);

        $get_party_hide_post     = $this->PostsApiModel->getpartyhiddenposts($userid);
        
        $get_party_report_post   = $this->PostsApiModel->getpartyReportedposts($userid);
      

        //$res_by_poll_state = $this->getPostsbystatePolls();


        $res_by_polls       = $this->getPostsbypolls();
        $res_by_polls_state = $this->getPostsbystatePolls($state);
        $res_by_party_polls = $this->getPartyPostsbypolls($userid);
        
        $get_posts_by_location = $this->PostsApiModel->getPostsbyLocation($state);

     
        
       

        $state_array = [];
        foreach($get_posts_by_location as $row){
            $state_array[] = $row['posts_id'];
        }

        $state_array_sort =   [];
        foreach($res_by_polls_state as $in_row){
            if(in_array($in_row['posts_id'],$state_array)){
                $state_array_sort[]    =  $in_row;
            }
        }

        $state_array_sort = array_slice($state_array_sort,0,5);

        $hide_party_array =   [];
        foreach($get_party_hide_post as $row){
            $hide_party_array[]   = $row['hide_posts_post_id'];
        }

        $report_party_array =   [];
        foreach($get_party_report_post as $row){
            $report_party_array[]   = $row['party_posts_report_post_id'];
        }


        $party_array_hide =   [];
        foreach($res_by_party_polls as $in_row){
            if(!in_array($in_row['party_posts_id'],$hide_party_array)){
                $party_array_hide[]    =  $in_row ;
            }
        }

       

        $party_array_final =   [];
        foreach($party_array_hide as $in_row){
            if(!in_array($in_row['party_posts_id'],$report_party_array)){
                $party_array_final[]    =  $in_row ;
            }
        }

        $party_array_final = array_slice($party_array_final,0,3);

        foreach($party_array_final as $k1 => $v1){
            $party_array_final[$k1]["posts_id"]         = $party_array_final[$k1]["party_posts_id"];
            $party_array_final[$k1]["posts_user_id"]    = $party_array_final[$k1]["party_posts_posted_by"];
            $party_array_final[$k1]["posts_title"]      = $party_array_final[$k1]["party_posts_title"];
            
            
            $party_array_final[$k1]["posts_content"] = $party_array_final[$k1]["party_posts_title"];

            $party_array_final[$k1]["posts_photo_or_video"] = $party_array_final[$k1]["posts_photo_or_video"];
          
            
            $party_array_final[$k1]["posts_video_thumbnail"]  = $media_url."assets/images/partyposts/thumbnail/".$party_array_final[$k1]["posts_videoparty_posts_video_thumbnail_thumbnail"];
          
            $party_array_final[$k1]["posts_uploaded_date"]  = $party_array_final[$k1]["party_posts_uploaded_date"]; 
            $party_array_final[$k1]["posts_type"]           = 1;
            $party_array_final[$k1]["posts_active"] = $party_array_final[$k1]["party_posts_active"];
            $party_array_final[$k1]["posts_created_type"] = 1;
            
         
            $party_array_final[$k1]["post_order_type"] = 3;
           
           
        }

        // print_r($party_array_final);
        // exit();
       

        
        $hide_array =   [];
        foreach($get_hide_post as $row){
            $hide_array[]   = $row['hide_posts_post_id'];
        }

        $report_array =   [];
        foreach($get_report_post as $row){
            $report_array[]   = $row['posts_report_post_id'];
        }



       
       
        // $final_array =   [];
        // foreach($res_by_poll as $in_row){
        //     if(!in_array($in_row['posts_id'],$hide_array)){
        //         $final_array[]    =  $in_row ;
        //     }
        // }


        $getFriends  = $this->PostsApiModel->getFriends($userid);
        $array_count = count($getFriends);

        $getFriends[$array_count]['friend_id'] = $userid;

       

       if($getFriends){
        foreach($getFriends as $value => $key){

            $res_data              = $this->PostsApiModel->getPosts($getFriends[$value]['friend_id']);
            foreach($res_data['data'] as $row){
                $res['datas'][] = $row;
            }
            
            }
        }

        
        

        foreach($res['datas'] as $val => $k){
            $res['datas'][$val]['post_order_type'] = 2;  // 2 for normal posts
        }
       
      
        $i=0;
        foreach($res['datas'] as $row){
            $total_array[$i] = $row['posts_uploaded_date'];
            $i++;
        }
        arsort($total_array);
       
        foreach($total_array as $key => $value){
            $res['data'][] = $res['datas'][$key];
        }

        foreach($res['data'] as $row_1){
            $res_by_polls[] = $row_1;
        }


        foreach($res_by_polls as $a1 => $v1){
            if($res_by_polls[$a1]['post_order_type'] == 2)
            $res_by_polls[$a1]['users_photo'] = $this->media_url."assets/images/users/".$res_by_polls[$a1]['users_photo'];
        }

        $final_array =   [];
         foreach($res_by_polls as $in_row){
             if(!in_array($in_row['posts_id'],$hide_array)){
                 $final_array[]    =  $in_row ;
             }
         }
         
         $final_last_array_sort = [];
         foreach($final_array as $in_row){
            if(!in_array($in_row['posts_id'],$report_array)){
                $final_last_array_sort[]    =  $in_row ;
            }
        }


        foreach($state_array_sort as $k => $v){

            $state_array_sort[$k]['post_order_type'] = 4;
        }

        foreach($state_array_sort as $row){
            $final_last_array_sort[] = $row;
                
        }
        
        foreach($state_post_final as $t => $k){
            $state_post_final[$t]['post_order_type'] = 2;
        }
       

        $state_post_final_last            = array_slice($state_post_final,0,2);
        $global_post_final_third          = array_slice($state_post_final,2,3);
        
        
        $res['data']                      = array_slice($res['data'],0,2); 
        $last_posts_final                 = array_slice($state_post_final,3,60);  
        
        foreach($res['data'] as $row){
            $state_post_final_last[] = $row;
        }

       
        foreach($global_post_final_third as $row){
            $state_post_final_last[] = $row;
        }

        

        foreach($last_posts_final as $row){

            $state_post_final_last[] = $row;
        }




       
         
        $post_count =  count($state_post_final_last);
       
        $final_last_array            = array_slice($state_post_final_last,$page_start,10);

        $final_last_array_next       = array_slice($state_post_final_last,$page_start+10,10);
       
        
        if(!empty($final_last_array)){

        foreach ($final_last_array as $key => $value){

            if($final_last_array[$key]['post_order_type'] == 2){
            
            $pollcount            = $this->PostsApiModel->pollsCount($final_last_array[$key]['posts_id']);
            $commentcount         = $this->PostsApiModel->commentsCount($final_last_array[$key]['posts_id']);
            $commentsCountouter   = $this->PostsApiModel->commentsCountouter($final_last_array[$key]['posts_id']);
            $is_user_polled       = $this->PostsApiModel->isUserpolled($final_last_array[$key]['posts_id'],$userid);  
           
            foreach($commentcount['data'] as $co => $va){
                $commentcount['data'][$co]['users_photo'] = $this->media_url."assets/images/users/".$commentcount['data'][$co]['users_photo']; 
            }
            
           
            $ispolled = ($is_user_polled['count'] > 0) ? true : false;
         
        
            $final_last_array[$key]['is_polled']             = $ispolled;            
            $token                                           = mt_rand(100000,999999);
            $final_last_array[$key]['posts_photo_or_video']  = $this->media_url.'assets/images/posts/'.$final_last_array[$key]['posts_photo_or_video'];
            $final_last_array[$key]['posts_video_thumbnail'] = $this->media_url.'assets/images/posts/thumbnails/'.$final_last_array[$key]['posts_video_thumbnail'];
          
            $final_last_array[$key]['poll_count']            = $pollcount['count'];
            $final_last_array[$key]['comment_count']         = $commentsCountouter;
            $final_last_array[$key]['comment']               = $commentcount['data'];
            //$res['data'][$key]['posts_uploaded_date']   = date('d-m-Y h:i A', strtotime($res['data'][$key]['posts_uploaded_date']));
            

            
               
               $date = new DateTime($final_last_array[$key]['posts_uploaded_date'], $GMT );
               $date->setTimezone(new DateTimeZone($zone['timezone']));
               $final_last_array[$key]['posts_uploaded_date']   = $date->format('Y-m-d H:i:s'); 
               
               $final_last_array[$key]['sharecode']             = $this->media_url.'post-view?postid='.$final_last_array[$key]['posts_id'].'&&type=0';
               $final_last_array[$key]['downloadlink']          = $final_last_array[$key]['posts_photo_or_video'].'?postid='.$final_last_array[$key]['posts_id'].'&&type=0';
       
            }
         }

       

         

            $this->output->set_output(json_encode(array('hidden_post_id' => $get_hide_post,'report_post_id' => $get_report_post,'posts'=>$final_last_array,'friends' => $getFriends,'is_completed' => count($final_last_array_next) > 0 ? 0 :1,'posts_count' => $post_count,'status'=>true)));
        }
        else{
            $this->output->set_output(json_encode(array('posts'=>'no data found on server','status'=>false)));
        }
    }






    // get posts api
    function returngetPosts($userid,$postid,$type,$country,$state,$district,$area,$page){                                                          

     
        $this->output->set_content_type('application/json');

        $zone = getallheaders();
        $GMT  = new DateTimeZone("GMT");
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";

        $country     = (!empty($country)) ? $country : NULL;  
        $state       = (!empty($state)) ? $state : NULL;
        $district    = (!empty($district)) ? $district : NULL;  
        $area        = (!empty($area)) ? $area : NULL;

        $page_start  = (!empty($page))  ?  $page : 0;


             


        $get_poll_party_id = $this->PostsApiModel->getpollpartyid($userid);

      
        $get_hide_post           = $this->PostsApiModel->gethiddenposts($userid);
        
        $get_report_post         = $this->PostsApiModel->getReportedposts($userid);

        $get_party_hide_post     = $this->PostsApiModel->getpartyhiddenposts($userid);
        
        $get_party_report_post   = $this->PostsApiModel->getpartyReportedposts($userid);
      

        //$res_by_poll_state = $this->getPostsbystatePolls();


        $res_by_polls       = $this->getPostsbypolls();
        $res_by_polls_state = $this->getPostsbystatePolls($state);
        $res_by_party_polls = $this->getPartyPostsbypolls($userid);
        
        $get_posts_by_location = $this->PostsApiModel->getPostsbyLocation($state);

     
        
       

        $state_array = [];
        foreach($get_posts_by_location as $row){
            $state_array[] = $row['posts_id'];
        }

        $state_array_sort =   [];
        foreach($res_by_polls_state as $in_row){
            if(in_array($in_row['posts_id'],$state_array)){
                $state_array_sort[]    =  $in_row;
            }
        }

        $state_array_sort = array_slice($state_array_sort,0,5);

        $hide_party_array =   [];
        foreach($get_party_hide_post as $row){
            $hide_party_array[]   = $row['hide_posts_post_id'];
        }

        $report_party_array =   [];
        foreach($get_party_report_post as $row){
            $report_party_array[]   = $row['party_posts_report_post_id'];
        }


        $party_array_hide =   [];
        foreach($res_by_party_polls as $in_row){
            if(!in_array($in_row['party_posts_id'],$hide_party_array)){
                $party_array_hide[]    =  $in_row ;
            }
        }

        

        $party_array_final =   [];
        foreach($party_array_hide as $in_row){
            if(!in_array($in_row['party_posts_id'],$report_party_array)){
                $party_array_final[]    =  $in_row ;
            }
        }

        $party_array_final = array_slice($party_array_final,0,3);

        foreach($party_array_final as $k1 => $v1){
            $party_array_final[$k1]["posts_id"]         = $party_array_final[$k1]["party_posts_id"];
            $party_array_final[$k1]["posts_user_id"]    = $party_array_final[$k1]["party_posts_posted_by"];
            $party_array_final[$k1]["posts_title"]      = $party_array_final[$k1]["party_posts_title"];
            
            
            $party_array_final[$k1]["posts_content"] = $party_array_final[$k1]["party_posts_title"];

            $party_array_final[$k1]["posts_photo_or_video"] = $party_array_final[$k1]["posts_photo_or_video"];
          
            
            $party_array_final[$k1]["posts_video_thumbnail"]  = $media_url."assets/images/partyposts/thumbnail/".$party_array_final[$k1]["posts_videoparty_posts_video_thumbnail_thumbnail"];
          
            $party_array_final[$k1]["posts_uploaded_date"]  = $party_array_final[$k1]["party_posts_uploaded_date"]; 
            $party_array_final[$k1]["posts_type"]           = 1;
            $party_array_final[$k1]["posts_active"] = $party_array_final[$k1]["party_posts_active"];
            $party_array_final[$k1]["posts_created_type"] = 1;
            
         
            $party_array_final[$k1]["post_order_type"] = 3;
           
           
        }

        // print_r($party_array_final);
        // exit();
       


        $hide_array =   [];
        foreach($get_hide_post as $row){
            $hide_array[]   = $row['hide_posts_post_id'];
        }

        $report_array =   [];
        foreach($get_report_post as $row){
            $report_array[]   = $row['posts_report_post_id'];
        }



       
       
        // $final_array =   [];
        // foreach($res_by_poll as $in_row){
        //     if(!in_array($in_row['posts_id'],$hide_array)){
        //         $final_array[]    =  $in_row ;
        //     }
        // }

       

        

        $getFriends  = $this->PostsApiModel->getFriends($userid);
        $array_count = count($getFriends);

        $getFriends[$array_count]['friend_id'] = $userid;

       

       if($getFriends){
        foreach($getFriends as $value => $key){

            $res_data              = $this->PostsApiModel->getPosts($getFriends[$value]['friend_id']);
            foreach($res_data['data'] as $row){
                $res['datas'][] = $row;
            }
            
            }
        }

        

        foreach($res['datas'] as $val => $k){
            $res['datas'][$val]['post_order_type'] = 2;  // 2 for normal posts
        }
       
      
        $i=0;
        foreach($res['datas'] as $row){
            $total_array[$i] = $row['posts_uploaded_date'];
            $i++;
        }
        arsort($total_array);
       
        foreach($total_array as $key => $value){
            $res['data'][] = $res['datas'][$key];
        }

        foreach($res['data'] as $row_1){
            $res_by_polls[] = $row_1;
        }


        foreach($res_by_polls as $a1 => $v1){
            if($res_by_polls[$a1]['post_order_type'] == 2)
            $res_by_polls[$a1]['users_photo'] = $this->media_url."assets/images/users/".$res_by_polls[$a1]['users_photo'];
        }

        $final_array =   [];
         foreach($res_by_polls as $in_row){
             if(!in_array($in_row['posts_id'],$hide_array)){
                 $final_array[]    =  $in_row ;
             }
         }
         
         $final_last_array_sort = [];
         foreach($final_array as $in_row){
            if(!in_array($in_row['posts_id'],$report_array)){
                $final_last_array_sort[]    =  $in_row ;
            }
        }


        foreach($state_array_sort as $k => $v){

            $state_array_sort[$k]['post_order_type'] = 4;
        }

        foreach($state_array_sort as $row){
            $final_last_array_sort[] = $row;
                
        }

        
         
        $post_count =  count($final_last_array_sort);
       
        $final_last_array      = array_slice($final_last_array_sort,$page_start,10);

        $final_last_array_next = array_slice($final_last_array_sort,$page_start+10,10);
       
                
        if(!empty($final_last_array)){

        foreach ($final_last_array as $key => $value){

            $final_last_array[$key]['is_completed'] = count($final_last_array_next);
            if($final_last_array[$key]['post_order_type'] == 2){
            
            $pollcount            = $this->PostsApiModel->pollsCount($final_last_array[$key]['posts_id']);
            $commentcount         = $this->PostsApiModel->commentsCount($final_last_array[$key]['posts_id']);
            $commentsCountouter   = $this->PostsApiModel->commentsCountouter($final_last_array[$key]['posts_id']);
            $is_user_polled       = $this->PostsApiModel->isUserpolled($final_last_array[$key]['posts_id'],$userid);  
           
            foreach($commentcount['data'] as $co => $va){
                $commentcount['data'][$co]['users_photo'] = $this->media_url."assets/images/users/".$commentcount['data'][$co]['users_photo']; 
            }
            
           
            $ispolled = ($is_user_polled['count'] > 0) ? true : false;
         
        
            $final_last_array[$key]['is_polled']             = $ispolled;            
            $token                                           = mt_rand(100000,999999);
            $final_last_array[$key]['posts_photo_or_video']  = $this->media_url.'assets/images/posts/'.$final_last_array[$key]['posts_photo_or_video'];
            $final_last_array[$key]['posts_video_thumbnail'] = $this->media_url.'assets/images/posts/thumbnails/'.$final_last_array[$key]['posts_video_thumbnail'];
          
            $final_last_array[$key]['poll_count']            = $pollcount['count'];
            $final_last_array[$key]['comment_count']         = $commentsCountouter;
            $final_last_array[$key]['comment']               = $commentcount['data'];
            //$res['data'][$key]['posts_uploaded_date']   = date('d-m-Y h:i A', strtotime($res['data'][$key]['posts_uploaded_date']));
            

            
               
               $date = new DateTime($final_last_array[$key]['posts_uploaded_date'], $GMT );
               $date->setTimezone(new DateTimeZone($zone['timezone']));
               $final_last_array[$key]['posts_uploaded_date'] = $date->format('Y-m-d H:i:s'); 
               
               $final_last_array[$key]['sharecode']             = $this->media_url.'post-view?token='.$token.'&&postid='.base64_encode($final_last_array[$key]['posts_id']);
               $final_last_array[$key]['downloadlink']          = $final_last_array[$key]['posts_photo_or_video'].'?postid='.$final_last_array[$key]['posts_id'].'&&type=0';
       
            }
         }


            return $final_last_array;
            //$this->output->set_output(json_encode(array('hidden_post_id' => $get_hide_post,'report_post_id' => $get_report_post,'posts'=>$final_last_array,'friends' => $getFriends,'posts_count' => $post_count,'status'=>true)));
        }
        else{
            $this->output->set_output(json_encode(array('posts'=>'no data found on server','status'=>false)));
        }
    }




    // // get posts api
    // function getPosts(){                                                          

     
    //     $this->output->set_content_type('application/json');

    //     $zone = getallheaders();
    //     $GMT  = new DateTimeZone("GMT");
    //     if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";

    //     $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
    //     $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
    //     $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
    //     $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;

    //     $page_start  = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;

    //     $userid      = $this->input->post('userid');
             

    //     $get_hide_post   = $this->PostsApiModel->gethiddenposts($userid);
        
    //     $get_report_post = $this->PostsApiModel->getReportedposts($userid);
      

    //     //$res_by_poll_state = $this->getPostsbystatePolls();


    //     $res_by_polls = $this->getPostsbypolls();
        
    //     $hide_array =   [];
    //     foreach($get_hide_post as $row){
    //         $hide_array[]   = $row['hide_posts_post_id'];
    //     }

    //     $report_array =   [];
    //     foreach($get_report_post as $row){
    //         $report_array[]   = $row['posts_report_post_id'];
    //     }
       
    //     // $final_array =   [];
    //     // foreach($res_by_poll as $in_row){
    //     //     if(!in_array($in_row['posts_id'],$hide_array)){
    //     //         $final_array[]    =  $in_row ;
    //     //     }
    //     // }

       

        

    //     $getFriends  = $this->PostsApiModel->getFriends($userid);
    //     $array_count = count($getFriends);

    //     $getFriends[$array_count]['friend_id'] = $userid;

       

    //    if($getFriends){
    //     foreach($getFriends as $value => $key){

    //         $res_data              = $this->PostsApiModel->getPosts($getFriends[$value]['friend_id']);
    //         foreach($res_data['data'] as $row){
    //             $res['datas'][] = $row;
    //         }
            
    //         }
    //     }

        

    //     foreach($res['datas'] as $val => $k){
    //         $res['datas'][$val]['post_order_type'] = 2;  // 2 for normal posts
    //     }
       
      
    //     $i=0;
    //     foreach($res['datas'] as $row){
    //         $total_array[$i] = $row['posts_uploaded_date'];
    //         $i++;
    //     }
    //     arsort($total_array);
       
    //     foreach($total_array as $key => $value){
    //         $res['data'][] = $res['datas'][$key];
    //     }

    //     foreach($res['data'] as $row_1){
    //         $res_by_polls[] = $row_1;
    //     }


    //     foreach($res_by_polls as $a1 => $v1){
    //         if($res_by_polls[$a1]['post_order_type'] == 2)
    //         $res_by_polls[$a1]['users_photo'] = $this->media_url."assets/images/users/".$res_by_polls[$a1]['users_photo'];
    //     }

    //     $final_array =   [];
    //      foreach($res_by_polls as $in_row){
    //          if(!in_array($in_row['posts_id'],$hide_array)){
    //              $final_array[]    =  $in_row ;
    //          }
    //      }
         
    //      $final_last_array = [];
    //      foreach($final_array as $in_row){
    //         if(!in_array($in_row['posts_id'],$report_array)){
    //             $final_last_array[]    =  $in_row ;
    //         }
    //     }
        
    //     $post_count =  count($final_last_array);
       
    //     $final_last_array     = array_slice($final_last_array,$page_start,10);
       
        
    //     if(!empty($final_last_array)){

    //     foreach ($final_last_array as $key => $value){

    //         if($final_last_array[$key]['post_order_type'] == 2){
            
    //         $pollcount            = $this->PostsApiModel->pollsCount($final_last_array[$key]['posts_id']);
    //         $commentcount         = $this->PostsApiModel->commentsCount($final_last_array[$key]['posts_id']);
    //         $commentsCountouter   = $this->PostsApiModel->commentsCountouter($final_last_array[$key]['posts_id']);
    //         $is_user_polled       = $this->PostsApiModel->isUserpolled($final_last_array[$key]['posts_id'],$userid);  
           
    //         foreach($commentcount['data'] as $co => $va){
    //             $commentcount['data'][$co]['users_photo'] = $this->media_url."assets/images/users/".$commentcount['data'][$co]['users_photo']; 
    //         }
            
           
    //         $ispolled = ($is_user_polled['count'] > 0) ? true : false;
         
        
    //         $final_last_array[$key]['is_polled']             = $ispolled;            
    //         $token                                      = mt_rand(100000,999999);
    //         $final_last_array[$key]['posts_photo_or_video']  = $this->media_url.'assets/images/posts/'.$final_last_array[$key]['posts_photo_or_video'];
    //         $final_last_array[$key]['posts_video_thumbnail'] = $this->media_url.'assets/images/posts/thumbnails/'.$final_last_array[$key]['posts_video_thumbnail'];
          
    //         $final_last_array[$key]['poll_count']            = $pollcount['count'];
    //         $final_last_array[$key]['comment_count']         = $commentsCountouter;
    //         $final_last_array[$key]['comment']               = $commentcount['data'];
    //         //$res['data'][$key]['posts_uploaded_date']   = date('d-m-Y h:i A', strtotime($res['data'][$key]['posts_uploaded_date']));
            

            
               
    //            $date = new DateTime($final_last_array[$key]['posts_uploaded_date'], $GMT );
    //            $date->setTimezone(new DateTimeZone($zone['timezone']));
    //            $final_last_array[$key]['posts_uploaded_date'] = $date->format('Y-m-d H:i:s'); 
               
    //            $final_last_array[$key]['sharecode']             = $this->media_url.'post-view?token='.$token.'&&postid='.base64_encode($final_last_array[$key]['posts_id']);
    //            $final_last_array[$key]['downloadlink']          = $final_last_array[$key]['posts_photo_or_video'].'?postid='.$final_last_array[$key]['posts_id'].'&&type=0';
       
    //         }
    //      }


         


        

    //         $this->output->set_output(json_encode(array('hidden_post_id' => $get_hide_post,'report_post_id' => $get_report_post,'posts'=>$final_last_array,'friends' => $getFriends,'posts_count' => $post_count,'status'=>true)));
    //     }
    //     else{
    //         $this->output->set_output(json_encode(array('posts'=>'no data found on server','status'=>false)));
    //     }
    // }





    // // get posts api
    // function getPosts(){                                                          

     
    //     $this->output->set_content_type('application/json');

    //     $zone = getallheaders();
    //     $GMT  = new DateTimeZone("GMT");
    //     if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";


    //     $date = new DateTime(date("Y-m-d H:i:s", strtotime("now")), $GMT );
    //     $date->setTimezone(new DateTimeZone($zone['timezone']));
    //     $datetime = $date->format('Y-m-d H:i:s'); 



    //     $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
    //     $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
    //     $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
    //     $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;

    //     $page_start  = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;

    //     $userid      = $this->input->post('userid');
             


    //     /************************** get posts sort code  ************************** */

    //     //  trending posts state 



    //     $get_trending_posts_state = $this->PostsApiModel->sortPosts(null,$state,null,null);

    //     foreach ($get_trending_posts_state as $key => $value){

            
    //         $pollcount            = $this->PostsApiModel->pollsCount($get_trending_posts_state[$key]['posts_id']);
    //         $commentcount         = $this->PostsApiModel->commentsCount($get_trending_posts_state[$key]['posts_id']);
    //         $commentsCountouter   = $this->PostsApiModel->commentsCountouter($get_trending_posts_state[$key]['posts_id']);
    //         $is_user_polled       = $this->PostsApiModel->isUserpolled($get_trending_posts_state[$key]['posts_id'],$userid);  
           
    //         foreach($commentcount['data'] as $co => $va){
    //             $commentcount['data'][$co]['users_photo'] = $this->media_url."assets/images/users/".$commentcount['data'][$co]['users_photo']; 
    //         }
            
           
    //         $ispolled = ($is_user_polled['count'] > 0) ? true : false;



    //         $uploaddate  =  new DateTime($get_trending_posts_state[$key]['posts_uploaded_date'], $GMT );
            
    //         $uploaddate->setTimezone(new DateTimeZone($zone['timezone']));
    //         $get_trending_posts_state[$key]['posts_uploaded_date']   = $uploaddate->format('Y-m-d H:i:s'); 
         
    //         $get_trending_posts_state[$key]['post_order_type']      = 1;
    //         $datePercent                                             = strtotime($datetime) - strtotime($get_trending_posts_state[$key]['posts_uploaded_date']);
    //         $pollCount                                               = $this->PostsApiModel->getPostspollCount($get_trending_posts_state[$key]['posts_id']); 
    //         $get_trending_posts_state[$key]['pollPercent']           = $pollCount > 0 ? intval($datePercent / $pollCount) : 0; 



    //         $get_trending_posts_state[$key]['users_photo']           = $this->media_url."assets/images/users/".$get_trending_posts_state[$key]['users_photo'];   
           
    //         $get_trending_posts_state[$key]['is_polled']             = $ispolled;            
    //         $token                                                   = mt_rand(100000,999999);
    //         $get_trending_posts_state[$key]['posts_photo_or_video']  = $this->media_url.'assets/images/posts/'.$get_trending_posts_state[$key]['posts_photo_or_video'];
    //         $get_trending_posts_state[$key]['posts_video_thumbnail'] = $this->media_url.'assets/images/posts/thumbnails/'.$get_trending_posts_state[$key]['posts_video_thumbnail'];
          
    //         $get_trending_posts_state[$key]['poll_count']            = $pollcount['count'];
    //         $get_trending_posts_state[$key]['comment_count']         = $commentsCountouter;
    //         $get_trending_posts_state[$key]['comment']               = $commentcount['data'];
    //         //$res['data'][$key]['posts_uploaded_date']   = date('d-m-Y h:i A', strtotime($res['data'][$key]['posts_uploaded_date']));
            

            
               
    //            $date = new DateTime($get_trending_posts_state[$key]['posts_uploaded_date'], $GMT );
    //            $date->setTimezone(new DateTimeZone($zone['timezone']));
    //            $get_trending_posts_state[$key]['posts_uploaded_date'] = $date->format('Y-m-d H:i:s'); 
               
    //            $get_trending_posts_state[$key]['sharecode']             = $this->media_url.'post-view?token='.$token.'&&postid='.base64_encode($get_trending_posts_state[$key]['posts_id']);
    //            $get_trending_posts_state[$key]['downloadlink']          = $get_trending_posts_state[$key]['posts_photo_or_video'].'?postid='.$get_trending_posts_state[$key]['posts_id'].'&&type=0';
       
            
    //      }


    //      $trending_posts_state_sort_array   = array();
    //      $trending_posts_state_final_array  = array();
        
    //      if($get_trending_posts_state){
      
            
    //      foreach($get_trending_posts_state as $res_1 => $key_1){
             
            
    //          if($key_1['pollPercent'] > 0){
    //          $trending_posts_state_sort_array[$key_1['pollPercent']] = $get_trending_posts_state[$res_1];
    //          }
    //      }
         
         
    //      ksort($trending_posts_state_sort_array);
       
    //      foreach($trending_posts_state_sort_array as $trending_posts_state_final_array[]);
         
    //      }

      

    //      // final array = $trending_posts_state_final_array



    //      //state trending posts  ends here


    //      // friend post
       
    //      $getFriends  = $this->PostsApiModel->getFriends($userid);

    //      $getFriends[$array_count]['friend_id'] = $userid;

       

    //      if($getFriends){
    //       foreach($getFriends as $value => $key){
  
    //           $res_data              = $this->PostsApiModel->getPosts($getFriends[$value]['friend_id']);
    //           foreach($res_data['data'] as $row){
    //               $friend_posts[] = $row;
    //           }
              
    //           }
    //       }

         
    //      foreach ($friend_posts as $key => $value){

            
    //         $pollcount            = $this->PostsApiModel->pollsCount($friend_posts[$key]['posts_id']);
    //         $commentcount         = $this->PostsApiModel->commentsCount($friend_posts[$key]['posts_id']);
    //         $commentsCountouter   = $this->PostsApiModel->commentsCountouter($friend_posts[$key]['posts_id']);
    //         $is_user_polled       = $this->PostsApiModel->isUserpolled($friend_posts[$key]['posts_id'],$userid);  
           
    //         foreach($commentcount['data'] as $co => $va){
    //             $commentcount['data'][$co]['users_photo'] = $this->media_url."assets/images/users/".$commentcount['data'][$co]['users_photo']; 
    //         }
            
           
    //         $ispolled = ($is_user_polled['count'] > 0) ? true : false;
    //         $friend_posts[$key]['post_order_type']       = 2; 
    //         $friend_posts[$key]['users_photo']           = $this->media_url."assets/images/users/".$friend_posts[$key]['users_photo'];   
    //         $friend_posts[$key]['is_polled']             = $ispolled;            
    //         $token                                       = mt_rand(100000,999999);
    //         $friend_posts[$key]['posts_photo_or_video']  = $this->media_url.'assets/images/posts/'.$friend_posts[$key]['posts_photo_or_video'];
    //         $friend_posts[$key]['posts_video_thumbnail'] = $this->media_url.'assets/images/posts/thumbnails/'.$friend_posts[$key]['posts_video_thumbnail'];
          
    //         $friend_posts[$key]['poll_count']            = $pollcount['count'];
    //         $friend_posts[$key]['comment_count']         = $commentsCountouter;
    //         $friend_posts[$key]['comment']               = $commentcount['data'];
    //         //$res['data'][$key]['posts_uploaded_date']   = date('d-m-Y h:i A', strtotime($res['data'][$key]['posts_uploaded_date']));
            

            
               
    //            $date = new DateTime($friend_posts[$key]['posts_uploaded_date'], $GMT );
    //            $date->setTimezone(new DateTimeZone($zone['timezone']));
    //            $friend_posts[$key]['posts_uploaded_date'] = $date->format('Y-m-d H:i:s'); 
               
    //            $friend_posts[$key]['sharecode']             = $this->media_url.'post-view?token='.$token.'&&postid='.base64_encode($friend_posts[$key]['posts_id']);
    //            $friend_posts[$key]['downloadlink']          = $friend_posts[$key]['posts_photo_or_video'].'?postid='.$friend_posts[$key]['posts_id'].'&&type=0';
       
            
    //      }

    //      $total_array = [];
    //      $i=0;
    //     foreach($friend_posts as $row){
    //         $total_array[$i] = $row['posts_uploaded_date'];
    //         $i++;
    //     }
    //     arsort($total_array);
       
    //     foreach($total_array as $key => $value){
    //         $friend_posts_final[] = $friend_posts[$key];
    //     }


        
       
    //     // final array = $friend_posts_final



    //      //friend posts ends here



    //      // global trending posts



    //      $get_trending_posts = $this->PostsApiModel->sortPosts(null,null,null,null);

    //     foreach ($get_trending_posts as $key => $value){

            
    //         $pollcount            = $this->PostsApiModel->pollsCount($get_trending_posts[$key]['posts_id']);
    //         $commentcount         = $this->PostsApiModel->commentsCount($get_trending_posts[$key]['posts_id']);
    //         $commentsCountouter   = $this->PostsApiModel->commentsCountouter($get_trending_posts[$key]['posts_id']);
    //         $is_user_polled       = $this->PostsApiModel->isUserpolled($get_trending_posts[$key]['posts_id'],$userid);  
           
    //         foreach($commentcount['data'] as $co => $va){
    //             $commentcount['data'][$co]['users_photo'] = $this->media_url."assets/images/users/".$commentcount['data'][$co]['users_photo']; 
    //         }
    //         $get_trending_posts[$key]['post_order_type']                = 3; 
    //         $get_trending_posts[$key]['users_photo']                    = $this->media_url."assets/images/users/".$get_trending_posts[$key]['users_photo'];   
           
    //         $ispolled = ($is_user_polled['count'] > 0) ? true : false;
         
    //         $uploaddate  =  new DateTime($get_trending_posts[$key]['posts_uploaded_date'], $GMT );
            
    //         $uploaddate->setTimezone(new DateTimeZone($zone['timezone']));
    //         $get_trending_posts[$key]['posts_uploaded_date']   = $uploaddate->format('Y-m-d H:i:s'); 
         

    //         $datePercent                                             = strtotime($datetime) - strtotime($get_trending_posts[$key]['posts_uploaded_date']);
    //         $pollCount                                               = $this->PostsApiModel->getPostspollCount($get_trending_posts[$key]['posts_id']); 
    //         $get_trending_posts[$key]['pollPercent']                 = $pollCount > 0 ? intval($datePercent / $pollCount) : 0; 





        
    //         $get_trending_posts[$key]['is_polled']             = $ispolled;            
    //         $token                                      = mt_rand(100000,999999);
    //         $get_trending_posts[$key]['posts_photo_or_video']  = $this->media_url.'assets/images/posts/'.$get_trending_posts[$key]['posts_photo_or_video'];
    //         $get_trending_posts[$key]['posts_video_thumbnail'] = $this->media_url.'assets/images/posts/thumbnails/'.$get_trending_posts[$key]['posts_video_thumbnail'];
          
    //         $get_trending_posts[$key]['poll_count']            = $pollcount['count'];
    //         $get_trending_posts[$key]['comment_count']         = $commentsCountouter;
    //         $get_trending_posts[$key]['comment']               = $commentcount['data'];
    //         //$res['data'][$key]['posts_uploaded_date']   = date('d-m-Y h:i A', strtotime($res['data'][$key]['posts_uploaded_date']));
            

            
               
    //            $date = new DateTime($get_trending_posts[$key]['posts_uploaded_date'], $GMT );
    //            $date->setTimezone(new DateTimeZone($zone['timezone']));
    //            $get_trending_posts[$key]['posts_uploaded_date'] = $date->format('Y-m-d H:i:s'); 
               
    //            $get_trending_posts[$key]['sharecode']             = $this->media_url.'post-view?token='.$token.'&&postid='.base64_encode($get_trending_posts[$key]['posts_id']);
    //            $get_trending_posts[$key]['downloadlink']          = $get_trending_posts[$key]['posts_photo_or_video'].'?postid='.$get_trending_posts[$key]['posts_id'].'&&type=0';
       
            
    //      }

    //      $trending_posts_sort_array   = array();
    //      $trending_posts_final_array  = array();
        
    //      if($get_trending_posts){
      
            
    //      foreach($get_trending_posts as $res_1 => $key_1){
             
            
    //          if($key_1['pollPercent'] > 0){
    //          $trending_posts_sort_array[$key_1['pollPercent']] = $get_trending_posts[$res_1];
    //          }
    //      }
         
         
    //      ksort($trending_posts_sort_array);
       
    //      foreach($trending_posts_sort_array as $trending_posts_final_array[]);
         
    //      }

        

    //      //final array => $trending_posts_sort_array
         
         
  
    //      //global trending posts ends here


    //      //national trending posts


         


    //      $get_national_trending_posts = $this->PostsApiModel->sortPosts($country,null,null,null);

    //     foreach ($get_national_trending_posts as $key => $value){

            
    //         $pollcount            = $this->PostsApiModel->pollsCount($get_national_trending_posts[$key]['posts_id']);
    //         $commentcount         = $this->PostsApiModel->commentsCount($get_national_trending_posts[$key]['posts_id']);
    //         $commentsCountouter   = $this->PostsApiModel->commentsCountouter($get_national_trending_posts[$key]['posts_id']);
    //         $is_user_polled       = $this->PostsApiModel->isUserpolled($get_national_trending_posts[$key]['posts_id'],$userid);  
           
    //         foreach($commentcount['data'] as $co => $va){
    //             $commentcount['data'][$co]['users_photo'] = $this->media_url."assets/images/users/".$commentcount['data'][$co]['users_photo']; 
    //         }
            
    //         $get_national_trending_posts[$key]['users_photo']           = $this->media_url."assets/images/users/".$get_national_trending_posts[$key]['users_photo'];   
           
    //         $ispolled = ($is_user_polled['count'] > 0) ? true : false;
         
    //         $uploaddate  =  new DateTime($get_national_trending_posts[$key]['posts_uploaded_date'], $GMT );
            
    //         $uploaddate->setTimezone(new DateTimeZone($zone['timezone']));
    //         $get_national_trending_posts[$key]['posts_uploaded_date']   = $uploaddate->format('Y-m-d H:i:s'); 
         

    //         $datePercent                                             = strtotime($datetime) - strtotime($get_national_trending_posts[$key]['posts_uploaded_date']);
    //         $pollCount                                               = $this->PostsApiModel->getPostspollCount($get_national_trending_posts[$key]['posts_id']); 
    //         $get_national_trending_posts[$key]['pollPercent']                 = $pollCount > 0 ? intval($datePercent / $pollCount) : 0; 





    //         $get_national_trending_posts[$key]['post_order_type']       = 4; 
    //         $get_national_trending_posts[$key]['is_polled']             = $ispolled;            
    //         $token                                      = mt_rand(100000,999999);
    //         $get_national_trending_posts[$key]['posts_photo_or_video']  = $this->media_url.'assets/images/posts/'.$get_national_trending_posts[$key]['posts_photo_or_video'];
    //         $get_national_trending_posts[$key]['posts_video_thumbnail'] = $this->media_url.'assets/images/posts/thumbnails/'.$get_national_trending_posts[$key]['posts_video_thumbnail'];
          
    //         $get_national_trending_posts[$key]['poll_count']            = $pollcount['count'];
    //         $get_national_trending_posts[$key]['comment_count']         = $commentsCountouter;
    //         $get_national_trending_posts[$key]['comment']               = $commentcount['data'];
    //         //$res['data'][$key]['posts_uploaded_date']   = date('d-m-Y h:i A', strtotime($res['data'][$key]['posts_uploaded_date']));
            

            
               
    //            $date = new DateTime($get_national_trending_posts[$key]['posts_uploaded_date'], $GMT );
    //            $date->setTimezone(new DateTimeZone($zone['timezone']));
    //            $get_national_trending_posts[$key]['posts_uploaded_date'] = $date->format('Y-m-d H:i:s'); 
               
    //            $get_national_trending_posts[$key]['sharecode']             = $this->media_url.'post-view?token='.$token.'&&postid='.base64_encode($get_national_trending_posts[$key]['posts_id']);
    //            $get_national_trending_posts[$key]['downloadlink']          = $get_national_trending_posts[$key]['posts_photo_or_video'].'?postid='.$get_national_trending_posts[$key]['posts_id'].'&&type=0';
       
            
    //      }

    //      $trending_national_posts_sort_array   = array();
    //      $trending_national_posts_final_array  = array();
        
    //      if($get_national_trending_posts){
      
            
    //      foreach($get_national_trending_posts as $res_1 => $key_1){
             
            
    //          if($key_1['pollPercent'] > 0){
    //          $trending_national_posts_sort_array[$key_1['pollPercent']] = $get_national_trending_posts[$res_1];
    //          }
    //      }
         
         
    //      ksort($trending_national_posts_sort_array);
       
    //      foreach($trending_national_posts_sort_array as $trending_national_posts_final_array[]);
         
    //      }

    //      //national trending posts ends here
    //      $trending_posts_state_final_array;     // 2
    //      $friend_posts_final;                   // 2
         
    //      $trending_posts_final_array;           // 1
    //      $trending_national_posts_final_array;  // 1 

    //      $first_array_size      =   sizeof($trending_posts_state_final_array);
    //      $second_array_size     =   sizeof($friend_posts_final);
    //      $third_array_size      =   sizeof($trending_posts_final_array);
    //      $fourth_array_size     =   sizeof($trending_national_posts_final_array);
    //      $total_size            =   $first_array_size+$second_array_size+$third_array_size+$fourth_array_size;



    //      $result_array  =   [];
    //      $per_page      =   10;
    //      $page_number   =   0;
    //      $last_count    =   ($page_number*10)+10;

    //      $first_count   =   (($last_count/10)*4);
    //      $second_count  =   (($last_count/10)*4);
    //      $third_count   =   $page_number+1;
    //      $fourth_count  =   $page_number+1;

    //      $current_count =   $page_number*10;

    //      for($i;0;$i<=;$i++){

    //      }

         
    //     //  for($current_count<=)

    //         // for($i=0;$i<=2;$i++){
                
    //         //     for($i =  ){

    //         //     }
    //         //     if($second_array_size > 0){

    //         //     }

    //         //     if($i != 2){
    //         //         $result_array[] =    
    //         //     }
    //         //     if($i != 2){

    //         //     }
    //         // }

    //      exit();

    //     /************************** get posts sort code ends here *********************** */

    // //     $get_poll_party_id       = $this->PostsApiModel->getpollpartyid($userid);

    // //     $get_hide_post           = $this->PostsApiModel->gethiddenposts($userid);
        
    // //     $get_report_post         = $this->PostsApiModel->getReportedposts($userid);

    // //     $get_party_hide_post     = $this->PostsApiModel->getpartyhiddenposts($userid);
        
    // //     $get_party_report_post   = $this->PostsApiModel->getpartyReportedposts($userid);
      

    // //     //$res_by_poll_state = $this->getPostsbystatePolls();


    // //     $res_by_polls       = $this->getPostsbypolls();
    // //     $res_by_polls_state = $this->getPostsbystatePolls($state);
    // //     $res_by_party_polls = $this->getPartyPostsbypolls($userid);
        
    // //     $get_posts_by_location = $this->PostsApiModel->getPostsbyLocation($state);

     
        
       

    // //     $state_array = [];
    // //     foreach($get_posts_by_location as $row){
    // //         $state_array[] = $row['posts_id'];
    // //     }

    // //     $state_array_sort =   [];
    // //     foreach($res_by_polls_state as $in_row){
    // //         if(in_array($in_row['posts_id'],$state_array)){
    // //             $state_array_sort[]    =  $in_row;
    // //         }
    // //     }

    // //     $state_array_sort = array_slice($state_array_sort,0,5);

    // //     $hide_party_array =   [];
    // //     foreach($get_party_hide_post as $row){
    // //         $hide_party_array[]   = $row['hide_posts_post_id'];
    // //     }

    // //     $report_party_array =   [];
    // //     foreach($get_party_report_post as $row){
    // //         $report_party_array[]   = $row['party_posts_report_post_id'];
    // //     }


    // //     $party_array_hide =   [];
    // //     foreach($res_by_party_polls as $in_row){
    // //         if(!in_array($in_row['party_posts_id'],$hide_party_array)){
    // //             $party_array_hide[]    =  $in_row ;
    // //         }
    // //     }

        

    // //     $party_array_final =   [];
    // //     foreach($party_array_hide as $in_row){
    // //         if(!in_array($in_row['party_posts_id'],$report_party_array)){
    // //             $party_array_final[]    =  $in_row ;
    // //         }
    // //     }

    // //     $party_array_final = array_slice($party_array_final,0,3);

    // //     foreach($party_array_final as $k1 => $v1){
    // //         $party_array_final[$k1]["posts_id"]         = $party_array_final[$k1]["party_posts_id"];
    // //         $party_array_final[$k1]["posts_user_id"]    = $party_array_final[$k1]["party_posts_posted_by"];
    // //         $party_array_final[$k1]["posts_title"]      = $party_array_final[$k1]["party_posts_title"];
            
            
    // //         $party_array_final[$k1]["posts_content"] = $party_array_final[$k1]["party_posts_title"];

    // //         $party_array_final[$k1]["posts_photo_or_video"] = $party_array_final[$k1]["posts_photo_or_video"];
          
            
    // //         $party_array_final[$k1]["posts_video_thumbnail"]  = $media_url."assets/images/partyposts/thumbnail/".$party_array_final[$k1]["posts_videoparty_posts_video_thumbnail_thumbnail"];
          
    // //         $party_array_final[$k1]["posts_uploaded_date"]  = $party_array_final[$k1]["party_posts_uploaded_date"]; 
    // //         $party_array_final[$k1]["posts_type"]           = 1;
    // //         $party_array_final[$k1]["posts_active"] = $party_array_final[$k1]["party_posts_active"];
    // //         $party_array_final[$k1]["posts_created_type"] = 1;
            
         
    // //         $party_array_final[$k1]["post_order_type"] = 3;
           
           
    // //     }

    // //     // print_r($party_array_final);
    // //     // exit();
       


    // //     $hide_array =   [];
    // //     foreach($get_hide_post as $row){
    // //         $hide_array[]   = $row['hide_posts_post_id'];
    // //     }

    // //     $report_array =   [];
    // //     foreach($get_report_post as $row){
    // //         $report_array[]   = $row['posts_report_post_id'];
    // //     }



       
       
    // //     // $final_array =   [];
    // //     // foreach($res_by_poll as $in_row){
    // //     //     if(!in_array($in_row['posts_id'],$hide_array)){
    // //     //         $final_array[]    =  $in_row ;
    // //     //     }
    // //     // }

       

        

    // //     $getFriends  = $this->PostsApiModel->getFriends($userid);
    // //     $array_count = count($getFriends);

    // //     $getFriends[$array_count]['friend_id'] = $userid;

       

    // //    if($getFriends){
    // //     foreach($getFriends as $value => $key){

    // //         $res_data              = $this->PostsApiModel->getPosts($getFriends[$value]['friend_id']);
    // //         foreach($res_data['data'] as $row){
    // //             $res['datas'][] = $row;
    // //         }
            
    // //         }
    // //     }

        

    // //     foreach($res['datas'] as $val => $k){
    // //         $res['datas'][$val]['post_order_type'] = 2;  // 2 for normal posts
    // //     }
       
      
    // //     $i=0;
    // //     foreach($res['datas'] as $row){
    // //         $total_array[$i] = $row['posts_uploaded_date'];
    // //         $i++;
    // //     }
    // //     arsort($total_array);
       
    // //     foreach($total_array as $key => $value){
    // //         $res['data'][] = $res['datas'][$key];
    // //     }

    // //     foreach($res['data'] as $row_1){
    // //         $res_by_polls[] = $row_1;
    // //     }


    // //     foreach($res_by_polls as $a1 => $v1){
    // //         if($res_by_polls[$a1]['post_order_type'] == 2)
    // //         $res_by_polls[$a1]['users_photo'] = $this->media_url."assets/images/users/".$res_by_polls[$a1]['users_photo'];
    // //     }

    // //     $final_array =   [];
    // //      foreach($res_by_polls as $in_row){
    // //          if(!in_array($in_row['posts_id'],$hide_array)){
    // //              $final_array[]    =  $in_row ;
    // //          }
    // //      }
         
    // //      $final_last_array = [];
    // //      foreach($final_array as $in_row){
    // //         if(!in_array($in_row['posts_id'],$report_array)){
    // //             $final_last_array[]    =  $in_row ;
    // //         }
    // //     }


    // //     foreach($state_array_sort as $k => $v){

    // //         $state_array_sort[$k]['post_order_type'] = 4;
    // //     }

    // //     foreach($state_array_sort as $row){
    // //         $final_last_array[] = $row;
                
    // //     }

     
    // //     $post_count =  count($final_last_array);
       
    // //     $final_last_array     = array_slice($final_last_array,$page_start,10);
       
        
    // //     if(!empty($final_last_array)){

    // //     foreach ($final_last_array as $key => $value){

    // //         if($final_last_array[$key]['post_order_type'] == 2){
            
    // //         $pollcount            = $this->PostsApiModel->pollsCount($final_last_array[$key]['posts_id']);
    // //         $commentcount         = $this->PostsApiModel->commentsCount($final_last_array[$key]['posts_id']);
    // //         $commentsCountouter   = $this->PostsApiModel->commentsCountouter($final_last_array[$key]['posts_id']);
    // //         $is_user_polled       = $this->PostsApiModel->isUserpolled($final_last_array[$key]['posts_id'],$userid);  
           
    // //         foreach($commentcount['data'] as $co => $va){
    // //             $commentcount['data'][$co]['users_photo'] = $this->media_url."assets/images/users/".$commentcount['data'][$co]['users_photo']; 
    // //         }
            
           
    // //         $ispolled = ($is_user_polled['count'] > 0) ? true : false;
         
        
    // //         $final_last_array[$key]['is_polled']             = $ispolled;            
    // //         $token                                      = mt_rand(100000,999999);
    // //         $final_last_array[$key]['posts_photo_or_video']  = $this->media_url.'assets/images/posts/'.$final_last_array[$key]['posts_photo_or_video'];
    // //         $final_last_array[$key]['posts_video_thumbnail'] = $this->media_url.'assets/images/posts/thumbnails/'.$final_last_array[$key]['posts_video_thumbnail'];
          
    // //         $final_last_array[$key]['poll_count']            = $pollcount['count'];
    // //         $final_last_array[$key]['comment_count']         = $commentsCountouter;
    // //         $final_last_array[$key]['comment']               = $commentcount['data'];
    // //         //$res['data'][$key]['posts_uploaded_date']   = date('d-m-Y h:i A', strtotime($res['data'][$key]['posts_uploaded_date']));
            

            
               
    // //            $date = new DateTime($final_last_array[$key]['posts_uploaded_date'], $GMT );
    // //            $date->setTimezone(new DateTimeZone($zone['timezone']));
    // //            $final_last_array[$key]['posts_uploaded_date'] = $date->format('Y-m-d H:i:s'); 
               
    // //            $final_last_array[$key]['sharecode']             = $this->media_url.'post-view?token='.$token.'&&postid='.base64_encode($final_last_array[$key]['posts_id']);
    // //            $final_last_array[$key]['downloadlink']          = $final_last_array[$key]['posts_photo_or_video'].'?postid='.$final_last_array[$key]['posts_id'].'&&type=0';
       
    // //         }
    // //      }



    // if($trending_posts_final_array){
        

    //         $this->output->set_output(json_encode(array('hidden_post_id' => $get_hide_post,'report_post_id' => $get_report_post,'posts'=>$final_last_array,'friends' => $getFriends,'posts_count' => $post_count,'status'=>true)));
    // }
    //     else{
    //         $this->output->set_output(json_encode(array('posts'=>'no data found on server','status'=>false)));
    //     }
    // }

    //poll posts
    function pollPosts(){

        $this->output->set_content_type('application/json');
        
        $post_user_id    = $this->input->post('postuserid');
        $posts_id        = $this->input->post('postid');
        $user_id         = $this->input->post('userid');
        
        if(!empty($posts_id && $user_id)){
           
        $res        = $this->PostsApiModel->pollPosts($user_id,$posts_id,$post_user_id);
        
        if($res['res'] == 1){
       
            $this->output->set_output(json_encode(array('is_polled'=>true,'message'=>"successfully polled",'status'=>true)));
        }
        
        else if($res['res'] == -1){

            $this->output->set_output(json_encode(array('is_polled'=>false,'message'=>'successfully unpolled','status'=>true)));
        
        }
        }
        else{
            $this->output->set_output(json_encode(array('message'=>"parameter missing",'status'=>false)));
        }
        
    }

    // share uploaded posts
    function sharePost(){

        $this->output->set_content_type('application/json');
        
        $posts_id   = $this->input->post('posts_id');
        $user_id    = $this->input->post('posts_user_id');
        $getPosts   = $this->PostsApiModel->getPostsbyid($posts_id);
        
        
        $posts_details = $getPosts[0];
        $insert        = array('posts_user_id'=>$user_id,'posts_title'=>$posts_details['posts_title'],'posts_photo_or_video'=>$posts_details['posts_photo_or_video'],
                        'posts_content'=>$posts_details['posts_content'],'posts_type'=>$posts_details['posts_type'],'posts_created_type'=>2);

        if(!empty($posts_details['posts_id'])) {
              

        $res           = $this->PostsApiModel->sharePosts($insert);
        
        if($res > 0){
       
            $this->output->set_output(json_encode(array('message'=>"succesfully shared",'status'=>true)));
        }
        
        else{
            $this->output->set_output(json_encode(array('message'=>'failed to share','status'=>false)));
        
            }
        }
        else{
            $this->output->set_output(json_encode(array('message'=>'no data found on this id','status'=>false)));
        }
    

    }

    // user profile posts
    function profilePosts(){

        $this->output->set_content_type('application/json');
        
        $zone = getallheaders();
        $GMT  = new DateTimeZone("GMT");
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";


        $dt = new DateTime();
                
        $current_date = $dt->format('d-m-Y h:i A');
       
      
        $date = new DateTime($current_date, $GMT );
        $date->setTimezone(new DateTimeZone($zone['timezone']));
        $date =   $date->format('Y-m-d h:i A');


        $page_start  = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;

        $user_id            = $this->input->post('userid');
        $logged_userid      = $this->input->post('loggeduserid');

        $get_hide_post          = $this->PostsApiModel->gethiddenposts($logged_userid);
        
        $get_report_post        = $this->PostsApiModel->getReportedposts($logged_userid);

        $get_report_challenge   = $this->PostsApiModel->getReportedchallenge($logged_userid);



        if(!empty ($user_id && $logged_userid)){
        $getProfile         = $this->PostsApiModel->getprofilebyid($user_id);
        $is_friend          = $this->PostsApiModel->isFriend($user_id , $logged_userid);

        if($is_friend > 0){
        $getPosts           = $this->PostsApiModel->getprofilePosts($user_id); 
        }
        else{      
        
        $getPosts           = $this->PostsApiModel->getprofilePostsnotfrnd($user_id);
        }

        $getChallenge       = $this->PostsApiModel->getChallengelist($user_id);

     

        if($getChallenge != null){
            
            foreach($getChallenge as $value => $key){
                
                $getChallenge[$value]['created_date']  =  strtotime($getChallenge[$value]['challenge_created_date']);
                
                
                // time validity section

                $now            = new DateTime($zone['timezone']);
               
             

                $expdate = $getChallenge[$value]['challenge_expired_date']." ".$getChallenge[$value]['challenge_expired_time'];
                
               
                $expired_date   = date("Y-m-d H:i:s ", strtotime($expdate));
                
                
               
                $getChallenge[$value]['cdate'] = $date;
               
                $current_date = strtotime($date);
                $exp_date     = strtotime($expdate);
              
              
                $diff   = $exp_date - $current_date;
                $getChallenge[$value]['diff'] = $diff;
                
                $getChallenge[$value]['challenge_status']    = $diff > 0 ? 1 : 0;
               
                $expired_date   = new DateTime($expired_date);    
                
                 
           
                
                $now_new     = $now->format('Y-m-d H:i:s');
                $expired_new = $expired_date->format('Y-m-d H:i:s');
                
                $now_new_date       = new DateTime($now_new);
                $expired_new_date   = new DateTime($expired_new);
              
                
              
                $interval       = $now_new_date->diff($expired_new_date); 
                
               
                $isPolled       = $this->PostsApiModel->isChallengepolled($getChallenge[$value]['challenge_id'] , $user_id);
                $getChallenge[$value]['ispolled']    = $isPolled > 0 ? 1 : 0;
               
                // print $date;
                // print "<pre>";
                // print $expdate;
                // print "<pre>";
                // print $interval->format('%d')." Days ".$interval->format('%h')." Hours ".$interval->format('%i')." Minutes " .$interval->format('%s');
                // exit();
                
                if($diff > 0){
               
                $getChallenge[$value]['validity_day']     = $interval->format('%d');
                $getChallenge[$value]['validity_hour']    = $interval->format('%h');
                $getChallenge[$value]['validity_minute']  = $interval->format('%i');
                $getChallenge[$value]['validity_second']  = $interval->format('%s');
                $getChallenge[$value]['challenge_winner'] = [];

              
                }

                else{

                $getChallenge[$value]['validity_day']     = 0;
                $getChallenge[$value]['validity_hour']    = 0;
                $getChallenge[$value]['validity_minute']  = 0;
                $getChallenge[$value]['validity_second']  = 0;

                $getChallenge[$value]['challenge_winner']        = $this->PostsApiModel->getWinner($getChallenge[$value]['challenge_id']);
              
              
                
                if($getChallenge[$value]['challenge_winner']){
               
                    foreach($getChallenge[$value]['challenge_winner'] as $key_1 => $value_1){

                       // $res[$value]['challenge_winner']['']
                       if($getChallenge[$value]['challenge_winner'][$key_1]['users_login_type'] == 0){

                       
                       $getChallenge[$value]['challenge_winner'][$key_1]['users_photo']             =$this->media_url."assets/images/users/".$getChallenge[$value]['challenge_winner'][$key_1]['users_photo'];
                    
                       }
                    if($getChallenge[$value]['challenge_type'] == 1){

                        $getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post']       = $this->media_url."assets/images/challenge/type1/".$getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post'];
                       
                    }
                    else{
                        $getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post']       = $this->media_url."assets/images/challenge/type2/".$getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post'];
                       
                    }

                    $getChallenge[$value]['challenge_winner'][$key_1]['challenge_media_type'] = $getChallenge[$value]['challenge_media_type'];
                    
                    $getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_video_thumbnail'] = $this->media_url."assets/images/challenge/thumb/".$getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_video_thumbnail'];
                    
                }
            }

               
                }
             

                // time validtity ends here


                if($getChallenge[$value]['challenge_posts_sub_type'] == 1) {

                    $explenatory_video                      = $this->ChallengeApiModel->getExplanetoryvideo($getChallenge[$value]['challenge_id']);
                    if($explenatory_video){
                    $getChallenge[$value]['explanetory_video']       = $this->media_url."assets/images/challenge/type1/".$explenatory_video[0]['challenge_posts_sub_post']; 
                    $getChallenge[$value]['explanetory_video_thumb'] = $this->media_url."assets/images/challenge/thumb/".$explenatory_video[0]['challenge_posts_video_thumbnail']; 
                    $getChallenge[$value]['challenge_posts_exp_media_type'] = $explenatory_video[0]['challenge_posts_exp_media_type']; 
                   
                    }
                }

                $getChallenge[$value]['pollCount']               = $this->ChallengeApiModel->challengePollcount($getChallenge[$value]['challenge_id']);
                $getChallenge[$value]['commentCount']            = $this->ChallengeApiModel->challengeCommentcount($getChallenge[$value]['challenge_id']);
                $getChallenge[$value]['challenge_posts_post']    = $this->ChallengeApiModel->getChallengeposts($getChallenge[$value]['challenge_id']);
                $getChallenge[$value]['comment']                 = $this->ChallengeApiModel->getChallengecomments($getChallenge[$value]['challenge_id']);
              
                $getChallenge[$value]['totalpollscount']         = $this->ChallengeApiModel->totalPostsCount($getChallenge[$value]['challenge_id']);
                $getChallenge[$value]['post_sub_type']               = 1;   
                
                foreach($getChallenge[$value]['comment'] as $pic => $key_pic){
                    $getChallenge[$value]['comment'][$pic]['users_photo'] = $this->media_url."assets/images/users/".$getChallenge[$value]['comment'][$pic]['users_photo'];
                }
               
                if($getChallenge[$value]['users_login_type'] == 0 ){
                $getChallenge[$value]['users_photo']             = $this->media_url."assets/images/users/".$getChallenge[$value]['users_photo'];
                   
                }

                foreach($getChallenge[$value]['challenge_posts_post'] as $values => $keys){
                   //$getChallenge[$value]['challenge_posts_post'][$values]['parent_position']    = $j;
                    $getChallenge[$value]['challenge_posts_post'][$values]['users_photo']                        = $this->media_url."assets/images/users/".$getChallenge[$value]['challenge_posts_post'][$values]['users_photo']; 
                    $getChallenge[$value]['challenge_posts_post'][$values]['pollCount']                          = $this->ChallengeApiModel->challengepostPollcount($getChallenge[$value]['challenge_id'],$getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_id']);
                    if($getChallenge[$value]['totalpollscount'] > 0) {
                   
                    $getChallenge[$value]['challenge_posts_post'][$values]['pollPercent']                        = intval(($getChallenge[$value]['challenge_posts_post'][$values]['pollCount'] * 100 ) / $getChallenge[$value]['totalpollscount']);
                   
                    }
                    else{
                        $getChallenge[$value]['challenge_posts_post'][$values]['pollPercent']                    = 0;
                    }
                    $getChallenge[$value]['challenge_posts_post'][$values]['challenge_media_type']               = $getChallenge[$value]['challenge_media_type'];
                    
                    $isPolledpost       = $this->ChallengeApiModel->isChallengepostpolled($getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_id'] , $user_id);
                   
                    $getChallenge[$value]['challenge_posts_post'][$values]['isPolled']                           = $isPolledpost > 0 ? 1 : 0;

                    if($getChallenge[$value]['challenge_type'] == 1){

                        $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post']       = $this->media_url."assets/images/challenge/type1/".$getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                       
                    }
                    else{
                        $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post']       = $this->media_url."assets/images/challenge/type2/".$getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                       
                    }
                        $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_video_thumbnail']    = $this->media_url."assets/images/challenge/thumb/".$getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_video_thumbnail'];
                    
                    //$res[$value]['challenge_posts_post'][$values]['users_photo']                = base_url()."assets/images/users/".$res[$value]['challenge_posts_post'][$values]['users_photo'];
            
                    $getChallenge[$value]['challenge_posts_post'][$values]['downloadlink']                    = $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                }

               
            }

         
          }

        if($getProfile){
         $getProfile = $getProfile[0];
         if($getProfile['users_username']==null)$getProfile['users_username']="";
         $getProfile['users_photo'] = $this->media_url.'assets/images/users/'.$getProfile['users_photo'];
        }
        

        if(!empty($getPosts['data'])){

            foreach ($getPosts['data'] as $key => $value){

            $date = new DateTime($getPosts['data'][$key]['posts_uploaded_date'], $GMT );
            $date->setTimezone(new DateTimeZone($zone['timezone']));
            $getPosts['data'][$key]['posts_uploaded_date'] = $date->format('Y-m-d H:i:s');
            
             $getPosts['data'][$key]['posts_photo_or_video']  = $this->media_url.'assets/images/posts/'.$getPosts['data'][$key]['posts_photo_or_video'];
             $pollcount                  = $this->PostsApiModel->pollsCount($getPosts['data'][$key]['posts_id']);
             $commentcount               = $this->PostsApiModel->commentsCount($getPosts['data'][$key]['posts_id']);
             $commentsCountouter         = $this->PostsApiModel->commentsCountouter($getPosts['data'][$key]['posts_id']);
             $is_user_polled             = $this->PostsApiModel->isUserpolled($getPosts['data'][$key]['posts_id'],$logged_userid);
             
             
             $getPosts['data'][$key]['posts_video_thumbnail'] = $this->media_url."assets/images/posts/thumbnails/".$getPosts['data'][$key]['posts_video_thumbnail'];
               

            foreach($commentcount['data'] as $co => $va){
                $commentcount['data'][$co]['users_photo'] = $this->media_url."assets/images/users/".$commentcount['data'][$co]['users_photo']; 
            }
            
            $ispolled = ($is_user_polled['count'] > 0) ? true : false;

            $getPosts['data'][$key]['created_date']     = strtotime(date("d-m-Y h:i A",strtotime($getPosts['data'][$key]['posts_uploaded_date']))); 
            $getPosts['data'][$key]['comment_count']    = $commentsCountouter; 
            $getPosts['data'][$key]['poll_count']       = $pollcount['count']; 
            $getPosts['data'][$key]['users_username']   = $getProfile['users_username'];
            $getPosts['data'][$key]['users_name']       = $getProfile['users_name'];
            $getPosts['data'][$key]['users_photo']      = $getProfile['users_photo'];
            $getPosts['data'][$key]['is_polled']        = $ispolled;
            $getPosts['data'][$key]['comment']          = $commentcount['data'];
            
            $getPosts['data'][$key]['post_sub_type']        = 0;
           
        
        }

            foreach($getChallenge as $row){

                $getPosts['data'][] = $row;
            }

            

           

            $i = 0;
            foreach($getPosts['data'] as $row){

                $totalArray[$i] = $row['created_date'];
                $i++;
            }
            arsort($totalArray);
           
            
            foreach($totalArray as $tot_key => $tot_val){
                $resultArray[] =  $getPosts['data'][$tot_key];
            
            }
            
            
          

            $k = 0;
            foreach($resultArray as $row_count => $key_count){
               
                if($resultArray[$row_count]['post_sub_type'] == 1) 
                {   
                    
                    $resultArray[$row_count]['parent_position'] = $k;
                    foreach($resultArray[$row_count]['challenge_posts_post'] as $sub_row_count => $key_row_count){
                        $resultArray[$row_count]['challenge_posts_post'][$sub_row_count]['parent_position'] = $k;
                    }
                }
                
                
             
                $k++;
               
            }

            $hide_array =   [];
            foreach($get_hide_post as $row){
                $hide_array[]   = $row['hide_posts_post_id'];
            }
    
            $report_array =   [];
            foreach($get_report_post as $row){
                $report_array[]   = $row['posts_report_post_id'];
            }

            $challenge_report_array =   [];
            foreach($get_report_challenge as $row){
                $challenge_report_array[]   = $row['challenge_report_challenge_id'];
            }
    
            
    
    
            $final_post_array =   [];
            foreach($resultArray as $in_row){
                if(!in_array($in_row['posts_id'],$hide_array)){
                    $final_post_array[]    =  $in_row ;
                }
            }

            $final_challenge_array =   [];
            foreach($final_post_array as $in_row){
                if(!in_array($in_row['challenge_id'],$challenge_report_array)){
                    $final_challenge_array[]    =  $in_row ;
                }
            }
            
            $final_last_array = [];
            foreach($final_challenge_array as $in_row){
               if(!in_array($in_row['posts_id'],$report_array)){
                   $final_last_array[]    =  $in_row ;
               }
           }

           $final_last_challenge_array = [];
           foreach($final_last_array as $in_row){
              if(!in_array($in_row['challenge_id'],$report_array)){
                  $final_last_challenge_array[]    =  $in_row ;
              }
          }

          $total_posts     = count($final_last_challenge_array); 


              
          $profile_posts_final          = array_slice($final_last_challenge_array,$page_start,10);

          $profile_posts_final_next     = array_slice($final_last_challenge_array,$page_start+10,10);

        }
        if($profile_posts_final){

            $this->output->set_output(json_encode(array('report_challenge_id' => $get_report_challenge,'hide_posts_id' => $get_hide_post ,'report_posts_id' =>$get_report_post,'posts'=>$profile_posts_final,'total_posts' => $total_posts,'is_completed' => count($profile_posts_final_next) > 0 ? 0 : 1,'status'=>true)));
        }
        
        else{

            $this->output->set_output(json_encode(array('posts'=>'no data found','status'=>false)));
        
         }
        }
        else{
            $this->output->set_output(json_encode(array('posts'=>'parameter missing','status'=>false)));
        }

    }





     // user profile posts
     function returnprofilePosts($user_id,$logged_userid,$page_start){

        $this->output->set_content_type('application/json');
       
        $zone = getallheaders();
        $GMT  = new DateTimeZone("GMT");
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";


        $dt = new DateTime();
                
        $current_date = $dt->format('d-m-Y h:i A');
       
      
        $date = new DateTime($current_date, $GMT );
        $date->setTimezone(new DateTimeZone($zone['timezone']));
        $date =   $date->format('Y-m-d h:i A');


        $page_start  = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;

      

        $get_hide_post          = $this->PostsApiModel->gethiddenposts($logged_userid);
        
        $get_report_post        = $this->PostsApiModel->getReportedposts($logged_userid);

        $get_report_challenge   = $this->PostsApiModel->getReportedchallenge($logged_userid);

        
       
        if(!empty ($user_id && $logged_userid)){

        $getProfile         = $this->PostsApiModel->getprofilebyid($user_id);
        $is_friend          = $this->PostsApiModel->isFriend($user_id , $logged_userid);
        
        if($is_friend > 0){
        $getPosts           = $this->PostsApiModel->getprofilePosts($user_id); 
        }
        else{      
        
        $getPosts           = $this->PostsApiModel->getprofilePostsnotfrnd($user_id);
        }

        $getChallenge       = $this->PostsApiModel->getChallengelist($user_id);

     

        if($getChallenge != null){
            
            foreach($getChallenge as $value => $key){
                
                $getChallenge[$value]['created_date']  =  strtotime($getChallenge[$value]['challenge_created_date']);
                
                
                // time validity section

                $now            = new DateTime($zone['timezone']);
               
             

                $expdate = $getChallenge[$value]['challenge_expired_date']." ".$getChallenge[$value]['challenge_expired_time'];
                
               
                $expired_date   = date("Y-m-d H:i:s ", strtotime($expdate));
                
                
               
                $getChallenge[$value]['cdate'] = $date;
               
                $current_date = strtotime($date);
                $exp_date     = strtotime($expdate);
              
              
                $diff   = $exp_date - $current_date;
                $getChallenge[$value]['diff'] = $diff;
                
                $getChallenge[$value]['challenge_status']    = $diff > 0 ? 1 : 0;
               
                $expired_date   = new DateTime($expired_date);    
                
                 
           
                
                $now_new     = $now->format('Y-m-d H:i:s');
                $expired_new = $expired_date->format('Y-m-d H:i:s');
                
                $now_new_date       = new DateTime($now_new);
                $expired_new_date   = new DateTime($expired_new);
              
                
              
                $interval       = $now_new_date->diff($expired_new_date); 
                
               
                $isPolled       = $this->PostsApiModel->isChallengepolled($getChallenge[$value]['challenge_id'] , $user_id);
                $getChallenge[$value]['ispolled']    = $isPolled > 0 ? 1 : 0;
               
                // print $date;
                // print "<pre>";
                // print $expdate;
                // print "<pre>";
                // print $interval->format('%d')." Days ".$interval->format('%h')." Hours ".$interval->format('%i')." Minutes " .$interval->format('%s');
                // exit();
                
                if($diff > 0){
               
                $getChallenge[$value]['validity_day']     = $interval->format('%d');
                $getChallenge[$value]['validity_hour']    = $interval->format('%h');
                $getChallenge[$value]['validity_minute']  = $interval->format('%i');
                $getChallenge[$value]['validity_second']  = $interval->format('%s');
                $getChallenge[$value]['challenge_winner'] = [];

              
                }

                else{

                $getChallenge[$value]['validity_day']     = 0;
                $getChallenge[$value]['validity_hour']    = 0;
                $getChallenge[$value]['validity_minute']  = 0;
                $getChallenge[$value]['validity_second']  = 0;

                $getChallenge[$value]['challenge_winner']        = $this->PostsApiModel->getWinner($getChallenge[$value]['challenge_id']);
              
              
                
                if($getChallenge[$value]['challenge_winner']){
               
                    foreach($getChallenge[$value]['challenge_winner'] as $key_1 => $value_1){

                       // $res[$value]['challenge_winner']['']
                       if($getChallenge[$value]['challenge_winner'][$key_1]['users_login_type'] == 0){

                       
                       $getChallenge[$value]['challenge_winner'][$key_1]['users_photo']             =$this->media_url."assets/images/users/".$getChallenge[$value]['challenge_winner'][$key_1]['users_photo'];
                    
                       }
                    if($getChallenge[$value]['challenge_type'] == 1){

                        $getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post']       = $this->media_url."assets/images/challenge/type1/".$getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post'];
                       
                    }
                    else{
                        $getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post']       = $this->media_url."assets/images/challenge/type2/".$getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_sub_post'];
                       
                    }

                    $getChallenge[$value]['challenge_winner'][$key_1]['challenge_media_type'] = $getChallenge[$value]['challenge_media_type'];
                    
                    $getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_video_thumbnail'] = $this->media_url."assets/images/challenge/thumb/".$getChallenge[$value]['challenge_winner'][$key_1]['challenge_posts_video_thumbnail'];
                    
                }
            }

               
                }
             
              
                // time validtity ends here


                if($getChallenge[$value]['challenge_posts_sub_type'] == 1) {

                    $explenatory_video                      = $this->ChallengeApiModel->getExplanetoryvideo($getChallenge[$value]['challenge_id']);
                    if($explenatory_video){
                    $getChallenge[$value]['explanetory_video']       = $this->media_url."assets/images/challenge/type1/".$explenatory_video[0]['challenge_posts_sub_post']; 
                    $getChallenge[$value]['explanetory_video_thumb'] = $this->media_url."assets/images/challenge/thumb/".$explenatory_video[0]['challenge_posts_video_thumbnail']; 
                    $getChallenge[$value]['challenge_posts_exp_media_type'] = $explenatory_video[0]['challenge_posts_exp_media_type']; 
                   
                    }
                }

                $getChallenge[$value]['pollCount']               = $this->ChallengeApiModel->challengePollcount($getChallenge[$value]['challenge_id']);
                $getChallenge[$value]['commentCount']            = $this->ChallengeApiModel->challengeCommentcount($getChallenge[$value]['challenge_id']);
                $getChallenge[$value]['challenge_posts_post']    = $this->ChallengeApiModel->getChallengeposts($getChallenge[$value]['challenge_id']);
                $getChallenge[$value]['comment']                 = $this->ChallengeApiModel->getChallengecomments($getChallenge[$value]['challenge_id']);
              
                $getChallenge[$value]['totalpollscount']         = $this->ChallengeApiModel->totalPostsCount($getChallenge[$value]['challenge_id']);
                $getChallenge[$value]['post_sub_type']               = 1;   
                
                foreach($getChallenge[$value]['comment'] as $pic => $key_pic){
                    $getChallenge[$value]['comment'][$pic]['users_photo'] = $this->media_url."assets/images/users/".$getChallenge[$value]['comment'][$pic]['users_photo'];
                }
               
                if($getChallenge[$value]['users_login_type'] == 0 ){
                $getChallenge[$value]['users_photo']             = $this->media_url."assets/images/users/".$getChallenge[$value]['users_photo'];
                   
                }

                foreach($getChallenge[$value]['challenge_posts_post'] as $values => $keys){
                   //$getChallenge[$value]['challenge_posts_post'][$values]['parent_position']    = $j;
                    $getChallenge[$value]['challenge_posts_post'][$values]['users_photo']                        = $this->media_url."assets/images/users/".$getChallenge[$value]['challenge_posts_post'][$values]['users_photo']; 
                    $getChallenge[$value]['challenge_posts_post'][$values]['pollCount']                          = $this->ChallengeApiModel->challengepostPollcount($getChallenge[$value]['challenge_id'],$getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_id']);
                    if($getChallenge[$value]['totalpollscount'] > 0) {
                   
                    $getChallenge[$value]['challenge_posts_post'][$values]['pollPercent']                        = intval(($getChallenge[$value]['challenge_posts_post'][$values]['pollCount'] * 100 ) / $getChallenge[$value]['totalpollscount']);
                   
                    }
                    else{
                        $getChallenge[$value]['challenge_posts_post'][$values]['pollPercent']                    = 0;
                    }
                    $getChallenge[$value]['challenge_posts_post'][$values]['challenge_media_type']               = $getChallenge[$value]['challenge_media_type'];
                    
                    $isPolledpost       = $this->ChallengeApiModel->isChallengepostpolled($getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_id'] , $user_id);
                   
                    $getChallenge[$value]['challenge_posts_post'][$values]['isPolled']                           = $isPolledpost > 0 ? 1 : 0;

                    if($getChallenge[$value]['challenge_type'] == 1){

                        $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post']       = $this->media_url."assets/images/challenge/type1/".$getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                       
                    }
                    else{
                        $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post']       = $this->media_url."assets/images/challenge/type2/".$getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                       
                    }
                        $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_video_thumbnail']    = $this->media_url."assets/images/challenge/thumb/".$getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_video_thumbnail'];
                    
                    //$res[$value]['challenge_posts_post'][$values]['users_photo']                = base_url()."assets/images/users/".$res[$value]['challenge_posts_post'][$values]['users_photo'];
            
                    $getChallenge[$value]['challenge_posts_post'][$values]['downloadlink']                    = $getChallenge[$value]['challenge_posts_post'][$values]['challenge_posts_sub_post'];
                }

               
            }

         
          }

        if($getProfile){
         $getProfile = $getProfile[0];
         if($getProfile['users_username']==null)$getProfile['users_username']="";
         $getProfile['users_photo'] = $this->media_url.'assets/images/users/'.$getProfile['users_photo'];
        }
        

        if(!empty($getPosts['data'])){

            foreach ($getPosts['data'] as $key => $value){

            $date = new DateTime($getPosts['data'][$key]['posts_uploaded_date'], $GMT );
            $date->setTimezone(new DateTimeZone($zone['timezone']));
            $getPosts['data'][$key]['posts_uploaded_date'] = $date->format('Y-m-d H:i:s');
            
             $getPosts['data'][$key]['posts_photo_or_video']  = $this->media_url.'assets/images/posts/'.$getPosts['data'][$key]['posts_photo_or_video'];
             $pollcount                  = $this->PostsApiModel->pollsCount($getPosts['data'][$key]['posts_id']);
             $commentcount               = $this->PostsApiModel->commentsCount($getPosts['data'][$key]['posts_id']);
             $commentsCountouter         = $this->PostsApiModel->commentsCountouter($getPosts['data'][$key]['posts_id']);
             $is_user_polled             = $this->PostsApiModel->isUserpolled($getPosts['data'][$key]['posts_id'],$logged_userid);
             
             
             $getPosts['data'][$key]['posts_video_thumbnail'] = $this->media_url."assets/images/posts/thumbnails/".$getPosts['data'][$key]['posts_video_thumbnail'];
               

            foreach($commentcount['data'] as $co => $va){
                $commentcount['data'][$co]['users_photo'] = $this->media_url."assets/images/users/".$commentcount['data'][$co]['users_photo']; 
            }
            
            $ispolled = ($is_user_polled['count'] > 0) ? true : false;

            $getPosts['data'][$key]['created_date']     = strtotime(date("d-m-Y h:i A",strtotime($getPosts['data'][$key]['posts_uploaded_date']))); 
            $getPosts['data'][$key]['comment_count']    = $commentsCountouter; 
            $getPosts['data'][$key]['poll_count']       = $pollcount['count']; 
            $getPosts['data'][$key]['users_username']   = $getProfile['users_username'];
            $getPosts['data'][$key]['users_name']       = $getProfile['users_name'];
            $getPosts['data'][$key]['users_photo']      = $getProfile['users_photo'];
            $getPosts['data'][$key]['is_polled']        = $ispolled;
            $getPosts['data'][$key]['comment']          = $commentcount['data'];
            
            $getPosts['data'][$key]['post_sub_type']        = 0;
           
        
        }

            foreach($getChallenge as $row){

                $getPosts['data'][] = $row;
            }

            

           

            $i = 0;
            foreach($getPosts['data'] as $row){

                $totalArray[$i] = $row['created_date'];
                $i++;
            }
            arsort($totalArray);
           
            
            foreach($totalArray as $tot_key => $tot_val){
                $resultArray[] =  $getPosts['data'][$tot_key];
            
            }
            
            
          

            $k = 0;
            foreach($resultArray as $row_count => $key_count){
               
                if($resultArray[$row_count]['post_sub_type'] == 1) 
                {   
                    
                    $resultArray[$row_count]['parent_position'] = $k;
                    foreach($resultArray[$row_count]['challenge_posts_post'] as $sub_row_count => $key_row_count){
                        $resultArray[$row_count]['challenge_posts_post'][$sub_row_count]['parent_position'] = $k;
                    }
                }
                
                
             
                $k++;
               
            }

            $hide_array =   [];
            foreach($get_hide_post as $row){
                $hide_array[]   = $row['hide_posts_post_id'];
            }
    
            $report_array =   [];
            foreach($get_report_post as $row){
                $report_array[]   = $row['posts_report_post_id'];
            }

            $challenge_report_array =   [];
            foreach($get_report_challenge as $row){
                $challenge_report_array[]   = $row['challenge_report_challenge_id'];
            }
    
            
    
    
            $final_post_array =   [];
            foreach($resultArray as $in_row){
                if(!in_array($in_row['posts_id'],$hide_array)){
                    $final_post_array[]    =  $in_row ;
                }
            }

            $final_challenge_array =   [];
            foreach($final_post_array as $in_row){
                if(!in_array($in_row['challenge_id'],$challenge_report_array)){
                    $final_challenge_array[]    =  $in_row ;
                }
            }
            
            $final_last_array = [];
            foreach($final_challenge_array as $in_row){
               if(!in_array($in_row['posts_id'],$report_array)){
                   $final_last_array[]    =  $in_row ;
               }
           }

           $final_last_challenge_array = [];
           foreach($final_last_array as $in_row){
              if(!in_array($in_row['challenge_id'],$report_array)){
                  $final_last_challenge_array[]    =  $in_row ;
              }
          }

          $total_posts     = count($final_last_challenge_array); 


              
          $profile_posts_final          = array_slice($final_last_challenge_array,$page_start,10);
          $profile_posts_final_next     = array_slice($final_last_challenge_array,$page_start+10,10);

          foreach($profile_posts_final as $k => $v){
            $profile_posts_final[$k]['is_completed_profile'] = $profile_posts_final_next; 
          }

          return $profile_posts_final;
            //$this->output->set_output(json_encode(array('report_challenge_id' => $get_report_challenge,'hide_posts_id' => $get_hide_post ,'report_posts_id' =>$get_report_post,'posts'=>$profile_posts_final,'total_posts' => $total_posts,'status'=>true)));
        }
        
        else{

            return "no data found";
        
         }
        }
        else{
            $this->output->set_output(json_encode(array('posts'=>'parameter missing','status'=>false)));
        }

    }

    // hide profile posts
    function hideprofilePost(){

        $this->output->set_content_type('application/json');

        $user_id     = $this->input->post('user_id');
        $post_id     = $this->input->post('post_id');
        $blockPost   = $this->PostsApiModel->hideprofilePost($user_id , $post_id);

        if($blockPost > 0){

            $this->output->set_output(json_encode(array('message' => "successfully updated",'status'=>true)));
        }
        else{
            $this->output->set_output(json_encode(array('posts' => "failed to update",'status'=>false)));
        }
    }

    // report posts
    function reportPost(){

        $this->output->set_content_type('application/json');

        $user_id       = $this->input->post('user_id');
        $post_id       = $this->input->post('post_id');
        $msg           = $this->input->post('message');
        $type          = $this->input->post('type');
        $logged_userid = $this->input->post('logged_userid');
        
        $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;  
        $page_start  = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;

        if(!empty($user_id && $post_id)){
            
        $res         = $this->PostsApiModel->reportPost($logged_userid , $post_id , $msg);


        switch($res){


            case $res > 0:


            switch ($type){
                
                case 0:
                $posts = $this->returngetPosts($user_id,$post_id,$type,$country,$state,$district,$area,$page_start);
                
                $last_posts = $posts[9];
                if($last_posts['posts_id'] == $postid || $last_posts == null)
                $this->output->set_output(json_encode(array('value' => "successfully reported",'is_completed'=>1,'status'=>true)));
                else
                $this->output->set_output(json_encode(array('value' => "successfully reported",'data' => array($last_posts),'is_completed' => $last_posts['is_completed'] > 0 ? 0 : 1,'status'=>true)));
                  
                break;


                case 1:
                $posts = $this->returnprofilePosts($user_id,$logged_userid,$page_start);
                
                $last_posts = $posts[9];
                if($last_posts['posts_id'] == $postid || $last_posts == null)
                $this->output->set_output(json_encode(array('value' => "successfully reported",'is_completed'=>1,'status'=>true)));
                else
                $this->output->set_output(json_encode(array('value' => "successfully reported",'data' => array($last_posts),'is_completed' => $last_posts['is_completed_profile'] > 0 ? 0 : 1,'status'=>true)));
                 
                break;
                
            }
            
            $this->PostsApiModel->updateReportcount($post_id);
            $removePosts = $this->PostsApiModel->removePostsbyreportcount($post_id);
           
            break;

            case -1:
            $this->output->set_output(json_encode(array('value' => "already reported",'status'=>false)));
            break;
            
            case 0:
            $this->output->set_output(json_encode(array('value' => "operation failed",'status'=>false)));
            break;

        }
    }
        else{
            $this->output->set_output(json_encode(array('value' => "parameter missing",'status'=>false)));
           
        }
     
    }

    //comment posts api
    function commentPost(){

        $this->output->set_content_type('application/json');

        $post_id          = $this->input->post('post_id');
        $user_id          = $this->input->post('user_id');
        $post_user_id     = $this->input->post('postuserid');
        $comment          = $this->input->post('comment');
        
        if(!empty($post_id && $user_id && $comment)){

        $res              = $this->PostsApiModel->commentPost($user_id , $post_id , $comment , $post_user_id);

        

        if($res > 0){

            $this->output->set_output(json_encode(array('message' => "successfully commented",'comment'=>$res['data'],'status'=>true)));
        }
        else{
            
            $this->output->set_output(json_encode(array('posts' => "failed to comment",'status'=>false)));
        }
    }
    else{
            $this->output->set_output(json_encode(array('posts' => "parameter missing",'status'=>false)));
    }
    }

    // get post comments
    function getPostcomments(){ 

        $this->output->set_content_type('application/json');

        $post_id     = $this->input->post('post_id');
        
        $res         = $this->PostsApiModel->getPostcomments($post_id);

        if($res){
        foreach($res as $key => $value){

            $res[$key]['users_photo']   = $this->media_url."assets/images/users/".$res[$key]['users_photo'];
            $res_reply                  = $this->PostsApiModel->getcommentsreply($res[$key]['posts_comments_id'],$res[$key]['posts_comments_post_id']);
           
            foreach($res_reply as $val  => $keys){
                
                $res_reply[$val]['users_photo']      = $this->media_url."assets/images/users/".$res_reply[$val]['users_photo'];
                $res_reply[$val]['reply_to_user']    = $this->PostsApiModel->getusersnamebyid($res_reply[$val]['posts_comment_reply_comment_user_id']);
        
            }   
           
            $res[$key]['reply']         = $res_reply;          
           
            
        }
    }

        if($res != null){

            $this->output->set_output(json_encode(array('data' => $res,'status'=>true)));
        }
        else{
            $this->output->set_output(json_encode(array('data' => "no value found",'status'=>false)));
        }
    }

    // get post comments by id
    function getPostcommentsbyid(){ 

        $this->output->set_content_type('application/json');

        $post_id     = $this->input->post('comment_id');
        
        $res         = $this->PostsApiModel->getPostcommentsbyid($post_id);

        foreach($res as $key => $value){
            $res[$key]['users_photo'] = $this->media_url."assets/images/users/".$res[$key]['users_photo'];
            $res_reply          = $this->PostsApiModel->getcommentsreply($res[$key]['posts_comments_id'],$res[$key]['posts_comments_post_id']);
            foreach($res_reply as $val  => $keys){
                $res_reply[$val]['users_photo']      = $this->media_url."assets/images/users/".$res_reply[$val]['users_photo'];
                $res_reply[$val]['reply_to_user']    = $this->PostsApiModel->getusersnamebyid($res_reply[$val]['posts_comment_reply_comment_user_id']);
        
            }   
            $res[$key]['reply'] = $res_reply;
        }

        if($res!=null){

            $this->output->set_output(json_encode(array('data' => $res,'status'=>true)));
        }
        else{
            $this->output->set_output(json_encode(array('data' => "no value found",'status'=>false)));
        }
    }
    
    // reply to comments
    function replyPostscomments(){

        $this->output->set_content_type('application/json');

        $post_id            = $this->input->post('post_id');
        $user_id            = $this->input->post('user_id');
        $comment_id         = $this->input->post('comment_id');
        $comment_user       = !empty($this->input->post('comment_user_id')) ? $this->input->post('comment_user_id') : 1;
        $reply              = $this->input->post('reply');
        
       
        $insert      = array('posts_comments_reply_posts_id'=>$post_id,
                        'posts_comments_reply_comment_id'=>$comment_id,
                        'posts_comments_reply'=>$reply,
                        'posts_comment_reply_comment_user_id' => $comment_user,
                        'posts_comments_reply_user_id' => $user_id);

        $res         = $this->PostsApiModel->replyPostscomments($insert,$comment_user);

        if($res > 0){

            $this->output->set_output(json_encode(array('message' => "succesfully inserted",'status'=>true)));
        }
        else{

            $this->output->set_output(json_encode(array('posts' => "no value found",'status'=>false)));
        }
    }


    //sort by categories Post old
    function sortPostsold(){

        $status_user = true; 
        $status_post = true;

        $this->output->set_content_type('application/json');

        $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;

       
        $res         = $this->PostsApiModel->sortPosts($country , $state , $district , $area);
      
        if($res > 0){
            
            foreach($res as $key => $value){
                
                $res[$key]['posts_photo_or_video'] = $this->media_url.'assets/images/posts/'.$res[$key]['posts_photo_or_video'];
            }
            
        }
        else{

                $status_post = 'false';
        }
        
        $this->output->set_output(json_encode(array('posts' => $res , 'status_post' => $status_post)));
    }





    //sort by categories Post
    function sortPosts(){

      

        $zone = getallheaders();
        $GMT  = new DateTimeZone("GMT");
        if(empty($zone['timezone'])) $zone['timezone'] = "Asia/Kolkata";

        $userid      = !empty($this->input->post('userid')) ? $this->input->post('userid') : 1;
        $page_start  = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;

        $this->output->set_content_type('application/json');

        $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;

        $res_user         = $this->UsersApiModel->sortUsers($country , $state , $district , $area);
        $res              = $this->PostsApiModel->sortPosts($country , $state , $district , $area);
        
        $get_report_post = $this->PostsApiModel->getReportedposts($userid);
        $get_hide_post   = $this->PostsApiModel->gethiddenposts($userid);

        if($res_user != 0){
  	$status_user = true; 
        
            
            foreach($res_user as $keys => $values){

                $res_user[$keys]['users_photo'] = $this->media_url.'assets/images/users/'.$res_user[$keys]['users_photo'];
                $res_user[$keys]['posts_count'] = $this->UsersApiModel->getpostCount($res_user[$keys]['user_id']);
            }
        }
        else{

            $status_user = false;
        }

     
        $date = new DateTime(date("Y-m-d H:i:s", strtotime("now")), $GMT );
        $date->setTimezone(new DateTimeZone($zone['timezone']));
        $datetime = $date->format('Y-m-d H:i:s'); 

        // $date = new DateTime($res['data'][$key]['posts_uploaded_date'], $GMT );
        // $date->setTimezone(new DateTimeZone($zone['timezone']));
        // $res['data'][$key]['posts_uploaded_date'] = $date->format('Y-m-d H:i:s'); 



        if($res != 0){
           
            foreach($res as $key => $value){
                
                $uploaddate = new DateTime($res[$key]['posts_uploaded_date'], $GMT );
                $uploaddate->setTimezone(new DateTimeZone($zone['timezone']));
                $res[$key]['posts_uploaded_date'] = $uploaddate->format('Y-m-d H:i:s'); 
              

                $datePercent                       = strtotime($datetime) - strtotime($res[$key]['posts_uploaded_date']);
                $pollCount                         = $this->PostsApiModel->getPostspollCount($res[$key]['posts_id']); 
                $res[$key]['pollPercent']          = $pollCount > 0 ? intval($datePercent / $pollCount) : 0; 
                $res[$key]['posts_photo_or_video'] = $this->media_url.'assets/images/posts/'.$res[$key]['posts_photo_or_video'];
                $res[$key]['posts_video_thumbnail']= $this->media_url.'assets/images/posts/thumbnails/'.$res[$key]['posts_video_thumbnail'];
                
            }
            
        }
        else{

          
        }
        
        $sort_array = array();
        $res_2      = array();
       
        if($res){

         
           
        foreach($res as $res_1 => $key_1){
            
            // $sort_array[$key_1['pollPercent']] = $res[$res_1];
            // print_r($sort_array);

            if($key_1['pollPercent'] > 0){
            $sort_array[$key_1['pollPercent']] = $res[$res_1];
            }
        }

      
        
        ksort($sort_array);
      
        foreach($sort_array as $res_2[]);
        
        }



        $hide_array =   [];
        foreach($get_hide_post as $row){
            $hide_array[]   = $row['hide_posts_post_id'];
        }

        $report_array =   [];
        foreach($get_report_post as $row){
            $report_array[]   = $row['posts_report_post_id'];
        }



        $final_array =   [];
        foreach($res_2 as $in_row){
            if(!in_array($in_row['posts_id'],$hide_array)){
                $final_array[]    =  $in_row ;
            }
        }
        
        $final_last_array = [];
        foreach($final_array as $in_row){
           if(!in_array($in_row['posts_id'],$report_array)){
               $final_last_array[]    =  $in_row ;
           }
       }

        $total_posts     = count($final_last_array);
        $final_posts     = array_slice($final_last_array,$page_start,20);


        if($final_posts){
$status_post = true;
}
else{
$status_post = false;
}
		 

        $this->output->set_output(json_encode(array('users' => $res_user,'hidden_post_id' => $get_hide_post,'report_post_id' => $get_report_post,'posts' => $final_posts ,'total_posts' => $total_posts,'status_user' => $status_user , 'status_post' => $status_post)));
    }


    //sort by categories Post new code 
    function sortPosts_new(){

        
        $status_user = true; 
        $status_post = true;

        $this->output->set_content_type('application/json');

        $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;

        $res_user         = $this->UsersApiModel->sortUsers_new($country , $state , $district , $area);
        $res              = $this->PostsApiModel->sortPosts_new($country , $state , $district , $area);
        

        if($res_user > 0){

            
            foreach($res_user as $keys => $values){

                $res_user[$keys]['users_photo'] = $this->media_url.'assets/images/users/'.$res_user[$keys]['users_photo'];
                $res_user[$keys]['posts_count'] = $this->UsersApiModel->getpostCount($res_user[$keys]['user_id']);
            }
        }
        else{

            $status_user = 'false';
        }

        if($res > 0){
            
            foreach($res as $key => $value){
                $res[$key]['posts_photo_or_video'] = $this->media_url.'assets/images/posts/'.$res[$key]['posts_photo_or_video'];
            }
            
        }
        else{

            $status_post = 'false';
        }
        
        $this->output->set_output(json_encode(array('users' => $res_user,'posts' => $res ,'status_user' => $status_user , 'status_post' => $status_post)));
    }

    // get posts api by posts id
    function getnotificationPostsbyid(){

        $this->output->set_content_type('application/json');
       
        $postsid = $this->input->post('postid');
        $userid  = $this->input->post('userid');

        if(!empty($postsid)){

        $res                  = $this->PostsApiModel->getnotificationPostsbyid($postsid);
        $pollcount            = $this->PostsApiModel->pollsCount($postsid);
        $isPostpolled         = $this->PostsApiModel->isPostpolled($postsid,$userid);
        $commentcount         = $this->PostsApiModel->commentsCount($postsid);
        $commentsCountouter   = $this->PostsApiModel->commentsCountouter($postsid);

        foreach($commentcount['data'] as $co => $va){
            
            $commentcount['data'][$co]['users_photo'] = $this->media_url."assets/images/users/".$commentcount['data'][$co]['users_photo']; 
        }
        if($res > 0){
            
            foreach($res as $key => $val){

                $res[$key]['posts_photo_or_video']  = $this->media_url."assets/images/posts/".$res[$key]['posts_photo_or_video'];
                $res[$key]['posts_video_thumbnail'] = $this->media_url."assets/images/posts/thumbnails/".$res[$key]['posts_video_thumbnail'];
                $res[$key]['users_photo']           = $this->media_url."assets/images/users/".$res[$key]['users_photo'];
                $res[$key]['poll_count']            = $pollcount['count'];
                $res[$key]['comment_count']         = $commentsCountouter;
                $res[$key]['comment']               = $commentcount['data'];
                $res[$key]['isPolled']              = $isPostpolled > 0 ? true : false;
            }
            $this->output->set_output(json_encode(array('value' => $res , 'status' => true))); 
        }
        else{
            $this->output->set_output(json_encode(array('value' => 'no data found' , 'status' => false))); 
        }
       
        }
        else{

            $this->output->set_output(json_encode(array('value' => 'parameter missing' ,'status' => false)));
        }
    }

    // edit posts
    function editPosts(){

        $this->output->set_content_type('applcaition/json');
        
       
        $postid     = $this->input->post('postid');
        $title      = $this->input->post('title');
        $content    = $this->input->post('content');  

        if(!empty($postid)){

        
        $update     = array('posts_title' => $title ,'posts_uploaded_date' => date('Y-m-d h:m:s'),'posts_content' => $content);
        $res        = $this->PostsApiModel->editPosts($postid , $update);
        if($res > 0){

            $this->output->set_output(json_encode(array('value' => 'successfully updated' , 'status' => true)));   
        } 
        else{
            $this->output->set_output(json_encode(array('value' => 'operation failed' , 'status' => false))); 
        }
     }
        else{
            $this->output->set_output(json_encode(array('value' => 'parameter missing' , 'status' => false))); 
   
        }
       
        
     
    }
    

     // delete posts
     function deletePosts(){

        $this->output->set_content_type('applcation/json');
        
        $postid           = $this->input->post('postid');
       
        $type             = $this->input->post('type');

        $logged_userid    = $this->input->post('logged_userid');


        $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;

        $page_start  = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;

        $userid      = $this->input->post('userid');

       

        if(!empty($postid)){

        $res        = $this->PostsApiModel->deletePosts($postid);
        if($res > 0){
            
            switch ($type){
                case 0:
                $posts = $this->returngetPosts($userid,$postid,$type,$country,$state,$district,$area,$page_start);
                
               
                $last_posts = $posts[9];
                if($last_posts['posts_id'] == $postid || $last_posts == null)
                $this->output->set_output(json_encode(array('value' => "successfully deleted",'is_completed'=>1,'status'=>true)));
                else
                $this->output->set_output(json_encode(array('value' => "successfully deleted",'data' => array($last_posts),'is_completed' => $last_posts['is_completed'] > 0 ? 0 : 1,'status'=>true)));
                             
                break;


                case 1:
                $posts = $this->returnprofilePosts($userid,$logged_userid,$page_start);
               
                $last_posts = $posts[9];
                if($last_posts['posts_id'] == $postid || $last_posts == null)
                $this->output->set_output(json_encode(array('value' => "successfully deleted",'is_completed'=>1,'status'=>true)));
                else
                $this->output->set_output(json_encode(array('value' => "successfully deleted",'data' => array($last_posts),'is_completed' => $last_posts['is_completed_profile'] > 0 ? 0 : 1,'status'=>true)));
                break;
                
            }
                      
            $delete_notifications = $this->PostsApiModel->deletenotifications($postid);
           
            
        } 
        else{
            $this->output->set_output(json_encode(array('value' => 'operation failed' , 'status' => false))); 
        }
        }
        else{
            $this->output->set_output(json_encode(array('value' => 'parameter missing' , 'status' => false))); 
   
        }
    
    }


     // hide posts
     function hidePosts(){

        $this->output->set_content_type('application/json');
        
        $postid      = $this->input->post('postid');
        $userid      = $this->input->post('userid');
        $type        = $this->input->post('type');  
        $country     = (!empty($this->input->post('country'))) ? $this->input->post('country') : NULL;  
        $state       = (!empty($this->input->post('state'))) ? $this->input->post('state') : NULL;
        $district    = (!empty($this->input->post('district'))) ? $this->input->post('district') : NULL;  
        $area        = (!empty($this->input->post('area'))) ? $this->input->post('area') : NULL;
        $page_start  = !empty($this->input->post('page_start'))  ?  $this->input->post('page_start') : 0;

        if(!empty($postid && $userid)){

        $res        = $this->PostsApiModel->hidePosts($postid , $userid);
        if($res > 0){

            switch ($type){
                case 0:
                $posts = $this->returngetPosts($userid,$postid,$type,$country,$state,$district,$area,$page_start);
              
                $last_posts = $posts[9];
                if($last_posts['posts_id'] == $postid || $last_posts == null)
                $this->output->set_output(json_encode(array('value' => "operation success",'is_completed'=>1,'status'=>true)));
                else
                $this->output->set_output(json_encode(array('value' => "operation success",'data' => array($last_posts),'is_completed' => $last_posts['is_completed'] > 0 ? 0 : 1,'status'=>true)));
               
                break;


                case 1:
                $posts = $this->returnprofilePosts($userid,$logged_userid,$page_start);
                $last_posts = $posts[9];
                if($last_posts['posts_id'] == $postid || $last_posts == null)
                $this->output->set_output(json_encode(array('value' => "operation success",'is_completed'=>1,'status'=>true)));
                else
                $this->output->set_output(json_encode(array('value' => "operation success",'data' => array($last_posts),'is_completed' => $last_posts['is_completed_profile'] > 0 ? 0 : 1,'status'=>true)));
               
                break;
                
            }
          
            
        } 
        else{
            $this->output->set_output(json_encode(array('value' => 'operation failed' , 'status' => false))); 
        }
        }
        else{
            $this->output->set_output(json_encode(array('value' => 'parameter missing' , 'status' => false))); 
   
        }
    
    }



}
