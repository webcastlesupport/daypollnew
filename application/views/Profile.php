<script>
    $(function () {
        loadforms(1);
    });

</script>



<style>


    #adminimageedit {
        opacity: 1;
        display: block;

        height: auto;
        transition: .5s ease;
        backface-visibility: hidden;
    }

    .middle {
        transition: .5s ease;
        opacity: 0;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        text-align: center;
    }

    .widget-user-image:hover #adminimageedit {
        opacity: 0.3;
    }

    .widget-user-image:hover .middle {
        opacity: 1;
    }



</style>


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Profile section
            <small>edit profile here</small>
        </h1>
        <ol class="breadcrumb">
            <li><a><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>Profile</a></li>

        </ol>
    </section>





    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-4">
                <!-- Widget: user widget style 1 -->
                <div class="box box-widget widget-user-2">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-blue">
                        <div id="propictriger" class="widget-user-image">

                            <img data-toggle="tooltip" data-placement="top" title="Change image" class="img-circle" id="adminimageedit" src="<?=base_url()?>assets/images/admin/<?=$this->session->userdata('sess_admin_photo');?>" alt="admin image">

                        </div>
                        <!-- /.widget-user-image -->
                        <h2 class="widget-user-username"><?=$this->session->userdata('sess_admin_firstname')."&nbsp;".$this->session->userdata('sess_admin_lastname');?></h2>
                        <h5 class="widget-user-desc">Admin</h5>
                    </div>
                    <div class="box-footer no-padding">
                        <ul class="nav nav-stacked">

                            <li><a>Username<span class="pull-right badge bg-gray"><?=$this->session->userdata('sess_admin_username');?></span></a></li>
                            <li><a>Firstname <span class="pull-right badge bg-gray"><?=$this->session->userdata('sess_admin_firstname');?></span></a></li>
                            <li><a>Lastname <span class="pull-right badge bg-gray"><?=$this->session->userdata('sess_admin_lastname');?></span></a></li>
                            <li><a>Last logdate <span class="pull-right badge bg-gray"><?=$this->session->userdata('sess_admin_logdate');?></span></a></li>

                        </ul>
                    </div>
                </div>
                <!-- /.widget-user -->
            </div>

            <form name="propicform" id="propicform" >
                <input type="file" name="propic" id="propic" style="display:none;visiblity:hidden;z-index:99999;">
            </form>


        <!-- Default box -->
        <div class="col-md-8">
            <!-- general form elements -->
            <div class="box">
                <div class="box-header with-border">

                    <h3 class="box-title">Edit profile details</h3>
                    <h5 class="pull-right" style="text-decoration: underline">
                        <a id="changepasswordlink" href="javascript:void(0)" style="display: none" onclick="loadforms(2);">Change password</a>
                        <a id="editprofilelink" href="javascript:void(0)" style="display: none" onclick="loadforms(1);">Edit profile</a>
                    </h5>

                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div id="profileeditdiv" style="display: none">
                <form role="form" action="javascript:void(0)" id="adminprofileeditform" method="post">
                    <div class="box-body">

                        <div class="form-group">
                            <label for="usernameedit">Username :</label>
                            <input type="text" class="form-control" id="usernameedit" value="<?=$this->session->userdata('sess_admin_username');?>" name="usernameedit" placeholder="Enter username">
                        </div>

                        <div class="form-group">
                            <label for="firstnameedit">Firstname :</label>
                            <input type="text" class="form-control" id="firstnameedit" name="firstnameedit" value="<?=$this->session->userdata('sess_admin_firstname');?>" placeholder="Enter firstname">
                        </div>

                        <div class="form-group">
                            <label for="lastnameedit">Lastname :</label>
                            <input type="text" class="form-control" id="lastnameedit" name="lastnameedit" value="<?=$this->session->userdata('sess_admin_lastname');?>"  placeholder="Enter lastname">
                        </div>



                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="reset" class="btn btn-primary pull-left">Reset</button>
                        <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </div>
                </form>
                </div>



                <div id="passwordeditdiv" style="display: none">
                    <form role="form" action="javascript:void(0)" id="adminpasswordform" method="post">
                        <div class="box-body">

                            <div class="form-group">
                                <label for="oldpass">Old password :</label>
                                <input type="text" class="form-control" id="oldpass"  name="oldpass" placeholder="Enter old password">
                            </div>

                            <div class="form-group">
                                <label for="newpass">New password :</label>
                                <input type="text" class="form-control" id="newpass" name="newpass"  placeholder="Enter new password">
                            </div>

                            <div class="form-group">
                                <label for="rnewpass">Repeat password :</label>
                                <input type="text" class="form-control" id="rnewpass" name="rnewpass"  placeholder="Repeat password">
                            </div>



                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="reset" class="btn btn-primary pull-left">Reset</button>
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>

    $(function () {
        uploadpicture();
        savephoto();
    });
</script>

<script>
    var uploadpicture=function(){  $("#propictriger").on("click",function(){   $('#propic').trigger('click');    });};

    var savephoto=function()
    {
        $("#propic").change(function(event)
        {

            var data=new FormData(document.getElementById("propicform"));

            $.ajax({url:site_url+'Dashboard/uploadprofilepic',
                type: 'POST',
                data: data,
                processData: false,
                contentType: false,
                enctype: 'multipart/form-data',

                success: function (data) {


                $("#custom_messages").html(data.message);
                $('#messagebox').modal({backdrop: 'static', keyboard: false});}
            });
            $('#messagebox').on('click', function () {
                location.reload(true);
            });
        });
    };
</script>


<script>



        var loadforms=function(no){

        if(no===1){$("#profileeditdiv").show();$("#passwordeditdiv").hide(); $("#changepasswordlink").show(); $("#editprofilelink").hide();}
        if(no===2){$("#profileeditdiv").hide();$("#passwordeditdiv").show(); $("#changepasswordlink").hide(); $("#editprofilelink").show();}

        }

</script>

<script>



    $.validator.setDefaults({

        // errorClass:'help-block',
        highlight: function (element) {
            $(element)
                .closest('.form-group')
                .addClass('has-error');

        },
        unhighlight: function (element) {
            $(element)
                .closest('.form-group')
                .removeClass('has-error')
                .addClass('has-success');

        }
    });


    $("#adminpasswordform").validate({

        rules: {


            oldpass: {required: true},
            newpass:{required:true},
            rnewpass:{required:true,equalTo:"#newpass"}

        },

        messages: {

            oldpass: {required: "Please enter your old password"},
            newpass:{required:"Please enter  new password"},
            rnewpass:{required:"Repeat new password",equalTo:'Both passwords must be equal'}
        }

    });


    $("#adminprofileeditform").validate({

        rules: {


            usernameedit: {required: true},
            firstnameedit:{required:true},
            lastnameedit:{required:true}

        },

        messages: {

            usernameedit: {required: "Please enter username"},
            firstnameedit:{required:"Please enter  firstname"},
            lastnameedit:{required:"Please enter lastname"}


        }

    });


    $("#adminprofileeditform").submit(function (event) {

        event.preventDefault();
        if(!$("#adminprofileeditform").valid()){return false;}

        $.post(site_url+"Dashboard/updateAdmin",{usernameedit:$("#usernameedit").val(),firstnameedit:$("#firstnameedit").val(),lastnameedit:$("#lastnameedit").val()},function (data) {

            $("#custom_messages").html(data.message);
            $('#messagebox').modal({backdrop: 'static', keyboard: false});

        });
    });



    $("#adminpasswordform").submit(function (event) {

        event.preventDefault();
        if(!$("#adminpasswordform").valid()){return false;}

        $.post(site_url+"Dashboard/updateAdminpassword",{oldpass:$("#oldpass").val(),rnewpass:$("#rnewpass").val()},function (data) {


            $("#custom_messages").html(data.message);
            $('#messagebox').modal({backdrop: 'static', keyboard: false});

            $("#adminpasswordform")[0].reset();

        });
    });


</script>

<script>
    $("#profileli").addClass('active');
</script>