
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Users list
            <small>view / block</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url();?>Dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>Users</a></li>

        </ol>
    </section>
    <section class="content">
   
        <div class="row">
        
       

            <div class="col-md-12">
            
                <div class="box box-solid">
               
                    <form>
                  
                        <div class="box-header with-border">

                          
                          
                            <h2 class="page-header"> <i class="fa fa-institution"></i> Users Search Options</h2>
                           
                          
                            <div class="" id="collegepages">
                              </div>
                             
                            
                            

                            <div class="col-md-3">
                                <div class="has-feedback">
                                    <input type="text" class="form-control input-sm" id="search"  placeholder="Search users">
                                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                </div>
                            </div>


                            <div class="col-md-5 bg-gray-ligh border-left border-right" style="padding:3px;">

                                <div class="form-group">

                                    <div class="col-lg-4"><label><input type="radio"  name="option" id="option" class="minimal" value="0" checked> Active</label></div>

                                    <div class="col-lg-4"><label><input type="radio" name="option" id="option" class="minimal" value="1"> Blocked </label></div>


                                </div>
                            </div>


                            
                            <div class="col-md-4 bg-gray-ligh border-left border-right" style="padding:3px;">


                                <select class="form-control" id="userselectparty">
                                    
                                    <option value="">Select party</option>
                                    <?php
                                    $res=$this->PartyModel->getPartylist();
                                    foreach ($res as $value){
                                    ?>
                                    
                                    
                                    <option value="<?=$value['party_id']?>"><?=$value['party_name']?></option>
                                    
                                    <?php
                                    }
                                    ?>
                                    
                                </select>

                            </div>



                                    
                            <div class="col-md-3">
                            <label>select date range:</label>
                                <div class="has-feedback">
                                    <input type="text" name="daterange" class="form-control input-sm" id="selectdaterange"  placeholder="select date">
                                    <!-- <button type="button" id="daterangeget" class="btn btn-info" onclick="getcolleges()">GET</button>
                          -->
                                </div>
                            </div>

                            <div class="form-group col-md-3">
                                <label>select country:</label>
                                <select class="form-control" id="selectcountry">
                                    <option value="">select country</option>
                                   <?php
                                    
                                    $res = $this->UserModel->getCountry();
                                    foreach($res as $val => $key){
                                        ?>

                                         <option value = "<?=$key['id']?>"><?=$key['name']?></option>

                                        <?php
                                    }
                                   ?>
                                </select>
                            </div>    

                             <div class="form-group col-md-3">
                                <label>select state:</label>
                                <select class="form-control" id="state">
                                    <option value="">select state</option>
                                   
                                </select>
                            </div>  
                            <div class="form-group col-md-3">
                                <label>select district:</label>
                                <select class="form-control" id="district">
                                    <option value="">select district</option>
                                   
                                </select>
                            </div>        



               
                            


                            <!-- <div class="col-md-5 bg-gray-ligh border-left border-right" style="padding:3px;">

                                <div class="form-group  ">

                                    <div class="col-lg-4"><label><input type="radio"  name="option" id="option" class="minimal" value="0" checked> Active</label></div>

                                    <div class="col-lg-4"><label><input type="radio" name="option" id="option" class="minimal" value="1"> Blocked </label></div>


                                </div>
                            </div> -->



                            
                            <!-- <div class="col-md-4 bg-gray-ligh border-left border-right" style="padding:3px;">


                                <select class="form-control" id="userselectparty">
                                    
                                    <option value="">Select party</option>
                                    <?php
                                    $res=$this->PartyModel->getPartylist();
                                    foreach ($res as $value){
                                    ?>
                                    
                                    
                                    <option value="<?=$value['party_id']?>"><?=$value['party_name']?></option>
                                    
                                    <?php
                                    }
                                    ?>
                                    
                                </select>

                            </div> -->



                            <div class="col-md-4" id="collegepage">

                            </div>

                        </div>

                    </form>
                    &nbsp;&nbsp;<label class="text-center text-green" id="ccustom_message" ></label>
                    <div class="box-body" id="collegebody">
                    </div>
                </div>
            </div>
        </div>
    
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #005384">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: white">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel" style="color: white">  <i class="fa fa-user"></i> &nbsp; User details</h4>


            </div>
            <div class="modal-body">
                <section class="invoice">
                    
                   
                    
                 
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-blue">
              <div class="widget-user-image">
                <img class="img-circle" src="" id="userimagedetailed" alt="user image">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username" id="namedetailed"></h3>
              <h5 class="widget-user-desc" id="statedetailed2"></h5>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a>DOB <span class="pull-right" id="dobdetailed"></span></a></li>
                <li><a>Country <span class="pull-right" id="countrydetailed"></span></a></li>
                <li><a>States <span class="pull-right" id="statedetailed"></span></a></li>
                <li><a>City <span class="pull-right" id="citydetailed"></span></a></li>
                <li><a>Created on <span class="pull-right" id="createddatedetailed"></span></a></li>
                <li><a>Last login <span class="pull-right" id="lastlogindetailed"></span></a></li>
                <li><a>Total posts <span class="pull-right badge bg-green" id="postcountdetailed"></span></a></li>
                
                <li><a>Followers <span class="pull-right badge bg-red" id="followerscountdetailed"></span></a></li>
           
              </ul>
            </div>
          </div>
          <!-- /.widget-user -->
        
                    
                    
                </section>
            </div>
            <div class="modal-footer" style="background-color: #005384">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
        </div>
    </div>
</div>
        
        
        


    <!-- Main content -->
    <section class="content">

        <div class="row" id="userslist">


        </div>

       <div id='collegepagess'></div>



        <!-- /.box -->

    </section>
    </section>
    <!-- /.content -->
</div>


<script>
var getuserdetails=function(id){
    
   $.post(site_url+"Users/getuserDetails",{userid:id},function(data){
       
 
       
            $("#userimagedetailed").attr("src",data.content.users_photo);
            $("#postcountdetailed").html(data.posts);
            $("#followerscountdetailed").html(data.followers);
        
            $("#namedetailed").html(data.content.users_name);
            $("#dobdetailed").html(data.content.users_dob);
            $("#countrydetailed").html(data.content.users_country);
            $("#statedetailed").html(data.content.users_state);
            $("#statedetailed2").html(data.content.users_state);
            $("#citydetailed").html(data.content.users_city);
            $("#areadetailed").html(data.content.users_area);
            $("#createddatedetailed").html(data.content.users_created_date);
            $("#lastlogindetailed").html(data.content.users_login_date);
            
            
   });
    $('#myModal').modal({  keyboard: false,backdrop:'static'});
}
</script>
<script>

    $(function () {

        $('.minimal').on('ifChecked ', function(event)
        {
            var v=$("input[name='option']:checked").val();
            getcolleges(1);
        });




        getcolleges(1);


        $("#search").keyup(function () {

           getcolleges(1);
        });
    });
</script>

<script>
    $("#userselectparty").change(function(){
        
       
       getcolleges();
    });
</script>


<script>



        var getcolleges = function (page) {
          
            var option              = $("input[name='option']:checked").val();
            var selected            = $("#userselectparty").val();
            var daterange           = $("#selectdaterange").val();
        
            var country             = $("#selectcountry").val();
            var state               = $("#state").val();
            var district            = $("#district").val();

            var search=$("#search").val();


            var my_page=1;
            if(page!==null&&page!==undefined&&page!=="") {my_page=page;}

   
            $.post(site_url+"Users/getallUsers",{page:my_page,country:country,state:state,district:district,daterange:daterange,search:search,option:option,selected:selected},function (data) {
            var dat="";
             

          

            if(data.error == 0){


              var content=data.content;
              for(var datas in content){

                  var mydata=content[datas];

                  dat+='<div class="col-md-4">';

                  dat+='<div class="custom-css box box-widget widget-user-2">';

                  dat+='<div class="widget-user-header bg-blue">';
                  dat+='<div class="widget-user-image">';

                  dat+='<img class="img-circle" src="'+mydata.users_photo+'" alt="user image">';
                  dat+='</div>';

                  dat+='<h3 class="widget-user-username">'+mydata.users_name+'</h3>';
                  dat+='<h5 class="widget-user-desc">'+mydata.party_name+'</h5>';
                  dat+='</div>';
                  dat+='<div class="box-footer no-padding">';
                  dat+='<ul class="nav nav-stacked">';
                  dat+='<li><a >Name <span class="pull-right">'+mydata.users_name+'</span></a></li>';
                  dat+='<li><a >Email <span class="pull-right">'+mydata.users_email+'</span></a></li>';
                  dat+='<li><a >Mobile <span class="pull-right">'+mydata.users_mobile+'</span></a></li>';
                  dat+='<li  color:"red"><a style="color:#444;text-decoration:none" href="javascript:void(0)">view details<span class="pull-right badge bg-blue" onclick=getuserdetails('+mydata.user_id+') >view all</span></a></li>';
                  
                  if(mydata.users_active == 0)  
                  dat+='<li  color:"red"><a style="color:#444" href="javascript:void(0)">Block user<span class="pull-right badge bg-green" onclick=unblockuserbyid('+mydata.user_id+') >unblock</span></a></li>';

                  else
                  dat+='<li  color:"red"><a style="color:#444" href="javascript:void(0)">Unblock user<span class="pull-right badge bg-red" onclick=blockuserbyid('+mydata.user_id+') >block</span></a></li>'; 
                  
                  
                  dat+='</ul>';
                  dat+='</div>';
                  dat+='</div>';

                  dat+='</div>';

              }


              var page_cotent='';var page_count=data.page_count;

          
    if(my_page > 1)
    {
       
    page_cotent+=' <ul class="pagination pagination-sm  pull-right" style="margin:0px;"><li><a  href="javascript:void(0);" onclick="getcolleges('+(my_page-1)+')">«&nbsp;Prev</a></li>';
    }
    if(my_page < page_count)
    {
         
    page_cotent+=' <ul class="pagination pagination-sm  pull-right" style="margin:0px;"><li><a  href="javascript:void(0);" onclick="getcolleges('+(my_page+1)+')">Next&nbsp;»</a></li>';
    }   

    $("#collegepagess").html(page_cotent);
          }
          else{
                dat+='<div class="col-md-12">';
                dat+='<div class="alert alert-danger alert-dismissible">';
                dat+='<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                dat+='<h4><i class="icon fa fa-ban"></i> Alert!</h4>';
                dat+='No data found in the server';
                dat+='</div>';
                dat+='</div>';


            }

            $("#userslist").html(dat);
           
        });
        $('input[name="daterange"]').val('');
        
    };
</script>
<script>
//iCheck for checkbox and radio inputs
$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
checkboxClass: 'icheckbox_minimal-blue',
radioClass   : 'iradio_minimal-blue'
});
</script>

<script>
    $("#usersli").addClass('active');
</script>

<script>
var blockuserbyid = function(id){

$.post(site_url+"Users/blockuser",{userid:id},function(data){


            $("#custom_messages").html(data.message);
            $("#messagebox").modal({  keyboard: false,backdrop:'static'});
            getcolleges();
});
}
</script>


<script>
var unblockuserbyid = function(id){

$.post(site_url+"Users/unblockuser",{userid:id},function(data){


            $("#custom_messages").html(data.message);
            $("#messagebox").modal({  keyboard: false,backdrop:'static'});
            getcolleges();
});
}
</script>


<script>
$(function() {

    

  $('input[name="daterange"]').daterangepicker({
    opens: 'right'
  });
});
</script>


<script>
$("#selectcountry").change(function(){


$.post(site_url+"Users/getstatebyid" , {cid:$("#selectcountry").val()} , function(data){
    
    var dat = '<option value ="">Choose state</option>';
if(data.error == 0){

   
    var values = data.values;
    for(var dats in values){
        var mydata = values[dats];
        dat += '<option value ="'+mydata.id+'">'+mydata.name+'</option>';
    }
   
}
$("#state").html(dat);
});
getcolleges();
});
</script>


<script>
$("#state").change(function(){

$.post(site_url+"Users/getdistrictbyid" , {cid:$("#state").val()} , function(data){
    var dat = '<option value ="">Choose district</option>';
if(data.error == 0){

   
    var values = data.values;
    for(var dats in values){
        var mydata = values[dats];
        dat += '<option value ="'+mydata.name+'">'+mydata.name+'</option>';
    }
   
}
$("#district").html(dat);
});
getcolleges();
});
</script>

<script>
$("#district").change(function(){
    getcolleges();
});
</script>


<script>
    $(document).ready(function(){
       
        $('input[name="daterange"]').val("");
    })

    $("#selectdaterange").on("change",function(){
        //getcolleges();
    })
</script>





