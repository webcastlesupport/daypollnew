<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small>View posts reports</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>challenge reports</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header bg-blue">
                        <h3 class="box-title">Challenge post Report</h3>
                    </div>
                    <br>
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SL.NO</th>
                                    <th>Challenge Title</th>
                                    <th>Created By</th>
                                    <th style="width: 150px !important;">Created Date</th>
                                    <th style="width: 150px !important;">Expire Date</th>
                                    <th style="width: 150px !important;">Expire Time</th>
                                    <th style="width: 150px !important;">Reported</th>
                                    <th style="width: 150px !important;">Polled</th>
                                    <th style="width: 150px !important;">Posts</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $i = 1;
                                    foreach ($challenges as $row) {
                                ?>
                                    <tr>
                                        <td><?= $i++;  ?></td>
                                        <td><?= $row->challenge_title ?></td>
                                        <td><?= $row->username ?></td>
                                        <td><?= date('d/m/Y', strtotime($row->challenge_created_date)) ?></td>
                                        <td><?= date('d/m/Y', strtotime($row->challenge_expired_date)) ?></td>
                                        <td><?= date('h:i A', strtotime($row->challenge_expired_time)) ?></td>
                                        <td><label class="label label-success"><?= $row->reportCount ?></label></td>
                                        <td><label class="label label-info"><?= $row->pollCount ?></label></td>
                                        <td><label class="label label-success"><?= $row->postCount ?></label></td>
                                    </tr>
                                <?php
                                    }
                                ?>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- Control Sidebar -->
<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<script>
    $("#reports").addClass('active');
    $("#challenge-report").addClass('active');
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })
</script>