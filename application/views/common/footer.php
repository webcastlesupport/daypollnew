
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.4.0
    </div>
    <strong><a href="<?= base_url();?>">Daypolls</a>.</strong> All rights
    reserved.
</footer>

<div class="modal fade" id="myModaltrending" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #005384">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: white">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel" style="color: white">  <i class="fa fa-user"></i> &nbsp; Edit Poll Count</h4>

            </div>
            <div class="modal-body">
            <section class="invoice">
                
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
           
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li>
                <form action="javascript:void(0)" id="editTrendingpostForm" method="post">
               
                <label>PollCount:</label>
                <input type ="text" id="pollcountedit" name="pollcountedit" class="form-control" placeholder="Enter poll count">
                </form>
                </li>
               </ul>
            </div>
          </div>
          <!-- /.widget-user -->
        
                    
                    
                </section>
            </div>
            <div class="modal-footer" style="background-color: #005384">
            <span style="color:white;font-size:16px" class="pull-left" id="trendingpostmsg"></span>
                <button type="button" id="pollcounteditbtn" class="btn bg-green btn-flat">Update</button>
                <button type="button" id="dissmissmodal" class="btn btn-flat" data-dismiss="modal">Cancel</button>

            </div>
        </div>
    </div>
</div>




<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>


<script>
$("#trendingli").click(function(){
   
    $.ajax({
        type : "POST",
        url  : "<?=base_url();?>Posts/getPollCount",
        success:function(data){
            console.log(data);
            $("#pollcountedit").val(data.count);
        } 
        });
    

    $("#myModaltrending").modal({  keyboard: false,backdrop:'static'});
});
</script>

<script>
$("#pollcounteditbtn").click(function(){

    $.ajax({
        type : "POST",
        url  : "<?=base_url();?>Posts/updatePollCount",
        data : {count:$("#pollcountedit").val()},
        success : function(data){
            $("#trendingpostmsg").html(data.msg);
            setTimeout(function(){ 
                $("#dissmissmodal").click();
                location.reload();
                }, 1000);
        }
    });

});    
</script>



<script type="text/javascript">
    // To make Pace works on Ajax calls
    $(document).ajaxStart(function () {
        Pace.restart()
    })
    $('.ajax').click(function () {
        $.ajax({
            url: '#', success: function (result) {
                $('.ajax-content').html('<hr>Ajax Request Completed !')
            }
        })
    })
</script>



<script>

    $("#logoutbtn").click(function () {

        $("#custom_message").html('Are you sure !!');
        $('#alertmodal').modal({backdrop: 'static', keyboard: false});
    });

</script>


<script src="<?=base_url();?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url();?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url();?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?=base_url();?>assets/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url();?>assets/dist/js/demo.js"></script>
</body>
</html>
