<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Daypoll | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/dist/css/skins/_all-skins.min.css?vb">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <!-- Morris chart -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/bower_components/morris.js/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/bower_components/jvectormap/jquery-jvectormap.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <!-- Daterange picker -->
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/pace/pace.min.css">
        <script src="<?= base_url(); ?>assets/bower_components/PACE/pace.min.js"></script>
        <!-- jQuery 3 -->
        <script src="<?= base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="<?= base_url(); ?>assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script src="<?= base_url(); ?>assets/dist/js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>assets/bower_components/moment/min/moment.min.js"></script>
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/iCheck/all.css">
        <script src="<?= base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?= base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?= base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <link rel="stylesheet" href="<?= base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]-->
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <!--[endif]-->
        <script>
            var site_url = "<?= base_url(); ?>";
        </script>
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a style = "color:#79c1ff;text-decoration:none" href="<?= base_url(); ?>" class="logo">

                    <span class="logo-mini"><b>D</b>P</span>

                    <span class="logo-lg"><b>Day</b>POLL</span>
                </a>
                <nav class="navbar navbar-static-top">
                    <a href="#" id="headerbartoggle" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="tasks-menu">
                                <a style="color:#79c1ff">
                                    <i class="">Last login : <?= $this->session->userdata('sess_admin_logdate'); ?></i>
                                </a>
                            </li>
                            <li class="user user-menu" id="logoutbtn">
                                <a data-toggle="tooltip" data-placement="bottom" title="Logout" href="javascript:void(0)" >
                                    <img src="<?= base_url(); ?>assets/images/admin/<?= $this->session->userdata('sess_admin_photo'); ?>" class="user-image" alt="User Image">
                                    <span class="hidden-xs"> Logout </span>
                                </a>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <div class="modal modal-primary" id="alertmodal">
                <div class="modal-dialog">
                    <div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button><h4 class="modal-title" id="custom_heading"></h4></div>
                        <div class="modal-body text-center"><p id="custom_message"></p></div>
                        <div class="modal-footer "><button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancel</button><a style="color: white" href="<?= base_url(); ?>Dashboard/logout"><button type="button" class="btn btn-primary pull-right">OK</button></a> </div>
                    </div>
                </div>
            </div>
            <div class="modal modal-primary" id="messagebox">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                            <h4 class="modal-title" id="custom_heading"></h4>
                        </div>
                        <div class="modal-body text-center"><p id="custom_messages"></p></div>
                        <div class="modal-footer "><button type="button" class="btn btn-primary pull-right" id="dismmissbtn" data-dismiss="modal">OK</button></a> </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(function (){
                    function reposition() {
                        var modal = $(this), dialog = modal.find('.modal-dialog');
                        modal.css('display', 'block');
                        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
                    }
                    $('.modal').on('show.bs.modal', reposition);
                    $(window).on('resize', function () {
                        $('.modal:visible').each(reposition);
                    });
                });
            </script>
            <script>
                $(document).ready(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                });
            </script>