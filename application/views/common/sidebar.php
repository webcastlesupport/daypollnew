<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
        </div>
        <ul class="sidebar-menu" data-widget="tree">
            <li id="dashboardli">
                <a data-toggle="tooltip" data-placement="right" title="Dashboard" href="<?= base_url(); ?>Dashboard">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    <span class="pull-right-container">

                    </span>
                </a>
            </li>
            <li id="usersli">
                <a data-toggle="tooltip" data-placement="right" title="Dashboard" href="<?= base_url(); ?>Users/users">
                    <i class="fa fa-user-circle-o"></i> <span>Users</span>
                    <span class="pull-right-container">

                    </span>
                </a>
            </li>
            <li id="profileli">
                <a data-toggle="tooltip" data-placement="right" title="Edit profile" href="<?= base_url(); ?>Dashboard/profile">
                    <i class="fa fa-gear"></i> <span>Account settings</span>
                    <span class="pull-right-container">

                    </span>
                </a>
            </li>
            <li id="postsul" class="treeview">
                <a href="#">
                    <i class="fa fa-television"></i>
                    <span>Posts</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li id="postsli"><a data-toggle="tooltip" href="<?= base_url(); ?>Posts/posts"><i class="fa fa-circle-o"></i> All posts</a></li>
                    <li id="blockedpostsli"><a href="<?= base_url(); ?>Posts/blockedposts"><i class="fa fa-circle-o"></i> Blocked posts</a></li>
                    <li id="reportpostsli"><a href="<?= base_url(); ?>Posts/postsreports"><i class="fa fa-circle-o"></i> Posts reports</a></li>
                </ul>
            </li>
            <li id="partyul" class="treeview">
                <a href="#">
                    <i class="fa fa-address-card"></i>
                    <span>Party</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li id="partyli"><a data-toggle="tooltip" href="<?= base_url(); ?>Party/party"><i class="fa fa-circle-o"></i> Party list</a></li>
                    <li id="partyclaimli"><a href="<?= base_url(); ?>PartyClaim/partyclaim"><i class="fa fa-circle-o"></i> Claim party</a></li>
                    <li id="partypostsli"><a data-toggle="tooltip" href="<?= base_url(); ?>Party/partyposts"><i class="fa fa-circle-o"></i> Party Posts</a></li>
                    <li id="blockedpartypostsli"><a data-toggle="tooltip" href="<?= base_url(); ?>Party/Blockedpartyposts"><i class="fa fa-circle-o"></i> Blocked Party Posts</a></li>
                    <li id="partyrestrictedli"><a data-toggle="tooltip" href="<?= base_url(); ?>Party/Partyrestrictednations"><i class="fa fa-circle-o"></i> Party Restricted nations</a></li>
                </ul>
            </li>
            <li id="groupul" class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Group</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li id="groupli"><a data-toggle="tooltip" href="<?= base_url(); ?>Group/group"><i class="fa fa-circle-o"></i> Group list</a></li>
                    <li id="blockedgrouppostsli"><a data-toggle="tooltip" href="<?= base_url(); ?>Group/blockedgroupposts"><i class="fa fa-circle-o"></i> Blocked group posts</a></li>
                </ul>
            </li>
            <li id="businessprofileul" class="treeview">
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>Business profile</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li id="bppostsli"><a data-toggle="tooltip" href="<?= base_url(); ?>BusinessProfile/businessprofileposts"><i class="fa fa-circle-o"></i> Posts</a></li>
                    <li id="bpreportpostsli"><a data-toggle="tooltip" href="<?= base_url(); ?>BusinessProfile/businessprofilepostsreport"><i class="fa fa-circle-o"></i> Posts report</a></li>
                    <li id="bpchallengeli"><a data-toggle="tooltip" href="<?= base_url(); ?>BusinessProfile/challenge"><i class="fa fa-circle-o"></i> Challenges</a></li>
                    <li id="bpchallengereportli"><a data-toggle="tooltip" href="<?= base_url(); ?>BusinessProfile/businessprofilechallengereport"><i class="fa fa-circle-o"></i> Challenge reports</a></li>
                    <li id="categoryli"><a data-toggle="tooltip" href="<?= base_url(); ?>Category/Category"><i class="fa fa-circle-o"></i> Category</a></li>
                    <li id="subcategoryli"><a data-toggle="tooltip" href="<?= base_url(); ?>Category/SubCategory"><i class="fa fa-circle-o"></i> Sub Category</a></li>
                    <li id="subcategory2li"><a data-toggle="tooltip" href="<?= base_url(); ?>Category/SubCategory2"><i class="fa fa-circle-o"></i> Sub Category 2</a></li>
                    <li id="subcategory3li"><a data-toggle="tooltip" href="<?= base_url(); ?>Category/SubCategory3"><i class="fa fa-circle-o"></i> Sub Category 3</a></li>
                    <li id="businessprofileli"><a data-toggle="tooltip" href="<?= base_url(); ?>BusinessProfile/businessprofile"><i class="fa fa-circle-o"></i> Business profile</a></li>
                </ul>
            </li>
            <li id="challengeli">
                <a data-toggle="tooltip" data-placement="right" title="View Challenge" href="<?= base_url(); ?>Challenge/challenge">
                    <i class="fa fa-trophy"></i><span>Challenge</span>
                </a>
            </li>
            <li id="feedbackli">
                <a data-toggle="tooltip" data-placement="right" title="Feedback" href="<?= base_url(); ?>Feedback/feedback">
                    <i class="fa fa-comments"></i><span>Feedback</span>
                    <span class="pull-right-container">

                    </span>
                </a>
            </li>
            <li id="reports" class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart"></i>
                    <span>Reports</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li id="challenge-report"><a data-toggle="tooltip" href="<?= base_url(); ?>Reports/challenges"><i class="fa fa-circle-o"></i> Challenges</a></li>
                    <li id="group-post-report"><a data-toggle="tooltip" href="<?= base_url(); ?>Reports/groupPosts"><i class="fa fa-circle-o"></i> Group Posts</a></li>
                    <li id="party-report"><a data-toggle="tooltip" href="<?= base_url(); ?>Reports/partyPosts"><i class="fa fa-circle-o"></i> Party Posts</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>