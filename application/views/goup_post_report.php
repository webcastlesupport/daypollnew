<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small>View Group Post reports</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>Group Post reports</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header bg-blue">
                        <h3 class="box-title">Group post Report</h3>
                    </div>
                    <br>
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SL.NO</th>
                                    <th>Group Name</th>
                                    <th>Created Date</th>
                                    <th>Created By</th>
                                    <th style="width: 150px !important;">Posts</th>
                                    <th style="width: 150px !important;">Followers Count</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $i = 1;
                                    foreach ($groups as $row) {
                                ?>
                                    <tr>
                                        <td><?= $i++;  ?></td>
                                        <td><?= $row->group_name ?></td>
                                        <td><?= date('d/m/Y',strtotime($row->group_created_date)) ?></td>
                                        <td><?= $row->username ?></td>
                                        <td><label class="label label-success"><?= $row->postCount ?></label></td>
                                        <td><label class="label label-success"><?= $row->followerCount ?></label></td>
                                    </tr>
                                <?php
                                    }
                                ?>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- Control Sidebar -->
<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<script>
    $("#reports").addClass('active');
    $("#group-post-report").addClass('active');
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })
</script>