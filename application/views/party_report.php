<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small>View party Post reports</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>Party Post reports</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header bg-blue">
                        <h3 class="box-title">Party post Report</h3>
                    </div>
                    <br>
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SL.NO</th>
                                    <th>Title</th>
                                    <th>Party</th>
                                    <th style="width: 150px !important;">Created Date</th>
                                    <th style="width: 150px !important;">Reports</th>
                                    <th style="width: 150px !important;">Polls</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $i = 1;
                                    foreach ($posts as $row) {
                                ?>
                                    <tr>
                                        <td><?= $i++;  ?></td>
                                        <td><?= $row->party_posts_title ?></td>
                                        <td><?= $row->partyName ?></td>
                                        <td><?= date('d/m/Y', strtotime($row->party_posts_uploaded_date)) ?></td>
                                        <td><label class="label label-success"><?= $row->reportCount ?></label></td>
                                        <td><label class="label label-info"><?= $row->pollCount ?></label></td>
                                    </tr>
                                <?php
                                    }
                                ?>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- Control Sidebar -->
<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<script>
    $("#reports").addClass('active');
    $("#party-report").addClass('active');
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })
</script>