<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h4>
            Party poll page
            <small>view party polls here</small>
        </h4>
        <ol class="breadcrumb">
        <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>party polls</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">


                    <div class="box-header bg-blue">
                        <h3 class="box-title">public poll list  &nbsp; </h3>
                       
                    </div>
                    <!-- /.box-header -->

                  
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>User photo</th>
                                <th>User name</th>
                               
                                
                            </tr>
                            </thead>
                          
                       
                        <?php 
                            $i = 1;
                            foreach($data['public'] as $row){
                        ?>
                         <tr>
                        <td><?=$i;?></td>
                        <td><img  width="70px;" src="<?=base_url();?>assets/images/users/<?=$row['users_photo']?>"></td>
                        <td><?=$row['users_name']?></td>
                        </tr>
                        <?php
                        
                        $i++;
                            }
                        ?>
                       
                            
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>



                <div class="box">


<div class="box-header bg-blue">
    <h3 class="box-title">secret poll list  &nbsp; </h3>
   
</div>
<!-- /.box-header -->


<div class="box-body">
    <table id="example2" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>User photo</th>
            <th>User name</th>
           
            
        </tr>
        </thead>
      
   
    <?php 
        $i = 1;
        foreach($data['secret'] as $row){
    ?>
     <tr>
    <td><?=$i;?></td>
    <td><img width="70px;" src="<?=base_url();?>assets/images/users/<?=$row['users_photo']?>"></td>
    <td><?=$row['users_name']?></td>
    </tr>
    <?php
      $i++;
    } 
  
    ?>
    
        
    </table>
</div>
<!-- /.box-body -->
</div>


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<!--details model-->


<div class="modal fade" id="myModaldetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #005384">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: white">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel" style="color: white">  <i class="fa fa-user"></i> &nbsp; User details</h4>


            </div>
            <div class="modal-body">
                <section class="invoice">
          
                 
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            
            
            <div class="widget-user-header bg-black" id="partycoverimage" style="background: url('../dist/img/photo1.png') center center;">
            
              <div class="widget-user-image">
                <img class="img-circle" src="<?=base_url();?>assets/images/party/none.png" id="partyimage" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username" id="partydetname"></h3>
             
              <br>
            </div>




            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a>Created on <span class="pull-right badge" id="partycreated"></span></a></li>
                <li><a>Active <span class="pull-right badge" id="partyactive"></span></a></li>
              
                <li><a>Approved <span class="pull-right badge" id="partyapproved"></span></a></li>
                <li><a>Public polls <span class="pull-right badge" id="partypublicpolls"></span></a></li>
                <li><a>Secret polls <span class="pull-right badge" id="partysecretpolls"></span></a></li>
                <li><a>Total joins<span class="pull-right badge" id="partyjoins"></span></a></li>
                <li><a href ="">View polled members<span class="pull-right badge bg-blue">view</span></a></li>                    
              
              </ul>
            </div>
          </div>
          <!-- /.widget-user -->
        
                    
                    
                </section>
            </div>
            <div class="modal-footer" style="background-color: #005384">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<form name="propicform" id="propicform" >
    <input type="text" value="" name="propicid" id="propicid" style="display:none;visiblity:hidden;z-index:99999;">
     <input type="file" name="propic" id="propic" style="display:none;visiblity:hidden;z-index:99999;">
</form>


<form name="coverpicform" id="coverpicform" >
    <input type="text" value="" name="coverpicid" id="coverpicid" style="display:none;visiblity:hidden;z-index:99999;">
     <input type="file" name="coverpic" id="coverpic" style="display:none;visiblity:hidden;z-index:99999;">
</form>



<!-- Modal -->
 
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header bg-blue-active">
<button type="button" class="close" id="subadmindismiss" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">×</span></button>
<h4 class="modal-title"><i class="fa  fa-user-plus"></i> Create party</h4>
</div>
<div class="modal-body">
<form action="javascript:void(0)" method="POST" name="partyform" id="partyform">


<div class="form-group ">
<label class="control-label" for="partyshortname"><i class="fa  fa-user"></i> Party short name</label>
<input type="text" class="form-control" name="partyshortname" id="partyshortname" placeholder="Enter party short name">
<span class="help-block"></span>
</div>

<div class="form-group ">
<label class="control-label" for="partyname"><i class="fa  fa-user"></i> Party name</label>
<input type="text" class="form-control" name="partyname" id="partyname" placeholder="Enter party name">
<span class="help-block"></span>
</div>


<div class="form-group ">
<label class="control-label" for="pname"><i class="fa  fa-user"></i> President name</label>
<input type="text" class="form-control" name="pname" id="pname" placeholder="Enter president name">
<span class="help-block"></span>
</div>


<div class="form-group ">
<label class="control-label" for="sname"><i class="fa  fa-user"></i> Secretary name</label>
<input type="text" class="form-control" name="sname" id="sname" placeholder="Enter secretary name">
<span class="help-block"></span>
</div>


<div class="form-group ">
<label class="control-label" for="email"><i class="fa  fa-user"></i> Email</label>
<input type="text" class="form-control" name="email" id="email" placeholder="Enter email">
<span class="help-block"></span>
</div>

<div class="form-group">
<label class="control-label" for="scope"><i class="fa  fa-user"></i> Scope </label>
<select class="form-control" name="scope" id="scope">
<option value="">Choose scope</option>

<option value="1">Country</option>
<option value="2">State</option>
<option value="3">District</option>
<!--<option value="4">Area</option> -->
</select>
</div>

<div class="form-group">
<label class="control-label" for="country"><i class="fa  fa-user"></i> Country </label>
<select class="form-control" name="country" id="country">
<option value="">Choose country</option>
<?php 
$res = $this->PartyModel->getCountry();
foreach($res as $val => $key){
    ?>
    <option value="<?= $res[$val]['id']?>"><?=$res[$val]['name']?></option>
    <?php
}
?>

</select>
</div>

<div class="form-group">
<label class="control-label" for="state"><i class="fa  fa-user"></i> State </label>
<select class="form-control" name="state" id="state">
<option value="">Choose state</option>

</select>
</div>


<div class="form-group">
<label class="control-label" for="district"><i class="fa  fa-user"></i> District </label>
<select class="form-control" name="district" id="district">
<option value="">Choose district</option>

</select>
</div>


<div class="modal-footer">
<button type="button" class="btn btn-default pull-left" data-dismiss="modal"><i class="fa  fa-close"></i> Cancel </button>
<button type="submit" class="btn btn-primary" id="save_button"><i class="fa  fa-floppy-o"></i> Save</button>
</div>
</form>

</div>
</div>

</div>
</div>







<!-- Edit Modal -->
 
<div class="modal fade" id="myeditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
<div class="modal-header bg-blue-active">
<button type="button" class="close" id="subadmindismiss2" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">×</span></button>
<h4 class="modal-title"><i class="fa  fa-user-plus"></i> Create party</h4>
</div>
<div class="modal-body">
<form action="javascript:void(0)" method="POST" name="editpartyform" id="editpartyform">

<div class="form-group ">
<label class="control-label" for="partyname"><i class="fa  fa-user"></i> Edit short name</label>
<input type="text" class="form-control" name="editpartyshortname" id="editpartyshortname" placeholder="Enter party name">
<span class="help-block"></span>
</div>

<div class="form-group ">
<label class="control-label" for="partyname"><i class="fa  fa-user"></i> Edit name</label>
<input type="text" class="form-control" name="editpartyname" id="editpartyname" placeholder="Enter party name">
<span class="help-block"></span>
</div>


<div class="form-group ">
<label class="control-label" for="pname"><i class="fa  fa-user"></i> President name</label>
<input type="text" class="form-control" name="editpname" id="editpname" placeholder="Enter president name">
<span class="help-block"></span>
</div>

<input type = "hidden" id="partyeditid" name = "partyeditid" value="">  
<div class="form-group ">
<label class="control-label" for="sname"><i class="fa  fa-user"></i> Secretary name</label>
<input type="text" class="form-control" name="editsname" id="editsname" placeholder="Enter secretary name">
<span class="help-block"></span>
</div>


<div class="form-group ">
<label class="control-label" for="email"><i class="fa  fa-user"></i> Email</label>
<input type="text" class="form-control" name="editemail" id="editemail" placeholder="Enter email">
<span class="help-block"></span>
</div>

<div class="form-group">
<label class="control-label" for="scope"><i class="fa  fa-user"></i> Scope </label>
<select class="form-control" name="editscope" id="editscope">
<option value="">Choose scope</option>

<option value="1">Country</option>
<option value="2">State</option>
<option value="3">District</option>
<!--<option value="4">Area</option> -->
</select>
</div>

<div class="form-group">
<label class="control-label" for="country"><i class="fa  fa-user"></i> Country </label>
<select class="form-control" name="editcountry" id="editcountry">
<option value="">Choose country</option>
<?php 
$res = $this->PartyModel->getCountry();
foreach($res as $val => $key){
    ?>
    <option value="<?= $res[$val]['id']?>"><?=$res[$val]['name']?></option>
    <?php
}
?>

</select>
</div>

<div class="form-group" id="editstatediv">
<label class="control-label" for="state"><i class="fa  fa-user"></i> State </label>
<select class="form-control" name="editstate" id="editstate">
<option value="">Choose state</option>

</select>
</div>



<div class="form-group" id="editstatestaticdiv">
<label class="control-label" for="email"><i class="fa  fa-user"></i> State</label>
<input type="text" class="form-control" readonly name="editstatestatic" id="editstatestatic">
<span class="help-block"></span>
</div>


<div class="form-group" id="editdistrictstaticdiv">
<label class="control-label" for="email"><i class="fa  fa-user"></i> District</label>
<input type="text" class="form-control" readonly name="editdistrictstatic" id="editdistrictstatic">
<span class="help-block"></span>
</div>

<div class="form-group" id="editdistrictdiv">
<label class="control-label" for="district"><i class="fa  fa-user"></i> District </label>
<select class="form-control" name="editdistrict" id="editdistrict">
<option value="">Choose district</option>

</select>
</div>


<div class="modal-footer">
<button type="button" class="btn btn-default pull-left" data-dismiss="modal"><i class="fa  fa-close"></i> Cancel </button>
<button type="submit" class="btn btn-primary" id="edit_save_button"><i class="fa  fa-floppy-o"></i> Update</button>
</div>
</form>

</div>
</div>
</div>
</div>
<!-- edit model ends here -->




<script>
var newentry=function(){$("#new_party_button").on("click",function(){
   
    $("#myModal").modal({keyboard:!1,backdrop:"static"})})}
</script>




<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
        })
    })
</script>


    
    
    <!-- /.content -->



<script>
 $("#partyul").addClass('active');
    $("#partyli").addClass('active');
</script>
