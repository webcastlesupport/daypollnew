<!DOCTYPE html>
<html lang="en">
<head>
   <title>Daypoll | Mobile App Landing HTML Templates</title>
   <meta charset="utf-8">
   <meta name="author" content="pixelstrap">
   <meta name="description" content="">
   <meta name="keywords" content="">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <meta http-equiv="X-UA-Compatible" content="IE=edge" />
   <!-- Fav icon -->
   <link rel="shortcut icon" href="<?=base_url();?>assets/postview/images/favicon.png">

   <!-- Font Family-->
   <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

   <!-- Font Awesome -->
   <link rel="stylesheet" href="<?=base_url();?>assets/postview/css/font-awesome.min.css" >

   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="<?=base_url();?>assets/postview/css/bootstrap.css">

 

   <!-- Style css-->
   <link rel="stylesheet" href="<?=base_url();?>assets/postview/css/style.css">

   <!-- Responsive css-->
   <link rel="stylesheet" href="<?=base_url();?>assets/postview/css/responsive.css">




</head>
<body data-spy="scroll" data-target=".navbar" data-offset="80">

<!-- Preloader -->
<div class="loader-wrapper">
   <div class="loader"></div>
</div>
<!-- Preloader end -->

<!-- Home Section start-->
   <div class="container">
      <div class="row justify-content-center">
         <div class="col-md-12">
            <div class="section-header">
               <div>
                  <img src="<?=base_url();?>assets/postview/images/logo-daypoll.png" class="img-fluid mx-auto d-block" alt="daypoll-app">
               </div>
            </div>
         </div>
         
         
         
         
    <?php
       $s3_url   = "https://s3.amazonaws.com/daypoll/"; 
       if($_GET['type']==0){
         
    ?>
       
<div class="card" style="width: 18rem;">
        
        <div class="card-header">
            <?=$value['posts_title'];?>
        </div>

  <?php if($value['photo_or_video'] == 0 ){?>       
  <img class="card-img-top" src="<?=$s3_url?>assets/images/posts/<?=$value['posts_photo_or_video']?>" alt="post image">
  <?php
  }
  elseif($value['photo_or_video'] == 1)
  {
  ?>
  
  <video style="padding:5px" width="285" height="200" controls>
  <source src="<?=$s3_url?>assets/images/posts/<?=$value['posts_photo_or_video']?>" type="video/mp4">
  <source src="<?=$s3_url?>assets/images/posts/<?=$value['posts_photo_or_video']?>" type="video/ogg">
  Your browser does not support the video tag.
   </video>
  <?php
   } 
   ?>
  <div class="card-body">
    <p class="card-text"><?=$value['posts_content'];?></p>
   <p class="card-text"><small class="text-muted"><?=date('d-m-Y  h:i A', strtotime($value['posts_uploaded_date']));?></small></p>
   <div class="fixed-divider"></div>
   <div class="counts">
      <p class="poll-count"><?=$pollcount?><span>polls</span></p>
      <p class="comment-count"><?=$commentcount?><span>comments</span></p>
   </div>
   <div class="dropdown-divider"></div>
   <a href="https://play.google.com/store/apps/details?id=com.daypoll&hl=en" class="btn btn-sm button-primary text-uppercase d-table mx-auto text-white mt-3">Download App</a>
  </div>
      </div>
   </div>
<!--Home Section End -->
<?php } ?>



<!------------------  BUSINESS PROFILE--------------------------->

<?php
       $s3_url   = "https://s3.amazonaws.com/daypoll/"; 
       if($_GET['type']==5){                             // business profile posts
       
    ?>
       
<div class="card" style="width: 18rem;">
        
        <div class="card-header">
            <?=$value['bp_posts_title'];?>
        </div>

  <?php if($value['bp_photo_or_video'] == 0 ){?>       
  <img class="card-img-top" src="<?=$s3_url?>assets/images/business_profile/<?=$value['bp_posts_photo_or_video']?>" alt="post image">
  <?php
  }
  elseif($value['bp_photo_or_video'] == 1)
  {
  ?>
  
  <video style="padding:5px" width="285" height="200" controls>
  <source src="<?=$s3_url?>assets/images/business_profile/<?=$value['bp_posts_photo_or_video']?>" type="video/mp4">
  <source src="<?=$s3_url?>assets/images/business_profile/<?=$value['bp_posts_photo_or_video']?>" type="video/ogg">
  Your browser does not support the video tag.
   </video>
  <?php
   } 
   ?>
  <div class="card-body">
    <p class="card-text"><?=$value['bp_posts_content'];?></p>
   <p class="card-text"><small class="text-muted"><?=date('d-m-Y  h:i A', strtotime($value['bp_posts_uploaded_date']));?></small></p>
   <div class="fixed-divider"></div>
   <div class="counts">
      <p class="poll-count"><?=$pollcount?><span>polls</span></p>
      <p class="comment-count"><?=$commentcount?><span>comments</span></p>
   </div>
   <div class="dropdown-divider"></div>
   <a href="https://play.google.com/store/apps/details?id=com.daypoll&hl=en" class="btn btn-sm button-primary text-uppercase d-table mx-auto text-white mt-3">Download App</a>
  </div>
      </div>
   </div>
<!--Home Section End -->
<?php } ?>


<!------------------------------------------------>



<?php 
       if($_GET['type']==6){
         
        
    ?>
       
<div class="card" style="width: 18rem;">
        
        <div class="card-header">
            <?=$value['challenge_title'];?>
        </div>
<?php
if($value['challenge_type'] == 1){ 
 
?>

<?php if($value['bp_challenge_posts_exp_media_type'] == 0 ){?>   
   
   <?php if($value['challenge_media_type'] == 1){ ?> 
      <img class="card-img-top" src="<?=$s3_url?>assets/images/business_profile/challenge/type1/<?=$value['bp_challenge_posts_sub_post']?>" alt="post image">
  
 
  <?php }
  else{
     ?>

   <video style="padding:5px" width="285" height="200" controls>
   <source src="<?=$s3_url?>assets/images/business_profile/challenge/type1/<?=$value['bp_challenge_posts_sub_post']?>" type="video/mp4">
   <source src="<?=$s3_url?>assets/images/business_profile/challenge/type1/<?=$value['bp_challenge_posts_sub_post']?>" type="video/ogg">
      Your browser does not support the video tag.
   </video>

  
     
     <?php
  }
  ?>
  <?php
  }
  elseif($value['bp_challenge_posts_exp_media_type'] == 1)
  {
  ?>
  
  <video style="padding:5px" width="285" height="200" controls>
  <source src="<?=$s3_url?>assets/images/business_profile/challenge/type1/<?=$value['bp_challenge_posts_sub_post']?>" type="video/mp4">
  <source src="<?=$s3_url?>assets/images/business_profile/challenge/type1/<?=$value['bp_challenge_posts_sub_post']?>" type="video/ogg">
  Your browser does not support the video tag.
   </video>
  <?php
   } 
   ?>     




  <div class="card-body">
    <p class="card-text"><?=$value['challenge_content']?></p>
   <p class="card-text"><small class="text-muted"><?=date('d-m-Y  h:i A', strtotime($value['challenge_created_date']));?></small></p>
   <div class="fixed-divider"></div>
   <div class="counts">
      <p class="poll-count"><?=$pollcount?><span>polls</span></p>
      <p class="comment-count"><?=$commentcount?><span>comments</span></p>
   </div>
   <div class="dropdown-divider"></div>
   <a href="https://play.google.com/store/apps/details?id=com.daypoll&hl=en" class="btn btn-sm button-primary text-uppercase d-table mx-auto text-white mt-3">Download App</a>
  </div>
      </div>
   </div>
<!--Home Section End -->
<?php } 



elseif($value['challenge_type'] == 2){
  
?>

<?php if($value['bp_challenge_posts_exp_media_type'] == 0 ){?> 
   
  <?php if($value['challenge_media_type'] == 1){ ?> 
  <img class="card-img-top" src="<?=$s3_url?>assets/images/business_profile/challenge/type2/<?=$value['bp_challenge_posts_sub_post']?>" alt="post image">
  <?php }
  else{
     ?>
   
   <video style="padding:5px" width="285" height="200" controls>
   <source src="<?=$s3_url?>assets/images/business_profile/challenge/type2/<?=$value['bp_challenge_posts_sub_post']?>" type="video/mp4">
   <source src="<?=$s3_url?>assets/images/business_profile/challenge/type2/<?=$value['bp_challenge_posts_sub_post']?>" type="video/ogg">
   
   Your browser does not support the video tag.
   </video>

     <?php
  }
  ?>
  <?php
  }
  elseif($value['bp_challenge_posts_exp_media_type'] == 1)
  {
  ?>
  
  <video style="padding:5px" width="285" height="200" controls>
  <source src="<?=$s3_url?>assets/images/business_profile/challenge/type2/<?=$value['bp_challenge_posts_sub_post']?>" type="video/mp4">
  <source src="<?=$s3_url?>assets/images/business_profile/challenge/type2/<?=$value['bp_challenge_posts_sub_post']?>" type="video/ogg">
  Your browser does not support the video tag.
   </video>
  <?php
   } 

   ?>     




  <div class="card-body">
    <p class="card-text"><?=$value['challenge_content']?></p>
   <p class="card-text"><small class="text-muted"><?=date('d-m-Y  h:i A', strtotime($value['challenge_created_date']));?></small></p>
   <div class="fixed-divider"></div>
   <div class="counts">
      <p class="poll-count"><?=$pollcount?><span>polls</span></p>
      <p class="comment-count"><?=$commentcount?><span>comments</span></p>
   </div>
   <div class="dropdown-divider"></div>
   <a href="https://play.google.com/store/apps/details?id=com.daypoll&hl=en" class="btn btn-sm button-primary text-uppercase d-table mx-auto text-white mt-3">Download App</a>
  </div>
      </div>
   </div>
<?php }
}
 ?>




<!----  challenge ends here ------------>



<?php 
       if($_GET['type']==1){
       
    ?>
       
<div class="card" style="width: 18rem;">
        
        <div class="card-header">
            <?=$value['party_posts_title'];?>
        </div>


        <?php if($value['photo_or_video'] == 0 ){?>       
  <img class="card-img-top" src="<?=$s3_url?>assets/images/partyposts/<?=$value['party_posts_photo_video']?>" alt="post image">
  <?php
  }
  elseif($value['photo_or_video'] == 1)
  {
  ?>
  
  <video style="padding:5px" width="285" height="200" controls>
  <source src="<?=$s3_url?>assets/images/partyposts/<?=$value['party_posts_photo_video']?>" type="video/mp4">
  <source src="<?=$s3_url?>assets/images/partyposts/<?=$value['party_posts_photo_video']?>" type="video/ogg">
  Your browser does not support the video tag.
   </video>
  <?php
   } 
   ?>



 <div class="card-body">
    <p class="card-text"><?=json_decode($value['party_posts_content'])?></p>
   <p class="card-text"><small class="text-muted"><?=date('d-m-Y  h:i A', strtotime($value['party_posts_uploaded_date']));?></small></p>
   <div class="fixed-divider"></div>
   <div class="counts">
      <p class="poll-count"><?=$pollcount?><span>polls</span></p>
      <p class="comment-count"><?=$commentcount?><span>comments</span></p>
   </div>
   <div class="dropdown-divider"></div>
   <a href="https://play.google.com/store/apps/details?id=com.daypoll&hl=en" class="btn btn-sm button-primary text-uppercase d-table mx-auto text-white mt-3">Download App</a>
  </div>
      </div>
   </div>
<!--Home Section End -->
<?php } ?>







<?php 
       if($_GET['type']==2){
         
    ?>
       
<div class="card" style="width: 18rem;">
        
        <div class="card-header">
            <?=$value['group_posts_title'];?>
        </div>

   <?php if($value['photo_or_video'] == 0 ){?>       
  <img class="card-img-top" src="<?=$s3_url?>assets/images/groupposts/<?=$value['group_posts_photo_video']?>" alt="post image">
  <?php
  }
  elseif($value['photo_or_video'] == 1)
  {
  ?>
  
  <video style="padding:5px" width="285" height="200" controls>
  <source src="<?=$s3_url?>assets/images/groupposts/<?=$value['group_posts_photo_video']?>" type="video/mp4">
  <source src="<?=$s3_url?>assets/images/groupposts/<?=$value['group_posts_photo_video']?>" type="video/ogg">
  Your browser does not support the video tag.
   </video>
  <?php
   } 
   ?>     
 
   <div class="card-body">
    <p class="card-text"><?=$value['group_posts_content']?></p>
   <p class="card-text"><small class="text-muted"><?=date('d-m-Y  h:i A', strtotime($value['group_posts_uploaded_date']));?></small></p>
   <div class="fixed-divider"></div>
   <div class="counts">
      <p class="poll-count"><?=$pollcount?><span>polls</span></p>
      <p class="comment-count"><?=$commentcount?><span>comments</span></p>
   </div>
   <div class="dropdown-divider"></div>
   <a href="https://play.google.com/store/apps/details?id=com.daypoll&hl=en" class="btn btn-sm button-primary text-uppercase d-table mx-auto text-white mt-3">Download App</a>
  </div>
      </div>
   </div>
<!--Home Section End -->
<?php } ?>





<?php 
       if($_GET['type']==3){
         
    
        
    ?>
       
<div class="card" style="width: 18rem;">
        
        <div class="card-header">
            <?=$value['challenge_title'];?>
        </div>
<?php
if($value['challenge_type'] == 1){ 

?>

<?php if($value['challenge_posts_exp_media_type'] == 0 ){?>       
  <img class="card-img-top" src="<?=$s3_url?>assets/images/challenge/type1/<?=$value['challenge_posts_sub_post']?>" alt="post image">
  <?php
  }
  elseif($value['challenge_posts_exp_media_type'] == 1)
  {
  ?>
  
  <video style="padding:5px" width="285" height="200" controls>
  <source src="<?=$s3_url?>assets/images/challenge/type1/<?=$value['challenge_posts_sub_post']?>" type="video/mp4">
  <source src="<?=$s3_url?>assets/images/challenge/type1/<?=$value['challenge_posts_sub_post']?>" type="video/ogg">
  Your browser does not support the video tag.
   </video>
  <?php
   } 
   ?>     




  <div class="card-body">
    <p class="card-text"><?=$value['challenge_content']?></p>
   <p class="card-text"><small class="text-muted"><?=date('d-m-Y  h:i A', strtotime($value['challenge_created_date']));?></small></p>
   <div class="fixed-divider"></div>
   <div class="counts">
      <p class="poll-count"><?=$pollcount?><span>polls</span></p>
      <p class="comment-count"><?=$commentcount?><span>comments</span></p>
   </div>
   <div class="dropdown-divider"></div>
   <a href="https://play.google.com/store/apps/details?id=com.daypoll&hl=en" class="btn btn-sm button-primary text-uppercase d-table mx-auto text-white mt-3">Download App</a>
  </div>
      </div>
   </div>
<!--Home Section End -->
<?php } 



elseif($value['challenge_type'] == 2){
?>

<?php if($value['challenge_posts_exp_media_type'] == 0 ){?>       
  <img class="card-img-top" src="<?=$s3_url?>assets/images/challenge/type2/<?=$value['challenge_posts_sub_post']?>" alt="post image">
  <?php
  }
  elseif($value['challenge_posts_exp_media_type'] == 1)
  {
  ?>
  
  <video style="padding:5px" width="285" height="200" controls>
  <source src="<?=$s3_url?>assets/images/challenge/type2/<?=$value['challenge_posts_sub_post']?>" type="video/mp4">
  <source src="<?=$s3_url?>assets/images/challenge/type2/<?=$value['challenge_posts_sub_post']?>" type="video/ogg">
  Your browser does not support the video tag.
   </video>
  <?php
   } 

   ?>     




  <div class="card-body">
    <p class="card-text"><?=$value['challenge_content']?></p>
   <p class="card-text"><small class="text-muted"><?=date('d-m-Y  h:i A', strtotime($value['challenge_created_date']));?></small></p>
   <div class="fixed-divider"></div>
   <div class="counts">
      <p class="poll-count"><?=$pollcount?><span>polls</span></p>
      <p class="comment-count"><?=$commentcount?><span>comments</span></p>
   </div>
   <div class="dropdown-divider"></div>
   <a href="https://play.google.com/store/apps/details?id=com.daypoll&hl=en" class="btn btn-sm button-primary text-uppercase d-table mx-auto text-white mt-3">Download App</a>
  </div>
      </div>
   </div>
<?php }
}
 ?>





<footer>
   <div class="container">
      <div class="row">
         <div class="col-sm-8">
            <div class="row contact-links">
               <div class="col-sm-3">
                  <h3>Company</h3>
                  <ul>
                     <a href="#"><li>About Daypoll</li></a>
                     <a href="#"><li>Contact</li></a>
                  </ul>
               </div>
               <!--<div class="col-sm-3">-->
               <!--   <h3>Community</h3>-->
               <!--   <ul>-->
               <!--      <a href="#"><li>Support</li></a>-->
               <!--      <a href="#"><li>Community Policy</li></a>-->
               <!--      <a href="#"><li>Safety Center</li></a>-->
               <!--   </ul>-->
               <!--</div>-->
               <div class="col-sm-3">
                  <h3>Legal</h3>
                  <ul>
                    <a href="terms-and-policies.html"><li>Terms And Policies</li></a>
                     <a href="privacy-policy.html"><li>Privacy Policy</li></a>
                     <!--<a href="#"><li>Law Enforcement</li></a>-->
                  </ul>
               </div>
               <!--<div class="col-sm-3">-->
               <!--   <h3>Contact</h3>-->
               <!--   <ul>-->
               <!--      <a href="#"><li>Virtual Items</li></a>-->
               <!--      <a href="#"><li>Copyright Policy</li></a>-->
               <!--      <a href="#"><li>Open Source</li></a>-->
               <!--   </ul>-->
               <!--</div>-->
            </div>
         </div>

         <div class="col-sm-4">
            <div class="row contact-social-links">
               <div class="follow-us text-center">
                  <span class="text-center">Follow us</span>
                  <ul>
                     <li>
                        <a class="follow-img-1" target="_blank" href="https://www.instagram.com/daypoll_app/"></a>
                        <p>Instagram</p>
                     </li>
                     <li>
                        <a class="follow-img-2" target="_blank" href="https://www.facebook.com/daypollapp"></a>
                        <p>Facebook</p>
                     </li>
                     <li>
                        <a class="follow-img-3" target="_blank" href="https://twitter.com/daypoll_app"></a>
                        <p>Twitter</p>
                     </li>
                     <li>
                        <a class="follow-img-4" target="_blank" href="https://www.youtube.com/channel/UCaVhq3ARtYsp-BY7e3reLwA"></a>
                        <p>Youtube</p>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</footer>


<!-- js file -->
<script src="<?=base_url();?>assets/postview/js/jquery-3.3.1.min.js" ></script>
<!-- bootstrap js file -->
<script src="<?=base_url();?>assets/postview/js/bootstrap.min.js" ></script>
<!-- popper js file -->
<script src="<?=base_url();?>assets/postview/js/popper.min.js" ></script>

<!-- script js file -->
<script src="<?=base_url();?>assets/postview/js/scroll.js"></script>
<script src="<?=base_url();?>assets/postview/js/script.js"></script>



</body>
</html>