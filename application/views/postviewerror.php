<!DOCTYPE html>
<html lang="en">
<head>
   <title>Daypoll | Mobile App Landing HTML Templates</title>
   <meta charset="utf-8">
   <meta name="author" content="pixelstrap">
   <meta name="description" content="">
   <meta name="keywords" content="">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <meta http-equiv="X-UA-Compatible" content="IE=edge" />
   <!-- Fav icon -->
   <link rel="shortcut icon" href="<?=base_url();?>assets/postview/images/favicon.png">

   <!-- Font Family-->
   <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

   <!-- Font Awesome -->
   <link rel="stylesheet" href="<?=base_url();?>assets/postview/css/font-awesome.min.css" >

   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="<?=base_url();?>assets/postview/css/bootstrap.css">

 

   <!-- Style css-->
   <link rel="stylesheet" href="<?=base_url();?>assets/postview/css/style.css">

   <!-- Responsive css-->
   <link rel="stylesheet" href="<?=base_url();?>assets/postview/css/responsive.css">




</head>
<body data-spy="scroll" data-target=".navbar" data-offset="80">

<!-- Preloader -->
<div class="loader-wrapper">
   <div class="loader"></div>
</div>
<!-- Preloader end -->

<!-- Home Section start-->
   <div class="container">
      <div class="row justify-content-center">
         <div class="col-md-12">
            <div class="section-header">
               <div>
                  <img src="<?=base_url();?>assets/postview/images/logo-daypoll.png" class="img-fluid mx-auto d-block" alt="daypoll-app">
               </div>
            </div>
         </div>
         
         
         
         
    
       
<div class="card" style="width: 18rem;">
        
        <div class="card-header">
            No data found
        </div>

  
 
  <div class="card-body">
   <div class="fixed-divider"></div>
  
   <div class="dropdown-divider"></div>
   <a href="https://play.google.com/store/apps/details?id=com.daypoll&hl=en" class="btn btn-sm button-primary text-uppercase d-table mx-auto text-white mt-3">Download App</a>
  </div>
      </div>
   </div>
<!--Home Section End -->





<footer>
   <div class="container">
      <div class="row">
         <div class="col-sm-8">
            <div class="row contact-links">
               <div class="col-sm-3">
                  <h3>Company</h3>
                  <ul>
                     <a href="#"><li>About Daypoll</li></a>
                     <a href="#"><li>Contact</li></a>
                  </ul>
               </div>
               <!--<div class="col-sm-3">-->
               <!--   <h3>Community</h3>-->
               <!--   <ul>-->
               <!--      <a href="#"><li>Support</li></a>-->
               <!--      <a href="#"><li>Community Policy</li></a>-->
               <!--      <a href="#"><li>Safety Center</li></a>-->
               <!--   </ul>-->
               <!--</div>-->
               <div class="col-sm-3">
                  <h3>Legal</h3>
                  <ul>
                    <a href="terms-and-policies.html"><li>Terms And Policies</li></a>
                     <a href="privacy-policy.html"><li>Privacy Policy</li></a>
                     <!--<a href="#"><li>Law Enforcement</li></a>-->
                  </ul>
               </div>
               <!--<div class="col-sm-3">-->
               <!--   <h3>Contact</h3>-->
               <!--   <ul>-->
               <!--      <a href="#"><li>Virtual Items</li></a>-->
               <!--      <a href="#"><li>Copyright Policy</li></a>-->
               <!--      <a href="#"><li>Open Source</li></a>-->
               <!--   </ul>-->
               <!--</div>-->
            </div>
         </div>

         <div class="col-sm-4">
            <div class="row contact-social-links">
               <div class="follow-us text-center">
                  <span class="text-center">Follow us</span>
                  <ul>
                     <li>
                        <a class="follow-img-1" target="_blank" href="https://www.instagram.com/daypoll_app/"></a>
                        <p>Instagram</p>
                     </li>
                     <li>
                        <a class="follow-img-2" target="_blank" href="https://www.facebook.com/daypollapp"></a>
                        <p>Facebook</p>
                     </li>
                     <li>
                        <a class="follow-img-3" target="_blank" href="https://twitter.com/daypoll_app"></a>
                        <p>Twitter</p>
                     </li>
                     <li>
                        <a class="follow-img-4" target="_blank" href="https://www.youtube.com/channel/UCaVhq3ARtYsp-BY7e3reLwA"></a>
                        <p>Youtube</p>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</footer>


<!-- js file -->
<script src="<?=base_url();?>assets/postview/js/jquery-3.3.1.min.js" ></script>
<!-- bootstrap js file -->
<script src="<?=base_url();?>assets/postview/js/bootstrap.min.js" ></script>
<!-- popper js file -->
<script src="<?=base_url();?>assets/postview/js/popper.min.js" ></script>

<!-- script js file -->
<script src="<?=base_url();?>assets/postview/js/scroll.js"></script>
<script src="<?=base_url();?>assets/postview/js/script.js"></script>



</body>
</html>